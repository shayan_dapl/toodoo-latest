<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Languages;

class LangMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:lang';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update language files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = Languages::all();
        $lang = [];
        foreach ($data as $each) {
            $lang['nl'][$each->segment][$each->field] = $each->nl;
            $lang['en'][$each->segment][$each->field] = $each->en;
            $lang['fr'][$each->segment][$each->field] = $each->fr;
        }
        if (!empty($lang)) {
            foreach ($lang as $folder => $files) {
                foreach ($files as $file => $content) {
                    $name = $file . '.php';
                    if (!file_exists($file)) {
                        fopen(public_path('../resources/lang/' . $folder . '/'.$name), 'w');
                    }
                    $fullPath = public_path('../resources/lang/' . $folder . '/'.$name);
                    file_put_contents($fullPath, '<?php return $arr = ' . var_export($content, true) . '; ?>');
                }
            }
        }
        return response()->json(true);
    }
}
