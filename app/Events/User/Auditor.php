<?php

namespace App\Events\User;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Auditor
{
    use Dispatchable, InteractsWithSockets;

    public $auditor;
    public $userId;
    public $emailCheck;
    public $email;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($owner, $id, $emailCheck = null, $email = null)
    {
        $this->auditor = $owner;
        $this->userId = $id;
        $this->emailCheck = $emailCheck;
        $this->email = $email;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
