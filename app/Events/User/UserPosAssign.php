<?php

namespace App\Events\User;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserPosAssign
{
    use Dispatchable, InteractsWithSockets;

    public $positions;
    public $companyId;
    public $userId;
    public $isSpecial;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($position, $company, $user, $special)
    {
        $this->positions = $position;
        $this->companyId = $company;
        $this->userId = $user;
        $this->isSpecial = $special;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
