<?php

function getAudits($process_id = 0, $lead_auditor = 0, $month = 0, $year = 0)
{
    if ($process_id != 0) {
        if ($lead_auditor == 0) {
            $audits = (new \App\Repositories\AuditCalender)->getAudits($process_id, $month, $year);
        } else {
            $leadAauditor = (new \App\Repositories\AuditCalender)->getleadAauditor($process_id, $lead_auditor, $month, $year);
            $auditees = (new \App\Repositories\AuditCalender)->getAuditees($process_id, $lead_auditor, $month, $year);
            $auditors = (new \App\Repositories\AuditCalender)->getAuditor($process_id, $lead_auditor, $month, $year);
            $audits = array_merge($leadAauditor, $auditees, $auditors);
        }

        if (!empty($audits[0])) {
            return $audits;
        } else {
            return "error";
        }
    }
}

function getAuditStatus($audit_id = 0)
{
    if ($audit_id != 0) {
        $audits = (new \App\Repositories\AuditCalender)->getExecutedAudit($audit_id);

        if ($audits->count() > 0) {
            return 'executed';
        } else {
            return "error";
        }
    }
}

function auditTypesList($iso, $parent = 0)
{
    $options = ['' => translate('form.select')];
    $language = \Config::get('settings.lang');
    $name = 'name_' . $language[session('lang')];
    $types = \App\Models\AuditTypeMaster::where('parent_id', $parent)->where('iso_ref_id', $iso);
    if ($types->count() > 0) {
        $types = $types->select('id', 'clause', $name)->get()->toArray();

        foreach ($types as $each) {
            $each[$name] = $each['clause'] . " " . $each[$name];
            $options[$each['id']] = $each[$name];
            $children = auditTypesList($iso, $each['id']);
            if (!empty($children)) {
                foreach ($children as $id => $child) {
                    $options[$id] = $child;
                }
            }
        }
    }
    return $options;
}

function selectedProcessNames($selectedProcess)
{
    $selectedProcessNames = '';
    foreach ($selectedProcess as $each) {
        $selectedProcessNames .= $each->processDetail->name .', ';
    }
    return rtrim($selectedProcessNames, ', ');
}

function pastMeetings($uniqueMeetingProcess, $meetingMonthArr, $meetingYearArr)
{
    $plannedAuditMeeting = [];
    $m = 0;
    foreach ($uniqueMeetingProcess as $kt => $vt) {
        $plannedAuditMeeting[$m]['process'] = $kt;
        foreach ($meetingMonthArr as $i => $j) {
            $auditsByProcess = [];
            foreach ($uniqueMeetingProcess[$kt] as $eachpros => $eachVal) {
                $auditStatus = '';
                $curYear = $meetingYearArr[$i];
                if ($uniqueMeetingProcess[$kt][$eachpros]['release_year'] != 0) {
                    if (($uniqueMeetingProcess[$kt][$eachpros]['release_year'] == $curYear) && ($uniqueMeetingProcess[$kt][$eachpros]['release_month'] == $i)) {
                        $auditsByProcess = $uniqueMeetingProcess[$kt][$eachpros];
                    }
                } else {
                    if (($uniqueMeetingProcess[$kt][$eachpros]['plan_year'] == $curYear) && ($uniqueMeetingProcess[$kt][$eachpros]['plan_month'] == $i)) {
                        $auditsByProcess = $uniqueMeetingProcess[$kt][$eachpros];
                    }
                }
                if (!empty($auditsByProcess)) {
                    if ($auditsByProcess['audit_status']==2 || $auditsByProcess['audit_status']==3) {
                        $auditStatus = 'executed';
                    } else {
                        $auditStatus = 'error';
                    }
                }
                if (!empty($auditsByProcess)) {
                    if ($auditsByProcess['audit_status'] == 2) {
                        $plannedAuditMeeting[$m]['months'][$i]['tdCls'] = 'back4a';
                        $plannedAuditMeeting[$m]['months'][$i]['aCls'] = 'text-default noCurserCls';
                    } elseif ($auditStatus == 'executed' and $auditsByProcess['audit_status'] == 1) {
                        $plannedAuditMeeting[$m]['months'][$i]['tdCls'] = 'backff';
                        $plannedAuditMeeting[$m]['months'][$i]['aCls'] = 'text-default noCurserCls';
                    } else {
                        $plannedAuditMeeting[$m]['months'][$i]['tdCls'] = '';
                        $plannedAuditMeeting[$m]['months'][$i]['aCls'] = '';
                    }

                    if ($auditsByProcess['release_date'] != '' and $auditsByProcess['audit_status'] == 2) {
                        $plannedAuditMeeting[$m]['months'][$i]['userType'] = '';
                        $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['release_date']));
                        $plannedAuditMeeting[$m]['months'][$i]['title'] =translate('words.original').' ' . $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                        $plannedAuditMeeting[$m]['months'][$i]['link'] = '#';
                    } elseif ($auditsByProcess['audit_date'] != '' and $auditStatus == 'executed' and $auditsByProcess['audit_status'] != 2) {
                        if ($auditsByProcess['auditor_type'] == 1) {
                            $plannedAuditMeeting[$m]['months'][$i]['userType'] = translate('words.lead_auditor');
                        } elseif ($auditsByProcess['auditor_type'] == 2) {
                            $plannedAuditMeeting[$m]['months'][$i]['userType'] = translate('words.auditor');
                        } else {
                            $plannedAuditMeeting[$m]['months'][$i]['userType'] = translate('words.auditee');
                        }
                        $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                        $plannedAuditMeeting[$m]['months'][$i]['title'] =translate('words.original').' ' . $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                        $plannedAuditMeeting[$m]['months'][$i]['link'] = '#';
                    } elseif ($auditsByProcess['audit_date'] != '' and $auditStatus != 'executed') {
                        if ($auditsByProcess['auditor_type'] == 1) {
                            $plannedAuditMeeting[$m]['months'][$i]['userType'] = translate('words.lead_auditor');
                            $plannedAuditMeeting[$m]['months'][$i]['link'] = url('/audit/audit-prepare/' . $auditsByProcess['id']);
                        } elseif ($auditsByProcess['auditor_type'] == 2) {
                            $plannedAuditMeeting[$m]['months'][$i]['userType'] = translate('words.auditor');
                            $plannedAuditMeeting[$m]['months'][$i]['link'] = url('/audit/audit-prepare/' . $auditsByProcess['id']);
                        } else {
                            $plannedAuditMeeting[$m]['months'][$i]['userType'] = translate('words.auditee');
                            $plannedAuditMeeting[$m]['months'][$i]['aCls'] = 'noCurserCls';
                            $plannedAuditMeeting[$m]['months'][$i]['link'] = '#';
                        }
                        $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                        $plannedAuditMeeting[$m]['months'][$i]['title'] = translate('words.original').' ' . $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                    } else {
                        if (Auth::guard('customer')->user()->id == $auditsByProcess['choose_lead_auditor']) {
                            $plannedAuditMeeting[$m]['months'][$i]['userType'] = translate('words.lead_auditor');
                            $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = 'X';
                            $plannedAuditMeeting[$m]['months'][$i]['title'] = 'Edit';
                            $plannedAuditMeeting[$m]['months'][$i]['link'] = url('/audit/audit-prepare/' . $auditsByProcess['id']);
                        } else {
                            $plannedAuditMeeting[$m]['months'][$i]['userType'] = '';
                            $plannedAuditMeeting[$m]['months'][$i]['title'] = '';
                            $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = '';
                            $plannedAuditMeeting[$m]['months'][$i]['tdCls'] = '';
                            $plannedAuditMeeting[$m]['months'][$i]['aCls'] = 'noCurserCls';
                            $plannedAuditMeeting[$m]['months'][$i]['link'] = "#";
                        }
                    }
                } else {
                    $plannedAuditMeeting[$m]['months'][$i]['userType'] = '';
                    $plannedAuditMeeting[$m]['months'][$i]['tdCls'] = '';
                    $plannedAuditMeeting[$m]['months'][$i]['aCls'] = 'noCurserCls';
                    $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = ' ';
                    $plannedAuditMeeting[$m]['months'][$i]['title'] = '';
                    $plannedAuditMeeting[$m]['months'][$i]['link'] = "#";
                }
            }
        }
        $m++;
    }
    return $plannedAuditMeeting;
}

function plannedAudit($uniqueProcess, $curYearPlanningTab)
{
    $plannedAudit = [];
    $j = 0;
    foreach ($uniqueProcess as $key => $v) {
        $plannedAudit[$j]['process'] = $key;
        for ($i = 1; $i < 13; $i++) {
            $auditsByProcess = [];
            foreach ($uniqueProcess[$key] as $eachprocess => $eachValue) {
                $auditStatus = '';
                if ($uniqueProcess[$key][$eachprocess]['release_year'] != 0) {
                    if (($uniqueProcess[$key][$eachprocess]['release_year'] == $curYearPlanningTab) && ($uniqueProcess[$key][$eachprocess]['release_month'] == $i)) {
                        $auditsByProcess = $uniqueProcess[$key][$eachprocess];
                    }
                } else {
                    if (($uniqueProcess[$key][$eachprocess]['plan_year'] == $curYearPlanningTab) && ($uniqueProcess[$key][$eachprocess]['plan_month'] == $i)) {
                        $auditsByProcess = $uniqueProcess[$key][$eachprocess];
                    }
                }
                if ($auditsByProcess != 'error') {
                    if ($uniqueProcess[$key][$eachprocess]['audit_status']==2 || $uniqueProcess[$key][$eachprocess]['audit_status']==3) {
                        $auditStatus = 'executed';
                    } else {
                        $auditStatus = 'error';
                    }
                }
                if (($curYearPlanningTab == date('Y') and $i < date('m')) or ($curYearPlanningTab < date('Y'))) {
                    if (!empty($auditsByProcess)) {
                        if ($auditsByProcess['audit_date'] != '') {
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                            $plannedAudit[$j]['months'][$i]['title'] = translate('words.original').' '. $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                            $plannedAudit[$j]['months'][$i]['aCls'] = 'text-default noCurserCls';
                        } else {
                            $plannedAudit[$j]['months'][$i]['auditDate'] = '';
                            $plannedAudit[$j]['months'][$i]['title'] = '';
                            $plannedAudit[$j]['months'][$i]['aCls'] = '';
                        }
                        $plannedAudit[$j]['months'][$i]['link'] = '#';
                        if ($auditsByProcess['audit_status'] == 2) {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'back4a';
                        } elseif ($auditStatus == 'executed') {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'backff';
                        } else {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'backe8';
                        }
                        if ($auditsByProcess['auditor_type'] == 1) {
                            $plannedAudit[$j]['months'][$i]['userType'] = translate('words.lead_auditor');
                        } elseif ($auditsByProcess['auditor_type'] == 2) {
                            $plannedAudit[$j]['months'][$i]['userType'] = translate('words.auditor');
                        } else {
                            $plannedAudit[$j]['months'][$i]['userType'] = translate('words.auditee');
                        }
                    } else {
                        $plannedAudit[$j]['months'][$i]['aCls'] = '';
                        $plannedAudit[$j]['months'][$i]['auditDate'] = '';
                        $plannedAudit[$j]['months'][$i]['title'] = '';
                        $plannedAudit[$j]['months'][$i]['link'] = '#';
                        $plannedAudit[$j]['months'][$i]['tdCls'] = 'backe8';
                        $plannedAudit[$j]['months'][$i]['userType'] = '';
                    }
                } else {
                    if (!empty($auditsByProcess)) {
                        if ($auditsByProcess['audit_status'] == 2) {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'back4a';
                            $plannedAudit[$j]['months'][$i]['aCls'] = 'text-default noCurserCls';
                        } elseif ($auditStatus == 'executed') {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'backff';
                            $plannedAudit[$j]['months'][$i]['aCls'] = 'text-default noCurserCls';
                        } else {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = '';
                            $plannedAudit[$j]['months'][$i]['aCls'] = '';
                        }
                        if ($auditsByProcess['release_date'] != '' and $auditsByProcess['audit_status'] == 2) {
                            if ($auditsByProcess['auditor_type'] == 1) {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.lead_auditor');
                            } elseif ($auditsByProcess['auditor_type'] == 2) {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.auditor');
                            } else {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.auditee');
                            }
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['release_date']));
                            $plannedAudit[$j]['months'][$i]['title'] = translate('words.original').' '. $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                            $plannedAudit[$j]['months'][$i]['link'] = '#';
                        } elseif ($auditsByProcess['audit_date'] != '' and $auditStatus == 'executed' and $auditsByProcess['audit_status'] != 2) {
                            if ($auditsByProcess['auditor_type'] == 1) {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.lead_auditor');
                            } elseif ($auditsByProcess['auditor_type'] == 2) {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.auditor');
                            } else {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.auditee');
                            }
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                            $plannedAudit[$j]['months'][$i]['title'] = translate('words.original').' '. $auditsByProcess['original_month'] . '/' .$auditsByProcess['original_year'];
                            $plannedAudit[$j]['months'][$i]['link'] = '#';
                        } elseif ($auditsByProcess['audit_date'] != '' and $auditStatus != 'executed') {
                            if ($auditsByProcess['auditor_type'] == 1) {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.lead_auditor');
                                $plannedAudit[$j]['months'][$i]['link'] = url('/audit/audit-prepare/' . $auditsByProcess['id']);
                            } elseif ($auditsByProcess['auditor_type'] == 2) {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.auditor');
                                $plannedAudit[$j]['months'][$i]['link'] = url('/audit/audit-prepare/' . $auditsByProcess['id']);
                            } else {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.auditee');
                                $plannedAudit[$j]['months'][$i]['aCls'] = 'noCurserCls';
                                $plannedAudit[$j]['months'][$i]['link'] = '#';
                            }
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                            $plannedAudit[$j]['months'][$i]['title'] = translate('words.original').' '. $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                        } else {
                            if (Auth::guard('customer')->user()->id == $auditsByProcess['choose_lead_auditor']) {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.lead_auditor');
                                $plannedAudit[$j]['months'][$i]['auditDate'] = 'X';
                                $plannedAudit[$j]['months'][$i]['title'] = translate('words.edit');
                                $plannedAudit[$j]['months'][$i]['link'] = url('/audit/audit-prepare/' . $auditsByProcess['id']);
                            } else {
                                $plannedAudit[$j]['months'][$i]['userType'] = '';
                                $plannedAudit[$j]['months'][$i]['title'] = '';
                                $plannedAudit[$j]['months'][$i]['auditDate'] = '';
                                $plannedAudit[$j]['months'][$i]['tdCls'] = '';
                                $plannedAudit[$j]['months'][$i]['aCls'] = 'noCurserCls';
                                $plannedAudit[$j]['months'][$i]['link'] = "#";
                            }
                        }
                    } else {
                        $plannedAudit[$j]['months'][$i]['userType'] = '';
                        $plannedAudit[$j]['months'][$i]['tdCls'] = '';
                        $plannedAudit[$j]['months'][$i]['aCls'] = 'noCurserCls';
                        $plannedAudit[$j]['months'][$i]['auditDate'] = ' ';
                        $plannedAudit[$j]['months'][$i]['title'] = '';
                        $plannedAudit[$j]['months'][$i]['link'] = "#";
                    }
                }
            }
        }
        $j++;
    }
    return $plannedAudit;
}
