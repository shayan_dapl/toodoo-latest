<?php
function auditCalender($uniqueProcess, $curYear, $category, $branchId = '')
{
    $plannedAudit = [];
    $j = 0;
    $allow = 1;
    foreach ($uniqueProcess as $key => $v) {
        $plannedAudit[$j]['process'] = $key;
        for ($i = 1; $i < 13; $i++) {
            $auditsByProcess = [];
            foreach ($uniqueProcess[$key] as $eachprocess => $eachValue) {
                $auditStatus = '';
                if ($uniqueProcess[$key][$eachprocess]['release_year'] != 0) {
                    if (($uniqueProcess[$key][$eachprocess]['release_year'] == $curYear) && ($uniqueProcess[$key][$eachprocess]['release_month'] == $i)) {
                        $auditsByProcess = $uniqueProcess[$key][$eachprocess];
                    }
                } else {
                    if (($uniqueProcess[$key][$eachprocess]['plan_year'] == $curYear) && ($uniqueProcess[$key][$eachprocess]['plan_month'] == $i)) {
                        $auditsByProcess = $uniqueProcess[$key][$eachprocess];
                    }
                }
                if (!empty($auditsByProcess)) {
                    if ($auditsByProcess['audit_status']==2 || $auditsByProcess['audit_status']==3) {
                        $auditStatus = 'executed';
                    } else {
                        $auditStatus = 'error';
                    }
                }
                if (($curYear == date('Y') and $i < date('m') and $allow != 1) or ($curYear < date('Y')and $allow != 1)) {
                    if (!empty($auditsByProcess)) {
                        if ($auditsByProcess['audit_date'] != '') {
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                            $plannedAudit[$j]['months'][$i]['title'] = translate('words.original').' '. $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                            $plannedAudit[$j]['months'][$i]['aCls'] = 'text-default noCurserCls';
                        } else {
                            $plannedAudit[$j]['months'][$i]['auditDate'] = '';
                            $plannedAudit[$j]['months'][$i]['title'] = '';
                            $plannedAudit[$j]['months'][$i]['aCls'] = '';
                        }
                        $plannedAudit[$j]['months'][$i]['link'] = '#';
                        if ($auditsByProcess['audit_status'] == 2) {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'back4a';
                        } elseif ($auditStatus == 'executed' and $auditsByProcess['audit_status'] == 1) {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'backff';
                        } else {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'backe8';
                        }
                        $plannedAudit[$j]['months'][$i]['userType'] = '';
                    } else {
                        $plannedAudit[$j]['months'][$i]['aCls'] = '';
                        $plannedAudit[$j]['months'][$i]['auditDate'] = '';
                        $plannedAudit[$j]['months'][$i]['title'] = '';
                        $plannedAudit[$j]['months'][$i]['link'] = '#';
                        $plannedAudit[$j]['months'][$i]['tdCls'] = 'backe8';
                        $plannedAudit[$j]['months'][$i]['userType'] = '';
                    }
                } else {
                    if (!empty($auditsByProcess)) {
                        if ($auditsByProcess['audit_status'] == 2) {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'back4a';
                            $plannedAudit[$j]['months'][$i]['aCls'] = 'text-default noCurserCls';
                        } elseif ($auditStatus == 'executed' and $auditsByProcess['audit_status'] == 3) {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'backff';
                            $plannedAudit[$j]['months'][$i]['aCls'] = 'text-default noCurserCls';
                        } else {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = '';
                            $plannedAudit[$j]['months'][$i]['aCls'] = '';
                        }
                        if ($auditsByProcess['release_date'] != '' and $auditsByProcess['audit_status'] == 2) {
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['release_date']));
                            $plannedAudit[$j]['months'][$i]['title'] = translate('words.original').' '. $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                            $plannedAudit[$j]['months'][$i]['link'] = '#';
                        } elseif ($auditsByProcess['audit_date'] != '' and $auditStatus == 'executed' and $auditsByProcess['audit_status'] != 2) {
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                            $plannedAudit[$j]['months'][$i]['title'] = translate('words.original').' '. $auditsByProcess['original_month'] . '/' .$auditsByProcess['original_year'];
                            $plannedAudit[$j]['months'][$i]['link'] = '#';
                        } elseif ($auditsByProcess['audit_date'] != '' and $auditStatus != 'executed') {
                            $plannedAudit[$j]['months'][$i]['link'] = url('/audit/audit-prepare/' . $auditsByProcess['id']);
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                            $plannedAudit[$j]['months'][$i]['title'] = translate('words.original').' '. $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                        } else {
                            if (Auth::guard('customer')->user()->id == $auditsByProcess['choose_lead_auditor']) {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.lead_auditor');
                                $plannedAudit[$j]['months'][$i]['auditDate'] = 'X';
                                $plannedAudit[$j]['months'][$i]['title'] = translate('words.edit');
                                ;
                                $plannedAudit[$j]['months'][$i]['link'] = url('/audit/audit-prepare/' . $auditsByProcess['id']);
                            } else {
                                $plannedAudit[$j]['months'][$i]['userType'] = '';
                                $plannedAudit[$j]['months'][$i]['title'] = '';
                                $plannedAudit[$j]['months'][$i]['auditDate'] = 'X';
                                $plannedAudit[$j]['months'][$i]['tdCls'] = '';
                                $plannedAudit[$j]['months'][$i]['aCls'] = (Auth::guard('customer')->user()->type == '2') ? '' : 'noCurserCls';
                                $plannedAudit[$j]['months'][$i]['link'] = (Auth::guard('customer')->user()->type == '2') ? url('/audit/audit-prepare/' . $auditsByProcess['id']) : '#';
                            }
                        }
                    } else {
                        if (Auth::guard('customer')->user()->type == '2') {
                            $panningLink = url('/audit/planning-add/' . $uniqueProcess[$key][$eachprocess]['process_id']) . '/' . $i . '/' . $curYear.'/'. $category;
                            if ($branchId != '') {
                                $panningLink .= '/'.$branchId;
                            }
                            $plannedAudit[$j]['months'][$i]['userType'] = '';
                            $plannedAudit[$j]['months'][$i]['tdCls'] = '';
                            $plannedAudit[$j]['months'][$i]['aCls'] = 'auditadd';
                            $plannedAudit[$j]['months'][$i]['auditDate'] = '';
                            $plannedAudit[$j]['months'][$i]['title'] = '';
                            $plannedAudit[$j]['months'][$i]['link'] = $panningLink;
                        } else {
                            $plannedAudit[$j]['months'][$i]['userType'] = '';
                            $plannedAudit[$j]['months'][$i]['tdCls'] = '';
                            $plannedAudit[$j]['months'][$i]['aCls'] = 'noCurserCls';
                            $plannedAudit[$j]['months'][$i]['auditDate'] = '';
                            $plannedAudit[$j]['months'][$i]['title'] = '';
                            $plannedAudit[$j]['months'][$i]['link'] = "#";
                        }
                    }
                }
            }
        }
        $j++;
    }
    return $plannedAudit;
}

function auditCalenderPrint($uniqueProcess, $curYear, $category, $branchId = '')
{
    $plannedAudit = [];
    $j = 0;
    $allow = 1;
    foreach ($uniqueProcess as $key => $v) {
        $plannedAudit[$j]['process'] = $key;
        for ($i = 1; $i < 13; $i++) {
            $auditsByProcess = [];
            foreach ($uniqueProcess[$key] as $eachprocess => $eachValue) {
                $auditStatus = '';
                if ($uniqueProcess[$key][$eachprocess]['release_year'] != 0) {
                    if (($uniqueProcess[$key][$eachprocess]['release_year'] == $curYear) && ($uniqueProcess[$key][$eachprocess]['release_month'] == $i)) {
                        $auditsByProcess = $uniqueProcess[$key][$eachprocess];
                    }
                } else {
                    if (($uniqueProcess[$key][$eachprocess]['plan_year'] == $curYear) && ($uniqueProcess[$key][$eachprocess]['plan_month'] == $i)) {
                        $auditsByProcess = $uniqueProcess[$key][$eachprocess];
                    }
                }
                if (!empty($auditsByProcess)) {
                    if ($auditsByProcess['audit_status']==2 || $auditsByProcess['audit_status']==3) {
                        $auditStatus = 'executed';
                    } else {
                        $auditStatus = 'error';
                    }
                }
                if (($curYear == date('Y') and $i < date('m') and $allow != 1) or ($curYear < date('Y')and $allow != 1)) {
                    if (!empty($auditsByProcess)) {
                        if ($auditsByProcess['audit_date'] != '') {
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                        } else {
                            $plannedAudit[$j]['months'][$i]['auditDate'] = '';
                        }
                        if ($auditsByProcess['audit_status'] == 2) {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'style=background-color:#4CAF50;color:#fff';
                        } elseif ($auditStatus == 'executed' and $auditsByProcess['audit_status'] == 1) {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'backff';
                        } else {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'backe8';
                        }
                        $plannedAudit[$j]['months'][$i]['userType'] = '';
                    } else {
                        $plannedAudit[$j]['months'][$i]['auditDate'] = '';
                        $plannedAudit[$j]['months'][$i]['tdCls'] = 'backe8';
                        $plannedAudit[$j]['months'][$i]['userType'] = '';
                    }
                } else {
                    if (!empty($auditsByProcess)) {
                        if ($auditsByProcess['audit_status'] == 2) {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'style=background-color:#4CAF50;color:#fff';
                        } elseif ($auditStatus == 'executed' and $auditsByProcess['audit_status'] == 3) {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = 'backff';
                        } else {
                            $plannedAudit[$j]['months'][$i]['tdCls'] = '';
                        }
                        if ($auditsByProcess['release_date'] != '' and $auditsByProcess['audit_status'] == 2) {
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['release_date']));
                        } elseif ($auditsByProcess['audit_date'] != '' and $auditStatus == 'executed' and $auditsByProcess['audit_status'] != 2) {
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                        } elseif ($auditsByProcess['audit_date'] != '' and $auditStatus != 'executed') {
                            $plannedAudit[$j]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                        } else {
                            if (Auth::guard('customer')->user()->id == $auditsByProcess['choose_lead_auditor']) {
                                $plannedAudit[$j]['months'][$i]['userType'] = translate('words.lead_auditor');
                                $plannedAudit[$j]['months'][$i]['auditDate'] = 'X';
                            } else {
                                $plannedAudit[$j]['months'][$i]['userType'] = '';
                                $plannedAudit[$j]['months'][$i]['auditDate'] = 'X';
                                $plannedAudit[$j]['months'][$i]['tdCls'] = '';
                            }
                        }
                    } else {
                        $plannedAudit[$j]['months'][$i]['userType'] = '';
                        $plannedAudit[$j]['months'][$i]['tdCls'] = '';
                        $plannedAudit[$j]['months'][$i]['auditDate'] = '';
                    }
                }
            }
        }
        $j++;
    }
    return $plannedAudit;
}
