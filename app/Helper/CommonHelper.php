<?php
function strippedText($input_text)
{
    if (strlen($input_text) > 40) {
        $pos = strpos($input_text, ' ', 40);
        if (!empty($pos)) {
            return substr($input_text, 0, $pos) . "...";
        } else {
            return substr($input_text, 0, 40) . "...";
        }
    } else {
        return $input_text;
    }
}

function timeLeft($auditLeft)
{
    $date1 = date('Y-m-d');
    $date2 = $auditLeft;

    $diff = abs(strtotime($date2) - strtotime($date1));

    $years = floor($diff / (365 * 60 * 60 * 24));
    $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
    $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

    $timeLeft = '';
    if ($years > 0) {
        $timeLeft .= $years . ' years ';
    }
    if ($months > 0) {
        $timeLeft .= $months . ' months ';
    }
    if ($days > 0) {
        $timeLeft .= $days . ' days';
    } elseif (($days <= 0) && (($months == 0) && ($years == 0))) {
        $timeLeft .= 'today';
    }
    return $timeLeft;
}

function extension($file)
{
    if (!empty($file)) {
        return strtolower(explode(".", $file)[count(explode(".", $file)) - 1]);
    } else {
        return false;
    }
}

function getfileType($filetype, $filename, $docType)
{
    $companyId = \Auth::guard('customer')->user()->company_id;
    switch ($filetype) {
        case 'xls':
            $icon = "fa-file-excel-o";
            $url = "https://view.officeapps.live.com/op/embed.aspx?src=" . url('storage/company/' . $companyId . '/documents/' . $docType . '/' . $filename);
            break;
        case 'xlsx':
            $icon = "fa-file-excel-o";
            $url = "https://view.officeapps.live.com/op/embed.aspx?src=" . url('storage/company/' . $companyId . '/documents/' . $docType . '/' . $filename);
            break;
        case 'doc':
            $icon = "fa-file-word-o";
            $url = "https://drive.google.com/viewer?url=" . url('storage/company/' . $companyId . '/documents/' . $docType . '/' . $filename);
            break;
        case 'docx':
            $icon = "fa-file-word-o";
            $url = "https://drive.google.com/viewer?url=" . url('storage/company/' . $companyId . '/documents/' . $docType . '/' . $filename);
            break;
        case 'pdf':
            $icon = "fa-file-pdf-o";
            $url = "https://drive.google.com/viewer?url=" . url('storage/company/' . $companyId . '/documents/' . $docType . '/' . $filename);
            break;
        case 'ppt':
            $icon = "fa-file-powerpoint-o";
            $url = "https://drive.google.com/viewer?url=" . url('storage/company/' . $companyId . '/documents/' . $docType . '/' . $filename);
            break;
        case 'pptx':
            $icon = "fa-file-powerpoint-o";
            $url = "https://drive.google.com/viewer?url=" . url('storage/company/' . $companyId . '/documents/' . $docType . '/' . $filename);
            break;
        default:
            $icon = "fa-file-text-o";
            $url = url('storage/company/' . $companyId . '/documents/' . $docType . '/' . $filename);
            break;
    }
    $fileTypeArr = array('icon' => $icon, 'url' => $url);
    return $fileTypeArr;
}

function days($to)
{
    $datediff = strtotime($to) - time();
    return floor($datediff / (60 * 60 * 24)) . " days left";
}

function dayCount($deadline)
{
    $dateDiff = strtotime($deadline) - time();
    $dateDiff = ceil($dateDiff / (60 * 60 * 24));
    return view('pages.helper.day_count', compact('dateDiff'));
}

function dayDifference($date)
{
    $dateDiff = strtotime($date) - time();
    return ceil($dateDiff / (60 * 60 * 24));
}

function translate($val)
{
    return __($val);
}

function getRiskOrOptTitle($riskOrOptId, $type)
{
    $getTitle = (new \App\Repositories\Home)->getRiskOrOptTitle($riskOrOptId, $type);
    return $getTitle;
}

function getContextStakeholderTitle($contextStakeholderId, $type)
{
    $getTitle = (new \App\Repositories\Home)->getContextStakeholderTitle($contextStakeholderId, $type);
    return $getTitle;
}

function generateRgb()
{
    $rgbColor = 'rgb(' . mt_rand(0, 255) . ',' . mt_rand(0, 255) . ',' . mt_rand(0, 255) . ')';

    return $rgbColor;
}

function monthGap($months)
{
    $datetime1 = date_create($months[0]);
    $datetime2 = date_create($months[1]);
    $interval = date_diff($datetime1, $datetime2);
    return $interval->m;
}

function getNextSubscriptionRemainingDays($price)
{
    $nextSubscription = Auth::guard('customer')->user()->company->next_renewal_date;
    $today = date_create(date("Y-m-d"));
    $nextSubscription = date_create($nextSubscription);
    $remainingDays = date_diff($today, $nextSubscription);
    $amount = (($price * 1)/30) * $remainingDays->days;
    return ['days' => $remainingDays->days, 'amount' => $amount];
}

function checkCompanyAccess($details)
{
    if (!empty($details)) {
        if ($details->company_id) {
            $companyId = $details->company_id;
        } else {
            $companyId = $details->process->company_id;
        }
        if ($companyId != Auth::guard('customer')->user()->company_id) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function fileUpload($file = null, $type = 'image', $path = '/image/', $name = null)
{
    if (!empty($file)) {
        switch ($type) {
            case 'file':
                $filename = $file->getClientOriginalName();
                $file->move(storage_path('app/public/company/' . \Auth::user()->company_id . $path), $filename);
                return $filename;
            case 'ajaximage':
                $extension = explode('/', explode(':', substr($file, 0, strpos($file, ';')))[1])[1];
                $filename = $name . time() . '.' . $extension;
                if (in_array(strtolower($extension), \Config::get('settings.img_extentions'))) {
                    $path = storage_path('app/public/company/' . \Auth::user()->company_id . $path . $filename);
                    \Intervention\Image\ImageManagerStatic::make($file)->resize(100, 100)->save($path);
                }
                return $filename;
            default:
                $extension = $file->getClientOriginalExtension();
                $filename = $name . '-' . time() . '.' . $extension;
                if (in_array(strtolower($extension), \Config::get('settings.img_extentions'))) {
                    $path = storage_path('app/public/company/' . \Auth::user()->company_id . $path . $filename);
                    \Intervention\Image\ImageManagerStatic::make($file->getRealPath())->resize(100, 100)->save($path);
                }
                return $filename;
        }
    }
}

function formatSize($bytes)
{
    $kb = 1024;
    $mb = $kb * 1024;
    $gb = $mb * 1024;
    $tb = $gb * 1024;
    if (($bytes >= 0) && ($bytes < $kb)) {
        return $bytes . ' B';
    } elseif (($bytes >= $kb) && ($bytes < $mb)) {
        return ceil($bytes / $kb) . ' KB';
    } elseif (($bytes >= $mb) && ($bytes < $gb)) {
        return ceil($bytes / $mb) . ' MB';
    } elseif (($bytes >= $gb) && ($bytes < $tb)) {
        return ceil($bytes / $gb) . ' GB';
    } elseif ($bytes >= $tb) {
        return ceil($bytes / $tb) . ' TB';
    } else {
        return $bytes . ' B';
    }
}
