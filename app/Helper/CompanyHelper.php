<?php

function makeTree($elements, $parentId = 0)
{
    $branch = [];

    foreach ($elements as $element) {
        if ($element->parent_id == $parentId) {
            $children = makeTree($elements, $element->id);
            if ($children) {
                $element->children = $children;
            }
            $branch[] = $element;
        }
    }

    return $branch;
}

function structureTree($data, $mode="edit")
{
    return View::make('pages.helper.position_tree', compact('data', 'mode'));
}

function removePosition($id = 0)
{
    if ($id != 0) {
        $check = (new \App\Repositories\UserTree)->getPositionById($id);
        if ($check->count() > 0) {
            $check->delete();
            $findChild = (new \App\Repositories\UserTree)->getPositionByParent($id);
            if ($findChild->count() > 0) {
                $getData = $findChild->get();
                foreach ($getData as $each) {
                    return removePosition($each->id);
                }
            } else {
                return "success";
            }
        } else {
            return "success";
        }
    }
}

function findParent($id, $parentId)
{
    $find = (new \App\Repositories\UserTree)->getPositionByCompany($id);
    if ($find->count() > 0) {
        $parent = $find->first()->parent_id;
        switch ($parent) {
            case $parentId:
                return 'Yes';
            case '':
                return 'No';
            default:
                findParent($parent, $parentId);
                break;
        }
    }
}

function invoice()
{
    $data = \App\Models\TransactionDetails::where('invoice_no', '<>', '')->orderBy('id', 'DESC')->pluck('invoice_no')->first();
    if (empty($data)) {
        return 'TD-'.date('Y').'-200';
    } else {
        $lastInvoice = array_reverse(explode('-', $data));
        if ($lastInvoice[1] == date('Y')) {
            $newNumber = $lastInvoice[0] + 1;
            return 'TD-' . date('Y') . '-' . $newNumber;
        } else {
            return 'TD-'.date('Y').'-200';
        }
    }
}

function informUser($process, $req, $docType)
{
    $lang = \Config::get('settings.lang');
    $rolesEveryone = $process->responsibleEveryone()->get()->where('everyone.status', 4)->count();
    if ($rolesEveryone > 0) {
        $positions = $process->company->positions;
    } else {
        $positions = [];
        $roles = $process->responsibleRoles;
        if ($roles->count() > 0) {
            foreach ($roles as $role) {
                $positions[] = $role->position;
            }
        }
    }
    foreach ($positions as $each) {
        if ($each->structure->count() > 0) {
            foreach ($each->structure as $eachStruct) {
                if (!empty($eachStruct->user) && !empty($eachStruct->user->email)) {
                    $mailData = [
                            'firstName' => $eachStruct->user->fname,
                            'processOwnerName' => Auth::user()->name,
                            'processName' => $process->name,
                            'docName' => $req->input('doc_ref'),
                            'version' => !empty($req->input('version')) ? $req->input('version') : '',
                            'docType' => $docType,
                            'remark' => !empty($req->input('remark')) ? $req->input('remark') : '',
                            'hid' => !empty($req->input('hid')) ? $req->input('hid') : '',
                            'lang' => $lang[$eachStruct->user->language]
                        ];
                    Mail::to($eachStruct->user->email, $eachStruct->user->name)->send(new \App\Mail\DocChangeNotify($mailData));
                }
            }
        }
    }
}

function resourceCalender($allResourceData, $curYear)
{
    $plannedResource = [];
    $j = 0;
    foreach ($allResourceData as $eachValue) {
        $plannedResource[$j]['type'] = $eachValue->resource->type;
        $plannedResource[$j]['serial'] = $eachValue->resource->serial_no;
        $plannedResource[$j]['activityId'] = $eachValue->id;
        for ($i = 1; $i < 13; $i++) {
            if ($eachValue->delegations->count() > 0) {
                foreach ($eachValue->delegations as $eachDelegate) {
                    $dateArr = explode('-', $eachDelegate->deadline);
                    $yer = $dateArr[0];
                    $mnth = $dateArr[1];
                    if ($mnth == $i) {
                        if ($eachDelegate->closed == 1) {
                            $plannedResource[$j]['months'][$i]['content'][] = date('d-m-Y', strtotime($eachDelegate->closed_at));
                            $plannedResource[$j]['months'][$i]['tdCls'][] = 'back4a p5 text-default';
                        } else {
                            $plannedResource[$j]['months'][$i]['content'][] = $eachDelegate->deadline;
                            $plannedResource[$j]['months'][$i]['tdCls'][] = 'openact4a p5';
                        }
                    } else {
                        $plannedResource[$j]['months'][$i]['content'][] = "";
                        $plannedResource[$j]['months'][$i]['tdCls'][] = '';
                    }
                }
            }
        }
        $j++;
    }
    return $plannedResource;
}
