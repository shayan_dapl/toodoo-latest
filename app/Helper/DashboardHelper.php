<?php

function delegationDetails($ref_id)
{
    $actionIds = (new \App\Repositories\Delegation)->getActionIds($ref_id);
    $actionIds = explode(',', $actionIds);
    $members = (new \App\Repositories\Delegation)->getDelegatedMembers($actionIds);

    $typeArr = [
    'immidiate_action' => 'Immidiate action',
    'root_cause' => 'Root cause analysis',
    'corrective_action' => 'Corrective action',
    'effectiveness' => 'Effectiveness',
    ];
    return view('pages.helper.delegation_details', compact('members', 'typeArr'));
}

function delegationStatus($executeData)
{
    $totalRequiredAction = 0;
    if ($executeData->immidiate_actions == 1) {
        $totalRequiredAction += 3;
    }
    if ($executeData->root_cause_analysis == 1) {
        $totalRequiredAction += 1;
    }
    if ($executeData->corrective_measure == 1) {
        $totalRequiredAction += 3;
    }
    if ($executeData->effectiveness == 1) {
        $totalRequiredAction += 1;
    }
    $getactionId = \App\Models\ReportIssueAction::where('company_id', Auth::guard('customer')->user()->company_id)->where('ref_id', $executeData->id)->pluck('id');
    if (!isset($getactionId[0])) {
        return 0;
    } else {
        if ($executeData->effectiveness == 1) {
            $countTotAction = \App\Models\ActionDelegateMember::where('action_id', $getactionId[0])->where('issue_type', 'effectiveness')->count();
            $countCompletedAction = \App\Models\ActionDelegateMember::where('action_id', $getactionId[0])->where('status', 1)->where('issue_type', 'effectiveness')->count();
            if (($countTotAction > $countCompletedAction) || ($countTotAction < 1)) {
                return 1;
            } else {
                return 2;
            }
        } elseif ($executeData->corrective_measure == 1) {
            $countTotAction = \App\Models\ActionDelegateMember::where('action_id', $getactionId[0])->where('issue_type', 'corrective_action')->count();
            $countCompletedAction = \App\Models\ActionDelegateMember::where('action_id', $getactionId[0])->where('status', 1)->where('issue_type', 'corrective_action')->count();
            if ($countTotAction > $countCompletedAction) {
                return 1;
            } else {
                return 2;
            }
        }
    }
}
