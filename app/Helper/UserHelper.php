<?php

function organizationStructure($data, $top, $all, $branch)
{
    $branches = [];
    $loopCount = 0;
    $children = $data->children;
    if (!empty($children)) {
        foreach ($children as $child) {
            $structureArr = $child->structure->toArray();
            if (!empty($structureArr)) {
                $structureArr[0]['name'] = $structureArr[0]['title'];
                $structureArr[0]['title'] = $structureArr[0]['fullname'];
                $branches[$loopCount] = $structureArr[0];
                if (empty($all) || (!empty($all) && $all == "all") || $top == 0) {
                    $subChildren = organizationStructure($child, $top, $all, $branch);
                    if (!empty($subChildren) && $branch == $child->branches->branch->id) {
                        $branches[$loopCount]['children'] = $subChildren;
                    }
                }
                $loopCount++;
            }
        }
    }
    return $branches;
}

function removeOrgPosition($id = 0)
{
    if ($id != 0) {
        $check = (new \App\Repositories\UserTree)->getOrgStructByID($id);
        if ($check->count() > 0) {
            $check->delete();
            $findChild = (new \App\Repositories\UserTree)->getOrgStructByParent($id);
            if ($findChild->count() > 0) {
                $getData = $findChild->get();
                foreach ($getData as $each) {
                    return removePosition($each->id);
                }
            } else {
                return "success";
            }
        } else {
            return "success";
        }
    }
}

function removeOrgPositionByuser($user_id = 0, $company_id = 0)
{
    if ($user_id != 0 && $company_id != 0) {
        $check = (new \App\Repositories\UserTree)->getOrgStructByCompany($user_id, $company_id);
        if ($check->count() > 0) {
            $check->delete();
            return "success";
        } else {
            return "error";
        }
    }
}

function getUserDetail($userId)
{
    $userDetail = (new \App\Repositories\Home)->getUserDetail($userId);
    return $userDetail;
}

function generateIntercomHMAC()
{
    return hash_hmac(
        'sha256', // hash function
        Auth::guard('customer')->user()->id, // user's id
        Config::get('settings.intercom_secret') // secret key (keep safe!)
    );
}
