<?php

namespace App\Http\Controllers;

use App;
use App\Models\Company;
use App\Models\PackageStoragePlan;
use App\Models\PackageUserPlan;
use App\Models\PackageMaster;
use App\Models\Users;
use App\Models\Resources;
use App\Models\ResourceDelegationActionLog;
use App\Models\ResourceWiseActivity;
use Illuminate\Http\Request;
use Mail;
use Config;
use App\Mail\TodoMailSend;

class ApiController extends Controller
{
    public function addCustomerAdmin(Request $req)
    {
        if (!empty($req->input())) {
            $companyModel = new Company;
            $userMasterModel = new Users;
            
            if ($req->input('currentLang') == 'en') {
                $currentLang = 'en';
            } elseif ($req->input('currentLang') == 'nl') {
                $currentLang = 'nl';
            } elseif ($req->input('currentLang') == 'fr') {
                $currentLang = 'fr';
            } else {
                $currentLang = 'nl';
            }
            session(['lang' => $currentLang]);
            session(['langName' => $currentLang]);
            App::setLocale(session('lang'));

            //Company Data
            $companyModel->name = stripslashes($req->input('company_name'));
            $company = Company::where('name', $req->input('company_name'))->count();
            if ($company > 0) {
                $response = array('status' => 0, 'message' => translate('error.company_already_exist'));
                return json_encode($response);
            } elseif (!filter_var($req->input('email'), FILTER_VALIDATE_EMAIL)) {
                $response = array('status' => 0, 'message' => translate('error.wrong_email_address'));
                return json_encode($response);
            }

            $companyModel->status = 1;
            $companyModel->is_trial = 1;
            $companyModel->user_allowed = Config::get('settings.trial_user_limit');
            $companyModel->storage_allowed = Config::get('settings.trial_storage_limit');
            //===================================//
            if ($companyModel->save()) {
                $inviteUsrToken = rand('1000', '9999').time();
                //User Master Data
                $userMasterModel->company_id = $companyModel->id;
                $userMasterModel->name = (stripslashes($req->input('fname')) . ' ' . stripslashes($req->input('lname')));
                $userMasterModel->email = stripslashes($req->input('email'));
                $userMasterModel->phone = stripslashes($req->input('phone'));
                $userMasterModel->type = 2;
                //Chekcing if user exists on audit execution
                $userMasterModel->invite_user_token = $inviteUsrToken;
                //Users model after save data
                $userMasterModel->language = $currentLang;
                $userMasterModel->fname = stripslashes($req->input('fname'));
                $userMasterModel->lname = stripslashes($req->input('lname'));
                $userMasterModel->can_process_owner = 1;
                $userMasterModel->is_auditor = 1;
                $userMasterModel->company_view = 1;
                //=======================//
                if ($userMasterModel->save()) {
                    //Zapier CRM code start
                    $params = array( 'CompanyName'=>stripslashes($req->input('company_name')), 'firstname'=>stripslashes($req->input('fname')), 'surname'=>stripslashes($req->input('lname')), 'email'=>stripslashes($req->input('email')), 'telephone'=>stripslashes($req->input('phone')));
                    $params = http_build_query($params);
                    $ch = curl_init();
                    $url = "https://hooks.zapier.com/hooks/catch/3867367/e67zw8/";
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, 1);                //0 for a get request
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $resp = curl_exec($ch);
                    curl_close($ch);
                    //Zapier CRM code end
                    $response = array('status' => 1, 'message' => 'Successfully registered, please check your email for further instructions');
                    return json_encode($response);
                }
            }
        }
    }

    public function PackageUserPlans()
    {
        $userPlans = PackageUserPlan::all()->toArray();
        $getBasePlanPrice = PackageMaster::basePrice()->first();
        foreach ($userPlans as $k=>$v) {
            $userPlans[$k]['price_per_month']  += $getBasePlanPrice;
        }
        if (!empty($userPlans)) {
            $response = array('status' => 1, 'message' => 'Data fetched successfully', 'data' => $userPlans);
        } else {
            $response = array('status' => 0, 'message' => 'Sorry no data found', 'data' => []);
        }
        return json_encode($response);
    }

    public function PackageStoragePlans()
    {
        $storagePlans = PackageStoragePlan::all()->toArray();
        $getBasePlanPrice = PackageMaster::basePrice()->first();
        foreach ($storagePlans as $k => $v) {
            $storagePlans[$k]['price_per_month'] += $getBasePlanPrice;
        }
        if (!empty($storagePlans)) {
            $response = array('status' => 1, 'message' => 'Data fetched successfully', 'data' => $storagePlans);
        } else {
            $response = array('status' => 0, 'message' => 'Sorry no data found', 'data' => []);
        }
        return json_encode($response);
    }

    public function resourceActionSheduleEntry()
    {
        $resourceAct = ResourceWiseActivity::all();
        foreach ($resourceAct as $each) {
            $nextDate = '';
            if ($each->delegations()->count() > 0) {
                $latestDeadline = date('Y-m-d', strtotime($each->delegations()->orderBy('id', 'DESC')->first()->deadline));
                $latestDelegateTo = $each->delegations()->latest()->first();
                if ($latestDeadline == date('Y-m-d')) {
                    switch ($each->periodicity) {
                        case '1':
                            $nextDate = date("Y-m-d", strtotime('+1 week'));
                            break;
                        case '2':
                            $nextDate = date("Y-m-d", strtotime('+1 month'));
                            break;
                        case '3':
                            $nextDate = date("Y-m-d", strtotime('+3 months'));
                            break;
                        case '4':
                            $nextDate = date("Y-m-d", strtotime('+6 months'));
                            break;
                        case '5':
                            $nextDate = date("Y-m-d", strtotime('+1 year'));
                            break;
                    }
                }
            }
            if ($nextDate !== '') {
                $log = new ResourceDelegationActionLog;
                $log->company_id = $latestDelegateTo->company_id;
                $log->resource_id = $latestDelegateTo->resource_id;
                $log->activity_id = $latestDelegateTo->activity_id;
                $log->delegate_to = $latestDelegateTo->delegate_to;
                $log->deadline = $nextDate;
                $log->save();
            }
        }
    }
}
