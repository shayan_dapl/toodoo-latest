<?php

namespace App\Http\Controllers\Audit;

use App;
use App\Http\Controllers\Controller;
use App\Models\ActionDelegateMember;
use App\Models\ActionMatrix;
use App\Models\AuditExecution;
use App\Models\IsoClause;
use App\Models\Process;
use App\Models\ReportedIssues;
use App\Models\ReportedIssuesDetails;
use App\Models\ReportIssueAction;
use App\Models\ReportingSourceUserAccess;
use App\Models\SourceTypes;
use App\Models\Users;
use App\Models\AuditDelegationDocs;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Session;
use View;
use Config;
use App\Repositories\AuditAction as ActionRepo;

class ActionController extends Controller
{
    protected $repo;

    public function __construct(ActionRepo $actionRepo)
    {
        $this->repo = $actionRepo;
    }

    public function reportIssueForm(Request $req)
    {
        $breadcrumbs = [
            '/audit/report-issue' => 'form.report_issue',
            '#' => 'word.form',
        ];
        $users = Users::companycheck()->inServiceWithEmail()->pluck('name', 'id')->prepend(translate('form.who'), '');
        $getsourceTypesAccess = ReportingSourceUserAccess::companycheck()->where('user_id', Auth::guard('customer')->user()->id)->pluck('source_type_id')->toArray();
        $sourceTypes = SourceTypes::active()->whereIn('id', $getsourceTypesAccess)->pluck('source_type', 'id')->prepend(translate('form.select'), '');
        $processes = Process::active()->pluck('name', 'id')->prepend(translate('words.process'), '');
        $langArr = Config::get('settings.lang');
        $isoNo = 'iso_no_' . $langArr[session('lang')];
        $isoRefs = IsoClause::pluck($isoNo, 'id')->prepend(translate('form.select'), '');
        $name = 'name_' . $langArr[session('lang')];
        $references = $this->repo->getReferences($name);
        $processId = !empty($req->process_id) ? $req->process_id : "";
        $yourReportedIssues = $this->repo->yourReportedIssues($getsourceTypesAccess);

        return View::make('pages.audit.report_issue_form', compact('breadcrumbs', 'sourceTypes', 'findingTypes', 'processes', 'processId', 'isoRefs', 'references', 'users', 'yourReportedIssues'));
    }

    public function reportIssueSave(Request $req)
    {
        if (!empty($req->input())) {
            $companyId = Auth::guard('customer')->user()->company_id;
            $issue = ReportedIssues::companyCheck()->where('issue_code', 'like', date('Ymd') . '%')->orderBy('id', 'DESC')->first();
            if (!isset($issue->exists)) {
                $issueCode = date('Ymd') . '-1';
            } else {
                $issueCode = str_replace(date('Ymd') . '-', '', $issue->issue_code);
                $issueCode = $issueCode + 1;
                $issueCode = date('Ymd') . '-' . $issueCode;
            }
            $model = new ReportedIssues;
            $model->issue_code = $issueCode;
            $model->company_id = $companyId;
            $model->source_type = $req->input('source_type');
            $model->reference = $req->input('reference');
            $model->status = 0;
            $model->created_by = Auth::guard('customer')->user()->id;
            $model->save();
            $report_id = $model->id;
            $detailsModel = new ReportedIssuesDetails;
            $detailsModel->reported_issue_id = $report_id;
            $detailsModel->issue = $req->input('issue');
            $detailsModel->action = $req->input('action');
            $detailsModel->who = $req->input('who');
            $detailsModel->deadline = $req->input('deadline');
            $detailsModel->related_process = $req->input('related_process');
            $detailsModel->status = 1;
            $detailsModel->created_by = Auth::guard('customer')->user()->id;
            $detailsModel->save();
            $req->session()->flash('success', translate('words.success_message'));
            //Holding previous source and reference
            $req->session()->flash('source', $req->input('source_type'));
            $req->session()->flash('reference', $req->input('reference'));
            return redirect('audit/report-issue');
        }
    }

    public function reportIssueFormEdit(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/audit/report-issue' => 'words.report_finding',
                '#' => 'View',
            ];
            $details = ReportedIssues::where('id', $req->id)->with('source_type')->with('finding_type')->with('process')->get()->toArray();
            return View::make('pages.audit.report_issue_view', compact('breadcrumbs', 'details'));
        }
    }

    public function reportIssueActionForm(Request $req)
    {
        $breadcrumbs = [
            '#' => 'form.report_delegation',
        ];
        $reported_issues = AuditExecution::with('iso', 'reference')->where('id', $req->issue_id)->with('docs')->first();
        $processOwnerDeatil = Users::find($reported_issues['process']['owner_id']);
        $processOwnerName = $processOwnerDeatil->name;
        $ref_id = $reported_issues['id'];
        $users = Users::where('company_id', $reported_issues['source_type']['company_id'])->where('no_email', 0)->get();
        $metrix_data = ActionMatrix::where('finding_type', $reported_issues['finding_type']['id'])->first();
        $reportedAction = ReportIssueAction::withRef($req->issue_id)->get()->pluck('id');
        $allActions = ActionDelegateMember::actionScope($reportedAction)->orderBy('issue_type')->get();
        $allPendingActions = ActionDelegateMember::actionScope($reportedAction)->where('status', 0)->orderBy('issue_type')->get();
        $immidiateActions = $rootCause = $correctiveMeasure = $effectiveness = [];
        $immidiateActionsPending = $rootCausePending = $correctiveMeasurePending = $effectivenessPending = [];
        foreach ($allActions as $eachAction) {
            if ($eachAction->issue_type == 'immidiate_action') {
                $immidiateActions[] = $eachAction;
            } elseif ($eachAction->issue_type == 'root_cause') {
                $rootCause[] = $eachAction;
            } elseif ($eachAction->issue_type == 'corrective_action') {
                $correctiveMeasure[] = $eachAction;
            } elseif ($eachAction->issue_type == 'immidiate_action') {
                $effectiveness[] = $eachAction;
            }
        }
        foreach ($allPendingActions as $eachPendingAction) {
            if ($eachPendingAction->issue_type == 'immidiate_action') {
                $immidiateActionsPending[] = $eachPendingAction;
            } elseif ($eachPendingAction->issue_type == 'root_cause') {
                $rootCausePending[] = $eachPendingAction;
            } elseif ($eachPendingAction->issue_type == 'corrective_action') {
                $correctiveMeasurePending[] = $eachPendingAction;
            } elseif ($eachPendingAction->issue_type == 'immidiate_action') {
                $effectivenessPending[] = $eachPendingAction;
            }
        }
        $metrixClass = ['', 'one', 'two', 'three', 'four'];
        $matrixData = $metrix_data->immidiate_actions + $metrix_data->root_cause_analysis + $metrix_data->corrective_measure + $metrix_data->effectiveness;
        $source_type = $req->issue_type;
        $metrixClass = $metrixClass[$matrixData];
        if (count($rootCausePending) == 0 && count($rootCause) > 0) {
            $define_corrective_link = '#define_corrective';
        } else {
            $define_corrective_link = '#';
        }

        if (count($correctiveMeasurePending) == 0 && count($correctiveMeasure) > 0) {
            $effectiveness_link = '#effectiveness';
        } else {
            $effectiveness_link = '#';
        }
        $immidiateActive = $rootcauseActive = $corrective_measureActive = $effectivenessActive = $immidiatAactiveCls = $rootCauseActiveCls = '';
        if (($metrix_data->immidiate_actions == 1) && ($metrix_data->root_cause_analysis == 1) && ($metrix_data->corrective_measure == 1) && ($metrix_data->effectiveness == 1)) {
            if (count($immidiateActionsPending) > 0) {
                $immidiateActive = 'active proactive';
                $immidiatAactiveCls = 'activecls';
            } elseif (count($rootCausePending) > 0) {
                $rootcauseActive = 'active proactive';
                $immidiateActive = 'proactive';
                $immidiatAactiveCls = '';
                $rootCauseActiveCls = 'activecls';
            } elseif (count($correctiveMeasurePending) > 0) {
                $corrective_measureActive = 'active proactive';
                $rootcauseActive = 'proactive';
                $immidiateActive = 'proactive';
            } elseif (count($effectivenessPending) > 0) {
                $effectivenessActive = 'active proactive';
                $corrective_measureActive = 'proactive';
                $rootcauseActive = 'proactive';
                $immidiateActive = 'proactive';
            } else {
                $immidiateActive = 'active proactive';
            }
        } elseif (($metrix_data->immidiate_actions == 0) && ($metrix_data->root_cause_analysis == 1) && ($metrix_data->corrective_measure == 1) && ($metrix_data->effectiveness == 1)) {
            if (count($rootCausePending) > 0) {
                $rootcauseActive = 'active proactive';
                $rootCauseActiveCls = 'activecls';
            } elseif (count($correctiveMeasurePending) > 0) {
                $corrective_measureActive = 'active proactive';
                $rootcauseActive = 'proactive';
            } elseif (count($effectivenessPending) > 0) {
                $effectivenessActive = 'active proactive';
                $corrective_measureActive = 'proactive';
                $rootcauseActive = 'proactive';
            } else {
                $rootcauseActive = 'active proactive';
            }
        } elseif (($metrix_data->immidiate_actions == 0) && ($metrix_data->root_cause_analysis == 0) && ($metrix_data->corrective_measure == 1) && ($metrix_data->effectiveness == 0)) {
            $corrective_measureActive = 'active proactive';
        }
        $viewpage = '';
        if ($req->is('audit/report-issue-action/*')) {
            $viewpage = 'pages.audit.report_issue_action_form';
        } elseif ($req->is('audit/action-by-member/*')) {
            $viewpage = 'pages.audit.action_by_member_form';
        } elseif ($req->is('audit/report-issue-view/*')) {
            $viewpage = 'pages.audit.report_issue_action_view';
        }
        return View::make($viewpage, compact('breadcrumbs', 'source_type', 'ref_id', 'users', 'metrix_data', 'reported_issues', 'immidiateActions', 'rootCause', 'correctiveMeasure', 'effectiveness', 'metrixClass', 'immidiateActionsPending', 'rootCausePending', 'correctiveMeasurePending', 'effectivenessPending', 'processOwnerName', 'define_corrective_link', 'effectiveness_link', 'immidiateActive', 'rootcauseActive', 'corrective_measureActive', 'effectivenessActive', 'immidiatAactiveCls', 'rootCauseActiveCls'));
    }

    public function issueActionSave(Request $req)
    {
        if (!empty($req->input())) {
            $issueActionModel = new ReportIssueAction;
            $dailySl = ReportIssueAction::where('created_at', 'like', (date('Y-m-d') . '%'))->count();
            $actionCode = date('Ymd') . '-' . ($dailySl + 1);
            $issueActionModel->ref_id = $req->input('ref_id');
            $issueActionModel->action_code = $actionCode;
            $issueActionModel->company_id = $req->input('company_id');
            $issueActionModel->info = $req->input('info');
            $issueActionModel->status = 0;
            $issueActionModel->created_by = Auth::guard('customer')->user()->id;
            $issueActionModel->updated_by = 0;
            if ($issueActionModel->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('audit/report-issue-action/' . $req->input('ref_id') . '/' . $req->input('issue_type'));
            }
        }
    }

    public function issueImmidiateActionSave(Request $req)
    {
        if (!empty($req->input())) {
            $issueActionModel = new ReportIssueAction;
            $issueExist = ReportIssueAction::withRef($req->input('ref_id'))->first();
            if (empty($issueExist)) {
                $dailySl = ReportIssueAction::where('created_at', 'like', (date('Y-m-d') . '%'))->count();
                $actionCode = date('Ymd') . '-' . ($dailySl + 1);
                $issueActionModel->ref_id = $req->input('ref_id');
                $issueActionModel->action_code = $actionCode;
                $issueActionModel->company_id = $req->input('company_id');
                $issueActionModel->status = 0;
                $issueActionModel->created_by = Auth::guard('customer')->user()->id;
                $issueActionModel->updated_by = 0;
                $issueActionModel->save();
                $action_id = $issueActionModel->id;
            } else {
                $action_id = $issueExist->id;
            }
            $memberModel = new ActionDelegateMember;
            $memberModel->action_id = $action_id;
            $memberModel->requested_action = $req->input('requested_action');
            if ($req->input('immidiate_actions') != '') {
                $memberModel->action_taken = $req->input('immidiate_actions');
                $memberModel->complete_date = date('Y-m-d');
                $memberModel->status = 1;
            } else {
                $memberModel->status = 0;
            }
            $memberModel->issue_type = 'immidiate_action';
            $memberModel->deadline = date('Y-m-d', strtotime($req->input('immidiate_actions_deadline')));
            $memberModel->member = $req->input('delegate_member_immidiate_actions');
            $memberModel->created_by = Auth::guard('customer')->user()->id;
            $memberModel->save();
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('audit/report-issue-action/' . $req->input('ref_id') . '/' . $req->input('issue_type'));
        }
    }

    public function issueRootCauseSave(Request $req)
    {
        if (!empty($req->input())) {
            $issueActionModel = new ReportIssueAction;
            $issueExist = ReportIssueAction::withRef($req->input('ref_id'))->first();
            if (empty($issueExist)) {
                $dailySl = ReportIssueAction::where('created_at', 'like', (date('Y-m-d') . '%'))->count();
                $actionCode = date('Ymd') . '-' . ($dailySl + 1);
                $issueActionModel->ref_id = $req->input('ref_id');
                $issueActionModel->action_code = $actionCode;
                $issueActionModel->company_id = $req->input('company_id');
                $issueActionModel->status = 0;
                $issueActionModel->created_by = Auth::guard('customer')->user()->id;
                $issueActionModel->updated_by = 0;
                $issueActionModel->save();
                $action_id = $issueActionModel->id;
            } else {
                $action_id = $issueExist->id;
            }
            $memberModel = new ActionDelegateMember;
            $memberModel->action_id = $action_id;
            $memberModel->requested_action = $req->input('requested_action');
            if ($req->input('root_cause_analysis') != '') {
                $memberModel->action_taken = $req->input('root_cause_analysis');
                $memberModel->complete_date = date('Y-m-d');
                $memberModel->status = 1;
            } else {
                $memberModel->status = 0;
            }
            $memberModel->issue_type = 'root_cause';
            $memberModel->deadline = date('Y-m-d', strtotime($req->input('root_cause_analysis_deadline')));
            $memberModel->member = $req->input('delegate_member_root_cause_analysis');
            $memberModel->created_by = Auth::guard('customer')->user()->id;
            $memberModel->updated_by = 0;
            $memberModel->save();
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('audit/report-issue-action/' . $req->input('ref_id') . '/' . $req->input('issue_type'));
        }
    }

    public function issueCorrectiveMeasureSave(Request $req)
    {
        if (!empty($req->input())) {
            $issueActionModel = new ReportIssueAction;
            $issueExist = ReportIssueAction::withRef($req->input('ref_id'))->first();
            if (empty($issueExist)) {
                $dailySl = ReportIssueAction::where('created_at', 'like', (date('Y-m-d') . '%'))->count();
                $actionCode = date('Ymd') . '-' . ($dailySl + 1);
                $issueActionModel->ref_id = $req->input('ref_id');
                $issueActionModel->action_code = $actionCode;
                $issueActionModel->company_id = $req->input('company_id');
                $issueActionModel->status = 0;
                $issueActionModel->created_by = Auth::guard('customer')->user()->id;
                $issueActionModel->updated_by = 0;
                $issueActionModel->save();
                $action_id = $issueActionModel->id;
            } else {
                $action_id = $issueExist->id;
            }
            $memberModel = new ActionDelegateMember;
            $memberModel->action_id = $action_id;
            $memberModel->requested_action = $req->input('requested_action');
            if ($req->input('define_corrective') != '') {
                $memberModel->action_taken = $req->input('define_corrective');
                $memberModel->complete_date = date('Y-m-d');
                $memberModel->status = 1;
            } else {
                $memberModel->status = 0;
            }
            $memberModel->issue_type = 'corrective_action';
            $memberModel->deadline = date('Y-m-d', strtotime($req->input('define_corrective_deadline')));
            $memberModel->member = $req->input('delegate_member_define_corrective');
            $memberModel->save();
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('audit/report-issue-action/' . $req->input('ref_id') . '/' . $req->input('issue_type'));
        }
    }

    public function issueEffectivenessSave(Request $req)
    {
        if (!empty($req->input())) {
            $issueActionModel = new ReportIssueAction;
            $issueExist = ReportIssueAction::withRef($req->input('ref_id'))->first();
            if (empty($issueExist)) {
                $dailySl = ReportIssueAction::where('created_at', 'like', (date('Y-m-d') . '%'))->count();
                $actionCode = date('Ymd') . '-' . ($dailySl + 1);
                $issueActionModel->ref_id = $req->input('ref_id');
                $issueActionModel->action_code = $actionCode;
                $issueActionModel->company_id = $req->input('company_id');
                $issueActionModel->status = 0;
                $issueActionModel->created_by = Auth::guard('customer')->user()->id;
                $issueActionModel->updated_by = 0;
                $issueActionModel->save();
                $action_id = $issueActionModel->id;
            } else {
                $action_id = $issueExist->id;
            }
            $memberModel = new ActionDelegateMember;
            $memberModel->action_id = $action_id;
            $memberModel->requested_action = $req->input('requested_action');
            if ($req->input('effectiveness') != '') {
                $memberModel->action_taken = $req->input('effectiveness');
                $memberModel->complete_date = date('Y-m-d');
                $memberModel->status = 1;
            } else {
                $memberModel->status = 0;
            }
            $memberModel->issue_type = 'effectiveness';
            $memberModel->deadline = date('Y-m-d', strtotime($req->input('effectiveness_deadline')));
            $memberModel->member = $req->input('delegate_member_effectiveness');
            $memberModel->save();
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('audit/report-issue-action/' . $req->input('ref_id') . '/' . $req->input('issue_type'));
        }
    }

    public function actionByMemberSave(Request $req)
    {
        if (!empty($req->input())) {
            ActionDelegateMember::where('id', $req->input('delegate_id'))->update(['action_taken' => $req->input('member_feedback'), 'status' => '1', 'complete_date' => date('Y-m-d')]);
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/audit/action-by-member/' . $req->input('ref_id'));
        }
    }

    public function actionByMemberdocForm(Request $req)
    {
        $hid = $req->delegationId;
        $data = AuditDelegationDocs::where('delegation_id', $hid)->get();
        return View::make('pages.audit.delegation_document_form', compact('hid', 'data'));
    }

    public function actionByMemberdocSave(Request $req)
    {
        if (!empty($req->input('file'))) {
            header('Content-Type: ' . $req->input('fileextension'));
            $str = "data:" . $req->input('fileextension') . ";base64,";
            $data = str_replace($str, "", $req->input('file'));
            $data = base64_decode($data);
            $filename = $req->input('filename');
            file_put_contents(storage_path('app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents/delegation_docs') . '/' . $filename, $data);
            $delegationDocModel = new AuditDelegationDocs;
            $delegationDocModel->delegation_id = $req->input('hid');
            $delegationDocModel->doc_name = $filename;
            $delegationDocModel->status = 1;
            $delegationDocModel->created_by = Auth::guard('customer')->user()->id;
            $delegationDocModel->updated_by = 0;
            $delegationDocModel->save();
            session(['details' => 'open']);
            return $req->input('hid');
        } else {
            return "Failed";
        }
    }
}
