<?php

namespace App\Http\Controllers\Audit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ActionMatrix;
use App\Models\FindingTypes;
use Auth;
use View;

class ActionMatrixController extends Controller
{
    public function actionMatrix()
    {
        $breadcrumbs = [
            '/audit/finding-type-list' => 'words.action_matrix',
        ];
        $finding_types = FindingTypes::leftJoin('audit_action_matrix', function ($join) {
            $join->on('finding_type.id', '=', 'audit_action_matrix.finding_type');
        })
            ->select(
                'finding_type.id as id',
                'finding_type.finding_type as finding_type',
                'audit_action_matrix.immidiate_actions as immidiate_actions',
                'audit_action_matrix.root_cause_analysis as root_cause_analysis',
                'audit_action_matrix.corrective_measure as corrective_measure',
                'audit_action_matrix.effectiveness as effectiveness'
            )
            ->where('finding_type.company_id', '=', Auth::guard('customer')->user()->company_id)
            ->get();

        return View::make('pages.audit.audit_action_matrix', compact('finding_types', 'breadcrumbs'));
    }

    public function actionMatrixSave(Request $req)
    {
        $totalRows = $req->input('num_rows');

        for ($i = 0; $i < $totalRows; $i++) {
            $matrix = ActionMatrix::where('finding_type', $req->input('finding_type_id')[$i])->get();

            if (!empty($req->input('finding_type_id')[$i]) && !empty($matrix[0])) {
                $model = ActionMatrix::find($matrix[0]->id);
                $model->updated_by = Auth::guard('customer')->user()->id;
            } else {
                $model = new ActionMatrix;
            }

            $model->finding_type = $req->input('finding_type_id')[$i];

            if (isset($req->input('immidiate_actions')[$i])) {
                $model->immidiate_actions = $req->input('immidiate_actions')[$i][0];
            } else {
                $model->immidiate_actions = 0;
            }

            if (isset($req->input('root_cause_analysis')[$i])) {
                $model->root_cause_analysis = $req->input('root_cause_analysis')[$i][0];
            } else {
                $model->root_cause_analysis = 0;
            }

            if (isset($req->input('corrective_measure')[$i])) {
                $model->corrective_measure = $req->input('corrective_measure')[$i][0];
            } else {
                $model->corrective_measure = 0;
            }

            if (isset($req->input('effectiveness')[$i])) {
                $model->effectiveness = $req->input('effectiveness')[$i][0];
            } else {
                $model->effectiveness = 0;
            }

            $model->created_by = Auth::guard('customer')->user()->id;
            $model->save();
        }

        $req->session()->flash('success', translate('words.success_message'));
        return redirect('/audit/action-matrix/');
    }
}
