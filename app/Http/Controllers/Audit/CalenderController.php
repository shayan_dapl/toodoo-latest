<?php

namespace App\Http\Controllers\Audit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use View;
use Auth;
use Config;
use App\Repositories\AuditPlanning as PlanningRepo;

class CalenderController extends Controller
{
    protected $repo;

    public function __construct(PlanningRepo $planningRepo)
    {
        $this->repo = $planningRepo;
    }
    public function planningList(Request $req)
    {
        $breadcrumbs = [
            '/audit/planning-list' => 'words.audit_plan',
            '#' => 'words.list',
        ];
        if ($req->year != '') {
            $curYear = $req->year;
            $selectedYear = $req->year;
            $printlink = '/'.$req->year;
        } else {
            $curYear = date('Y');
            $selectedYear = '';
            $printlink = '';
        }
        $branchId = ($req->branchid != 'all') ? $req->branchid : '';
        $months = [translate('words.january'), translate('words.february'), translate('words.march'), translate('words.april'), translate('words.may'), translate('words.june'), translate('words.july'), translate('words.august'), translate('words.september'), translate('words.october'), translate('words.november'), translate('words.december')];
        $plannedAudits = $this->repo->getPlannedAudits('internal', $req->branchid);
        $plannedExternalAudits = $this->repo->getPlannedAudits('external', $req->branchid);
        $uniqueProcess = [];
        foreach ($plannedAudits as $eachprocess => $eachData) {
            $uniqueProcess[$eachData['name']][] = $eachData;
        }
        $plannedAudit = auditCalender($uniqueProcess, $curYear, 'internal', $branchId);

        /////External Audit

        $uniqueexternalAuditProcess = [];
        foreach ($plannedExternalAudits as $eachprocess => $eachData) {
            $uniqueexternalAuditProcess[$eachData['name']][] = $eachData;
        }
        $plannedExternalAudit = auditCalender($uniqueexternalAuditProcess, $curYear, 'external', $branchId);
        $prvYear = $curYear - 1;
        $nxtYear = $curYear + 1;
        $selectBranches = Branch::companyStatusCheck();
        $branches = $selectBranches->pluck('name', 'id');
        if (isset($req->branchid) && ($selectBranches->count() > 1)) {
            $selectedBranch = $req->branchid;
            $branches = $branches->prepend(translate('words.all'), 'all');
        } elseif ($selectBranches->count() == 1) {
            $selectedBranch = $selectBranches->pluck('id')[0];
        }
        return View::make('pages.audit.audit_list', compact('breadcrumbs', 'months', 'plannedAudit', 'plannedExternalAudit', 'curYear', 'prvYear', 'nxtYear', 'branches', 'selectedYear', 'selectedBranch', 'printlink'));
    }
    public function auditCalPrint(Request $req)
    {
        if ($req->year != '') {
            $curYear = $req->year;
            $selectedYear = $req->year;
        } else {
            $curYear = date('Y');
            $selectedYear = '';
        }
        $branchId = ($req->branchid != 'all') ? $req->branchid : '';
        $auditType = $req->type;
        $months = [translate('words.january'), translate('words.february'), translate('words.march'), translate('words.april'), translate('words.may'), translate('words.june'), translate('words.july'), translate('words.august'), translate('words.september'), translate('words.october'), translate('words.november'), translate('words.december')];
        $plannedAudits = $this->repo->getPlannedAudits($auditType, $req->branchid);
        $uniqueProcess = [];
        foreach ($plannedAudits as $eachprocess => $eachData) {
            $uniqueProcess[$eachData['name']][] = $eachData;
        }
        $plannedAudit = auditCalenderPrint($uniqueProcess, $curYear, $auditType, $branchId);

        $prvYear = $curYear - 1;
        $nxtYear = $curYear + 1;
        $selectBranches = Branch::companyStatusCheck();
        $branches = $selectBranches->pluck('name', 'id');
        if (isset($req->branchid) && ($selectBranches->count() > 1)) {
            $selectedBranch = $req->branchid;
            $branches = $branches->prepend(translate('words.all'), 'all');
        } elseif ($selectBranches->count() == 1) {
            $selectedBranch = $selectBranches->pluck('id')[0];
        }
        $pdf = app('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->setPaper('A4', 'landscape');
        $pdf->loadView('pages.audit.audit_calender_print', compact('months', 'plannedAudit', 'curYear', 'prvYear', 'nxtYear', 'branches', 'selectedYear', 'selectedBranch', 'auditType'));
        return $pdf->download(str_replace(' ', '-', 'Audit-print').'.pdf');
    }
}
