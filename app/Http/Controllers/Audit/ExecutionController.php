<?php

namespace App\Http\Controllers\Audit;

use App;
use App\Http\Controllers\Controller;
use App\Models\AuditExecution;
use App\Models\AuditExecutionDocs;
use App\Models\AuditPlanMaster;
use App\Models\AuditTypeMaster;
use App\Models\FindingTypes;
use App\Models\IsoClause;
use App\Models\Users;
use App\Models\Process;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;
use Config;

class ExecutionController extends Controller
{
    public function auditExecuteForm(Request $req)
    {
        $breadcrumbs = [
            '#' => 'words.audit_execute',
        ];
        $audit_detail = AuditPlanMaster::checkAuditId($req->audit_id)->with('lead_auditor', 'audit_type', 'process', 'auditors')->first();
        if (!checkCompanyAccess($audit_detail)) {
            $req->session()->flash('error', translate('words.you_dont_have_access'));
            return redirect('/home');
        }
        $auditees = Users::companycheck()->where('out_of_service', null)->where('id', '<>', $audit_detail->choose_lead_auditor)->pluck('name', 'id')->toArray();

        $auditees_selected = (new \App\Repositories\AuditExecution)->getSelectedAuditee($req->audit_id);
        $langArr = Config::get('settings.lang');

        $references = auditTypesList($audit_detail['audit_type']['id']);

        $findingType = FindingTypes::companyCheck()
            ->pluck('finding_type', 'id')
            ->prepend(translate('form.finding_type'), '');

        $execution_details = AuditExecution::checkAuditId($req->audit_id)->orderBy('id', 'DESC')->get();
        $iso_no = 'iso_no_' . $langArr[session('lang')];
        $iso_refs = IsoClause::pluck($iso_no, 'id');

        $execution_count = $execution_details->count();

        $porcessData = Process::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select_process'), '');
        $selectedProcess = $audit_detail->process[0]->process_id;
        return View::make('pages.audit.audit_execute_form', compact('breadcrumbs', 'audit_detail', 'auditees', 'auditees_selected', 'references', 'findingType', 'execution_details', 'iso_refs', 'execution_count', 'porcessData', 'selectedProcess'));
    }

    public function execAuditType(Request $req)
    {
        $options = auditTypesList($req->ref_id);
        $topTypes = AuditTypeMaster::where('parent_id', 0)->where('iso_ref_id', $req->ref_id)->pluck('id')->toArray();
        $type = $req->type;
        return View::make('pages.helper.audit_type_dropdown', compact('options', 'topTypes', 'type'));
    }

    public function auditExecuteGeneralFormSave(Request $req)
    {
        if (!empty($req->input('hid'))) {
            $auditPrepModel = AuditPlanMaster::find($req->input('hid'));
            $auditPrepModel->visited_location = $req->input('visited_location');
            $auditPrepModel->updated_by = Auth::guard('customer')->user()->id;
            $auditPrepModel->save();

            $auditees = $req->input('auditee');
            (new \App\Repositories\AuditExecution)->removeOldAuditee($req->input('hid'));
            if (!empty($auditees)) {
                foreach ($auditees as $m => $auditee) {
                    $plan_auditees = array(
                        'audit_id' => $req->input('hid'),
                        'user_id' => $auditee,
                        'status' => 1,
                    );
                    (new \App\Repositories\AuditExecution)->insertNewAuditee($plan_auditees);
                }
            }

            $req->session()->forget('details');
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/audit/audit-execute/' . $req->input('hid'));
        }
    }

    public function auditExecuteDetailFormSave(Request $req)
    {
        $totalRows = count($req->input('observation'));
        for ($i = 0; $i < $totalRows; $i++) {
            if (!empty($req->input('observation')[$i]) && !empty($req->input('finding_type')[$i]) && !empty($req->input('references')[$i]) && !empty($req->input('process')[$i])) {
                if (!empty($req->input('id')[$i])) {
                    $model = AuditExecution::find($req->input('id')[$i]);
                    $model->updated_by = Auth::guard('customer')->user()->id;
                } else {
                    $model = new AuditExecution;
                }
                $model->audit_id = $req->input('audit_id');
                $model->process_id = $req->input('process')[$i];
                $model->observation = $req->input('observation')[$i];
                $model->iso_no = $req->input('iso')[$i];
                $model->finding_type_id = $req->input('finding_type')[$i];
                $model->reference_id = $req->input('references')[$i];
                $model->evidence = $req->input('evidence')[$i];
                $model->created_by = Auth::guard('customer')->user()->id;
                $model->save();
            }
        }
        $auditPrepModel = AuditPlanMaster::find($req->input('audit_id'));
        $auditPrepModel->status = 3;
        $auditPrepModel->save();
        session(['details' => 'open']);
        $req->session()->flash('success', translate('words.success_message'));
        return redirect('/audit/audit-execute/' . $req->input('audit_id'));
    }

    public function detailsRowSave(Request $req)
    {
        $model = AuditExecution::findOrNew($req->input('id'));
        $model->audit_id = $req->input('audit_id');
        $model->process_id = $req->input('process');
        $model->observation = $req->input('observation');
        $model->iso_no = $req->input('iso');
        $model->finding_type_id = $req->input('findingtype');
        $model->reference_id = $req->input('references');
        $model->evidence = $req->input('evidence');
        $model->created_by = Auth::guard('customer')->user()->id;
        if ($model->save()) {
            return $model->id;
        }
    }

    public function auditExecuteDetailsRemove(Request $req)
    {
        if (!empty($req->id)) {
            $details = AuditExecution::checkExecutionId($req->id);
            $auditId = $details->first()->audit_id;
            $details->delete();
            $req->session()->flash('success', translate('words.success_message'));
            $req->session()->flash('details', 'open');
            return redirect('/audit/audit-execute/' . $auditId);
        }
    }

    public function detailsDocumentForm(Request $req)
    {
        if (!empty($req->execution_id) && $req->execution_id !== 0) {
            $data = AuditExecutionDocs::checkExecutionId($req->execution_id)->get();
            $evidence = ($req->evidence) ? $req->evidence : '';
            $hid = $req->execution_id;
            $row = '';
            return View::make('pages.audit.exec_document_form', compact('data', 'hid', 'row', 'evidence'));
        }
        if (!empty($req->newrow) && $req->newrow !== 0) {
            $data = [];
            $evidence = ($req->evidence) ? $req->evidence : '';
            $hid = '';
            $row = $req->newrow;
            return View::make('pages.audit.exec_document_form', compact('data', 'hid', 'row', 'evidence'));
        }
    }

    public function detailsDocuments(Request $req)
    {
        if (!empty($req->execution_id) && $req->execution_id !== 0) {
            $data = AuditExecutionDocs::checkExecutionId($req->execution_id)->get();
            $hid = $req->execution_id;
            $row = '';
            $noShowForm = "1";
            return View::make('pages.audit.exec_document_form', compact('data', 'hid', 'row', 'noShowForm'));
        }
    }

    public function detailsDocumentSave(Request $req)
    {
        if (!empty($req->input('file'))) {
            header('Content-Type: ' . $req->input('fileextension'));
            $str = "data:" . $req->input('fileextension') . ";base64,";
            $data = str_replace($str, "", $req->input('file'));
            $data = base64_decode($data);
            $filename = $req->input('filename');
            file_put_contents(storage_path('app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents/execution_docs') . '/' . $filename, $data);
            if ($req->input('newdata')) {
                $model = new AuditExecution;
                $model->audit_id = $req->input('audit_id');
                $model->process_id = $req->input('process');
                $model->observation = $req->input('observation');
                $model->iso_no = $req->input('iso');
                $model->finding_type_id = $req->input('findingtype');
                $model->reference_id = $req->input('references');
                $model->evidence = $req->input('evidence');
                $model->created_by = Auth::guard('customer')->user()->id;
                $model->save();
                $executionId = $model->id;
            } else {
                $executionId = $req->input('executionid');
                AuditExecution::checkExecutionId($executionId)->update(['evidence' => $req->input('evidence')]);
            }
            $executionDocModel = new AuditExecutionDocs;
            $executionDocModel->execution_id = $executionId;
            $executionDocModel->doc_name = $filename;
            $executionDocModel->status = 1;
            $executionDocModel->created_by = Auth::guard('customer')->user()->id;
            $executionDocModel->updated_by = 0;
            $executionDocModel->save();
            session(['details' => 'open']);
            return $executionId;
        } else {
            return "Failed";
        }
    }

    public function executePreview(Request $req)
    {
        if (!empty($req->audit_id)) {
            $findings = FindingTypes::companyCheck()->get();
            $audit = $req->audit_id;
            return View::make('pages.audit.exec_preview', compact('findings', 'audit'));
        } else {
            return response()->json(false);
        }
    }

    public function auditRelease(Request $req)
    {
        if (!empty($req->input('audit_id'))) {
            $executionData = AuditExecution::checkAuditId($req->input('audit_id'))->count();
            if ($executionData > 0) {
                if ($req->input('release_date')) {
                    $releaseMonth = date('m', strtotime($req->input('release_date')));
                    $releaseYear = date('Y', strtotime($req->input('release_date')));
                    $releaseDate = date(Config::get('settings.db_date'), strtotime($req->input('release_date')));
                } else {
                    $releaseMonth = date('m');
                    $releaseYear = date('Y');
                    $releaseDate = date(Config::get('settings.db_date'));
                }
                AuditPlanMaster::checkAuditId($req->input('audit_id'))->update(['status' => 2, 'release_month' => $releaseMonth, 'release_year' => $releaseYear,'release_date' => $releaseDate]);
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/home');
            } else {
                $req->session()->flash('error', translate('alert.no_execution_data'));
                return redirect(\URL::previous());
            }
        } else {
            return redirect(\URL::previous());
        }
    }

    public function auditRepotPrint(Request $req)
    {
        $auditData = AuditPlanMaster::find($req->audit_id);
        $auditedProcess = $auditFindings = $auditFindingsId = $reference = $executionDocs = $executedClause = $executedIsos = [];
        foreach ($auditData->executions as $eachData) {
            $auditedProcess [] = $eachData->process->name;
            $auditFindings [] = $eachData->findingtypes->finding_type;
            $auditFindingsId [] = $eachData->findingtypes->id;
            $reference [] = $eachData->iso->iso_no_en;
            $executedClause [] = $eachData->reference_id;
            foreach ($eachData->docs as $eachDoc) {
                $executionDocs [] = $eachDoc->doc_name;
            }
            $executedIsos [$eachData->iso->iso_no_en] = $eachData->iso->clauses;
        }
        $auditedProcess = array_unique($auditedProcess);
        $reference = array_unique($reference);
        $executionDocs = array_unique($executionDocs);
        $auditFindings = array_count_values($auditFindings);
        $auditFindingsId = array_count_values($auditFindingsId);
        $findings = FindingTypes::companyCheck()->get();
        $totalfinding = [];
        $allFindings = [];
        foreach ($findings as $eachFinding) {
            if (strpos($eachFinding->finding_type, '(') !== false) {
                $allFindings [$eachFinding->finding_type] = strstr($eachFinding->finding_type, "(", true);
            } else {
                $allFindings [$eachFinding->finding_type] = $eachFinding->finding_type;
            }
            if (array_key_exists($eachFinding->id, $auditFindingsId)) {
                $totalfinding [$eachFinding->id] = $auditFindingsId[$eachFinding->id];
            } else {
                $totalfinding [$eachFinding->id] = 0;
            }
        }
        $getAllClauses = $auditData->audit_type->clauses;
        $pdf = app('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView('pages.audit.report_print', compact('auditData', 'auditedProcess', 'auditFindings', 'reference', 'executionDocs', 'findings', 'totalfinding', 'getAllClauses', 'executedClause', 'allFindings', 'executedIsos'));
        return $pdf->download(str_replace(' ', '-', $auditData->audit_name).'.pdf');
    }
}
