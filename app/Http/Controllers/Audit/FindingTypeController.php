<?php

namespace App\Http\Controllers\Audit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FindingTypes;
use App\Models\AuditExecution;
use Auth;
use View;
use App\Http\Requests\FindingTypeRequest;

class FindingTypeController extends Controller
{
    public function list()
    {
        $breadcrumbs = [
            '#' => 'words.finding_type'
        ];
        return View::make('pages.audit.finding_type_list', compact('breadcrumbs'));
    }

    public function listData()
    {
        $data = FindingTypes::companyStatusCheck()->get();
        return response()->json($data);
    }

    public function save(FindingTypeRequest $req)
    {
        if (!empty($req->input())) {
            $model = FindingTypes::findOrNew($req->input('id'));
            $model->company_id = Auth::guard('customer')->user()->company_id;
            $model->finding_type = $req->input('finding_type');
            $model->status = 1;
            if ($model->save()) {
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }

    public function remove(Request $req)
    {
        if (!empty($req->id)) {
            $auditExist = AuditExecution::where('finding_type_id', $req->id)->count();
            if ($auditExist == 0) {
                FindingTypes::where('id', $req->id)->update(['status' => 3]);
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }
}
