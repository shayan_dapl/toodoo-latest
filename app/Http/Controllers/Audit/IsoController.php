<?php

namespace App\Http\Controllers\Audit;

use App;
use App\Http\Controllers\Controller;
use App\Models\AuditTypeMaster;
use App\Models\IsoClause;
use Illuminate\Http\Request;
use App\Http\Requests\IsoRequest;
use Session;
use View;

class IsoController extends Controller
{
    public function auditIsoList()
    {
        $breadcrumbs = [
            '/audit/iso/list' => 'words.audit',
            '#' => 'words.standerd_iso_clause',
        ];
        $details = IsoClause::all();

        return View::make('pages.audit.iso_clause_list', compact('breadcrumbs', 'details'));
    }

    public function auditIsoForm()
    {
        $breadcrumbs = [
            '/audit/iso/list' => 'words.standerd_iso_clause',
            '#' => 'words.add',
        ];
        return View::make('pages.audit.iso_clause_form', compact('breadcrumbs'));
    }

    public function auditIsoFormSave(IsoRequest $req)
    {
        if (!empty($req->input())) {
            $model = IsoClause::findOrNew($req->input('hid'));
            $model->iso_no_nl = $req->input('iso_no_nl');
            $model->iso_no_en = $req->input('iso_no_en');
            $model->iso_no_fr = $req->input('iso_no_fr');
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('audit/iso/list');
            }
        }
    }

    public function auditIsoFormEdit(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/audit/iso/ist' => 'words.standerd_iso_clause',
                '#' => 'words.edit',
            ];
            $details = IsoClause::find($req->id);

            return View::make('pages.audit.iso_clause_form', compact('breadcrumbs', 'details'));
        }
    }

    public function auditIsoDelete(Request $req)
    {
        if (!empty($req->id)) {
            $child = AuditTypeMaster::where('iso_ref_id', $req->id)->count();
            if ($child == 0) {
                IsoClause::destroy($req->id);
                $req->session()->flash('success', translate('words.success_message'));
            } else {
                $req->session()->flash('error', translate('words.child_exist_can_not_delete'));
            }
            return redirect('/audit/iso/list');
        }
    }
}
