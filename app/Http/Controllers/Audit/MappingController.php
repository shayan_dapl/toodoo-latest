<?php

namespace App\Http\Controllers\Audit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\AuditPlanMaster;
use App\Models\AuditTypeMaster;
use App\Models\Process;
use App\Models\PackageMaster;
use App\Models\IsoClause;
use View;
use Auth;
use Config;
use App\Repositories\AuditPlanning as PlanningRepo;

class MappingController extends Controller
{
    protected $repo;

    public function __construct(PlanningRepo $planningRepo)
    {
        $this->repo = $planningRepo;
    }
    public function auditMapping(Request $req)
    {
        $breadcrumbs = [
            '#' => 'words.audit_mapping',
        ];
        if ($req->year != '') {
            $curYear = $req->year;
            $selectedYear = $req->year;
            $printlink = '/'.$req->year;
        } else {
            $curYear = date('Y');
            $selectedYear = '';
            $printlink = '';
        }
        $selectBranches = Branch::companyStatusCheck();
        $branches = $selectBranches->pluck('name', 'id')->prepend(translate('words.select_branch'), 'all');
        $selectProcess = Process::companyStatusCheck();
        $process = $selectProcess->pluck('name', 'id')->prepend(translate('words.select_process'), 'all');
        $name = 'iso_no_'.session('lang');
        $selectIso = IsoClause::pluck($name, 'id');

        $selectedBranch = $req->branchid;
        $selectedProcess = $req->process;
        $selectedDate = $req->releaseDate;
        $selectedPackage = ($req->package != '') ? $req->package : 3;
        $selectedYear = ($req->year != '') ? $req->year : date('Y');

        $getAllClause = AuditTypeMaster::where('iso_ref_id', $selectedPackage)->get();
        $allAudits = AuditPlanMaster::companyCheck()->where('status', 2)->where('release_year', $selectedYear);
        if ($selectedBranch != 'all' && $selectedBranch != '') {
            $allAudits = $allAudits->whereHas('branch', function ($q) use ($selectedBranch) {
                $q->where('branch_id', $selectedBranch);
            });
        }
        if ($selectedProcess != 'all' && $selectedProcess != '') {
            $allAudits = $allAudits->whereHas('executions', function ($q) use ($selectedProcess) {
                $q->where('process_id', $selectedProcess);
            });
        }
        if ($selectedDate != '') {
            $filterDate = date('Y-m-d', strtotime($selectedDate));
            $allAudits = $allAudits->where('release_date', $filterDate);
        }
        if ($selectedPackage != '') {
            $allAudits = $allAudits->where('type_of_audit', $selectedPackage);
        }
        $allAudits = $allAudits->orderBy('release_date', 'asc')->get();
        $mapArr = [];
        $releaseDateArr = [];
        if ($allAudits->count() > 0) {
            foreach ($allAudits as $eachAudit) {
                $releaseDateArr [] = $eachAudit->release_date;
                foreach ($getAllClause as $eachClause) {
                    if ($eachAudit->executions()->count() > 0) {
                        foreach ($eachAudit->executions as $eachExecution) {
                            $mapArr[$eachClause->clause]['name'] = $eachClause->name_nl;
                            if ($eachExecution->reference_id == $eachClause->id) {
                                $mapArr[$eachClause->clause]['content'][$eachAudit->release_date] = 'X';
                            } else {
                                $mapArr[$eachClause->clause]['content'][$eachAudit->release_date] = '';
                            }
                        }
                    }
                }
            }
        }
        $releaseDateArr = array_unique($releaseDateArr);
        return View::make('pages.audit.audit_mapping', compact('breadcrumbs', 'mapArr', 'releaseDateArr', 'curYear', 'branches', 'selectedBranch', 'selectedProcess', 'selectedDate', 'selectedPackage', 'selectedYear', 'process', 'selectIso'));
    }
}
