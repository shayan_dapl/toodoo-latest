<?php

namespace App\Http\Controllers\Audit;

use App;
use App\Models\AuditExecution;
use App\Models\AuditPlanAuditors;
use App\Models\AuditPlanMaster;
use App\Models\IsoClause;
use App\Models\Process;
use App\Models\Users;
use App\Models\ProcessToAudit;
use App\Models\Branch;
use App\Models\ProcessBranch;
use App\Models\PackageMaster;
use App\Models\AuditBranch;
use App\Http\Requests\AuditPlanRequest;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use App\Mail\LeadAuditorChosen;
use Session;
use View;
use Config;

class PlanningController extends Controller
{
    public function auditPlanningForm(Request $req)
    {
        $breadcrumbs = [
            '#' => 'words.audit_planning',
        ];
        if ($req->id) {
            $selected_process = $req->id;
        } else {
            $selected_process = '';
        }
        if ($req->month) {
            $selected_month = $req->month;
        } else {
            $selected_month = '';
        }
        if ($req->year) {
            $selected_year = $req->year;
        } else {
            $selected_year = '';
        }
        $selectedBranchId = isset($req->branchId) ? $req->branchId : '';
        $processDetail = Process::find($selected_process);
        if (!checkCompanyAccess($processDetail)) {
            $req->session()->flash('error', translate('words.you_dont_have_access'));
            return redirect('/home');
        }
        $selected_process_owner = $processDetail->owner_id;
        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        if ($req->type == 'internal') {
            $auditors = Users::companyCheck()->where('is_auditor', 1)->where('out_of_service', null)
                ->where('id', '<>', $selected_process_owner)->pluck('name', 'id')->prepend(translate('form.select'), '');
        } else {
            $auditors = Users::companyCheck()->where('is_auditor', 1)->where('out_of_service', null)->pluck('name', 'id')->prepend(translate('form.select'), '');
        }
        if ($selectedBranchId != '') {
            $porcess = ProcessBranch::with('process')->where('branch_id', $selectedBranchId)->get()->pluck('process.name', 'process.id')->prepend(translate('form.select'), '');
        } else {
            $porcess = Process::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        }
        
        $branches = Branch::companyStatusCheck()->pluck('name', 'id');
        $langArr = Config::get('settings.lang');
        $iso_no = 'iso_no_' . $langArr[session('lang')];
        $audit_type = IsoClause::pluck($iso_no, 'id')->prepend(translate('form.select'), '');
        $getBasePackage = PackageMaster::where('is_base', 1)->pluck('iso_id');
        $getBasePackageIso = $getBasePackage[0];
        if ($selected_process != '') {
            $selected_process_title = Process::where('id', $selected_process)->pluck('name');
        } else {
            $selected_process_title = Process::companyStatusCheck()->pluck('name');
        }
        $selected_process_title = $selected_process_title[0];
        if (isset($selected_month)) {
            $monthyear = $selected_month . '/' . $selected_year;
        }
        $audit_category = array('internal' => translate('words.internal'), 'external' => translate('words.external'));
        $currentAuditcat = $req->type;
        return View::make('pages.audit.planning_form', compact('breadcrumbs', 'months', 'auditors', 'porcess', 'branches', 'selected_process', 'selected_process_title', 'selected_month', 'selected_year', 'audit_type', 'monthyear', 'audit_category', 'currentAuditcat', 'getBasePackageIso', 'selectedBranchId'));
    }

    public function auditPlanningSave(AuditPlanRequest $req)
    {
        if (!empty($req->input())) {
            $monthYear = explode('/', $req->input('choose_audit_month'));
            $month = $monthYear[0];
            $year = $monthYear[1];
            $auditPlanModel = new AuditPlanMaster;
            $auditPlanModel->company_id = Auth::guard('customer')->user()->company_id;
            $auditPlanModel->audit_name = $req->input('audit_title');
            $auditPlanModel->category = !empty($req->input('audit_category')) ? $req->input('audit_category') : '';
            $auditPlanModel->audit_company = !empty($req->input('audit_company')) ? $req->input('audit_company') : '';
            $auditPlanModel->plan_month = $month;
            $auditPlanModel->plan_year = $year;
            $auditPlanModel->original_month = $month;
            $auditPlanModel->original_year = $year;
            $auditPlanModel->type_of_audit = $req->input('iso_ref');
            $auditPlanModel->choose_lead_auditor = $req->input('choose_lead_auditor');
            $auditPlanModel->status = 1;
            $createdBy = !empty($req->input('hid')) ? $auditPlanModel->created_by : Auth::guard('customer')->user()->id;
            $updatedBy = !empty($req->input('hid')) ? Auth::guard('customer')->user()->id : 0;
            $auditPlanModel->created_by = $createdBy;
            $auditPlanModel->updated_by = $updatedBy;
            if ($auditPlanModel->save()) {
                foreach ($req->input('branch') as $eachBranch) {
                    $auditBranchModel = new AuditBranch;
                    $auditBranchModel->audit_id = $auditPlanModel->id;
                    $auditBranchModel->branch_id = $eachBranch;
                    $auditBranchModel->save();
                }
                foreach ($req->input('choose_process') as $eachProcess) {
                    $processAuditModel = new ProcessToAudit;
                    $processAuditModel->audit_id = $auditPlanModel->id;
                    $processAuditModel->process_id = $eachProcess;
                    $processAuditModel->save();
                }
                if (empty($req->input('hid'))) {
                    $leadAuditorDetail = Users::find($req->input('choose_lead_auditor'));
                    $processModel = AuditPlanMaster::find($auditPlanModel->id);
                    $selectedProcessNames = selectedProcessNames($processModel->process);
                    $auditTeamMember = [
                        'audit_id' => $auditPlanModel->id,
                        'auditor_id' => $req->input('choose_lead_auditor'),
                        'auditor_type' => 1,
                        'status' => 1,
                    ];
                    (new \App\Repositories\AuditPlanning)->insertTeamMember($auditTeamMember);
                    $lang = Config::get('settings.lang');
                    $mailData = array(
                                        'auditId' => $auditPlanModel->id,
                                        'name' => $leadAuditorDetail->fname,
                                        'processName' => $selectedProcessNames,
                                        'customerAdmin' =>Auth::guard('customer')->user()->name,
                                        'lang' => $lang[$leadAuditorDetail->language],
                                        'category' => $req->input('audit_category')
                                      );
                    Mail::to($leadAuditorDetail->email)->send(new LeadAuditorChosen($mailData));
                }
            }
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/audit/planning-list/all');
        }
    }

    public function checkProcessBranch(Request $req)
    {
        if (!empty($req->input('selectedBranch'))) {
            $selectedBranch = json_decode($req->input('selectedBranch'));
            $processOfselBranch = ProcessBranch::with('process')
                                    ->whereIn('branch_id', $selectedBranch)
                                    ->groupBy('process_id')
                                    ->havingRaw('COUNT(*) = '.count($selectedBranch))
                                    ->get()
                                    ->pluck('process.name', 'process.id')
                                    ->prepend(translate('form.select'), '')
                                    ->toArray();
            $data = [];
            $i = 0;
            foreach ($processOfselBranch as $key => $val) {
                $data[$i]['id'] = $key;
                $data[$i]['text'] = $val;
                $i++;
            }
            return $data;
        }
    }

    public function checkProcessOwner(Request $req)
    {
        if (!empty($req->input('selectedProcess'))) {
            $selectedProcess = json_decode($req->input('selectedProcess'));
            $processOwners = [];
            foreach ($selectedProcess as $eachprocess) {
                $porcess = Process::find($eachprocess);
                $processOwners[] = $porcess->owner_id;
            }
            if ($req->input('type') == 'internal') {
                $leadAuditors = Users::companyStatusCheck()->inServiceWithEmail()->where('is_auditor', 1)->whereNotIn('id', $processOwners)->pluck('name', 'id')->prepend(translate('form.select'), '')->toArray();
            } else {
                $leadAuditors = Users::companyStatusCheck()->inServiceWithEmail()->where('is_auditor', 1)->pluck('name', 'id')->prepend(translate('form.select'), '')->toArray();
            }
            $filteredAuditors = [];
            $i = 0;
            foreach ($leadAuditors as $key=>$val) {
                $filteredAuditors [$i]['id'] = $key;
                $filteredAuditors [$i]['text'] = $val;
                $i++;
            }
            return $filteredAuditors;
        } else {
            return response()->json(false);
        }
    }
    public function auditDuplicateCheck(Request $req)
    {
        $checkProcess = 0 ;
        if (!empty($req->input())) {
            $auditType = $req->input('audit_category');
            $chooseProcess = $req->input('choose_process');
            $monthYear = explode('/', $req->input('choose_audit_month'));
            $month = $monthYear[0];
            $year = $monthYear[1];
            $getAuditData = AuditPlanMaster::where('category', $auditType)->where('plan_month', $month)->where('plan_year', $year)->pluck('id')->toArray();
            if (!empty($getAuditData)) {
                $checkProcess = ProcessToAudit::whereIn('audit_id', $getAuditData)->whereIn('process_id', $chooseProcess)->count();
            }
        }
        return $checkProcess;
    }
}
