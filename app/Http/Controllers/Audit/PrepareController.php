<?php

namespace App\Http\Controllers\Audit;

use App;
use App\Http\Controllers\Controller;
use App\Models\AuditPlanAuditees;
use App\Models\AuditPlanMaster;
use App\Models\Process;
use App\Models\Users;
use Auth;
use Illuminate\Http\Request;
use Mail;
use Session;
use View;
use Config;
use App\Mail\AuditorAssigned;
use App\Mail\AuditeeAssigned;
use App\Repositories\AuditPrepare as PrepareRepo;

class PrepareController extends Controller
{
    protected $repo;

    public function __construct(PrepareRepo $prepareRepo)
    {
        $this->repo = $prepareRepo;
    }

    public function auditPrepareForm(Request $req)
    {
        $breadcrumbs = [
            '#' => 'words.audit_preperation',
        ];

        $user = Auth::guard('customer')->user();
        $plannedAudits = $this->repo->getPlannedAudits($user->company_id, $req->id);
        $selectedProcessOwner = [];
        $selectedProcessNames = [];
        $selectedBranchNames = [];
        $editable = true;
        foreach ($plannedAudits->process as $each) {
            $selectedProcessOwner[] = $each->processDetail->owner_id;
            $selectedProcessNames[] = $each->processDetail->name;
        }
        foreach ($plannedAudits->branch as $each) {
            $selectedBranchNames[] = $each->branchDetail->name;
        }

        $audit_id = $req->id;
        if ($plannedAudits->category != 'internal') {
            $selectedProcessOwner = [];
        }
        
        $internalAuditors = $this->repo->getInternalAuditor($selectedProcessOwner);
        $internalAuditors_selected = $this->repo->selectedInternalAuditor($audit_id);

        if ($plannedAudits->count() > 0) {
            $auditees = $this->repo->getAuditee($plannedAudits->choose_lead_auditor);
        } else {
            $auditees = array(translate('form.select'), '');
        }
        if (array_key_exists($user->id, $auditees)) {
            $editable = false;
        }

        $auditeesSelected = $this->repo->getSelectedAuditee($audit_id);
     
        return View::make('pages.audit.audit_preparation_edit', compact('breadcrumbs', 'plannedAudits', 'internalAuditors_selected', 'internalAuditors', 'auditees', 'auditeesSelected', 'selectedProcessNames', 'selectedProcessOwner', 'editable', 'selectedBranchNames'));
    }

    public function auditPrepareSave(Request $req)
    {
        if (!empty($req->input())) {
            $invitation = ($req->input('invitation') != null) ? 1 : 0;

            //Loading Models
            $auditPrepModel = [];
            $getaudit_det = [];
            if (!empty($req->input('hid'))) {
                $auditPrepModel = AuditPlanMaster::find($req->input('hid'));
                $getaudit_det = AuditPlanMaster::where('id', $req->input('hid'))->get();
            }
            $selectedProcessNames = selectedProcessNames($auditPrepModel->process);

            $updatedBy = !empty($req->input('hid')) ? Auth::guard('customer')->user()->id : 0;

            $new_month = date('m', strtotime($req->input('audit_date')));
            $new_year = date('Y', strtotime($req->input('audit_date')));

            $auditPrepModel->plan_month = $new_month;
            $auditPrepModel->plan_year = $new_year;

            $auditPrepModel->original_month = $getaudit_det[0]->plan_month;
            $auditPrepModel->original_year = $getaudit_det[0]->plan_year;

            $auditPrepModel->plan_date = $req->input('audit_date');
            $auditPrepModel->plan_time = $req->input('audit_time');
            $auditPrepModel->objective = $req->input('objective');
            $auditPrepModel->updated_by = $updatedBy;

            //Setting model data for user master details
            if ($auditPrepModel->save()) {
                //////Insert internal auditors
                $internal_auditors = $req->input('internal_auditor');

                $this->repo->deleteTeamMember($req->input('hid'));
                if (!empty($internal_auditors)) {
                    foreach ($internal_auditors as $k => $auditor) {
                        $audit_team_mamber = array(
                            'audit_id' => $req->input('hid'),
                            'auditor_id' => $auditor,
                            'auditor_type' => 2,
                            'status' => 1,
                        );
                        $this->repo->insertTeamMember($audit_team_mamber);
                    }
                }

                //////Insert internal auditees
                $auditees = $req->input('auditee');
                AuditPlanAuditees::where('audit_id', $req->input('hid'))->delete();

                if (!empty($auditees)) {
                    foreach ($auditees as $m => $auditee) {
                        $plan_auditees = array(
                            'audit_id' => $req->input('hid'),
                            'user_id' => $auditee,
                            'status' => 1,
                        );
                        AuditPlanAuditees::insert($plan_auditees);
                    }
                }

                if ($invitation == 1) {
                    $lang = Config::get('settings.lang');
                    ////////mail send to internal
                    if (!empty($internal_auditors)) {
                        foreach ($internal_auditors as $internal => $audito) {
                            $internal_auditor_detail = Users::find($audito);
                            $mailData = array(
                                'auditId' => $req->input('hid'),
                                'name' => $internal_auditor_detail->fname,
                                'auditName' => $auditPrepModel->audit_name,
                                'processName' => $selectedProcessNames,
                                'leadAuditor' => $auditPrepModel->lead_auditor->name,
                                'auditDate' => $req->input('audit_date'),
                                'auditTime' => $req->input('audit_time'),
                                'lang' => $lang[$internal_auditor_detail->language],
                                'category' => $auditPrepModel->category
                            );
                            Mail::to($internal_auditor_detail->email)->send(new AuditorAssigned($mailData));
                        }
                    }
                    ////////mail send to Auditee
                    if (!empty($auditees)) {
                        foreach ($auditees as $audite => $audites) {
                            $auditee = Users::find($audites);
                            $mailData = array(
                                'name' => $auditee->fname,
                                'auditName' => $auditPrepModel->audit_name,
                                'processName' => $selectedProcessNames,
                                'leadAuditor' => $auditPrepModel->lead_auditor->name,
                                'auditDate' => $req->input('audit_date'),
                                'auditTime' => $req->input('audit_time'),
                                'lang' => $lang[$auditee->language],
                                'category' => $auditPrepModel->category
                            );
                            if ($auditee->email != '') {
                                Mail::to($auditee->email)->send(new AuditeeAssigned($mailData));
                            }
                        }
                    }
                }

                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/home');
            }
        }
    }
    public function deleteAudit(Request $req)
    {
        $auditPrepModel = AuditPlanMaster::find($req->id);
        if (!empty($auditPrepModel)) {
            if ($auditPrepModel->delete()) {
                $req->session()->flash('success', translate('words.success_message'));
            }
        }
        return redirect('/home');
    }
}
