<?php

namespace App\Http\Controllers\Audit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ReportedIssues;
use App\Models\Users;
use App\Models\ReportingSourceUserAccess;
use App\Models\SourceTypes;
use App\Models\Process;
use App\Models\IsoClause;
use App\Models\ReportedIssuesDetails;
use App\Models\ExternalIssueFixing;
use App\Models\CompanySettings;
use App\Models\Supplier;
use Auth;
use View;
use Config;

class ReportFindingController extends Controller
{
    public function reportIssueForm(Request $req)
    {
        $breadcrumbs = [
            '/audit/report-issue' => 'form.report_issue',
            '#' => 'words.form',
        ];
        $users = Users::companyCheck()->where('out_of_service', null)->where('no_email', 0)->pluck('name', 'id')->prepend(translate('form.who'), '');
        $getsourceTypesAccess = ReportingSourceUserAccess::companyCheck()->where('user_id', Auth::guard('customer')->user()->id)->pluck('source_type_id')->toArray();
        $getallSourceTypesAccess = ReportingSourceUserAccess::companyCheck()->pluck('source_type_id')->toArray();
        $sourceTypeIptions = SourceTypes::companyStatusCheck();
        $sourceTypes = $sourceTypeIptions->where(function ($query) use ($getsourceTypesAccess,$getallSourceTypesAccess) {
            $query->whereIn('id', $getsourceTypesAccess);
            $query->orWhere('type', '1');
            $query->orwhereNotIn('id', $getallSourceTypesAccess);
        })->pluck('source_type', 'id')->prepend(translate('form.select'), '');
        $getsourceTypesAccess = $sourceTypeIptions->where(function ($query) use ($getsourceTypesAccess) {
            $query->whereIn('id', $getsourceTypesAccess);
        })->pluck('id');

        $processes = Process::companyStatusCheck()->pluck('name', 'id')->prepend(translate('words.process'), '');

        $langArr = Config::get('settings.lang');
        $isoNo = 'iso_no_' . $langArr[session('lang')];
        $isoRefs = IsoClause::pluck($isoNo, 'id')->prepend(translate('form.select'), '');
        $langArr = Config::get('settings.lang');
        $name = 'name_' . $langArr[session('lang')];
        $references = (new \App\Repositories\AuditAction)->getReferences($name);
        if (!empty($req->process_id)) {
            $processId = $req->process_id;
        } else {
            $processId = "";
        }
        $isDeadlineEditable = CompanySettings::companyCheck()->pluck('deadline_editable')[0];
        $reportedIssuesOverview = (new \App\Repositories\AuditAction)->reportedIssuesOverview($getsourceTypesAccess);
        $suppliers = Supplier::companyStatusCheck()->where('is_active', 1)->pluck('name', 'id')->prepend(translate('form.select'), '');
        return View::make('pages.audit.reported_issue.overview', compact('breadcrumbs', 'sourceTypes', 'findingTypes', 'processes', 'processId', 'isoRefs', 'references', 'users', 'reportedIssuesOverview', 'isDeadlineEditable', 'suppliers'));
    }

    public function reportIssueSave(Request $req)
    {
        if (!empty($req->input())) {
            $companyId = Auth::guard('customer')->user()->company_id;
            $issue = ReportedIssues::companyCheck()->where('issue_code', 'like', date('Ymd') . '%')->orderBy('id', 'DESC')->first();
            if (!isset($issue->exists)) {
                $issueCode = date('Ymd') . '-1';
            } else {
                $issueCode = str_replace(date('Ymd') . '-', '', $issue->issue_code);
                $issueCode = $issueCode + 1;
                $issueCode = date('Ymd') . '-' . $issueCode;
            }
            $model = new ReportedIssues;
            $model->issue_code = $issueCode;
            $model->company_id = $companyId;
            $model->source_type = $req->input('source_type');
            if (!empty($req->input('supplier'))) {
                $model->supplier = $req->input('supplier');
                $req->session()->flash('supplier', $req->input('supplier'));
            }
            $model->reference = $req->input('reference');
            $model->status = 0;
            $model->created_by = Auth::guard('customer')->user()->id;
            $model->save();
            $report_id = $model->id;
            $detailsModel = new ReportedIssuesDetails;
            $detailsModel->reported_issue_id = $report_id;
            $detailsModel->issue = $req->input('issue');
            $detailsModel->action = $req->input('action');
            $detailsModel->who = $req->input('who');
            $detailsModel->deadline = $req->input('deadline');
            $detailsModel->related_process = $req->input('related_process');
            $detailsModel->status = 1;
            $detailsModel->created_by = Auth::guard('customer')->user()->id;
            $detailsModel->save();
            $req->session()->flash('success', translate('words.success_message'));
            //Holding previous source and reference
            $req->session()->flash('source', $req->input('source_type'));
            $req->session()->flash('reference', $req->input('reference'));
            return redirect('audit/report-issue');
        }
    }

    public function reportIssueFormEdit(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/audit/report-issue' => 'words.report_finding',
                '#' => 'View',
            ];
            $details = ReportedIssues::where('id', $req->id)->with('source_type')->with('finding_type')->with('process')->get()->toArray();

            return View::make('pages.audit.report_issue_view', compact('breadcrumbs', 'details'));
        }
    }

    public function reportedIssueFixForm(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '#' => 'form.report_delegation',
            ];
            $id = $req->id;
            $data = ReportedIssuesDetails::find($req->id);
            if ($data->who != Auth::guard('customer')->user()->id) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            return View::make('pages.audit.issue_fix_form', compact('breadcrumbs', 'data', 'id'));
        }
    }

    public function reportedIssueFixSave(Request $req)
    {
        if (!empty($req->input())) {
            $model = new ExternalIssueFixing;
            $model->issue_detail_id = $req->input('hid');
            $model->action_description = $req->input('action_description');
            if (!empty($req->file('upload_doc'))) {
                $filename = $req->file('upload_doc')->getClientOriginalName();
                $req->file('upload_doc')->move(
                    storage_path('app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents/external_issue_fixing_docs/'),
                    $filename
                );
                $model->supporting_doc = $filename;
            }
            $model->fixed_on = date('Y-m-d');
            if ($model->save()) {
                ReportedIssuesDetails::where('id', $req->input('hid'))->update(['status' => '2']);
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/home');
            }
        }
    }

    public function getFixingData(Request $req)
    {
        if (!empty($req->id)) {
            $data = ExternalIssueFixing::where('issue_detail_id', $req->id)->first();
            return View::make('pages.audit.fixed_issue_support_data', compact('data'));
        }
    }

    public function updateDeadline(Request $req)
    {
        if (!empty($req->input('issue_id')) && !empty($req->input('deadline'))) {
            $model = ReportedIssuesDetails::find($req->input('issue_id'));
            $model->deadline = $req->input('deadline');
            $model->save();
        }
        $req->session()->flash('success', translate('words.success_message'));
        return redirect('/audit/report-issue');
    }

    public function reportIssueDetail(Request $req)
    {
        $breadcrumbs = [
            '/audit/report-issue' => 'form.report_issue',
            '#' => 'words.form',
        ];
        $users = Users::companyCheck()->where('out_of_service', null)->where('no_email', 0)->pluck('name', 'id')->prepend(translate('form.who'), '');
        $getsourceTypesAccess = ReportingSourceUserAccess::companyCheck()->where('user_id', Auth::guard('customer')->user()->id)->pluck('source_type_id')->toArray();
        $sourceTypeIptions = SourceTypes::companyStatusCheck();
        $sourceTypes = $sourceTypeIptions->where(function ($query) use ($getsourceTypesAccess) {
            $query->whereIn('id', $getsourceTypesAccess);
            $query->orWhere('type', '1');
        })->pluck('source_type', 'id')->prepend(translate('form.select'), '');
        $getsourceTypesAccess = $sourceTypeIptions->where(function ($query) use ($getsourceTypesAccess) {
            $query->whereIn('id', $getsourceTypesAccess);
        })->pluck('id');
        $processes = Process::companyStatusCheck()->pluck('name', 'id')->prepend(translate('words.process'), '');
        $langArr = Config::get('settings.lang');
        $isoNo = 'iso_no_' . $langArr[session('lang')];
        $isoRefs = IsoClause::pluck($isoNo, 'id')->prepend(translate('form.select'), '');
        $langArr = Config::get('settings.lang');
        $name = 'name_' . $langArr[session('lang')];
        $references = (new \App\Repositories\AuditAction)->getReferences($name);
        if (!empty($req->process_id)) {
            $processId = $req->process_id;
        } else {
            $processId = "";
        }
        $isDeadlineEditable = CompanySettings::companyCheck()->pluck('deadline_editable')[0];
        $sourceTypeId = !empty($req->source) ? $req->source : 0;
        $reference = !empty($req->reference) ? base64_decode($req->reference) : '';
        $reportedIssuesDetails = (new \App\Repositories\AuditAction)->reportedIssuesDetails($getsourceTypesAccess, $sourceTypeId, $reference);
        $referenceList = $reportedIssuesDetails->pluck('reference', 'reference')->prepend(translate('form.select'), '')->toArray();
        $suppliers = Supplier::companyStatusCheck()->where('is_active', 1)->pluck('name', 'id')->prepend(translate('form.select'), '');
        return View::make('pages.audit.reported_issue.detail', compact('breadcrumbs', 'sourceTypes', 'findingTypes', 'processes', 'processId', 'isoRefs', 'references', 'users', 'reportedIssuesDetails', 'isDeadlineEditable', 'referenceList', 'suppliers'));
    }
}
