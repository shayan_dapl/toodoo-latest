<?php

namespace App\Http\Controllers\Audit;

use App;
use App\Http\Controllers\Controller;
use App\Models\SourceTypes;
use App\Http\Requests\SourceTypeRequest;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;
use App\Models\Users;
use App\Models\ReportingSourceUserAccess;
use App\Models\Supplier;

class SourceTypeController extends Controller
{
    public function sourceTypelist()
    {
        $breadcrumbs = [
            '#' => 'words.reporting_source',
        ];
        $source_types = SourceTypes::companyCheck()->where('status', '<>', 3)->get();

        return View::make('pages.audit.audit_source_type_list', compact('source_types', 'breadcrumbs'));
    }

    public function sourceTypeForm()
    {
        $breadcrumbs = ['#' => 'words.reporting_source'];
        $users = Users::companyCheck()->where('out_of_service', null)->pluck('name', 'id');
        $usersSelected = [];
        $audit_source_type = [];
        return View::make('pages.audit.audit_source_type_form', compact('breadcrumbs', 'users', 'usersSelected', 'audit_source_type'));
    }

    public function sourceTypeSave(SourceTypeRequest $req)
    {
        if (!empty($req->input())) {
            if (!empty($req->input('hid'))) {
                $model = SourceTypes::find($req->input('hid'));
            } else {
                $model = new SourceTypes;
            }
            $companyId = Auth::guard('customer')->user()->company_id;
            $model->source_type = $req->input('audit_source_type');
            $model->status = 1;
            $model->company_id = Auth::guard('customer')->user()->company_id;
            $typeWiseUserAccess = [];
            if ($model->save()) {
                $userAccess = $req->input('user_access');
                ReportingSourceUserAccess::where('source_type_id', $model->id)->delete();
                if (!empty($userAccess)) {
                    foreach ($userAccess as $m => $user) {
                        $typeWiseUserAccess[] = array(
                            'source_type_id' => $model->id,
                            'company_id' => $companyId,
                            'user_id' => $user,
                            'status' => 1,
                        );
                    }
                    ReportingSourceUserAccess::insert($typeWiseUserAccess);
                }
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('audit/source-type-list');
            }
        }
    }

    public function sourceTypeEdit(Request $req)
    {
        $breadcrumbs = [
            '#' => 'words.reporting_source',
        ];
        $audit_source_type = SourceTypes::find($req->id);
        if (!checkCompanyAccess($audit_source_type)) {
            $req->session()->flash('error', translate('words.you_dont_have_access'));
            return redirect('/home');
        }
        $users = Users::companyCheck()->where('out_of_service', null)->pluck('name', 'id');
        $usersSelected = ReportingSourceUserAccess::where('source_type_id', $req->id)->pluck('user_id')->toArray();

        return View::make('pages.audit.audit_source_type_form', compact('audit_source_type', 'breadcrumbs', 'users', 'usersSelected'));
    }

    public function sourceTypeDelete(Request $req)
    {
        if (!empty($req->id)) {
            $model = SourceTypes::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $model->status = 3;
            $model->updated_by = Auth::guard('customer')->user()->id;
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('audit/source-type-list');
            }
        }
    }

    public function getSourceTypeAccess(Request $req)
    {
        if (!empty($req->id)) {
            $sourceData = SourceTypes::findOrFail($req->id);
            $assignedUsers = [];
            $assignedUsersCount = 0;
            if ($sourceData->assignedusers()->count()>0) {
                $assignedUsers = $sourceData->assignedusers;
                $assignedUsersCount = $sourceData->assignedusers()->count();
            }
            $suppliers = Supplier::companyStatusCheck()->where('is_active', 1)->pluck('name', 'id')->prepend(translate('form.select'), '');
            return View::make('pages.audit.reported_issue.teamnotify_icon', compact('sourceData', 'assignedUsers', 'assignedUsersCount', 'suppliers'));
        } else {
            return response()->json(false);
        }
    }
}
