<?php

namespace App\Http\Controllers\Audit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Supplier;
use App\Models\SupplierParameter;
use App\Models\SupplierRating;
use App\Models\Users;
use App\Models\SupplierEvaluationActionLog;
use App\Models\SupplierEvaluationRates;
use App\Models\CompanySettings;
use App\Models\SupplierCorrectiveActionLog;
use App\Models\SupplierCorrectiveActionComments;
use App\Models\SupplierCorrectiveActionFiles;
use View;
use Auth;
use PDF;

class SupplierController extends Controller
{
    public function index(Request $req)
    {
        $breadcrumbs = [
            '/audit/supplier-evaluation' => 'words.supplier_evaluation'
        ];
        $suppliers = Supplier::companyStatusCheck()->where('is_eval', 1)->get();
        $parameters = SupplierParameter::companyStatusCheck()->get();
        $users = Users::companyCheck()->inServiceWithEmail()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $ratings = SupplierRating::companyStatusCheck()->select('id', 'rating', 'description')->get()->toArray();
        $legend = $ratings;
        $settings = CompanySettings::companyCheck()->first()->supplier_threshold;
        $thresholdLimit = ceil((count($ratings)*$settings)/100);
        array_unshift($ratings, ['id' => '', 'rating' => translate('form.select'), 'description' => '']);
        $ratings = json_encode($ratings);
        return View::make('pages.audit.supplier_evaluation', compact('breadcrumbs', 'suppliers', 'parameters', 'ratings', 'users', 'thresholdLimit', 'legend'));
    }

    public function save(Request $req)
    {
        if (!empty($req->input())) {
            $model = new SupplierEvaluationActionLog;
            $model->company_id = Auth::guard('customer')->user()->company_id;
            $model->supplier_id = $req->input('supplier_id');
            $model->comment = $req->input('comment');
            $model->action = $req->input('action');
            $model->delegate_to = $req->input('delegateTo');
            $model->deadline = $req->input('deadline');
            if ($model->save()) {
                if (!empty($req->input('parameters'))) {
                    foreach ($req->input('parameters') as $key => $param) {
                        $rate = new SupplierEvaluationRates;
                        $rate->company_id = Auth::guard('customer')->user()->company_id;
                        $rate->log_id = $model->id;
                        $rate->parameter_id = $param;
                        $rate->rating_id = $req->input('rates')[$key];
                        $rate->save();
                    }
                }
            }
            if (!empty($req->input('threshold'))) {
                $sarc = new SupplierCorrectiveActionLog;
                $sarc->company_id = Auth::guard('customer')->user()->company_id;
                $sarc->log_id = $model->id;
                $sarc->supplier_id = $req->input('supplier_id');
                $sarc->parameters = $req->input('threshold');
                $sarc->deadline = $req->input('sarcdeadline');
                $sarc->delegate_to = $req->input('sarcdelegate');
                $sarc->save();
            }
            return response()->json(true);
        } else {
            return response()->json(false);
        }
    }

    public function list(Request $req)
    {
        $data = SupplierEvaluationActionLog::companyCheck()->where('supplier_id', $req->supplier)->with('rates')->with('comments')->with('correctives')->orderBy('id', 'DESC')->get();
        return response()->json($data);
    }

    public function correctiveIssueFixing(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '#' => 'words.action',
            ];
            $log = SupplierCorrectiveActionLog::find($req->id);
            return View::make('pages.supplier.corrective_rating_issue_fixing', compact('breadcrumbs', 'log'));
        }
    }

    public function correctiveIssueFixingSave(Request $req)
    {
        if (!empty($req->input())) {
            //Comment adding
            if (!empty($req->input('reply'))) {
                $comment = new SupplierCorrectiveActionComments;
                $comment->company_id = Auth::guard('customer')->user()->company_id;
                $comment->log_id = $req->input('action_log_id');
                $comment->reply = $req->input('reply');
                $comment->save();
            }

            //Supporting document uploading per comment
            if (!empty($req->file('upload_doc'))) {
                foreach ($req->file('upload_doc') as $index => $file) {
                    $filename = time() . '-' . $file->getClientOriginalName();
                    $file->move(
                        storage_path('app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents/supplier_corrective_action_docs/'),
                        $filename
                    );
                    $fileModel = new SupplierCorrectiveActionFiles;
                    $fileModel->comment_id = $comment->id;
                    $fileModel->doc_name = $filename;
                    $fileModel->submitted_by = Auth::guard('customer')->user()->id;
                    $fileModel->save();
                }
            }

            //Action log closed
            if ($req->input('closed')) {
                SupplierCorrectiveActionLog::where('id', $req->input('action_log_id'))->update(['closed' => 1, 'closed_at' => date('Y-m-d')]);
            }

            $req->session()->flash('success', translate('words.success_message'));
            return redirect('home/issues');
        }
    }

    public function downloadScar(Request $req)
    {
        if (empty($req->id)) {
            return redirect(\URL::previous());
        }
        $action = SupplierEvaluationActionLog::find($req->id);
        $corrective = $action->correctives;
        $aboveThreshold = $action->rates()->whereNotIn('parameter_id', explode(",", $corrective->parameters))->get()->pluck('rate.description', 'parameter.name')->toArray();
        $belowThreshold = $action->rates()->whereIn('parameter_id', explode(",", $corrective->parameters))->get()->pluck('rate.description', 'parameter.name')->toArray();
        $html = View::make('pages.supplier.download_scar', compact('action', 'corrective', 'aboveThreshold', 'belowThreshold'))->render();
        return PDF::load($html)->filename('/tmp/'.str_replace('/', '-', $action->supplier->name).'.pdf')->download();
    }
}
