<?php

namespace App\Http\Controllers\Audit;

use App;
use App\Http\Controllers\Controller;
use App\Models\AuditTypeMaster;
use App\Models\IsoClause;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;
use Config;

class TypeController extends Controller
{
    public function getAuditType(Request $req)
    {
        $options = auditTypesList($req->ref_id);
        $topTypes = AuditTypeMaster::parentType($req->ref_id)->pluck('id')->toArray();
        $type = $req->type;
        return View::make('pages.helper.audit_type_dropdown', compact('options', 'topTypes', 'type'));
    }

    public function auditTypeList(Request $req)
    {
        $breadcrumbs = [
            '/audit/iso/list' => 'words.standerd_iso_clause',
            '#' => 'words.list',
        ];
        $auditTypes = AuditTypeMaster::where('iso_ref_id', $req->id)->orderBy('id', 'DESC')->get();
        $isoDetail = IsoClause::where('id', $req->id)->first();

        return View::make('pages.audit.audit_type_list', compact('breadcrumbs', 'auditTypes', 'isoDetail'));
    }

    public function auditTypeForm(Request $req)
    {
        $breadcrumbs = [
            ('/audit/type/' . $req->id) => 'words.audit_type',
            '#' => 'words.add',
        ];
        $selectedAuditType = $req->id;
        $parentAuditType = auditTypesList($req->id);
        $language = Config::get('settings.lang');
        $iso_no = 'iso_no_' . $language[session('lang')];
        $topTypes = AuditTypeMaster::parentType($req->id)->pluck('id')->toArray();
        $isoReference = IsoClause::all()->pluck($iso_no, 'id')->prepend(translate('form.select'), '0');
        $ref_id = $selectedAuditType;
        return View::make('pages.audit.audit_type_form', compact('breadcrumbs', 'selectedAuditType', 'parentAuditType', 'topTypes', 'isoReference', 'ref_id'));
    }

    public function auditTypeSave(Request $req)
    {
        $returnUrl = !empty($req->input('hid')) ? ('/audit/type/edit/' . $req->input('hid')) : '/audit/type-form/' . $req->input('iso_reference');

        if (!empty($req->input())) {
            $match = 0;
            $langArr = Config::get('settings.lang');
            ;
            //Loading Models
            if (!empty($req->input('hid'))) {
                $auditTypeModel = AuditTypeMaster::find($req->input('hid'));
                $match = AuditTypeMaster::where('id', '<>', $req->input('hid'))
                    ->where('clause', $req->input('clause'))
                    ->where('status', 1)
                    ->count();
            } else {
                $auditTypeModel = new AuditTypeMaster;
                $match = AuditTypeMaster::where('clause', $req->input('clause'))->where('status', 1)->count();
            }

            $createdBy = !empty($req->input('hid')) ? $auditTypeModel->created_by : Auth::guard('web')->user()->id;
            $updatedBy = !empty($req->input('hid')) ? Auth::guard('web')->user()->id : 0;

            $auditTypeModel->clause = $req->input('clause');

            $auditTypeModel->name_nl = $req->input('name_nl');
            $auditTypeModel->description_nl = $req->input('description_nl');

            $auditTypeModel->name_en = $req->input('name_en');
            $auditTypeModel->description_en = $req->input('description_en');

            $auditTypeModel->name_fr = $req->input('name_fr');
            $auditTypeModel->description_fr = $req->input('description_fr');

            $auditTypeModel->parent_id = $req->input('parent_audit_type');
            $auditTypeModel->iso_ref_id = $req->input('iso_reference');

            $status = 1;
            foreach ($langArr as $eachKey => $eachVal) {
                if ($req->input('name_' . $eachKey) == "" || $req->input('description_' . $eachKey) == "") {
                    $status = 0;
                }
            }
            $auditTypeModel->status = $status;
            $auditTypeModel->created_by = $createdBy;
            $auditTypeModel->updated_by = $updatedBy;

            if ($match == 0) {
                //Setting model data for user master details
                if ($auditTypeModel->save()) {
                    $req->session()->flash('success', translate('words.success_message'));
                    return redirect('/audit/type/' . $req->input('iso_reference'));
                }
            } else {
                $req->session()->flash("error", ($req->input('parent_id') . ' ' . $req->input('name') . translate('words.audit_type_with_same_name_already_exist')));
                return redirect($returnUrl)->withInput($req->input());
            }
        }
    }

    public function auditTypeEdit(Request $req)
    {
        if (!empty($req->id)) {
            $langArr = Config::get('settings.lang');
            $iso_no = 'iso_no_' . $langArr[session('lang')];
            $auditType = AuditTypeMaster::find($req->id);
            $parentAuditType = auditTypesList($auditType->iso_ref_id);

            $isoReference = IsoClause::all()->pluck($iso_no, 'id')->prepend(translate('form.select'), '0');
            $topTypes = AuditTypeMaster::parentType($auditType->iso_ref_id)->pluck('id')->toArray();
            $breadcrumbs = [
                ('/audit/type/' . $auditType->iso_ref_id) => 'words.audit_type',
                '#' => 'words.edit',
            ];
            if (isset($auditType->iso_ref_id)) {
                $ref_id = $auditType->iso_ref_id;
            } else {
                $ref_id = '';
            }
            return View::make('pages.audit.audit_type_form', compact('breadcrumbs', 'parentAuditType', 'isoReference', 'topTypes', 'auditType', 'ref_id'));
        }
    }

    public function auditTypeDelete(Request $req)
    {
        if (!empty($req->id)) {
            $childCheck = AuditTypeMaster::where('parent_id', $req->id);
            $isoId = '';
            if ($childCheck->count() == 0) {
                $isoId = AuditTypeMaster::where('id', $req->id)->first()->iso_ref_id;
                AuditTypeMaster::where('id', $req->id)->delete();
                $req->session()->flash('success', translate('words.success_message'));
            } else {
                $req->session()->flash('error', translate('words.child_exist_can_not_delete'));
            }

            return redirect('/audit/type/' . $isoId);
        }
    }
}
