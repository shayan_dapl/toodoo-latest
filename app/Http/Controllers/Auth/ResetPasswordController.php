<?php 

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Models\Users;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use View;

class ResetPasswordController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Password Reset Controller
      |--------------------------------------------------------------------------
      |
      | This controller is responsible for handling password reset requests
      | and uses a simple trait to include this behavior. You're free to
      | explore this trait and override any methods you wish to tweak.
      |
     */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        //Setting the default Language
        if (!Session::has('lang')) {
            Session::put('lang', 'nl');
            Session::put('langName', 'nl');
        } else {
            Session::put('lang', Session::get('lang'));
            Session::put('langName', Session::get('lang'));
        }

        return View::make('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        Users::where('email', $user->email)->update([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60)
        ]);
        //Membership validity checks
        if ($user->type == 1) {
            \Session::flash('success', translate('alert.password_changed_all'));
            Auth::guard('web')->login($user);
        } elseif ($user->type == 4) {
            \Session::flash('success', translate('alert.password_changed_all'));
            Auth::guard('web')->login($user);
            $this->redirectTo = '/language';
        } else {
            if (Users::inServiceWithEmail()->where('email', $user->email)->count() > 1) {
                Auth::guard('customer')->login($user);
                $this->redirectTo = '/entry-panel';
            } else {
                if ($user->out_of_service != null) {
                    \Session::flash("faliure", translate('error.profile_blocked'));
                    $this->redirectTo = '/';
                } else {
                    $dateDiff = strtotime($user->company->trial_end) - strtotime(date('Y-m-d'));
                    $segment = ceil($dateDiff / (60 * 60 * 24));
                    if ($segment <= 0) {
                        \Session::flash("faliure", translate('alert.trial_period_over'));
                        if ($user->type == 3) {
                            $this->redirectTo = '/';
                        } else {
                            Auth::guard('customer')->login($user);
                            $this->redirectTo = '/membership';
                        }
                    } else {
                        \Session::flash('success', translate('alert.password_changed_all'));
                        Auth::guard('customer')->login($user);
                    }
                }
            }
        }
    }

    public function messages()
    {
        return [
            'passwords.user' => translate('passwords.user')
        ];
    }
}
