<?php

namespace App\Http\Controllers\Company;

use App;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\CustomerAdminRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use App\Models\Countries;
use App\Models\Language;
use App\Models\Company;
use App\Models\PackageMaster;
use App\Models\PriceIncreaseLog;
use App\Models\Branch;
use App\Models\Department;
use App\Models\Process;
use App\Models\PaymentCustomerid;
use View;
use Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
use Config;
use Illuminate\Support\Facades\Hash;
use App\Models\OrganizationStructure;
use Illuminate\Support\Facades\Session;
use Mail;
use App\Mail\PackPriceIncreaseRequest;
use Mollie;

class AdminController extends Controller
{
    public function listing()
    {
        $breadcrumbs = [
            '#' => 'words.customer',
            '/list' => 'words.list',
        ];
        $users = Users::where('id', '<>', 1)->where('type', '2')->groupBy('company_id')->orderBy('id', 'DESC')->get();
        return View::make('pages.company.list', compact('breadcrumbs', 'users'));
    }

    public function subscriptionListing()
    {
        $breadcrumbs = [
            '#' => 'words.customer',
            '/list' => 'words.list',
        ];
        $subscribedCompany = Company::where('status', 1)->where('subscription_id', '<>', '')->get();
        return View::make('pages.company.subscribed_list', compact('breadcrumbs', 'subscribedCompany'));
    }

    public function sendPriceIncreaseRequest(Request $req)
    {
        if (!empty($req->input())) {
            $proposedPcnt = $req->input('increase_percent');
            $selectedItems = $req->input('item');
            $selectedCompanyIds = $req->input('cmpid');
            $getSelecedCompanyData = Company::whereIn('id', $selectedCompanyIds)->get();
            $lang = Config::get('settings.lang');
            foreach ($getSelecedCompanyData as $eachcompany) {
                foreach ($eachcompany->customeradmins()->get() as $eachadmin) {
                    $mailData = array(
                                        'name' => $eachadmin->fname,
                                        'customerAdmin' =>Auth::guard('web')->user()->name,
                                        'lang' => $lang[$eachadmin->language],
                                        'percent' => $proposedPcnt
                                    );
                    Mail::to($eachadmin->email)->send(new PackPriceIncreaseRequest($mailData));
                }
                $priceIncreaseModel = new PriceIncreaseLog;
                $priceIncreaseModel->company_id = $eachcompany->id;
                $priceIncreaseModel->current_package_price = $eachcompany->package_price;
                $priceIncreaseModel->current_user_price = $eachcompany->user_package_price;
                $priceIncreaseModel->current_storage_price = $eachcompany->storage_package_price;
                $priceIncreaseModel->package_price_inc_req = in_array("package", $selectedItems) ? 1 : 0;
                $priceIncreaseModel->user_price_inc_req = in_array("user", $selectedItems) ? 1 : 0;
                $priceIncreaseModel->storage_price_inc_req = in_array("storage", $selectedItems) ? 1 : 0;
                $priceIncreaseModel->pct_request = $proposedPcnt;
                $priceIncreaseModel->request_date =  date(Config::get('settings.db_date'));
                if ($priceIncreaseModel->save()) {
                    $companyModel = Company::find($eachcompany->id);
                    $companyModel->price_inc_approve_status = 0 ;
                    $companyModel->save();
                }
            }
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/subscription-list');
        }
    }

    public function form()
    {
        $breadcrumbs = [
            '/list' => 'words.customer',
            '/form' => 'words.add',
        ];
        $language = Language::all()->pluck('display_text', 'name')->prepend(translate('form.select'), '');
        return View::make('pages.company.form', compact('breadcrumbs', 'language'));
    }

    public function formSave(CustomerAdminRequest $req)
    {
        if (!empty($req->input())) {
            $companyModel = Company::findOrNew($req->input('compid'));
            $userMasterModel = Users::findOrNew($req->input('masterid'));
            $createdBy = !empty($req->input('masterid')) ? $userMasterModel->created_by : Auth::user()->id;
            $updatedBy = !empty($req->input('masterid')) ? Auth::user()->id : 0;
            $companyModel->name = $req->input('name');
            $companyModel->address_line1 = $req->input('address_line1');
            $companyModel->address_line2 = $req->input('address_line2');
            $companyModel->zip = $req->input('comp_zip');
            $companyModel->city = $req->input('comp_city');
            $companyModel->country = $req->input('comp_country');
            $companyModel->tva_no = $req->input('tva_no');
            $companyModel->status = ($req->input('block') != null) ? 2 : 1;
            $companyModel->is_trial = 1;
            $companyModel->trial_end = date('Y-m-d', strtotime($req->input('trail_end_date')));
            $companyModel->user_allowed = Config::get('settings.trial_user_limit');
            $companyModel->storage_allowed = Config::get('settings.trial_storage_limit');
            $companyModel->created_by = $createdBy;
            $companyModel->updated_by = $updatedBy;
            if (!empty($req->file('company_photo'))) {
                $extension = $req->file('company_photo')->getClientOriginalExtension();
                if (in_array(strtolower($extension), Config::get('settings.img_extentions'))) {
                    $filename = 'Company-' . time() . '.' . $extension;
                    $path = storage_path('app/public/company/' . $companyModel->id . '/image/' . $filename);
                    Image::make($req->file('company_photo')->getRealPath())->resize(100, 100)->save($path);
                    $companyModel->photo = $filename;
                }
            }
            if ($companyModel->save()) {
                $inviteUsrToken = ($req->input('invite_user') != '') ? rand('1000', '9999').time() : '';
                $userMasterModel->company_id = $companyModel->id;
                $userMasterModel->name = ($req->input('fname') . ' ' . $req->input('lname'));
                $userMasterModel->fname = $req->input('fname');
                $userMasterModel->lname = $req->input('lname');
                if (!empty($req->file('photo'))) {
                    $extension = $req->file('photo')->getClientOriginalExtension();
                    if (in_array(strtolower($extension), Config::get('settings.img_extentions'))) {
                        $filename = 'User-' . time() . '.' . $extension;
                        $path = storage_path('app/public/company/' . $userMasterModel->company_id . '/image/users/' . $filename);
                        Image::make($req->file('photo')->getRealPath())->resize(100, 100)->save($path);
                        $userMasterModel->photo = $filename;
                    }
                }
                $userMasterModel->language = $req->input('language');
                $userMasterModel->email = $req->input('email');
                $userMasterModel->remember_token = $req->input('_token');
                $userMasterModel->type = 2;
                $userMasterModel->can_process_owner = 1;
                $userMasterModel->is_auditor = 1;
                $userMasterModel->company_view = 1;
                $userMasterModel->invite_user_token = $inviteUsrToken;
                $userMasterModel->created_by = $createdBy;
                $userMasterModel->updated_by = $updatedBy;
                $userMasterModel->save();
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/list');
            }
        }
    }

    public function edit(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '#' => 'words.customer',
                '/form' => 'words.edit',
            ];
            $user = Users::find($req->id);
            $company = $user->company;
            $language = Language::all()->pluck('display_text', 'name')->prepend(translate('form.select'), '');
            $lang  = Config::get('settings.lang');
            $countries = Countries::all()->pluck('name_'.$lang[session('lang')], 'id')->prepend(translate('form.select'), '');
            return View::make('pages.company.form', compact('breadcrumbs', 'company', 'countries', 'language', 'user'));
        }
    }

    public function remove(Request $req)
    {
        if (Auth::guard('web')->check()) {
            $customerId = PaymentCustomerid::where('company_id', $req->id)->first();
            $subscriptionId = Company::find($req->id)->subscription_id;
            if (!empty($customerId) && !empty($subscriptionId)) {
                $cancelled = Mollie::api()->customersSubscriptions()->withParentId($customerId->customer_id)->cancel($subscriptionId);
                if ($cancelled->status == 'cancelled') {
                    Mollie::api()->customers()->delete($customerId->customer_id);
                }
            }
            Company::where('id', $req->id)->delete();
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/list');
        }
    }

    public function changePassword()
    {
        $breadcrumbs = [
            '/password' => 'words.change_password',
        ];
        $hid = (Auth::guard('customer')->check()) ? Auth::guard('customer')->user()->email : Auth::guard('web')->user()->email;
        return View::make('pages.index.change_password', compact('breadcrumbs', 'hid'));
    }

    public function changePasswordSave(ChangePasswordRequest $req)
    {
        if (!empty($req->input())) {
            $password = Hash::make($req->input('password'));
            Users::where('email', $req->input('hid'))->update(['password' => $password]);
            $req->session()->flash('success', translate('alert.password_changed_all'));
            return redirect('/home');
        }
    }

    public function customerAdminExchange(Request $req)
    {
        $fromPerson = Auth::guard('customer')->user()->id;
        $toPerson = $req->input('to_person');
        $cadminExpires = date("Y-m-d", strtotime($req->input('dated')));
        Users::where('id', $toPerson)->update(['type' => 2]);
        Users::where('id', $fromPerson)->update(['type' => 3, 'out_of_service' => $cadminExpires]);
        $checkOrgPos = OrganizationStructure::where('user_id', $fromPerson)->count();
        if ($checkOrgPos > 0) {
            $orgStruct = OrganizationStructure::where('user_id', $fromPerson)->first();
            $orgStruct->user_id = $toPerson;
            $orgStruct->save();
        }
        //If top person is leaving company hadning over position to somebody else
        $findTopper = Users::where('id', $fromPerson)->where('is_top_person', 1)->count();
        if ($findTopper > 0) {
            Users::where('id', $fromPerson)->update(['is_top_person' => 0]);
            Users::where('id', $toPerson)->update(['is_top_person' => 1]);
        }
        Auth::guard('customer')->logout();
        Auth::guard('web')->logout();
        Session::flush();
    }

    public function viewDetail(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/subscription-list' => 'words.manage_subscriptions',
                '#' => 'words.view_detail',
            ];
            $company = Company::find($req->id);
            $priceIncLog = [];
            foreach ($company->priceincreaseproposal()->get() as $key => $eachLog) {
                $pctIncrease = $eachLog->pct_request;
                $currentPackagePrice = $eachLog->current_package_price;
                $currentUserPrice = $eachLog->current_user_price;
                $currentStoragePrice = $eachLog->current_storage_price;

                $newPackagePrice = ($eachLog->package_price_inc_req == 1) ? number_format($currentPackagePrice + ($currentPackagePrice * $pctIncrease)/100, 2) : $currentPackagePrice;
                $newUserPrice = ($eachLog->user_price_inc_req == 1) ? number_format($currentUserPrice + ($currentUserPrice * $pctIncrease)/100, 2) : $currentUserPrice;
                $newStoragePrice = ($eachLog->storage_price_inc_req == 1) ? number_format($currentStoragePrice + ($currentStoragePrice * $pctIncrease)/100, 2) : $currentStoragePrice;
                $priceIncLog[$key]['current_pkg_price'] = $currentPackagePrice;
                $priceIncLog[$key]['new_pkg_price'] = $newPackagePrice;
                $priceIncLog[$key]['current_user_price'] = $currentUserPrice;
                $priceIncLog[$key]['new_user_price'] = $newUserPrice;
                $priceIncLog[$key]['current_storage_price'] = $currentStoragePrice;
                $priceIncLog[$key]['new_storage_price'] = $newStoragePrice;
                $priceIncLog[$key]['proposed_percent'] = $eachLog->pct_request;
                $priceIncLog[$key]['status'] = ($eachLog->approve_status == 1) ? translate('words.approved') : translate('table.pending');
                $priceIncLog[$key]['approve_date'] = ($eachLog->approve_status == 1) ? date(Config::get('settings.dashed_date'), strtotime($eachLog->approved_date)) : translate('words.na');
            }
            return View::make('pages.company.log_detail', compact('breadcrumbs', 'priceIncLog'));
        }
    }
    public function accountInfo(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                'list' => 'words.customer',
                '#' => 'words.account_info',
            ];
            $user = Users::find($req->id);
            $company = $user->company;
            $plans = ['iso' => "", 'user' => "", 'storage' => ""];
            $iso = [];
            if ($company->transactions->count() > 0) {
                foreach ($company->transactions as $each) {
                    foreach ($each->purchased as $pur) {
                        if ($pur->type == 'iso' && $pur->status == 1) {
                            $choosenPkg = $pur->iso()->name_en;
                            $iso[$choosenPkg] = $choosenPkg;
                        }
                    }
                }
                $plans['iso'] = implode(", ", $iso);
            } else {
                $iso = PackageMaster::where('is_base', 1)->first();
                $plans['iso'] = translate('words.base_package');
            }
            $companyFolderPath = storage_path('app/public/company/' . $company->id);
            $fileSize = 0;
            foreach (File::allFiles($companyFolderPath) as $file) {
                $fileSize += $file->getSize();
            }
            $userAdded = Users::where('company_id', $company->id)->inServiceWithEmail()->where('email', '<>', Config::get('settings.associate_user'))->count();
            $branchAdded = Branch::where('company_id', $company->id)->where('status', 1)->count();
            $departmentAdded = Department::where('company_id', $company->id)->where('status', 1)->count();
            $processAdded = Process::where('company_id', $company->id)->where('status', 1)->count();
            $plans['user'] = $company->user_allowed;
            $plans['storage'] = $company->storage_allowed;
            return View::make('pages.company.account_info', compact('breadcrumbs', 'company', 'plans', 'fileSize', 'userAdded', 'branchAdded', 'departmentAdded', 'processAdded'));
        }
    }
}
