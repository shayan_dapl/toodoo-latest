<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\Branch;
use Auth;

class BranchController extends Controller
{
    public function branch()
    {
        $breadcrumbs = [
            '/branch/list' => 'words.branch',
        ];
        return View::make('pages.company.branch', compact('breadcrumbs'));
    }

    public function branchData(Request $req)
    {
        return Branch::companyStatusCheck()->get()->toJson();
    }

    public function branchSave(Request $req)
    {
        if (!empty($req->input())) {
            if (empty($req->input('hid'))) {
                $sameData = Branch::companyStatusCheck()->where('name', $req->input('name'))->count();
                if ($sameData > 0) {
                    return response()->json(false);
                } else {
                    $model = Branch::findOrNew($req->input('hid'));
                    $model->company_id = Auth::guard('customer')->user()->company_id;
                    $model->name = $req->input('name');
                    if (!empty($req->input('is_corporate'))) {
                        $model->is_corporate = $req->input('is_corporate');
                    }
                    if ($model->save()) {
                        return response()->json(true);
                    }
                }
            } else {
                $sameData = Branch::companyStatusCheck()->where('name', $req->input('name'))->where('id', '<>', $req->input('hid'))->count();
                if ($sameData > 0) {
                    return response()->json(false);
                } else {
                    $model = Branch::findOrNew($req->input('hid'));
                    $model->company_id = Auth::guard('customer')->user()->company_id;
                    $model->name = $req->input('name');
                    if (!empty($req->input('is_corporate'))) {
                        $model->is_corporate = $req->input('is_corporate');
                    }
                    if ($model->save()) {
                        return response()->json(true);
                    }
                }
            }
        }
    }

    public function branchRemove(Request $req)
    {
        $child = Branch::branchChild($req->input('id'));
        if ($child > 0) {
            return response('child', 200)->header('Content-Type', 'text/plain');
        } else {
            $isCorporate = Branch::find($req->input('id'))->is_corporate;
            if ($isCorporate == 1) {
                return response()->json(false);
            } else {
                Branch::destroy($req->input('id'));
                return response()->json(true);
            }
        }
    }

    public function checkCorporate()
    {
        $corporateData = Branch::companyStatusCheck()->where('is_corporate', 1)->count();
        if ($corporateData > 0) {
            return response()->json(false);
        } else {
            return response()->json(true);
        }
    }
}
