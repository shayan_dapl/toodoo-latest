<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\Department;
use App\Models\Position;
use App\Models\Branch;
use App\Models\DepartmentBranch;
use Auth;

class DepartmentController extends Controller
{
    public function department()
    {
        $breadcrumbs = [
            '/department/list' => 'words.department',
        ];
        $branches = Branch::companyStatusCheck()->pluck('name', 'id')->toArray();
        return View::make('pages.company.department', compact('breadcrumbs', 'branches'));
    }

    public function departmentData()
    {
        return Department::companyStatusCheck()->with('branches')->get()->toJson();
    }

    public function departmentSave(Request $req)
    {
        if (!empty($req->input())) {
            $sameBranchDepartments = DepartmentBranch::companyCheck()->where('branch_id', $req->input('branch'))->pluck('department_id')->toArray();
            if (empty($req->input('hid'))) {
                $sameData = Department::companyStatusCheck()->whereIn('id', $sameBranchDepartments)->where('name', $req->input('name'))->count();
                if ($sameData > 0) {
                    return response()->json(false);
                } else {
                    $model = Department::findOrNew($req->input('hid'));
                    $model->company_id = Auth::guard('customer')->user()->company_id;
                    if (count($sameBranchDepartments) == 0) {
                        $model->is_top = 1;
                    }
                    $model->name = $req->input('name');
                    if ($model->save()) {
                        DepartmentBranch::where('department_id', $model->id)->delete();
                        $deptBranchModel = new DepartmentBranch;
                        $deptBranchModel->company_id = Auth::guard('customer')->user()->company_id;
                        $deptBranchModel->department_id = $model->id;
                        $deptBranchModel->branch_id = $req->input('branch');
                        $deptBranchModel->save();
                        return response()->json(true);
                    }
                }
            } else {
                $sameData = Department::companyStatusCheck()->whereIn('id', $sameBranchDepartments)->where('name', $req->input('name'))->where('id', '<>', $req->input('hid'))->count();
                if ($sameData > 0) {
                    return response()->json(false);
                } else {
                    $model = Department::findOrNew($req->input('hid'));
                    $model->company_id = Auth::guard('customer')->user()->company_id;
                    $model->name = $req->input('name');
                    if ($model->save()) {
                        DepartmentBranch::where('department_id', $model->id)->delete();
                        $deptBranchModel = new DepartmentBranch;
                        $deptBranchModel->company_id = Auth::guard('customer')->user()->company_id;
                        $deptBranchModel->department_id = $model->id;
                        $deptBranchModel->branch_id = $req->input('branch');
                        $deptBranchModel->save();
                        return response()->json(true);
                    }
                }
            }
        }
    }

    public function departmentRemove(Request $req)
    {
        $child = Position::companyStatusCheck()->where('department_id', $req->input('id'))->count();
        if ($child > 0) {
            return response('child', 200)->header('Content-Type', 'text/plain');
        } else {
            $isTop = Department::find($req->input('id'))->is_top;
            if ($isTop == 1) {
                return response()->json(false);
            } else {
                Department::destroy($req->input('id'));
                return response()->json(true);
            }
        }
    }
}
