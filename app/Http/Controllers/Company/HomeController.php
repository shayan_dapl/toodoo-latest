<?php

namespace App\Http\Controllers\Company;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\CustomerAdminRequest;
use App\Models\Company;
use App\Models\CompanySettings;
use App\Models\Countries;
use App\Models\Language;
use App\Models\Process;
use App\Models\ReportedIssuesDetails;
use App\Models\Users;
use App\Models\ContextStakeholderActionLog;
use App\Models\RiskOpportunityActionLog;
use App\Models\SourceTypes;
use App\Models\TransactionDetails;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Mail;
use Session;
use View;
use Config;
use Excel;

use App\Repositories\Home as HomeRepo;

class HomeController extends Controller
{
    protected $repo;

    public function __construct(HomeRepo $homeRepo)
    {
        $this->repo = $homeRepo;
    }

    //For Main Dashboard Panel
    public function home()
    {
        $breadcrumbs = [
        '/home' => 'words.home',
        ];
        $backgroundColors = [];
        if (Auth::guard('web')->check()) {
            $companyRevenue = Company::where('id', '<>', 1)->where('status', 1)->sum('subscription_amount');

            $allUsers = Users::where('id', '<>', 1)->where('email', '<>', Config::get('settings.associate_user'))->whereNotIn('type', [1,4])->where('id', '<>', 1)->with('company')->get()->toArray();

            $trialActiveCompany = Company::where('id', '<>', 1)->where('is_trial', 1)->where('trial_end', '>=', date('Y-m-d'))->count();
            $subscribedActiveCompany = Company::where('id', '<>', 1)->where('is_trial', 0)->where('trial_end', '>=', date('Y-m-d'))->count();

            $trialInactiveCompany = Company::where('id', '<>', 1)->where('is_trial', 1)->where('trial_end', '<', date('Y-m-d'))->count();
            $subscribedInactiveCompany = Company::where('id', '<>', 1)->where('is_trial', 0)->where('trial_end', '<', date('Y-m-d'))->count();

            $trialActiveUser = 0;
            $subscribedActiveUser = 0;

            $trialInactiveUser = 0;
            $subscribedInactiveUser = 0;

            foreach ($allUsers as $eachUser) {
                if (($eachUser['company']['is_trial'] == 1) && ($eachUser['company']['trial_end'] >= date('Y-m-d')) && ($eachUser['out_of_service'] == '')) {
                    $trialActiveUser = $trialActiveUser + 1;
                } elseif (($eachUser['company']['is_trial'] == 0) && ($eachUser['company']['trial_end'] >= date('Y-m-d')) && ($eachUser['out_of_service'] == '')) {
                    $subscribedActiveUser = $subscribedActiveUser + 1;
                } elseif (($eachUser['company']['is_trial'] == 1) && (($eachUser['out_of_service'] != '') || ($eachUser['company']['trial_end'] < date('Y-m-d')))) {
                    $trialInactiveUser = $trialInactiveUser + 1;
                } elseif (($eachUser['company']['is_trial'] == 0)  && (($eachUser['out_of_service'] != '') || ($eachUser['company']['trial_end'] < date('Y-m-d')))) {
                    $subscribedInactiveUser = $subscribedInactiveUser + 1;
                }
            }
            $urgencyCount = 0;
            $activeCompany = 0;
            $inactiveCompany = 0;
            $internalPendingIssues = $unplannedAuditsNo = $meetingPending = $langArr = $extArr = $issueMonths = $openPieChart = $closedPieChart = $issueCategories = $legends = $backgroundColors = [];
        }

        if (Auth::guard('customer')->check()) {
            $companyId = Auth::guard('customer')->user()->company_id;
            $origins = SourceTypes::companyStatusCheck()->pluck('source_type', 'id');
            $activeCompany = Company::where('id', $companyId)->where('status', 1)->count();
            $inactiveCompany = Company::where('id', $companyId)->where('status', 2)->count();
            $unplannedAuditsNo = $this->repo->totalUnplannedAudits();
            $comingSevenDays = [date('Y-m-d'), date('Y-m-d', strtotime('+7 days'))];
            $pendingAuditee = $this->repo->pendingAuditsAditee(2, $comingSevenDays);
            $pendingAuditor = $this->repo->pendingAuditsAditor(2, $comingSevenDays);
            $meetingPending = $pendingAuditee->count() + $pendingAuditor->count();
            $internalPendingIssues = $this->repo->internalPendingIssues();
            $prevSixMonths = [date('Y-m-d', strtotime('-6 month')), date('Y-m-d')];
            $urgentIssues = $this->repo->urgentIssues();
            $veryUrgent = 0;
            $lessUrgent = 0;

            $trialActiveCompany = 0;
            $subscribedActiveCompany = 0;
            $trialInactiveCompany = 0;
            $subscribedInactiveCompany = 0;

            $trialActiveUser = 0;
            $subscribedActiveUser = 0;
            $trialInactiveUser = 0;
            $subscribedInactiveUser = 0;
            $companyRevenue = 0.00;

            if (!empty($urgentIssues)) {
                foreach ($urgentIssues as $eachUrgency) {
                    $findPending = 'none';
                    if ($eachUrgency->member_status != 'none') {
                        $memberStatus = explode(',', $eachUrgency->member_status);
                        $findPending = array_search(0, $memberStatus);
                    }
                    if ($findPending !== false) {
                        if (dayDifference($eachUrgency->created_at) < 1 && dayDifference($eachUrgency->created_at) > -8) {
                            $veryUrgent++;
                        }
                        if (dayDifference($eachUrgency->urgency) > 10) {
                            $lessUrgent++;
                        }
                    }
                }
            }
            $urgencyCount = CompanySettings::where('company_id', $companyId)->pluck('urgency_count')[0];
            $allOpenIssueData = $this->repo->openAndClosedIssues('all', 'open', $prevSixMonths);
            $allClosedIssueData = $this->repo->openAndClosedIssues('all', 'closed', $prevSixMonths);
            $issueBarChart = [];
            $issueBarChart['months'] = json_encode(array_keys($allOpenIssueData['months']));
            $issueBarChart['openIssues'] = json_encode(array_values($allOpenIssueData['months']));
            $issueBarChart['closedIssues'] = json_encode(array_values($allClosedIssueData['months']));
            $issueCategories = [
            "all" => translate('words.show_all'),
            "risk" => translate('words.risk'),
            "opportunity" => translate('words.opportunity'),
            "context" => translate('words.context'),
            "stakeholder" => translate('words.stakeholder'),
            "external_issues" => translate('words.external_issues'),
            "internal_issues" => translate('words.internal_issues'),
            "competent_ratings" => translate('words.competent_ratings'),
            "supplier_ratings" => translate('words.supplier_action'),
            "corrective_action" => translate('words.scar')
            ];
            $langArr = [
            'immidiate_action' => translate('words.immidiate_actions'),
            'root_cause' => translate('words.root_cause_analysis'),
            'corrective_action' => translate('words.corrective_action'),
            'effectiveness' => translate('words.effectiveness'),
            '1' => translate('table.not_fixed'),
            ];
            $extArr = [
            '1' => translate('table.on_going'),
            ];

            $openPieChart = $this->repo->issuesPieChart('open', $prevSixMonths);
            $closedPieChart = $this->repo->issuesPieChart('closed', $prevSixMonths);
        }

        return View::make('pages.home.dashboard', compact('breadcrumbs', 'activeCompany', 'inactiveCompany', 'internalPendingIssues', 'unplannedAuditsNo', 'urgentIssues', 'veryUrgent', 'urgencyCount', 'meetingPending', 'langArr', 'extArr', 'issueMonths', 'issueCategories', 'issueBarChart', 'openPieChart', 'closedPieChart', 'trialActiveCompany', 'subscribedActiveCompany', 'trialInactiveCompany', 'subscribedInactiveCompany', 'trialActiveUser', 'subscribedActiveUser', 'trialInactiveUser', 'subscribedInactiveUser', 'companyRevenue'));
    }

    //Making additional customer admin
    public function makeCustomerAdmin(Request $req)
    {
        $count = Users::companyCheck()->where('email', '<>', Config::get('settings.associate_user'))->where('type', 2)->count();
        if ($req->status == "on") {
            if (Users::find($req->id)->email == Config::get('settings.associate_user')) {
                Users::where('id', $req->id)->update(['type' => 2]);
                return response()->json(true);
            } else {
                if ($count > Config::get('settings.extra_customer_admin')) {
                    return response()->json(false);
                } else {
                    Users::where('id', $req->id)->update(['type' => 2]);
                    return response()->json(true);
                }
            }
        }
        if ($req->status == "off") {
            if ($count == Config::get('settings.extra_customer_admin')) {
                return response()->json(false);
            } else {
                Users::where('id', $req->id)->update(['type' => 3]);
                return response()->json(true);
            }
        }
    }

    public function getExport(Request $req)
    {
        $type = $req->type;
        Excel::create('Export data', function ($excel) use ($type) {
            $excel->sheet('Sheet 1', function ($sheet) use ($type) {
                if ($type == 'trial-active') {
                    $companies = Company::where('id', '<>', 1)->where('is_trial', 1)->where('trial_end', '>=', date('Y-m-d'))->get();
                } elseif ($type == 'trial-inactive') {
                    $companies = Company::where('id', '<>', 1)->where('is_trial', 1)->where('trial_end', '<', date('Y-m-d'))->get();
                } elseif ($type == 'subscribed-active') {
                    $companies = Company::where('id', '<>', 1)->where('is_trial', 0)->where('trial_end', '>=', date('Y-m-d'))->get();
                } elseif ($type == 'subscribed-inactive') {
                    $companies = Company::where('id', '<>', 1)->where('is_trial', 0)->where('trial_end', '<', date('Y-m-d'))->get();
                }
            
                $data = [];
                foreach ($companies as $eachCompany) {
                    $email = '';
                    if (!empty($eachCompany->customeradmins[0])) {
                        $customeradmin = $eachCompany->customeradmins[0]['name'];
                        $email = $eachCompany->customeradmins[0]['email'];
                    } else {
                        $customeradmin = '';
                        $email = '';
                    }
                    $address = '';
                    $tvaNo = '';
                    if ($eachCompany->street != '') {
                        $address .= $eachCompany->street .', ';
                    }
                    if ($eachCompany->nr != '') {
                        $address .= $eachCompany->nr .', ';
                    }
                    if ($eachCompany->bus != '') {
                        $address .= $eachCompany->bus .', ';
                    }
                    if ($eachCompany->zip != '') {
                        $address .= $eachCompany->zip .', ';
                    }
                    if ($eachCompany->city != '') {
                        $address .= $eachCompany->city;
                    }
                    if ($eachCompany->tva_no != '') {
                        $tvaNo = $eachCompany->tva_no;
                    }
                    $lastlogin = isset($eachCompany->log->in_time) ? $eachCompany->log->in_time : '';
                    $data[] = array(
                    $eachCompany->name,
                    $customeradmin,
                    $email,
                    $address,
                    $tvaNo,
                    $lastlogin
                );
                }
                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array(translate('table.company'), translate('words.customer'), translate('table.email'), translate('words.address'), translate('table.tva_no'), translate('table.last_login'));
                $sheet->prependRow(1, $headings);
            });
        })->export('xls');
    }
    public function showCompanies(Request $req)
    {
        $type = $req->type;
        if ($type == 'trial-active') {
            $companies = Company::where('id', '<>', 1)->where('is_trial', 1)->where('trial_end', '>=', date('Y-m-d'))->get();
            $modalheader = translate('words.trial_active_companies');
        } elseif ($type == 'trial-inactive') {
            $companies = Company::where('id', '<>', 1)->where('is_trial', 1)->where('trial_end', '<', date('Y-m-d'))->get();
            $modalheader = translate('words.trial_disabled_companies');
        } elseif ($type == 'subscribed-active') {
            $companies = Company::where('id', '<>', 1)->where('is_trial', 0)->where('trial_end', '>=', date('Y-m-d'))->get();
            $modalheader = translate('words.subscribed_active_companies');
        } elseif ($type == 'subscribed-inactive') {
            $companies = Company::where('id', '<>', 1)->where('is_trial', 0)->where('trial_end', '<', date('Y-m-d'))->get();
            $modalheader = translate('words.subscribed_disabled_companies');
        }
        return View::make('pages.home.companyList', compact('companies', 'modalheader', 'type'));
    }
    public function showInvoice(Request $req)
    {
        $breadcrumbs = [
        '/home' => 'words.home',
        ];
        $selectedCompany = '';
        if ($req->id != '') {
            $transactionDetail = TransactionDetails::where('company_id', $req->id)->where('status', 'paid')->get();
            $selectedCompany = $req->id;
        } else {
            $transactionDetail = TransactionDetails::where('status', 'paid')->get();
        }
       
        $companies = Company::where('id', '<>', 1)->pluck('name', 'id')->prepend(translate('form.select'), '');
        return View::make('pages.company.invoice_list', compact('breadcrumbs', 'transactionDetail', 'companies', 'selectedCompany'));
    }

    public function downloadInvoiceList(Request $req)
    {
        $fileName = '';
        if ($req->id != '') {
            $transactionDetail = TransactionDetails::where('company_id', $req->id)->where('status', 'paid')->get();
            $company = Company::find($req->id);
            $fileName = $company->name.'_'.date('d-m-Y');
        } else {
            $transactionDetail = TransactionDetails::where('status', 'paid')->get();
            $fileName = 'invoices_'.date('d-m-Y');
        }
        $invoice_txt = '<?xml version="1.0" encoding="utf-8"?>'."\n\r";
        $invoice_txt .= '<invoicelist>'."\n\r";
        if ($transactionDetail->count() > 0) {
            foreach ($transactionDetail as $invoice) {
                $invoice_txt .= '<invoice>'."\n\r";
                $invoice_txt .= '<invoicedate>' .date(Config::get('settings.dashed_date'), strtotime($invoice->payment_date)). '</invoicedate>'."\n\r";
                $invoice_txt .= '<invoiceno>' .$invoice->invoice_no. '</invoiceno>'."\n\r";
                $invoice_txt .= '<amount>€' .$invoice->amount. '</amount>'."\n\r";
                $invoice_txt .= '<vatamount>€' .$invoice->vat_amount. '</vatamount>'."\n\r";
                $invoice_txt .= '<totalamount>€' .$invoice->total_amount. '</totalamount>'."\n\r";
                $invoice_txt .= '<company>' .str_replace('&', '&amp;', $invoice->company->name). '</company>'."\n\r";
                $invoice_txt .= '<address>' .$invoice->invoicedata->street.', '.$invoice->invoicedata->nr.', '.$invoice->invoicedata->bus. '</address>'."\n\r";
                $invoice_txt .= '<zip>' .$invoice->invoicedata->zip. '</zip>'."\n\r";
                $invoice_txt .= '<city>' .$invoice->invoicedata->city. '</city>'."\n\r";
                $invoice_txt .= '<country>' .$invoice->company->countries->name_en. '</country>'."\n\r";
                $invoice_txt .= '<tva>' .$invoice->invoicedata->tva_no. '</tva>'."\n\r";
                $invoice_txt .= '</invoice>'."\n\r";
            }
        }
        $invoice_txt .= '</invoicelist>'."\n\r";
        header('Content-type: text/xml');
        header('Content-Disposition: attachment; filename="'.$fileName.'.xml"');
        return $invoice_txt;
    }
}
