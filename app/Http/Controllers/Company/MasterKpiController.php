<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
use App\Http\Requests\MasterKpiRequest;
use App\Models\KpiUnit;
use App\Models\MasterKpi;
use App\Models\MasterKpiData;
use App\Models\ProcessKpiData;
use Auth;
use View;
use Config;

class MasterKpiController extends Controller
{
    public function index()
    {
        $breadcrumbs = [
            '/master-kpi/list' => 'words.master_kpi',
            '#' => 'words.list',
        ];
        $masterKpis = MasterKpi::companyStatusCheck()->get();
        $boolOptions = ['' => translate('form.select'), '1' => translate('words.true'), '0' => translate('words.false')];
        $trendOptions = ['' => translate('form.select'), '1' => translate('words.up'), '0' => translate('words.down')];
        $frequency = ['' => translate('form.select'), 'daily' => translate('words.daily'),'weekly' => translate('words.weekly'), 'monthly' => translate('words.monthly'), 'quarterly' => translate('words.quarterly')];
        $langArr = Config::get('settings.lang');
        $kpiUnitName = 'name_' . $langArr[session('lang')];
        $kpiUnits = KpiUnit::all()->pluck($kpiUnitName, 'id')->prepend(translate('form.select'), '');

        return View::make('pages.master_kpi.index', compact('breadcrumbs', 'masterKpis', 'frequency', 'kpiUnits', 'boolOptions', 'trendOptions'));
    }

    public function data()
    {
        $data = MasterKpi::companyStatusCheck()->with('kpiunit')->get();
        return response()->json($data);
    }

    public function masterKpiSave(MasterKpiRequest $req)
    {
        if (!empty($req->input())) {
            $target = '';
            if ($req->input('target') != '') {
                $target = $req->input('target');
            } elseif ($req->input('targetBool') != '') {
                $target = $req->input('targetBool');
            } else {
                $target = $req->input('targetTrend');
            }
            $model = MasterKpi::findOrNew($req->input('hid'));
            $model->company_id = Auth::user()->company_id;
            $model->name = $req->input('kpiName');
            $model->valid_from = date(Config::get('settings.db_date'), strtotime($req->input('validFrom')));
            $model->valid_to = date(Config::get('settings.db_date'), strtotime($req->input('validTo')));
            $model->frequency = $req->input('frequency');
            $model->kpi_unit_id = $req->input('kpiUnit');
            $model->target = $target;
            $model->status = 1;
            if ($model->save()) {
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }

    public function masterKpiRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = MasterKpi::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return response()->json(false);
            }
            $model->status = 3;
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return response()->json(true);
            }
        }
    }

    public function masterKpiDataList(Request $req)
    {
        $breadcrumbs = [
            '/master-kpi/list' => 'words.master_kpi',
            '#' => 'words.list',
        ];
        $details = MasterKpi::companyCheck()->where('id', $req->id)->first();
        if (!checkCompanyAccess($details)) {
            $req->session()->flash('error', translate('words.you_dont_have_access'));
            return redirect('/home');
        }
        $masterKpiData = MasterKpiData::where('master_kpi_id', $req->id)->where('status', 1)->get();
        $frequency = ['' => translate('form.select'), 'daily' => translate('words.daily'), 'weekly' => translate('words.weekly'), 'monthly' => translate('words.monthly'), 'quarterly' => translate('words.quarterly')];
        $langArr = Config::get('settings.lang');
        $kpiUnitName = 'name_' . $langArr[session('lang')];
        $kpiUnits = KpiUnit::all()->pluck($kpiUnitName, 'id')->prepend(translate('form.select'), '');
        $boolOptions = ['' => translate('form.select'), '1' => translate('words.true'), '0' => translate('words.false')];
        $trendOptions = ['' => translate('form.select'), '1' => translate('words.up'), '0' => translate('words.down')];
        $labelArr = [];
        $dataArr = [];
        if (!empty($masterKpiData)) {
            foreach ($masterKpiData as $eachData) {
                $labelArr[] = date(Config::get('settings.dashed_date'), strtotime($eachData->created_at));
                $dataArr[] = $eachData->kpi_value;
            }
        }
        $labelArr = json_encode($labelArr);
        $dataArr = json_encode($dataArr);
        return View::make('pages.master_kpi.master_kpi_data_list', compact('breadcrumbs', 'masterKpiData', 'details', 'frequency', 'kpiUnits', 'boolOptions', 'trendOptions', 'labelArr', 'dataArr'));
    }

    public function masterKpiData(Request $req)
    {
        $data = MasterKpiData::where('master_kpi_id', $req->id)->where('status', 1)->get();
        return response()->json($data);
    }

    public function masterKpiGraph(Request $req)
    {
        $details[] = ['label' => [], 'detail' => []];
        $label = [];
        $detail = [];
        $data = MasterKpiData::where('master_kpi_id', $req->id)->where('status', 1)->orderBy('period', 'ASC')->get();
        if (!empty($data)) {
            foreach ($data as $eachData) {
                $label[] = $eachData->period;
                $detail[] = $eachData->kpi_value;
            }
        }
        $details['label'] = $label;
        $details['detail'] = $detail;
        return response()->json($details);
    }

    public function masterKpiDataSave(Request $req)
    {
        if (!empty($req->input())) {
            $kpiVal = '';
            if ($req->input('target') != '') {
                $kpiVal = $req->input('target');
            } elseif ($req->input('targetBool') != '') {
                $kpiVal = $req->input('targetBool');
            } else {
                $kpiVal = $req->input('targetTrend');
            }
            $model = MasterKpiData::findOrNew($req->input('hid'));
            $model->company_id = Auth::user()->company_id;
            $model->master_kpi_id = $req->input('masterKpiId');
            $model->kpi_value = $kpiVal;
            $model->period = $req->input('period');
            $model->status = 1;
            if ($model->save()) {
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        }
    }

    public function masterKpiDataRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = MasterKpiData::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return response()->json(false);
            }
            $model->status = 3;
            if ($model->save()) {
                return response()->json(true);
            }
        }
    }

    public function kpiRelationGraph()
    {
        $breadcrumbs = [
            '/master-kpi/list' => 'words.master_kpi',
        ];
        $masterKpis = MasterKpi::companyStatusCheck()->get();
        return View::make('pages.master_kpi.kpi_relation_graph', compact('breadcrumbs', 'masterKpis'));
    }

    public function restrict(Request $req)
    {
        $restricted = [];
        $startTime = strtotime($req->input('from'));
        $endTime = strtotime($req->input('to'));
        if ($req->input('segment') == 'master') {
            $existing = MasterKpiData::companyStatusCheck()->where('master_kpi_id', $req->input('id'))->pluck('period')->toArray();
        } else {
            $existing = ProcessKpiData::companyStatusCheck()->where('kpi_id', $req->input('id'))->pluck('period')->toArray();
        }
        $immidiateNext = 0;
        switch ($req->input('type')) {
            case 'weekly':
                while ($startTime < $endTime) {
                    if ($immidiateNext == 0) {
                        if (in_array(date('Y-W', $startTime), $existing)) {
                            $restricted[] = date('Y-W', $startTime);
                        }
                        if (!in_array(date('Y-W', $startTime), $existing)) {
                            $immidiateNext = 1;
                        }
                    } else {
                        $restricted[] = date('Y-W', $startTime);
                    }
                    $startTime += strtotime('+1 week', 0);
                }
                break;
            case 'monthly':
                while ($startTime < $endTime) {
                    if ($immidiateNext == 0) {
                        if (in_array(date('Y-m', $startTime), $existing)) {
                            $restricted[] = date('Y-m', $startTime);
                        }
                        if (!in_array(date('Y-m', $startTime), $existing)) {
                            $immidiateNext = 1;
                        }
                    } else {
                        $restricted[] = date('Y-m', $startTime);
                    }
                    $startTime += strtotime('+1 month', 0);
                }
                break;
            case 'quarterly':
                while ($startTime < $endTime) {
                    $year = date('Y', $startTime);
                    $month = date('n', $startTime);
                    $quarter = $year .'-'. ceil($month / 3);
                    if ($immidiateNext == 0) {
                        if (in_array($quarter, $existing)) {
                            $restricted[] = $quarter;
                        }
                        if (!in_array($quarter, $existing)) {
                            $immidiateNext = 1;
                        }
                    } else {
                        $restricted[] = $quarter;
                    }
                    $startTime = strtotime('+3 months', $startTime);
                }
                break;
        }
        return $restricted;
    }
}
