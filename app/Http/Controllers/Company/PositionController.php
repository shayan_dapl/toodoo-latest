<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
use App\Http\Requests\PositionRequest;
use App\Models\Department;
use App\Models\Language;
use App\Models\OrganizationStructure;
use App\Models\Position;
use App\Models\Users;
use App\Models\Company;
use App\Models\Branch;
use App\Models\DepartmentBranch;
use App\Models\PositionBranches;
use Auth;
use Excel;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
use Session;
use View;
use Config;
use Mail;
use App\Mail\TodoMailSend;

class PositionController extends Controller
{
    public function positionList()
    {
        $breadcrumbs = [
            '/position/list' => 'words.position',
            '#' => 'words.list',
        ];
        $positions = Position::companyStatusCheck()->select('id', 'department_id', 'name', 'parent_id', 'is_special_position', 'is_assistant', 'status')->get();
        $treeStyle = makeTree($positions);
        $positions = structureTree($treeStyle);
        return View::make('pages.company.position_list', compact('breadcrumbs', 'positions'));
    }

    public function departmentPositions(Request $req)
    {
        if (!empty($req->id) && $req->id != 0) {
            $parent = ['' => translate('form.select')];
            $data = Department::find($req->id)->positions()->where('is_assistant', 0)->where('is_special_position', 0)->where('status', 1)->orderBy('id');
            $parentRequired = '';
            if ($data->count() >= 1) {
                foreach ($data->get() as $element) {
                    if ($req->hidpos != 0) {
                        $editPosBranchId = Position::find($req->hidpos)->branches->branch_id;
                        if ($editPosBranchId == $element->branches->branch_id) {
                            if ($req->hidpos != $element->id && $req->hidpos != $element->parent_id) {
                                $children = makeTree($data->get(), $element->id);
                                if ($children) {
                                    $parent[$element->id] = $element->name;
                                }
                            }
                        } else {
                            $parent[$element->id] = $element->name;
                        }
                    } else {
                        $parent[$element->id] = $element->name;
                    }
                }
                $parentRequired = 'required';
            }
        } else {
            $parent = ['' => translate('form.select')];
            $parentRequired = 'required';
        }
        return View::make('pages.company.position_by_department', compact('parent', 'parentRequired'));
    }

    public function positionForm(Request $req)
    {
        $breadcrumbs = [
            '/position/list' => 'words.position',
            '#' => 'words.add',
        ];
        $branches = Branch::companyCheck()->get()->pluck('name', 'id')->toArray();
        $deptBranches = DepartmentBranch::where('branch_id', array_keys($branches)[0])->get()->pluck('department_id')->toArray();
        $department = Department::companyCheck()->whereIn('id', $deptBranches)->get()->toJson();
        $parent = ['' => translate('form.select')];
        $users = Users::nonAssociatedUser();
        $specialUsers = Users::activeSpecialUsers()->get();
        $positionCount = Position::companyStatusCheck()->count();
        $language = Language::all()->pluck('display_text', 'name')->prepend(translate('form.select'), '');
        return View::make('pages.company.position_form', compact('breadcrumbs', 'department', 'parent', 'users', 'specialUsers', 'language', 'branches'));
    }

    public function positionFormSave(PositionRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $createdBy = empty($req->input('hid')) ? Auth::guard('customer')->user()->id : 0;
        $updatedBy = !empty($req->input('hid')) ? Auth::guard('customer')->user()->id : 0;
        $trialUserCount = Users::companyStatusCheck()->inServiceWithEmail()->where('email', '<>', Config::get('settings.associate_user'))->count();
        $endLimit = (Auth::guard('customer')->user()->company->is_trial == 1) ? Config::get('settings.trial_user_limit') : Auth::guard('customer')->user()->company->user_allowed;
        $flashMsg = (Auth::guard('customer')->user()->company->is_trial == 1) ? translate('error.trial_user_limit') : (Auth::guard('customer')->user()->company->user_allowed .' '. translate('error.package_user_limit'));
        $model = Position::findOrNew($req->input('hid'));
        $model->company_id = Auth::guard('customer')->user()->company_id;
        $model->department_id = $req->input('department_id');
        $model->name = $req->input('name');
        $model->parent_id = (!empty($model->exists) && $model->parent_id == null) ? null : $req->input('parent_id');
        $model->info = $req->input('info');
        $model->is_assistant = ($req->input('assistant') != null) ? 1: 0;
        if (!empty($model->exists) && $model->is_special_position == 1 && $req->input('is_special_position') == null) {
            OrganizationStructure::companyCheck()->where('position_id', $model->id)->delete();
        }
        $model->is_special_position = ($req->input('is_special_position') != null) ? 1: 0;
        $model->status = 1;
        $model->created_by = $createdBy;
        $model->updated_by = $updatedBy;
        if ($model->save()) {
            if (!empty($req->input('branch'))) {
                if (!empty($model->exists)) {
                    PositionBranches::where('position_id', $model->id)->delete();
                }
                $branches = new PositionBranches;
                $branches->company_id = $model->company_id;
                $branches->position_id = $model->id;
                $branches->branch_id = $req->input('branch');
                $branches->save();
            }
            $user = "";
            if (!empty($req->input('user_normal')) || !empty($req->input('user_special'))) {
                $user = !empty($req->input('user_normal')) ? $req->input('user_normal') : $req->input('user_special');
                if (!empty($req->input('user_special'))) {
                    $specialUserExists = OrganizationStructure::where('user_id', $req->input('user_special'))->first();
                    if (!empty($specialUserExists)) {
                        $user = "";
                    }
                }
            }
            if (!empty($req->input('manage_normal_user')) || (!empty($req->input('manage_special_user')) && ($req->input('special_user_check') == 'special_single'))) {
                $inviteUsrToken = rand('1000', '9999').time();
                $fname = (!empty($req->input('manage_special_user')) && ($req->input('special_user_check') == 'special_single')) ? $req->input('special_first_name') : $req->input('fname');
                $lname = (!empty($req->input('manage_special_user')) && ($req->input('special_user_check') == 'special_single')) ? $req->input('special_last_name') : $req->input('last_name');
                $email = (!empty($req->input('manage_special_user')) && ($req->input('special_user_check') == 'special_single')) ? $req->input('special_email') : $req->input('email');
                $photo = !empty($req->input('photo')) ? $req->input('photo') : $req->input('special_photo');
                $language = (!empty($req->input('manage_special_user')) && ($req->input('special_user_check') == 'special_single')) ? $req->input('special_language') : $req->input('language');
                $auditor = (!empty($req->input('manage_special_user')) && ($req->input('special_user_check') == 'special_single')) ? $req->input('is_special_auditor') : $req->input('is_auditor');
                $owner = (!empty($req->input('manage_special_user')) && ($req->input('special_user_check') == 'special_single')) ? $req->input('can_special_process_owner') : $req->input('can_process_owner');
                if (!empty($email) && $trialUserCount >= $endLimit) {
                    $req->session()->flash('error', $flashMsg);
                    return redirect('/position/list');
                }
                $masterModel = new Users;
                $masterModel->company_id = Auth::guard('customer')->user()->company_id;
                $masterModel->name = ($fname . ' ' . $lname);
                $masterModel->fname = $fname;
                $masterModel->lname = $lname;
                if (!empty($photo)) {
                    $extension = explode('/', explode(':', substr($photo, 0, strpos($photo, ';')))[1])[1];
                    if (in_array(strtolower($extension), Config::get('settings.img_extentions'))) {
                        $filename = 'User-' . time() . '.' . $extension;
                        $path = storage_path('app/public/company/' . $masterModel->company_id . '/image/users/' . $filename);
                        Image::make($photo)->resize(100, 100)->save($path);
                        $masterModel->photo = $filename;
                    }
                }
                $masterModel->language = $language;
                $masterModel->email = ($req->input('no_email') == null) ? $email : null;
                $masterModel->remember_token = $req->input('_token');
                $masterModel->type = 3;
                $masterModel->invite_user_token = $inviteUsrToken;
                $masterModel->is_auditor = ($auditor != null) ? 1 : 0;
                $masterModel->can_process_owner = ($owner != null) ? 1 : 0;
                $masterModel->is_top_person = ($model->parent_id == null) ? 1 : 0;
                $masterModel->no_email = ($req->input('no_email') != null || $req->input('special_no_email') != null) ? 1 : 0;
                if (!empty($req->input('manage_special_user')) && $req->input('special_user_check') == 'special_single') {
                    $masterModel->is_special_user = 1;
                }
                $masterModel->created_by = $createdBy;
                $masterModel->updated_by = $updatedBy;
                if ($masterModel->save()) {
                    $user = $masterModel->id;
                }
            }
            if (!empty($user)) {
                $userToOrg = new OrganizationStructure;
                $userToOrg->company_id = Auth::guard('customer')->user()->company_id;
                $userToOrg->user_id = $user;
                $userToOrg->department_id = $model->department_id;
                $userToOrg->position_id = $model->id;
                $userToOrg->assistant = ($req->input('assistant') != null) ? 1 : 0;
                $userToOrg->assist_as = $req->input('name');
                $userToOrg->special_user = ($req->input('is_special_position') != null) ? 1 : 0;
                if (OrganizationStructure::where('position_id', $model->id)->count() > 0) {
                    $existOrgStruct = OrganizationStructure::where('position_id', $model->id)->first();
                    $existOrgStruct->user_id = $user;
                    $existOrgStruct->department_id = $model->department_id;
                    $existOrgStruct->special_user = $userToOrg->special_user;
                    $existOrgStruct->save();
                } else {
                    $userToOrg->save();
                }
            } elseif (!empty($model->exists) && empty($user)) {
                $hasChild = 0;
                $childPositions = $model->children;
                if ($childPositions->count() > 0) {
                    foreach ($childPositions as $each) {
                        if (OrganizationStructure::where('position_id', $each->id)->count() > 0) {
                            $hasChild++;
                        }
                    }
                }
                if ($hasChild == 0) {
                    OrganizationStructure::where('position_id', $model->id)->delete();
                } else {
                    $req->session()->flash('error', translate('error.child_user_exists'));
                }
            }
            if (empty($req->input('hid')) && !empty($req->input('manage_special_user')) && $req->input('special_user_check') == 'special_multiple') {
                if (!empty($req->file('doc_file'))) {
                    $extension = File::extension($req->file('doc_file')->getClientOriginalName());
                    if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
                        $userPreview['position'] = ['id' => $model->id, 'name' => $model->name, 'department_id' => $model->department_id];
                        $langArr = array_keys(Config::get('settings.lang'));
                        $path = $req->file('doc_file')->getRealPath();
                        $data = Excel::load($path, function ($reader) {
                        })->get();
                        if (!empty($data) && $data->count()) {
                            foreach ($data as $key => $value) {
                                $remark = null;
                                if (!empty($value->firstname) && !empty($value->lastname)) {
                                    if ($value->emailoptional != null) {
                                        if (filter_var(trim($value->emailoptional), FILTER_VALIDATE_EMAIL)) {
                                            if (Users::companyStatusCheck()->inServiceWithEmail()->where('email', $value->emailoptional)->count() == 0) {
                                                if ($trialUserCount >= $endLimit) {
                                                    $remark = $flashMsg;
                                                }
                                                $trialUserCount++;
                                            } else {
                                                $remark = translate('form.email_exists');
                                            }
                                        } else {
                                            $remark = translate('error.wrong_email_format');
                                        }
                                    }
                                    $userPreview['users'][] = ['fname' => $value->firstname, 'lname' => $value->lastname, 'language' => in_array($value->languagenlenfr, $langArr) ? $value->languagenlenfr : 'nl', 'email' => !empty($value->emailoptional) ? trim($value->emailoptional) : null, 'remark' => $remark];
                                }
                            }
                            $breadcrumbs = ['/position/list' => 'words.position', '#' => 'words.user_preview'];
                            return View::make('pages.company.position_preview', compact('breadcrumbs', 'userPreview'));
                        }
                    }
                }
            }
        }
        $req->session()->flash('success', translate('words.success_message'));
        return redirect('/position/list');
    }

    public function positionSpecialSave(Request $req)
    {
        if (!empty($req->input('data'))) {
            $data = json_decode($req->input('data'));
            $nullCheck = 0;
            if ($data !== false && !empty($data->users)) {
                foreach ($data->users as $each) {
                    if ($each->remark == null) {
                        if (Users::companyCheck()->inServiceWithEmail()->where('email', $each->email)->count() == 0) {
                            $nullCheck++;
                            $specialUser = new Users;
                            $specialUser->company_id = Auth::guard('customer')->user()->company_id;
                            $specialUser->name = ($each->fname . ' ' . $each->lname);
                            $specialUser->fname = $each->fname;
                            $specialUser->lname = $each->lname;
                            $specialUser->language = $each->language;
                            $specialUser->email = filter_var($each->email, FILTER_VALIDATE_EMAIL) ? $each->email : null;
                            $specialUser->no_email = !empty($each->email) ? 0 : 1;
                            $specialUser->remember_token = $req->input('_token');
                            $specialUser->type = 3;
                            $specialUser->invite_user_token = rand('1000', '9999').time();
                            $specialUser->is_special_user = 1;
                            $specialUser->created_by = Auth::guard('customer')->user()->id;
                            $specialUser->updated_by = 0;
                            if ($specialUser->save()) {
                                $userToOrg = new OrganizationStructure;
                                $userToOrg->company_id = Auth::guard('customer')->user()->company_id;
                                $userToOrg->user_id = $specialUser->id;
                                $userToOrg->department_id = $data->position->department_id;
                                $userToOrg->position_id = $data->position->id;
                                $userToOrg->special_user = 1;
                                $userToOrg->save();
                            }
                        }
                    }
                }
                if ($nullCheck == 0) {
                    $req->session()->flash('error', translate('alert.no_user_added_on_special'));
                    return redirect('/position/list');
                } else {
                    $req->session()->flash('success', translate('words.success_message'));
                    return redirect('/position/list');
                }
            } else {
                $req->session()->flash('error', translate('alert.no_user_added_on_special'));
                return redirect('/position/list');
            }
        } else {
            $req->session()->flash('error', translate('alert.no_user_added_on_special'));
            return redirect('/position/list');
        }
    }

    public function positionEdit(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/position/list' => 'words.position',
                '#' => 'words.edit',
            ];
            $details = Position::find($req->id);
            if (!checkCompanyAccess($details)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $selectedBranches = $details->branches->branch_id;
            $branches = Branch::companyCheck()->get()->pluck('name', 'id')->toArray();
            $deptBranches = DepartmentBranch::where('branch_id', $selectedBranches)->get()->pluck('department_id')->toArray();
            $department = Department::companyCheck()->whereIn('id', $deptBranches)->get()->toJson();
            $parent = ['' => translate('form.select')];
            $data = Department::find($details->department_id)->positions()->where('is_assistant', 0)->where('is_special_position', 0)->where('status', 1)->orderBy('id');
            if ($data->count() >= 1) {
                foreach ($data->get() as $element) {
                    if ($details->id != $element->id && $details->id != $element->parent_id) {
                        $children = makeTree($data->get(), $element->id);
                        if ($children) {
                            $parent[$element->id] = $element->name;
                        }
                    }
                }
            }
            $users = Users::companyCheck()->where('out_of_service', null)->where('is_special_user', 0)->get();
            $specialUsers = Users::activeSpecialUsers()->get();
            $positionCount = Position::companyStatusCheck()->count();
            $language = Language::all()->pluck('display_text', 'name')->prepend(translate('form.select'), '');
            $holdingUserCount = OrganizationStructure::where('company_id', $details->company_id)->where('position_id', $req->id);
            if ($holdingUserCount->count() > 0) {
                $holdingUser = $holdingUserCount->first()->user_id;
            } else {
                $holdingUser = '';
            }
            return View::make('pages.company.position_form', compact('breadcrumbs', 'details', 'department', 'parent', 'users', 'specialUsers', 'language', 'branches', 'selectedBranches', 'holdingUser'));
        } else {
            return response()->json(false);
        }
    }

    public function positionRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = Position::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            } else {
                $foundChild = $model->children->count();
                if ($foundChild > 0) {
                    $req->session()->flash('error', translate('error.child_position_exists'));
                } else {
                    $inOrgStruct = $model->orgExistance->count();
                    if ($inOrgStruct > 0) {
                        OrganizationStructure::where('position_id', $req->id)->delete();
                    }
                    Position::where('id', $req->id)->delete();
                    $req->session()->flash('success', translate('words.success_message'));
                }
                return redirect("/position/list");
            }
        }
    }

    public function positionRearrange(Request $req)
    {
        if (!empty($req->child) && !empty($req->parent)) {
            if ($req->parent == 'undefined') {
                $data = Position::companyStatusCheck()->where('id', $req->child)->first();
                $branch = $data->branches->branch_id;
                $department = $data->department_id;
                if (Position::companyStatusCheck()->where('department_id', $department)->where('parent_id', null)->count() == 0) {
                    //Setting new parent child set
                    $parent = ($req->parent == 'undefined') ? null : $req->parent;
                    Position::companyStatusCheck()->where('id', $req->child)->update(['parent_id' => $parent]);
                    return response()->json(true);
                } else {
                    return response()->json(false);
                }
            } else {
                $ifChildHasParent = Position::find($req->child)->parent_id;
                if ($ifChildHasParent != null) {
                    $childBranchId = Position::find($req->child)->branches->branch_id;
                    $currentParentBranchId = Position::find($ifChildHasParent)->branches->branch_id;
                    $newParentBranchId = Position::find($req->parent)->branches->branch_id;
                    if ($currentParentBranchId == $newParentBranchId || $childBranchId == $newParentBranchId) {
                        //Setting new parent child set
                        Position::companyStatusCheck()->where('id', $req->child)->update(['parent_id' => $req->parent]);
                        return response()->json(true);
                    } else {
                        return response()->json(false);
                    }
                } else {
                    //Setting new parent child set
                    Position::companyStatusCheck()->where('id', $req->child)->update(['parent_id' => $req->parent]);
                    return response()->json(true);
                }
            }
        } else {
            return response()->json(false);
        }
    }
}
