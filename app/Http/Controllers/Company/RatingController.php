<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\Rating;
use Auth;
use App\Http\Requests\RatingRequest;

class RatingController extends Controller
{
    public function rating()
    {
        $breadcrumbs = [
            '/rating/list' => 'words.rating',
        ];

        return View::make('pages.company.rating', compact('breadcrumbs'));
    }

    public function ratingData()
    {
        $ratingData = Rating::companyStatusCheck()->get()->toArray();
        return response()->json($ratingData);
    }

    public function ratingSave(RatingRequest $req)
    {
        if (!empty($req->input())) {
            $sameData = Rating::companyStatusCheck()->where('name', $req->input('name'));
            if (!empty($req->input('hid'))) {
                $sameData = $sameData->where('id', '<>', $req->input('hid'));
            }
            if ($sameData->count() > 0) {
                return response()->json(false);
            } else {
                $model = Rating::findOrNew($req->input('hid'));
                $model->company_id = Auth::guard('customer')->user()->company_id;
                $model->name = $req->input('name');
                $model->description = $req->input('desc');
                if ($model->save()) {
                    return response()->json(true);
                }
            }
        }
    }

    public function ratingRemove(Request $req)
    {
        Rating::destroy($req->input('id'));
        return response()->json(true);
    }
}
