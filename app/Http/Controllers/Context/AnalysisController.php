<?php

namespace App\Http\Controllers\Context;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContextAnalysisRequest;
use App\Models\ContextAnalysis;
use App\Models\ContextSubjects;
use App\Models\Process;
use App\Models\Users;
use App\Models\ContextStakeholderActionLog;
use App\Models\ContextStakeholderActionComments;
use App\Models\ContextStakeholderActionFiles;
use App\Models\Branch;
use App\Models\ContextBranch;
use App\Models\ProcessBranch;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;

class AnalysisController extends Controller
{
    public function contextAnalysisForm()
    {
        $breadcrumbs = [
            '#' => 'words.context_analysis',
        ];
        $subjects = ContextSubjects::companyCheck()->pluck('subject', 'id')->prepend(translate('form.select'), '');
        $process = Process::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $users = Users::companyStatusCheck()->inServiceWithEmail()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $allContexts = ContextAnalysis::companyCheck()->where('status', '<>', 3)->with('subjectname', 'powner', 'process')->get();
        $branches = Branch::companyStatusCheck()->pluck('name', 'id');
        if ($branches->count() == 1) {
            foreach ($branches as $eachBranch=>$curBranch) {
                $selectedBranches [] = $eachBranch;
            }
        } else {
            $selectedBranches = [];
        }
        return View::make('pages.context.context_analysis_form', compact('subjects', 'process', 'users', 'allContexts', 'breadcrumbs', 'branches', 'selectedBranches'));
    }

    public function contextAnalysisSave(ContextAnalysisRequest $req)
    {
        if (!empty($req->input())) {
            if (!empty($req->input('hid'))) {
                $model = ContextAnalysis::find($req->input('hid'));
            } else {
                $model = new ContextAnalysis;
            }

            $createdBy = !empty($req->input('hid')) ? $model->created_by : Auth::guard('customer')->user()->id;
            $updatedBy = !empty($req->input('hid')) ? Auth::guard('customer')->user()->id : 0;

            $model->process_id = $req->input('process_id');
            $model->company_id = Auth::guard('customer')->user()->company_id;
            $model->issue = $req->input('issue');
            $model->subject = $req->input('subject');
            $model->issue_type = $req->input('issue_type');
            $model->owner = $req->input('owner');
            $model->monitoring_info = $req->input('monitoring_info');
            $model->possible_influence = $req->input('possible_influence');
            $model->impact = $req->input('impact');
            $model->risk_or_opportunity = $req->input('risk');
            $model->description = $req->input('description');
            $model->status = 1;
            $model->created_by = $createdBy;
            $model->updated_by = $updatedBy;

            if ($model->save()) {
                ContextBranch::where('context_id', $model->id)->delete();
                foreach ($req->input('branches') as $eachBranch) {
                    $contextBranchModel = new ContextBranch;
                    $contextBranchModel->context_id = $model->id;
                    $contextBranchModel->branch_id =  $eachBranch;
                    $contextBranchModel->save();
                }
                if ($req->input('action_yes_no') == "Yes") {
                    $action = new ContextStakeholderActionLog;
                    $action->company_id = Auth::guard('customer')->user()->company_id;
                    $action->type = "context";
                    $action->contxt_stakehold_id = $model->id;
                    $action->action = $req->input('action');
                    $action->deligate_to = $req->input('who');
                    $action->deadline = $req->input('deadline');
                    $action->save();
                }
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('context/context-analysis-form');
            }
        }
    }

    public function contextAnalysisEdit(Request $req)
    {
        $breadcrumbs = [
            '#' => 'words.context_analysis',
        ];
        $details = ContextAnalysis::find($req->id);
        if (!checkCompanyAccess($details)) {
            $req->session()->flash('error', translate('words.you_dont_have_access'));
            return redirect('/home');
        }
        $selectedBranches = [];
        foreach ($details->branches as $branch) {
            $selectedBranches [] = $branch->branch_id;
        }
        $subjects = ContextSubjects::companyCheck()->pluck('subject', 'id')->prepend(translate('form.select'), '');
        $process = Process::companyCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $users = Users::companyCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $allContexts = ContextAnalysis::companyCheck()->where('status', '<>', 3)->with('subjectname')->with('powner')->with('process')->get();
        $branches = Branch::companyStatusCheck()->pluck('name', 'id');
        $contextActionLog = ContextStakeholderActionLog::where('contxt_stakehold_id', $req->id)->where('type', 'context')->get();
        return View::make('pages.context.context_analysis_form', compact('details', 'subjects', 'process', 'users', 'allContexts', 'breadcrumbs', 'branches', 'selectedBranches', 'contextActionLog'));
    }

    public function contextAnalysisDelete(Request $req)
    {
        if (!empty($req->id)) {
            $model = ContextAnalysis::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $model->status = 3;
            $model->updated_by = Auth::guard('customer')->user()->id;
            if ($model->save()) {
                $actionlogs = ContextStakeholderActionLog::where('contxt_stakehold_id', $req->id)->where('type', 'context')->pluck('id');
                ContextStakeholderActionComments::where('type', 'context')->whereIn('log_id', $actionlogs)->delete();
                ContextStakeholderActionLog::where('contxt_stakehold_id', $req->id)->where('type', 'context')->delete();
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('context/context-analysis-form');
            }
        }
    }

    public function issueFixing(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '#' => 'words.action',
            ];
            $log = ContextStakeholderActionLog::find($req->id);

            return View::make('pages.context.context_stakeholder_issue_fixing', compact('breadcrumbs', 'log'));
        }
    }

    public function issueFixingSave(Request $req)
    {
        if (!empty($req->input())) {
            //Comment adding
            if (!empty($req->input('reply'))) {
                $comment = new ContextStakeholderActionComments;
                $comment->company_id = Auth::guard('customer')->user()->company_id;
                $comment->log_id = $req->input('action_log_id');
                $comment->type = $req->input('type');
                $comment->reply = $req->input('reply');
                $comment->save();
            }

            //Supporting document uploading per comment
            if (!empty($req->file('upload_doc'))) {
                foreach ($req->file('upload_doc') as $index => $file) {
                    $filename = time() . '-' . $file->getClientOriginalName();
                    $file->move(
                        storage_path('app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents/con_stake_action_docs/'),
                        $filename
                    );
                    $fileModel = new ContextStakeholderActionFiles;
                    $fileModel->comment_id = $comment->id;
                    $fileModel->document_name = $filename;
                    $fileModel->submitted_by = Auth::guard('customer')->user()->id;
                    $fileModel->save();
                }
            }

            //Action log closed
            if ($req->input('closed')) {
                ContextStakeholderActionLog::where('id', $req->input('action_log_id'))->update(['closed' => 1, 'closed_at' => date('Y-m-d')]);
            }

            $req->session()->flash('success', translate('words.success_message'));
            return redirect('context-stakeholder-fixing/' . $req->input('action_log_id'));
        }
    }

    public function checkProcessBranch(Request $req)
    {
        if (!empty($req->input('selectedBranch'))) {
            $selectedBranch = json_decode($req->input('selectedBranch'), true);
            $getProcessids = ProcessBranch::companyCheck()->whereIn('branch_id', $selectedBranch)->groupBy('process_id')->pluck('process_id')->toArray();
            $filteredProcessData = Process::companyStatusCheck()->whereIn('id', $getProcessids)->pluck('name', 'id')->prepend(translate('form.select'), '')->toArray();
            
            $filteredProcess = [];
            $i = 0;
            foreach ($filteredProcessData as $key=>$val) {
                $filteredProcess [$i]['id'] = $key;
                $filteredProcess [$i]['text'] = $val;
                $i++;
            }
            return $filteredProcess;
        } else {
            return response()->json(false);
        }
    }
}
