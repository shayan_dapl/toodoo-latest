<?php

namespace App\Http\Controllers\Context;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ContextAnalysis;
use App\Models\ContextStakeholderActionLog;
use App\Models\Branch;
use Auth;
use View;
use Session;

class ContextController extends Controller
{
    public function contextMap()
    {
        $breadcrumbs = [
            '#' => 'words.context_map',
        ];
        $internal_opportunity = ContextAnalysis::companyCheck()
            ->where('status', '<>', 3)
            ->where('issue_type', 'internal')
            ->where('risk_or_opportunity', 'opportunity')
            ->with('logs')
            ->with('openlogs')
            ->with('overduelogs')
            ->get()->toArray();

        $external_opportunity = ContextAnalysis::companyCheck()
            ->where('status', '<>', 3)
            ->where('issue_type', 'external')
            ->where('risk_or_opportunity', 'opportunity')
            ->with('logs')
            ->with('openlogs')
            ->with('overduelogs')
            ->get()->toArray();

        $internal_risk = ContextAnalysis::companyCheck()
            ->where('status', '<>', 3)
            ->where('issue_type', 'internal')
            ->where('risk_or_opportunity', 'risk')
            ->with('logs')
            ->with('openlogs')
            ->with('overduelogs')
            ->get()->toArray();
        
        $external_risk = ContextAnalysis::companyCheck()
            ->where('status', '<>', 3)
            ->where('issue_type', 'external')
            ->where('risk_or_opportunity', 'risk')
            ->with('logs')
            ->with('openlogs')
            ->with('overduelogs')
            ->get()->toArray();
        $branchData = Branch::companyStatusCheck();
        $branches = $branchData->pluck('name', 'id')->prepend(translate('form.select'), '0');
        $selectedBranch = '';
        if ($branchData->count() == 1) {
            $selectedBranch = $branchData->pluck('id')[0];
        }
        return View::make('pages.context.context_map', compact('breadcrumbs', 'internal_opportunity', 'external_opportunity', 'internal_risk', 'external_risk', 'branches', 'selectedBranch'));
    }

    public function filterContextMap(Request $req)
    {
        $internal_opportunity = ContextAnalysis::companyCheck()
            ->where('status', '<>', 3)
            ->where('issue_type', 'internal')
            ->where('risk_or_opportunity', 'opportunity')
            ->with('logs')
            ->with('openlogs')
            ->with('overduelogs')
            ->get();
        $internalOpportunity  = [];
       
        foreach ($internal_opportunity as $eachInternalOpportunity) {
            if ($req->branchId != 0) {
                if ($eachInternalOpportunity->brancheids($req->branchId, $eachInternalOpportunity->id) > 0) {
                    $internalOpportunity [] = $eachInternalOpportunity;
                }
            } else {
                $internalOpportunity [] = $eachInternalOpportunity;
            }
        }

        $external_opportunity = ContextAnalysis::companyCheck()
            ->where('status', '<>', 3)
            ->where('issue_type', 'external')
            ->where('risk_or_opportunity', 'opportunity')
            ->with('logs')
            ->with('openlogs')
            ->with('overduelogs')
            ->get();
        $externalOpportunity  = [];
        foreach ($external_opportunity as $eachExternalOpportunity) {
            if ($req->branchId != 0) {
                if ($eachExternalOpportunity->brancheids($req->branchId, $eachExternalOpportunity->id) > 0) {
                    $externalOpportunity [] = $eachExternalOpportunity;
                }
            } else {
                $externalOpportunity [] = $eachExternalOpportunity;
            }
        }
        $internal_risk = ContextAnalysis::companyCheck()
            ->where('status', '<>', 3)
            ->where('issue_type', 'internal')
            ->where('risk_or_opportunity', 'risk')
            ->with('logs')
            ->with('openlogs')
            ->with('overduelogs')
            ->get();
        $internalRisk = [];
        foreach ($internal_risk as $eachInternalRisk) {
            if ($req->branchId != 0) {
                if ($eachInternalRisk->brancheids($req->branchId, $eachInternalRisk->id) > 0) {
                    $internalRisk [] = $eachInternalRisk;
                }
            } else {
                $internalRisk [] = $eachInternalRisk;
            }
        }
        $external_risk = ContextAnalysis::companyCheck()
            ->where('status', '<>', 3)
            ->where('issue_type', 'external')
            ->where('risk_or_opportunity', 'risk')
            ->with('logs')
            ->with('openlogs')
            ->with('overduelogs')
            ->get();
        $externalRisk = [];
        foreach ($external_risk as $eachExternalRisk) {
            if ($req->branchId != 0) {
                if ($eachExternalRisk->brancheids($req->branchId, $eachExternalRisk->id) > 0) {
                    $externalRisk [] = $eachExternalRisk;
                }
            } else {
                $externalRisk [] = $eachExternalRisk;
            }
        }
        return View::make('pages.context.filtered_context_map', compact('internalOpportunity', 'externalOpportunity', 'internalRisk', 'externalRisk'));
    }

    public function actionLog(Request $req)
    {
        if (!empty($req->contextId) && !empty($req->type)) {
            $type = $req->type;
            $data = ContextStakeholderActionLog::where('contxt_stakehold_id', $req->contextId)->where('type', $req->type)->get();
            return View::make('pages.context.context_action_log', compact('data', 'type'));
        }
    }
}
