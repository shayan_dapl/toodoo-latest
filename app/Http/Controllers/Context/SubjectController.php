<?php

namespace App\Http\Controllers\Context;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContextSubjectRequest;
use App\Models\ContextSubjects;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;

class SubjectController extends Controller
{
    public function list()
    {
        $breadcrumbs = [
            '#' => 'words.context_subject',
        ];
        return View::make('pages.context.context_subject_list', compact('breadcrumbs'));
    }

    public function data()
    {
        return ContextSubjects::companyCheck()->where('status', '<>', 3)->get()->toJson();
    }

    public function save(ContextSubjectRequest $req)
    {
        if (!empty($req->input())) {
            if (!empty($req->input('hid'))) {
                $model = ContextSubjects::companyCheck()->where('id', $req->input('hid'))->first();
            } else {
                $model = new ContextSubjects;
            }
            $model->subject = $req->input('subject');
            $model->company_id = Auth::guard('customer')->user()->company_id;
            if ($model->save()) {
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }

    public function remove(Request $req)
    {
        if (!empty($req->id)) {
            $model = ContextSubjects::companyCheck()->where('id', $req->id)->first();
            if (!checkCompanyAccess($model)) {
                return response()->json(false);
            }
            $model->status = 3;
            $model->updated_by = Auth::guard('customer')->user()->id;
            if ($model->save()) {
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }
}
