<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\Branch;
use App\Models\Department;
use App\Models\Position;
use App\Models\PositionBranches;
use App\Models\Users;
use App\Models\ProcessCategory;
use App\Models\Company;
use App\Models\Process;
use App\Models\DepartmentBranch;
use App\Models\OrganizationStructure;
use App\Models\Language;
use App\Models\ProcessBranch;
use View;
use App\Http\Requests\PositionRequest;
use App\Http\Requests\UserRequest;
use Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class GetStartedController extends Controller
{
    public function stepStarted()
    {
        return View::make('pages.getstarted.started');
    }

    public function stepBranch()
    {
        $data = Branch::companyStatusCheck()->get();
        $count = $data->count();
        $list = $data->toJson();
        $department = OrganizationStructure::companyCheck()->count();
        return View::make('pages.getstarted.branch', compact('count', 'list', 'department'));
    }

    public function branches()
    {
        return Branch::companyStatusCheck()->get()->toJson();
    }

    public function stepDepartment()
    {
        $topPersonExists = OrganizationStructure::companyCheck()->count();
        $serialNo = 1;
        $branchData = Branch::companyCheck();
        if ($topPersonExists == 0) {
            $branch = $branchData->first();
        } else {
            $branch = $branchData->unused()->first();
            $serialNo += DepartmentBranch::companyCheck()->groupBy('branch_id')->get()->count();
            $branches = Branch::companyCheck()->pluck('name', 'id')->toArray();
            if (empty($branch)) {
                $branch = Branch::companyCheck()->latest()->first();
            }
        }
        $departmentData = Department::companyStatusCheck()->get();
        $count = $departmentData->count();
        $list = $departmentData->toJson();
        $topBranchName = ($topPersonExists == 0) ? $branch->name : '';
        
        return View::make('pages.getstarted.department', compact('serialNo', 'branch', 'branches', 'count', 'list', 'topBranchName'));
    }

    public function departments(Request $req)
    {
        $departments = DepartmentBranch::where('branch_id', $req->id)->get()->pluck('department_id')->toArray();
        return Department::companyCheck()->whereIn('id', $departments)->get()->toJson();
    }

    public function stepDepartmentCopy()
    {
        $topPersonExists = OrganizationStructure::companyCheck()->count();
        if ($topPersonExists == 0) {
            return redirect('/getstarted/started');
        } else {
            $branch = Branch::companyCheck()->unused()->first();
            $branches = DepartmentBranch::companyCheck()->with('branch')->get()->pluck('branch.name', 'branch.id')->toArray();
            if (empty($branches) || empty($branch)) {
                return redirect('/getstarted/started');
            } else {
                return View::make('pages.getstarted.department_copy', compact('branch', 'branches'));
            }
        }
    }

    public function stepDepartmentCopySave(Request $req)
    {
        if (!empty($req->input())) {
            $departments = DepartmentBranch::companyCheck()->where('branch_id', $req->input('fromBranch'))->get();
            if ($departments->count() > 0) {
                foreach ($departments as $each) {
                    $department = Department::find($each->department_id);
                    $newDepartment = new Department;
                    $newDepartment->company_id = $department->company_id;
                    $newDepartment->is_top = $department->is_top;
                    $newDepartment->name = $department->name;
                    $newDepartment->status = $department->status;
                    if ($newDepartment->save()) {
                        $newDepartmentBranch = new DepartmentBranch;
                        $newDepartmentBranch->company_id = $newDepartment->company_id;
                        $newDepartmentBranch->department_id = $newDepartment->id;
                        $newDepartmentBranch->branch_id = $req->input('toBranch');
                        $newDepartmentBranch->save();
                    }
                }
            }
            return response()->json(true);
        } else {
            return response()->json(false);
        }
    }

    public function stepFunction(Request $req)
    {
        $topPersonExists = OrganizationStructure::companyCheck()->count();
        $branches = DepartmentBranch::companyCheck()->with('branch')->get()->pluck('branch.name', 'branch.id')->toArray();
        $unusedBranches = Branch::companyCheck()->whereNotIn('id', array_keys($branches))->get()->pluck('name', 'id')->toArray();
        $positionBranches = PositionBranches::companyCheck();
        $existingFunctions = $positionBranches->count();
        $branchId = !empty($req->branch) ? $req->branch : array_keys($branches)[0];
        $showExisting = !empty($req->branch) ? true : false;
        $positionIds = $positionBranches->where('branch_id', $branchId)->pluck('position_id')->toArray();
        $positions = Position::companyCheck()->whereIn('id', $positionIds)->select('id', 'department_id', 'name', 'parent_id', 'is_special_position', 'is_assistant', 'status')->where('status', 1)->get();
        $treeStyle = makeTree($positions);
        $positions = structureTree($treeStyle, 'static');
        return View::make('pages.getstarted.function', compact('topPersonExists', 'branches', 'unusedBranches', 'positions', 'existingFunctions', 'showExisting'));
    }

    public function stepFunctionCreate(Request $req)
    {
        $topPersonExists = OrganizationStructure::companyCheck()->count();
        $selected = $req->input('branch');
        $isCorporate = Branch::find($selected)->is_corporate;
        $branches = DepartmentBranch::companyCheck()->with('branch')->get()->pluck('branch.name', 'branch.id')->toArray();
        $departments = DepartmentBranch::companyCheck()->where('branch_id', $selected)->with('department')->get()->pluck('department.name', 'department.id')->toArray();
        ksort($departments);
        $department = Department::find(array_keys($departments)[0]);
        $topDept = $department->is_top;
        $data = $department->positions()->where('is_assistant', 0)->where('is_special_position', 0)->where('status', 1)->orderBy('id');
        $parent = json_encode([]);
        $parentSelected = "";
        if ($data->count() >= 1) {
            $parent = $data->get()->toJson();
            $parentSelected = $data->get()[0]->id;
        }
        $users = Users::companyCheck()->inServiceWithEmail()->where('is_special_user', 0)->get()->pluck('name', 'id')->toArray();
        return View::make('pages.getstarted.create_function', compact('topPersonExists', 'branches', 'isCorporate', 'selected', 'departments', 'topDept', 'parent', 'parentSelected', 'users'));
    }

    public function stepFunctionParent(Request $req)
    {
        $department = Department::find($req->id);
        $top = $department->is_top;
        $data = $department->positions()->where('is_assistant', 0)->where('is_special_position', 0)->where('status', 1)->orderBy('id');
        $result = ["top" => $top, "parent" => ""];
        if ($data->count() >= 1) {
            $result["parent"] = $data->get();
        }
        return response()->json($result);
    }

    public function stepFunctionSave(PositionRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        session()->put('hid', $req->input('hid'));
        session()->put('department_id', $req->input('department_id'));
        session()->put('name', $req->input('name'));
        session()->put('parent_id', $req->input('parent_id'));
        session()->put('info', $req->input('info'));
        session()->put('assistant', $req->input('assistant'));
        session()->put('is_special_position', $req->input('is_special_position'));
        session()->put('branch', $req->input('branch'));
        return response()->json(true);
    }

    public function stepUser(Request $req)
    {
        $language = Language::all()->pluck('display_text', 'name');
        $users = Users::companyCheck()->orderBy('id', 'DESC')->get();
        $position = [
            'hid' => session()->get('hid'),
            'department_id' => session()->get('department_id'),
            'name' => session()->get('name'),
            'parent_id' => session()->get('parent_id'),
            'info' => session()->get('info'),
            'assistant' => session()->get('assistant'),
            'is_special_position' => session()->get('is_special_position'),
            'branch' => session()->get('branch')
        ];
        session()->forget('hid');
        session()->forget('department_id');
        session()->forget('name');
        session()->forget('parent_id');
        session()->forget('info');
        session()->forget('assistant');
        session()->forget('is_special_position');
        session()->forget('branch');
        return View::make('pages.getstarted.user', compact('language', 'position', 'users'));
    }

    public function stepUserGetImage(Request $req)
    {
        $url = Config::get('settings.gravatar_url');
        $url .= md5(strtolower(trim($req->input('email'))));
        $url .= "?s=100&d=404&r=g";
        $response = get_headers($url);
        if ($response[0] != "HTTP/1.1 404 Not Found") {
            return $url;
        } else {
            return response()->json(false);
        }
    }

    public function stepUserSave(UserRequest $req)
    {
        if (empty($req->input()))
            return response()->json(false);
        $configUser = Config::get('settings.associate_user');
        $ext = Config::get('settings.img_extentions');
        $master = new Users;
        $master->email = ($req->input('no_email') == null) ? $req->input('email') : null;
        $endLimit = Config::get('settings.trial_user_limit');
        $flashMsg = translate('error.trial_user_limit');
        $auth = Auth::guard('customer')->user();
        if ($auth->company->is_trial == 1) {
            $endLimit = $auth->company->user_allowed;
            $flashMsg = ($auth->company->user_allowed .' '. translate('error.package_user_limit'));
        }
        //Trial user limit checking
        $trialLimit = Users::companyCheck()->inServiceWithEmail()->where('email', '<>', $configUser)->count();
        if ($trialLimit >= $endLimit) {
            return response()->json(['email' => [$flashMsg]], 422);
        }
        $master->company_id = Auth::user()->company_id;
        $master->name = $req->input('fname') . ' ' . $req->input('lname');
        $master->fname = $req->input('fname');
        $master->lname = $req->input('lname');
        if (!empty($req->input('photo'))) {
            $extension = explode('/', explode(':', substr($req->input('photo'), 0, strpos($req->input('photo'), ';')))[1])[1];
            $extension = strtolower($extension);
            if (in_array($extension, $ext)) {
                $filename = 'User-' . time() . '.' . $extension;
                $path = storage_path('app/public/company/' . $master->company_id . '/image/users/' . $filename);
                Image::make($req->input('photo'))->resize(100, 100)->save($path);
                $master->photo = $filename;
            }
        }
        $master->language = $req->input('language');
        $master->remember_token = $req->input('_token');
        $master->type = $req->input('usertype');
        $master->is_auditor = ($req->input('is_auditor') != null) ? 1 : 0;
        $master->out_of_service = ($req->input('out_of_service') != null) ? $req->input('out_of_service') : null;
        $master->can_process_owner = ($req->input('can_process_owner') != null) ? 1 : 0;
        $master->no_email = ($req->input('no_email') != null) ? 1 : 0;
        $inviteUsrToken = ($req->input('invite_user') != null) ? rand('1000', '9999').time() : '';
        $master->invite_user_token = $inviteUsrToken;
        $master->created_by = Auth::user()->id;
        $master->updated_by = 0;
        if ($master->save()) {
            return response()->json($master->id);
        }
        return response()->json(false);
    }

    public function stepProcess()
    {
        $branches = DepartmentBranch::companyCheck()->with('branch')->get()->pluck('branch.name', 'branch.id')->toArray();
        $existingProcesses = ProcessBranch::companyCheck()->count();
        $processes = Process::companyCheck()->with('branches')->get();
        return View::make('pages.getstarted.process', compact('branches', 'existingProcesses', 'processes'));
    }

    public function stepProcessCreate(Request $req)
    {
        $branch = Branch::whereIn('id', $req->input('branch'))->pluck('name', 'id')->toArray();
        $lang = Config::get('settings.lang');
        $catName = 'name_' . $lang[session('lang')];
        $categories = ProcessCategory::where('status', 1)->pluck($catName, 'id')->toJson();
        $owners = Users::companyCheck()->where('can_process_owner', 1)->with('company')->pluck('name', 'id')->prepend(translate('form.select'), '')->toArray();
        $ids = json_encode($branch);
        return View::make('pages.getstarted.create_process', compact('branch', 'categories', 'catName', 'owners', 'ids'));
    }

    public function dismiss(Request $req)
    {
        if ($req->type == "permanent") {
            if (empty(session('dismiss_started'))) {
                session(['dismiss_started' => 1]);
                return response()->json(1);
            } else {
                session()->forget('dismiss_started');
                return response()->json(0);
            }
        }

        if ($req->type == "temporary") {
            if (empty(session('close_started'))) {
                session(['close_started' => 1]);
                return response()->json(1);
            }
        }
    }

    public function reopen(Request $req)
    {
        Company::where('id', Auth::guard('customer')->user()->company_id)->update(['getstarted' => 0]);
        session()->forget('dismiss_started');
        session()->forget('close_started');
        return response()->json(true);
    }
}
