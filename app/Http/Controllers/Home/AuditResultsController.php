<?php

namespace App\Http\Controllers\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ActionDelegateMember;
use Auth;
use View;
use App\Models\Users;
use App\Repositories\AuditExecution as ExecutionRepo;

class AuditResultsController extends Controller
{
    protected $repo;

    public function __construct(ExecutionRepo $executionRepo)
    {
        $this->repo = $executionRepo;
    }
    public function index()
    {
        $breadcrumbs = array(
            '/home' => 'words.home',
            );
        $processOwners = [];
        if (Auth::guard('customer')->check()) {
            $auditResults = $this->repo->getAuditResult();
            $processOwners = Users::companyCheck()->where('can_process_owner', 1)->pluck('name')->toArray();
        }
        return View::make('pages.home.auditresult', compact('breadcrumbs', 'auditResults', 'processOwners'));
    }
}
