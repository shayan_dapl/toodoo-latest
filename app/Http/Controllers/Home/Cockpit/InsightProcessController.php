<?php

namespace App\Http\Controllers\home\cockpit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Repositories\Home as HomeRepo;

class InsightProcessController extends Controller
{
    protected $repo;

    public function __construct(HomeRepo $homeRepo)
    {
        $this->repo = $homeRepo;
    }
    
    public function index()
    {
        $breadcrumbs = array(
            '/home' => 'words.home',
        );
        $urgentIssues = $this->repo->urgentIssues();

        $veryUrgent = 0;
        $lessUrgent = 0;
        if (!empty($urgentIssues)) {
            foreach ($urgentIssues as $eachUrgency) {
                $findPending = 'none';
                if ($eachUrgency->member_status != 'none') {
                    $memberStatus = explode(',', $eachUrgency->member_status);
                    $findPending = array_search(0, $memberStatus);
                }
                if ($findPending !== false) {
                    if (dayDifference($eachUrgency->created_at) < 1 && dayDifference($eachUrgency->created_at) > -8) {
                        $veryUrgent++;
                    }
                    if (dayDifference($eachUrgency->urgency) > 10) {
                        $lessUrgent++;
                    }
                }
            }
        }
        $upcomingSevendaysDuration = [date('Y-m-d'), date('Y-m-d', strtotime('+7 days'))];
        $unplannedAuditsNo = $this->repo->totalUnplannedAudits();
        $internalPendingIssues = $this->repo->internalPendingIssues();
        $pendingAuditee = $this->repo->pendingAuditsAditee(2, $upcomingSevendaysDuration);
        $pendingAuditor = $this->repo->pendingAuditsAditor(2, $upcomingSevendaysDuration);
        $meetingPending = $pendingAuditee->count() + $pendingAuditor->count();
        
        $months = [date('Y-m-d', strtotime('-6 month')), date('Y-m-d')];
        
        $insightProcessData = $this->repo->insigntsPerProcess($months);
        $processData = json_encode($insightProcessData['process']);
        $insightData = [];
        foreach ($insightProcessData['insight'] as $k => $eachprocess) {
            $insightData[] = array('label' => $k, 'backgroundColor' => generateRgb(), 'data' => $eachprocess);
        }
        $insightData = json_encode($insightData);
        $openClosedPerInsight = $this->repo->openAndClosedIssuesPerInsight($months);
        $findingNames = json_encode($openClosedPerInsight['findingType']);
        $openFindings = json_encode($openClosedPerInsight['processInsight']['open']);
        $closedFindings = json_encode($openClosedPerInsight['processInsight']['closed']);
        return View::make('pages.home.cockpit.insightsToProcess', compact('breadcrumbs', 'veryUrgent', 'unplannedAuditsNo', 'internalPendingIssues', 'meetingPending', 'issueMonths', 'processData', 'insightData', 'findingNames', 'openFindings', 'closedFindings'));
    }
}
