<?php

namespace App\Http\Controllers\home\cockpit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Repositories\Home as HomeRepo;

class UnplannedAuditController extends Controller
{
    protected $repo;

    public function __construct(HomeRepo $homeRepo)
    {
        $this->repo = $homeRepo;
    }
    public function index()
    {
        $breadcrumbs = array(
            '/home' => 'words.home',
        );
        $urgentIssues = $this->repo->urgentIssues();

        $veryUrgent = 0;
        $lessUrgent = 0;
        if (!empty($urgentIssues)) {
            foreach ($urgentIssues as $eachUrgency) {
                $findPending = 'none';
                if ($eachUrgency->member_status != 'none') {
                    $memberStatus = explode(',', $eachUrgency->member_status);
                    $findPending = array_search(0, $memberStatus);
                }
                if ($findPending !== false) {
                    if (dayDifference($eachUrgency->created_at) < 1 && dayDifference($eachUrgency->created_at) > -8) {
                        $veryUrgent++;
                    }
                    if (dayDifference($eachUrgency->urgency) > 10) {
                        $lessUrgent++;
                    }
                }
            }
        }
        $upcomingSevendaysDuration = [date('Y-m-d'), date('Y-m-d', strtotime('+7 days'))];
        $unplannedAuditsNo = $this->repo->totalUnplannedAudits();
        $internalPendingIssues = $this->repo->internalPendingIssues();
        $pendingAuditee = $this->repo->pendingAuditsAditee(2, $upcomingSevendaysDuration);
        $pendingAuditor = $this->repo->pendingAuditsAditor(2, $upcomingSevendaysDuration);
        $meetingPending = $pendingAuditee->count() + $pendingAuditor->count();
        
        $monthArr = [];
        $monthYear = [];
        for ($i = 0; $i < 6; $i++) {
            $monthArr[] = date('F', strtotime("-$i month"));
            $monthYear[date('m', strtotime("-$i month"))] = date('Y', strtotime("-$i month"));
        }
        $monthArr = json_encode(array_reverse($monthArr));
        $monthYear = array_reverse($monthYear, true);
        $unplannedAudits = $this->repo->unplannedAudits();
        $unplannedAuditsMonthWise = [];
        foreach ($monthYear as $k => $v) {
            $unplannedAuditsMonthWise[] = $this->repo->unplannedAuditsMonthWise($k, $v);
        }

        $unplannedAuditsMonthWise = json_encode($unplannedAuditsMonthWise);

        $plannedAuditsMonthWise = [];
        foreach ($monthYear as $k => $v) {
            $plannedAuditsMonthWise[] = $this->repo->plannedAuditsMonthWise($k, $v, 1)->count();
        }

        $plannedAuditsMonthWise = json_encode($plannedAuditsMonthWise);

        $ongoingAuditsMonthWise = [];
        foreach ($monthYear as $k => $v) {
            $ongoingAuditsMonthWise[] = $this->repo->plannedAuditsMonthWise($k, $v, 3)->count();
        }

        $ongoingAuditsMonthWise = json_encode($ongoingAuditsMonthWise);

        $finishedAuditsMonthWise = [];
        foreach ($monthYear as $k => $v) {
            $finishedAuditsMonthWise[] = $this->repo->plannedAuditsMonthWise($k, $v, 2)->count();
        }
        $finishedAuditsMonthWise = json_encode($finishedAuditsMonthWise);
        return View::make('pages.home.cockpit.unplannedAudits', compact('breadcrumbs', 'veryUrgent', 'unplannedAuditsNo', 'internalPendingIssues', 'meetingPending', 'unplannedAuditsMonthWise', 'plannedAuditsMonthWise', 'ongoingAuditsMonthWise', 'finishedAuditsMonthWise', 'upcomingSevendaysDuration', 'monthYear', 'monthArr', 'unplannedAudits'));
    }
}
