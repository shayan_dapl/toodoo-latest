<?php

namespace App\Http\Controllers\home\cockpit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Config;
use Auth;
use App\Repositories\Home as HomeRepo;

class UpcomingMeetingController extends Controller
{
    protected $repo;

    public function __construct(HomeRepo $homeRepo)
    {
        $this->repo = $homeRepo;
    }
    public function index()
    {
        $breadcrumbs = array(
            '/home' => 'words.home',
        );
        $urgentIssues = $this->repo->urgentIssues();

        $veryUrgent = 0;
        $lessUrgent = 0;
        if (!empty($urgentIssues)) {
            foreach ($urgentIssues as $eachUrgency) {
                $findPending = 'none';
                if ($eachUrgency->member_status != 'none') {
                    $memberStatus = explode(',', $eachUrgency->member_status);
                    $findPending = array_search(0, $memberStatus);
                }
                if ($findPending !== false) {
                    if (dayDifference($eachUrgency->created_at) < 1 && dayDifference($eachUrgency->created_at) > -8) {
                        $veryUrgent++;
                    }
                    if (dayDifference($eachUrgency->urgency) > 10) {
                        $lessUrgent++;
                    }
                }
            }
        }
        $upcomingSevendaysDuration = [date('Y-m-d'), date('Y-m-d', strtotime('+7 days'))];
        $unplannedAuditsNo = $this->repo->totalUnplannedAudits();
        $internalPendingIssues = $this->repo->internalPendingIssues();
        $pendingAuditee = $this->repo->pendingAuditsAditee(2, $upcomingSevendaysDuration);
        $pendingAuditor = $this->repo->pendingAuditsAditor(2, $upcomingSevendaysDuration);
        $meetingPending = $pendingAuditee->count() + $pendingAuditor->count();
        
        $months = [date('Y-m-d', strtotime('-6 month')), date('Y-m-d')];
        $pendingAuditee = $this->repo->pendingAuditsAditee(2, $upcomingSevendaysDuration);
        $pendingAuditor = $this->repo->pendingAuditsAditor(2, $upcomingSevendaysDuration);
        $top3Meetings = $pendingAuditee->union($pendingAuditor)->toArray();
        $meetingMonthArr = [];
        $meetingYearArr = [];
        for ($i = 0; $i < 6; $i++) {
            $meetingMonthArr[date('n', strtotime("-$i month"))] = date('M', strtotime("-$i month")) . '/' . date('Y', strtotime("-$i month"));
            $meetingYearArr[date('n', strtotime("-$i month"))] = date('Y', strtotime("-$i month"));
        }
        $meetingMonthArr = array_reverse($meetingMonthArr, true);
        $meetingYearArr = array_reverse($meetingYearArr, true);

        /////Internal audit
        $plannedAuditsforMeeting = $this->repo->plannedAuditsforMeeting($months, 'internal')->toArray();
        $uniqueMeetingProcess = [];
        foreach ($plannedAuditsforMeeting as $eachprocess => $eachData) {
            $uniqueMeetingProcess[$eachData['name']][] = $eachData;
        }
        $plannedAuditMeeting = pastMeetings($uniqueMeetingProcess, $meetingMonthArr, $meetingYearArr);

        ////External Auditll
        $plannedExternalAuditsforMeeting = $this->repo->plannedAuditsforMeeting($months, 'external')->toArray();
        $uniqueMeetingProcess = [];
        foreach ($plannedExternalAuditsforMeeting as $eachprocess => $eachData) {
            $uniqueMeetingProcess[$eachData['name']][] = $eachData;
        }
        $planneExternaldAuditMeeting = pastMeetings($uniqueMeetingProcess, $meetingMonthArr, $meetingYearArr);

        $monthsForMeetingCalender = $meetingMonthArr;
        return View::make('pages.home.cockpit.upcomingMeeting', compact('breadcrumbs', 'veryUrgent', 'unplannedAuditsNo', 'internalPendingIssues', 'meetingPending', 'top3Meetings', 'plannedAuditMeeting', 'planneExternaldAuditMeeting', 'monthsForMeetingCalender'));
    }
}
