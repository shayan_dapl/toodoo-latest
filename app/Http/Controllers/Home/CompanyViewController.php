<?php

namespace App\Http\Controllers\Home;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\CustomerAdminRequest;
use App\Models\Company;
use App\Models\CompanySettings;
use App\Models\Countries;
use App\Models\Language;
use App\Models\Process;
use App\Models\ReportedIssuesDetails;
use App\Models\Users;
use App\Models\ContextStakeholderActionLog;
use App\Models\RiskOpportunityActionLog;
use App\Models\SourceTypes;
use App\Models\TransactionDetails;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Mail;
use Session;
use View;
use Config;
use Excel;

use App\Repositories\Home as HomeRepo;

class CompanyViewController extends Controller
{
    protected $repo;

    public function __construct(HomeRepo $homeRepo)
    {
        $this->repo = $homeRepo;
    }

    //For Main Dashboard Panel
    public function index()
    {
        $breadcrumbs = [
        '/home' => 'words.home',
        ];
        $backgroundColors = [];
        $companyId = Auth::guard('customer')->user()->company_id;
        $prevSixMonths = [date('Y-m-d', strtotime('-6 month')), date('Y-m-d')];
        $allOpenIssueData = $this->repo->openAndClosedIssues('all', 'open', $prevSixMonths, 'companyview');
        $allClosedIssueData = $this->repo->openAndClosedIssues('all', 'closed', $prevSixMonths, 'companyview');
        $issueBarChart = [];
        $issueBarChart['months'] = json_encode(array_keys($allOpenIssueData['months']));
        $issueBarChart['openIssues'] = json_encode(array_values($allOpenIssueData['months']));
        $issueBarChart['closedIssues'] = json_encode(array_values($allClosedIssueData['months']));
        $issueCategories = [
        "all" => translate('words.show_all'),
        "risk" => translate('words.risk'),
        "opportunity" => translate('words.opportunity'),
        "context" => translate('words.context'),
        "stakeholder" => translate('words.stakeholder'),
        "external_issues" => translate('words.external_issues'),
        "internal_issues" => translate('words.internal_issues'),
        "competent_ratings" => translate('words.competent_ratings'),
        "supplier_ratings" => translate('words.supplier_action'),
        "corrective_action" => translate('words.scar')
        ];
        $langArr = [
        'immidiate_action' => translate('words.immidiate_actions'),
        'root_cause' => translate('words.root_cause_analysis'),
        'corrective_action' => translate('words.corrective_action'),
        'effectiveness' => translate('words.effectiveness'),
        '1' => translate('table.not_fixed'),
        ];

        $openPieChart = $this->repo->issuesPieChart('open', $prevSixMonths, 'companyview');
        $closedPieChart = $this->repo->issuesPieChart('closed', $prevSixMonths, 'companyview');
        $users = Users:: companyCheck()->where('out_of_service', null)->where('no_email', 0)->orderBy('name', 'ASC')->pluck('name', 'id')->prepend(translate('form.select'), '');
        return View::make('pages.home.companyView', compact('breadcrumbs', 'langArr', 'issueCategories', 'issueBarChart', 'openPieChart', 'closedPieChart', 'users'));
    }
}
