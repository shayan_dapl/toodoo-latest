<?php

namespace App\Http\Controllers\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CompanySettings;
use Auth;
use View;
use App\Repositories\Home as HomeRepo;

class MeetingController extends Controller
{
    protected $repo;

    public function __construct(HomeRepo $homeRepo)
    {
        $this->repo = $homeRepo;
    }
    public function index()
    {
        $breadcrumbs = array(
            '/home' => 'words.home',
        );
        if (Auth::guard('customer')->check()) {
            $getauditorAndAuditees = $this->repo->pendingAuditsAditorAuditee();
        }
        return View::make('pages.home.meeting', compact('breadcrumbs', 'getauditorAndAuditees'));
    }
}
