<?php

namespace App\Http\Controllers\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use View;
use App\Repositories\Home as HomeRepo;

class ProcessImprovementController extends Controller
{
    protected $repo;

    public function __construct(HomeRepo $homeRepo)
    {
        $this->repo = $homeRepo;
    }
    public function index()
    {
        $breadcrumbs = array(
            '/home' => 'words.home',
            );
        if (Auth::guard('customer')->check()) {
            $pendingIssues = [];
            $internalPendingIssues = $this->repo->internalPendingIssues();
            foreach ($internalPendingIssues as $k => $v) {
                $pendingIssues[$v->process][] = $v;
            }
        }
        return View::make('pages.home.processImprovement', compact('breadcrumbs', 'pendingIssues'));
    }
}
