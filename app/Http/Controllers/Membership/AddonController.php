<?php

namespace App\Http\Controllers\Membership;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PackageMaster;
use App\Models\TransactionDetails;
use Auth;
use View;
use App\Models\PackagePurchaseMaster;
use App\Models\PackageUserPlan;
use App\Models\PackageStoragePlan;
use App\Models\PaymentCustomerid;
use App\Models\Company;
use App\Models\PriceIncreaseLog;
use App\Models\Users;
use App\Models\InvoiceData;
use App\Models\Countries;
use App\Models\SubscriptionCancellationLog;
use Mollie;
use Config;
use File;
use PDF;

class AddonController extends Controller
{
    public function account()
    {
        $breadcrumbs = [
            '/' => 'words.invoice',
        ];
        $company = Auth::guard('customer')->user()->company;
        $plans = ['iso' => "", 'user' => "", 'storage' => ""];
        $iso = [];
        if ($company->transactions->count() > 0) {
            foreach ($company->transactions as $each) {
                foreach ($each->purchased as $pur) {
                    if ($pur->type == 'iso' && $pur->status == 1) {
                        $choosenPkg = $pur->iso()->name_en;
                        $iso[$choosenPkg] = $choosenPkg;
                    }
                }
            }
            $plans['iso'] = implode(", ", $iso);
        } else {
            $iso = PackageMaster::where('is_base', 1)->first();
            $plans['iso'] = translate('words.base_package');
        }
        $companyFolderPath = storage_path('app/public/company/' . $company->id);
        $fileSize = 0;
        foreach (File::allFiles($companyFolderPath) as $file) {
            $fileSize += $file->getSize();
        }
        $userAdded = Users::companyCheck()->inServiceWithEmail()->where('email', '<>', Config::get('settings.associate_user'))->count();
        $plans['user'] = $company->user_allowed;
        $plans['storage'] = $company->storage_allowed;
        $approvalPanel = false;
        $currentPackagePrice = '';
        $currentUserPrice = '';
        $currentStoragePrice = '';
        $newUserPrice = '';
        $newStoragePrice = '';
        $newPackagePrice = '';
        $newSubscriptionAmount = '';
        $proposalId = '';
        if ($company->price_inc_approve_status == '0') {
            $proposalData = PriceIncreaseLog::companyCheck()->where('approve_status', 0)->orderBy('id', 'DESC')->first();
            $approvalPanel = true;
            $proposalId = $proposalData->id;
            $pctIncrease = $proposalData->pct_request;
            $currentPackagePrice = $company->package_price;
            $currentUserPrice = $company->user_package_price;
            $currentStoragePrice = $company->storage_package_price;
            $newPackagePrice = ($proposalData->package_price_inc_req == 1) ? number_format($currentPackagePrice + ($currentPackagePrice * $pctIncrease)/100, 2) : $currentPackagePrice;
            $newUserPrice = ($proposalData->user_price_inc_req == 1) ? number_format($currentUserPrice + ($currentUserPrice * $pctIncrease)/100, 2) : $currentUserPrice;
            $newStoragePrice = ($proposalData->storage_price_inc_req == 1) ? number_format($currentStoragePrice + ($currentStoragePrice * $pctIncrease)/100, 2) : $currentStoragePrice;
            $newSubscriptionAmount = number_format($newPackagePrice + $newUserPrice + $newStoragePrice, 2);
        }
        return View::make('pages.membership.account', compact('breadcrumbs', 'company', 'plans', 'approvalPanel', 'currentPackagePrice', 'currentUserPrice', 'currentStoragePrice', 'newPackagePrice', 'newUserPrice', 'newStoragePrice', 'newSubscriptionAmount', 'proposalId', 'fileSize', 'userAdded'));
    }

    public function iso()
    {
        $iso = PackageMaster::where('is_base', '<>', 1)->where('status', 1)->get();
        $details = TransactionDetails::companyCheck()->where('status', null)->orderBy('id', 'DESC')->first();
        $selected = [];
        if (!empty($details)) {
            $selected = $details->purchased()->where('type', 'iso')->get()->pluck('package_id')->toArray();
        }
        return View::make('pages.membership.addon.iso', compact('iso', 'details', 'selected'));
    }
    
    public function isoSave(Request $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = TransactionDetails::findOrNew($req->input('hid'));
        $model->company_id = Auth::guard('customer')->user()->company_id;
        if ($model->save()) {
            PackagePurchaseMaster::where('transaction_id', $model->id)->delete();
            if (!empty($req->input('packages'))) {
                foreach ($req->input('packages') as $id) {
                    $purchaseModel = new PackagePurchaseMaster;
                    $purchaseModel->transaction_id = $model->id;
                    $purchaseModel->package_id = $id;
                    $purchaseModel->type = "iso";
                    $purchaseModel->day_count = getNextSubscriptionRemainingDays($purchaseModel->iso($id)->price)['days'] ;
                    $purchaseModel->amount = getNextSubscriptionRemainingDays($purchaseModel->iso($id)->price)['amount'];
                    $purchaseModel->save();
                }
            }
            return redirect('/membership/preview-addon');
        }
    }
    
    public function user()
    {
        $userPlans = PackageUserPlan::where('is_base', 0)->where('is_max', 0)->get()->toArray();
        $subscriptionTenure = Company::find(Auth::guard('customer')->user()->company_id)->subscription_tenure;
        $details = TransactionDetails::companyCheck()->where('status', null)->orderBy('id', 'desc')->first();
        foreach ($userPlans as $k => $v) {
            if ($subscriptionTenure == 1) {
                $per = \Config::get('settings.membership_monthly_per');
                $userPlans[$k]['price_per_month'] = $userPlans[$k]['price_per_month'] + ($userPlans[$k]['price_per_month'] * $per)/100;
            } else {
                $userPlans[$k]['price_per_month'] = $userPlans[$k]['price_per_month'] * 12;
            }
        }
        return View::make('pages.membership.addon.user', compact('userPlans', 'details', 'subscriptionTenure'));
    }
    
    public function userSave(Request $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = TransactionDetails::findOrNew($req->input('hid'));
        $model->company_id = Auth::guard('customer')->user()->company_id;
        if ($model->save()) {
            PackagePurchaseMaster::where('transaction_id', $model->id)->delete();
            $purchaseModel = new PackagePurchaseMaster;
            $purchaseModel->transaction_id = $model->id;
            $purchaseModel->package_id = $req->input('userCount');
            $purchaseModel->type = "user";
            $purchaseModel->day_count = getNextSubscriptionRemainingDays($purchaseModel->user($req->input('userCount'))->price_per_month)['days'] ;
            $purchaseModel->amount = getNextSubscriptionRemainingDays($purchaseModel->user($req->input('userCount'))->price_per_month)['amount'];
            $purchaseModel->save();
            return redirect('/membership/preview-addon');
        }
    }

    public function preview(Request $req)
    {
        $txn = TransactionDetails::companyCheck()->where('status', null)->orderBy('id', 'DESC')->first();
        $subscriptionTenure = Company::find(Auth::guard('customer')->user()->company_id)->subscription_tenure;
        if ($txn->company->is_vat == 0 && $txn->company->countries->vat_percent != '') {
            $vat = $txn->company->countries->vat_percent;
        } else {
            $vat = '';
        }
        $type = null;
        $data = [
            'iso' => [],
            'user' => [],
            'company' => $txn
        ];
        if ($txn->purchased()->count() > 0) {
            foreach ($txn->purchased()->get() as $each) {
                if ($each->type == "iso") {
                    $data['iso'][] = $each->iso();
                    $type = 'iso';
                }
                if ($each->type == "user") {
                    $data['user'] = $each->user();
                    $type = 'user';
                }
            }
        }
        return View::make('pages.membership.addon.preview', compact('data', 'vat', 'type', 'subscriptionTenure'));
    }

    public function makePayment(Request $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $customerExist = PaymentCustomerid::where('company_id', Auth::guard('customer')->user()->company_id)->first();
        if (!empty($customerExist)) {
            $customerId = $customerExist->customer_id;
        } else {
            $customer = Mollie::api()->customers()->create([
                "name"  => $req->input('name'),
                "email" => $req->input('email')
            ]);
            $customerId = $customer->id;
            $paymentCustomer = new PaymentCustomerid;
            $paymentCustomer->company_id = $req->input('company_id');
            $paymentCustomer->user_id = Auth::guard('customer')->user()->id;
            $paymentCustomer->customer_id = $customer->id;
            $paymentCustomer->save();
        }
        $payment = mollie::api()->payments()->create([
            'amount'        => $req->input('amount'),
            'customerId'    => $customerId,
            'recurringType' => 'first',
            'description'   => translate('words.my_addon_payment'),
            'redirectUrl'   => url('/membership/addon-success'),
            'webhookUrl'    => url('/api/update-addon-payment-status'),
            'metadata'    => [
                'base_amount' => $req->input('base_amount'),
                'vat_amount' =>  $req->input('vat'),
                'next_renewal_amt' => $req->input('nextPayment'),
                'next_renewal_amt_without_vat' => $req->input('nextPaymentWithoutVat'),
                'package_price' => $req->input('package_price'),
                'user_package_price' => $req->input('user_package_price'),
                'storage_package_price' => $req->input('storage_package_price')
            ]
        ]);
        $model = TransactionDetails::find($req->input('id'));
        $model->type = 'addon_payment';
        $model->transaction_id = $payment->id;
        $model->user_id = Auth::guard('customer')->user()->id;
        $model->profile_id = $payment->profileId;
        $model->package_price = $payment->metadata->package_price;
        $model->user_package_price = $payment->metadata->user_package_price;
        $model->storage_package_price = $payment->metadata->storage_package_price;
        $model->amount = $payment->metadata->base_amount;
        $model->vat_amount = $payment->metadata->vat_amount;
        $model->total_amount = $payment->amount;
        if ($model->save()) {
            $invoiceData = new InvoiceData;
            $invoiceData->company_id =Auth::guard('customer')->user()->company->id;
            $invoiceData->transaction_id = $req->input('id');
            $invoiceData->username = Auth::guard('customer')->user()->name;
            $invoiceData->company_logo = Auth::guard('customer')->user()->company->photo;
            $invoiceData->company_name = Auth::guard('customer')->user()->company->name;
            $invoiceData->street = Auth::guard('customer')->user()->company->street;
            $invoiceData->nr = Auth::guard('customer')->user()->company->nr;
            $invoiceData->bus = Auth::guard('customer')->user()->company->bus;
            $invoiceData->zip = Auth::guard('customer')->user()->company->zip;
            $invoiceData->city = Auth::guard('customer')->user()->company->city;
            $invoiceData->country = Auth::guard('customer')->user()->company->country;
            $invoiceData->tva_no = Auth::guard('customer')->user()->company->tva_no;
            $invoiceData->save();
            return redirect($payment->links->paymentUrl);
        }
    }

    public function updateAddonPaymentStatus(Request $req)
    {
        $txnId = $req->input('id');
        $txn = TransactionDetails::where('transaction_id', $txnId)->first();
        $payment = Mollie::api()->payments()->get($txnId);
        TransactionDetails::where('transaction_id', $txnId)->update(['status' => $payment->status, 'payment_date' =>  date('Y-m-d')]);
        if ($payment->status == 'paid') {
            PackagePurchaseMaster::where('transaction_id', $txn->id)->update(['status' => 1]);
            $customerId = PaymentCustomerid::where('company_id', $txn->company_id)->first()->customer_id;
            $getCompanyData = Company::where('id', $txn->company_id)->first();
            $intervel = ($getCompanyData->subscription_tenure == 1) ? '1 month' : '12 months';
            TransactionDetails::where('transaction_id', $txnId)->update(['invoice_no' => invoice()]);
            ////update user and storage count start
            $parchasedData = PackagePurchaseMaster::where('transaction_id', $txn->id)->where('status', 1)->get();
            $addedUser = 0;
            $addedStorage = 0;
            if (!empty($parchasedData)) {
                foreach ($parchasedData as $eachData) {
                    if ($eachData->type == 'user') {
                        $addedUser = $addedUser + PackageUserPlan::find($eachData->package_id)->user_count;
                    }
                    if ($eachData->type == 'storage') {
                        $addedStorage = $addedStorage + PackageStoragePlan::find($eachData->package_id)->space;
                    }
                }
            }
            $totUserAllowed = $getCompanyData->user_allowed + $addedUser;
            $totStorageAllowed = $getCompanyData->storage_allowed + $addedStorage;
            Company::where('id', $txn->company_id)->update(['user_allowed' => $totUserAllowed, 'storage_allowed' => $totStorageAllowed]);
            ////update user and storage count end
            //Cancel old Subscription
            $cancelSubscription = Mollie::api()->customersSubscriptions()->withParentId($customerId)->cancel($getCompanyData->subscription_id);
            if ($cancelSubscription->status == 'cancelled') {
                $mandates = Mollie::api()->customersMandates()->withParentId($customerId)->all();
                if (!empty($mandates->data)) {
                    if ($mandates->data[0]->status == 'pending' || $mandates->data[0]->status == 'valid') {
                        $startDate = date(Config::get('settings.db_date'), strtotime($getCompanyData->next_renewal_date));
                        $subscription = Mollie::api()->customersSubscriptions()->withParentId($customerId)->create([
                            "amount"      => $payment->metadata->next_renewal_amt,
                            "interval"    => $intervel,
                            "startDate"   => $startDate,
                            "description" => "Subscription Modified",
                            "webhookUrl"  => url('/api/subscription-webhook')
                        ]);
                        $package_price = $getCompanyData->package_price + $txn->package_price;
                        $user_package_price = $getCompanyData->user_package_price + $txn->user_package_price;
                        $storage_package_price = $getCompanyData->storage_package_price + $txn->storage_package_price;

                        Company::where('id', $txn->company_id)->update(['subscription_id' => $subscription->id, 'subscription_amount' =>  $payment->metadata->next_renewal_amt_without_vat, 'package_price' => $package_price, 'user_package_price' => $user_package_price, 'storage_package_price' => $storage_package_price]);
                    }
                }
            }
        }
    }

    public function success()
    {
        return View::make('pages.membership.addon.success');
    }

    public function transactionDetail(Request $req)
    {
        $data = TransactionDetails::where('id', $req->id)->first();
        $paymentType = $data->type;
        $customerId = 'TD'.str_pad($data->user_id, 5, '0', STR_PAD_LEFT);
        $field = 'name_'.session('lang');
        $countryData = '';
        $getcountry = Countries::where('id', $data->invoicedata->country)->pluck($field);
        if (!empty($getcountry)) {
            $countryData = $getcountry[0];
        }
        return View::make('pages.membership.invoice', compact('data', 'customerId', 'countryData', 'paymentType'));
    }
    
    public function printInvoice(Request $req)
    {
        $data = TransactionDetails::where('id', $req->id)->first();
        $paymentType = $data->type;
        $customerId = 'TD'.str_pad($data->user_id, 5, '0', STR_PAD_LEFT);
        $field = 'name_'.session('lang');
        $countryData = '';
        $getcountry = Countries::where('id', $data->invoicedata->country)->pluck($field);
        if (!empty($getcountry)) {
            $countryData = $getcountry[0];
        }
        $html = View::make('pages.membership.invoice_print', compact('data', 'customerId', 'countryData', 'paymentType'))->render();
        return PDF::load($html)->filename('/tmp/'.str_replace('/', '-', $data->invoice_no).'.pdf')->download();
    }

    public function approvePriceRequest(Request $req)
    {
        $company = Auth::guard('customer')->user()->company;
        $customerId = PaymentCustomerid::where('company_id', $company->id)->first()->customer_id;
        $newPackagePrice = $req->input('newPackagePrice');
        $newUserPrice = $req->input('newUserPrice');
        $newStoragePrice = $req->input('newStoragePrice');
        $newSubsAmtWithoutVat = $req->input('newSubscriptionAmount');
        $proposalId = $req->input('proposalId');
        $newSubscriptionAmount = $newSubsAmtWithoutVat;
        if ($company->is_vat == 0) {
            $vat = ($newSubsAmtWithoutVat * $company->countries->vat_percent) / 100;
            $newSubscriptionAmount = $newSubsAmtWithoutVat + $vat;
        }
        //Cancel old Subscription
        $cancelSubscription = Mollie::api()->customersSubscriptions()->withParentId($customerId)->cancel($company->subscription_id);
        if ($cancelSubscription->status == 'cancelled') {
            $mandates = Mollie::api()->customersMandates()->withParentId($customerId)->all();
            if (!empty($mandates->data)) {
                if ($mandates->data[0]->status == 'pending' || $mandates->data[0]->status == 'valid') {
                    $subscription = Mollie::api()->customersSubscriptions()->withParentId($customerId)->create([
                        "amount"      => $newSubscriptionAmount,
                        "interval"    => "1 month",
                        "startDate"   => $company->next_renewal_date,
                        "description" => "Subscription Modified",
                        "webhookUrl"  => url('/api/subscription-webhook')
                    ]);
                    PriceIncreaseLog::where('id', $proposalId)->update(['approve_status' => 1, 'approve_by' => Auth::guard('customer')->user()->id, 'approved_date' => date('Y-m-d')]);
                    Company::where('id', $company->id)->update(['subscription_id' => $subscription->id, 'subscription_amount' =>  $newSubsAmtWithoutVat, 'package_price' => $newPackagePrice, 'user_package_price' => $newUserPrice, 'storage_package_price' => $newStoragePrice, 'price_inc_approve_status' => 1]);
                }
            }
        }
        return redirect('/membership/account');
    }

    public function endSubscription()
    {
        $customerId = Auth::guard('customer')->user()->customerId->customer_id;
        $subscriptionId = Auth::guard('customer')->user()->company->subscription_id;
        if (!empty($customerId) && !empty($subscriptionId)) {
            $cancelSubscription = Mollie::api()->customersSubscriptions()->withParentId($customerId)->cancel($subscriptionId);
            if ($cancelSubscription->status == 'cancelled') {
                Company::where('id', Auth::guard('customer')->user()->company_id)->update(['subscription_cancelled_at' => date('Y-m-d')]);
                $log = new SubscriptionCancellationLog;
                $log->company_id = Auth::guard('customer')->user()->company_id;
                $log->save();
                return response()->json(true);
            }
        }
        return response()->json(false);
    }
}
