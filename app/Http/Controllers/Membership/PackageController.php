<?php

namespace App\Http\Controllers\Membership;

use App\Http\Controllers\Controller;
use App\Models\IsoClause;
use App\Models\PackageMaster;
use App\Models\PackageStoragePlan;
use App\Models\PackageUserPlan;
use App\Models\PackagePriceLog;
use Illuminate\Http\Request;
use View;
use Config;

class PackageController extends Controller
{
    public function list(Request $req)
    {
        $breadcrumbs = [
            '/package/list' => 'words.package',
            '#' => 'words.list',
        ];
        $data = PackageMaster::with('iso')->get();

        return View::make('pages.settings.package_list', compact('breadcrumbs', 'data'));
    }

    public function form(Request $req)
    {
        $breadcrumbs = [
            '/package/list' => 'words.package',
            '#' => 'words.add',
        ];
        $language = Config::get('settings.lang');
        $iso_no = 'iso_no_' . $language[session('lang')];
        $isoReference = IsoClause::all()->pluck($iso_no, 'id')->prepend(translate('form.select'), '0');
        $selectedAuditType = $req->id;
        $ref_id = $selectedAuditType;
        return View::make('pages.settings.package_form', compact('breadcrumbs', 'isoReference', 'selectedAuditType'));
    }

    public function edit(Request $req)
    {
        if (!empty($req->id)) {
            $details = PackageMaster::find($req->id);
            $breadcrumbs = [
                '/package/list' => 'words.package',
                '#' => 'words.edit',
            ];
            $language = Config::get('settings.lang');
            $iso_no = 'iso_no_' . $language[session('lang')];
            $isoReference = IsoClause::all()->pluck($iso_no, 'id')->prepend(translate('form.select'), '0');
            $selectedIso = $details->iso_id;
            return View::make('pages.settings.package_form', compact('breadcrumbs', 'details', 'isoReference', 'selectedIso'));
        }
    }

    public function save(Request $req)
    {
        if (!empty($req->input())) {
            $model = PackageMaster::findOrNew($req->input('hid'));
            $model->iso_id = $req->input('iso_reference');
            $model->name_en = $req->input('name_en');
            $model->name_fr = $req->input('name_fr');
            $model->name_nl = $req->input('name_nl');
            $model->description_en = $req->input('description_en');
            $model->description_fr = $req->input('description_fr');
            $model->description_nl = $req->input('description_nl');
            $model->is_base = $req->input('base_package');
            $model->price = $req->input('price');
            $model->status = 1;
            if ($model->save()) {
                if (($req->input('old_price') == '') || ($req->input('old_price') != $req->input('price'))) {
                    $pricelog = new PackagePriceLog;
                    $pricelog->package_id = $model->id;
                    $pricelog->old_price = $req->input('old_price');
                    $pricelog->new_price = $req->input('price');
                    $pricelog->change_date = date('Y-m-d');
                    $pricelog->save();
                }
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/package/list');
            }
        }
    }

    public function userPlanlist(Request $req)
    {
        $breadcrumbs = [
            '/package/user-plan' => 'words.user_plans',
            '#' => 'words.list',
        ];
        $data = PackageUserPlan::where('is_base', '<>', 1)->where('is_max', '<>', 1)->get();

        return View::make('pages.settings.user_plan_list', compact('breadcrumbs', 'data'));
    }

    public function edituserPlan(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/package/user-plan' => 'words.user_plans',
                '#' => 'words.edit',
            ];
            $details = PackageUserPlan::find($req->id);
            return View::make('pages.settings.edit_user_plan', compact('breadcrumbs', 'details'));
        }
    }

    public function userPlanSave(Request $req)
    {
        if (!empty($req->input())) {
            $model = PackageUserPlan::find($req->input('hid'));
            $model->price_per_month = $req->input('price');
            $model->status = 1;
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/package/user-plan');
            }
        }
    }
}
