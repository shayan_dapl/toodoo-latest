<?php

namespace App\Http\Controllers\Membership;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\Company;
use App\Models\Users;
use App\Http\Requests\ModifyRequest;
use App\Models\Countries;
use Auth;
use App\Http\Requests\CompanyDetailRequest;
use App\Models\PackageMaster;
use App\Models\TransactionDetails;
use App\Models\PackagePurchaseMaster;
use App\Models\PackageUserPlan;
use App\Models\PackageStoragePlan;
use App\Models\PaymentCustomerid;
use App\Models\InvoiceData;
use Config;
use Mollie;

class TrialController extends Controller
{
    public function index(Request $req)
    {
        return View::make('pages.membership.trial.index');
    }

    public function memberDetail(Request $req)
    {
        return View::make('pages.membership.trial.member');
    }

    public function memberModify(Request $req)
    {
        return View::make('pages.membership.trial.modify');
    }

    public function memberModifySave(ModifyRequest $req)
    {
        if (!empty($req->input())) {
            Company::where('id', $req->input('compid'))->update(['name' => $req->input('company_name')]);
            $name = $req->input('first_name') .' '. $req->input('last_name');
            Users::where('id', $req->input('hid'))->update(['name' => $name, 'fname' => $req->input('first_name'), 'lname' => $req->input('last_name'), 'phone' => $req->input('phone')]);
            return redirect('/membership/detail');
        } else {
            return response()->json(false);
        }
    }

    public function companyDetails(Request $req)
    {
        $langArr = Config::get('settings.lang');
        $language = 'name_' . $langArr[session('lang')];
        $countries = Countries::all()->pluck($language, 'id')->prepend(translate('form.select'), '');
        $companyDetail = Company::find(Auth::guard('customer')->user()->company_id);
        return View::make('pages.membership.trial.company', compact('countries', 'companyDetail'));
    }
    
    public function companyDetailsSave(CompanyDetailRequest $req)
    {
        if (!empty($req->input())) {
            $updateArr = [
                'street' => $req->input('street'),
                'nr' => $req->input('nr'),
                'bus' => $req->input('bus'),
                'zip' => $req->input('zip'),
                'city' => $req->input('city'),
                'country' => $req->input('comp_country'),
                'tva_no' => $req->input('tva_no'),
                'is_vat' => ($req->input('is_apply_vat') != null) ? 1 : 0
            ];
            Company::where('id', $req->input('compid'))->update($updateArr);
        }
        return redirect('/membership/choose-iso');
    }

    public function chooseIso(Request $req)
    {
        $iso = PackageMaster::where('status', 1)->get();
        $details = TransactionDetails::companyCheck()->where('status', null)->orderBy('id', 'DESC')->first();
        $selected = [];
        if (!empty($details)) {
            $selected = $details->purchased()->where('type', 'iso')->get()->pluck('package_id')->toArray();
        }
        return View::make('pages.membership.trial.iso', compact('iso', 'details', 'selected'));
    }

    public function chooseIsoSave(Request $req)
    {
        if (!empty($req->input())) {
            $model = TransactionDetails::findOrNew($req->input('hid'));
            $model->company_id = Auth::guard('customer')->user()->company_id;
            if ($model->save()) {
                $tenure = $req->input('tenure');
                PackagePurchaseMaster::where('transaction_id', $model->id)->delete();
                $cmpModel = Company::find(Auth::guard('customer')->user()->company_id);
                $cmpModel->subscription_tenure = $tenure;
                $cmpModel->save();
                if (!empty($req->input('packages'))) {
                    foreach ($req->input('packages') as $id) {
                        $purchaseModel = new PackagePurchaseMaster;
                        $dayCount = ($tenure == 1) ? 30 : 365 ;
                        $per = \Config::get('settings.membership_monthly_per');
                        if ($tenure == 1) {
                            $amount = $purchaseModel->iso($id)->price + ($purchaseModel->iso($id)->price * $per)/100;
                        } else {
                            $amount = $purchaseModel->iso($id)->price * 12;
                        }
                        $purchaseModel->transaction_id = $model->id;
                        $purchaseModel->package_id = $id;
                        $purchaseModel->type = "iso";
                        $purchaseModel->day_count = $dayCount ;
                        $purchaseModel->amount = $amount;
                        $purchaseModel->save();
                    }
                }
                return redirect('/membership/choose-addons');
            }
        } else {
            return response()->json(false);
        }
    }

    public function chooseAddons(Request $req)
    {
        $userPlans = PackageUserPlan::where('is_max', '0')->get()->toArray();
        $details = TransactionDetails::companyCheck()->where('status', null)->orderBy('id', 'desc')->first();
        $getallPerchesedPrice = PackagePurchaseMaster::where('transaction_id', $details->id)->where('type', 'iso')->sum('amount');
        $getBaseUserPlan = PackageUserPlan::where('is_base', 1)->pluck('user_count')->first();
        $subscriptionTenure = Company::find(Auth::guard('customer')->user()->company_id)->subscription_tenure;
        foreach ($userPlans as $k => $v) {
            if ($v['is_base'] != 1) {
                $userPlans[$k]['user_count'] += $getBaseUserPlan;
            }
            if ($subscriptionTenure == 1) {
                $per = \Config::get('settings.membership_monthly_per');
                $userPlans[$k]['price_per_month'] = $userPlans[$k]['price_per_month'] + ($userPlans[$k]['price_per_month'] * $per)/100;
            } else {
                $userPlans[$k]['price_per_month'] = $userPlans[$k]['price_per_month'] * 12;
            }
            $userPlans[$k]['price_per_month'] += $getallPerchesedPrice;
        }
        $storagePlans = PackageStoragePlan::where('is_base', '<>', 1)->get()->pluck('storage_data', 'id')->prepend(translate('form.select'), 0);
        return View::make('pages.membership.trial.addons', compact('userPlans', 'storagePlans', 'details', 'subscriptionTenure'));
    }

    public function chooseAddonSave(Request $req)
    {
        if (!empty($req->input())) {
            PackagePurchaseMaster::where('transaction_id', $req->input('hid'))->where('type', '<>', 'iso')->where('status', 0)->delete();
            $tenure = $req->input('tenure');
            foreach ($req->input('addons') as $type => $packageId) {
                if ($packageId != 0) {
                    $model = new PackagePurchaseMaster;
                    $dayCount = ($tenure == 1) ? 30 : 365 ;
                    $per = \Config::get('settings.membership_monthly_per');
                    if ($tenure == 1) {
                        $amount = $model->$type($packageId)->price_per_month + ($model->$type($packageId)->price_per_month * $per)/100;
                    } else {
                        $amount = $model->$type($packageId)->price_per_month * 12;
                    }
                    $model->transaction_id = $req->input('hid');
                    $model->package_id = $packageId;
                    $model->type = $type;
                    $model->day_count = $dayCount;
                    $model->amount = $amount;
                    $model->save();
                }
            }
            return redirect('/membership/preview');
        } else {
            return response()->json(false);
        }
    }

    public function preview(Request $req)
    {
        $txn = TransactionDetails::companyCheck()->where('status', null)->orderBy('id', 'DESC')->first();
        $subscriptionTenure = Company::find(Auth::guard('customer')->user()->company_id)->subscription_tenure;
        if ($txn->company->is_vat == 0 && $txn->company->countries->vat_percent != '') {
            $vat = $txn->company->countries->vat_percent;
        } else {
            $vat = '';
        }
        $data['company'] = $txn;
        if ($txn->purchased()->count() > 0) {
            foreach ($txn->purchased()->get() as $each) {
                if ($each->type == "iso") {
                    $data['iso'][] = $each->iso();
                }
                if ($each->type == "user") {
                    $data['user'] = $each->user();
                }
                if ($each->type == "storage") {
                    $data['storage'] = $each->storage();
                }
            }
        }
        return View::make('pages.membership.trial.preview', compact('data', 'vat', 'subscriptionTenure'));
    }

    public function makePayment(Request $req)
    {
        if (!empty($req->input())) {
            $customerExist = PaymentCustomerid::where('user_id', Auth::guard('customer')->user()->id)->first();
            if (!empty($customerExist)) {
                $customerId = $customerExist->customer_id;
            } else {
                $customer = Mollie::api()->customers()->create([
                    "name"  => $req->input('name'),
                    "email" => $req->input('email')
                ]);
                $customerId = $customer->id;
                $paymentCustomer = new PaymentCustomerid;
                $paymentCustomer->company_id = $req->input('company_id');
                $paymentCustomer->user_id = Auth::guard('customer')->user()->id;
                $paymentCustomer->customer_id = $customer->id;
                $paymentCustomer->save();
            }
            $payment = mollie::api()->payments()->create([
                'amount'        => $req->input('amount'),
                'customerId'    => $customerId,
                'recurringType' => 'first',
                'description'   => translate('words.my_initial_payment'),
                'redirectUrl'   => url('/membership/success'),
                'webhookUrl'    => url('/api/update-payment-status'),
                'metadata'    => [
                                    'base_amount' => $req->input('base_amount'),
                                    'vat_amount' =>  $req->input('vat'),
                                    'package_price' => $req->input('package_price'),
                                    'user_package_price' => $req->input('user_package_price'),
                                    'storage_package_price' => $req->input('storage_package_price'),
                                 ]
            ]);
            $model = TransactionDetails::find($req->input('id'));
            $model->type = 'first_payment';
            $model->transaction_id = $payment->id;
            $model->user_id = Auth::guard('customer')->user()->id;
            $model->profile_id = $payment->profileId;
            $model->package_price = $payment->metadata->package_price;
            $model->user_package_price = $payment->metadata->user_package_price;
            $model->storage_package_price = $payment->metadata->storage_package_price;
            $model->amount = $payment->metadata->base_amount;
            $model->vat_amount = $payment->metadata->vat_amount;
            $model->total_amount = $payment->amount;
            if ($model->save()) {
                $invoiceData = new InvoiceData;
                $invoiceData->company_id =Auth::guard('customer')->user()->company->id;
                $invoiceData->transaction_id = $req->input('id');
                $invoiceData->username = Auth::guard('customer')->user()->name;
                $invoiceData->company_logo = Auth::guard('customer')->user()->company->photo;
                $invoiceData->company_name = Auth::guard('customer')->user()->company->name;
                $invoiceData->street = Auth::guard('customer')->user()->company->street;
                $invoiceData->nr = Auth::guard('customer')->user()->company->nr;
                $invoiceData->bus = Auth::guard('customer')->user()->company->bus;
                $invoiceData->zip = Auth::guard('customer')->user()->company->zip;
                $invoiceData->city = Auth::guard('customer')->user()->company->city;
                $invoiceData->country = Auth::guard('customer')->user()->company->country;
                $invoiceData->tva_no = Auth::guard('customer')->user()->company->tva_no;
                $invoiceData->save();
                return redirect($payment->links->paymentUrl);
            }
        } else {
            return response()->json(false);
        }
    }

    public function updateTrialPaymentStatus(Request $req)
    {
        $txnId = $req->input('id');

        $txn = TransactionDetails::where('transaction_id', $txnId)->first();
        $payment = Mollie::api()->payments()->get($txnId);
        TransactionDetails::where('transaction_id', $txnId)->update(['status' => $payment->status, 'payment_date' =>  date('Y-m-d')]);
        if ($payment->status == 'paid') {
            PackagePurchaseMaster::where('transaction_id', $txn->id)->update(['status' => 1]);
            $customerId = PaymentCustomerid::where('user_id', $txn->user_id)->first()->customer_id;
            $getCompanyData = Company::where('id', $txn->company_id)->first();
            $intervel = ($getCompanyData->subscription_tenure == 1) ? '1 month' : '12 months';
            $paymentDesc = ($getCompanyData->subscription_tenure == 1) ? Config::get('settings.monthly_payment_desc') : Config::get('settings.yearly_payment_desc');
            TransactionDetails::where('transaction_id', $txnId)->update(['invoice_no' => invoice()]);
            ////update user and storage count start
            $parchasedData = PackagePurchaseMaster::where('transaction_id', $txn->id)->where('status', 1)->get();
            $addedUser = 0;
            $addedStorage = 0;
            if (!empty($parchasedData)) {
                foreach ($parchasedData as $eachData) {
                    if ($eachData->type == 'user') {
                        $addedUser = $addedUser + PackageUserPlan::find($eachData->package_id)->user_count;
                    }
                    if ($eachData->type == 'storage') {
                        $addedStorage = $addedStorage + PackageStoragePlan::find($eachData->package_id)->space;
                    }
                }
            }
            if (empty($getCompanyData->subscription_cancelled_at)) {
                $totUserAllowed = $getCompanyData->user_allowed + $addedUser;
                $totStorageAllowed = $getCompanyData->storage_allowed + $addedStorage;
            } else {
                $totUserAllowed = Config::get('settings.trial_user_limit') + $addedUser;
                $totStorageAllowed = Config::get('settings.trial_storage_limit') + $addedStorage;
            }
            Company::where('id', $txn->company_id)->update(['is_trial' => 0, 'user_allowed' => $totUserAllowed, 'storage_allowed' => $totStorageAllowed]);
            ////update user and storage count end
            $mandates = Mollie::api()->customersMandates()->withParentId($customerId)->all();
            if (!empty($mandates->data)) {
                if ($mandates->data[0]->status == 'pending' || $mandates->data[0]->status == 'valid') {
                    $subscription = Mollie::api()->customersSubscriptions()->withParentId($customerId)->create([
                        "amount"      => $txn->total_amount,
                        "interval"    => $intervel,
                        "startDate"   => date('Y-m-d', strtotime('+'.$intervel)),
                        "description" => Config::get('settings.payment_desc'),
                        "webhookUrl"  => url('/api/subscription-webhook')
                    ]);
                    Company::where('id', $txn->company_id)->update(['subscription_id' => $subscription->id, 'trial_end' => date('Y-m-d', strtotime('+'.$intervel)),'next_renewal_date' => date('Y-m-d', strtotime('+'.$intervel)),'subscription_amount' => $txn->amount, 'package_price' => $txn->package_price, 'user_package_price' => $txn->user_package_price, 'storage_package_price' => $txn->storage_package_price, 'subscription_cancelled_at' => null]);
                }
            }
        }
    }

    public function success(Request $req)
    {
        return View::make('pages.membership.success');
    }

    public function subscriptionWebhook(Request $req)
    {
        $txnId = $req->input('id');
        $payment = Mollie::api()->payments()->get($txnId);
        $customerData = PaymentCustomerid::where('customer_id', $payment->customerId)->first();
        $companyData = Company::where('id', $customerData->company_id)->first();
        if ($companyData->is_vat == 0) {
            $vatPrcnt = Countries::find($companyData->country)->vat_percent;
            $actualAmt = ($payment->amount/(100+$vatPrcnt)) * 100;
            $vatAmt = ($actualAmt * $vatPrcnt) / 100;
        } else {
            $actualAmt = $payment->amount;
            $vatAmt = 0.00;
        }
               
        $model = new TransactionDetails;
        $model->invoice_no = ($payment->status == 'paid') ? invoice() : '';
        $model->company_id = $customerData->company_id;
        $model->type = 'renewal';
        $model->transaction_id = $txnId;
        $model->user_id = $customerData->user_id;
        $model->profile_id = $payment->profileId;
        $model->amount =  $actualAmt;
        $model->vat_amount =  $vatAmt;
        $model->total_amount = $payment->amount;
        $model->status = $payment->status;
        $model->payment_date =  date('Y-m-d');
        $model->save();

        $companyModel = Company::find($customerData->company_id);
        $invoiceData = new InvoiceData;
        $invoiceData->company_id = $companyModel->id;
        $invoiceData->transaction_id = $model->id;
        $invoiceData->username = $companyModel->customeradmins[0]->name;
        $invoiceData->company_logo = $companyModel->photo;
        $invoiceData->company_name = $companyModel->name;
        $invoiceData->street = $companyModel->street;
        $invoiceData->nr = $companyModel->nr;
        $invoiceData->bus = $companyModel->bus;
        $invoiceData->zip = $companyModel->zip;
        $invoiceData->city = $companyModel->city;
        $invoiceData->country = $companyModel->country;
        $invoiceData->tva_no = $companyModel->tva_no;
        $invoiceData->save();
        if ($payment->status == 'paid') {
            $intervel = ($companyModel->subscription_tenure == 1) ? '1 month' : '12 months';
            $companyModel->trial_end = date('Y-m-d', strtotime('+'.$intervel));
            $companyModel->next_renewal_date = date('Y-m-d', strtotime('+'.$intervel));
        } else {
            $companyModel->status = 2;
        }
        $companyModel->save();
    }
}
