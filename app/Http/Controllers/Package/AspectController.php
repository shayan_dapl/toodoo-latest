<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PackageAspectType;
use View;
use App\Http\Requests\PackageAspectTypeRequest;
use Auth;

class AspectController extends Controller
{
    public function list()
    {
        $breadcrumbs = ['/package/aspect-type' => 'words.package_aspect_type', '#' => 'words.list'];
        return View::make('pages.package.aspect', compact('breadcrumbs'));
    }

    public function listData()
    {
        return PackageAspectType::companyStatusCheck()->get()->toJson();
    }

    public function save(PackageAspectTypeRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = PackageAspectType::findOrNew($req->input('id'));
        $model->company_id = Auth::user()->company_id;
        $model->name = $req->input('name');
        $model->save();
        return response()->json(true);
    }

    public function remove(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        PackageAspectType::where('id', $req->id)->update(['status' => 3]);
        return response()->json(true);
    }
}
