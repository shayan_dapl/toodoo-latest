<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PackageCategory;
use View;
use App\Http\Requests\PackageCategoryRequest;
use Auth;

class CategoryController extends Controller
{
    public function list()
    {
        $breadcrumbs = ['/package/category' => 'words.package_category', '#' => 'words.list'];
        return View::make('pages.package.category', compact('breadcrumbs'));
    }

    public function listData()
    {
        return PackageCategory::companyStatusCheck()->get()->toJson();
    }

    public function save(PackageCategoryRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = PackageCategory::findOrNew($req->input('id'));
        $model->company_id = Auth::user()->company_id;
        $model->name = $req->input('name');
        $model->save();
        return response()->json(true);
    }

    public function remove(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        PackageCategory::where('id', $req->id)->update(['status' => 3]);
        return response()->json(true);
    }
}
