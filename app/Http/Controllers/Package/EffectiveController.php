<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PackageEffectEnv;
use View;
use App\Http\Requests\PackageEffectEnvRequest;
use Auth;

class EffectiveController extends Controller
{
    public function list()
    {
        $breadcrumbs = ['/package/effective-env' => 'words.package_effective_env', '#' => 'words.list'];
        return View::make('pages.package.effective', compact('breadcrumbs'));
    }

    public function listData()
    {
        return PackageEffectEnv::companyStatusCheck()->get()->toJson();
    }

    public function save(PackageEffectEnvRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = PackageEffectEnv::findOrNew($req->input('id'));
        $model->company_id = Auth::user()->company_id;
        $model->name = $req->input('name');
        $model->save();
        return response()->json(true);
    }

    public function remove(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        PackageEffectEnv::where('id', $req->id)->update(['status' => 3]);
        return response()->json(true);
    }
}
