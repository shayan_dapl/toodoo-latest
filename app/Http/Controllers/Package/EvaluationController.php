<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PackageEvaluation;
use View;
use App\Http\Requests\PackageEvaluationRequest;
use Auth;

class EvaluationController extends Controller
{
    public function list()
    {
        $breadcrumbs = ['/package/evaluation' => 'words.package_evaluation', '#' => 'words.list'];
        return View::make('pages.package.evaluation', compact('breadcrumbs'));
    }

    public function listData()
    {
        return PackageEvaluation::companyStatusCheck()->get()->toJson();
    }

    public function save(PackageEvaluationRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = PackageEvaluation::findOrNew($req->input('id'));
        $model->company_id = Auth::user()->company_id;
        $model->name = $req->input('name');
        $model->save();
        return response()->json(true);
    }

    public function remove(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        PackageEvaluation::where('id', $req->id)->update(['status' => 3]);
        return response()->json(true);
    }
}
