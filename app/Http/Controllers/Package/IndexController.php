<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\AddonPackage;
use App\Models\AddonPackageAnalysis;
use App\Models\MasterKpi;
use App\Models\PackageLocation;
use App\Models\PackageSituation;
use App\Models\PackageThemeEnv;
use App\Models\PackageEffectEnv;
use App\Models\PackageCategory;
use App\Models\PackageAspectType;
use App\Models\PackageEvaluation;
use App\Models\PackageParameter;
use App\Models\PackageRating;
use App\Models\Process;
use App\Models\AddonPackageActionLog;
use App\Models\AddonPackageActionComments;
use App\Models\AddonPackageActionFiles;
use View;
use App\Http\Requests\PackageRequest;
use App\Http\Requests\AddonPackageFixingRequest;

class IndexController extends Controller
{
    public function index()
    {
        $breadcrumbs = ['#' => 'words.package'];
        $process = Auth::user()->company->processes()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $kpi = MasterKpi::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $location = PackageLocation::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $situation = PackageSituation::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $theme = PackageThemeEnv::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $effective = PackageEffectEnv::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $category = PackageCategory::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $aspect = PackageAspectType::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $evaluation = PackageEvaluation::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $parameter = PackageParameter::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $rating = PackageRating::companyStatusCheck()->pluck('rating', 'id')->prepend(translate('form.select'), '');
        $who = Auth::user()->company->users()->inServiceWithEmail()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $list = AddonPackage::companyCheck()->get();
        return View::make('pages.package.index', compact('breadcrumbs', 'process', 'kpi', 'location', 'situation', 'theme', 'effective', 'category', 'aspect', 'evaluation', 'parameter', 'rating', 'who', 'list'));
    }

    public function save(Request $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = AddonPackage::findOrNew($req->input('id'));
        $model->company_id = Auth::user()->company_id;
        $model->process_id = $req->input('process');
        $model->location_id = $req->input('location');
        $model->activity_desc = $req->input('activity_desc');
        $model->situation_id = $req->input('situation');
        $model->theme_id = $req->input('theme');
        $model->aspect_env = $req->input('aspect_env');
        $model->effective_id = $req->input('effective');
        $model->category_id = $req->input('category');
        $model->aspect_type_id = $req->input('aspect');
        $model->evaluation_id = $req->input('evaluation');
        $model->existing_actions = $req->input('existing_action');
        $model->kpi_id = $req->input('kpi');
        if ($model->save()) {
            foreach ($req->input('analysis')['parameter'] as $index => $parameter) {
                if (!empty($parameter) && !empty($req->input('analysis')['rating'][$index])) {
                    $analysis = new AddonPackageAnalysis;
                    $analysis->addon_package_id = $model->id;
                    $analysis->parameter_id = $parameter;
                    $analysis->rating_id = $req->input('analysis')['rating'][$index];
                    $analysis->save();
                }
            }
            foreach ($req->input('action')['action'] as $index => $action) {
                if (!empty($action) && !empty($req->input('action')['who'][$index]) && !empty($req->input('action')['deadline'][$index])) {
                    $log = new AddonPackageActionLog;
                    $log->company_id = Auth::user()->company_id;
                    $log->process_id = $req->input('process');
                    $log->package_id = $model->id;
                    $log->action = $action;
                    $log->delegate_to = $req->input('action')['who'][$index];
                    $log->deadline = $req->input('action')['deadline'][$index];
                    $log->save();
                }
            }
        }
        $req->session()->flash('success', translate('words.success_message'));
        return redirect('/package');
    }

    public function fixing(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        $breadcrumbs = ['#' => 'words.action'];
        $log = AddonPackageActionLog::companyCheck()->where('id', $req->id)->first();
        return View::make('pages.package.action_fixing', compact('breadcrumbs', 'log'));
    }

    public function fixingSave(AddonPackageFixingRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $comment = new AddonPackageActionComments;
        $comment->company_id = Auth::guard('customer')->user()->company_id;
        $comment->log_id = $req->input('log_id');
        $comment->reply = $req->input('reply');
        $comment->save();
        if (!empty($req->file('upload_doc'))) {
            foreach ($req->file('upload_doc') as $index => $file) {
                $filename = time() . '-' . $file->getClientOriginalName();
                $file->move(storage_path('app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents/addon_package_action_docs/'), $filename);
                $fileModel = new AddonPackageActionFiles;
                $fileModel->comment_id = $comment->id;
                $fileModel->doc_name = $filename;
                $fileModel->submitted_by = Auth::guard('customer')->user()->id;
                $fileModel->save();
            }
        }
        if ($req->input('closed')) {
            AddonPackageActionLog::where('id', $req->input('log_id'))->update(['closed' => 1, 'closed_at' => date('Y-m-d')]);
        }
        $req->session()->flash('success', translate('words.success_message'));
        return redirect('home/issues');
    }
}
