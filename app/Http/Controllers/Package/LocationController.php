<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PackageLocation;
use View;
use App\Http\Requests\PackageLocationRequest;
use Auth;

class LocationController extends Controller
{
    public function list()
    {
        $breadcrumbs = ['/package/location' => 'words.package_location', '#' => 'words.list'];
        return View::make('pages.package.location', compact('breadcrumbs'));
    }

    public function listData()
    {
        return PackageLocation::companyStatusCheck()->get()->toJson();
    }

    public function save(PackageLocationRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = PackageLocation::findOrNew($req->input('id'));
        $model->company_id = Auth::user()->company_id;
        $model->name = $req->input('name');
        $model->save();
        return response()->json(true);
    }

    public function remove(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        PackageLocation::where('id', $req->id)->update(['status' => 3]);
        return response()->json(true);
    }
}
