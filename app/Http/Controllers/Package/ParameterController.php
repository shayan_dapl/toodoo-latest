<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PackageParameter;
use View;
use App\Http\Requests\PackageParameterRequest;
use Auth;

class ParameterController extends Controller
{
    public function list()
    {
        $breadcrumbs = ['/package/parameter' => 'words.parameters', '#' => 'words.list'];
        return View::make('pages.package.parameter', compact('breadcrumbs'));
    }

    public function data()
    {
        return PackageParameter::companyStatusCheck()->get()->toJson();
    }

    public function save(PackageParameterRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = PackageParameter::findOrNew($req->input('id'));
        $model->company_id = Auth::guard('customer')->user()->company_id;
        $model->name = $req->input('name');
        $model->save();
        return response()->json(true);
    }

    public function remove(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        PackageParameter::where('id', $req->id)->update(['status' => 3]);
        return response()->json(true);
    }
}
