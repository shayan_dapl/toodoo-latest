<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PackageRating;
use View;
use App\Http\Requests\PackageRatingRequest;
use Auth;

class RatingController extends Controller
{
    public function list()
    {
        $breadcrumbs = ['/package/ratings' => 'words.ratings', '#' => 'words.list'];
        return View::make('pages.package.rating', compact('breadcrumbs'));
    }

    public function data()
    {
        return PackageRating::companyStatusCheck()->get()->toJson();
    }

    public function save(PackageRatingRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = PackageRating::findOrNew($req->input('id'));
        $model->company_id = Auth::guard('customer')->user()->company_id;
        $model->rating = $req->input('rating');
        $model->description = $req->input('description');
        $model->save();
        return response()->json(true);
    }

    public function remove(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        PackageRating::where('id', $req->id)->update(['status' => 3]);
        return response()->json(true);
    }
}
