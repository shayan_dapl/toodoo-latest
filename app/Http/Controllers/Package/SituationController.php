<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PackageSituation;
use View;
use App\Http\Requests\PackageSituationRequest;
use Auth;

class SituationController extends Controller
{
    public function list()
    {
        $breadcrumbs = ['/package/situation' => 'words.package_situation', '#' => 'words.list'];
        return View::make('pages.package.situation', compact('breadcrumbs'));
    }

    public function listData()
    {
        return PackageSituation::companyStatusCheck()->get()->toJson();
    }

    public function save(PackageSituationRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = PackageSituation::findOrNew($req->input('id'));
        $model->company_id = Auth::user()->company_id;
        $model->name = $req->input('name');
        $model->save();
        return response()->json(true);
    }

    public function remove(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        PackageSituation::where('id', $req->id)->update(['status' => 3]);
        return response()->json(true);
    }
}
