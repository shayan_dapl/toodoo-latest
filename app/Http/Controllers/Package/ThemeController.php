<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PackageThemeEnv;
use View;
use App\Http\Requests\PackageThemeEnvRequest;
use Auth;

class ThemeController extends Controller
{
    public function list()
    {
        $breadcrumbs = ['/package/theme-env' => 'words.package_theme_env', '#' => 'words.list'];
        return View::make('pages.package.theme', compact('breadcrumbs'));
    }

    public function listData()
    {
        return PackageThemeEnv::companyStatusCheck()->get()->toJson();
    }

    public function save(PackageThemeEnvRequest $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        $model = PackageThemeEnv::findOrNew($req->input('id'));
        $model->company_id = Auth::user()->company_id;
        $model->name = $req->input('name');
        $model->save();
        return response()->json(true);
    }

    public function remove(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        PackageThemeEnv::where('id', $req->id)->update(['status' => 3]);
        return response()->json(true);
    }
}
