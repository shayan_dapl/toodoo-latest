<?php

namespace App\Http\Controllers\Process;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Process;
use App\Models\Position;
use App\Models\Users;
use App\Models\CompetentRatings;
use App\Models\CompetentRatingsActionLog;
use App\Models\CompetentRatingActionComments;
use App\Models\CompetentRatingActionFiles;
use App\Models\CompetentRatingActionType;
use App\Models\Rating;
use App\Models\Branch;
use App\Http\Requests\CompetentRatingRequest;
use Auth;
use View;
use DB;
use App\Repositories\CompetentMetrix as CompetentRepo;

class CompetentController extends Controller
{
    protected $repo;

    public function __construct(CompetentRepo $competentRepo)
    {
        $this->repo = $competentRepo;
    }

    public function competentMetrix(Request $req)
    {
        $breadcrumbs = [
            '/process/list/' => 'words.process',
            '#' => 'words.competent_metrix',
        ];
        $competentParams = '';
        $positions = [];
        $processData = [];
        $processInfo = [];
        $stepInfo = [];
        $structures = [];
        $orgIds = [];
        $allPositions = Auth::user()->company->positions()->select('id as position_id', 'name as positionname')->where('status', 1)->get()->toArray();
        $positionId = $req->position_id;
        $selectedBranch = $req->branch_id;
        $selectedRating = $req->rating_id;
        $process = $this->repo->competentProcess($selectedRating, $positionId, $selectedBranch);
        foreach ($process as $eachProcess => $eachData) {
            if ($eachData['positionstatus'] == 4) {
                foreach (Auth::user()->company->positions as $each) {
                    if ($positionId != '' && $positionId != 'all' && $positionId == $each->id) {
                        $positions[$each->id] = $each->name;
                        $processData[$eachData['name']][$eachData['step_name']][] = $each->id;
                        $structures[$each->id] = Position::find($each->id);
                    }
                    if ($positionId == '' || $positionId == 'all') {
                        $positions[$each->id] = $each->name;
                        $processData[$eachData['name']][$eachData['step_name']][] = $each->id;
                        $structures[$each->id] = Position::find($each->id);
                    }
                }
            } else {
                $positions[$eachData['position_id']] = $eachData['positionname'];
                $processData[$eachData['name']][$eachData['step_name']][] = $eachData['position_id'];
                $structures[$eachData['position_id']] = Position::find($eachData['position_id']);
            }
            $processInfo[$eachData['name']] = $eachData['id'];
            $stepInfo[$eachData['name']][$eachData['step_name']] = $eachData['step_id'];
        }
        $users = Users::companyCheck()->inServiceWithEmail()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $branchData = Branch::companyStatusCheck();
        $branches = $branchData->pluck('name', 'id');
        if ($branchData->count() > 1) {
            $branches = $branches->prepend(translate('words.select_branch'), '');
        }
        $ratings = CompetentRatings::companyCheck();
        if ($selectedRating != '') {
            $orgIds = $ratings->where('rating', $selectedRating)->where('is_latest', 1)->pluck('org_id')->toArray();
            $ratings = $ratings->where('rating', $selectedRating)->where('is_latest', 1)->get()->toArray();
        } else {
            $ratings = $ratings->get()->toArray();
        }
        $ratingLevel = Rating::companyCheck()->get()->toArray();
        $actionTypes = CompetentRatingActionType::companyStatusCheck()->pluck('type', 'id')->prepend(translate('form.select'), '')->toArray();
        if ($selectedBranch != '') {
            if (($positionId != '') && ($positionId != 'all')) {
                $competentParams = '/'.$positionId.'/'.$selectedBranch;
            } else {
                $competentParams = '/all'.'/'.$selectedBranch;
            }
        } else {
            if ((($positionId != '') && ($positionId != 'all')) && ($selectedBranch != '')) {
                $competentParams = '/'.$positionId.'/'.$selectedBranch;
            } else {
                if ($positionId != '') {
                    $competentParams = '/'.$positionId;
                }
            }
        }
        return View::make('pages.process.competent_metrix', compact('breadcrumbs', 'process', 'positions', 'users', 'structures', 'positionId', 'processData', 'ratings', 'processInfo', 'stepInfo', 'ratingLevel', 'actionTypes', 'competentParams', 'branches', 'allPositions', 'selectedBranch', 'selectedRating', 'orgIds'));
    }

    public function rating(Request $req)
    {
        if (!empty($req->process) && !empty($req->step) && !empty($req->org)) {
            $data = CompetentRatings::companyCheck()->where('process_id', $req->process)->where('step_id', $req->step)->where('org_id', $req->org)->orderBy('id', 'DESC')->with('actions')->get()->toJson();
            return $data;
        } else {
            return response()->json(false);
        }
    }

    public function ratingSave(CompetentRatingRequest $req)
    {
        if (!empty($req->input())) {
            $existing = CompetentRatings::companyCheck()->where('process_id', $req->input('process'))->where('step_id', $req->input('step'))->where('org_id', $req->input('org'));
            if ($existing->count() > 0) {
                $existing->update(['is_latest' => 0]);
            }
            $rating = new CompetentRatings;
            $rating->company_id = Auth::guard('customer')->user()->company_id;
            $rating->process_id = $req->input('process');
            $rating->step_id = $req->input('step');
            $rating->org_id = $req->input('org');
            $rating->rating = $req->input('rating');
            $rating->comment = $req->input('comment');
            $rating->is_action = $req->input('isAction');
            $rating->is_latest = 1;
            if ($rating->save()) {
                if ($rating->is_action == 1) {
                    $action = new CompetentRatingsActionLog;
                    $action->company_id = Auth::guard('customer')->user()->company_id;
                    $action->rating_id = $rating->id;
                    $action->action = $req->input('action');
                    $action->action_type = $req->input('action_type');
                    $action->delegate_to = $req->input('delegateTo');
                    $action->deadline = $req->input('deadline');
                    if ($action->save()) {
                        return response()->json(true);
                    }
                }
            }
        } else {
            return response()->json(false);
        }
    }
    
    public function issueFixing(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '#' => 'words.action',
            ];
            $log = CompetentRatingsActionLog::companyCheck()->where('id', $req->id)->first();
            return View::make('pages.process.competent_rating_issue_fixing', compact('breadcrumbs', 'log'));
        }
    }

    public function issueFixingSave(Request $req)
    {
        if (!empty($req->input())) {
            //Comment adding
            if (!empty($req->input('reply'))) {
                $comment = new CompetentRatingActionComments;
                $comment->company_id = Auth::guard('customer')->user()->company_id;
                $comment->log_id = $req->input('action_log_id');
                $comment->reply = $req->input('reply');
                $comment->save();
            }

            //Supporting document uploading per comment
            if (!empty($req->file('upload_doc'))) {
                foreach ($req->file('upload_doc') as $index => $file) {
                    $filename = time() . '-' . $file->getClientOriginalName();
                    $file->move(
                        storage_path('app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents/competent_rating_action_docs/'),
                        $filename
                    );
                    $fileModel = new CompetentRatingActionFiles;
                    $fileModel->comment_id = $comment->id;
                    $fileModel->document_name = $filename;
                    $fileModel->submitted_by = Auth::guard('customer')->user()->id;
                    $fileModel->save();
                }
            }

            //Action log closed
            if ($req->input('closed')) {
                CompetentRatingsActionLog::where('id', $req->input('action_log_id'))->update(['closed' => 1, 'closed_at' => date('Y-m-d')]);
            }

            $req->session()->flash('success', translate('words.success_message'));
            return redirect('home/issues');
        }
    }

    public function actionType(Request $req)
    {
        $breadcrumbs = [
            '#' => 'words.action_type'
        ];
        return View::make('pages.company.rating_action_type', compact('breadcrumbs'));
    }

    public function actionTypeData(Request $req)
    {
        $data = CompetentRatingActionType::companyCheck()->where('status', 1)->get()->toArray();
        return response()->json($data);
    }

    public function actionTypeSave(Request $req)
    {
        if (!empty($req->input())) {
            $match = CompetentRatingActionType::companyStatusCheck()->where('type', $req->input('type'));
            if (!empty($req->input('hid'))) {
                $match = $match->where('id', '<>', $req->input('hid'));
            }

            if ($match->count() == 0) {
                $model = CompetentRatingActionType::findOrNew($req->input('hid'));
                $model->company_id = Auth::guard('customer')->user()->company_id;
                $model->type = $req->input('type');
                $model->save();
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }

    public function actionTypeRemove(Request $req)
    {
        if (!empty($req->input())) {
            $data = CompetentRatingActionType::companyStatusCheck()->where('id', $req->input('id'))->first();
            if ($data->actions->count() > 0) {
                return response()->json(false);
            } else {
                CompetentRatingActionType::where('id', $req->input('id'))->update(['status' => 3]);
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }
}
