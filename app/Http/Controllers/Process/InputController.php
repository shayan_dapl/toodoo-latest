<?php

namespace App\Http\Controllers\Process;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessInputRequest;
use App\Models\Input;
use App\Models\Process;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;

class InputController extends Controller
{
    public function processInputForm(Request $req)
    {
        $process = Process::companyStatusCheck()
                ->where('id', '<>', session('turtle_process_id'))
                ->pluck('name', 'id')
                ->prepend(translate('form.select'), '');
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/process/categorized' => 'words.process',
                '/process-step/input/form' => 'words.input',
                '#' => 'words.edit',
            ];
            $details = Input::find($req->id);
            if (!checkCompanyAccess($details)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            return View::make('pages.process.input_form', compact('breadcrumbs', 'details', 'process'));
        } else {
            $breadcrumbs = [
                '/process/categorized' => 'words.process',
                '/process-step/input/form' => 'words.input',
                '' => 'words.add',
            ];
            return View::make('pages.process.input_form', compact('breadcrumbs', 'process'));
        }
    }

    public function processInputFormSave(ProcessInputRequest $req)
    {
        if (!empty($req->input())) {
            if (!empty($req->hid)) {
                $model = Input::find($req->hid);
                $model->process_id = $req->input('process_id');
                $model->supporting_process_id = $req->input('supporting_process_id');
                $model->comment = $req->input('comment');
                $model->name = $req->input('name');
                $model->updated_by = Auth::user()->id;
            } else {
                $model = new Input;
                $model->process_id = $req->input('process_id');
                $model->supporting_process_id = $req->input('supporting_process_id');
                $model->comment = $req->input('comment');
                $model->name = $req->input('name');
                $model->status = 1;
                $model->created_by = Auth::user()->id;
                $model->updated_by = 0;
            }
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/turtle/' . session('turtle_process_id'));
            }
        }
    }

    public function processInputRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = Input::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $model->status = 3;
            $model->updated_by = Auth::user()->id;
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/turtle/' . session('turtle_process_id'));
            }
        }
    }
}
