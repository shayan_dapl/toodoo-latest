<?php

namespace App\Http\Controllers\Process;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Process;
use App\Models\Users;
use App\Models\Branch;
use App\Models\Position;
use Auth;
use View;

class JobDescriptionController extends Controller
{
    public function jobDescriptionMetrix(Request $req)
    {
        $breadcrumbs = [
            '/process/list/' => 'words.process',
            '#' => 'words.job_description_metrix'
        ];
        $positionId = $req->position_id;
        $selectedBranch = $req->branch_id;
        $competentParams = '';
        $positions = [];
        $processData = [];
        $allPositions = Auth::user()->company->positions()->select('id as position_id', 'name as positionname')->where('status', 1)->get()->toArray();
        $branchData = Branch::companyStatusCheck();
        $branches = $branchData->pluck('name', 'id');
        if ($branchData->count() > 1) {
            $branches = $branches->prepend(translate('words.select_branch'), '');
        }
        if ($selectedBranch != '') {
            if (($positionId != '') && ($positionId != 'all')) {
                $competentParams = '/'.$positionId.'/'.$selectedBranch;
            } else {
                $competentParams = '/all'.'/'.$selectedBranch;
            }
        } else {
            if ((($positionId != '') && ($positionId != 'all')) && ($selectedBranch != '')) {
                $competentParams = '/'.$positionId.'/'.$selectedBranch;
            } else {
                if ($positionId != '') {
                    $competentParams = '/'.$positionId;
                }
            }
        }
        if ($positionId != '' || $selectedBranch != '') {
            $everyOne = Position::companyCheck()->where('status', 4)->first()->id;
            $process = Process::join('process_steps', function ($join) {
                $join->on('process.id', '=', 'process_steps.process_id');
            })->join('roles', function ($join) {
                $join->on('process_steps.id', '=', 'roles.process_step_id');
            })->join('position', function ($join) {
                $join->on('position.id', '=', 'roles.position_id');
            })->leftJoin('process_branches', function ($join) {
                $join->on('process.id', '=', 'process_branches.process_id');
            })->leftJoin('position_branches', function ($join) {
                $join->on('position.id', '=', 'position_branches.position_id');
            })->select('process.id', 'process.name', 'process_steps.step_name', 'roles.position_id', 'position.id', 'position.name as positionname', 'position.status as positionstatus')->where('process.company_id', Auth::user()->company_id)->where('process.status', '<>', 3)->where('roles.role_id', 1);
            if (($positionId != '') && ($positionId != 'all')) {
                $process = $process->whereIn('roles.position_id', [$positionId, $everyOne]);
            }
            if ($selectedBranch != '') {
                $process = $process->where('process_branches.branch_id', $selectedBranch)->where(function ($query) use ($selectedBranch, $everyOne) {
                    $query->where('position_branches.branch_id', $selectedBranch)->orWhere('position.id', $everyOne);
                });
            }
            $process = $process->distinct('roles.process_step_id')->get();
        } else {
            $process = Process::join('process_steps', function ($join) {
                $join->on('process.id', '=', 'process_steps.process_id');
            })->join('roles', function ($join) {
                $join->on('process_steps.id', '=', 'roles.process_step_id');
            })->join('position', function ($join) {
                $join->on('position.id', '=', 'roles.position_id');
            })->select('process.id', 'process.name', 'process_steps.step_name', 'roles.position_id', 'position.name as positionname', 'position.status as positionstatus')->where('process.company_id', Auth::user()->company_id)->where('process.status', '<>', 3)->where('roles.role_id', 1)->distinct('roles.process_step_id')->get();
        }
        foreach ($process as $eachProcess => $eachData) {
            if ($eachData['positionstatus'] == 4) {
                foreach (Auth::user()->company->positions as $each) {
                    if ($positionId != '' && $positionId != 'all' && $positionId == $each->id) {
                        $positions[$each->id] = $each->name;
                        $processData[$eachData['name']][$eachData['step_name']][] = $each->id;
                    }
                    if ($positionId == '' || $positionId == 'all') {
                        $positions[$each->id] = $each->name;
                        $processData[$eachData['name']][$eachData['step_name']][] = $each->id;
                    }
                }
            } else {
                $positions[$eachData['position_id']] = $eachData['positionname'];
                $processData[$eachData['name']][$eachData['step_name']][] = $eachData['position_id'];
            }
        }
        $users = Users::companyCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        return View::make('pages.process.job_description_metrix', compact('breadcrumbs', 'process', 'positions', 'users', 'allPositions', 'positionId', 'selectedBranch', 'processData', 'branches', 'competentParams'));
    }
}
