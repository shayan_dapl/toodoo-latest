<?php

namespace App\Http\Controllers\Process;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessKpiRequest;
use App\Models\Kpi;
use App\Models\Process;
use App\Models\KpiUnit;
use App\Models\MasterKpi;
use App\Models\ProcessKpiData;
use App\Models\KpiToMasterKpi;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;
use Config;

class KpiController extends Controller
{
    private $breadcrumbs;

    public function processKpiForm(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
            '/process/categorized' => 'words.process',
            '/process-step/kpi/form' => 'words.kpi',
            '#' => 'words.edit',
            ];
            $details = Kpi::find($req->id);
            if (!checkCompanyAccess($details)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            
            $process = Process::companyCheck()->get();
            $masterKpi = MasterKpi::companyCheck()->get()->pluck('name', 'id');
            $frequency = ['' => translate('form.select'), 'daily' => translate('words.daily'),'weekly' => translate('words.weekly'), 'monthly' => translate('words.monthly'), 'quarterly' => translate('words.quarterly')];
            $langArr = Config::get('settings.lang');
            $kpi_unit_name = 'name_' . $langArr[session('lang')];
            $kpiUnits = KpiUnit::all()->pluck($kpi_unit_name, 'id')->prepend(translate('form.select'), '');
            $boolOptions = ['' => translate('form.select'), '1' => translate('words.true'), '0' => translate('words.false')];
            $trendOptions = ['' => translate('form.select'), '1' => translate('words.up'), '0' => translate('words.down')];
            return View::make('pages.process.kpi_form', compact('breadcrumbs', 'details', 'process', 'masterKpi', 'kpiUnit', 'frequency', 'kpiUnits', 'boolOptions', 'trendOptions'));
        } else {
            $breadcrumbs = [
            '/process/categorized' => 'words.process',
            '/process-step/kpi/form' => 'words.kpi',
            '#' => 'words.add',
            ];
            $process = Process::companyCheck()->get();
            $masterKpi = MasterKpi::companyCheck()->get()->pluck('name', 'id');
            $frequency = ['' => translate('form.select'), 'daily' => translate('words.daily'),'weekly' => translate('words.weekly'), 'monthly' => translate('words.monthly'), 'quarterly' => translate('words.quarterly')];
            $langArr = Config::get('settings.lang');
            $kpi_unit_name = 'name_' . $langArr[session('lang')];
            $kpiUnits = KpiUnit::all()->pluck($kpi_unit_name, 'id')->prepend(translate('form.select'), '');
            $boolOptions = ['' => translate('form.select'), '1' => translate('words.true'), '0' => translate('words.false')];
            $trendOptions = ['' => translate('form.select'), '1' => translate('words.up'), '0' => translate('words.down')];
            return View::make('pages.process.kpi_form', compact('breadcrumbs', 'process', 'masterKpi', 'kpiUnit', 'frequency', 'kpiUnits', 'boolOptions', 'trendOptions'));
        }
    }

    public function processKpiFormSave(ProcessKpiRequest $req)
    {
        if (!empty($req->input())) {
            $target = '';
            if ($req->input('targettxt') != '') {
                $target = $req->input('targettxt');
            } elseif ($req->input('targetboolopt') != '') {
                $target = $req->input('targetboolopt');
            } else {
                $target = $req->input('targettrndopt');
            }
            $model = Kpi::findOrNew($req->hid);
            $model->process_id = $req->input('process_id');
            $model->name = $req->input('kpi_name');
            $model->input = $req->input('input');
            $model->valid_from = date(Config::get('settings.db_date'), strtotime($req->input('valid_from')));
            $model->valid_to = date(Config::get('settings.db_date'), strtotime($req->input('valid_to')));
            $model->frequency = $req->input('frequency');
            $model->kpi_unit_id = $req->input('kpi_unit_id');
            $model->target = $target;
            $model->status = 1;
            $model->save();
            KpiToMasterKpi::where('kpi_id', $model->id)->delete();
            if (!empty($req->input('master_kpi'))) {
                $kpiToMasterData = [];
                foreach ($req->input('master_kpi') as $each) {
                    $kpiToMasterData[] = [
                        'master_kpi_id' => $each,
                        'kpi_id' => $model->id,
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                }
                KpiToMasterKpi::insert($kpiToMasterData);
            }
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/process/turtle/' . session('turtle_process_id'));
        }
    }

    public function processKpiRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = Kpi::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $model->status = 3;
            $model->updated_by = Auth::user()->id;
            if ($model->save()) {
                KpiToMasterKpi::where('kpi_id', $model->id)->delete();
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/turtle/' . session('turtle_process_id'));
            }
        }
    }

    public function processKpiDataList(Request $req)
    {
        $breadcrumbs = [
            '/process/categorized' => 'words.process',
            '#' => 'words.list',
        ];
        $details = Kpi::find($req->id);
        if (!checkCompanyAccess($details)) {
            $req->session()->flash('error', translate('words.you_dont_have_access'));
            return redirect('/home');
        }
        $frequency = ['' => translate('form.select'), 'daily' => translate('words.daily'), 'weekly' => translate('words.weekly'), 'monthly' => translate('words.monthly'), 'quarterly' => translate('words.quarterly')];
        $langArr = Config::get('settings.lang');
        $kpiUnitName = 'name_' . $langArr[session('lang')];
        $kpiUnits = KpiUnit::all()->pluck($kpiUnitName, 'id')->prepend(translate('form.select'), '');
        $boolOptions = ['' => translate('form.select'), '1' => translate('words.true'), '0' => translate('words.false')];
        $trendOptions = ['' => translate('form.select'), '1' => translate('words.up'), '0' => translate('words.down')];
        return View::make('pages.process_kpi.process_kpi_data_list', compact('breadcrumbs', 'details', 'boolOptions', 'trendOptions', 'frequency', 'kpiUnits'));
    }

    public function processKpiData(Request $req)
    {
        $data = ProcessKpiData::where('kpi_id', $req->id)->where('status', 1)->get();
        return response()->json($data);
    }

    public function processKpiGraph(Request $req)
    {
        $graph = ['label' => [], 'detail' => []];
        $processKpidata = ProcessKpiData::companyStatusCheck()->where('kpi_id', $req->id)->get();
        if (!empty($processKpidata)) {
            foreach ($processKpidata as $eachData) {
                $graph['label'][] = $eachData->period;
                $graph['detail'][] = $eachData->kpi_value;
            }
        }
        return response()->json($graph);
    }

    public function processKpiDataSave(Request $req)
    {
        if (!empty($req->input())) {
            $kpiVal = '';
            if ($req->input('target') != '') {
                $kpiVal = $req->input('target');
            } elseif ($req->input('targetBool') != '') {
                $kpiVal = $req->input('targetBool');
            } else {
                $kpiVal = $req->input('targetTrend');
            }
            $model = ProcessKpiData::findOrNew($req->hid);
            $model->company_id = Auth::user()->company_id;
            $model->kpi_id = $req->input('masterKpiId');
            $model->kpi_value = $kpiVal;
            $model->period = $req->input('period');
            $model->status = 1;
            if ($model->save()) {
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        }
    }

    public function processKpiDataRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = ProcessKpiData::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $model->status = 3;
            if ($model->save()) {
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }
}
