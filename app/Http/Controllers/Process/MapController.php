<?php

namespace App\Http\Controllers\Process;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Config;
use App\Models\Process;
use App\Models\Risk;
use View;
use App\Models\Opportunity;
use App\Models\Position;
use App\Models\Roles;
use App\Models\ProcessSteps;
use App\Models\ProcessDocumentTypes;
use App\Models\ProcessRegulationTypes;
use App\Models\Resources;
use App\Models\ResourcesType;
use App\Models\ResourceActivity;
use App\Models\ResourceWiseActivity;
use Auth;
use Excel;

class MapController extends Controller
{
    public function processRiskMap()
    {
        $breadcrumbs = [
            '/process/list/' => 'words.process',
            '#' => 'words.risk_map',
        ];
        $levels = Config::get('settings.risk_opp_level');
        $processes = Process::companyStatusCheck()->pluck('id')->toArray();
        foreach ($levels as $eachLevel => $riskData) {
            $risks = Risk::whereIn('process_id', $processes)->where('risk_level', $eachLevel)->where('status', 1);
            if ($risks->count() > 0) {
                $levels[$eachLevel] = $risks->get();
            }
        }
        return View::make('pages.process.risk_map', compact('breadcrumbs', 'levels'));
    }

    public function processOpportunityMap()
    {
        $breadcrumbs = [
            '/process/list/' => 'words.process',
            '#' => 'words.opportunity_map',
        ];
        $levels = Config::get('settings.risk_opp_level');
        $processes = Process::companyStatusCheck()->pluck('id')->toArray();
        foreach ($levels as $eachLevel => $opportunityData) {
            $opportunities = Opportunity::whereIn('process_id', $processes)->where('level', $eachLevel)->where('status', 1);
            if ($opportunities->count() > 0) {
                $levels[$eachLevel] = $opportunities->get();
            }
        }
        return View::make('pages.process.opportunity_map', compact('breadcrumbs', 'levels'));
    }

    public function processRACIMap(Request $req)
    {
        $breadcrumbs = [
            '/process/list/' => 'words.process',
            '#' => 'words.raci_map',
        ];

        $roles = [];
        $processes = Process::companyStatusCheck()->get();
        $process = [];
        if (!empty($req->process_id)) {
            $process = Process::find($req->process_id);
            $positions = Position::companyStatusCheck()->orWhere('status', 4);
            if ($positions->count() > 0) {
                foreach ($positions->get() as $eachPosition) {
                    $rolesData = Roles::where('process_id', $req->process_id)->where('position_id', $eachPosition->id)->where('status', 1);
                    if ($rolesData->count() > 0) {
                        $roles[$eachPosition->name] = $rolesData->select('role_id', 'process_step_id')->get()->toArray();
                    }
                }
            }
        }
        $steps = ProcessSteps::where('process_id', $req->process_id)->where('status', 1)->get();

        return View::make('pages.process.raci_map', compact('breadcrumbs', 'processes', 'steps', 'roles', 'process'));
    }

    public function processRACIMapDetails(Request $req)
    {
        $posId = Position::where('name', $req->posname)->companyCheck()->first()->id;
        $roles = Roles::where('position_id', $posId)->where('status', 1)->get();
        return View::make('pages.process.raci_detail', compact('roles'));
    }

    public function processDocumentMap()
    {
        $breadcrumbs = [
            '/process/list/' => 'words.process',
            '#' => 'words.document_map',
        ];
        $details = [];
        $docTypes = ProcessDocumentTypes::companyStatusCheck()->get();
        foreach ($docTypes as $eachDoc) {
            $allowData = [];
            if ($eachDoc->documentdocs->count() > 0) {
                foreach ($eachDoc->documentdocs as $each) {
                    if ($each->process->status == 1) {
                        $allowData[] = $each;
                    }
                }
            }
            $details[$eachDoc->type_name] = $allowData;
        }
        return View::make('pages.process.document_map', compact('breadcrumbs', 'details'));
    }

    public function processRegulationMap()
    {
        $breadcrumbs = [
            '/process/list/' => 'words.process',
            '#' => 'words.regulation_map',
        ];
        $details = [];
        $docTypes = ProcessRegulationTypes::companyStatusCheck()->get();
        foreach ($docTypes as $eachDoc) {
            $allowData = [];
            if ($eachDoc->regulationdocs->count() > 0) {
                foreach ($eachDoc->regulationdocs as $each) {
                    if ($each->process->status == 1) {
                        $allowData[] = $each;
                    }
                }
            }
            $details[$eachDoc->type_name] = $allowData;
        }
        return View::make('pages.process.regulation_map', compact('breadcrumbs', 'details'));
    }

    public function processResourceMap(Request $req)
    {
        $breadcrumbs = ['#' => translate('words.resource_map')];
        $resources = Resources::companyCheck()->get();
        return View::make('pages.process.resource_map', compact('breadcrumbs', 'resources'));
    }

    public function processResourceCalender(Request $req)
    {
        $breadcrumbs = [
            '/process/list/' => 'words.process',
            '#' => 'words.resource_calender',
        ];
        $selectedResource = $req->resource_type;
        $selectedBrand = $req->brand;
        $selectedOwner = $req->owner;
        $selectedActivity = $req->activity;
        $resource = isset($req->resource_type) ? base64_decode($req->resource_type) : '';
        $brand = isset($req->brand) ? base64_decode($req->brand) : '';
        $owner = isset($req->owner) ? base64_decode($req->owner) : '';
        $activity = isset($req->activity) ? base64_decode($req->activity) : '';
        $myResourceData = [];
        $curYear = ($req->year != '') ? $req->year : date('Y');
        $selectedYear = $req->year;
        $prvYear = $curYear - 1;
        $nxtYear = $curYear + 1;
        $prvlink = $prvYear;
        $nxtlink = $nxtYear;
        if ($resource != '' || $brand != '' || $owner != '' || $activity != '') {
            $prvlink = $selectedResource.'/'.$selectedBrand.'/'.$selectedOwner.'/'.$selectedActivity.'/'.$prvYear;
            $nxtlink = $selectedResource.'/'.$selectedBrand.'/'.$selectedOwner.'/'.$selectedActivity.'/'.$nxtYear;
        }
        //Fetch data
        $allResourceActData = ResourceWiseActivity::companyCheck();
        //Resource Filter
        if ($resource != '-1' && $resource != '') {
            $allResourceActData = $allResourceActData->whereHas('resource', function ($q) use ($resource) {
                $q->where('resource_id', $resource);
            });
        }
        //Brand Filter
        if ($brand != '-1' && $brand != '') {
            $allResourceActData = $allResourceActData->whereHas('resource', function ($q) use ($brand) {
                $q->where('brand', $brand);
            });
        }
        //Owner Filter
        if ($owner != '-1' && $owner != '') {
            $allResourceActData = $allResourceActData->whereHas('activity', function ($q) use ($owner) {
                $q->where('process_owner', $owner);
            });
        }
        //Activity filter
        if ($activity != '-1' && $activity != '') {
            $allResourceActData = $allResourceActData->whereHas('activity', function ($q) use ($activity) {
                $q->where('activity_id', $activity);
            });
        }
        //Year Filter
        $allResourceActData = $allResourceActData->with(['delegations' => function ($q) use ($curYear) {
            $q->where('deadline', 'LIKE', $curYear . '%');
        }]);
        $allResourceActData = $allResourceActData->get();
        //Generate calender data
        $uniqueType = [];
        foreach ($allResourceActData as $each=>$eachActivity) {
            $uniqueType[] = $eachActivity;
        }
        $allResourceCalender = resourceCalender($uniqueType, $curYear);
        //Filter Data
        $resourceType = ResourcesType::companyStatusCheck()->get()->pluck('name', 'encoded_id')->prepend(translate('form.select_resource_type'), base64_encode('-1'));
        $brands = Resources::with('process')->get()->where('process.company_id', Auth::user()->company_id)->pluck('brand', 'encoded_brand')->prepend(translate('form.select_brand'), base64_encode('-1'));
        $processOwners = Auth::guard('customer')->user()->company->users()->where('can_process_owner', 1)->get()->pluck('name', 'encoded_id')->prepend(translate('form.select_owner'), base64_encode('-1'));
        $activity = ResourceActivity::companyStatusCheck()->get()->pluck('activity', 'encoded_id')->prepend(translate('form.select_activity'), base64_encode('-1'));
        $months = [translate('words.january'), translate('words.february'), translate('words.march'), translate('words.april'), translate('words.may'), translate('words.june'), translate('words.july'), translate('words.august'), translate('words.september'), translate('words.october'), translate('words.november'), translate('words.december')];
        return View::make('pages.process.resource_calendar', compact('breadcrumbs', 'allResourceCalender', 'months', 'curYear', 'prvlink', 'nxtlink', 'resourceType', 'brands', 'processOwners', 'activity', 'selectedYear', 'selectedResource', 'selectedBrand', 'selectedOwner', 'selectedActivity'));
    }

    public function resourceActionEventLog(Request $req)
    {
        if (!empty($req->activityId)) {
            $data = ResourceWiseActivity::find($req->activityId);
            return View::make('pages.process.resource_event_log', compact('data', 'type'));
        }
    }
}
