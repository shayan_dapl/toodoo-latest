<?php

namespace App\Http\Controllers\Process;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessOutputRequest;
use App\Models\Input;
use App\Models\Output;
use App\Models\Process;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;

class OutputController extends Controller
{
    public function processOutputForm(Request $req)
    {
        $process = Process::companyStatusCheck()->where('id', '<>', session('turtle_process_id'))->pluck('name', 'id')->prepend(translate('form.select'), '');
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/process/categorized' => 'words.process',
                '/process-step/output/form' => 'words.output',
                '#' => 'words.edit',
            ];
            $details = Output::find($req->id);
            if (!checkCompanyAccess($details)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            return View::make('pages.process.output_form', compact('breadcrumbs', 'details', 'process'));
        } else {
            $breadcrumbs = [
                '/process/categorized' => 'words.process',
                '/process-step/output/form' => 'words.output',
                '#' => 'words.add',
            ];

            return View::make('pages.process.output_form', compact('breadcrumbs', 'process'));
        }
    }

    public function processOutputFormSave(ProcessOutputRequest $req)
    {
        if (!empty($req->input())) {
            if (!empty($req->hid)) {
                $model = Output::find($req->hid);
                $model->process_id = $req->input('process_id');
                $model->supporting_process_id = $req->input('supporting_process_id');
                $model->comment = $req->input('comment');
                $model->output = $req->input('output');
                $model->updated_by = Auth::user()->id;
            } else {
                $model = new Output;
                $model->process_id = $req->input('process_id');
                $model->supporting_process_id = $req->input('supporting_process_id');
                $model->comment = $req->input('comment');
                $model->output = $req->input('output');
                $model->status = 1;
                $model->created_by = Auth::user()->id;
                $model->updated_by = 0;
            }
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/turtle/' . session('turtle_process_id'));
            }
        }
    }

    public function processOutputRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = Output::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $model->status = 3;
            $model->updated_by = Auth::user()->id;
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/turtle/' . session('turtle_process_id'));
            }
        }
    }
}
