<?php

namespace App\Http\Controllers\Process;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessProcedureRequest;
use App\Models\Language;
use App\Models\ProcedureDocs;
use App\Models\Procedures;
use App\Models\Process;
use App\Models\ProcessDocumentTypes;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Session;
use View;
use Config;
use Mail;
use App\Mail\DocChangeNotify;

class ProcedureController extends Controller
{
    public function processProcedureForm(Request $req)
    {
        $process = Process::companyCheck()->get();
        $doctypes = ProcessDocumentTypes::companyCheck()->where('status', '<>', 3)->pluck('type_name', 'id')->prepend(translate('form.select'), '');
        $language = Language::all()->pluck('display_text', 'name')->prepend(translate('words.ml'), 'ml')->prepend(translate('form.select'), '');
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/process/categorized' => 'words.process',
                '/process-step/procedure/form' => 'words.procedures',
                '#' => 'words.edit',
            ];
            $details = Procedures::find($req->id);
            if (!checkCompanyAccess($details)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $version = '';
            $source_id = 0;
            if (!empty($req->version) && $req->version == 'version') {
                $version = $req->version;
                $source_id = Procedures::where('process_id', $details->process_id)
                    ->where('type', 'P')
                    ->where('status', '<>', 3)
                    ->where('doc_ref', $details->doc_ref)
                    ->whereRaw('id IN(SELECT MIN(id) FROM `procedures` WHERE `type`="P" AND process_id='.$details->process_id.' GROUP BY `doc_ref`)')
                    ->first();
                if (!empty($source_id)) {
                    $source_id = $source_id->id;
                } else {
                    $source_id = '';
                }
            }
            return View::make('pages.process.procedure_form', compact('breadcrumbs', 'details', 'process', 'doctypes', 'language', 'version', 'source_id'));
        } else {
            $breadcrumbs = [
                '/process/categorized' => 'words.process',
                '/process-step/procedure/form' => 'words.procedures',
                '#' => 'words.add',
            ];
            return View::make('pages.process.procedure_form', compact('breadcrumbs', 'process', 'doctypes', 'language'));
        }
    }

    public function processProcedureFormSave(ProcessProcedureRequest $req)
    {
        if (!empty($req->input())) {
            switch ($req->input('version_number')) {
                case 'version':
                    $model = new Procedures;
                    $model->version = $req->input('version');
                    $model->created_by = Auth::user()->id;
                    $model->updated_by = 0;
                    $getLastVersion = Procedures::where('process_id', $req->input('process_id'))->where('type', 'P')->where('doc_ref', $req->input('doc_ref'))->orderBy('id', 'DESC')->first();
                    Procedures::where('id', $getLastVersion->id)->update(['remark'=> $req->input('remark')]);
                    break;
                case '':
                    $model = Procedures::findOrNew($req->input('hid'));
                    $model->version = !empty($model->exists) ? $model->version : $req->input('version');
                    $model->created_by = !empty($model->exists) ? $model->created_by : Auth::user()->id;
                    $model->updated_by = !empty($model->exists) ? Auth::user()->id : 0;
                    break;
            }
            $model->type = 'P';
            $model->process_id = $req->input('process_id');
            $model->doc_ref = $req->input('doc_ref');
            $model->doc_type_id = empty($req->input('doc_type_id')) ? 0 : $req->input('doc_type_id');
            $model->language = $req->input('language');
            $model->status = 1;
            $model->save();
            if (!empty($req->input('hid')) && !empty($req->input('version_number'))) {
                Procedures::where('id', $req->input('hid'))->update(['is_archived' => 1]);
            }
            $procedureDocModel = new ProcedureDocs;
            $procedureDocModel->procedure_id = $model->id;
            $procedureDocModel->source_id = (!empty($req->input('source_id') && !empty($req->input('version_number'))) && $req->input('source_id') != 0) ? $req->input('source_id') : $model->id;
            $procedureDocModel->status = 1;
            $procedureDocModel->created_by = Auth::user()->id;
            $procedureDocModel->updated_by = 0;
            if (($req->input('upload_doc') == 'Yes' || !empty($req->input('version_number'))) && !empty($req->file('upload_doc'))) {
                $filename = $req->file('upload_doc')->getClientOriginalName();
                $req->file('upload_doc')->move(storage_path('app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents/process_docs/'), $filename);
                $procedureDocModel->doc_name = $filename;
                $procedureDocModel->save();
            }
            if (!empty($req->input('location_doc'))) {
                $procedureDocModel->location_doc = $req->input('location_doc');
                $procedureDocModel->save();
            }
            if ($req->input('inform_user') != null) {
                $process = $model->process;
                $docType = !empty($req->input('doc_type_id')) ? ProcessDocumentTypes::find($req->input('doc_type_id'))->type_name : '';
                informUser($process, $req, $docType);
            }
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/process/turtle/' . session('turtle_process_id'));
        }
    }

    public function processProcedureRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = Procedures::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            Procedures::where('doc_ref', $model->doc_ref)->where('type', 'P')->update(['status' => 3]);
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/process/turtle/' . session('turtle_process_id'));
        }
    }

    public function processProcedureDocRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = ProcedureDocs::find($req->id);
            if (file_exists(public_path('process_docs/' . $model->doc_name))) {
                unlink(public_path('process_docs/' . $model->doc_name));
            }
            $model->delete();
            return redirect()->back();
        }
    }

    public function processProcedureArchive(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                ('process/turtle/' . $req->turtle_id) => 'words.turtle',
                '#' => 'words.archive',
            ];
            $data = ProcedureDocs::where('source_id', $req->id)
                ->where('status', '<>', 3)
                ->orderBy('id', 'DESC')
                ->get();
            array_forget($data, 0);
            return View::make('pages.process.procedures_archive', compact('breadcrumbs', 'data'));
        }
    }

    public function downloadDoc(Request $req)
    {
        $pathToFile = storage_path('/app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents'.'/'.$req->doctype.'/' . $req->docTitle);
        return response()->download($pathToFile);
    }
}
