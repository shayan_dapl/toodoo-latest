<?php

namespace App\Http\Controllers\Process;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessRequest;
use App\Models\Input;
use App\Models\Kpi;
use App\Models\Opportunity;
use App\Models\Output;
use App\Models\PackageMaster;
use App\Models\Procedures;
use App\Models\Process;
use App\Models\ProcessCategory;
use App\Models\ProcessDocumentTypes;
use App\Models\ProcessSteps;
use App\Models\Risk;
use App\Models\Roles;
use App\Models\Scope;
use App\Models\Users;
use App\Models\Branch;
use App\Models\ProcessBranch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Auth;
use Session;
use URL;
use View;
use DB;

class ProcessController extends Controller
{
    public function makeStatusChange(Request $req)
    {
        if ($req->model == "Process") {
            $details = Process::find($req->id);
            if (!checkCompanyAccess($details)) {
                return response(translate('words.you_dont_have_access'), 200)->header('Content-Type', 'text/plain');
            } else {
                if ($details->parent_id == 0 || $details->parent_process->status != 0) {
                    Process::where('id', $req->id)->update(['status' => $req->status]);
                    if ($req->status == 0) {
                        Process::where('parent_id', $req->id)->update(['status' => $req->status]);
                    }
                } else {
                    return response("faliure", 200)->header('Content-Type', 'text/plain');
                }
            }
        }
        if ($req->model == "ProcessDocumentTypes") {
            $details = ProcessDocumentTypes::find($req->id);
            $details->status = $req->status;
        }
        if ($req->model == "PackageMaster") {
            $details = PackageMaster::find($req->id);
            $details->status = $req->status;
        }

        if ($details->save()) {
            return response("success", 200)->header('Content-Type', 'text/plain');
        }
    }

    public function processList()
    {
        $breadcrumbs = [
            '/process/list/' => 'words.process',
            '#' => 'words.list',
        ];
        $details = Process::companyCheck()->orderBy('id', 'DESC')->get();
        return View::make('pages.process.list', compact('breadcrumbs', 'details'));
    }

    public function categorizedProcess(Request $req)
    {
        $breadcrumbs = [
            '/process/list/' => 'words.process',
            '#' => 'words.company_map',
        ];
        $selectedBranch = '';
        $filteredProcess = ProcessBranch::companyCheck();
        if (!empty($req->branchId)) {
            $selectedBranch = $req->branchId;
            $filteredProcess = $filteredProcess->where('branch_id', $selectedBranch);
        }
        $filteredProcess = $filteredProcess->pluck('process_id')->toArray();
        $details = ProcessCategory::all();
        $branches = Branch::companyStatusCheck()->pluck('name', 'id')->prepend(translate('words.all'), '');
        return View::make('pages.process.categorized_process_list', compact('breadcrumbs', 'details', 'branches', 'selectedBranch', 'filteredProcess'));
    }

    public function processForm(Request $req)
    {
        //Backtracking previous two steps
        $segments = explode("/", URL::previous());
        $backtrace = $segments[count($segments) - 2] . '/' . $segments[count($segments) - 1];
        $req->session()->flash('backtrace', $backtrace);

        $name = [
            'nl' => 'name_nl',
            'en' => 'name_en',
            'fr' => 'name_fr',
        ];

        $category = ProcessCategory::where('status', 1)->pluck($name[session('lang')], 'id');
        $processOwners = Users::companyCheck()->where('can_process_owner', 1)->with('company')->count();
        $owners = Users::companyCheck()->where('can_process_owner', 1)->with('company')->pluck('name', 'id')->prepend(translate('form.select'), '');
        $company = Auth::guard('customer')->user()->company;
        $parentProcess = Process::companyStatusCheck()->where('parent_id', 0)->pluck('name', 'id')->prepend(translate('form.select'), '0');
        $subprocess = "hide";
        $nocategory = "";
        $branches = Branch::companyStatusCheck()->pluck('name', 'id');
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/process/list' => 'words.process',
                '#' => 'words.edit',
            ];

            $details = Process::find($req->id);
            if (!checkCompanyAccess($details)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            if ($details->parent_id != 0) {
                $subprocess = "";
                $nocategory = "hide";
            }
            foreach ($details->branches as $branch) {
                $selectedBranches [] = $branch->branch_id;
            }
            return View::make('pages.process.form', compact('breadcrumbs', 'details', 'category', 'company', 'owners', 'subprocess', 'nocategory', 'parentProcess', 'branches', 'selectedBranches', 'processOwners'));
        } else {
            $breadcrumbs = [
                '/process/list' => 'words.process',
                '#' => 'words.add',
            ];
            if ($branches->count() == 1) {
                foreach ($branches as $eachBranch=>$curBranch) {
                    $selectedBranches [] = $eachBranch;
                }
            } else {
                $selectedBranches = [];
            }
            return View::make('pages.process.form', compact('breadcrumbs', 'category', 'company', 'owners', 'subprocess', 'nocategory', 'parentProcess', 'branches', 'processOwners', 'selectedBranches'));
        }
    }

    public function processFormSave(ProcessRequest $req)
    {
        if (!empty($req->input())) {
            $model = Process::findOrNew($req->hid);
            if ($req->input('is_sub_process') != null) {
                $model->parent_id = $req->input('parentProcess');
                $parentCat = Process::find($req->input('parentProcess'))->category_id;
                $model->category_id = $parentCat;
            } else {
                $model->parent_id = 0;
                $model->category_id = $req->input('category_id');
            }
            $model->owner_id = $req->input('owner_id');
            $model->company_id = Auth::user()->company_id;
            $model->no_turtle_process = ($req->input('no_turtle_process') != null) ? 1 : 0;
            $model->name = $req->input('name');
            $model->status = 1;
            if (empty($req->hid)) {
                $model->created_by = Auth::user()->id;
            }
            $model->updated_by = !empty($req->hid) ? Auth::user()->id : 0;
            if ($model->save()) {
                ProcessBranch::where('process_id', $model->id)->delete();
                foreach ($req->input('branches') as $eachBranch) {
                    $processBranchModel = new ProcessBranch;
                    $processBranchModel->company_id = Auth::user()->company_id;
                    $processBranchModel->process_id = $model->id;
                    $processBranchModel->branch_id =  $eachBranch;
                    $processBranchModel->save();
                }
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/list');
            }
        }
    }

    public function processRemove(Request $req)
    {
        if (!empty($req->id)) {
            $allow = 0;
            $model = Process::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $allow = $model->scopes()->count() + $model->procedures()->count() + $model->kpis()->count() + $model->inputs()->count() + $model->steps()->count() + $model->outputs()->count() + $model->risks()->count() + $model->opportunities()->count() + $model->roles()->count() + $model->audits()->count() + $model->resources()->count() +$model->context()->count();

            if ($allow > 0) {
                $req->session()->flash('error', translate('error.process_data_exists'));
                return redirect('/process/list');
            } else {
                $model->delete();
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/list');
            }
        }
    }

    public function changeProcess(Request $req)
    {
        if ($req->process_id != 0) {
            Session::put('turtle_process_id', $req->process_id);
            return "set";
        }
        if ($req->process_id == 0) {
            Session::forget('turtle_process_id');
            return "unset";
        }
    }

    public function stepsRearrange(Request $req)
    {
        if (!empty($req->input('ids'))) {
            $idsArr = explode(",", $req->input('ids'));
            $ids = count($idsArr);
            for ($i = 0; $i < $ids; $i++) {
                ProcessSteps::where('id', $idsArr[$i])->update(['serial' => ($i + 1)]);
            }
        }
    }

    public function processRearrange(Request $req)
    {
        if (!empty($req->input('ids'))) {
            $idsArr = explode(",", $req->input('ids'));
            $ids = count($idsArr);
            for ($i = 0; $i < $ids; $i++) {
                Process::where('id', $idsArr[$i])->update(['serial' => ($i + 1)]);
            }
        }
    }
}
