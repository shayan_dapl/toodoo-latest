<?php

namespace App\Http\Controllers\Process;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ProcessRegulationTypes;
use App\Models\Process;
use App\Http\Requests\ProcessDocTypeRequest;
use App\Models\Procedures;
use View;
use Auth;

class RegulationTypeController extends Controller
{
    public function list()
    {
        $breadcrumbs = [
            '/process/list' => 'words.process',
            '/process/regulationtype' => 'words.regulationtype'
        ];
        return View::make('pages.process.regulariontype_list', compact('breadcrumbs'));
    }

    public function listData()
    {
        $data = ProcessRegulationTypes::companyStatusCheck()->get();
        return response()->json($data);
    }

    public function save(ProcessDocTypeRequest $req)
    {
        if (!empty($req->input())) {
            $model = ProcessRegulationTypes::findOrNew($req->input('id'));
            $model->company_id = Auth::guard('customer')->user()->company_id;
            $model->type_name = $req->input('type_name');
            $model->status = 1;
            if ($model->save()) {
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }

    public function remove(Request $req)
    {
        if (!empty($req->id)) {
            $process = Process::companyStatusCheck()->pluck('id');
            $data = Procedures::where('doc_type_id', $req->id)->where('status', 1)->whereIn('process_id', $process)->count();
            if ($data > 0) {
                return response()->json(false);
            } else {
                ProcessRegulationTypes::where('id', $req->id)->update(['status' => 3]);
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }
}
