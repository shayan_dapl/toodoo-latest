<?php

namespace App\Http\Controllers\Process;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ResourceActivityRequest;
use App\Models\ResourceActivity;
use Auth;
use Session;
use View;

class ResourceActivityController extends Controller
{
    public function list()
    {
        $breadcrumbs = [
            '#' => 'words.resource_activity',
        ];
        return View::make('pages.process.resource_activity_list', compact('breadcrumbs'));
    }

    public function data()
    {
        return ResourceActivity::companyStatusCheck()->get()->toJson();
    }

    public function save(ResourceActivityRequest $req)
    {
        if (!empty($req->input())) {
            $model = ResourceActivity::findOrNew($req->input('hid'));
            $model->activity = $req->input('activity');
            $model->status = 1;
            $model->company_id = Auth::guard('customer')->user()->company_id;
            if ($model->save()) {
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }

    public function remove(Request $req)
    {
        if (!empty($req->id)) {
            $model = ResourceActivity::find($req->id);
            if (!checkCompanyAccess($model)) {
                return response()->json(false);
            }
            $model->status = 3;
            if ($model->save()) {
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }
}
