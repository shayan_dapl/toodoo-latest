<?php

namespace App\Http\Controllers\Process;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ResourcesType;
use View;
use Auth;
use App\Http\Requests\ResourcesTypeRequest;

class ResourceTypeController extends Controller
{
    public function list()
    {
        $breadcrumbs = [
            '/resourcestype/list' => 'words.resourcestype',
            '#' => 'words.list'
        ];
        return View::make('pages.process.resources_type', compact('breadcrumbs'));
    }

    public function data()
    {
        $data = ResourcesType::companyStatusCheck()->get();
        return response()->json($data);
    }

    public function save(ResourcesTypeRequest $req)
    {
        if (!empty($req->input())) {
            $model = ResourcesType::findOrNew($req->input('id'));
            $model->company_id = Auth::guard('customer')->user()->company_id;
            $model->name = $req->input('name');
            if ($model->save()) {
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }

    public function remove(Request $req)
    {
        if (!empty($req->id)) {
            $data = ResourcesType::where('id', $req->id)->first();
            if ($data->resources->count() == 0) {
                ResourcesType::where('id', $req->id)->update(['status' => 3]);
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }
}
