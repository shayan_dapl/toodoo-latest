<?php

namespace App\Http\Controllers\Process;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessResourceRequest;
use App\Models\Process;
use App\Models\Resources;
use App\Models\ResourcesDocs;
use App\Models\ResourceActivity;
use App\Models\ResourceWiseActivity;
use App\Models\ResourcesType;
use App\Models\Users;
use App\Models\ResourceDelegationActionLog;
use App\Models\ResourceDelegationActionComments;
use App\Models\ResourceDelegationActionFiles;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Session;
use View;
use Excel;

class ResourcesController extends Controller
{
    public function processResourcesForm(Request $req)
    {
        $breadcrumbs = [
            '/process/categorized' => 'words.process',
            '/process-step/resources/form' => 'words.resources'
        ];
        $resourceType = ResourcesType::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $activity = ResourceActivity::companyStatusCheck()->pluck('activity', 'id')->prepend(translate('form.select_activity'), '');
        $process = Process::companyCheck()->get();
        $activityDetails = [];
        $processOwners = Auth::guard('customer')->user()->company->users()->where('can_process_owner', 1)->pluck('name', 'id')->prepend(translate('form.select'), '');
        if (!empty($req->id)) {
            $breadcrumbs['#'] = 'words.edit';
            $details = Resources::find($req->id);
            if (!checkCompanyAccess($details)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $activityData = ResourceWiseActivity::where('resource_id', $req->id);
            if ($activityData->count() > 0) {
                $activityDetails = $activityData->get();
            }
            return View::make('pages.process.resources_form', compact('breadcrumbs', 'details', 'process', 'activity', 'activityDetails', 'resourceType', 'processOwners'));
        } else {
            $breadcrumbs['#'] = 'words.add';
            return View::make('pages.process.resources_form', compact('breadcrumbs', 'process', 'activity', 'activityDetails', 'resourceType', 'processOwners'));
        }
    }

    public function processResourcesFormSave(ProcessResourceRequest $req)
    {
        if (!empty($req->input()) && empty($req->input('multiple'))) {
            $model = Resources::findOrNew($req->input('hid'));
            $model->company_id = Auth::user()->company_id;
            $model->process_id = $req->input('process_id');
            $model->resource_id = $req->input('resource');
            $model->brand = $req->input('brand');
            $model->type = $req->input('type');
            $model->serial_no = !empty(trim($req->input('serial_no'))) ? $req->input('serial_no') : null;
            $model->status = 1;
            if (empty($req->input('hid'))) {
                $model->created_by = Auth::user()->id;
            }
            $model->updated_by = Auth::user()->id;
            if ($model->save()) {
                if (!empty(trim($req->input('serial_no')))) {
                    foreach ($req->input('activity') as $each) {
                        if (!empty($each['activity']) && !empty($each['owner']) && !empty($each['periodicity']) && !empty($each['start_date'])) {
                            $activitymodel = ResourceWiseActivity::findOrNew($each['id']);
                            $newActivity = empty($activitymodel->exists) ? 1 : 0;
                            $activitymodel->company_id = Auth::user()->company_id;
                            $activitymodel->resource_id = $model->id;
                            $activitymodel->activity_id = $each['activity'];
                            $activitymodel->process_owner = $each['owner'];
                            $activitymodel->periodicity = $each['periodicity'];
                            $activitymodel->start_date = date("Y-m-d", strtotime($each['start_date']));
                            $activitymodel->status = 1;
                            $activitymodel->save();
                            if ($newActivity == 1) {
                                $log = new ResourceDelegationActionLog;
                                $log->company_id = Auth::user()->company_id;
                                $log->resource_id = $model->id;
                                $log->activity_id = $activitymodel->id;
                                $log->delegate_to = $activitymodel->process_owner;
                                $log->deadline = $activitymodel->start_date;
                                $log->save();
                            }
                        }
                    }
                }
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/turtle/' . session('turtle_process_id'));
            }
        }
        if (!empty($req->file('doc_file')) && !empty($req->input('multiple'))) {
            $path = $req->file('doc_file')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get()->toArray();
            $preview = $setup = [];
            if (!empty($data)) {
                foreach ($data as $each) {
                    $counter = 0;
                    $issues = [];
                    $remark = [];
                    foreach ($each as $header => $value) {
                        $activityErr = [
                            'activity' => translate('error.add_activity'),
                            'processowner' => translate('error.add_process_owner'),
                            'periodicity' => translate('error.add_periodicity'),
                            'startdate' => translate('error.add_start_date')
                        ];
                        switch ($header) {
                            case 'process':
                                $process = Process::companyStatusCheck()->where('name', $each[$header])->first();
                                $remark[] = empty(trim($each[$header])) ? translate('words.add_process') : '';
                                $remark[] = (empty($process) || $process->id != session('turtle_process_id')) ? translate('error.invalid_process') : '';
                                $issues[] = empty(trim($each[$header])) ? translate('words.add_process') : '';
                                $issues[] = (empty($process) || $process->id != session('turtle_process_id')) ? translate('error.invalid_process') : '';
                                break;
                            case 'resource':
                                $resource = ResourcesType::companyStatusCheck()->where('name', $each[$header])->count();
                                $remark[] = empty(trim($each[$header])) ? translate('error.add_resource') : '';
                                $remark[] = (!empty(trim($each[$header])) && $resource == 0) ? translate('error.invalid_resource') : '';
                                $issues[] = empty(trim($each[$header])) ? translate('error.add_resource') : '';
                                $issues[] = (!empty(trim($each[$header])) && $resource == 0) ? translate('error.invalid_resource') : '';
                                break;
                            case 'brand':
                                $remark[] = empty(trim($each[$header])) ? translate('error.add_brand') : '';
                                $issues[] = empty(trim($each[$header])) ? translate('error.add_brand') : '';
                                break;
                            case 'type':
                                $remark[] = empty(trim($each[$header])) ? translate('error.add_type') : '';
                                $issues[] = empty(trim($each[$header])) ? translate('error.add_type') : '';
                                break;
                            case 'serial':
                            case 'activity':
                            case 'processowner':
                            case 'periodicity':
                            case 'startdate':
                                if (!empty(trim($each['serial'])) && (!empty(trim($each['activity'])) || !empty(trim($each['processowner'])) || !empty(trim($each['periodicity'])) || !empty(trim($each['startdate'])))) {
                                    if ($header == 'activity' && !empty(trim($each[$header]))) {
                                        $remark[] = empty(ResourceActivity::companyStatusCheck()->where('activity', $each[$header])->first()) ? translate('error.invalid_activity') : '';
                                    }
                                    $remark[] = empty(trim($each[$header])) ? $activityErr[$header] : '';
                                    if (!empty(trim($each[$header])) && $header == 'processowner') {
                                        $powner = Users::companyStatusCheck()->inServiceWithEmail()->where('email', $each[$header])->where('can_process_owner', 1)->count();
                                        $remark[] = ($powner == 0) ? translate('error.invalid_process_owner') : '';
                                    }
                                    $remark[] = (!empty(trim($each[$header])) && $header == 'periodicity' && (!is_int($each[$header]) && ($each[$header] < 1 || $each[$header] > 5))) ? translate('error.invalid_periodicity') : '';
                                    $remark[] = ($header == 'startdate' && !empty(trim($each['startdate'])) && strtotime($each['startdate']) === false) ? translate('error.invalid_date_format') : '';
                                }
                                break;
                        }
                    }
                    $preview[] = [
                        'process' => $each['process'],
                        'resource' => $each['resource'],
                        'brand' => $each['brand'],
                        'type' => $each['type'],
                        'serial' => $each['serial'],
                        'activity' => $each['activity'],
                        'processowner' => $each['processowner'],
                        'periodicity' => $each['periodicity'],
                        'startdate' => $each['startdate'],
                        'remark' => array_filter($remark)
                    ];
                    $setup[$each['serial']]['process'] = $each['process'];
                    $setup[$each['serial']]['resource'] = $each['resource'];
                    $setup[$each['serial']]['brand'] = $each['brand'];
                    $setup[$each['serial']]['type'] = $each['type'];
                    $setup[$each['serial']]['serial'] = $each['serial'];
                    $setup[$each['serial']]['activities'][] = [
                        'activity' => $each['activity'],
                        'processowner' => $each['processowner'],
                        'periodicity' => $each['periodicity'],
                        'startdate' => $each['startdate'],
                        'remark' => array_filter($remark)
                    ];
                    $setup[$each['serial']]['remark'] = array_filter($issues);
                }
                $breadcrumbs = ['/process-step/resources/form' => 'words.resources', '#' => 'words.resource_preview'];
                return View::make('pages.process.resource_preview', compact('breadcrumbs', 'preview', 'setup'));
            }
        }
    }

    public function processResourcesRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = Resources::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $model->status = 3;
            $model->updated_by = Auth::user()->id;
            if ($model->save()) {
                ResourceWiseActivity::where('resource_id', $model->id)->delete();
                ResourceDelegationActionLog::where('resource_id', $model->id)->delete();
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/turtle/' . session('turtle_process_id'));
            }
        }
    }

    public function processResourceDocRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = ResourcesDocs::find($req->id);
            if (file_exists(public_path('process_resources/' . $model->doc_name))) {
                unlink(public_path('process_resources/' . $model->doc_name));
            }
            $model->delete();
            return redirect('/process-step/resources/edit/' . $model->resource_id);
        }
    }

    public function processRemoveActivity(Request $req)
    {
        ResourceWiseActivity::where('id', $req->activity_id)->delete();
        return "success";
    }

    public function resourcePreviewSave(Request $req)
    {
        if (!empty($req->input('data'))) {
            $data = json_decode($req->input('data'), true);
            foreach ($data as $each) {
                if (empty($each['remark'])) {
                    $model = new Resources;
                    $model->company_id = Auth::user()->company_id;
                    $model->process_id = Process::companyStatusCheck()->where('name', $each['process'])->first()->id;
                    $model->resource_id = ResourcesType::companyStatusCheck()->where('name', $each['resource'])->first()->id;
                    $model->brand = $each['brand'];
                    $model->type = $each['type'];
                    $model->serial_no = $each['serial'];
                    $model->status = 1;
                    $model->created_by = Auth::user()->id;
                    $model->updated_by = 0;
                    if ($model->save()) {
                        foreach ($each['activities'] as $active) {
                            if (empty($active['remark']) && !empty(trim($active['activity']))) {
                                $activitymodel = new ResourceWiseActivity;
                                $activitymodel->company_id = Auth::user()->company_id;
                                $activitymodel->resource_id = $model->id;
                                $activitymodel->activity_id = ResourceActivity::companyStatusCheck()->where('activity', $active['activity'])->first()->id;
                                $activitymodel->process_owner = Users::companyStatusCheck()->inServiceWithEmail()->where('email', $active['processowner'])->first()->id;
                                $activitymodel->periodicity = $active['periodicity'];
                                $activitymodel->start_date = date('Y-m-d', strtotime($active['startdate']));
                                $activitymodel->status = 1;
                                if ($activitymodel->save()) {
                                    $log = new ResourceDelegationActionLog;
                                    $log->company_id = Auth::user()->company_id;
                                    $log->resource_id = $model->id;
                                    $log->activity_id = $activitymodel->id;
                                    $log->delegate_to = $activitymodel->process_owner;
                                    $log->deadline = $activitymodel->start_date;
                                    $log->save();
                                }
                            }
                        }
                    }
                }
            }
            $req->session()->flash('success', translate('words.success_message'));
        }
        return redirect('/process/turtle/' . session('turtle_process_id'));
    }

    public function issueFixing(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        $breadcrumbs = [
            '#' => 'words.action',
        ];
        $log = ResourceDelegationActionLog::companyCheck()->where('id', $req->id)->first();
        return View::make('pages.process.resource_action_fixing', compact('breadcrumbs', 'log'));
    }

    public function issueFixingSave(Request $req)
    {
        if (empty($req->input())) {
            return response()->json(false);
        }
        //Comment adding
        if (!empty($req->input('reply'))) {
            $comment = new ResourceDelegationActionComments;
            $comment->company_id = Auth::guard('customer')->user()->company_id;
            $comment->log_id = $req->input('log_id');
            $comment->reply = $req->input('reply');
            $comment->save();
        }
        //Supporting document uploading per comment
        if (!empty($req->file('upload_doc'))) {
            foreach ($req->file('upload_doc') as $index => $file) {
                $filename = time() . '-' . $file->getClientOriginalName();
                $file->move(storage_path('app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents/resource_action_docs/'), $filename);
                $fileModel = new ResourceDelegationActionFiles;
                $fileModel->comment_id = $comment->id;
                $fileModel->doc_name = $filename;
                $fileModel->submitted_by = Auth::guard('customer')->user()->id;
                $fileModel->save();
            }
        }
        //Action log closed
        if ($req->input('closed')) {
            ResourceDelegationActionLog::where('id', $req->input('log_id'))->update(['closed' => 1, 'closed_at' => date('Y-m-d')]);
        }
        $req->session()->flash('success', translate('words.success_message'));
        return redirect('home/issues');
    }
}
