<?php

namespace App\Http\Controllers\Process;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessRiskRequest;
use App\Models\Process;
use App\Models\Risk;
use App\Models\RiskOpportunityAction;
use App\Models\RiskOpportunityActionComments;
use App\Models\RiskOpportunityActionFiles;
use App\Models\RiskOpportunityActionLog;
use App\Models\Users;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Session;
use View;

class RiskController extends Controller
{
    private $breadcrumbs;

    public function processRiskForm(Request $req)
    {
        $actions = RiskOpportunityAction::companyCheck()->first();
        if (!empty($actions)) {
            $definedActions = ($actions->count() > 0) ? $actions->actions : "";
        } else {
            $definedActions = "";
        }

        if (!empty($req->id)) {
            $breadcrumbs = array(
                '/process/categorized' => 'words.process',
                '/process-step/risk/form' => 'words.risk',
                '#' => 'words.edit',
            );

            $process = Process::companyCheck()->get();
            $details = Risk::find($req->id);
            if (!checkCompanyAccess($details)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $risk_list = Risk::where('process_id', session('turtle_process_id'))->where('status', '<>', 3)->get();
            $riskOptAct = RiskOpportunityAction::companyCheck()->pluck('actions');
            $contains = 0;
            if (isset($riskOptAct[0])) {
                $contains = str_contains($riskOptAct[0], $details->risk_level);
            }
            if ($contains) {
                $riskOptActCls = '';
            } else {
                $riskOptActCls = 'hide';
            }

            return View::make('pages.process.risk_form', compact('breadcrumbs', 'details', 'process', 'risk_list', 'definedActions', 'riskOptActCls'));
        } else {
            $breadcrumbs = array(
                '/process/categorized' => 'words.process',
                '/process-step/risk/form' => 'words.risk',
                '#' => 'words.add',
            );
            $process = Process::companyCheck()->get();
            $risk_list = Risk::where('process_id', session('turtle_process_id'))->where('status', '<>', 3)->get();
            $riskOptActCls = 'hide';
            $users = Users::companyCheck()->inServiceWithEmail()->pluck('name', 'id')->prepend(translate('form.select'), '');
            return View::make('pages.process.risk_form', compact('breadcrumbs', 'process', 'risk_list', 'definedActions', 'riskOptActCls', 'users'));
        }
    }

    public function processRiskFormSave(ProcessRiskRequest $req)
    {
        if (!empty($req->hid)) {
            $model = Risk::find($req->hid);
            $model->process_id = $req->input('process_id');
            $model->risk = $req->input('risk');
            $model->risk_level = $req->input('level');
            $model->comment = $req->input('comment');
            $model->updated_by = Auth::user()->id;
        } else {
            $model = new Risk;
            $model->process_id = $req->input('process_id');
            $model->risk = $req->input('risk');
            $model->risk_level = $req->input('level');
            $model->comment = $req->input('comment');
            $model->status = 1;
            $model->created_by = Auth::user()->id;
            $model->updated_by = 0;
        }
        if ($model->save()) {
            $entry = new RiskOpportunityActionLog;
            $entry->company_id = Auth::guard('customer')->user()->company_id;
            $entry->process_id = $req->input('process_id');
            $entry->risk_opp_id = $model->id;
            $entry->type = $req->input('type');
            $entry->action = $req->input('action');
            $entry->delegate_to = !empty($req->input('who')) ? $req->input('who') : 0;
            $entry->action_status = $req->input('level');
            $entry->comment = $req->input('comment');
            if (!empty($req->input('deadline'))) {
                $entry->deadline = $req->input('deadline');
            } else {
                $entry->closed = 1;
                $entry->closed_at = date('Y-m-d');
            }
            $entry->save();

            $req->session()->flash('success', translate('words.success_message'));
            if ($req->input('save')) {
                return redirect('/process/turtle/' . session('turtle_process_id'));
            } else {
                return redirect('process-step/risk/form');
            }
        }
    }

    public function processRiskRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = Risk::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $model->status = 3;
            $model->updated_by = Auth::user()->id;
            if ($model->save()) {
                $actionlogs = RiskOpportunityActionLog::where('risk_opp_id', $req->id)->where('type', 'risk')->pluck('id');
                RiskOpportunityActionComments::where('type', 'risk')->whereIn('risk_opp_id', $actionlogs)->delete();
                RiskOpportunityActionLog::where('risk_opp_id', $req->id)->where('type', 'risk')->delete();
                $req->session()->flash('success', translate('words.success_message'));
            }
            return redirect('/process/turtle/' . session('turtle_process_id'));
        }
    }

    public function processRiskActionComment(Request $req)
    {
        if (!empty($req->input())) {
            //Comment adding
            if (!empty($req->input('reply'))) {
                $comment = new RiskOpportunityActionComments;
                $comment->company_id = Auth::guard('customer')->user()->company_id;
                $comment->process_id = $req->input('process_id');
                $comment->type = $req->input('type');
                $comment->risk_opp_id = $req->input('risk_id');
                $comment->reply = $req->input('reply');
                $comment->save();
            }

            //Supporting document uploading per comment
            if (!empty($req->file('upload_doc'))) {
                foreach ($req->file('upload_doc') as $index => $file) {
                    $filename = time() . '-' . $file->getClientOriginalName();
                    $file->move(
                        storage_path('app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents/risk_opp_action_docs/'),
                        $filename
                    );
                    $fileModel = new RiskOpportunityActionFiles;
                    $fileModel->comment_id = $comment->id;
                    $fileModel->document_name = $filename;
                    $fileModel->submitted_by = Auth::guard('customer')->user()->id;
                    $fileModel->save();
                }
            }

            //Action log closed
            if ($req->input('closed')) {
                RiskOpportunityActionLog::where('id', $req->input('action_log_id'))->update(['closed' => 1, 'closed_at' => date('Y-m-d')]);
            }

            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/process/turtle/' . session('turtle_process_id'));
        }
    }

    public function processRiskActionAdd(Request $req)
    {
        if (!empty($req->input())) {
            //Set level according to action
            Risk::where('id', $req->input('risk_id'))->update(['risk_level' => $req->input('risk_level')]);

            //Add action to log
            $action = new RiskOpportunityActionLog;
            $action->company_id = Auth::guard('customer')->user()->company_id;
            $action->process_id = $req->input('process_id');
            $action->type = "risk";
            $action->risk_opp_id = $req->input('risk_id');
            $action->action = $req->input('action');
            $action->action_status = $req->input('risk_level');
            $action->delegate_to = !empty($req->input('who')) ? $req->input('who') : 0;
            $action->comment = $req->input('comment');
            if (!empty($req->input('deadline'))) {
                $action->deadline = $req->input('deadline');
            } else {
                $action->closed = 1;
                $action->closed_at = date('Y-m-d');
            }
            if ($action->save()) {
                $req->session()->flash('success', translate('words.success_message'));
            }
            return redirect('/process/turtle/' . session('turtle_process_id'));
        }
    }

    public function processRiskActionEventLog(Request $req)
    {
        if (!empty($req->riskId) && !empty($req->type)) {
            $type = $req->type;
            $data = RiskOpportunityActionLog::where('risk_opp_id', $req->riskId)->where('process_id', Session::get('turtle_process_id'))->where('type', $req->type)->get();
            return View::make('pages.process.risk_action_event_log', compact('data', 'type'));
        }
    }
}
