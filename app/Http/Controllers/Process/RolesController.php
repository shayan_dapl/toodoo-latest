<?php

namespace App\Http\Controllers\Process;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessRolesRequest;
use App\Models\Position;
use App\Models\ProcessSteps;
use App\Models\Roles;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;

class RolesController extends Controller
{
    public function processRolesForm(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/process/categorized' => 'words.process',
                '/process-step/roles/form' => 'words.roles',
                '#' => 'words.edit',
            ];
            $position = Position::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
            $details = Roles::find($req->id);
            $process_steps = ProcessSteps::where('process_id', $details->process_id)->pluck('step_name', 'id')->prepend(translate('form.select'), '');
            $roles = explode("|", $details->role_id);

            return View::make('pages.process.roles_form', compact('breadcrumbs', 'details', 'process_steps', 'position', 'roles'));
        } else {
            $breadcrumbs = [
                '/process/categorized' => 'words.process',
                '/process-step/roles/form' => 'words.roles',
                '#' => 'words.add',
            ];
            $position = Position::companyStatusCheck()->pluck('name', 'id')->prepend(translate('form.select'), '');
            $process_steps = ProcessSteps::where('process_id', session('turtle_process_id'))->pluck('step_name', 'id')->prepend(translate('form.select'), '');

            return View::make('pages.process.roles_form', compact('breadcrumbs', 'process_steps', 'position'));
        }
    }

    public function processRolesFormSave(ProcessRolesRequest $req)
    {
        if (!empty($req->input())) {
            if (!empty($req->input('roles'))) {
                if (!empty($req->hid)) {
                    $model = Roles::find($req->hid);
                    $model->process_id = $req->input('process_id');
                    $model->process_step_id = $req->input('process_step_id');
                    $model->position_id = $req->input('position_id');
                    $model->role_id = implode("|", $req->input('roles'));
                    $model->updated_by = Auth::guard('customer')->user()->id;
                    if ($model->save()) {
                        $req->session()->flash('success', translate('words.success_message'));
                        if (session('turtle_process_id')) {
                            return redirect('/process/turtle/' . session('turtle_process_id'));
                        } else {
                            return redirect('/process-step/resources/list');
                        }
                    }
                } else {
                    $model = new Roles;
                    $model->process_id = $req->input('process_id');
                    $model->process_step_id = $req->input('process_step_id');
                    $model->position_id = $req->input('position_id');
                    $model->role_id = implode("|", $req->input('roles'));
                    $model->status = 1;
                    $model->created_by = Auth::guard('customer')->user()->id;
                    $model->updated_by = 0;
                    if ($model->save()) {
                        $req->session()->flash('success', translate('words.success_message'));
                        return redirect('/process/turtle/' . session('turtle_process_id'));
                    }
                }
            }
        }
    }

    public function processRemoveRole(Request $req)
    {
        Roles::where('process_id', $req->process_id)->where('process_step_id', $req->step_id)->where('position_id', $req->position_id)->delete();
        return "success";
    }
}
