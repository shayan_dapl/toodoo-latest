<?php

namespace App\Http\Controllers\Process;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessScopeRequest;
use App\Models\Process;
use App\Models\Scope;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;

class ScopeController extends Controller
{
    public function processScopeForm(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/process/categorized' => 'words.process',
                '/process-step/scope/form' => 'words.scope',
                '#' => 'words.edit',
            ];
            $process = Process::companyCheck()->get();
            $details = Scope::find($req->id);
            if (!checkCompanyAccess($details)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            return View::make('pages.process.scope_form', compact('breadcrumbs', 'details', 'process'));
        } else {
            $process = Process::companyCheck()->get();
            $breadcrumbs = [
                '/process/categorized' => 'words.process',
                '/process-step/scope/form' => 'words.scope',
                '#' => 'words.add',
            ];

            return View::make('pages.process.scope_form', compact('breadcrumbs', 'process'));
        }
    }

    public function processScopeFormSave(ProcessScopeRequest $req)
    {
        if (!empty($req->input())) {
            if (!empty($req->hid)) {
                $model = Scope::find($req->hid);
                $model->process_id = $req->input('process_id');
                $model->scope = $req->input('scope');
                $model->updated_by = Auth::user()->id;
            } else {
                $model = new Scope;
                $model->process_id = $req->input('process_id');
                $model->scope = $req->input('scope');
                $model->status = 1;
                $model->created_by = Auth::user()->id;
                $model->updated_by = 0;
            }

            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/turtle/' . session('turtle_process_id'));
            }
        }
    }

    public function processScopeRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = Scope::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $model->status = 3;
            $model->updated_by = Auth::user()->id;
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/turtle/' . session('turtle_process_id'));
            }
        }
    }
}
