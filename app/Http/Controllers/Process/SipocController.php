<?php

namespace App\Http\Controllers\Process;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Process;
use View;

class SipocController extends Controller
{
    public function sipoc(Request $req)
    {
        $breadcrumbs = [
            '/process/list/' => 'words.process',
            '#' => 'words.sipoc',
        ];
        $data = [];
        $process = Process::where('id', $req->process_id)->companyCheck()->where('status', '<>', 3)->first();
        $data['input'][] = $process->inputs()->where('status', '<>', 3)->get();
        $data['output'][] = $process->outputs()->where('status', '<>', 3)->get();
        $data['steps'][] = $process->steps()->orderBy('serial', 'ASC')->orderBy('step_no', 'ASC')->where('status', '<>', 3)->get();
        foreach ($data['input'][0] as $eachData) {
            $data['supplier'][] = $eachData->supporting_process;
        }
        if (!empty($data['supplier'])) {
            $data['supplier'] = array_unique($data['supplier']) ;
        } else {
            $data['supplier'] = [];
        }
        

        foreach ($data['output'][0] as $eachData) {
            $data['customer'][] = $eachData->supporting_process;
        }
        if (!empty($data['customer'])) {
            $data['customer'] = array_unique($data['customer']) ;
        } else {
            $data['customer'] = [];
        }
                           
        return View::make('pages.process.sipoc_view', compact('breadcrumbs', 'data', 'process'));
    }
}
