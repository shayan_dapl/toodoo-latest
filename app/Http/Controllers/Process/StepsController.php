<?php

namespace App\Http\Controllers\Process;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessStepsRequest;
use App\Models\Position;
use App\Models\Process;
use App\Models\ProcessSteps;
use App\Models\Roles;
use App\Models\PositionBranches;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;
use DB;

class StepsController extends Controller
{
    public function processStepsForm(Request $req)
    {
        $breadcrumbs = [
            '/process/categorized' => 'words.process',
            '/process-step/steps/form' => 'words.process_steps',
        ];
        $process = Process::companyCheck()->where('id', session('turtle_process_id'))->first();
        $branches = $process->branches->pluck('branch_id')->toArray();
        $position = PositionBranches::companyCheck()->with('position')->whereIn('branch_id', $branches)->get()->where('position.status', 1);
        if (count($branches) > 1) {
            $position = $position->pluck('position.name_with_details', 'position.id')->toArray();
        } else {
            $position = $position->pluck('position.name', 'position.id')->toArray();
        }
        $position = array_reverse($position, true);
        $everyOne = Position::companyCheck()->where('status', 4)->first();
        //$position[$everyOne->id] = $everyOne->name;
        $position[''] = translate('form.select');
        $position = array_reverse($position, true);
        if (!empty($req->id)) {
            $breadcrumbs['#'] = 'words.edit';
            $roleDetails = [];
            $details = ProcessSteps::where('process_id', session('turtle_process_id'))->where('id', $req->id)->first();
            if (!checkCompanyAccess($details)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $roleData = Roles::where('process_step_id', $req->id);
            if ($roleData->count() > 0) {
                $roleDetails = $roleData->select('roles.*', DB::raw('GROUP_CONCAT(role_id) as `roles_id`'))->groupBy('position_id')->get();
            }
            $stepList = ProcessSteps::where('process_id', session('turtle_process_id'))->where('status', 1)->get();
        } else {
            $breadcrumbs['#'] = 'words.add';
            $roleDetails = $details = [];
            $stepList = ProcessSteps::where('process_id', session('turtle_process_id'))->where('status', 1)->get();
        }
        return View::make('pages.process.steps_form', compact('breadcrumbs', 'details', 'position', 'roleDetails', 'stepList', 'everyOne'));
    }

    public function processStepsFormSave(ProcessStepsRequest $req)
    {
        if (!empty($req->input())) {
            if (!empty($req->hid)) {
                $model = ProcessSteps::find($req->hid);
                $model->process_id = $req->input('process_id');
                $model->step_no = $req->input('step_no');
                $model->step_name = $req->input('step_name');
                $model->updated_by = Auth::guard('customer')->user()->id;
            } else {
                $model = new ProcessSteps;
                $model->process_id = $req->input('process_id');
                $model->step_no = $req->input('step_no');
                $model->step_name = $req->input('step_name');
                $model->status = 1;
                $model->created_by = Auth::guard('customer')->user()->id;
                $model->updated_by = 0;
            }
            if ($model->save()) {
                foreach ($req->input('roles') as $role) {
                    if (isset($role['responsibility']) && !empty($role['position'])) {
                        $responsibilityExist = Roles::where('process_step_id', $model->id)->where('position_id', $role['position'])->pluck('role_id')->toArray();
                        $newResponsible = $role['responsibility'];
                        if (!empty($responsibilityExist)) {
                            foreach ($responsibilityExist as $eachRes) {
                                if (!in_array($eachRes, $newResponsible)) {
                                    Roles::where('process_step_id', $model->id)->where('position_id', $role['position'])->where('role_id', $eachRes)->delete();
                                }
                            }
                        }
                        foreach ($newResponsible as $eachnewRes) {
                            $rolemodel = Roles::where('process_step_id', $model->id)->where('position_id', $role['position'])->where('role_id', $eachnewRes)->first();
                            if (!$rolemodel) {
                                $rolemodel = new Roles;
                            }
                            $rolemodel->process_id = $req->input('process_id');
                            $rolemodel->process_step_id = $model->id;
                            $rolemodel->position_id = $role['position'];
                            $rolemodel->role_id = $eachnewRes;
                            $rolemodel->status = 1;
                            if ($role['id'] != "") {
                                $rolemodel->updated_by = Auth::guard('customer')->user()->id;
                            } else {
                                $rolemodel->created_by = Auth::guard('customer')->user()->id;
                                $rolemodel->updated_by = 0;
                            }
                            $rolemodel->save();
                        }
                    }
                }
                $req->session()->flash('success', translate('words.success_message'));
                if (strtolower($req->input('add_step')) == 'add_step') {
                    return redirect('/process-step/steps/form/');
                } else {
                    return redirect('/process/turtle/' . session('turtle_process_id'));
                }
            }
        }
    }

    public function processStepsRemove(Request $req)
    {
        if (!empty($req->id)) {
            $model = ProcessSteps::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $model->status = 3;
            $model->updated_by = Auth::user()->id;
            if ($model->save()) {
                Roles::where('process_step_id', $req->id)->delete();
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/process/turtle/' . session('turtle_process_id'));
            }
        }
    }
}
