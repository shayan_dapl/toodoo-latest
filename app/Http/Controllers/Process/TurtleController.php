<?php

namespace App\Http\Controllers\Process;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Input;
use App\Models\Kpi;
use App\Models\Opportunity;
use App\Models\Output;
use App\Models\Position;
use App\Models\Procedures;
use App\Models\Process;
use App\Models\ProcessSteps;
use App\Models\Resources;
use App\Models\Risk;
use App\Models\RiskOpportunityAction;
use App\Models\RiskOpportunityActionComments;
use App\Models\RiskOpportunityActionLog;
use App\Models\Roles;
use App\Models\Scope;
use App\Models\Users;
use App\Models\Branch;
use App\Models\ProcessKpiData;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
use View;
use DB;
use Config;

class TurtleController extends Controller
{
    public function turtle(Request $req)
    {
        $no_turtle = Process::where('id', $req->process_id)->where('no_turtle_process', 1)->count();
        if ($no_turtle > 0) {
            return redirect('/process/categorized');
        } else {
            $breadcrumbs = [
                '/process/categorized' => 'words.company_map',
                '#' => 'words.turtle',
            ];
            $process = [];
            $turtle_arr = [];
            $modify = "no";
            $process = Process::where('id', $req->process_id)->companyCheck()->where('status', '<>', 3)->first();
            if (!checkCompanyAccess($process)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            if (Auth::guard('customer')->user()->type == 2 || Auth::guard('customer')->user()->id == $process->owner_id) {
                $modify = "yes";
            }
            if (!empty($req->process_id)) {
                Session::put('turtle_process_id', $req->process_id);
            }
            if ($req->input('process_id') != 0 || !empty($req->process_id)) {
                $process_id = !empty($req->process_id) ? $req->process_id : $req->input('process_id');
                $turtle_arr['scope'][] = $process->scopes()->where('status', '<>', 3)->get();
                $turtle_arr['procedures'][] = $process->procedures()
                    ->where('type', 'P')
                    ->where('status', '<>', 3)
                    ->whereRaw('id IN(SELECT MAX(id) FROM `procedures` where process_id = ? and type = "P" and status != 3 GROUP BY `doc_ref`)', $process_id)
                    ->orderBy('created_at', 'ASC')
                    ->get();
                $turtle_arr['regulations'][] = $process->procedures()->where('type', 'R')
                    ->where('type', 'R')
                    ->where('status', '<>', 3)
                    ->whereRaw('id IN(SELECT MAX(id) FROM `procedures` where process_id = ? and type = "R" and status != 3 GROUP BY `doc_ref`)', $process_id)
                    ->orderBy('created_at', 'ASC')
                    ->get();
                $turtle_arr['kpis'][] = $process->kpis()->where('status', '<>', 3)->get();
                $turtle_arr['input'][] = $process->inputs()->where('status', '<>', 3)->get();
                $turtle_arr['steps'][] = $process->steps()->orderBy('serial', 'ASC')->orderBy('step_no', 'ASC')->where('status', '<>', 3)->get();
                $turtle_arr['output'][] = $process->outputs()->where('status', '<>', 3)->get();
                $turtle_arr['risks'][] = $process->risks()->where('status', '<>', 3)->get();
                $turtle_arr['opportunity'][] = $process->opportunities()->where('status', '<>', 3)->get();
                $turtle_arr['resources'][] = $process->resources()->where('status', '<>', 3)->get();
                $turtle_arr['roles'][] = $process->racimap();
            }

            return View::make('pages.process.turtle_board', compact('breadcrumbs', 'process', 'turtle_arr', 'modify'));
        }
    }

    public function turtleOutput(Request $req)
    {
        if (!empty($req->process_id)) {
            $process = [];
            $turtle = [];
            $process = Process::where('id', $req->process_id)->companyCheck()->where('status', '<>', 3)->first();
            if (!empty($req->process_id)) {
                Session::put('turtle_process_id', $req->process_id);
            }
            if ($req->input('process_id') != 0 || !empty($req->process_id)) {
                $process_id = !empty($req->process_id) ? $req->process_id : $req->input('process_id');
                $turtle['scope'][] = $process->scopes()->where('status', '<>', 3)->get();
                $turtle['procedures'][] = $process->procedures()->where('type', 'P')->where('status', '<>', 3)->whereRaw('id IN(SELECT MAX(id) FROM `procedures` where process_id = ? and type = "P" and status != 3 GROUP BY `doc_ref`)', $process_id)->orderBy('created_at', 'ASC')->get();
                $turtle['regulations'][] = $process->procedures()->where('type', 'R')->where('status', '<>', 3)->whereRaw('id IN(SELECT MAX(id) FROM `procedures` where process_id= ? and type = "R" and status != 3 GROUP BY `doc_ref`)', $process_id)->orderBy('created_at', 'ASC')->get();
                $turtle['kpis'][] = $process->kpis()->where('status', '<>', 3)->get();
                $turtle['input'][] = $process->inputs()->where('status', '<>', 3)->get();
                $turtle['steps'][] = $process->steps()->orderBy('serial', 'ASC')->orderBy('step_no', 'ASC')->where('status', '<>', 3)->get();
                $turtle['output'][] = $process->outputs()->where('status', '<>', 3)->get();
                $turtle['risks'][] = $process->risks()->where('status', '<>', 3)->get();
                $turtle['opportunity'][] = $process->opportunities()->where('status', '<>', 3)->get();
                $turtle['resources'][] = $process->resources()->where('status', '<>', 3)->get();
                $turtle['roles'][] = $process->racimap();
            }
        }
        $html = View::make('pages.process.turtle_output', compact('process', 'turtle'))->render();
        return \PDF::load($html)->setPaper('A4', 'landscape')->filename('/tmp/'.$process->name.'.pdf')->download();
    }

    public function turtleDetails(Request $req)
    {
        $modify = (Auth::guard('customer')->user()->type == 2 || Auth::guard('customer')->user()->id == $req->processOwnerId) ? "yes" : "no";
        $for = $req->model;
        $id = $req->id;
        switch ($req->model) {
            case 'Scope':
                $details = Scope::find($id);
                return View::make('pages.process.detail_view', compact('for', 'id', 'details', 'modify'));
            case 'Procedures':
                $details = Procedures::find($id);
                $source_id = Procedures::where('process_id', $details->process_id)->where('type', 'P')->where('status', '<>', 3)->where('doc_ref', $details->doc_ref)->whereRaw('id IN(SELECT MIN(id) FROM `procedures` where process_id = ? and type = "P" and status != 3 GROUP BY `doc_ref`)', $details->process_id)->first();
                if (!empty($source_id)) {
                    $source_id = $source_id->id;
                }
                return View::make('pages.process.detail_view', compact('for', 'id', 'details', 'source_id', 'modify'));
            case 'Regulations':
                $details = Procedures::find($id);
                $source_id = Procedures::where('process_id', $details->process_id)->where('type', 'R')->where('status', '<>', 3)->where('doc_ref', $details->doc_ref)->whereRaw('id IN(SELECT MIN(id) FROM `procedures` where process_id = ? and type = "R" and status != 3 GROUP BY `doc_ref`)', $details->process_id)->first();
                if (!empty($source_id)) {
                    $source_id = $source_id->id;
                }
                return View::make('pages.process.detail_view', compact('for', 'id', 'details', 'source_id', 'modify'));
            case 'Kpi':
                $details = Kpi::find($id);
                $labelArr = [];
                $dataArr = [];
                if (!empty($details->processkpi)) {
                    foreach ($details->processkpi as $eachData) {
                        $labelArr[] = $eachData->created_at;
                        $dataArr[] = $eachData->kpi_value;
                    }
                }
                $labelArr = json_encode($labelArr);
                $dataArr = json_encode($dataArr);
                $boolOptions = ['' => translate('form.select'), '1' => translate('words.true'), '0' => translate('words.false')];
                $trendOptions = ['' => translate('form.select'), '1' => translate('words.up'), '0' => translate('words.down')];
                switch ($details->kpi_unit_id) {
                    case '3':
                        $details->target = $boolOptions[$details->target];
                        break;
                    case '4':
                        $details->target = $trendOptions[$details->target];
                        break;
                    default:
                        $details->target = $details->target;
                        break;
                }
                $frequency = ['' => translate('form.select'), 'daily' => translate('words.daily'),'weekly' => translate('words.weekly'), 'monthly' => translate('words.monthly'), 'quarterly' => translate('words.quarterly'), 'half_yearly' => translate('words.half_yearly'), 'yearly' => translate('words.yearly')];
                return View::make('pages.process.detail_view', compact('for', 'id', 'details', 'modify', 'labelArr', 'dataArr', 'frequency'));
            case 'Input':
                $details = Input::find($id);
                return View::make('pages.process.detail_view', compact('for', 'id', 'details', 'modify'));
            case 'ProcessSteps':
                $details = ProcessSteps::find($id);
                $roleDetails = [];
                if ($details->roles->count() > 0) {
                    $roleDetails = $details->roles()->select('roles.*', DB::raw('GROUP_CONCAT(role_id) as `roles_id`'))->groupBy('position_id')->get();
                }
                return View::make('pages.process.detail_view', compact('for', 'id', 'details', 'roleDetails', 'modify'));
            case 'Output':
                $details = Output::find($id);
                return View::make('pages.process.detail_view', compact('for', 'id', 'details', 'modify'));
            case 'Risk':
                $details = Risk::find($id);
                $commentAllow = '';
                $definedActions = [];
                $log = RiskOpportunityActionLog::checkRiskId($details->id, 'risk')->latest();
                $action = $log->first();
                $riskOptAct = RiskOpportunityAction::companyCheck()->pluck('actions');
                $users = Users::companyCheck()->inServiceWithEmail()->pluck('name', 'id')->prepend(translate('form.select'), '');
                $deligation = $log->where('delegate_to', Auth::guard('customer')->user()->id)->where('closed', 0)->first();
                if (!empty($deligation)) {
                    $commentAllow = "yes";
                }
                if (!empty($riskOptAct[0])) {
                    $definedActions = $riskOptAct[0];
                }
                return View::make('pages.process.detail_view', compact('for', 'id', 'details', 'action', 'modify', 'definedActions', 'users', 'commentAllow'));
            case 'Opportunity':
                $details = Opportunity::find($id);
                $commentAllow = '';
                $definedActions = [];
                $log = RiskOpportunityActionLog::checkOpptId($details->id, 'opportunity')->latest();
                $action = $log->first();
                $riskOptAct = RiskOpportunityAction::companyCheck()->pluck('actions');
                $users = Users::companyCheck()->inServiceWithEmail()->pluck('name', 'id')->prepend(translate('form.select'), '');
                $deligation = $log->where('delegate_to', Auth::guard('customer')->user()->id)->where('closed', 0)->first();
                if (!empty($deligation)) {
                    $commentAllow = "yes";
                }
                if (!empty($riskOptAct[0])) {
                    $definedActions = $riskOptAct[0];
                }
                return View::make('pages.process.detail_view', compact('for', 'id', 'details', 'action', 'modify', 'definedActions', 'users', 'commentAllow'));
            case 'Resources':
                $details = Resources::find($id);
                return View::make('pages.process.detail_view', compact('for', 'id', 'details', 'modify'));
            case 'Roles':
                $details = Roles::find($id);
                $process = $details->process;
                $position = $details->position;
                $process_steps = $details->steps;
                $roles = [];
                $data = Roles::where('process_id', $details->process_id)->where('position_id', $details->position_id)->get();
                if ($data->count() > 0) {
                    foreach ($data as $each) {
                        $roles[] = $each->role_id;
                    }
                }
                return View::make('pages.process.detail_view', compact('for', 'id', 'details', 'process', 'position', 'process_steps', 'roles', 'modify'));
        }
    }

    public function turtleGraph(Request $req)
    {
        $labelArr = [];
        $dataArr = [];
        $id = $req->id;
        $details = Kpi::find($id);
        $processKpidata = ProcessKpiData::where('kpi_id', $id)->get();
        $kpiTitle = '';
        if (!empty($processKpidata[0])) {
            foreach ($processKpidata as $eachData) {
                $labelArr[] = $eachData->created_at;
                $dataArr[] = $eachData->kpi_value;
            }
            $kpiTitle = $processKpidata[0]->processkpi->name;
        }
        $labelArr = json_encode($labelArr);
        $dataArr = json_encode($dataArr);
        return View::make('pages.process.graph_view', compact('id', 'kpiTitle', 'labelArr', 'dataArr'));
    }
}
