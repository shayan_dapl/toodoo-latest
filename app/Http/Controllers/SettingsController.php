<?php

namespace App\Http\Controllers;

use App;
use App\Models\CompanySettings;
use App\Models\GeneralSettings;
use App\Models\RiskOpportunityAction;
use App\Models\Countries;
use App\Models\KpiUnit;
use App\Models\Users;
use Auth;
use Illuminate\Http\Request;
use Session;
use View;
use Config;
use Validator;

class SettingsController extends Controller
{
    public function riskOppAction()
    {
        $breadcrumbs = [
            '#' => 'words.risk_opp_action',
        ];
        $data = RiskOpportunityAction::companyCheck();
        $definedAction = ($data->count() > 0) ? explode(",", $data->first()->actions) : [];
        return View::make('pages.settings.risk_opp_action', compact('breadcrumbs', 'definedAction'));
    }

    public function riskOppActionSave(Request $req)
    {
        if (!empty($req->input())) {
            $actions = "";
            if (!empty($req->input('action'))) {
                $actions = implode(',', $req->input('action'));
            }
            $companyId = Auth::guard('customer')->user()->company_id;
            $model = RiskOpportunityAction::firstOrNew(['company_id' => $companyId]);
            $model->actions = $actions;
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/settings/risk-opp-action');
            }
        }
    }

    public function companySettings()
    {
        $breadcrumbs = [
            '#' => 'words.company_settings',
        ];
        $companyId = Auth::guard('customer')->user()->company_id;
        $data = CompanySettings::companyCheck()->first();
        $users = Users:: companyCheck()
            ->where('out_of_service', null)
            ->where('no_email', 0)
            ->orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->toArray();
        $selectedUsers = Users:: companyCheck()
            ->where('company_view', 1)
            ->orderBy('name', 'ASC')
            ->pluck('id')
            ->toArray();
        return View::make('pages.settings.company_settings', compact('breadcrumbs', 'data', 'companyId', 'users', 'selectedUsers'));
    }

    public function generalSettings(Request $req)
    {
        if (!empty($req->input())) {
            $model = GeneralSettings::findOrNew($req->input('hid'));
            $model->package_trial_count = $req->input('package_trial_count');
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/trial-period');
            }
        }

        $breadcrumbs = [
            '/trial-period' => 'words.general_settings',
        ];
        $data = GeneralSettings::find(1);

        return View::make('pages.settings.trial_period', compact('breadcrumbs', 'data'));
    }

    public function vatSettings(Request $req)
    {
        $breadcrumbs = [
            '/vat-settings' => 'words.vat-settings',
        ];
        $data = Countries::all();
        return View::make('pages.settings.vat_settings', compact('breadcrumbs', 'data'));
    }

    public function editVatSettings(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/vat-settings' => 'words.vat-settings',
                '#' => 'words.edit',
            ];
            $details = Countries::find($req->id);
            return View::make('pages.settings.edit_vat_settings', compact('breadcrumbs', 'details'));
        }
    }

    public function saveVatSettings(Request $req)
    {
        if (!empty($req->input())) {
            $model = Countries::find($req->input('hid'));
            $model->vat_percent = $req->input('vat_percent');
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/vat-settings');
            }
        }
    }

    public function removeTrialBanner()
    {
        Session::put('banner', 0);
        return response()->json(true);
    }

    public function kpiUnit()
    {
        $breadcrumbs = [
            '/kpi-unit' => 'words.kpi_unit',
        ];
        return View::make('pages.settings.kpi_unit', compact('breadcrumbs'));
    }

    public function kpiUnitData()
    {
        return KpiUnit::all()->toJson();
    }

    public function kpiUnitSave(Request $req)
    {
        if (!empty($req->input())) {
            KpiUnit::where('id', $req->input('id'))
            ->update([
                'name_nl' => $req->input('name_nl'),
                'name_en' => $req->input('name_en'),
                'name_fr' => $req->input('name_fr')
            ]);

            return response()->json(true);
        } else {
            return response()->json(false);
        }
    }

    public function companySettingsSave(Request $req)
    {
        if (!empty($req->input())) {
            $validator = Validator::make($req->all(), [
                'urgency_count' => 'required',
                'supplier_threshold' => 'required'
            ]);
            if ($validator->fails()) {
                return redirect('settings/company')->withErrors($validator)->withInput();
            }
            $selectedUsers = $req->input('users');
            Users::where('company_id', Auth::guard('customer')->user()->company_id)->update(['company_view' => 0]);
            if (!empty($selectedUsers)) {
                foreach ($selectedUsers as $m => $user) {
                    $userModel = Users::find($user);
                    $userModel->company_view = 1;
                    $userModel->save();
                }
            }
            $model = CompanySettings::firstOrNew(['company_id' => Auth::guard('customer')->user()->company_id]);
            $model->urgency_count = $req->input('urgency_count');
            $model->deadline_editable = ($req->input('deadline_editable') != null) ? 1 : 0;
            $model->supplier_threshold = $req->input('supplier_threshold');
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/settings/company');
            }
        } else {
            return response()->json(false);
        }
    }

    public function policy()
    {
        $breadcrumbs = [
            '/policy' => 'words.policy',
        ];
        return View::make('pages.settings.policy', compact('breadcrumbs'));
    }
}
