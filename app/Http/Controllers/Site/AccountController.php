<?php

namespace App\Http\Controllers\Site;

use App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;
use App\Models\Users;
use View;
use Illuminate\Support\Facades\Hash;
use Auth;
use Config;
use Mail;
use App\Models\Company;
use App\Mail\CompanyCreation;
use App\Mail\EndUserCreation;

class AccountController extends Controller
{
    public function createAccount(Request $req)
    {
        if (!empty($req->inviteToken)) {
            $hid = '';
            $user = Users::where('invite_user_token', $req->inviteToken)->first();
            //Setting the default Language
            if (!empty($user)) {
                if (!Session::has('lang') || !Session::has('userlang')) {
                    Session::put('lang', $user->language);
                    Session::put('langName', $user->language);
                    Session::put('userlang', $user->language);
                    App::setLocale(session('lang'));
                }
                if (!empty($user)) {
                    $hid = $user->id;
                    return View::make('pages.index.create_account', compact('hid'));
                } else {
                    return redirect('/');
                }
            } else {
                return redirect('/');
            }
        }
    }

    public function SaveCreateAccount(Request $req)
    {
        if (!empty($req->input())) {
            if (!empty(session('userlang'))) {
                session()->forget('userlang');
            }
            if (!empty(session('close_started'))) {
                session()->forget('close_started');
            }
            $user = Users::where('id', $req->input('hid'))->where('invite_user_token', '<>', '')->first();
            if (!empty($user)) {
                Users::where('email', $user->email)->update(['password' => Hash::make($req->input('password')), 'invite_user_token' => '']);
                $req->session()->flash('success', translate('alert.password_changed_all'));

                $guard = ($user->type == 1 || $user->type == 4) ? 'web' : 'customer';
                Auth::guard($guard)->login($user);
                if (!empty($user->language)) {
                    Session::put('lang', $user->language);
                    Session::put('langName', $user->language);
                    App::setLocale(session('lang'));
                }
                if (Users::where('email', $user->email)->count() > 1) {
                    return redirect('/entry-panel');
                } else {
                    $dateDiff = strtotime($user->company->trial_end) - strtotime(date('Y-m-d'));
                    $segment = ceil($dateDiff / (60 * 60 * 24));
                    if ($segment <= 0) {
                        if ($user->type == 2) {
                            $req->session()->flash("faliure", translate('alert.trial_period_over'));
                            return ($user->type == 3) ? redirect("/") : redirect("/membership");
                        } else {
                            $req->session()->flash("faliure", translate('error.profile_blocked'));
                            return redirect("/");
                        }
                    } else {
                        return redirect('/home');
                    }
                }
            } else {
                $req->session()->flash("faliure", translate('error.invalid_user'));
                return redirect("/");
            }
        }
    }

    public function resendInvitationMail(Request $req)
    {
        $userFullName = Auth::user()->name;
        $user = Users:: where('id', $req->userId)->first();
        $inviteUsrToken = $user->invite_user_token;
        $lang = Config::get('settings.lang');
        $mailData = [
            'id' => $inviteUsrToken,
            'created_by' => Auth::user()->name,
            'fname' => $user->fname,
            'name' => $user->name,
            'email' => $user->email,
            'company' => Auth::user()->company->name,
            'lang' => $lang[$user->language]
        ];
        if ($user->type == 2) {
            return Mail::to($mailData['email'], $mailData['name'])->bcc(Config::get('mail.from.address'))->send(new CompanyCreation($mailData));
        } elseif ($user->type == 3) {
            return Mail::to($mailData['email'], $mailData['name'])->bcc(Config::get('mail.from.address'))->send(new EndUserCreation($mailData));
        }
        return json_encode('success');
    }

    public function entryPanel(Request $req)
    {
        if (!empty($req->company_id)) {
            if (!empty(session('userlang'))) {
                session()->forget('userlang');
            }
            if (!empty(session('close_started'))) {
                session()->forget('close_started');
            }
            $user = Users::where('company_id', $req->company_id)->where('email', Auth::guard('customer')->user()->email)->first();
            Session::put('lang', $user->language);
            Session::put('langName', $user->language);
            Auth::guard('customer')->login($user);
            Users::where('id', $user->id)->update(['not_yet_logged' => 1]);
            $dateDiff = strtotime($user->company->trial_end) - strtotime(date('Y-m-d'));
            $segment = ceil($dateDiff / (60 * 60 * 24));
            if ($segment <= 0 && $user->type != 1) {
                $req->session()->flash("faliure", translate('alert.trial_period_over'));
                return ($user->type == 3) ? redirect("/") : redirect("/membership");
            }
            $req->session()->flash("intercomm", 1);
            return redirect('/home');
        }
        $email = Auth::guard('customer')->user()->email;
        Auth::guard('customer')->user()->password;
        //Setting the default Language
        if (!Session::has('lang') || !Session::has('userlang')) {
            Session::put('lang', Auth::guard('customer')->user()->language);
            Session::put('langName', Auth::guard('customer')->user()->language);
            Session::put('userlang', Auth::guard('customer')->user()->language);
            App::setLocale(session('lang'));
        }
        $companyIds = Users::where('email', $email)->inServiceWithEmail()->where('type', '<>', 1)->pluck('company_id');
        $companies = Company::whereIn('id', $companyIds)->get();
        return View::make('pages.index.entry_panel')->with('companies', $companies);
    }
}
