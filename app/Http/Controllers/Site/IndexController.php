<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;
use View;
use App\Http\Requests\IndexRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Models\Users;
use Illuminate\Support\Facades\Hash;
use Auth;
use App;
use App\Models\Languages;
use App\Models\Company;

class IndexController extends Controller
{
    public function index()
    {
        //Setting the default Language
        if (!Session::has('lang')) {
            Session::put('lang', 'nl');
            Session::put('langName', 'nl');
            App::setLocale(session('lang'));
        }
        return View::make('pages.index.index');
    }

    public function login(IndexRequest $req)
    {
        if (empty($req->input())) {
            return redirect('/');
        }
        $getUser = Users::inServiceWithEmail()->where('email', $req->input('username'));
        if ($getUser->count() == 0) {
            $req->session()->flash("faliure", translate('error.invalid_user'));
            return redirect("/");
        }
        $user = $getUser->first();
        if ($user->status != 1) {
            $req->session()->flash("faliure", translate('error.profile_blocked'));
            return redirect("/");
        }
        if (Hash::check($req->input('password'), $user->password) && $user->status == 1) {
            $guard = ($user->type == 1 || $user->type == 4) ? 'web' : 'customer';
            Auth::guard($guard)->login($user);
            if (!empty($user->language)) {
                session(['lang' => $user->language]);
                session(['langName' => $user->language]);
                App::setLocale(session('lang'));
            }
            if ($user->type == 4) {
                return redirect('/language');
            }
            if ($getUser->count() > 1) {
                return redirect('/entry-panel');
            }
            //Membership validity checks
            $dateDiff = strtotime($user->company->trial_end) - strtotime(date('Y-m-d'));
            $segment = ceil($dateDiff / (60 * 60 * 24));
            if ($segment <= 0 && $user->type != 1) {
                $req->session()->flash("faliure", translate('alert.trial_period_over'));
                return ($user->type == 3) ? redirect("/") : redirect("/membership");
            }
            $req->session()->flash("intercomm", 1);
            return redirect('/home');
        }
        $req->session()->flash("faliure", translate('error.invalid_user'));
        return redirect("/");
    }

    public function prevent(Request $req)
    {
        if (!empty($req->input())) {
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/');
        }
        return View::make('pages.index.prevent');
    }

    public function changeYourPassword()
    {
        return View::make('pages.index.change_your_password');
    }

    public function changeYourPasswordSave(ChangePasswordRequest $req)
    {
        if (!empty($req->input())) {
            $password = Hash::make($req->input('password'));
            Users::where('email', $req->input('hid'))->update(['password' => $password, 'not_yet_logged' => 1]);
            $req->session()->flash('success', translate('alert.password_changed_all'));
            return redirect('/home');
        }
    }

    public function setLang(Request $req)
    {
        session(['lang' => $req->name]);
        session(['langName' => $req->name]);
        App::setLocale(session('lang'));
    }

    public function languageManager(Request $req)
    {
        if (!empty($req->input('lang'))) {
            foreach ($req->input('lang') as $id => $eachLang) {
                Languages::where('id', $id)->update([
                    'nl' => $eachLang['nl'],
                    'en' => $eachLang['en'],
                    'fr' => $eachLang['fr'],
                ]);
            }
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/language');
        }
        $breadcrumbs = [
            '/language' => 'words.language',
        ];
        $languages = [];
        $data = Languages::all();
        if (!empty($data)) {
            foreach ($data as $each) {
                $languages[$each->segment][] = $each->toArray();
            }
        }

        return View::make('pages.settings.language', compact('breadcrumbs', 'languages'));
    }

    public function languageRestore()
    {
        $data = Languages::all();
        $lang = [];
        foreach ($data as $each) {
            $lang['nl'][$each->segment][$each->field] = $each->nl;
            $lang['en'][$each->segment][$each->field] = $each->en;
            $lang['fr'][$each->segment][$each->field] = $each->fr;
        }
        if (!empty($lang)) {
            foreach ($lang as $folder => $files) {
                foreach ($files as $file => $content) {
                    if (!file_exists($file)) {
                        $name = $file.'.php';
                        fopen(public_path('../resources/lang/' . $folder . '/'.$name), 'w');
                    }
                    $fullPath = public_path('../resources/lang/' . $folder . '/'.$name);
                    file_put_contents($fullPath, '<?php return $arr = ' . var_export($content, true) . '; ?>');
                }
            }
        }
        return response()->json(true);
    }

    public function logout()
    {
        if (!empty(session('dismiss_started'))) {
            Company::where('id', Auth::guard('customer')->user()->company_id)->update(['getstarted' => 1]);
        }
        Auth::guard('customer')->logout();
        Auth::guard('web')->logout();
        Session::flush();
        Session::put('lang', 'nl');
        Session::put('langName', 'nl');
        return redirect(\URL::previous());
    }
}
