<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\Language;
use App\Models\Countries;
use App\Models\OrganizationStructure;
use App\Models\Position;
use App\Models\Process;
use App\Models\Users;
use View;
use App\Http\Requests\ProfileRequest;
use App\Models\Company;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
use Config;
use Event;
use App\Events\User\UserPosAssign;
use App\Events\User\ProcessOwnership;
use App\Events\User\Auditor;

class ProfileController extends Controller
{
    public function profile()
    {
        $breadcrumbs = [
            '/profile' => 'words.profile',
        ];
        $details = (Auth::guard('customer')->check()) ? Auth::guard('customer')->user() : Auth::guard('web')->user();
        $company = $details->company;
        $orgStruct = OrganizationStructure::companyCheck();
        $orgStructExistance = $orgStruct->count();
        $language = Language::all()->pluck('display_text', 'name')->prepend(translate('form.select'), '');
        $langArr = Config::get('settings.lang');
        $name = 'name_' . $langArr[session('lang')];
        $countries = Countries::all()->pluck($name, 'id')->prepend(translate('form.select'), '');
        $positions = Position::companyStatusCheck()->whereNotIn('id', $orgStruct->pluck('position_id'))->unusedPositions();
        $customerAdminHandOverUser = [0 => translate('form.select')];
        $associatedUsers = $orgStruct->pluck('user_id')->toArray();
        $nonAssociatedUsers = Users::whereNotIn('id', $associatedUsers)->nonAssociatedUser();
        if ($orgStructExistance == 0) {
            $nonAssociatedUsers = [];
        }
        if (Auth::guard('customer')->check()) {
            $orgStructExistance = $orgStruct->count();
        }

        return View::make('pages.home.profile', compact('breadcrumbs', 'details', 'company', 'orgStructExistance', 'positions', 'nonAssociatedUsers', 'customerAdminHandOverUser', 'countries', 'language'));
    }

    public function profileSave(ProfileRequest $req)
    {
        if (!empty($req->input())) {
            $userMasterModel = Users::find($req->input('hid'));
            if ($userMasterModel->type == 2) {
                $companyModel = Company::find($userMasterModel->company_id);
                $companyModel->name = $req->input('name');
                $companyModel->street = $req->input('street');
                $companyModel->nr = $req->input('nr');
                $companyModel->bus = $req->input('bus');
                $companyModel->zip = $req->input('zip');
                $companyModel->city = $req->input('city');
                $companyModel->country = $req->input('country');
                $companyModel->tva_no = $req->input('tva_no');
                if (!empty($req->file('company_photo'))) {
                    $companyModel->photo = fileUpload($req->file('company_photo'), 'file', '/image/', 'Company-');
                }
                $companyModel->status = ($req->input('block') != null) ? 2 : 1;
                $companyModel->updated_by = Auth::user()->id;
                $companyModel->save();
            }
            $userMasterModel->name = $req->input('fname') . ' ' . $req->input('lname');
            $userMasterModel->fname = $req->input('fname');
            $userMasterModel->lname = $req->input('lname');
            if (!empty($req->file('photo'))) {
                $userMasterModel->photo = fileUpload($req->file('photo'), 'file', '/image/users/', 'User-');
            }
            $userMasterModel->email = $req->input('email');
            if (!empty($userMasterModel->company->subscription_id)) {
                $userMasterModel->language = $req->input('language');
                $userMasterModel->can_process_owner = ($req->input('can_process_owner') != null) ? 1 : 0;
                $userMasterModel->is_auditor = ($req->input('is_auditor') != null) ? 1 : 0;
            }
            $userMasterModel->out_of_service = ($req->input('out_of_service') != null) ? $req->input('out_of_service') : null;
            $userMasterModel->updated_by = Auth::user()->id;
            $userMasterModel->save();
            if (!empty($userMasterModel->company->subscription_id)) {
                Event::fire(new ProcessOwnership($req->input('can_process_owner'), $req->input('hid')));
                Event::fire(new Auditor($req->input('is_auditor'), $req->input('hid')));
            }
            Event::fire(new UserPosAssign($req->input('functions'), $userMasterModel->company_id, $userMasterModel->id, null));
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/home');
        } else {
            return response()->json(false);
        }
    }
}
