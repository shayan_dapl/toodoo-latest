<?php

namespace App\Http\Controllers;

use App\Http\Requests\StakeholderRequest;
use App\Models\StakeholderAnalysis;
use App\Models\StakeholderCategory;
use App\Models\ContextStakeholderActionLog;
use App\Models\ContextStakeholderActionComments;
use App\Models\ContextStakeholderActionFiles;
use App\Models\Users;
use App\Models\Branch;
use App\Models\StakeholderBranch;
use Auth;
use Illuminate\Http\Request;
use View;
use Session;
use App\Repositories\Stakeholder as StakeholderRepo;

class StakeholderController extends Controller
{
    protected $repo;

    public function __construct(StakeholderRepo $stakeholderRepo)
    {
        $this->repo = $stakeholderRepo;
    }
    public function map(Request $req)
    {
        $breadcrumbs = ['/stakeholder-analysis/map' => 'words.stakeholder_analysis'];
        $data = $this->repo->getstackholderdata();
        $stakes = $data;
        $stakes[] = [
            "id" => "",
            "name" => "",
            "category" => "",
            "price" => 0,
            "rating" => 0,
            "occurrence" => 0,
            "delegate" => "",
            "deadline" => "",
            "status" => ""
        ];
        $stakes[] = [
            "id" => "",
            "name" => "",
            "category" => "",
            "price" => 5,
            "rating" => 5,
            "occurrence" => 0,
            "delegate" => "",
            "deadline" => "",
            "status" => ""
        ];
        $stakes = json_encode($stakes);
        $branchData = Branch::companyStatusCheck();
        $branches = $branchData->pluck('name', 'id')->prepend(translate('form.select'), '0');
        $selectedBranch = '';
        if ($branchData->count() == 1) {
            $selectedBranch = $branchData->pluck('id')[0];
        }
        return View::make('pages.stakeholder.map', compact('breadcrumbs', 'stakes', 'branches', 'selectedBranch'));
    }

    public function filterStackMap(Request $req)
    {
        $data = $this->repo->getstackholderdata();
        $filteredData = [];
        foreach ($data as $eachData) {
            if ($req->branchId != 0) {
                $getBranches = StakeholderBranch::where('stakeholder_id', $eachData['id'])->where('branch_id', $req->branchId)->count();
                if ($getBranches > 0) {
                    $filteredData [] = $eachData;
                }
            } else {
                $filteredData [] = $eachData;
            }
        }

        $stakes = $filteredData;
        $stakes[] = [
            "id" => "",
            "name" => "",
            "category" => "",
            "price" => 0,
            "rating" => 0,
            "occurrence" => 0,
            "delegate" => "",
            "deadline" => "",
            "status" => ""
        ];
        $stakes[] = [
            "id" => "",
            "name" => "",
            "category" => "",
            "price" => 5,
            "rating" => 5,
            "occurrence" => 0,
            "delegate" => "",
            "deadline" => "",
            "status" => ""
        ];
        $stakes = json_encode($stakes);

        return View::make('pages.stakeholder.filtered_map', compact('stakes'));
    }

    public function category(Request $req)
    {
        $breadcrumbs = [
            '/stakeholder-analysis/map' => 'words.stakeholder_analysis',
            '/stakeholder-analysis/category' => 'words.category',
        ];
        $showDemo = '';
        return View::make('pages.stakeholder.category', compact('breadcrumbs', 'showDemo'));
    }

    public function categoryData(Request $req)
    {
        return StakeholderCategory::companyCheck()->get()->toJson();
    }

    public function categorySave(Request $req)
    {
        if (!empty($req->input())) {
            if (empty($req->input('hid'))) {
                $sameData = StakeholderCategory::companyCheck()->where('name', $req->input('name'))->count();
                if ($sameData) {
                    return response()->json(false);
                } else {
                    $model = StakeholderCategory::findOrNew($req->input('hid'));
                    $model->company_id = Auth::guard('customer')->user()->company_id;
                    $model->name = $req->input('name');
                    if ($model->save()) {
                        return response()->json(true);
                    }
                }
            } else {
                $model = StakeholderCategory::findOrNew($req->input('hid'));
                $model->company_id = Auth::guard('customer')->user()->company_id;
                $model->name = $req->input('name');
                if ($model->save()) {
                    return response()->json(true);
                }
            }
        }
    }

    public function categoryRemove(Request $req)
    {
        $data = StakeholderCategory::where('id', $req->input('id'))->first();
        if ($data->analysis->count() > 0) {
            return response()->json(false);
        } else {
            StakeholderCategory::where('id', $req->input('id'))->delete();
            return response()->json(true);
        }
    }

    public function list(Request $req)
    {
        $breadcrumbs = [
            '/stakeholder-analysis/map' => 'words.stakeholder_analysis',
            '/stakeholder-analysis/list' => 'words.list',
        ];
        $data = StakeholderAnalysis::companyStatusCheck()->get();
        return View::make('pages.stakeholder.list', compact('breadcrumbs', 'data'));
    }

    public function form(Request $req)
    {
        $breadcrumbs = [
            '/stakeholder-analysis/map' => 'words.stakeholder_analysis',
            '/stakeholder-analysis/list' => 'words.list',
            '/stakeholder-analysis/form' => 'words.add',
        ];
        $category = StakeholderCategory::companyCheck()->get()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $users = Users::companyCheck()->where('out_of_service', null)->where('no_email', 0)->pluck('name', 'id')->prepend(translate('form.select'), '');
        $branches = Branch::companyStatusCheck()->pluck('name', 'id');
        if ($branches->count() == 1) {
            foreach ($branches as $eachBranch=>$curBranch) {
                $selectedBranches [] = $eachBranch;
            }
        } else {
            $selectedBranches = [];
        }
        return View::make('pages.stakeholder.form', compact('breadcrumbs', 'category', 'users', 'branches', 'selectedBranches'));
    }

    public function save(StakeholderRequest $req)
    {
        if (!empty($req->input())) {
            $model = StakeholderAnalysis::findOrNew($req->input('hid'));
            $model->company_id = Auth::guard('customer')->user()->company_id;
            $model->type = $req->input('internal_external');
            $model->category_id = $req->input('category');
            $model->stakeholder_name = $req->input('stakeholder');
            $model->needs = $req->input('needs_expectations');
            $model->detail = $req->input('detail');
            $model->impact = $req->input('impacts');
            $model->interest = $req->input('interests');
            if ($model->save()) {
                StakeholderBranch::where('stakeholder_id', $model->id)->delete();
                foreach ($req->input('branches') as $eachBranch) {
                    $stakeholderBranchModel = new StakeholderBranch;
                    $stakeholderBranchModel->stakeholder_id = $model->id;
                    $stakeholderBranchModel->branch_id =  $eachBranch;
                    $stakeholderBranchModel->save();
                }
                if ($req->input('action_yes_no') == "Yes") {
                    $action = new ContextStakeholderActionLog;
                    $action->company_id = Auth::guard('customer')->user()->company_id;
                    $action->type = "stakeholder";
                    $action->contxt_stakehold_id = $model->id;
                    $action->action = $req->input('action');
                    $action->deligate_to = $req->input('who');
                    $action->deadline = $req->input('deadline');
                    $action->save();
                }
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/stakeholder-analysis/list');
            }
        }
    }

    public function edit(Request $req)
    {
        $breadcrumbs = [
            '/stakeholder-analysis/map' => 'words.stakeholder_analysis',
            '/stakeholder-analysis/list' => 'words.list',
            '#' => 'words.edit',
        ];
        $category = StakeholderCategory::companyCheck()->get()->pluck('name', 'id')->prepend(translate('form.select'), '');
        $details = StakeholderAnalysis::find($req->id);
        if (!checkCompanyAccess($details)) {
            $req->session()->flash('error', translate('words.you_dont_have_access'));
            return redirect('/home');
        }
        $users = Users::companyCheck()->where('out_of_service', null)->where('no_email', 0)->pluck('name', 'id')->prepend(translate('form.select'), '');
        $branches = Branch::companyStatusCheck()->pluck('name', 'id');
        $selectedBranches = [];
        foreach ($details->branches as $branch) {
            $selectedBranches [] = $branch->branch_id;
        }
        $stakeholderActionLog = ContextStakeholderActionLog::where('contxt_stakehold_id', $req->id)->where('type', 'stakeholder')->get();
        return View::make('pages.stakeholder.form', compact('breadcrumbs', 'category', 'details', 'users', 'branches', 'selectedBranches', 'stakeholderActionLog'));
    }
    public function remove(Request $req)
    {
        if (!empty($req->id)) {
            $model = StakeholderAnalysis::find($req->id);
            if (!checkCompanyAccess($model)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $model->status = 3;
            if ($model->save()) {
                $actionlogs = ContextStakeholderActionLog::where('contxt_stakehold_id', $req->id)->where('type', 'stakeholder')->pluck('id');
                ContextStakeholderActionComments::where('type', 'stakeholder')->whereIn('log_id', $actionlogs)->delete();
                ContextStakeholderActionLog::where('contxt_stakehold_id', $req->id)->where('type', 'stakeholder')->delete();
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('stakeholder-analysis/list');
            }
        }
    }
}
