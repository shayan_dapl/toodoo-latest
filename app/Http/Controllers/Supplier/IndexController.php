<?php

namespace App\Http\Controllers\Supplier;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Supplier;
use View;
use App\Http\Requests\SupplierRequest;
use Auth;
use App\Http\Requests\SupplierUploadRequest;
use Illuminate\Support\Facades\File;
use Excel;

class IndexController extends Controller
{
    public function list()
    {
        $breadcrumbs = [
            '/supplier/list' => 'words.supplier_evaluation',
            '#' => 'words.list'
        ];
        return View::make('pages.supplier.list', compact('breadcrumbs'));
    }

    public function listData()
    {
        $data = Supplier::companyStatusCheck()->get();
        return response()->json($data);
    }

    public function save(SupplierRequest $req)
    {
        if (!empty($req->input())) {
            $model = Supplier::findOrNew($req->input('id'));
            $model->company_id = Auth::guard('customer')->user()->company_id;
            $model->name = $req->input('name');
            $model->service = $req->input('service');
            $model->comment = $req->input('comment');
            if ($model->save()) {
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }

    public function remove(Request $req)
    {
        if (!empty($req->id)) {
            $data = Supplier::companyStatusCheck()->where('id', $req->id)->first();
            if (empty($data->action())) {
                Supplier::where('id', $req->id)->update(['status' => 3]);
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }

    public function status(Request $req)
    {
        if (!empty($req->input('id'))) {
            if ($req->input('type') == "evaluated") {
                Supplier::where('id', $req->input('id'))->update(['is_eval' => $req->input('make')]);
            }
            if ($req->input('type') == "active") {
                Supplier::where('id', $req->input('id'))->update(['is_active' => $req->input('make')]);
            }
            return response()->json(true);
        } else {
            return response()->json(false);
        }
    }

    public function preview(SupplierUploadRequest $req)
    {
        $data = Excel::load($req->file('doc_file')->getRealPath())->get()->toArray();
        if (empty($data)) {
            return response()->json(false);
        }
        $preview = [];
        foreach ($data as $each) {
            $remark = [];
            foreach ($each as $header => $value) {
                switch ($header) {
                    case 'suppliername':
                        $process = Supplier::companyStatusCheck()->where('name', $each[$header])->first();
                        $remark[] = empty(trim($each[$header])) ? translate('error.add_supplier') : '';
                        $remark[] = (!empty($process)) ? translate('error.supplier_exist') : '';
                        break;
                    case 'productservice':
                        $remark[] = empty(trim($each[$header])) ? translate('error.add_product_service') : '';
                        break;
                }
            }
            $preview[] = [
                'supplier' => $each['suppliername'],
                'product' => $each['productservice'],
                'comment' => $each['comment'],
                'remark' => array_filter($remark)
            ];
        }
        $breadcrumbs = ['/supplier/list' => 'words.supplier', '#' => 'words.supplier_preview'];
        return View::make('pages.supplier.preview', compact('breadcrumbs', 'preview'));
    }

    public function upload(Request $req)
    {
        $data = json_decode($req->input('data'), true);
        foreach ($data as $each) {
            if (empty($each['remark'])) {
                $model = new Supplier;
                $model->company_id = Auth::user()->company_id;
                $model->name = $each['supplier'];
                $model->service = $each['product'];
                $model->comment = $each['comment'];
                $model->save();
            }
        }
        $req->session()->flash('success', translate('words.success_message'));
        return redirect('/supplier/list');
    }
}
