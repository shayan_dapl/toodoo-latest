<?php

namespace App\Http\Controllers\Supplier;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\SupplierParameter;
use View;
use App\Http\Requests\SupplierParameterRequest;
use Auth;

class ParameterController extends Controller
{
    public function list()
    {
        $breadcrumbs = [
            '/supplier/parameters' => 'words.parameters',
            '#' => 'words.list'
        ];
        return View::make('pages.supplier.parameters_list', compact('breadcrumbs'));
    }

    public function listData()
    {
        $data = SupplierParameter::companyStatusCheck()->get();
        return response()->json($data);
    }

    public function save(SupplierParameterRequest $req)
    {
        if (!empty($req->input())) {
            $model = SupplierParameter::findOrNew($req->input('id'));
            $model->company_id = Auth::guard('customer')->user()->company_id;
            $model->name = $req->input('name');
            if ($model->save()) {
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }

    public function remove(Request $req)
    {
        if (!empty($req->id)) {
            $data = SupplierParameter::where('id', $req->id)->first();
            if ($data->rates->count() == 0) {
                SupplierParameter::where('id', $req->id)->update(['status' => 3]);
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }
}
