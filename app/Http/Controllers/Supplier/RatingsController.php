<?php

namespace App\Http\Controllers\Supplier;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\SupplierRating;
use App\Models\SupplierEvaluationActionLog;
use App\Models\SupplierEvaluationActionComments;
use App\Models\SupplierEvaluationActionFiles;
use View;
use App\Http\Requests\SupplierRatingRequest;
use Auth;

class RatingsController extends Controller
{
    public function list()
    {
        $breadcrumbs = [
            '/supplier/ratings' => 'words.ratings',
            '#' => 'words.list'
        ];
        return View::make('pages.supplier.ratings_list', compact('breadcrumbs'));
    }

    public function listData()
    {
        $data = SupplierRating::companyStatusCheck()->get();
        return response()->json($data);
    }

    public function save(SupplierRatingRequest $req)
    {
        if (!empty($req->input())) {
            $model = SupplierRating::findOrNew($req->input('id'));
            $model->company_id = Auth::guard('customer')->user()->company_id;
            $model->rating = $req->input('rating');
            $model->description = $req->input('description');
            if ($model->save()) {
                return response()->json(true);
            }
        } else {
            return response()->json(false);
        }
    }

    public function remove(Request $req)
    {
        if (!empty($req->id)) {
            $data = SupplierRating::where('id', $req->id)->first();
            if ($data->rates->count() == 0) {
                SupplierRating::where('id', $req->id)->update(['status' => 3]);
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }
    public function issueFixing(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '#' => 'words.action',
            ];
            $log = SupplierEvaluationActionLog::find($req->id);
            return View::make('pages.supplier.supplier_rating_issue_fixing', compact('breadcrumbs', 'log'));
        }
    }

    public function issueFixingSave(Request $req)
    {
        if (!empty($req->input())) {
            //Comment adding
            if (!empty($req->input('reply'))) {
                $comment = new SupplierEvaluationActionComments;
                $comment->company_id = Auth::guard('customer')->user()->company_id;
                $comment->log_id = $req->input('action_log_id');
                $comment->reply = $req->input('reply');
                $comment->save();
            }

            //Supporting document uploading per comment
            if (!empty($req->file('upload_doc'))) {
                foreach ($req->file('upload_doc') as $index => $file) {
                    $filename = time() . '-' . $file->getClientOriginalName();
                    $file->move(
                        storage_path('app/public/company/' . Auth::guard('customer')->user()->company_id . '/documents/supplier_rating_action_docs/'),
                        $filename
                    );
                    $fileModel = new SupplierEvaluationActionFiles;
                    $fileModel->comment_id = $comment->id;
                    $fileModel->doc_name = $filename;
                    $fileModel->submitted_by = Auth::guard('customer')->user()->id;
                    $fileModel->save();
                }
            }

            //Action log closed
            if ($req->input('closed')) {
                SupplierEvaluationActionLog::where('id', $req->input('action_log_id'))->update(['closed' => 1, 'closed_at' => date('Y-m-d')]);
            }

            $req->session()->flash('success', translate('words.success_message'));
            return redirect('home/issues');
        }
    }
}
