<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App;
use App\Http\Requests\UserRequest;
use App\Models\Company;
use App\Models\Language;
use App\Models\OrganizationStructure;
use App\Models\Position;
use App\Models\Process;
use App\Models\Users;
use App\Models\PositionLog;
use Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
use Session;
use View;
use Config;
use Event;
use App\Events\User\UserPosAssign;
use App\Events\User\ProcessOwnership;
use App\Events\User\Auditor;
use Mail;
use App\Mail\CompanyCreation;
use App\Mail\EndUserCreation;

class IndexController extends Controller
{
    public function userList()
    {
        $breadcrumbs = [
            '/user/list' => 'words.user',
            '#' => 'words.list',
        ];
        $users = Users::companycheck()->orderBy('id', 'DESC')->get();
        return View::make('pages.user.user_list', compact('breadcrumbs', 'users'));
    }

    public function userForm(Request $req)
    {
        $breadcrumbs = [
            '/user/list' => 'words.user',
            '#' => 'words.add',
        ];
        $language = Language::all()->pluck('display_text', 'name')->prepend(translate('form.select'), '');
        $orgStruct = OrganizationStructure::companyCheck();
        $orgStructExistance = $orgStruct->count();
        $positions = Position::companyStatusCheck()->whereNotIn('id', $orgStruct->pluck('position_id'))->unusedPositions();
        $specialPositions = Position::companyStatusCheck()->unusedSpecialPositions();
        return View::make('pages.user.user_form', compact('breadcrumbs', 'positions', 'orgStructExistance', 'specialPositions', 'language'));
    }

    public function userFormSave(UserRequest $req)
    {
        if (!empty($req->input())) {
            $sendMailAgain = 0;
            $userMasterModel = Users::findOrNew($req->input('hid'));
            $createdBy = !empty($userMasterModel->exists) ? $userMasterModel->created_by : Auth::user()->id;
            $updatedBy = !empty($userMasterModel->exists) ? Auth::user()->id : 0;
            $userMasterModel->company_id = Auth::user()->company->id;
            $userMasterModel->name = $req->input('fname') . ' ' . $req->input('lname');
            $userMasterModel->fname = $req->input('fname');
            $userMasterModel->lname = $req->input('lname');
            if (!empty($req->input('photo'))) {
                $userMasterModel->photo = fileUpload($req->input('photo'), 'ajaximage', '/image/users/', 'User-');
            }
            $userMasterModel->language = $req->input('language');
            $userMasterModel->email = ($req->input('no_email') == null) ? $req->input('email') : null;
            $userMasterModel->remember_token = $req->input('_token');
            $userMasterModel->type = $req->input('usertype');
            $userMasterModel->is_auditor = ($req->input('is_auditor') != null) ? 1 : 0;
            $userMasterModel->out_of_service = ($req->input('out_of_service') != null) ? $req->input('out_of_service') : null;
            $userMasterModel->can_process_owner = ($req->input('can_process_owner') != null) ? 1 : 0;
            $userMasterModel->is_special_user = ($req->input('is_special_user') != null) ? 1 : 0;
            if ($userMasterModel->no_email == 1 && $req->input('no_email') == null) {
                $sendMailAgain = 1;
            }
            $userMasterModel->no_email = ($req->input('no_email') != null) ? 1 : 0;
            if (empty($model->exists) && $req->input('invite_user') != '') {
                $userMasterModel->invite_user_token = rand('1000', '9999').time();
            }
            $userMasterModel->created_by = $createdBy;
            $userMasterModel->updated_by = $updatedBy;
            $userMasterModel->save();
            if ($sendMailAgain == 1) {
                $lang = Config::get('settings.lang');
                $createdBy = ($userMasterModel->type != 2) ? Auth::guard('customer')->user()->name : 1;
                $mailData = ['id' => $userMasterModel->invite_user_token, 'created_by' => $createdBy, 'fname' => $userMasterModel->fname, 'name' => $userMasterModel->name, 'email' => $userMasterModel->email, 'company' => $userMasterModel->company->name, 'lang' => $lang[$userMasterModel->language]];
                switch ($userMasterModel->type) {
                    case '2':
                        Mail::to($mailData['email'], $mailData['name'])->bcc(Config::get('mail.from.address'))->send(new CompanyCreation($mailData));
                        break;
                    default:
                        Mail::to($mailData['email'], $mailData['name'])->bcc(Config::get('mail.from.address'))->send(new EndUserCreation($mailData));
                        break;
                }
            }
            Event::fire(new ProcessOwnership($req->input('can_process_owner'), $req->input('hid'), $req->input('no_email'), $req->input('email')));
            Event::fire(new Auditor($req->input('is_auditor'), $req->input('hid'), $req->input('no_email'), $req->input('email')));
            Event::fire(new UserPosAssign($req->input('functions'), $userMasterModel->company_id, $userMasterModel->id, $req->input('is_special_user')));
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/user/list');
        } else {
            return response()->json(false);
        }
    }

    public function userEdit(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/user/list' => 'words.user',
                '#' => 'words.edit',
            ];
            $details = Users::find($req->id);
            if (!checkCompanyAccess($details)) {
                $req->session()->flash('error', translate('words.you_dont_have_access'));
                return redirect('/home');
            }
            $email = ($details->is_special_user == 1) ? "" : (($details->no_email == 1) ? "" : "required");
            $noEmail = ($details->no_email == 1) ? "hide" : "";
            $language = Language::all()->pluck('display_text', 'name')->prepend(translate('form.select'), '');
            $orgStruct = OrganizationStructure::companyCheck();
            $orgStructExistance = $orgStruct->count();
            $positions = Position::companyStatusCheck()->whereNotIn('id', $orgStruct->pluck('position_id'))->unusedPositions();
            $specialPositions = Position::companyStatusCheck()->unusedSpecialPositions();
            $associatedUsers = $orgStruct->pluck('user_id')->toArray();
            $nonAssociatedUsers = Users::where('id', '<>', $req->id)->nonAssociatedUser();
            $nonAssociatedSpecialUsers = Users::where('id', '<>', $req->id)->nonAssociatedSpecialUser();
            if ($orgStructExistance == 0) {
                $nonAssociatedUsers = [];
                $nonAssociatedSpecialUsers = [];
            }
            $special = '';
            if ($details->is_special_user == 1) {
                $special = $details->specialPositions();
            }
            $onlyCAdmin = Users::companyCheck()->inServiceWithEmail()->where('type', 2)->where('id', '<>', $req->id)->get();
            $onlyProcessOwner = Users::companyCheck()->inServiceWithEmail()->where('can_process_owner', 1)->where('id', '<>', $req->id)->get();
            $onlyAuditor = Users::companyCheck()->inServiceWithEmail()->where('is_auditor', 1)->where('id', '<>', $req->id)->get();
            $users = Users::companyCheck()->inServiceWithEmail()->where('id', '<>', $req->id)->get();

            return View::make('pages.user.user_form', compact('breadcrumbs', 'details', 'positions', 'orgStructExistance', 'specialPositions', 'language', 'email', 'noEmail', 'nonAssociatedUsers', 'nonAssociatedSpecialUsers', 'special', 'onlyCAdmin', 'onlyProcessOwner', 'onlyAuditor', 'users'));
        } else {
            return response()->json(false);
        }
    }

    public function positionLog(Request $req)
    {
        if (empty($req->id)) {
            return response()->json(false);
        }
        $breadcrumbs = [
                '/user/list' => 'words.user',
                '#' => 'words.position_log',
            ];
        $positionLog = PositionLog::where('user_id', $req->id)->get();
        return View::make('pages.user.position_log', compact('breadcrumbs', 'positionLog'));
    }
}
