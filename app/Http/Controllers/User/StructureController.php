<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App;
use App\Models\ActionDelegateMember;
use App\Models\AuditPlanAuditees;
use App\Models\AuditPlanAuditors;
use App\Models\AuditPlanMaster;
use App\Models\Company;
use App\Models\Branch;
use App\Models\DepartmentBranch;
use App\Models\Department;
use App\Models\OrganizationStructure;
use App\Models\Position;
use App\Models\Process;
use App\Models\ReportedIssuesDetails;
use App\Models\CompetentRatingsActionLog;
use App\Models\Users;
use Auth;
use Session;
use View;

class StructureController extends Controller
{
    public function index(Request $req)
    {
        $breadcrumbs = [
            '/user/list' => 'words.user',
            '#' => 'words.structure',
        ];
        $branchData = Branch::companyStatusCheck();
        $branches = DepartmentBranch::companyCheck()->with('branch')->get()->pluck('branch.name', 'branch.id')->toArray();
        $branch = $branchData->where('is_corporate', 1)->first();
        $departments = $branch->departments()->with('department')->get()->where('department.status', 1)->pluck('department.name', 'department.id')->prepend(translate('words.show_all'), 'all')->toArray();
        $department = "all";
        $tree = [];
        $structure = "";
        $dept = $branch->departments()->with('department')->get()->where('department.status', 1)->where('department.is_top', 1)->first()->department;
        if (!empty($req->branch)) {
            $branch = Branch::companyStatusCheck()->where('id', $req->branch)->first();
            if ($branch->departments->count() > 0) {
                $departments = $branch->departments()->with('department')->get()->where('department.status', 1)->pluck('department.name', 'department.id')->prepend(translate('words.show_all'), 'all')->toArray();
                $dept = $branch->departments()->with('department')->get()->where('department.status', 1)->where('department.is_top', 1)->first()->department;
                if (!empty($req->department) && $req->department != "all") {
                    $department = $req->department;
                    $dept = Department::find($department);
                }
            }
        }
        if (!empty($dept)) {
            $position = $dept->positions()->first();
            if (!empty($position)) {
                $structureArr = $position->structure->toArray();
                if (!empty($structureArr)) {
                    $structureArr[0]['name'] = $structureArr[0]['title'];
                    $structureArr[0]['title'] = $structureArr[0]['fullname'];
                    $tree[0] = $structureArr[0];
                    $tree[0]['children'] = organizationStructure($position, $dept->is_top, $department, $branch->id);
                }
            }
        }
        $treeStyle = (object) $tree;
        $structure = json_encode($treeStyle, true);
        return View::make('pages.user.user_structure', compact('breadcrumbs', 'branches', 'branch', 'departments', 'department', 'tree', 'structure'));
    }

    public function parentPosition(Request $req)
    {
        if (!empty($req->position_id)) {
            $parent = Position::find($req->position_id)->parent_id;
            $parentCheck = OrganizationStructure::companyCheck()->where('position_id', $parent);
            if ($parent != null && $parentCheck->count() > 0) {
                $response = [
                    'id' => $parentCheck->first()->id,
                    'name' => ($parentCheck->first()->user->name .' ('. $parentCheck->first()->position->name .')')
                ];
                return response()->json($response);
            } else {
                if ($parent == null && $parentCheck->count() == 0) {
                    $response = [
                        'id' => 0,
                        'name' => translate('form.report_none')
                    ];
                    return response()->json($response);
                } else {
                    return response("no_parent", 200)->header('Content-Type', 'text/plain');
                }
            }
        } else {
            return null;
        }
    }

    public function structureRemove(Request $req)
    {
        if (!empty($req->id)) {
            $user = OrganizationStructure::companyCheck()->where('id', $req->id)->first()->user;
            $positionIds = OrganizationStructure::companyCheck()->where('id', $req->id)->first()->position->children->pluck('id')->toArray();
            $checkChildren = OrganizationStructure::whereIn('position_id', $positionIds)->count();
            if ($checkChildren > 0) {
                return response("no", 200)->header('Content-Type', 'text/plain');
            } else {
                $deleted = removeOrgPosition($req->id);
                if ($deleted == "success") {
                    $checkTopPos = $user->positions()->pluck('position.parent_id')->toArray();
                    if (!in_array("", $checkTopPos)) {
                        Users::where('id', $user->id)->update(['is_top_person' => 0]);
                    }
                    return response("yes", 200)->header('Content-Type', 'text/plain');
                }
            }
        } else {
            return response()->json(false);
        }
    }

    public function structureRemoveAll(Request $req)
    {
        if (!empty($req->id) && !empty($req->company_id)) {
            $getpositions = OrganizationStructure::where('company_id', $req->company_id)->where('user_id', $req->id)->get();
            $flag = 0;
            foreach ($getpositions as $k => $v) {
                $checkChildren = OrganizationStructure::where('parent', $v['id'])->count();
                if ($checkChildren > 0) {
                    $flag = 1;
                }
            }
            if ($flag > 0) {
                return response("no", 200)->header('Content-Type', 'text/plain');
            } else {
                $deleted = removeOrgPositionByuser($req->id, $req->company_id);
                if ($deleted == "success") {
                    Users::where('id', $req->id)->update(['is_special_user' => 1]);
                    return response("yes", 200)->header('Content-Type', 'text/plain');
                }
            }
        }
    }

    public function rmSpecialPos(Request $req)
    {
        if (!empty($req->id) && !empty($req->company_id)) {
            OrganizationStructure::where('company_id', $req->company_id)->where('user_id', $req->id)->where('special_user', 1)->delete();
            Users::where('id', $req->id)->update(['is_special_user' => 0]);
            return response("yes", 200)->header('Content-Type', 'text/plain');
        } else {
            return response()->json(false);
        }
    }

    public function exchangePosition(Request $req)
    {
        if (!empty($req->input())) {
            if (!empty($req->input('to'))) {
                foreach ($req->input('to') as $segment => $each) {
                    switch ($segment) {
                        case 'position':
                            foreach ($each as $present => $new) {
                                if (!empty($new)) {
                                    $orgStruct = OrganizationStructure::where('id', $present)->first();
                                    $orgStruct->user_id = $new;
                                    $orgStruct->save();
                                }
                            }
                            break;
                        case 'process':
                            foreach ($each as $present => $new) {
                                if (!empty($new)) {
                                    Process::where('id', $present)->update(['owner_id' => $new]);
                                }
                            }
                            break;
                        case 'plannedaudit':
                            foreach ($each as $present => $new) {
                                if (!empty($new)) {
                                    AuditPlanMaster::where('id', $present)->update(['choose_lead_auditor' => $new]);
                                }
                            }
                            break;
                        case 'auditmember':
                            foreach ($each as $present => $new) {
                                if (!empty($new)) {
                                    AuditPlanAuditors::where('id', $present)->update(['auditor_id' => $new]);
                                }
                            }
                            break;
                        case 'auditee':
                            foreach ($each as $present => $new) {
                                if (!empty($new)) {
                                    AuditPlanAuditees::where('id', $present)->update(['user_id' => $new]);
                                }
                            }
                            break;
                        case 'external':
                            foreach ($each as $present => $new) {
                                if (!empty($new)) {
                                    ReportedIssuesDetails::where('id', $present)->update(['who' => $new]);
                                }
                            }
                            break;
                        case 'delegate':
                            foreach ($each as $present => $new) {
                                if (!empty($new)) {
                                    ActionDelegateMember::where('id', $present)->update(['member' => $new]);
                                }
                            }
                            break;
                        case 'competent':
                            foreach ($each as $present => $new) {
                                if (!empty($new)) {
                                    CompetentRatingsActionLog::where('id', $present)->update(['delegate_to' => $new]);
                                }
                            }
                            break;
                    }
                }
            }

            if ($req->input('audit_handover') == null) {
                Users::where('id', $req->input('current_user'))
                ->update(['out_of_service' => implode('-', array_reverse(explode("-", $req->input('out_service_date')))), 'status' => 3]);
            }
            $req->session()->flash('success', translate('words.success_message'));
            return redirect('/user/list');
        } else {
            return response()->json(false);
        }
    }
}
