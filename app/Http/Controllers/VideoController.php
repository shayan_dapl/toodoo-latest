<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\VideoRequest;
use App\Models\VideoMaster;
use App\Models\VideoCategory;
use Illuminate\Http\Request;
use App\Http\Requests\VideoCatRequest;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
use View;
use Config;

class VideoController extends Controller
{
    /////Video Management
    public function catList(Request $req)
    {
        $breadcrumbs = [
            'video/category/list' => 'words.video_category',
            '#' => 'words.list'
        ];
        $details = VideoCategory::where('status', 1)->orderBy('sequence', 'ASC')->get();

        return View::make('pages.settings.video.video_cat_list', compact('breadcrumbs', 'details'));
    }

    public function catForm(Request $req)
    {
        $breadcrumbs = [
            '/video/category/list' => 'words.video_category',
            '#' => 'words.add',
        ];
        $category = VideoCategory::where('status', 1)->where('parent_id', 0)
        ->pluck('vid_cat_'.session('lang'), 'id')->prepend(translate('form.select'), '0');

        return View::make('pages.settings.video.video_cat_form', compact('breadcrumbs', 'category'));
    }

    public function catFormSave(VideoCatRequest $req)
    {
        if (!empty($req->input())) {
            $model = VideoCategory::findOrNew($req->input('hid'));
            $model->parent_id = $req->input('parent_cat');
            $model->sequence = ($req->input('parent_cat') == 0) ? $req->input('sequence') : 9999;
            $model->vid_cat_nl = $req->input('vid_cat_nl');
            $model->vid_cat_en = $req->input('vid_cat_en');
            $model->vid_cat_fr = $req->input('vid_cat_fr');
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('video/category/list');
            }
        }
    }

    public function catFormEdit(Request $req)
    {
        if (!empty($req->id)) {
            $breadcrumbs = [
                '/video/category/list' => 'words.video_category',
                '#' => 'words.edit',
            ];
            $details = VideoCategory::find($req->id);
            $category = VideoCategory::where('parent_id', 0)->pluck('vid_cat_'.session('lang'), 'id')->prepend(translate('form.select'), '0');
            return View::make('pages.settings.video.video_cat_form', compact('breadcrumbs', 'details', 'category'));
        }
    }

    public function catDelete(Request $req)
    {
        if (!empty($req->id)) {
            $model = VideoCategory::find($req->id);
            $model->status = 3;
            if ($model->save()) {
                $getchiledIds = VideoCategory::where('parent_id', $req->id)->pluck('id')->toArray();
                VideoCategory::where('parent_id', $req->id)->update(['status' => 3]);
                VideoMaster::where('category_id', $req->id)->orWhereIn('category_id', $getchiledIds)->update(['status' => 3]);
                $req->session()->flash('success', translate('words.success_message'));
            }
            return redirect('/video/category/list');
        }
    }
    /////Video Management
    public function list(Request $req)
    {
        $breadcrumbs = [
            '/video/list' => 'words.video',
            '#' => 'words.list',
        ];
        $data = VideoMaster::where('status', 1)->get();

        return View::make('pages.settings.video.video_list', compact('breadcrumbs', 'data'));
    }

    public function form(Request $req)
    {
        $breadcrumbs = [
            '/video/list' => 'words.video',
            '#' => 'words.add',
        ];
        $language = Config::get('settings.lang');
        $selectedAuditType = $req->id;
        $ref_id = $selectedAuditType;
        $vid_cat = 'vid_cat_' . $language[session('lang')];
        $vidCategory = VideoCategory::where('parent_id', 0)->pluck($vid_cat, 'id')->prepend(translate('form.select'), '');
        return View::make('pages.settings.video.video_form', compact('breadcrumbs', 'selectedAuditType', 'vidCategory'));
    }

    public function edit(Request $req)
    {
        if (!empty($req->id)) {
            $details = VideoMaster::find($req->id);
            $breadcrumbs = [
                '/video/list' => 'words.video',
                '#' => 'words.edit',
            ];
            $findParent = VideoCategory::where('id', $details->category_id)->pluck('parent_id');
            if ($findParent[0] != 0) {
                $selectedParent =  $findParent[0];
            } else {
                $selectedParent =  $details->category_id;
            }
            $language = Config::get('settings.lang');
            $vid_cat = 'vid_cat_' . $language[session('lang')];
            $vidCategory = VideoCategory::where('parent_id', 0)->pluck($vid_cat, 'id')->prepend(translate('form.select'), '');
            return View::make('pages.settings.video.video_form', compact('breadcrumbs', 'details', 'vidCategory', 'selectedParent'));
        }
    }

    public function save(VideoRequest $req)
    {
        if (!empty($req->input())) {
            $model = VideoMaster::findOrNew($req->input('hid'));
            if ($req->input('video_sub_cat') != '') {
                $category = $req->input('video_sub_cat');
            } else {
                $category = $req->input('vid_category');
            }
            $model->category_id = $category;
            $model->title_en = $req->input('title_en');
            $model->title_nl = $req->input('title_nl');
            $model->title_fr = $req->input('title_fr');
            $model->description_en = $req->input('description_en');
            $model->description_fr = $req->input('description_fr');
            $model->description_nl = $req->input('description_nl');
            $model->media_type = $req->input('media_type');
            $model->status = 1;
            if ($req->input('media_type') == 'image') {
                if (!empty($req->file('image_nl'))) {
                    $extension = $req->file('image_nl')->getClientOriginalExtension();
                    if (in_array(strtolower($extension), Config::get('settings.img_extentions'))) {
                        $filename = 'NL-' . time() . '.' . $extension;
                        $path = storage_path('app/public/tutorial/'. $filename);
                        Image::make($req->file('image_nl')->getRealPath())->save($path);
                        $model->path_nl = $filename;
                    }
                }
                if (!empty($req->file('image_en'))) {
                    $extension = $req->file('image_en')->getClientOriginalExtension();
                    if (in_array(strtolower($extension), Config::get('settings.img_extentions'))) {
                        $filename = 'EN-' . time() . '.' . $extension;
                        $path = storage_path('app/public/tutorial/'. $filename);
                        Image::make($req->file('image_en')->getRealPath())->save($path);
                        $model->path_en = $filename;
                    }
                }
                if (!empty($req->file('image_fr'))) {
                    $extension = $req->file('image_fr')->getClientOriginalExtension();
                    if (in_array(strtolower($extension), Config::get('settings.img_extentions'))) {
                        $filename = 'FR-' . time() . '.' . $extension;
                        $path = storage_path('app/public/tutorial/'. $filename);
                        Image::make($req->file('image_fr')->getRealPath())->save($path);
                        $model->path_fr = $filename;
                    }
                }
            } elseif ($req->input('media_type') == 'video') {
                $model->path_nl = $req->input('video_link_nl');
                $model->path_en = $req->input('video_link_en');
                $model->path_fr = $req->input('video_link_fr');
            }
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
                return redirect('/video/list');
            }
        }
    }

    public function delete(Request $req)
    {
        if (!empty($req->id)) {
            $model = VideoMaster::find($req->id);
            $model->status = 3;
            if ($model->save()) {
                $req->session()->flash('success', translate('words.success_message'));
            }
            return redirect('/video/list');
        }
    }

    public function tutorial()
    {
        $breadcrumbs = [
            '/video/tutorial' => 'words.video_tutorial'
        ];
        $categories = VideoCategory::where('status', 1)->orderBy('sequence')->get();
        return View::make('pages.settings.video.tutorial', compact('breadcrumbs', 'categories'));
    }

    public function getSubCategory(Request $req)
    {
        $vidcat = 'vid_cat_'.session('lang');
        $getsubCat = VideoCategory::where('parent_id', $req->pid)->pluck($vidcat, 'id');
        $selectesSubCat = '';
        if (isset($req->selectedid)) {
            $selectesSubCat = $req->selectedid;
        }
        return View::make('pages.settings.video.video_sub_cat', compact('getsubCat', 'selectesSubCat'));
    }
}
