<?php

namespace App\Http\Controllers\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ActionDelegateMember;
use Auth;
use View;

class AuditResultsController extends Controller
{
    public function index()
    {
        $breadcrumbs = array(
            '/home' => 'words.home',
            );
        if (Auth::guard('customer')->check()) {
            $auditResults = ActionDelegateMember::leftJoin('report_issue_action', function ($join) {
                $join->on('report_issue_action.id', '=', 'action_delegate_members.action_id');
            })->leftJoin('audit_execution', function ($join) {
                $join->on('audit_execution.id', '=', 'report_issue_action.ref_id');
            })->leftJoin('audit_plan_master', function ($join) {
                $join->on('audit_plan_master.id', '=', 'audit_execution.audit_id');
            })
            ->leftJoin('process', function ($join) {
                $join->on('process.id', '=', 'audit_execution.process_id');
            })
            ->leftJoin('user_master', function ($join) {
                $join->on('user_master.id', '=', 'process.owner_id');
            })
            ->leftJoin('finding_type', function ($join) {
                $join->on('finding_type.id', '=', 'audit_execution.finding_type_id');
            })->select('audit_execution.id', 'audit_plan_master.category', 'audit_plan_master.plan_date', 'process.name as processname', 'finding_type.finding_type as finding', 'action_delegate_members.requested_action as description', 'user_master.name as processowner', 'action_delegate_members.status')
            ->where('audit_plan_master.company_id', Auth::guard('customer')->user()->company_id)
            ->where('process.status', 1)
            ->get();
        }
        return View::make('pages.home.auditresult', compact('breadcrumbs', 'auditResults'));
    }
}
