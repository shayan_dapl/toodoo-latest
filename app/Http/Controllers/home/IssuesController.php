<?php 

namespace App\Http\Controllers\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CompanySettings;
use Auth;
use View;

class IssuesController extends Controller
{
    public function index()
    {
        $breadcrumbs = [
            '/home' => 'words.home',
        ];
        if (Auth::guard('customer')->check()) {
            $internalPendingIssues = (new \App\Repositories\Home)->internalPendingIssues();
            $urgentIssues = (new \App\Repositories\Home)->urgentIssues();
            $veryUrgent = 0;
            $lessUrgent = 0;
            if (!empty($urgentIssues)) {
                foreach ($urgentIssues as $eachUrgency) {
                    $findPending = 'none';
                    if ($eachUrgency->member_status != 'none') {
                        $memberStatus = explode(',', $eachUrgency->member_status);
                        $findPending = array_search(0, $memberStatus);
                    }
                    if ($findPending !== false) {
                        if (dayDifference($eachUrgency->urgency) < 11) {
                            $veryUrgent++;
                        }
                        if (dayDifference($eachUrgency->urgency) > 10) {
                            $lessUrgent++;
                        }
                    }
                }
            }
            $pendingIssues = [];
            foreach ($internalPendingIssues as $k => $v) {
                $pendingIssues[$v->process][] = $v;
            }
            $urgencyCount = CompanySettings::companyCheck()->pluck('urgency_count')[0];
            $langArr = [
                'immidiate_action' => translate('form.immidiate_actions'),
                'root_cause' => translate('form.root_cause_analysis'),
                'corrective_action' => translate('form.corrective_action'),
                'effectiveness' => translate('form.effectiveness'),
                '1' => translate('table.on_going'),
            ];
            $extArr = [
                '1' => translate('table.on_going'),
            ];
        }
        return View::make('pages.home.issues', compact('breadcrumbs', 'urgentIssues', 'veryUrgent', 'lessUrgent', 'pendingIssues', 'urgencyCount', 'langArr', 'extArr'));
    }
}
