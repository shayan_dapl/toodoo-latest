<?php

namespace App\Http\Controllers\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CompanySettings;
use Auth;
use View;

class MeetingController extends Controller
{
    public function index()
    {
        $breadcrumbs = array(
            '/home' => 'words.home',
        );
        if (Auth::guard('customer')->check()) {
            $getauditorAndAuditees = (new \App\Repositories\Home)->pendingAuditsAditorAuditee();
        }
        return View::make('pages.home.meeting', compact('breadcrumbs', 'getauditorAndAuditees'));
    }
}
