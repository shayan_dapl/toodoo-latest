<?php 

namespace App\Http\Controllers\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use View;
use Config;

class PlanningController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new \App\Repositories\Home;
    }
    public function index(Request $req)
    {
        $breadcrumbs = array(
            '/home' => 'words.home',
        );
        if (Auth::guard('customer')->check()) {
            //Audit planning tab
            $auditPlans = $this->repo->auditPlans();
            $plannedAuditCount = 0;
            $preparedAuditCount = 0;
            if (!empty($auditPlans)) {
                foreach ($auditPlans as $eachAuditPlan) {
                    if ($eachAuditPlan->plan_time == null) {
                        $plannedAuditCount++;
                    }
                    if ($eachAuditPlan->plan_date != null and $eachAuditPlan->plan_time != null and $eachAuditPlan->executions->count() == 0) {
                        $preparedAuditCount++;
                    }
                }
            }
            $notReleasedExecutedAudits = [];
            foreach ($auditPlans as $each) {
                if ($each->executions->count() > 0) {
                    $notReleasedExecutedAudits[] = $each;
                }
            }
            $releasedExecutedAudits = $this->repo->releasedExecutedAudits();
            $monthsForCalender = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            if ($req->year) {
                $curYearPlanningTab = $req->year;
            } else {
                $curYearPlanningTab = date('Y');
            }
            /////////////////////FOR AUDITPLANNING TAB CALENDER//////////////////
            $prvYear = $curYearPlanningTab - 1;
            $nxtYear = $curYearPlanningTab + 1;
            $first = date('Y-m-d', strtotime($curYearPlanningTab."-01-01"));
            $last  = date('Y-m-d', strtotime($curYearPlanningTab."-12-31"));
            $curYearRange = [$first, $last];
            
            //Internal Audit
            $plannedAudits = $this->repo->plannedAuditsforMeeting($curYearRange, 'internal')->toArray();
            $uniqueProcess = [];
            foreach ($plannedAudits as $eachprocess => $eachData) {
                $uniqueProcess[$eachData['name']][] = $eachData;
            }
            $plannedAudit = plannedAudit($uniqueProcess, $curYearPlanningTab);

            ///External Audit
            $plannedExternalAudits = (new \App\Repositories\Home)->plannedAuditsforMeeting($curYearRange, 'external')->toArray();
            $uniqueProcess = [];
            foreach ($plannedExternalAudits as $eachprocess => $eachData) {
                $uniqueProcess[$eachData['name']][] = $eachData;
            }
            $plannedExternalAudit = plannedAudit($uniqueProcess, $curYearPlanningTab);
            /////Audit planning tab end
            return View::make('pages.home.planning', compact('breadcrumbs', 'auditPlans', 'plannedAuditCount', 'preparedAuditCount', 'notReleasedExecutedAudits', 'releasedExecutedAudits', 'prvYear', 'nxtYear', 'langArr', 'extArr', 'curYearPlanningTab', 'monthsForCalender', 'plannedAudit', 'plannedExternalAudit'));
        }
    }
}
