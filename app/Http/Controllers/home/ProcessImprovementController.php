<?php

namespace App\Http\Controllers\home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use View;

class ProcessImprovementController extends Controller
{
    public function index()
    {
        $breadcrumbs = array(
            '/home' => 'words.home',
            );
        if (Auth::guard('customer')->check()) {
            $pendingIssues = [];
            $internalPendingIssues = (new \App\Repositories\Home)->internalPendingIssues();
            foreach ($internalPendingIssues as $k => $v) {
                $pendingIssues[$v->process][] = $v;
            }
        }
        return View::make('pages.home.processImprovement', compact('breadcrumbs', 'pendingIssues'));
    }
}
