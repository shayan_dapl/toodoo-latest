<?php

namespace App\Http\Controllers\home\cockpit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use View;
use Config;

class AjaxController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new \App\Repositories\Home;
    }

    public function homeChartFilter(Request $req)
    {
        $finalData = ['issueMonths' => [], 'openIssuesChartData' => [], 'closedIssuesChartData' => []];
        switch ($req->for) {
            case 'open-closed-issues':
                $issueBarChart = ['months' => [], 'openIssues' => [], 'closedIssues' => []];
                if (!empty($req->value)) {
                    $segment = explode("|", $req->value);
                    $months = [$segment[0], date('Y-m-d')];
                    $open = $this->repo->openAndClosedIssues($segment[1], 'open', $months);
                    $closed = $this->repo->openAndClosedIssues($segment[1], 'closed', $months);
                    $issueBarChart = ['months' => array_keys($open['months']), 'openIssues' => array_values($open['months']), 'closedIssues' => array_values($closed['months'])];
                }
                return response()->json($issueBarChart);
            case 'unplanned-audit':
                $monthArr = [];
                $months = [$req->value, date('Y-m-d')];
                $gap = monthGap($months);
                $unplannedAuditsMonthWise = $plannedAuditsMonthWise = $ongoingAuditsMonthWise = $finishedAuditsMonthWise = [];
                for ($i = $gap; $i >= 0; $i--) {
                    $monthArr[] = date('F', strtotime("-$i month"));
                    $month = date('m', strtotime("-$i month"));
                    $year = date('Y', strtotime("-$i month"));
                    $unplannedAuditsMonthWise[] = $this->repo->unplannedAuditsMonthWise($month, $year);
                    $plannedAuditsMonthWise[] = $this->repo->plannedAuditsMonthWise($month, $year, 1)->count();
                    $ongoingAuditsMonthWise[] = $this->repo->plannedAuditsMonthWise($month, $year, 3)->count();
                    $finishedAuditsMonthWise[] = $this->repo->plannedAuditsMonthWise($month, $year, 2)->count();
                }
                $finalData = ['monthArr' => $monthArr, 'unplannedAuditsMonthWise' => $unplannedAuditsMonthWise, 'plannedAuditsMonthWise' => $plannedAuditsMonthWise, 'ongoingAuditsMonthWise' => $ongoingAuditsMonthWise, 'finishedAuditsMonthWise' => $finishedAuditsMonthWise];
                return $finalData;
            case 'insight-process':
                $months = [$req->value, date('Y-m-d')];
                $insightProcessData = $this->repo->insigntsPerProcess($months);
                $finalData['processData'] = $insightProcessData['process'];
                $insightData = [];
                foreach ($insightProcessData['insight'] as $k => $eachprocess) {
                    $insightData[] = array('label' => $k, 'backgroundColor' => generateRgb(), 'data' => $eachprocess);
                }
                $finalData['insightData'] = $insightData;
                return $finalData;
            case 'open-closed-per-insight':
                $months = [$req->value, date('Y-m-d')];
                $openClosedPerInsight = $this->repo->openAndClosedIssuesPerInsight($months);
                $finalData['findingNames'] = $openClosedPerInsight['findingType'];
                $finalData['openFindings'] = $openClosedPerInsight['processInsight']['open'];
                $finalData['closedFindings'] = $openClosedPerInsight['processInsight']['closed'];
                return $finalData;
            case 'internal':
            case 'external':
                $months = [date('Y-m-d', strtotime('-'.$req->value.' month')), date('Y-m-d')];
                $plannedAuditsforMeeting = $this->repo->plannedAuditsforMeeting($months, $req->for)->toArray();
                $uniqueMeetingProcess = [];
                foreach ($plannedAuditsforMeeting as $eachprocess => $eachData) {
                    $uniqueMeetingProcess[$eachData['name']][] = $eachData;
                }
                $meetingMonthArr = [];
                $meetingYearArr = [];
                for ($i = 0; $i < $req->value; $i++) {
                    $meetingMonthArr[date('n', strtotime("-$i month"))] = date('M', strtotime("-$i month")) . '/' . date('Y', strtotime("-$i month"));
                    $meetingYearArr[date('n', strtotime("-$i month"))] = date('Y', strtotime("-$i month"));
                }
                $meetingMonthArr = array_reverse($meetingMonthArr, true);
                $meetingYearArr = array_reverse($meetingYearArr, true);
                $plannedAuditMeeting = [];
                $m = 0;
                foreach ($uniqueMeetingProcess as $kt => $vt) {
                    $plannedAuditMeeting[$m]['process'] = $kt;
                    foreach ($meetingMonthArr as $i => $j) {
                        $auditsByProcess = [];
                        foreach ($uniqueMeetingProcess[$kt] as $eachpros => $eachVal) {
                            $auditStatus = '';
                            $curYear = $meetingYearArr[$i];
                            if (!empty($uniqueMeetingProcess[$kt][$eachpros])) {
                                if (($uniqueMeetingProcess[$kt][$eachpros]['plan_year'] == $curYear) && ($uniqueMeetingProcess[$kt][$eachpros]['plan_month'] == $i)) {
                                    $auditsByProcess = $uniqueMeetingProcess[$kt][$eachpros];
                                }
                            }
                            if (!empty($auditsByProcess)) {
                                if ($auditsByProcess['audit_status'] == 2 || $auditsByProcess['audit_status'] == 3) {
                                    $auditStatus = 'executed';
                                } else {
                                    $auditStatus = 'error';
                                }
                            }
                            if (!empty($auditsByProcess)) {
                                if ($auditsByProcess['audit_status'] == 2) {
                                    $plannedAuditMeeting[$m]['months'][$i]['tdCls'] = 'back4a';
                                    $plannedAuditMeeting[$m]['months'][$i]['aCls'] = 'text-default noCurserCls';
                                } elseif ($auditStatus == 'executed' && $auditsByProcess['audit_status'] == 1) {
                                    $plannedAuditMeeting[$m]['months'][$i]['tdCls'] = 'backff';
                                    $plannedAuditMeeting[$m]['months'][$i]['aCls'] = 'text-default noCurserCls';
                                } else {
                                    $plannedAuditMeeting[$m]['months'][$i]['tdCls'] = '';
                                    $plannedAuditMeeting[$m]['months'][$i]['aCls'] = '';
                                }
                                if ($auditsByProcess['audit_date'] != '' && $auditsByProcess['audit_status'] == 2) {
                                    $plannedAuditMeeting[$m]['months'][$i]['userType'] = '';
                                    $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                                    $plannedAuditMeeting[$m]['months'][$i]['title'] = 'Original ' . $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                                    $plannedAuditMeeting[$m]['months'][$i]['link'] = '#';
                                } elseif ($auditsByProcess['audit_date'] != '' && $auditStatus == 'executed' && $auditsByProcess['audit_status'] != 2) {
                                    if ($auditsByProcess['auditor_type'] == 1) {
                                        $plannedAuditMeeting[$m]['months'][$i]['userType'] = 'Lead Auditor';
                                    } elseif ($auditsByProcess['auditor_type'] == 2) {
                                        $plannedAuditMeeting[$m]['months'][$i]['userType'] = 'Auditor';
                                    } else {
                                        $plannedAuditMeeting[$m]['months'][$i]['userType'] = 'Auditee';
                                    }
                                    $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                                    $plannedAuditMeeting[$m]['months'][$i]['title'] = 'Original ' . $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                                    $plannedAuditMeeting[$m]['months'][$i]['link'] = '#';
                                } elseif ($auditsByProcess['audit_date'] != '' && $auditStatus != 'executed') {
                                    if ($auditsByProcess['auditor_type'] == 1) {
                                        $plannedAuditMeeting[$m]['months'][$i]['userType'] = 'Lead Auditor';
                                        $plannedAuditMeeting[$m]['months'][$i]['link'] = url('/audit/audit-prepare/' . $auditsByProcess['id']);
                                    } elseif ($auditsByProcess['auditor_type'] == 2) {
                                        $plannedAuditMeeting[$m]['months'][$i]['userType'] = 'Auditor';
                                        $plannedAuditMeeting[$m]['months'][$i]['link'] = url('/audit/audit-prepare/' . $auditsByProcess['id']);
                                    } else {
                                        $plannedAuditMeeting[$m]['months'][$i]['userType'] = 'Auditee';
                                        $plannedAuditMeeting[$m]['months'][$i]['aCls'] = 'noCurserCls';
                                        $plannedAuditMeeting[$m]['months'][$i]['link'] = '#';
                                    }
                                    $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = date(Config::get('settings.dashed_date'), strtotime($auditsByProcess['audit_date']));
                                    $plannedAuditMeeting[$m]['months'][$i]['title'] = 'Original ' . $auditsByProcess['original_month'] . '/' . $auditsByProcess['original_year'];
                                } else {
                                    if (Auth::guard('customer')->user()->id == $auditsByProcess['choose_lead_auditor']) {
                                        $plannedAuditMeeting[$m]['months'][$i]['userType'] = 'Lead Auditor';
                                        $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = 'X';
                                        $plannedAuditMeeting[$m]['months'][$i]['title'] = 'Edit';
                                        $plannedAuditMeeting[$m]['months'][$i]['link'] = url('/audit/audit-prepare/' . $auditsByProcess['id']);
                                    } else {
                                        $plannedAuditMeeting[$m]['months'][$i]['userType'] = '';
                                        $plannedAuditMeeting[$m]['months'][$i]['title'] = '';
                                        $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = '';
                                        $plannedAuditMeeting[$m]['months'][$i]['tdCls'] = '';
                                        $plannedAuditMeeting[$m]['months'][$i]['aCls'] = 'noCurserCls';
                                        $plannedAuditMeeting[$m]['months'][$i]['link'] = "#";
                                    }
                                }
                            } else {
                                $plannedAuditMeeting[$m]['months'][$i]['userType'] = '';
                                $plannedAuditMeeting[$m]['months'][$i]['tdCls'] = '';
                                $plannedAuditMeeting[$m]['months'][$i]['aCls'] = 'noCurserCls';
                                $plannedAuditMeeting[$m]['months'][$i]['auditDate'] = ' ';
                                $plannedAuditMeeting[$m]['months'][$i]['title'] = '';
                                $plannedAuditMeeting[$m]['months'][$i]['link'] = "#";
                            }
                        }
                    }
                    $m++;
                }
                $monthsForMeetingCalender = $meetingMonthArr;
                return View::make('pages.home.meetingCalender', compact('plannedAuditMeeting', 'monthsForMeetingCalender'));
            case 'open-closed-issues-pie':
                $result = [];
                $months = [$req->value, date('Y-m-d')];
                $result['open'] = $this->repo->issuesPieChart('open', $months);
                $result['closed'] = $this->repo->issuesPieChart('closed', $months);
                return $result;
            }
    }
}
