<?php

namespace App\Http\Controllers\home\cockpit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class InsightProcessController extends Controller
{
    public function index()
    {
        $breadcrumbs = array(
            '/home' => 'words.home',
        );
        $urgentIssues = (new \App\Repositories\Home)->urgentIssues();

        $veryUrgent = 0;
        $lessUrgent = 0;
        if (!empty($urgentIssues)) {
            foreach ($urgentIssues as $eachUrgency) {
                $findPending = 'none';
                if ($eachUrgency->member_status != 'none') {
                    $memberStatus = explode(',', $eachUrgency->member_status);
                    $findPending = array_search(0, $memberStatus);
                }
                if ($findPending !== false) {
                    if (dayDifference($eachUrgency->urgency) < 11) {
                        $veryUrgent++;
                    }
                    if (dayDifference($eachUrgency->urgency) > 10) {
                        $lessUrgent++;
                    }
                }
            }
        }
        $upcomingSevendaysDuration = [date('Y-m-d'), date('Y-m-d', strtotime('+7 days'))];
        $unplannedAuditsNo = (new \App\Repositories\Home)->totalUnplannedAudits();
        $internalPendingIssues = (new \App\Repositories\Home)->internalPendingIssues();
        $pendingAuditee = (new \App\Repositories\Home)->pendingAuditsAditee(2, $upcomingSevendaysDuration);
        $pendingAuditor = (new \App\Repositories\Home)->pendingAuditsAditor(2, $upcomingSevendaysDuration);
        $meetingPending = $pendingAuditee->count() + $pendingAuditor->count();
        
        $months = [date('Y-m-d', strtotime('-6 month')), date('Y-m-d')];
        
        $insightProcessData = (new \App\Repositories\Home)->insigntsPerProcess($months);
        $processData = json_encode($insightProcessData['process']);
        $insightData = [];
        foreach ($insightProcessData['insight'] as $k => $eachprocess) {
            $insightData[] = array('label' => $k, 'backgroundColor' => generateRgb(), 'data' => $eachprocess);
        }
        $insightData = json_encode($insightData);
        $openClosedPerInsight = (new \App\Repositories\Home)->openAndClosedIssuesPerInsight($months);
        $findingNames = json_encode($openClosedPerInsight['findingType']);
        $openFindings = json_encode($openClosedPerInsight['processInsight']['open']);
        $closedFindings = json_encode($openClosedPerInsight['processInsight']['closed']);
        return View::make('pages.home.cockpit.insightsToProcess', compact('breadcrumbs', 'veryUrgent', 'unplannedAuditsNo', 'internalPendingIssues', 'meetingPending', 'issueMonths', 'processData', 'insightData', 'findingNames', 'openFindings', 'closedFindings'));
    }
}
