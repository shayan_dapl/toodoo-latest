<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Position;

class AssistantPosition
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $input = $request->all();
        if (isset($input['assistant']) && empty($input['hid'])) {
            $assistantExists = Position::where('parent_id', $input['parent_id'])->where('is_assistant', 1)->count();
            if ($assistantExists > 0) {
                if (isset($input['started'])) {
                    return response()->json(translate('alert.cant_add_multiple_assistant'), 201);
                } else {
                    $request->session()->flash('error', translate('alert.cant_add_multiple_assistant'));
                    return redirect('/position/list');
                }
            } else {
                $parentName = Position::find($input['parent_id'])->name;
                $input['name'] = translate('form.assistant') .'-'. $parentName;
                $request->replace($input);
                return $next($request);
            }
        } else {
            return $next($request);
        }
    }
}
