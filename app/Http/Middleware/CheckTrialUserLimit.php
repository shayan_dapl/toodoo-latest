<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Config;
use App\Models\Users;

class CheckTrialUserLimit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $checkLimit = 1;
        $endLimit = (Auth::guard('customer')->user()->company->is_trial == 1) ? Config::get('settings.trial_user_limit') : Auth::guard('customer')->user()->company->user_allowed;
        $flashMsg = (Auth::guard('customer')->user()->company->is_trial == 1) ? translate('error.trial_user_limit') : (Auth::guard('customer')->user()->company->user_allowed .' '. translate('error.package_user_limit'));
        if (!empty($request->input('hid'))) {
            $user = Users::companyCheck()->where('id', $request->input('hid'))->first();
            if (!empty($request->input('email'))) {
                if ($user->email != null) {
                    $checkLimit = 0;
                }
            } else {
                $checkLimit = 0;
            }
        }
        if ($checkLimit == 1) {
            if (!empty($request->input('email')) && Users::companyCheck()->inServiceWithEmail()->where('email', '<>', Config::get('settings.associate_user'))->count() >= $endLimit) {
                $request->session()->flash('error', $flashMsg);
                return redirect('/user/list');
            } else {
                return $next($request);
            }
        } else {
            return $next($request);
        }
    }
}
