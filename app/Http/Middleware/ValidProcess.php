<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\Process;
use Session;

class ValidProcess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Process::find(Session::get('turtle_process_id'))->status == 1) {
            return $next($request);
        } else {
            return redirect('/process/list');
        }
    }
}
