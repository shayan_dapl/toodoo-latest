<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuditPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->input('audit_category') == "external") {
            return [
                'audit_title' => 'required',
                'choose_process' => 'required|not_in:""',
                'iso_ref' => 'required|not_in:""',
                'choose_audit_month' => 'required',
                'choose_lead_auditor' => 'required|not_in:""',
                'audit_company' => 'required'
            ];
        } else {
            return [
                'audit_title' => 'required',
                'choose_process' => 'required|not_in:""',
                'iso_ref' => 'required|not_in:""',
                'choose_audit_month' => 'required',
                'choose_lead_auditor' => 'required|not_in:""'
            ];
        }
    }
}
