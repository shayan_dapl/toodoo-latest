<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = [
            'street' => 'required',
            'nr' => 'required',
            'zip' => 'required',
            'city' => 'required',
            'comp_country' => 'required'
        ];

        if ($this->input('is_apply_vat') == null) {
            $required['tva_no'] = 'required';
        }
        return $required;
    }

    public function messages()
    {
        return [
            'street.required' => translate('error.required_field'),
            'nr.required' => translate('error.required_field'),
            'zip.required' => translate('error.required_field'),
            'city.required' => translate('error.required_field'),
            'comp_country.required' => translate('error.required_field'),
            'tva_no.required' => translate('error.required_field')
        ];
    }
}
