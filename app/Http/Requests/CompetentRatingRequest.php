<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompetentRatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reqired = [
            'process' => 'required',
            'step' => 'required',
            'org' => 'required',
            'rating' => 'required'
        ];
        if ($this->input('isAction') == 1) {
            $reqired['action'] = 'required';
            $reqired['delegateTo'] = 'required';
            $reqired['deadline'] = 'required';
        }
        return $reqired;
    }
}
