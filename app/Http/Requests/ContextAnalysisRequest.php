<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContextAnalysisRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'issue' => 'required',
            'subject' => 'required',
            'issue_type' => 'required',
            'process_id' => 'required',
            'owner' => 'required',
            'monitoring_info' => 'required',
            'possible_influence' => 'required',
            'impact' => 'required',
            'risk' => 'required',
            'description' => 'required'
        ];
    }
}
