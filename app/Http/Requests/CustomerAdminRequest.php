<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Lang;

class CustomerAdminRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!empty($this->input('masterid'))) {
            $arr = [
                'email' => 'required|email',
                'fname' => 'required',
                'lname' => 'required',
                'user_password' => !empty($this->input('user_password')) ? 'min:8' : '',
                'password_confirmation' => !empty($this->input('password_confirmation')) ? 'same:user_password|min:8' : '',
                'language' => 'required|not_in:""',
                'name' => 'required',
            ];
            if (!empty($this->input('subscription_id'))) {
                $arr1 = [
                    'tva_no' => 'required',
                    'address_line1' => 'required',
                    'comp_zip' => 'required',
                    'comp_city' => 'required',
                    'comp_country' => 'required|not_in:""',
                ];
                $arr = array_merge($arr1);
            }
            return $arr;
        } else {
            $arr = [
                'email' => 'required|email',
                'fname' => 'required',
                'lname' => 'required',
                'user_password' => 'min:8',
                'password_confirmation' => 'same:user_password|min:8',
                'language' => 'required|not_in:""',
                'name' => 'required',
            ];

            if (!empty($this->input('subscription_id'))) {
                $arr1 = [
                    'tva_no' => 'required',
                    'address_line1' => 'required',
                    'comp_zip' => 'required',
                    'comp_city' => 'required',
                    'comp_country' => 'required|not_in:""',
                ];
                $arr = array_merge($arr1);
            }
            return $arr;
        }
    }
}
