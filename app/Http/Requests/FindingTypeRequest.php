<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FindingTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!empty($this->input('id'))) {
            return [
                'finding_type' => 'required|samefindingtype:' . $this->input('id')
            ];
        } else {
            return [
                'finding_type' => 'required|samefindingtype'
            ];
        }
    }

    public function messages()
    {
        return [
            'finding_type.samefindingtype' => translate('alert.already_exist')
        ];
    }
}
