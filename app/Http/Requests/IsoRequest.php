<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IsoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'iso_no_nl' => 'required',
            'iso_no_en' => 'required',
            'iso_no_fr' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'iso_no_nl.required' => translate('error.required_field'),
            'iso_no_en.required' => translate('error.required_field'),
            'iso_no_fr.required' => translate('error.required_field')
        ];
    }
}
