<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Factory as ValidationFactory;
use App\Models\PackageRating;

class PackageRatingRequest extends FormRequest
{
    public function __construct(ValidationFactory $validationFactory)
    {
        $validationFactory->extend(
            'duplicate',
            function ($attribute, $value, $parameters) {
                $found = PackageRating::companyStatusCheck()->where('rating', $value);
                if (!empty($parameters)) {
                    $found = $found->where('id', '<>', $parameters[0]);
                }
                $found = ($found->count() == 0) ? 'unique:package_rating' : '';
                return $found;
            }
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rating' => 'required|duplicate:' . $this->input('id')
        ];
    }

    public function messages()
    {
        return [
            'rating.duplicate' => translate('alert.already_exist')
        ];
    }
}
