<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\PackageThemeEnv;

use Illuminate\Validation\Factory as ValidationFactory;

class PackageThemeEnvRequest extends FormRequest
{
    public function __construct(ValidationFactory $validationFactory)
    {
        $validationFactory->extend(
            'duplicate',
            function ($attribute, $value, $parameters) {
                $found = PackageThemeEnv::companyStatusCheck()->where('name', $value);
                if (!empty($parameters)) {
                    $found = $found->where('id', '<>', $parameters[0]);
                }
                $found = ($found->count() == 0) ? 'unique:package_theme_env' : '';
                return $found;
            }
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|duplicate:' . $this->input('id')
        ];
    }

    public function messages()
    {
        return [
            'name.duplicate' => translate('alert.already_exist')
        ];
    }
}
