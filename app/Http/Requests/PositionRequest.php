<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\File;
use App\Models\Position;
use Auth;
use Lang;

class PositionRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $counter = Position::where('company_id', Auth::guard('customer')->user()->company_id)
                            ->where('department_id', $this->input('department_id'))
                            ->count();

        //For position creation
        if (empty($this->input('hid'))) {
            //For first position entry
            if ($counter < 1) {
                //When adding a new user (normal) into first position with/without email
                if (!empty($this->input('manage_normal_user'))) {
                    return [
                        'name' => 'required',
                        'email' => empty($this->input('no_email')) ? 'required|email|sameemail' : '',
                        'fname' => 'required',
                        'last_name' => 'required',
                        'language' => 'required'
                    ];
                } else {
                    return [
                        'name' => 'required'
                    ];
                }
            } else {
                //When adding a new user (normal) with/without email
                if (!empty($this->input('manage_normal_user'))) {
                    return [
                        'name' => empty($this->input('assistant')) ? 'required|sameposition:' . $this->input('hid') . ',' . $this->input('branch') : '',
                        'email' => empty($this->input('no_email')) ? 'required|email|sameemail' : '',
                        'fname' => 'required',
                        'last_name' => 'required',
                        'language' => 'required'
                    ];
                } elseif (!empty($this->input('manage_special_user'))) { //When adding a new user (normal) into first position with email
                    if ($this->input('special_user_check') == 'special_single') {
                        return [
                            'name' => 'required|sameposition:' . $this->input('hid') . ',' . $this->input('branch'),
                            'special_first_name' => 'required',
                            'special_last_name' => 'required'
                        ];
                    }

                    if ($this->input('special_user_check') == 'special_multiple') {
                        $desiredExtensions = ['xlsx', 'xls', 'csv'];
                        $extension = !empty($this->file('doc_file')) ? File::extension($this->file('doc_file')->getClientOriginalName()) : '';

                        return [
                            'name' => 'required|sameposition:' . $this->input('hid') . ',' . $this->input('branch'),
                            'doc_file' => in_array($extension, $desiredExtensions) ? '' : 'mimes:csv or xls or xlsx'
                        ];
                    }
                } else {
                    return [
                        'parent_id' => 'required',
                        'name' => !empty($this->input('assistant')) ? '' : 'required|sameposition:' . $this->input('hid') . ',' . $this->input('branch')
                    ];
                }
            }
        }

        //For position modification
        if (!empty($this->input('hid'))) {
            //Modifying top position
            if ($counter == 1) {
                return [
                    'name' => 'required'
                ];
            } else {
                $conflict = findParent($this->input('parent_id'), $this->input('hid'));
                if ($conflict == 'Yes') {
                    return ['parent_id' => 'required|conflicted'];
                } else {
                    //When adding a new user (normal) with/without email
                    if (!empty($this->input('manage_normal_user'))) {
                        return [
                            'name' => 'required|sameposition:' . $this->input('hid') . ',' . $this->input('branch'),
                            'email' => empty($this->input('no_email')) ? 'required|email|sameemail' : '',
                            'fname' => 'required',
                            'last_name' => 'required',
                            'user_password' => empty($this->input('no_email')) ? 'required|min:8' : '',
                            'confirm_password' => empty($this->input('no_email')) ? 'required|same:user_password|min:8' : '',
                            'language' => 'required'
                        ];
                    } elseif (!empty($this->input('manage_special_user'))) { //When adding a new user (normal) into first position with email
                        if ($this->input('special_user_check') == 'special_single') {
                            return [
                                'name' => 'required|sameposition:' . $this->input('hid') . ',' . $this->input('branch'),
                                'special_first_name' => 'required',
                                'special_last_name' => 'required'
                            ];
                        }

                        if ($this->input('special_user_check') == 'special_multiple') {
                            $desiredExtensions = ['xlsx', 'xls', 'csv'];
                            $extension = !empty($this->file('doc_file')) ? File::extension($this->file('doc_file')->getClientOriginalName()) : '';

                            return [
                                'name' => 'required|sameposition:' . $this->input('hid') . ',' . $this->input('branch'),
                                'doc_file' => in_array($extension, $desiredExtensions) ? '' : 'mimes:csv or xls or xlsx'
                            ];
                        }
                    } else {
                        return [
                            'name' => ($this->input('assistant') != null) ? '' : 'required|sameposition:' . $this->input('hid') . ',' . $this->input('branch')
                        ];
                    }
                }
            }
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'parent_id.conflicted' => translate('alert.parent_is_child_of_this'),
            'name.sameposition' => translate('alert.name_already_taken'),
            'email.sameemail' => translate('form.email_exists')
        ];
    }
}
