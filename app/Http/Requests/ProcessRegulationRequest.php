<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProcessRegulationRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->input('hid') == "" && strtolower($this->input('upload_doc')) == "yes") {
            return [
                'process_id' => 'required|not_in:""',
                'doc_ref' => 'required',
                'language' => 'required|not_in:""',
                'version' => 'required'
            ];
        } else {
            return [
                'process_id' => 'required|not_in:""',
                'doc_ref' => 'required',
                'language' => 'required|not_in:""'
            ];
        }
    }
}
