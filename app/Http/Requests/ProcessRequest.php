<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Lang;

class ProcessRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!empty($this->input('hid'))) {
            return [
                'name' => 'required|sameprocess:' . $this->input('hid'),
                'owner_id' => 'required|not_in:""'
            ];
        } else {
            return [
                'name' => 'required|sameprocess',
                'owner_id' => 'required|not_in:""'
            ];
        }
    }

    public function messages()
    {
        return [
            'name.sameprocess' => translate('alert.same_process')
        ];
    }
}
