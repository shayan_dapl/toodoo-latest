<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\File;

class ProcessResourceRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!empty($this->input('multiple'))) {
            $desiredExtensions = ['xlsx', 'xls', 'csv'];
            $extension = !empty($this->file('doc_file')) ? File::extension($this->file('doc_file')->getClientOriginalName()) : '';
            return [
                'process_id' => 'required|not_in:""',
                'doc_file' => in_array($extension, $desiredExtensions) ? '' : 'required|mimes:xls or xlsx'
            ];
        } else {
            return [
                'process_id' => 'required|not_in:""',
                'brand' => 'required',
                'type' => 'required'
            ];
        }
    }
}
