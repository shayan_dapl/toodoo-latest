<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProcessRiskRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'process_id' => 'required|not_in:""',
            'risk' => 'required|samerisk:' . $this->input('process_id') . ',' . $this->input('hid'),
            'level' => 'required|not_in:""'
        ];
    }

    public function messages()
    {
        return [
            'risk.samerisk' => translate('form.risk_exist')
        ];
    }
}
