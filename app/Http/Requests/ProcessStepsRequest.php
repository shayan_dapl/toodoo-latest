<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProcessStepsRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'process_id' => 'required|not_in:""',
            'step_no' => 'required|samestep:' . $this->input('process_id') . ',' . $this->input('hid'),
            'step_name' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'step_no.samestep' => translate('alert.already_exist')
        ];
    }
}
