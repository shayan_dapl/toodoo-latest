<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResourcesTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!empty($this->input('id'))) {
            return [
                'name' => 'required|sameresourcetypes:' . $this->input('id')
            ];
        } else {
            return [
                'name' => 'required|sameresourcetypes'
            ];
        }
    }

    public function messages()
    {
        return [
            'name.sameresourcetypes' => translate('alert.already_exist')
        ];
    }
}
