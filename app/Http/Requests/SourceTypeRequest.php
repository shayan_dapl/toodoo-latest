<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Lang;

class SourceTypeRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!empty($this->input('hid'))) {
            return [
                    'audit_source_type' => 'required|samesourcetype:' . $this->input('compid') . ',' . $this->input('hid')
                ];
        } else {
            return [
                    'audit_source_type' => 'required|samesourcetype'
                ];
        }
    }

    public function messages()
    {
        return [
            'audit_source_type.samesourcetype' => translate('form.source_type_exists')
        ];
    }
}
