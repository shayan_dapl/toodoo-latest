<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!empty($this->input('id'))) {
            return [
                'rating' => 'required|samesupplierratings:' . $this->input('id'),
                'description' => 'required'
            ];
        } else {
            return [
                'rating' => 'required|samesupplierratings',
                'description' => 'required'
            ];
        }
    }

    public function messages()
    {
        return [
            'rating.samesupplierratings' => translate('alert.already_exist')
        ];
    }
}
