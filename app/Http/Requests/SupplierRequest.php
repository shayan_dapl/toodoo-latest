<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!empty($this->input('id'))) {
            return [
                'name' => 'required|samesupplier:' . $this->input('id'),
                'service' => 'required'
            ];
        } else {
            return [
                'name' => 'required|samesupplier',
                'service' => 'required'
            ];
        }
    }

    public function messages()
    {
        return [
            'name.samesupplier' => translate('alert.already_exist')
        ];
    }
}
