<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\File;

class SupplierUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $desiredExtensions = ['xlsx', 'xls', 'csv'];
        $extension = !empty($this->file('doc_file')) ? File::extension($this->file('doc_file')->getClientOriginalName()) : '';
        return [
            'doc_file' => in_array($extension, $desiredExtensions) ? '' : 'required|mimes:xls or xlsx'
        ];
    }
}
