<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Lang;

class UserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!empty($this->input('hid'))) {
            if (($this->input('is_special_user') == 1) || ($this->input('no_email') != null)) {
                return [
                    'fname' => 'required',
                    'lname' => 'required'
                ];
            } else {
                return [
                    'email' => 'required|email|sameuser:' . $this->input('compid') . ',' . $this->input('hid'),
                    'user_password' => !empty($this->input('user_password')) ? 'min:8' : '',
                    'confirm_password' => !empty($this->input('user_password')) ? 'same:user_password|min:8' : '',
                    'fname' => 'required',
                    'lname' => 'required',
                    'language' => 'required|not_in:0'
                ];
            }
        } else {
            if (($this->input('is_special_user') == 1) || ($this->input('no_email') == 1)) {
                return [
                    'fname' => 'required',
                    'lname' => 'required'
                ];
            } else {
                return [
                    'email' => 'required|email|sameuser',
                    'fname' => 'required',
                    'lname' => 'required',
                    'user_password' => 'min:8',
                    'confirm_password' => 'same:user_password|min:8',
                    'language' => 'required|not_in:0'
                ];
            }
        }
    }

    public function messages()
    {
        return [
            'email.sameuser' => translate('form.email_exists')
        ];
    }
}
