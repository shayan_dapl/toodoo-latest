<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class ValidateCompany extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'fname' => 'required',
            'lname' => 'required',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
            'language' => 'required|not_in:0',
            'name' => 'required',
            'tva_no' => 'required',
            'address_line1' => 'required',
            'comp_zip' => 'required',
            'comp_city' => 'required|not_in:0',
            'comp_country' => 'required|not_in:0'
        ];
    }
}
