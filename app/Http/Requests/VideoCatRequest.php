<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VideoCatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vid_cat_nl' => 'required',
            'vid_cat_en' => 'required',
            'vid_cat_fr' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'vid_cat_nl.required' => translate('error.required_field'),
            'vid_cat_en.required' => translate('error.required_field'),
            'vid_cat_fr.required' => translate('error.required_field')
        ];
    }
}
