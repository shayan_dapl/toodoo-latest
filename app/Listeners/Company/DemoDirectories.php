<?php

namespace App\Listeners\Company;

use App\Events\Company\DemoData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\File;

class DemoDirectories
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemoData  $event
     * @return void
     */
    public function handle(DemoData $event)
    {
        $basePath = 'app/public/company/' . $event->id;
        $directories = [
            '/image',
            '/image/users',
            '/documents',
            '/documents/execution_docs',
            '/documents/delegation_docs',
            '/documents/external_issue_fixing_docs',
            '/documents/process_docs',
            '/documents/regulation_docs',
            '/documents/process_resources',
            '/documents/risk_opp_action_docs',
            '/documents/con_stake_action_docs',
            '/documents/competent_rating_action_docs',
            '/documents/supplier_rating_action_docs',
            '/documents/supplier_corrective_action_docs',
            '/documents/resource_action_docs',
            '/documents/addon_package_action_docs'
        ];
        File::makeDirectory(storage_path($basePath), 0777, true, true);
        foreach ($directories as $each) {
            File::makeDirectory(storage_path($basePath . $each), 0777, true, true);
        }
    }
}
