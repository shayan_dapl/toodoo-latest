<?php

namespace App\Listeners\Company;

use App\Events\Company\DemoData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\FindingTypes;
use App\Models\ActionMatrix;

class DemoFindingTypes
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemoData  $event
     * @return void
     */
    public function handle(DemoData $event)
    {
        $fixedTypes = ['Positive finding', 'Opportunity for improvement (OFI)', 'Minor Non Conformity (Minor)', 'Major Non Conformity (Major)'];
        $i = 0;
        foreach ($fixedTypes as $eachFixedType) {
            $i++;
            $model = new FindingTypes;
            $model->company_id = $event->id;
            $model->finding_type = $eachFixedType;
            $model->status = 1;
            $model->type = 1;
            $model->created_by = 1;
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                $matrix = new ActionMatrix;
                $matrix->finding_type = $model->id;
                switch ($i) {
                    case '2':
                        $matrix->corrective_measure = 1;
                        break;
                    case '3':
                        $matrix->root_cause_analysis = 1;
                        $matrix->corrective_measure = 1;
                        $matrix->effectiveness = 1;
                        break;
                    case '4':
                        $matrix->immidiate_actions = 1;
                        $matrix->root_cause_analysis = 1;
                        $matrix->corrective_measure = 1;
                        $matrix->effectiveness = 1;
                        break;
                }
                $matrix->save();
            }
        }
    }
}
