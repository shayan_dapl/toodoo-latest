<?php

namespace App\Listeners\Company;

use App\Events\Company\DemoData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\PackageRating;

class DemoPackageRating
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemoData  $event
     * @return void
     */
    public function handle(DemoData $event)
    {
        $list = [];
        for ($inc=1; $inc<=5; $inc++) {
            $list[] = ['company_id' => $event->id, 'rating' => $inc, 'type' => 1, 'created_at' => date('Y-m-d H:i:s')];
        }
        PackageRating::insert($list);
    }
}
