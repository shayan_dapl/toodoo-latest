<?php

namespace App\Listeners\Company;

use App\Events\Company\DemoData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\ProcessDocumentTypes;

class DemoProcessDocumentTypes
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemoData  $event
     * @return void
     */
    public function handle(DemoData $event)
    {
        $list = [];
        $data = ['procedure', 'instructie', 'handboek'];
        foreach ($data as $each) {
            $list[] = ['company_id' => $event->id, 'type_name' => $each, 'status' => 1, 'created_at' => date('Y-m-d H:i:s')];
        }
        ProcessDocumentTypes::insert($list);
    }
}
