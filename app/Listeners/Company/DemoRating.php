<?php

namespace App\Listeners\Company;

use App\Events\Company\DemoData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Rating;

class DemoRating
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemoData  $event
     * @return void
     */
    public function handle(DemoData $event)
    {
        $data = [];
        for ($i=1; $i<=3; $i++) {
            $data[] = ['company_id' => $event->id, 'name' => $i, 'status' => 1, 'created_at' => date('Y-m-d H:i:s')];
        }
        Rating::insert($data);
    }
}
