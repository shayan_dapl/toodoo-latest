<?php

namespace App\Listeners\Company;

use App\Events\Company\DemoData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\RiskOpportunityAction;

class DemoRiskOpportunityAction
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemoData  $event
     * @return void
     */
    public function handle(DemoData $event)
    {
        $data[] = ['company_id' => $event->id, 'actions' => 'H,M', 'created_at' => date('Y-m-d H:i:s')];
        RiskOpportunityAction::insert($data);
    }
}
