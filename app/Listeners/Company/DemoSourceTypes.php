<?php

namespace App\Listeners\Company;

use App\Events\Company\DemoData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\SourceTypes;

class DemoSourceTypes
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemoData  $event
     * @return void
     */
    public function handle(DemoData $event)
    {
        $list = [];
        $data = [
            ['name' => 'Customer complaint', 'deletable' => 0, 'source_status' => 1],
            ['name' => 'Supplier complaint', 'deletable' => 0, 'source_status' => 2],
            ['name' => 'Meeting', 'deletable' => 1, 'source_status' => 3]
        ];
        foreach ($data as $each) {
            $list[] = ['company_id' => $event->id, 'source_type' => $each['name'], 'status' => 1, 'type' => 1, 'deletable' => $each['deletable'], 'source_status' =>$each['source_status'], 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')];
        }
        SourceTypes::insert($list);
    }
}
