<?php

namespace App\Listeners\Company;

use App\Events\Company\DemoData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\GeneralSettings;
use App\Models\Company;
use App\Models\Branch;
use App\Models\Department;
use App\Models\DepartmentBranch;
use App\Models\Position;

class DemoStructure
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemoData  $event
     * @return void
     */
    public function handle(DemoData $event)
    {
        $branchModel = new Branch;
        $branchModel->company_id = $event->id;
        $branchModel->name = Company::find($event->id)->name;
        $branchModel->status = 1;
        $branchModel->is_corporate = 1;
        $branchModel->created_at = date('Y-m-d H:i:s');
        $branchModel->save();
        $departmentModel = new Department;
        $departmentModel->company_id = $event->id;
        $departmentModel->name = 'Everyone';
        $departmentModel->status = 4;
        $departmentModel->created_at = date('Y-m-d H:i:s');
        $departmentModel->save();
        $topDepartmentModel = new Department;
        $topDepartmentModel->company_id = $event->id;
        $topDepartmentModel->is_top = 1;
        $topDepartmentModel->name = 'Management';
        $topDepartmentModel->status = 1;
        $topDepartmentModel->created_at = date('Y-m-d H:i:s');
        $topDepartmentModel->save();
        $topDeptToCorpBranchModel = new DepartmentBranch;
        $topDeptToCorpBranchModel->company_id = $event->id;
        $topDeptToCorpBranchModel->department_id = $topDepartmentModel->id;
        $topDeptToCorpBranchModel->branch_id = $branchModel->id;
        $topDeptToCorpBranchModel->created_at = date('Y-m-d H:i:s');
        $topDeptToCorpBranchModel->save();
        $positionModel = new Position;
        $positionModel->company_id = $event->id;
        $positionModel->department_id = $departmentModel->id;
        $positionModel->name = 'Everyone';
        $positionModel->parent_id = null;
        $positionModel->info = 'Everyones position only needed in turtle';
        $positionModel->status = 4;
        $positionModel->created_by = 1;
        $positionModel->updated_by = 0;
        $positionModel->created_at = date('Y-m-d H:i:s');
        $positionModel->save();
        $settings = GeneralSettings::find(1);
        $trialStart = date('Y-m-d');
        $trialEnd = !empty($settings) ? date('Y-m-d', strtotime('+' . $settings->package_trial_count . ' days')) : date('Y-m-d', strtotime('+14 days'));
        Company::where('id', $event->id)->update(['trial_start' => $trialStart, 'trial_end' => $trialEnd]);
    }
}
