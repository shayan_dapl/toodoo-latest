<?php

namespace App\Listeners\Company;

use App\Events\Company\DemoData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\SupplierParameter;

class DemoSupplierParameter
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemoData  $event
     * @return void
     */
    public function handle(DemoData $event)
    {
        $list = [];
        $data = ['Price', 'Quality', 'Flexibility'];
        foreach ($data as $each) {
            $list[] = ['company_id' => $event->id, 'name' => $each, 'status' => 1, 'created_at' => date('Y-m-d H:i:s')];
        }
        SupplierParameter::insert($list);
    }
}
