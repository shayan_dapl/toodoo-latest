<?php

namespace App\Listeners\Company;

use App\Events\Company\DemoData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\SupplierRating;

class DemoSupplierRating
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemoData  $event
     * @return void
     */
    public function handle(DemoData $event)
    {
        $data = ['Bad', 'To improve', 'Good', 'excellent'];
        $list = [];
        $rate = 0;
        foreach ($data as $each) {
            $rate++;
            $list[] = ['company_id' => $event->id, 'rating' => $rate, 'description' => $each, 'status' => 1, 'created_at' => date('Y-m-d H:i:s')];
        }
        SupplierRating::insert($list);
    }
}
