<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\EntryLog;
use Auth;
use Config;

class TrackUserLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLog  $event
     * @return void
     */
    public function handle()
    {
        //
    }

    public function in()
    {
        if (Auth::guard('customer')->check()) {
            if (EntryLog::where('company_id', Auth::guard('customer')->user()->company_id)->count() > 0) {
                return EntryLog::where('company_id', Auth::guard('customer')->user()->company_id)->update(['in_time' => date('Y-m-d H:i:s')]);
            } else {
                return EntryLog::create([
                    'company_id' => Auth::guard('customer')->user()->company_id,
                    'ip_addr' => Config::get('settings.ip'),
                    'in_time' => date('Y-m-d H:i:s'),
                    'out_time' => null
                ]);
            }
        }
    }

    public function out()
    {
        if (Auth::guard('customer')->check()) {
            return EntryLog::where('company_id', Auth::guard('customer')->user()->company_id)->update(['out_time' => date('Y-m-d H:i:s')]);
        } else {
            return response()->json(false);
        }
    }
}
