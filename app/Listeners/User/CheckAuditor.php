<?php

namespace App\Listeners\User;

use App\Events\User\ProcessOwnership;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\AuditPlanAuditors;
use App\Models\AuditPlanMaster;
use App\Models\Users;
use Session;

class CheckAuditor
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProcessOwnership  $event
     * @return void
     */
    public function handle()
    {
        //
    }

    public function check($event)
    {
        $owner = ($event->auditor != null) ? 1 : 0;
        if ($owner == 0 && !empty($event->userId)) {
            $member = AuditPlanAuditors::where('auditor_id', $event->userId)->first();
            if (!empty($member)) {
                $status = AuditPlanMaster::companyCheck()->where('id', $member->audit_id)->first()->status;
            }
            if ($status != 2) {
                $upd = ['is_auditor' => 1];
                if ($event->emailCheck != null && !empty($event->email)) {
                    $upd['email'] = $event->email;
                    $upd['no_email'] = 0;
                }
                Users::where('id', $event->userId)->update($upd);
                $auditorErr = (Session::has('error')) ? Session::get('error') . " & " : '';
                $auditorErr = $auditorErr . translate('error.user_assigned_on_audit');
                Session::flash('error', $auditorErr);
            }
        }
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\User\Auditor',
            'App\Listeners\User\CheckAuditor@check'
        );
    }
}
