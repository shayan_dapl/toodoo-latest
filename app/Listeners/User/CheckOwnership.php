<?php

namespace App\Listeners\User;

use App\Events\User\ProcessOwnership;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Process;
use App\Models\Users;
use Session;

class CheckOwnership
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProcessOwnership  $event
     * @return void
     */
    public function handle()
    {
        //
    }

    public function check($event)
    {
        $owner = ($event->processOwner != null) ? 1 : 0;
        if ($owner == 0 && !empty($event->userId)) {
            $chkProcess = Process::where('owner_id', $event->userId)->count();
            if ($chkProcess > 0) {
                $upd = ['can_process_owner' => 1];
                if ($event->emailCheck != null && !empty($event->email)) {
                    $upd['email'] = $event->email;
                    $upd['no_email'] = 0;
                }
                Users::where('id', $event->userId)->update($upd);
                Session::flash('error', translate('error.user_assigned_on_process'));
            }
        }
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\User\ProcessOwnership',
            'App\Listeners\User\CheckOwnership@check'
        );
    }
}
