<?php

namespace App\Listeners\User;

use App\Events\User\UserPosAssign;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Users;
use App\Models\OrganizationStructure;
use App\Models\Position;

class PositionSave
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserPosAssign  $event
     * @return void
     */
    public function handle()
    {
        //
    }

    public function normalPosition($event)
    {
        if (!empty($event->positions['position'])) {
            $event->positions['position'] = array_keys(array_flip($event->positions['position']));
            foreach ($event->positions['position'] as $index => $position) {
                if ($position != 0) {
                    $details = Position::find($position);
                    $org = new OrganizationStructure;
                    $org->company_id = $event->companyId;
                    $org->user_id = $event->userId;
                    $org->department_id = $details->department_id;
                    $org->position_id = $position;
                    $org->save();
                    if ($details->parent_id == null) {
                        Users::where('id', $event->userId)->update(['is_top_person' => 1]);
                    }
                }
            }
        }
    }

    public function specialPosition($event)
    {
        if ($event->isSpecial != null && !empty($event->positions['special'])) {
            if ($event->positions['special']['id'] != 0) {
                $special = OrganizationStructure::findOrNew($event->positions['special']['existing']);
                $special->company_id = $event->companyId;
                $special->user_id = $event->userId;
                $special->department_id = Position::find($event->positions['special']['id'])->department_id;
                $special->position_id = $event->positions['special']['id'];
                $special->special_user = 1;
                $special->save();
            } else {
                OrganizationStructure::where('id', $event->positions['special']['existing'])->delete();
            }
        }
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\User\UserPosAssign',
            'App\Listeners\User\PositionSave@normalPosition'
        );

        $events->listen(
            'App\Events\User\UserPosAssign',
            'App\Listeners\User\PositionSave@specialPosition'
        );
    }
}
