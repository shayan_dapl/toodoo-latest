<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Languages;
use Config;

class AuditeeAssigned extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $auditName;
    public $processName;
    public $leadAuditor;
    public $auditDate;
    public $auditTime;
    public $category;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->name = $mailData['name'];
        $this->auditName = $mailData['auditName'];
        $this->processName = $mailData['processName'];
        $this->leadAuditor = $mailData['leadAuditor'];
        $this->auditDate = $mailData['auditDate'];
        $this->auditTime = $mailData['auditTime'];
        $this->lang = $mailData['lang'];
        $this->category = $mailData['category'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $adminEmail = Config::get('mail.from.address');
        $adminName = Config::get('mail.from.name');
        $subject = Languages::where('field', 'auditee_assigned')->where('segment', 'email')->pluck($this->lang);
        return $this->from($adminEmail, $adminName)->subject($subject[0])->view('emails.'.$this->lang.'.auditee_assigned_template', compact('name', 'auditName', 'processName', 'auditDate', 'auditTime', 'leadAuditor', 'category'));
    }
}
