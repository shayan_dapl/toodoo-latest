<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Languages;
use Config;
use Session;

class DocChangeNotify extends Mailable
{
    use Queueable, SerializesModels;
    public $firstName;
    public $processOwnerName;
    public $processName;
    public $docName;
    public $version;
    public $remark;
    public $hid;
    public $docType;
    public $lang;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->firstName = $mailData['firstName'];
        $this->processOwnerName = $mailData['processOwnerName'];
        $this->processName = $mailData['processName'];
        $this->docName = $mailData['docName'];
        $this->version = $mailData['version'];
        $this->docType = $mailData['docType'];
        $this->lang = $mailData['lang'];
        $this->remark = $mailData['remark'];
        $this->hid = $mailData['hid'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $adminEmail = Config::get('mail.from.address');
        $adminName = Config::get('mail.from.name');
        $subject = Languages::where('field', 'doc_change_nofitication')->where('segment', 'email')->pluck($this->lang);
        return $this->from($adminEmail, $adminName)->subject($subject[0])->view('emails.'.$this->lang.'.doc_change_notify_template', compact('firstName', 'processOwnerName', 'processName', 'docName', 'version', 'docType', 'lang', 'hid', 'remark'));
    }
}
