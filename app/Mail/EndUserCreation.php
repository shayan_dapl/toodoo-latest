<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Languages;
use Config;
use Session;

class EndUserCreation extends Mailable
{
    use Queueable, SerializesModels;
    public $fname;
    public $name;
    public $email;
    public $company;
    public $id;
    public $created_by;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->fname = $mailData['fname'];
        $this->name = $mailData['name'];
        $this->email = $mailData['email'];
        $this->company = $mailData['company'];
        $this->id = $mailData['id'];
        $this->lang = $mailData['lang'];
        $this->created_by = $mailData['created_by'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $adminEmail = Config::get('mail.from.address');
        $adminName = Config::get('mail.from.name');
        $subject = Languages::where('field', 'end_user_creation')->where('segment', 'email')->pluck($this->lang);
        return $this->from($adminEmail, $adminName)->subject($subject[0])->view('emails.'.$this->lang.'.end_user_creation_template', compact('fname', 'name', 'email', 'company', 'id', 'created_by'));
    }
}
