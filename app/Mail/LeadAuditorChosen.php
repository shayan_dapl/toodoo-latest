<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Languages;
use Config;
use Session;

class LeadAuditorChosen extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $processName;
    public $customerAdmin;
    public $auditId;
    public $category;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->name = $mailData['name'];
        $this->auditId = $mailData['auditId'];
        $this->processName = $mailData['processName'];
        $this->customerAdmin = $mailData['customerAdmin'];
        $this->lang = $mailData['lang'];
        $this->category = $mailData['category'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $adminEmail = Config::get('mail.from.address');
        $adminName = Config::get('mail.from.name');
        $subject = Languages::where('field', 'lead_auditor_chosen')->where('segment', 'email')->pluck($this->lang);
        return $this->from($adminEmail, $adminName)->subject($subject[0])->view('emails.'.$this->lang.'.lead_auditor_chosen_template', compact('name', 'auditId', 'processName', 'customerAdmin', 'category'));
    }
}
