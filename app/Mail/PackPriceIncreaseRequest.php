<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Languages;
use Config;
use Session;

class PackPriceIncreaseRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $customerAdmin;
    public $percent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->name = $mailData['name'];
        $this->customerAdmin = $mailData['customerAdmin'];
        $this->lang = $mailData['lang'];
        $this->percent = $mailData['percent'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $adminEmail = Config::get('mail.from.address');
        $adminName = Config::get('mail.from.name');
        $subject = Languages::where('field', 'package_price_increase_request')->where('segment', 'email')->pluck($this->lang);
        return $this->from($adminEmail, $adminName)->subject($subject[0])->view('emails.'.$this->lang.'.package_price_increase_request_template', compact('name', 'customerAdmin', 'percent'));
    }
}
