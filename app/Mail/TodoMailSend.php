<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Config;

class TodoMailSend extends Mailable
{
    use Queueable, SerializesModels;

    public $name;

    public $auditId;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->name = $mailData['name'];
        $this->auditId = $mailData['auditId'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $adminEmail = Config::get('mail.from.address');
        $adminName = Config::get('mail.from.name');
        return $this->from($adminEmail, $adminName)->subject($this->mailSubject)->view('emails.defult_template', compact('name', 'auditId'));
    }
}
