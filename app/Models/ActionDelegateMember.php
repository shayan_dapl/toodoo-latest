<?php 

namespace App\Models;

use Config;

use Illuminate\Database\Eloquent\Model;

class ActionDelegateMember extends Model
{
    use \App\Traits\ActionDelegateMemberTraits;
    
    protected $table = "action_delegate_members";

    public function action()
    {
        return $this->belongsTo('App\Models\ReportIssueAction');
    }

    public function user()
    {
        return $this->hasOne('App\Models\Users', 'id', 'member');
    }

    public function formattedDate()
    {
        return date(Config::get('settings.slashed_date'), strtotime($this->deadline));
    }

    public function docs()
    {
        return $this->hasMany('App\Models\AuditDelegationDocs', 'delegation_id', 'id');
    }

    public function dayCount()
    {
        $dateDiff = strtotime($this->deadline) - time();
        $dateDiff = ceil($dateDiff / (60 * 60 * 24));
        if ($dateDiff < 0) {
            $show = '<span class="overdue">' . abs($dateDiff) . " days overdue</span>";
        } elseif ($dateDiff == 0) {
            $show = '<span class="dayleft">' . abs($dateDiff) . " days left</span>";
        } else {
            $show = '<span class="dayleft">' . $dateDiff . " days left</span>";
        }
        return $show;
    }

    protected $appends = [];
}
