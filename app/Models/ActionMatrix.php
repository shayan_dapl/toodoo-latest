<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActionMatrix extends Model
{
    protected $table = "audit_action_matrix";
}
