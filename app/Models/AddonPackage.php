<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddonPackage extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "addon_package";

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function process()
    {
        return $this->belongsTo('App\Models\Process', 'process_id', 'id');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\PackageLocation', 'location_id', 'id');
    }

    public function situation()
    {
        return $this->belongsTo('App\Models\PackageSituation', 'situation_id', 'id');
    }

    public function theme()
    {
        return $this->belongsTo('App\Models\PackageThemeEnv', 'theme_id', 'id');
    }

    public function effective()
    {
        return $this->belongsTo('App\Models\PackageEffectEnv', 'effective_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\PackageCategory', 'category_id', 'id');
    }

    public function aspect()
    {
        return $this->belongsTo('App\Models\PackageAspectType', 'aspect_type_id', 'id');
    }

    public function evaluation()
    {
        return $this->belongsTo('App\Models\PackageEvaluation', 'evaluation_id', 'id');
    }

    public function kpi()
    {
        return $this->belongsTo('App\Models\MasterKpi', 'kpi_id', 'id');
    }
}
