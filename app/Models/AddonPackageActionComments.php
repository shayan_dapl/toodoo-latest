<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddonPackageActionComments extends Model
{
    protected $table = "addon_package_action_comments";

    public function files()
    {
        return $this->hasMany('App\Models\AddonPackageActionFiles', 'comment_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return date(\Config::get('settings.dashed_date'), strtotime($value));
    }
}
