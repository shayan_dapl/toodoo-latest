<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddonPackageActionFiles extends Model
{
    protected $table = "addon_package_action_files";
}
