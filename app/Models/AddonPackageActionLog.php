<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddonPackageActionLog extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "addon_package_action_log";

    public function setDeadlineAttribute($value)
    {
        $this->attributes['deadline'] = date("Y-m-d", strtotime($value));
    }

    public function getDeadlineAttribute($value)
    {
        return date(\Config::get('settings.dashed_date'), strtotime($value));
    }

    public function package()
    {
        return $this->belongsTo('App\Models\AddonPackage', 'package_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\AddonPackageActionComments', 'log_id', 'id');
    }
}
