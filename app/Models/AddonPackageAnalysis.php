<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddonPackageAnalysis extends Model
{
    protected $table = "addon_package_analysis";

    public function package()
    {
        return $this->belongsTo('App\Models\AddonPackage', 'addon_package_id', 'id');
    }

    public function parameter()
    {
        return $this->belongsTo('App\Models\PackageParameter', 'parameter_id', 'id');
    }

    public function rating()
    {
        return $this->belongsTo('App\Models\PackageRating', 'rating_id', 'id');
    }
}
