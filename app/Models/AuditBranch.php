<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditBranch extends Model
{
    protected $table = "audit_branches";

    public function branchDetail()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id', 'id');
    }
}
