<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditDelegationDocs extends Model
{
    protected $table = "audit_delegation_docs";

    public function delegation()
    {
        return $this->belongsTo('App\Models\ActionDelegateMember');
    }
}
