<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;

class AuditExecution extends Model
{
    use \App\Traits\AuditExecutionTraits;
    
    protected $table = "audit_execution";

    public function audit()
    {
        return $this->belongsTo('App\Models\AuditPlanMaster');
    }

    public function docs()
    {
        return $this->hasMany('App\Models\AuditExecutionDocs', 'execution_id', 'id');
    }

    public function getSourceTypeAttribute()
    {
        if ($this->audit_id!='') {
            $companyId = \App\Models\AuditPlanMaster::find($this->audit_id)->company_id;
            $sourceTypeId = \App\Models\SourceTypes::where('company_id', $companyId)->first();
            if ($sourceTypeId->count() > 0) {
                return $sourceTypeId;
            } else {
                return [];
            }
        }
    }

    public function getFindingTypeAttribute()
    {
        $data = \App\Models\FindingTypes::find($this->finding_type_id);
        if ($data->count() > 0) {
            return $data;
        } else {
            return [];
        }
    }

    public function findingtypes()
    {
        return $this->belongsTo('App\Models\FindingTypes', 'finding_type_id', 'id');
    }

    public function getProcessAttribute()
    {
        if ($this->audit_id!='') {
            $processId = $this->process_id;
            $process = \App\Models\Process::find($processId);
            if ($process->count() > 0) {
                return $process;
            } else {
                return [];
            }
        }
    }

    public function references()
    {
        return $this->belongsTo('App\Models\AuditTypeMaster');
    }

    public function getReferencesAttribute()
    {
        $langArr = \Config::get('settings.lang');
        $name = ' name_' . $langArr[session('lang')];

        $data = \App\Models\AuditTypeMaster::select(\DB::raw("CONCAT(clause, ' ', $name) AS full_name"), 'id')->where('iso_ref_id', $this->iso_no)->pluck('full_name', 'id')->prepend(translate('form.select'), '');

        return $data;
    }

    public function iso()
    {
        return $this->belongsTo('App\Models\IsoClause', 'iso_no', 'id');
    }

    public function reference()
    {
        return $this->belongsTo('App\Models\AuditTypeMaster', 'reference_id', 'id');
    }

    public function actions()
    {
        return $this->hasMany('App\Models\ReportIssueAction', 'ref_id', 'id');
    }

    protected $appends = ['source_type', 'finding_type', 'process'];
}
