<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditExecutionDocs extends Model
{
    use \App\Traits\AuditExecutionDocsTraits;
    
    protected $table = "audit_execution_docs";

    public function execution()
    {
        return $this->belongsTo('App\Models\AuditExecution');
    }
}
