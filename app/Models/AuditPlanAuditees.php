<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditPlanAuditees extends Model
{
    protected $table = "audit_plan_auditees";

    public function plan()
    {
        return $this->belongsTo('App\Models\AuditPlanMaster', 'audit_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users');
    }

    public function getLeadAuditorAttribute()
    {
        $leadAuditorId = \App\Models\AuditPlanMaster::find($this->audit_id)->choose_lead_auditor;
        return Users::find($leadAuditorId)->name;
    }

    public function getLeadAuditorIdAttribute()
    {
        $leadAuditorId = \App\Models\AuditPlanMaster::find($this->audit_id)->choose_lead_auditor;
        return $leadAuditorId;
    }

    public function getProcessAttribute()
    {
        $getAllProcess = \App\Models\ProcessToAudit::where('audit_id', $this->audit_id)->get()->map(function ($each) {
            return $each->processDetail->name;
        })->toArray();

        return $getAllProcess;
    }

    protected $appends = ['lead_auditor', 'lead_auditor_id', 'process'];
}
