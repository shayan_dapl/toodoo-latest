<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditPlanMaster extends Model
{
    use \App\Traits\CompanyCheckTraits, \App\Traits\AuditPlanMasterTraits;

    protected $table = "audit_plan_master";

    protected $guarded = ['id'];

    public function audit_type()
    {
        return $this->belongsTo('App\Models\IsoClause', 'type_of_audit', 'id');
    }

    public function setPlanDateAttribute($value)
    {
        $this->attributes['plan_date'] = date('Y-m-d', strtotime($value));
    }

    public function process()
    {
        return $this->hasMany('App\Models\ProcessToAudit', 'audit_id', 'id');
    }

    public function lead_auditor()
    {
        return $this->belongsTo('App\Models\Users', 'choose_lead_auditor', 'id');
    }

    public function branch()
    {
        return $this->hasMany('App\Models\AuditBranch', 'audit_id', 'id');
    }

    public function executions()
    {
        return $this->hasMany('App\Models\AuditExecution', 'audit_id', 'id');
    }

    public function auditors()
    {
        return $this->hasMany('App\Models\AuditPlanAuditors', 'audit_id', 'id')->where('auditor_type', '<>', '1');
    }

    public function auditees()
    {
        return $this->hasMany('App\Models\AuditPlanAuditees', 'audit_id', 'id');
    }
}
