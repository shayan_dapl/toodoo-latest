<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;

class AuditTypeMaster extends Model
{
    use \App\Traits\AuditTypeMasterTraits;
    protected $table = "audit_type_master";
    public $timestamps = false;
    protected $guarded = ['id'];

    public function getParentAttribute()
    {
        $parentData = \App\Models\AuditTypeMaster::where('id', $this->parent_id);
        if ($parentData->count() > 0) {
            $clause = [
                'nl' => ($parentData->first()->clause . ' ' . $parentData->first()->name_nl),
                'en' => ($parentData->first()->clause . ' ' . $parentData->first()->name_en),
                'fr' => ($parentData->first()->clause . ' ' . $parentData->first()->name_fr),
            ];
            return $clause[session('lang')];
        } else {
            return "NA";
        }
    }

    protected $appends = ['parent'];
}
