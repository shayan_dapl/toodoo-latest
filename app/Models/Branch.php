<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use \App\Traits\CompanyCheckTraits, \App\Traits\BranchTraits;
    protected $table = "branch";

    public function setEditAttribute()
    {
        $this->edit = false;
    }

    public function getEditAttribute()
    {
        return false;
    }

    public static function branchChild($branch)
    {
        $departmentBranch = DepartmentBranch::where('branch_id', $branch)->count();
        $contextBranch = ContextBranch::where('branch_id', $branch)->count();
        $positionBranch = PositionBranches::where('branch_id', $branch)->count();
        $processBranch = ProcessBranch::where('branch_id', $branch)->count();
        $stakeholderBranch = StakeholderBranch::where('branch_id', $branch)->count();
        
        $totalChild = $departmentBranch + $contextBranch + $positionBranch + $processBranch + $stakeholderBranch;
        return $totalChild;
    }

    public function departments()
    {
        return $this->hasMany('App\Models\DepartmentBranch', 'branch_id', 'id');
    }

    protected $appends = ['edit'];
}
