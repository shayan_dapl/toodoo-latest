<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsersTraits;
use Config;

class Company extends Model
{
    protected $table = "company";
    public $timestamps = false;
    protected $prefilled = "no";
    use UsersTraits;

    public function branches()
    {
        return $this->hasMany('App\Models\Branch', 'company_id', 'id');
    }

    public function departments()
    {
        return $this->hasMany('App\Models\Department', 'company_id', 'id')->where('status', 1);
    }

    public function positions()
    {
        return $this->hasMany('App\Models\Position', 'company_id', 'id')->where('status', 1);
    }

    public function categorywiseprocess($cat)
    {
        return $this->hasMany('App\Models\Process', 'company_id', 'id')->where('status', 1)->where('category_id', $cat);
    }

    public function audits()
    {
        return $this->hasMany('App\Models\AuditPlanMaster', 'company_id', 'id');
    }

    public function contexts()
    {
        return $this->hasMany('App\Models\ContextAnalysis', 'company_id', 'id')->where('status', 1);
    }

    public function stakeholders()
    {
        return $this->hasMany('App\Models\StakeholderAnalysis', 'company_id', 'id')->where('status', 1);
    }

    public function reportfindings()
    {
        return $this->hasMany('App\Models\ReportedIssues', 'company_id', 'id');
    }

    public function users()
    {
        return $this->hasMany('App\Models\Users', 'company_id', 'id');
    }

    public function customeradmins()
    {
        return $this->hasMany('App\Models\Users', 'company_id', 'id')->inServiceWithEmail()->where('type', 2)->where('email', '<>', Config::get('settings.associate_user'));
    }

    public function processes()
    {
        return $this->hasMany('App\Models\Process', 'company_id', 'id');
    }
    
    public function getTrialEndDateAttribute()
    {
        return date('d-m-Y', strtotime($this->trial_end));
    }

    public function getTrialDaysAttribute()
    {
        $segment = '';
        if ($this->trial_end != null && $this->trial_start != null) {
            $dateDiff = strtotime($this->trial_end) - strtotime(date('Y-m-d'));
            $segment = ceil($dateDiff / (60 * 60 * 24));
        }
        return $segment;
    }

    public function getTrialCountAttribute()
    {
        $segment = '';
        if ($this->trial_end != null && $this->trial_start != null) {
            $dateDiff = strtotime($this->trial_end) - strtotime($this->trial_start);
            $dateDiff = ceil($dateDiff / (60 * 60 * 24));
            $segment = round($dateDiff / 3);
        }
        return $segment;
    }

    public function setPrefilledAttribute($value)
    {
        $this->prefilled = $value;
    }

    public function getPrefilledAttribute()
    {
        return $this->prefilled;
    }

    public function countries()
    {
        return $this->belongsTo('App\Models\Countries', 'country', 'id');
    }

    public function log()
    {
        return $this->hasOne('App\Models\EntryLog');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\TransactionDetails', 'company_id', 'id')->where('invoice_no', '<>', '')->orderBy('transaction_details.id', 'desc');
    }

    public function priceincreaseproposal()
    {
        return $this->hasMany('App\Models\PriceIncreaseLog', 'company_id', 'id')->orderBy('price_increase_request_log.id', 'asc');
    }

    public function getSubscriptionCancelledAtAttribute($value)
    {
        return !empty($value) ? \Carbon\Carbon::parse($value)->format(Config::get('settings.dashed_date')) : '';
    }

    public function getNextRenewalDateAttribute($value)
    {
        return !empty($value) ? \Carbon\Carbon::parse($value)->format(Config::get('settings.dashed_date')) : '';
    }

    public function evaluatedSupplier()
    {
        return $this->hasMany('App\Models\SupplierEvaluationActionLog', 'company_id', 'id');
    }

    public function resourceActivity()
    {
        return $this->hasMany('App\Models\ResourceDelegationActionLog', 'company_id', 'id');
    }

    protected $appends = ['trial_days', 'trial_count', 'trial_end_date'];
}
