<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyPackageDetails extends Model
{
    protected $table = "company_package_details";

    public function package()
    {
        return $this->belongsTo('App\Models\PackageMaster', 'package_id', 'id');
    }
}
