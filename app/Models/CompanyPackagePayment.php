<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyPackagePayment extends Model
{
    protected $table = "company_package_payment";
}
