<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanySettings extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "company_settings";

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }
}
