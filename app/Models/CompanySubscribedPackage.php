<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanySubscribedPackage extends Model
{
    protected $table = "company_subscribed_package";

    public function packages()
    {
        return $this->hasMany('App\Models\CompanyPackageDetails', 'subscription_id', 'id')->get()->pluck('package_id')->toArray();
    }

    public function details()
    {
        return $this->hasMany('App\Models\CompanyPackageDetails', 'subscription_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo('App\Models\PackageUserPlan', 'user_count', 'id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\PackageStoragePlan', 'used_storage_id', 'id');
    }

    public function payment()
    {
        return $this->hasOne('App\Models\CompanyPackagePayment', 'used_package_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }
}
