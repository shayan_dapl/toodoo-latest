<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetentRatingActionComments extends Model
{
    protected $table = "competent_rating_action_comments";

    public function log()
    {
        return $this->belongsTo('App\Models\CompetentRatingsActionLog');
    }

    public function files()
    {
        return $this->hasMany('App\Models\CompetentRatingActionFiles', 'comment_id', 'id');
    }
}
