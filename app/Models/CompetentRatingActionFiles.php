<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetentRatingActionFiles extends Model
{
    protected $table = "competent_rating_action_files";

    public function comment()
    {
        return $this->belongsTo('App\Models\CompetentRatingActionComments');
    }
}
