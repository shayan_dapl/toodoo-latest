<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetentRatingActionType extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "competent_rating_action_type";

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function actions()
    {
        return $this->hasMany('App\Models\CompetentRatingsActionLog', 'action_type', 'id');
    }
}
