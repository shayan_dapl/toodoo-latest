<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetentRatings extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = 'competent_ratings';

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function process()
    {
        return $this->belongsTo('App\Models\Processs');
    }

    public function step()
    {
        return $this->belongsTo('App\Models\ProcessSteps');
    }

    public function org()
    {
        return $this->belongsTo('App\Models\OrganizationStructure');
    }

    public function ratingName()
    {
        return $this->belongsTo('App\Models\Rating', 'rating', 'id');
    }

    public function actions()
    {
        return $this->hasMany('App\Models\CompetentRatingsActionLog', 'rating_id', 'id');
    }

    public function getUserAttribute()
    {
        return ($this->actions()->count() > 0) ? $this->actions[0]->user->name : null;
    }

    public function getCreatedAtAttribute($value)
    {
        return date(\Config::get('settings.dashed_date'), strtotime($value));
    }

    public function getRatingNameAttribute()
    {
        if ($this->ratingName()->count() > 0) {
            return $this->ratingName()->first()->name;
        } else {
            return null;
        }
    }

    protected $appends = ['rating_name', 'user'];
}
