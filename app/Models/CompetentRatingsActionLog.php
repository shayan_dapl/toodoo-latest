<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetentRatingsActionLog extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = 'competent_ratings_action_log';

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\Users', 'delegate_to', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\CompetentRatingActionComments', 'log_id', 'id');
    }

    public function setDeadlineAttribute($value)
    {
        $this->attributes['deadline'] = date('Y-m-d', strtotime($value));
    }

    public function actionType()
    {
        return $this->belongsTo('App\Models\CompetentRatingActionType', 'action_type', 'id');
    }

    public function getTypeNameAttribute()
    {
        return !empty($this->actionType) ? $this->actionType->type : null;
    }

    public function getFilesAttribute()
    {
        $files = [];
        if ($this->comments->count() > 0) {
            foreach ($this->comments as $eachComment) {
                if ($eachComment->files->count() > 0) {
                    foreach ($eachComment->files as $eachFile) {
                        $extension = extension($eachFile->document_name);
                        $files[] = getfileType($extension, $eachFile->document_name, 'competent_rating_action_docs');
                    }
                }
            }
        }
        return $files;
    }

    protected $appends = ['type_name', 'files'];
}
