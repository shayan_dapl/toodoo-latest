<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class ContextAnalysis extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "context_analysis";

    public function subjectname()
    {
        return $this->belongsTo('App\Models\ContextSubjects', 'subject', 'id');
    }

    public function powner()
    {
        return $this->belongsTo('App\Models\Users', 'owner', 'id');
    }

    public function process()
    {
        return $this->belongsTo('App\Models\Process', 'process_id', 'id');
    }

    public function logs()
    {
        return $this->hasMany('App\Models\ContextStakeholderActionLog', 'contxt_stakehold_id', 'id')->where('type', 'context');
    }

    public function branches()
    {
        return $this->hasMany('App\Models\ContextBranch', 'context_id', 'id');
    }

    public function brancheids($branchId = null, $contextId)
    {
        return \App\Models\ContextBranch::where('branch_id', $branchId)->where('context_id', $contextId)->count();
    }

    public function openlogs()
    {
        return $this->hasMany('App\Models\ContextStakeholderActionLog', 'contxt_stakehold_id', 'id')->where('type', 'context')->where('closed', 0);
    }

    public function overduelogs()
    {
        return $this->hasMany('App\Models\ContextStakeholderActionLog', 'contxt_stakehold_id', 'id')->where('type', 'context')->where('closed', 0)->whereDate('deadline', '<', \Carbon\Carbon::now());
    }
}
