<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContextBranch extends Model
{
    protected $table = "context_branches";


    public function contextbranch()
    {
        return $this->belongsTo('App\Models\ContextAnalysis', 'id', 'context_id');
    }
}
