<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContextStakeholderActionComments extends Model
{
    protected $table = "context_stakeholder_action_comments";

    public function log()
    {
        return $this->belongsTo('App\Models\ContextStakeholderActionLog');
    }

    public function files()
    {
        return $this->hasMany('App\Models\ContextStakeholderActionFiles', 'comment_id', 'id');
    }
}
