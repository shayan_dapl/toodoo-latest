<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContextStakeholderActionFiles extends Model
{
    protected $table = "context_stackholder_action_files";

    public function comment()
    {
        return $this->belongsTo('App\Models\ContextStakeholderActionComments');
    }
}
