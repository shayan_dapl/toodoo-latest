<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class ContextStakeholderActionLog extends Model
{
    use \App\Traits\CompanyCheckTraits;

    protected $table = "context_stakeholder_action_log";

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\ContextStakeholderActionComments', 'log_id', 'id');
    }

    public function setDeadlineAttribute($value)
    {
        $this->attributes['deadline'] = \Carbon\Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function origin()
    {
        return ($this->type == 'stakeholder') ? StakeholderAnalysis::find($this->contxt_stakehold_id)->stakeholder_name : ContextAnalysis::find($this->contxt_stakehold_id)->issue;
    }

    public function getDeadlineAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format(Config::get('settings.dashed_date'));
    }

    public function getDeligateToAttribute($value)
    {
        $userData = Users::find($value);
        if (!empty($userData)) {
            return $userData->name;
        } else {
            return "";
        }
    }
}
