<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContextSubjects extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "context_subject";
}
