<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "department";
    protected $edit = false;

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function positions()
    {
        return $this->hasMany('App\Models\Position');
    }

    public function branches()
    {
        return $this->hasMany('App\Models\DepartmentBranch', 'department_id', 'id');
    }

    public function setEditAttribute()
    {
        $this->edit = false;
    }

    public function getEditAttribute()
    {
        return false;
    }

    public function getBranchNamesAttribute()
    {
        $branches = $this->branches()->get();
        $branchName = [];
        if ($branches->count() > 0) {
            foreach ($branches as $branch) {
                $branchName[] = $branch->branch->name;
            }
        }
        return implode(", ", $branchName);
    }
    
    public function getBranchIdsAttribute()
    {
        $branches = $this->branches()->get();
        $branchId = [];
        if ($branches->count() > 0) {
            foreach ($branches as $branch) {
                $branchId[] = $branch->branch->id;
            }
        }
        return implode(", ", $branchId);
    }

    protected $appends = ['edit', 'branchNames', 'branchIds'];
}
