<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepartmentBranch extends Model
{
    use \App\Traits\CompanyCheckTraits;
    
    protected $table = "department_branches";

    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'department_id', 'id');
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id', 'id');
    }
}
