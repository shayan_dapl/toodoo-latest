<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class EntryLog extends Model
{
    protected $table = "entry_log";
    public $timestamps = false;
    protected $fillable = ['company_id', 'ip_addr', 'in_time', 'out_time'];

    public function user()
    {
        return $this->belongsTo("App\Models\Users");
    }

    public function getInTimeAttribute($value)
    {
        if (!empty($value)) {
            return \Carbon\Carbon::parse($value)->format(Config::get('settings.dashed_date_time'));
        } else {
            return "";
        }
    }
}
