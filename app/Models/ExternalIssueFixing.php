<?php 

namespace App\Models;

use Config;

use Illuminate\Database\Eloquent\Model;

class ExternalIssueFixing extends Model
{
    protected $table = "external_issue_fixing";

    public function getCreatedAtAttribute($value)
    {
        return date(Config::get('settings.slashed_date'), strtotime($value));
    }
}
