<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FindingTypes extends Model
{
    use \App\Traits\CompanyCheckTraits;
    use \App\Traits\FindingTypesTraits;

    protected $table = "finding_type";

    public function matrix()
    {
        return $this->belongsTo('App\Models\ActionMatrix', 'finding_type', 'id');
    }

    public function executedTypes($audit)
    {
        return \App\Models\AuditExecution::where('audit_id', $audit)->where('finding_type_id', $this->id)->count();
    }
}
