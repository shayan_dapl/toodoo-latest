<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Input extends Model
{
    protected $table = "input";

    public function process()
    {
        return $this->belongsTo('App\Models\Process');
    }

    public function getSupportingProcessAttribute()
    {
        if (!empty($this->supporting_process_id)) {
            return Process::where('id', $this->supporting_process_id)->first()->name;
        } else {
            return null;
        }
    }
}
