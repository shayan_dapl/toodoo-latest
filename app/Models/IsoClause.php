<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IsoClause extends Model
{
    protected $table = "iso_reference";
    public $timestamps = false;

    public function clauses()
    {
        return $this->hasMany('App\Models\AuditTypeMaster', 'iso_ref_id', 'id');
    }
}
