<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kpi extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "kpi";

    public function process()
    {
        return $this->belongsTo('App\Models\Process');
    }

    public function masterkpi()
    {
        return $this->hasMany('App\Models\KpiToMasterKpi', 'kpi_id', 'id');
    }

    public function kpiunit()
    {
        return $this->belongsTo('App\Models\KpiUnit', 'kpi_unit_id', 'id');
    }

    public function processkpi()
    {
        return $this->hasMany('App\Models\ProcessKpiData', 'kpi_id', 'id');
    }

    public function getValidFromAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format(\Config::get('settings.dashed_date'));
    }

    public function getValidToAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format(\Config::get('settings.dashed_date'));
    }
}
