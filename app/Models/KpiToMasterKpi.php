<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KpiToMasterKpi extends Model
{
    protected $table = 'kpi_to_master_kpi';

    public function master()
    {
        return $this->belongsTo('App\Models\MasterKpi', 'master_kpi_id', 'id');
    }

    public function kpi()
    {
        return $this->belongsTo('App\Models\Kpi', 'kpi_id', 'id');
    }
}
