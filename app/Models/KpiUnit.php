<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KpiUnit extends Model
{
    protected $table = 'kpi_unit';

    public function setEditAttribute($value)
    {
        $this->edit = false;
    }

    public function getEditAttribute()
    {
        return false;
    }

    protected $appends = ['edit'];
}
