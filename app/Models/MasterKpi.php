<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterKpi extends Model
{
    use \App\Traits\CompanyCheckTraits;

    protected $table = "master_kpi";

    public function kpiunit()
    {
        return $this->belongsTo('App\Models\KpiUnit', 'kpi_unit_id', 'id');
    }

    public function processkpis()
    {
        return $this->hasMany("App\Models\KpiToMasterKpi", "master_kpi_id", "id");
    }
}
