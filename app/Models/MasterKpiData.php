<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterKpiData extends Model
{
    use \App\Traits\CompanyCheckTraits;

    protected $table = "master_kpi_data";

    public function masterkpi()
    {
        return $this->belongsTo('App\Models\MasterKpi', 'master_kpi_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format(\Config::get('settings.dashed_date'));
    }
}
