<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Opportunity extends Model
{
    protected $table = "opportunity";

    public function process()
    {
        return $this->belongsTo('App\Models\Process');
    }

    public function getOpportunityactionAttribute()
    {
        $data = RiskOpportunityActionLog::where('type', 'opportunity')->where('risk_opp_id', $this->id)->latest()->first();
        if (!empty($data)) {
            return $data;
        } else {
            return [];
        }
    }

    protected $appends = ['opportunityaction'];
}
