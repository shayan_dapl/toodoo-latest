<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganizationStructure extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "organization_structure";
    protected $fillable = ['company_id', 'user_id', 'department_id', 'position_id'];

    public function user()
    {
        return $this->belongsTo("App\Models\Users", "user_id", "id");
    }

    public function department()
    {
        return $this->belongsTo("App\Models\Department");
    }

    public function position()
    {
        return $this->belongsTo("App\Models\Position", "position_id", "id");
    }

    public function parentName($parent)
    {
        return OrganizationStructure::where('position_id', $parent)->first();
    }

    public function getTitleAttribute()
    {
        if (!empty($this->position)) {
            $branch = \App\Models\PositionBranches::where('position_id', $this->position->id)->first()->branch->name;
            return $branch ." <br /> ". $this->position->name;
        } else {
            if (!empty($this->assist_as)) {
                return $this->assist_as;
            } else {
                return translate('form.assistant');
            }
        }
    }

    public function getFullnameAttribute()
    {
        if ($this->special_user == 1) {
            return \App\Models\OrganizationStructure::where('company_id', \Auth::guard('customer')->user()->company_id)
                ->where('position_id', $this->position->id)
                ->where('parent', $this->parent)
                ->where('special_user', 1)
                ->count() . " " . translate('form.person');
        } else {
            $icons = "";
            if ($this->user->is_auditor == 1) {
                $icons .= view('pages.helper.icons')->with('au', 1)->render();
            }
            if ($this->user->can_process_owner == 1) {
                $icons .= view('pages.helper.icons')->with('po', 1)->render();
            }
            return ($icons . $this->user->name);
        }
    }

    public function getPhotoAttribute()
    {
        if ($this->special_user == 1) {
            $name = "";
            $users = \App\Models\OrganizationStructure::where('company_id', \Auth::guard('customer')->user()->company_id)
                ->where('position_id', $this->position->id)
                ->where('parent', $this->parent)
                ->where('special_user', 1)
                ->get();
            if (!empty($users)) {
                $iteration = 0;
                foreach ($users as $user) {
                    $iteration++;
                    $name .= $user->user->name;
                    if ($iteration < count($users)) {
                        $name .= '<hr style="margin: 5px 0px 5px 0px; border-color: #4D8DCE">';
                    }
                }
            }
            return $name;
        } else {
            return $this->user->photo;
        }
    }

    public function getAssistantAttribute()
    {
        $assist = \App\Models\Position::where('company_id', \Auth::guard('customer')->user()->company_id)
            ->where('parent_id', $this->position_id)
            ->where('is_assistant', 1);
        if ($assist->count() > 0) {
            $data = \App\Models\OrganizationStructure::where('company_id', \Auth::guard('customer')->user()->company_id)
                ->where('position_id', $assist->first()->id)
                ->where('assistant', 1);
            if ($data->count() > 0) {
                $details = $data->first()->toArray();
                $details['name'] = $details['title'];
                $details['title'] = $details['fullname'];
                return $details;
            } else {
                return 0;
            }
        }
    }

    public function getParentidAttribute()
    {
        $data = OrganizationStructure::find($this->parent);
        if (!empty($data)) {
            return $data;
        } else {
            return [];
        }
    }

    public $timestamps = false;
    protected $appends = ['title', 'fullname', 'photo', 'assistant'];
}
