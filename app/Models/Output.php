<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Output extends Model
{
    protected $table = "output";

    public function process()
    {
        return $this->belongsTo('App\Models\Process');
    }

    public function input()
    {
        return $this->belongsTo('App\Models\Input');
    }

    public function getSupportingProcessAttribute()
    {
        if (isset($this->supporting_process_id)) {
            return Process::where('id', $this->supporting_process_id)->first()->name;
        } else {
            return null;
        }
    }
}
