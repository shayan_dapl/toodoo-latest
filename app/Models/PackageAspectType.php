<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageAspectType extends Model
{
    protected $table = "package_aspect_type";
    use \App\Traits\CompanyCheckTraits;

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }
}
