<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageCategory extends Model
{
    protected $table = "package_category";
    use \App\Traits\CompanyCheckTraits;

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }
}
