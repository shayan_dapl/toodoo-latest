<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageEffectEnv extends Model
{
    protected $table = "package_effect_env";
    use \App\Traits\CompanyCheckTraits;

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }
}
