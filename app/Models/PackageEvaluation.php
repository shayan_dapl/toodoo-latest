<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageEvaluation extends Model
{
    protected $table = "package_evaluation";
    use \App\Traits\CompanyCheckTraits;

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }
}
