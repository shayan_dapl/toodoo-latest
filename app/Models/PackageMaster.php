<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageMaster extends Model
{
    use \App\Traits\PackageMasterTraits, \App\Traits\CompanyCheckTraits;
    protected $table = "package_master";

    public function getDurationAttribute($value)
    {
        return (int) $value;
    }

    public function iso()
    {
        return $this->belongsTo('App\Models\IsoClause', 'iso_id', 'id');
    }
}
