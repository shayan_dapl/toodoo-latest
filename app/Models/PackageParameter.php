<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageParameter extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "package_parameter";

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }
}
