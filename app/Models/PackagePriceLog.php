<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackagePriceLog extends Model
{
    protected $table = "package_price_log";
}
