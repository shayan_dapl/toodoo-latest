<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackagePurchaseMaster extends Model
{
    protected $table = "package_purchase_master";

    public function transaction()
    {
        return $this->belongsTo('App\Models\TransactionDetails', 'transaction_id', 'id');
    }

    public function iso($id = null)
    {
        if ($id == null) {
            $id = $this->package_id;
        }
        return \App\Models\PackageMaster::find($id);
    }

    public function user($id = null)
    {
        if ($id == null) {
            $id = $this->package_id;
        }
        return \App\Models\PackageUserPlan::find($id);
    }

    public function storage($id = null)
    {
        if ($id == null) {
            $id = $this->package_id;
        }
        return \App\Models\PackageStoragePlan::find($id);
    }
}
