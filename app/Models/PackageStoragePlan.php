<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageStoragePlan extends Model
{
    protected $table = "package_storage_plan";

    public function getStorageDataAttribute()
    {
        return $this->space/1024 . ' GB (+ €' . $this->price_per_month . '/ Month)';
    }
}
