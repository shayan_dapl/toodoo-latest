<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageThemeEnv extends Model
{
    protected $table = "package_theme_env";
    use \App\Traits\CompanyCheckTraits;

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }
}
