<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Auth;

class Position extends Model
{
    use \App\Traits\CompanyCheckTraits, \App\Traits\PositionTraits;
    
    protected $table = "position";

    public function company()
    {
        return $this->belongsTo("App\Models\Company");
    }

    public function department()
    {
        return $this->belongsTo("App\Models\Department");
    }

    public function structure()
    {
        return $this->hasMany("App\Models\OrganizationStructure", "position_id", "id")
            ->where('active_user', 1)
            ->where('assistant', 0);
    }

    public function competentstructure($orgIds = [])
    {
        $orgData = $this->hasMany("App\Models\OrganizationStructure", "position_id", "id")
            ->where('active_user', 1)
            ->where('assistant', 0);
        if (!empty($orgIds)) {
            $orgData = $orgData->whereIn('id', $orgIds);
        }
        return $orgData;
    }

    public function orgExistance()
    {
        return $this->hasMany("App\Models\OrganizationStructure", "position_id", "id")
            ->where('active_user', 1);
    }

    public function children()
    {
        return $this->hasMany("App\Models\Position", "parent_id", "id");
    }

    public function getParentAttribute()
    {
        $data = \App\Models\Position::where('company_id', \Auth::guard('customer')->user()->company_id)
            ->where('id', $this->parent_id)
            ->where('status', 1)
            ->first();
        if (!empty($data)) {
            return $data;
        } else {
            return [];
        }
    }

    public function getChildAssistantAttribute()
    {
        $data = \App\Models\Position::where('parent_id', $this->id)
            ->where('is_assistant', 1)
            ->where('is_special_position', 0)
            ->where('status', 1)
            ->count();
        if ($data > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function branches()
    {
        return $this->hasOne('App\Models\PositionBranches', 'position_id', 'id');
    }

    public function getNameWithDetailsAttribute()
    {
        $routeNames = [
            "process-step-form",
            "process-step-edit",
            "user-form",
            "user-edit",
            "profile-form"
        ];
        if (in_array(\Request::route()->getName(), $routeNames)) {
            return $this->name . ' (' . $this->branches->branch->name . ' : ' . $this->department->name . ')';
        } else {
            return null;
        }
    }

    public function roles()
    {
        return $this->hasManyTo('App\Models\Roles', 'position_id', 'id');
    }

    protected $appends = ['parent', 'child_assistant', 'name_with_details'];
}
