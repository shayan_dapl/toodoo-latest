<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PositionBranches extends Model
{
    use \App\Traits\CompanyCheckTraits;

    protected $table = "position_branches";

    public function position()
    {
        return $this->belongsTo('App\Models\Position', 'position_id', 'id');
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id', 'id');
    }

    public function getPositionNameAttribute()
    {
        return $this->branch->name . "-" . $this->position->name;
    }

    protected $appends = ['position_name'];
}
