<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class PositionLog extends Model
{
    use \App\Traits\CompanyCheckTraits;

    protected $table = "position_log";
    protected $fillable = ['company_id', 'user_id', 'position_id'];

    public function company()
    {
        return $this->belongsTo("App\Models\Company", 'company_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo("App\Models\Users", 'user_id', 'id');
    }

    public function position()
    {
        return $this->belongsTo("App\Models\Position", 'position_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format(Config::get('settings.dashed_date'));
    }
}
