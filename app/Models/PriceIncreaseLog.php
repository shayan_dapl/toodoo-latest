<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceIncreaseLog extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "price_increase_request_log";
}
