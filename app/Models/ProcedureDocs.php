<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcedureDocs extends Model
{
    protected $table = "procedure_docs";

    public function procedure()
    {
        return $this->belongsTo('App\Models\Procedures');
    }
}
