<?php 

namespace App\Models;

use Config;

use Illuminate\Database\Eloquent\Model;

class Procedures extends Model
{
    protected $table = "procedures";

    public function process()
    {
        return $this->belongsTo('App\Models\Process');
    }

    public function docs()
    {
        return $this->hasMany('App\Models\ProcedureDocs', 'procedure_id', 'id');
    }

    public function regdocs()
    {
        return $this->hasMany('App\Models\RegulationDocs', 'regulation_id', 'id');
    }

    public function documents($docId)
    {
        return ProcedureDocs::where('procedure_id', $docId)->latest()->first();
    }

    public function regdocuments($docId)
    {
        return RegulationDocs::where('regulation_id', $docId)->latest()->first();
    }

    public function getDocumentTypeAttribute()
    {
        $doc = ProcessDocumentTypes::where('id', $this->doc_type_id);
        $result = "";
        if ($doc->count() > 0) {
            $result = $doc->first()->type_name;
        }
        return $result;
    }

    public function getRegulationTypeAttribute()
    {
        $doc = ProcessRegulationTypes::where('id', $this->doc_type_id);
        $result = "";
        if ($doc->count() > 0) {
            $result = $doc->first()->type_name;
        }
        return $result;
    }

    public function getCreatedAtAttribute($value)
    {
        return date(Config::get('settings.dashed_date'), strtotime($value));
    }
}
