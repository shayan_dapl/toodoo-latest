<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\ProcessTraits;

class Process extends Model
{
    use ProcessTraits;
    use \App\Traits\CompanyCheckTraits;

    protected $table = "process";

    public function category()
    {
        return $this->belongsTo('App\Models\ProcessCategory', 'category_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function owner()
    {
        return $this->belongsTo('App\Models\Users', 'owner_id', 'id');
    }

    public function scopes()
    {
        return $this->hasMany('App\Models\Scope');
    }

    public function procedures()
    {
        return $this->hasMany('App\Models\Procedures');
    }

    public function kpis()
    {
        return $this->hasMany('App\Models\Kpi');
    }

    public function inputs()
    {
        return $this->hasMany('App\Models\Input');
    }

    public function steps()
    {
        return $this->hasMany('App\Models\ProcessSteps')->where('status', '<>', 3);
    }

    public function outputs()
    {
        return $this->hasMany('App\Models\Output');
    }

    public function risks()
    {
        return $this->hasMany('App\Models\Risk');
    }

    public function opportunities()
    {
        return $this->hasMany('App\Models\Opportunity');
    }

    public function resources()
    {
        return $this->hasMany('App\Models\Resources');
    }

    public function roles()
    {
        return $this->hasMany('App\Models\Roles');
    }

    public function responsibleEveryone()
    {
        return $this->hasMany('App\Models\Roles')->where('role_id', 1)->with('everyone');
    }

    public function responsibleRoles()
    {
        return $this->hasMany('App\Models\Roles')->where('role_id', 1)->groupBy('position_id');
    }

    public function audits()
    {
        return $this->hasMany('App\Models\ProcessToAudit');
    }

    public function context()
    {
        return $this->hasMany('App\Models\ContextAnalysis');
    }

    public function getParentAttribute($value)
    {
        $name = \App\Models\Process::where('id', $this->parent_id);
        if ($name->count() > 0) {
            return "(" . $name->first()->name . ")";
        } else {
            return "";
        }
    }

    public function getChildrenAttribute($value)
    {
        $children = \App\Models\Process::where('parent_id', $this->id);
        if ($children->count() > 0) {
            return $children->get();
        } else {
            return [];
        }
    }

    public function racimap()
    {
        $roles = \App\Models\Roles::where('process_id', $this->id)
            ->where('status', 1)
            ->select(
                'position_id',
                \DB::raw('GROUP_CONCAT(id SEPARATOR "|") AS id'),
                \DB::raw('GROUP_CONCAT(role_id SEPARATOR "|") AS role_id'),
                \DB::raw('(select name from position where id = position_id) as position_name')
            )
            ->where('status', '<>', 3)
            ->groupBy('position_id');
        if ($roles->count() > 0) {
            return $roles->get();
        } else {
            return "";
        }
    }

    public function log()
    {
        return $this->hasMany('App\Models\RiskOpportunityActionLog', 'process_id', 'id');
    }

    public function planmaster()
    {
        return $this->hasMany('App\Models\AuditPlanMaster', 'process_to_audit', 'id');
    }

    public function executions()
    {
        return $this->hasMany('App\Models\AuditExecution', 'process_id', 'id');
    }

    public function branches()
    {
        return $this->hasMany('App\Models\ProcessBranch', 'process_id', 'id');
    }

    public function getParentProcessAttribute()
    {
        if ($this->parent_id != 0) {
            return \App\Models\Process::find($this->parent_id);
        } else {
            return null;
        }
    }

    protected $appends = ['parent', 'children', 'parent_process'];
}
