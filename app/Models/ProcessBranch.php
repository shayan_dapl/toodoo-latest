<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessBranch extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "process_branches";

    public function process()
    {
        return $this->belongsTo('App\Models\Process', 'process_id', 'id')->where('status', 1);
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id', 'id');
    }
}
