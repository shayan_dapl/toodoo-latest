<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessCategory extends Model
{
    protected $table = "process_category";

    public function processes()
    {
        return $this->hasMany('App\Models\Process', 'category_id', 'id');
    }
}
