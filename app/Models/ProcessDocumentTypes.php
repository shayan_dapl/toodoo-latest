<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessDocumentTypes extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "process_document_types";

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function active()
    {
        return \App\Models\Procedures::where('doc_type_id', $this->id)->where('status', 1)->count();
    }

    public function documentdocs()
    {
        return $this->hasMany('App\Models\Procedures', 'doc_type_id', 'id')->with('process')->where('type', 'P')->where('status', 1)->where('is_archived', null)->groupBy('doc_ref');
    }
}
