<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessKpiData extends Model
{
    use \App\Traits\CompanyCheckTraits;

    protected $table = "process_kpi_data";

    public function processkpi()
    {
        return $this->belongsTo('App\Models\Kpi', 'kpi_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format(\Config::get('settings.dashed_date'));
    }
}
