<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessRegulationTypes extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "process_regulation_types";

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function active()
    {
        return \App\Models\Procedures::where('doc_type_id', $this->id)->where('status', 1)->count();
    }

    public function regulationdocs()
    {
        return $this->hasMany('App\Models\Procedures', 'doc_type_id', 'id')->with('process')->where('type', 'R')->where('status', '<>', 3)->where('is_archived', null)->groupBy('doc_ref');
    }
}
