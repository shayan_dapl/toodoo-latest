<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessSteps extends Model
{
    protected $table = "process_steps";

    public function process()
    {
        return $this->belongsTo('App\Models\Process', 'process_id', 'id');
    }

    public function roles()
    {
        return $this->hasMany('App\Models\Roles', 'process_step_id', 'id');
    }
}
