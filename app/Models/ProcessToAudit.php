<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessToAudit extends Model
{
    protected $table = "process_to_audit";

    public function processDetail()
    {
        return $this->belongsTo('App\Models\Process', 'process_id', 'id');
    }
}
