<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegulationDocs extends Model
{
    protected $table = "regulation_docs";

    public function regulation()
    {
        return $this->belongsTo('App\Models\Procedures');
    }
}
