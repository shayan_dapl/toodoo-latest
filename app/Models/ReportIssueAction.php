<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportIssueAction extends Model
{
    use \App\Traits\ReportIssueActionTraits;
    
    protected $table = "report_issue_action";

    public function setDeadlineAttribute($value)
    {
        $this->attributes['deadline'] = date("Y-m-d", strtotime($value));
    }

    public function getDeadlineAttribute($value)
    {
        return date("m/d/Y", strtotime($value));
    }

    public function execution()
    {
        return $this->belongsTo('App\Models\AuditExecution', 'ref_id', 'id');
    }

    public function delegatemembers()
    {
        return $this->hasMany('App\Models\ActionDelegateMember', 'action_id', 'id');
    }
}
