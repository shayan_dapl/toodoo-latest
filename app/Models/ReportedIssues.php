<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportedIssues extends Model
{
    use \App\Traits\CompanyCheckTraits;
    
    protected $table = "reported_issues";

    public function linkedprocess()
    {
        return $this->hasMany('App\Models\Process', 'id', 'process');
    }

    public function source()
    {
        return $this->belongsTo('App\Models\SourceTypes', 'source_type', 'id');
    }

    public function process()
    {
        return $this->belongsTo('App\Models\Process', 'process', 'id');
    }

    public function details()
    {
        return $this->hasMany('App\Models\ReportedIssuesDetails', 'reported_issue_id', 'id');
    }

    public function suppliername()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier', 'id');
    }
}
