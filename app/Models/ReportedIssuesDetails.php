<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportedIssuesDetails extends Model
{
    protected $table = "reported_issues_details";

    public function setDeadlineAttribute($value)
    {
        $this->attributes['deadline'] = date("Y-m-d", strtotime($value));
    }

    public function issues()
    {
        return $this->belongsTo('App\Models\ReportedIssues', 'reported_issue_id', 'id');
    }

    public function relatedProcess()
    {
        return $this->belongsTo('App\Models\Process', 'related_process', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users', 'who', 'id');
    }

    public function reportedBy()
    {
        return $this->belongsTo('App\Models\Users', 'created_by', 'id');
    }

    public function fixing()
    {
        return $this->hasMany('App\Models\ExternalIssueFixing', 'issue_detail_id', 'id');
    }
}
