<?php 

namespace App\Models;

use App\Models\RiskOpportunityActionFiles;
use Illuminate\Database\Eloquent\Model;

class ReportingSourceUserAccess extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "reporting_source_user_access";

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users', 'user_id', 'id');
    }
}
