<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceActivity extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "resource_activity";

    public function getEncodedIdAttribute()
    {
        return base64_encode($this->id);
    }

    protected $appends = ['encoded_id'];
}
