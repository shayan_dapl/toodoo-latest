<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceDelegationActionComments extends Model
{
    protected $table = "resource_delegation_action_comments";

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function log()
    {
        return $this->belongsTo('App\Models\ResourceDelegationActionLog');
    }

    public function files()
    {
        return $this->hasMany('App\Models\ResourceDelegationActionFiles', 'comment_id', 'id');
    }
}
