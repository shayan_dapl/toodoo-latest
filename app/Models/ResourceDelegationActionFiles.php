<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceDelegationActionFiles extends Model
{
    protected $table = "resource_delegation_action_files";
}
