<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceDelegationActionLog extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "resource_delegation_action_log";

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function resource()
    {
        return $this->belongsTo('App\Models\Resources', 'resource_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users', 'delegate_to', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\ResourceDelegationActionComments', 'log_id', 'id');
    }

    public function relatedactivity()
    {
        return $this->belongsTo('App\Models\ResourceWiseActivity', 'activity_id', 'id');
    }

    public function getDeadlineAttribute($value)
    {
        return date(\Config::get('settings.dashed_date'), strtotime($value));
    }
}
