<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceWiseActivity extends Model
{
    protected $table = "resource_wise_activity";
    use \App\Traits\CompanyCheckTraits;

    public function activity()
    {
        return $this->belongsTo('App\Models\ResourceActivity', 'activity_id', 'id');
    }

    public function owner()
    {
        return $this->belongsTo('App\Models\Users', 'process_owner', 'id');
    }
    
    public function resource()
    {
        return $this->belongsTo('App\Models\Resources', 'resource_id', 'id');
    }

    public function delegations()
    {
        return $this->hasMany('App\Models\ResourceDelegationActionLog', 'activity_id', 'id');
    }

    public function getStartDateAttribute($value)
    {
        return date(\Config::get('settings.dashed_date'), strtotime($value));
    }
}
