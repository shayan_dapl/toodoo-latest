<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resources extends Model
{
    protected $table = "resources";
    use \App\Traits\CompanyCheckTraits;
    
    public function process()
    {
        return $this->belongsTo('App\Models\Process');
    }

    public function docs()
    {
        return $this->hasMany('App\Models\ResourcesDocs', 'resource_id', 'id');
    }

    public function activities()
    {
        return $this->hasMany('App\Models\ResourceWiseActivity', 'resource_id', 'id');
    }

    public function resourceType()
    {
        return $this->belongsTo('App\Models\ResourcesType', 'resource_id', 'id');
    }

    public function delegations()
    {
        return $this->hasMany('App\Models\ResourceDelegationActionLog', 'resource_id', 'id');
    }

    public function getEncodedBrandAttribute()
    {
        return base64_encode($this->brand);
    }

    protected $appends = ['encoded_brand'];
}
