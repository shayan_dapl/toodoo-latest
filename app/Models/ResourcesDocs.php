<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourcesDocs extends Model
{
    protected $table = "resources_docs";

    public function resource()
    {
        return $this->belongsTo('App\Models\Resources');
    }
}
