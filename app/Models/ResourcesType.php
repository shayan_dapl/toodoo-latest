<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourcesType extends Model
{
    use \App\Traits\CompanyCheckTraits;

    protected $table = "resources_type";

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function resources()
    {
        return $this->hasMany('App\Models\Resources', 'resource_id', 'id');
    }

    public function getEncodedIdAttribute()
    {
        return base64_encode($this->id);
    }

    protected $appends = ['encoded_id'];
}
