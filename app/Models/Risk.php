<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Risk extends Model
{
    protected $table = "risk";

    public function process()
    {
        return $this->belongsTo('App\Models\Process');
    }

    public function getRiskactionAttribute()
    {
        $data = RiskOpportunityActionLog::where('type', 'risk')->where('risk_opp_id', $this->id)->latest()->first();
        if (!empty($data)) {
            return $data;
        } else {
            return [];
        }
    }

    protected $appends = ['riskaction'];
}
