<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiskOpportunityAction extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $fillable = ['company_id'];
    protected $table = "risk_opportunity_action";

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }
}
