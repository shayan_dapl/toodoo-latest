<?php 

namespace App\Models;

use Config;

use App\Models\RiskOpportunityActionFiles;
use Illuminate\Database\Eloquent\Model;

class RiskOpportunityActionComments extends Model
{
    use \App\Traits\RiskOpportunityActionLogTraits;
    protected $table = "risk_opportunity_action_comments";

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function process()
    {
        return $this->belongsTo('App\Models\Process', 'process_id', 'id');
    }

    public function getCreatedAtAttribute()
    {
        return date(Config::get('settings.slashed_date'));
    }

    public function files()
    {
        $data = RiskOpportunityActionFiles::where('comment_id', $this->id);
        if ($data->count() > 0) {
            return $data->get();
        } else {
            return [];
        }
    }

    protected $appends = ['files'];
}
