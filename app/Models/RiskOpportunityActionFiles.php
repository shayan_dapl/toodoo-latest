<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiskOpportunityActionFiles extends Model
{
    protected $table = "risk_opportunity_action_files";

    public function action()
    {
        return $this->belongsTo('App\Models\RiskOpportunityActionComments');
    }
}
