<?php 

namespace App\Models;

use App\Models\RiskOpportunityActionComments;
use Illuminate\Database\Eloquent\Model;

class RiskOpportunityActionLog extends Model
{
    use \App\Traits\CompanyCheckTraits, \App\Traits\RiskOpportunityActionLogTraits;
    
    protected $table = "risk_opportunity_action_log";

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function process()
    {
        return $this->belongsTo('App\Models\Process', 'process_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users', 'delegate_to', 'id');
    }

    public function setDeadlineAttribute($value)
    {
        $this->attributes['deadline'] = date("Y-m-d", strtotime($value));
    }

    public function comments($type)
    {
        $data = RiskOpportunityActionComments::where('type', $type)->where('risk_opp_id', $this->id)->orderBy('id', 'DESC');
        if ($data->count() > 0) {
            return $data->get();
        } else {
            return [];
        }
    }

    protected $appends = ['comments'];
}
