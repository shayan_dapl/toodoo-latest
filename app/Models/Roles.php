<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = "roles";

    public function process()
    {
        return $this->belongsTo('App\Models\Process', 'process_id', 'id');
    }

    public function steps()
    {
        return $this->belongsTo('App\Models\ProcessSteps', 'process_step_id', 'id');
    }

    public function position()
    {
        return $this->belongsTo('App\Models\Position', 'position_id', 'id');
    }

    public function everyone()
    {
        return $this->belongsTo('App\Models\Position', 'position_id', 'id')->where('status', 4);
    }

    public function structure()
    {
        return $this->belongsTo('App\Models\OrganizationStructure', 'position_id', 'position_id');
    }
}
