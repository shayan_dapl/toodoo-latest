<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scope extends Model
{
    protected $table = "scope";

    public function process()
    {
        return $this->belongsTo('App\Models\Process');
    }
}
