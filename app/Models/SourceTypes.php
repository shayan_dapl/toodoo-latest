<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SourceTypes extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "source_type";

    public function assignedusers()
    {
        return $this->hasMany('App\Models\ReportingSourceUserAccess', 'source_type_id', 'id');
    }
}
