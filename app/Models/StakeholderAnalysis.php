<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StakeholderAnalysis extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "stakeholder_analysis";
    protected $fillable = ['category'];

    public function category()
    {
        return $this->belongsTo('App\Models\StakeholderCategory', 'category_id', 'id');
    }

    public function branches()
    {
        return $this->hasMany('App\Models\StakeholderBranch', 'stakeholder_id', 'id');
    }

    public function brancheids($branchId = null, $stakeholderId)
    {
        return \App\Models\StakeholderBranch::where('branch_id', $branchId)->where('stakeholder_id', $stakeholderId)->count();
    }
}
