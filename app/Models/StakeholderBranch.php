<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StakeholderBranch extends Model
{
    protected $table = "stakeholder_branches";
}
