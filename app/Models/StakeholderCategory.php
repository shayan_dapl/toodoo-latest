<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StakeholderCategory extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = 'stakeholder_category';

    public function analysis()
    {
        return $this->hasMany('App\Models\StakeholderAnalysis', 'category_id', 'id');
    }
}
