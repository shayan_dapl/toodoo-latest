<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionCancellationLog extends Model
{
    protected $table = "subscription_cancellation_log";

    protected $fillable = ['company_id'];
}
