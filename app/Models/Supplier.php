<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = 'supplier';

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function action()
    {
        return $this->hasMany('App\Models\SupplierEvaluationActionLog', 'supplier_id', 'id')->with('rates')->orderBy('id', 'DESC')->first();
    }
}
