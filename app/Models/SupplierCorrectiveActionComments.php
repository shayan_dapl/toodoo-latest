<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierCorrectiveActionComments extends Model
{
    protected $table = "supplier_corrective_action_comments";

    public function log()
    {
        return $this->belongsTo('App\Models\SupplierCorrectiveActionLog');
    }

    public function files()
    {
        return $this->hasMany('App\Models\SupplierCorrectiveActionFiles', 'comment_id', 'id');
    }
}
