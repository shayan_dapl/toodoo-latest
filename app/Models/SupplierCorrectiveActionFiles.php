<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierCorrectiveActionFiles extends Model
{
    protected $table = "supplier_corrective_action_files";
}
