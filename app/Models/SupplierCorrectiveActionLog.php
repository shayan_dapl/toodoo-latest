<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use App\Models\Users;

class SupplierCorrectiveActionLog extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table="supplier_corrective_action_log";

    public function setDeadlineAttribute($value)
    {
        $this->attributes['deadline'] = date('Y-m-d', strtotime($value));
    }

    public function comments()
    {
        return $this->hasMany('App\Models\SupplierCorrectiveActionComments', 'log_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users', 'delegate_to', 'id');
    }

    public function getDeligatedUserAttribute()
    {
        return !empty($this->delegate_to) ? Users::find($this->delegate_to)->name : '';
    }

    public function getDeadlineAttribute($value)
    {
        return ($value !== '1970-01-01') ? date(Config::get('settings.dashed_date'), strtotime($value)) : '';
    }

    public function getFilesAttribute()
    {
        $files = [];
        if ($this->comments->count() > 0) {
            foreach ($this->comments as $eachComment) {
                if ($eachComment->files->count() > 0) {
                    foreach ($eachComment->files as $eachFile) {
                        $extension = extension($eachFile->doc_name);
                        $files[] = getfileType($extension, $eachFile->doc_name, 'supplier_corrective_action_docs');
                    }
                }
            }
        }
        return $files;
    }

    protected $appends = ['deligated_user', 'files'];
}
