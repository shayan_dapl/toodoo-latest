<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierEvaluationActionComments extends Model
{
    protected $table = 'supplier_evaluation_action_comments';

    public function log()
    {
        return $this->belongsTo('App\Models\SupplierEvaluationActionLog');
    }

    public function files()
    {
        return $this->hasMany('App\Models\SupplierEvaluationActionFiles', 'comment_id', 'id');
    }
}
