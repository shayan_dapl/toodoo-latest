<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierEvaluationActionFiles extends Model
{
    protected $table = 'supplier_evaluation_action_files';
}
