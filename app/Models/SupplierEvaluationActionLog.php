<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierEvaluationActionLog extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = 'supplier_evaluation_action_log';

    public function setDeadlineAttribute($value)
    {
        $this->attributes['deadline'] = date('Y-m-d', strtotime($value));
    }

    public function rates()
    {
        return $this->hasMany('App\Models\SupplierEvaluationRates', 'log_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\SupplierEvaluationActionComments', 'log_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return date(\Config::get('settings.dashed_date'), strtotime($value));
    }

    public function getDeadlineAttribute($value)
    {
        return date(\Config::get('settings.dashed_date'), strtotime($value));
    }

    public function getFilesAttribute()
    {
        $files = [];
        if ($this->comments->count() > 0) {
            foreach ($this->comments as $eachComment) {
                if ($eachComment->files->count() > 0) {
                    foreach ($eachComment->files as $eachFile) {
                        $extension = extension($eachFile->doc_name);
                        $files[] = getfileType($extension, $eachFile->doc_name, 'supplier_rating_action_docs');
                    }
                }
            }
        }
        return $files;
    }

    public function getUserAttribute()
    {
        if (!empty($this->delegate_to)) {
            return \App\Models\Users::companyCheck()->where('id', $this->delegate_to)->first()->name;
        } else {
            return null;
        }
    }

    public function correctives()
    {
        return $this->hasOne('App\Models\SupplierCorrectiveActionLog', 'log_id', 'id');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id', 'id');
    }

    protected $appends = ['files', 'user'];
}
