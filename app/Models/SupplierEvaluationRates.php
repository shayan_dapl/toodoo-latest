<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierEvaluationRates extends Model
{
    protected $table = 'supplier_evaluation_rates';

    public function parameter()
    {
        return $this->belongsTo('App\Models\SupplierParameter', 'parameter_id', 'id');
    }

    public function rate()
    {
        return $this->belongsTo('App\Models\SupplierRating', 'rating_id', 'id');
    }

    public function getParameterNameAttribute()
    {
        return \App\Models\SupplierParameter::companyStatusCheck()->where('id', $this->parameter_id)->first()->name;
    }

    public function getRatingNameAttribute()
    {
        return \App\Models\SupplierRating::companyStatusCheck()->where('id', $this->rating_id)->first()->description;
    }

    protected $appends = ['parameter_name', 'rating_name'];
}
