<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierParameter extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = 'supplier_parameter';

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function rates()
    {
        return $this->hasMany('App\Models\SupplierEvaluationRates', 'parameter_id', 'id');
    }
}
