<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierRating extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = 'supplier_rating';

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function rates()
    {
        return $this->hasMany('App\Models\SupplierEvaluationRates', 'rating_id', 'id');
    }
}
