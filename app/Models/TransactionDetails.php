<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionDetails extends Model
{
    use \App\Traits\CompanyCheckTraits;
    protected $table = "transaction_details";

    public function purchased()
    {
        return $this->hasMany('App\Models\PackagePurchaseMaster', 'transaction_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function invoicedata()
    {
        return $this->belongsTo('App\Models\InvoiceData', 'id', 'transaction_id');
    }
}
