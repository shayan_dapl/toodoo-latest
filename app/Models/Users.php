<?php 

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Config;
use App\Traits\UsersTraits;
use App\Traits\CompanyCheckTraits;
use Auth;

class Users extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable, UsersTraits, CompanyCheckTraits;

    protected $table = "user_master";
    protected $primaryKey = 'id';
    protected $fillable = ['email', 'password'];
    protected $hidden = ['password'];

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function getTokenIdAttribute($value)
    {
        return base64_decode($value);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format(Config::get('settings.dashed_date'));
    }

    public function getLogoAttribute($value)
    {
        $logo = \App\Models\Company::where('id', $this->company_id)->first();
        return $logo->photo;
    }

    public function getAuditPreperationAttribute($value)
    {
        return \App\Models\AuditPlanMaster::where('choose_lead_auditor', $this->id)->where('status', 1)->where('plan_date', null)->count();
    }

    public function getOutOfServiceAttribute($value)
    {
        if ($value == null) {
            return "";
        } else {
            return \Carbon\Carbon::parse($value)->format(Config::get('settings.slashed_date'));
        }
    }

    public function out()
    {
        if ($this->out_of_service == null) {
            return "";
        } else {
            return str_replace("/", "-", $this->out_of_service);
        }
    }

    public function setOutOfServiceAttribute($value)
    {
        if (!empty(explode("/", $value)[2]) && !empty(explode("/", $value)[1]) && !empty(explode("/", $value)[0])) {
            $val = explode("/", $value)[2] . "-" . explode("/", $value)[1] . "-" . explode("/", $value)[0];
            $this->attributes['out_of_service'] = $val;
        } else {
            $this->attributes['out_of_service'] = null;
        }
    }

    public function positions()
    {
        return \App\Models\OrganizationStructure::where('user_id', $this->id)->where('special_user', 0)->get();
    }

    public function specialPositions()
    {
        return \App\Models\OrganizationStructure::where('user_id', $this->id)->where('special_user', 1)->first();
    }

    public function processes()
    {
        return \App\Models\Process::where('owner_id', $this->id)->where('status', 1)->get();
    }

    public function plannedAudits()
    {
        return \App\Models\AuditPlanMaster::where('choose_lead_auditor', $this->id)->get();
    }

    public function otherPOwners($auditId, $userId = null)
    {
        $auditDetails = \App\Models\AuditPlanMaster::find($auditId);
        $process = $auditDetails->process;
        $ownerIds = [];
        if ($process->count() > 0) {
            foreach ($process as $each) {
                $ownerIds[] = $each->processDetail->owner_id;
            }
        }
        return Users::companyStatusCheck()->inServiceWithEmail()->whereNotIn('id', $ownerIds)->where([['id', '<>', $userId], ['is_auditor', 1]])->get();
    }

    public function auditMember()
    {
        return $this->hasMany('App\Models\AuditPlanAuditors', 'auditor_id', 'id');
    }

    public function auditees()
    {
        return \App\Models\AuditPlanAuditees::where('user_id', $this->id)->get();
    }

    public function externalIssues()
    {
        return \App\Models\ReportedIssuesDetails::where('who', $this->id)->get();
    }

    public function delegateMember()
    {
        return \App\Models\ActionDelegateMember::where('member', $this->id)->get();
    }

    public function competentAction()
    {
        return \App\Models\CompetentRatingsActionLog::where('delegate_to', $this->id)->get();
    }

    public function topPosition()
    {
        $topPositions = [];
        $positions = \App\Models\OrganizationStructure::where('user_id', $this->id);
        if ($positions->count() > 0) {
            foreach ($positions->get() as $each) {
                $parent = \App\Models\Position::find($each->position_id);
                if (!empty($parent) && $parent->parent_id == null) {
                    $topPositions[] = $parent->id;
                }
            }
        }
        return \App\Models\OrganizationStructure::whereIn('position_id', $topPositions)->get();
    }

    public function userroles()
    {
        $roles = [];
        if ($this->type == 2) {
            $roles [] = 'admin';
        }
        if ($this->is_auditor == 1) {
            $roles [] = 'auditor';
        }
        if ($this->can_process_owner == 1) {
            $roles [] = 'processowner';
        }
        return $roles = implode(',', $roles);
    }

    public function customerId()
    {
        return $this->hasOne('App\Models\PaymentCustomerid', 'user_id', 'id');
    }

    public function getEncodedIdAttribute()
    {
        return base64_encode($this->id);
    }

    protected $appends = ['logo', 'audit_preperation', 'encoded_id'];
}
