<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoCategory extends Model
{
    protected $table = "video_category";
    public $timestamps = false;

    public function contents()
    {
        return $this->hasMany('App\Models\VideoMaster', 'category_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\VideoCategory', 'parent_id', 'id')->where('status', 1);
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\VideoCategory', 'parent_id', 'id');
    }
}
