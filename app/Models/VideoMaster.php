<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoMaster extends Model
{
    protected $table = "video_master";

    public function category()
    {
        return $this->belongsTo('App\Models\VideoCategory', 'category_id', 'id');
    }
}
