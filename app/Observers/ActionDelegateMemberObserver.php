<?php

namespace App\Observers;

use App\Models\ActionDelegateMember;
use Auth;

class ActionDelegateMemberObserver
{
    public function created(ActionDelegateMember $actdelmember)
    {
        $actdelmember->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ActionDelegateMember $actdelmember)
    {
        $actdelmember->updated_at = date('Y-m-d H:i:s');
    }
}
