<?php

namespace App\Observers;

use App\Models\ActionMatrix;
use Auth;

class ActionMatrixObserver
{
    public function created(ActionMatrix $actmatdt)
    {
        $actmatdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ActionMatrix $actmatdt)
    {
        $actmatdt->updated_at = date('Y-m-d H:i:s');
    }
}
