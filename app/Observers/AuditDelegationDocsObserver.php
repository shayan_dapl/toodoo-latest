<?php

namespace App\Observers;

use App\Models\AuditDelegationDocs;
use Auth;

class AuditDelegationDocsObserver
{
    public function created(AuditDelegationDocs $adtdeldcsdt)
    {
        $adtdeldcsdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(AuditDelegationDocs $adtdeldcsdt)
    {
        $adtdeldcsdt->updated_at = date('Y-m-d H:i:s');
    }
}
