<?php

namespace App\Observers;

use App\Models\AuditExecutionDocs;
use Auth;

class AuditExecutionDocsObserver
{
    public function created(AuditExecutionDocs $adtexedoc)
    {
        $adtexedoc->created_at = date('Y-m-d H:i:s');
    }

    public function updated(AuditExecutionDocs $adtexedoc)
    {
        $adtexedoc->updated_at = date('Y-m-d H:i:s');
    }
}
