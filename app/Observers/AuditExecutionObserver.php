<?php

namespace App\Observers;

use App\Models\AuditExecution;
use Auth;

class AuditExecutionObserver
{
    public function created(AuditExecution $adtexedt)
    {
        $adtexedt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(AuditExecution $adtexedt)
    {
        $adtexedt->updated_at = date('Y-m-d H:i:s');
    }
}
