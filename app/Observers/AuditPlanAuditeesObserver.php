<?php

namespace App\Observers;

use App\Models\AuditPlanAuditees;
use Auth;

class AuditPlanAuditeesObserver
{
    public function created(AuditPlanAuditees $apadteedt)
    {
        $apadteedt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(AuditPlanAuditees $apadteedt)
    {
        $apadteedt->updated_at = date('Y-m-d H:i:s');
    }
}
