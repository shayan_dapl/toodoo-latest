<?php

namespace App\Observers;

use App\Models\AuditPlanAuditors;
use Auth;

class AuditPlanAuditorsObserver
{
    public function created(AuditPlanAuditors $apaudtrdt)
    {
        $apaudtrdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(AuditPlanAuditors $apaudtrdt)
    {
        $apaudtrdt->updated_at = date('Y-m-d H:i:s');
    }
}
