<?php

namespace App\Observers;

use App\Models\AuditPlanMaster;
use Auth;

class AuditPlanMasterObserver
{
    public function created(AuditPlanMaster $apmdt)
    {
        $apmdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(AuditPlanMaster $apmdt)
    {
        $apmdt->updated_at = date('Y-m-d H:i:s');
    }
}
