<?php

namespace App\Observers;

use App\Models\AuditTypeMaster;
use Auth;

class AuditTypeMasterObserver
{
    public function created(AuditTypeMaster $adttypdt)
    {
        $adttypdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(AuditTypeMaster $adttypdt)
    {
        $adttypdt->updated_at = date('Y-m-d H:i:s');
    }
}
