<?php

namespace App\Observers;

use App\Models\Branch;
use Auth;

class BranchObserver
{
    public function created(Branch $brdt)
    {
        $brdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Branch $brdt)
    {
        $brdt->updated_at = date('Y-m-d H:i:s');
    }
}
