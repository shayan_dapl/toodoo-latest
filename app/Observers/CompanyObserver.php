<?php

namespace App\Observers;

use App\Models\Company;
use Event;
use App\Events\Company\DemoData;

class CompanyObserver
{
    public function created(Company $company)
    {
        $company->created_at = date('Y-m-d H:i:s');
        Event::fire(new DemoData($company->id));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(Company $company)
    {
        $company->updated_at = date('Y-m-d H:i:s');
    }
}
