<?php

namespace App\Observers;

use App\Models\CompanySettings;
use Auth;

class CompanySettingsObserver
{
    public function created(CompanySettings $cmpstdt)
    {
        $cmpstdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(CompanySettings $cmpstdt)
    {
        $cmpstdt->updated_at = date('Y-m-d H:i:s');
    }
}
