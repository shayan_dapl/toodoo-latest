<?php

namespace App\Observers;

use App\Models\ContextAnalysis;
use Auth;

class ContextAnalysisObserver
{
    public function created(ContextAnalysis $contxtandt)
    {
        $contxtandt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ContextAnalysis $contxtandt)
    {
        $contxtandt->updated_at = date('Y-m-d H:i:s');
    }
}
