<?php

namespace App\Observers;

use App\Models\ContextStakeholderActionComments;
use Auth;

class ContextStakeholderActionCommentsObserver
{
    public function created(ContextStakeholderActionComments $constkactcmnt)
    {
        $constkactcmnt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ContextStakeholderActionComments $constkactcmnt)
    {
        $constkactcmnt->updated_at = date('Y-m-d H:i:s');
    }
}
