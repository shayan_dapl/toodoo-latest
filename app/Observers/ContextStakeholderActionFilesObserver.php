<?php

namespace App\Observers;

use App\Models\ContextStakeholderActionFiles;
use Auth;

class ContextStakeholderActionFilesObserver
{
    public function created(ContextStakeholderActionFiles $constkactfiledt)
    {
        $constkactfiledt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ContextStakeholderActionFiles $constkactfiledt)
    {
        $constkactfiledt->updated_at = date('Y-m-d H:i:s');
    }
}
