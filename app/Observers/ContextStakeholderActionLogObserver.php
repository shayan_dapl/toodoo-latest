<?php

namespace App\Observers;

use App\Models\ContextStakeholderActionLog;
use Auth;

class ContextStakeholderActionLogObserver
{
    public function created(ContextStakeholderActionLog $contstkdt)
    {
        $contstkdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ContextStakeholderActionLog $contstkdt)
    {
        $contstkdt->updated_at = date('Y-m-d H:i:s');
    }
}
