<?php

namespace App\Observers;

use App\Models\ContextSubjects;
use Auth;

class ContextSubjectsObserver
{
    public function created(ContextSubjects $consubdt)
    {
        $consubdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ContextSubjects $consubdt)
    {
        $consubdt->updated_at = date('Y-m-d H:i:s');
    }
}
