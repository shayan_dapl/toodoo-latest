<?php

namespace App\Observers;

use App\Models\Countries;
use Auth;

class CountriesObserver
{
    public function created(Countries $cntDt)
    {
        $cntDt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Countries $cntDt)
    {
        $cntDt->updated_at = date('Y-m-d H:i:s');
    }
}
