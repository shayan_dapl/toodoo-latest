<?php

namespace App\Observers;

use App\Models\Department;
use Auth;

class DepartmentObserver
{
    public function created(Department $deptdt)
    {
        $deptdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Department $deptdt)
    {
        $deptdt->updated_at = date('Y-m-d H:i:s');
    }
}
