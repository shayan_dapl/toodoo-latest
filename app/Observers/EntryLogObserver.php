<?php

namespace App\Observers;

use App\Models\EntryLog;
use Auth;

class EntryLogObserver
{
    public function created(EntryLog $entlogdt)
    {
        $entlogdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(EntryLog $entlogdt)
    {
        $entlogdt->updated_at = date('Y-m-d H:i:s');
    }
}
