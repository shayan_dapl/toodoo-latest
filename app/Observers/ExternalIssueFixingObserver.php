<?php

namespace App\Observers;

use App\Models\ExternalIssueFixing;
use Auth;

class ExternalIssueFixingObserver
{
    public function created(ExternalIssueFixing $extissfnddt)
    {
        $extissfnddt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ExternalIssueFixing $extissfnddt)
    {
        $extissfnddt->updated_at = date('Y-m-d H:i:s');
    }
}
