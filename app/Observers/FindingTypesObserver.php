<?php

namespace App\Observers;

use App\Models\FindingTypes;
use Auth;

class FindingTypesObserver
{
    public function created(FindingTypes $ftdt)
    {
        $ftdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(FindingTypes $ftdt)
    {
        $ftdt->updated_at = date('Y-m-d H:i:s');
    }
}
