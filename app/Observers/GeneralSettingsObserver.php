<?php

namespace App\Observers;

use App\Models\GeneralSettings;
use Auth;

class GeneralSettingsObserver
{
    public function created(GeneralSettings $gsdt)
    {
        $gsdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(GeneralSettings $gsdt)
    {
        $gsdt->updated_at = date('Y-m-d H:i:s');
    }
}
