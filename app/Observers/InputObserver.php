<?php

namespace App\Observers;

use App\Models\Input;
use Auth;

class InputObserver
{
    public function created(Input $inptdt)
    {
        $inptdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Input $inptdt)
    {
        $inptdt->updated_at = date('Y-m-d H:i:s');
    }
}
