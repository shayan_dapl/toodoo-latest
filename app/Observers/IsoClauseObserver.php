<?php

namespace App\Observers;

use App\Models\IsoClause;
use Auth;

class IsoClauseObserver
{
    public function created(IsoClause $isocldt)
    {
        $isocldt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(IsoClause $isocldt)
    {
        $isocldt->updated_at = date('Y-m-d H:i:s');
    }
}
