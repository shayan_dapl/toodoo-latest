<?php

namespace App\Observers;

use App\Models\Kpi;
use Auth;

class KpiObserver
{
    public function created(Kpi $kpidt)
    {
        $kpidt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Kpi $kpidt)
    {
        $kpidt->updated_at = date('Y-m-d H:i:s');
    }
}
