<?php

namespace App\Observers;

use App\Models\Languages;
use Auth;

class LanguagesObserver
{
    public function created(Languages $langdt)
    {
        $langdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Languages $langdt)
    {
        $langdt->updated_at = date('Y-m-d H:i:s');
    }
}
