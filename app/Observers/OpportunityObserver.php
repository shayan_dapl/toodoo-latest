<?php

namespace App\Observers;

use App\Models\Opportunity;
use Auth;

class OpportunityObserver
{
    public function created(Opportunity $oppdt)
    {
        $oppdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Opportunity $oppdt)
    {
        $oppdt->updated_at = date('Y-m-d H:i:s');
    }
}
