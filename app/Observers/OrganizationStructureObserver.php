<?php

namespace App\Observers;

use App\Models\OrganizationStructure;
use Auth;

class OrganizationStructureObserver
{
    public function created(OrganizationStructure $orgstdt)
    {
        $orgstdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(OrganizationStructure $orgstdt)
    {
        $orgstdt->updated_at = date('Y-m-d H:i:s');
    }
}
