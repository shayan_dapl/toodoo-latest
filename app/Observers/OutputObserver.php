<?php

namespace App\Observers;

use App\Models\Output;
use Auth;

class OutputObserver
{
    public function created(Output $outputdt)
    {
        $outputdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Output $outputdt)
    {
        $outputdt->updated_at = date('Y-m-d H:i:s');
    }
}
