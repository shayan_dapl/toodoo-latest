<?php

namespace App\Observers;

use Auth;
use App\Models\PackageMaster;

class PackageMasterObserver
{
    public function created(PackageMaster $pkgmst)
    {
        $pkgmst->created_at = date('Y-m-d H:i:s');
    }

    public function updated(PackageMaster $pkgmst)
    {
        $pkgmst->updated_at = date('Y-m-d H:i:s');
    }
}
