<?php

namespace App\Observers;

use App\Models\PackagePurchaseMaster;
use Auth;

class PackagePurchaseMasterObserver
{
    public function created(PackagePurchaseMaster $pckpurmst)
    {
        $pckpurmst->created_at = date('Y-m-d H:i:s');
    }

    public function updated(PackagePurchaseMaster $pckpurmst)
    {
        $pckpurmst->updated_at = date('Y-m-d H:i:s');
    }
}
