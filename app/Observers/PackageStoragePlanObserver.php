<?php

namespace App\Observers;

use App\Models\PackageStoragePlan;
use Auth;

class PackageStoragePlanObserver
{
    public function created(PackageStoragePlan $pckstpl)
    {
        $pckstpl->created_at = date('Y-m-d H:i:s');
    }

    public function updated(PackageStoragePlan $pckstpl)
    {
        $pckstpl->updated_at = date('Y-m-d H:i:s');
    }
}
