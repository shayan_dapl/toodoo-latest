<?php

namespace App\Observers;

use App\Models\PackageUserPlan;
use Auth;

class PackageUserPlanObserver
{
    public function created(PackageUserPlan $pckuspl)
    {
        $pckuspl->created_at = date('Y-m-d H:i:s');
    }

    public function updated(PackageUserPlan $pckuspl)
    {
        $pckuspl->updated_at = date('Y-m-d H:i:s');
    }
}
