<?php

namespace App\Observers;

use App\Models\PasswordResets;
use Auth;

class PasswordResetsObserver
{
    public function created(PasswordResets $passreset)
    {
        $passreset->created_at = date('Y-m-d H:i:s');
    }

    public function updated(PasswordResets $passreset)
    {
        $passreset->updated_at = date('Y-m-d H:i:s');
    }
}
