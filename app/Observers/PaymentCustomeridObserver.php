<?php

namespace App\Observers;

use App\Models\PaymentCustomerid;
use Auth;

class PaymentCustomeridObserver
{
    public function created(PaymentCustomerid $pmtcust)
    {
        $pmtcust->created_at = date('Y-m-d H:i:s');
    }

    public function updated(PaymentCustomerid $pmtcust)
    {
        $pmtcust->updated_at = date('Y-m-d H:i:s');
    }
}
