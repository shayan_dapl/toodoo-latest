<?php

namespace App\Observers;

use App\Models\Position;
use Auth;

class PositionObserver
{
    public function created(Position $position)
    {
        $position->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Position $position)
    {
        $position->updated_at = date('Y-m-d H:i:s');
    }
}
