<?php

namespace App\Observers;

use App\Models\ProcedureDocs;
use Auth;

class ProcedureDocsObserver
{
    public function created(ProcedureDocs $proceddocs)
    {
        $proceddocs->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ProcedureDocs $proceddocs)
    {
        $proceddocs->updated_at = date('Y-m-d H:i:s');
    }
}
