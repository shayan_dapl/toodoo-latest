<?php

namespace App\Observers;

use App\Models\Procedures;
use Auth;

class ProceduresObserver
{
    public function created(Procedures $proced)
    {
        $proced->created_at = date('Y-m-d H:i:s');
    }
    
    public function updated(Procedures $proced)
    {
        $proced->updated_at = date('Y-m-d H:i:s');
    }
}
