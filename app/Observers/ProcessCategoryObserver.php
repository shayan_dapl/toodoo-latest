<?php

namespace App\Observers;

use App\Models\ProcessCategory;
use Auth;

class ProcessCategoryObserver
{
    public function created(ProcessCategory $procescat)
    {
        $procescat->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ProcessCategory $procescat)
    {
        $procescat->updated_at = date('Y-m-d H:i:s');
    }
}
