<?php

namespace App\Observers;

use App\Models\ProcessDocumentTypes;
use Auth;

class ProcessDocumentTypesObserver
{
    public function created(ProcessDocumentTypes $procesdoctype)
    {
        $procesdoctype->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ProcessDocumentTypes $procesdoctype)
    {
        $procesdoctype->updated_at = date('Y-m-d H:i:s');
    }
}
