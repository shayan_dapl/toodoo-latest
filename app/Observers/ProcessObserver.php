<?php

namespace App\Observers;

use App\Models\Process;
use Auth;

class ProcessObserver
{
    public function created(Process $proce)
    {
        $proce->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Process $proce)
    {
        $proce->updated_at = date('Y-m-d H:i:s');
    }
}
