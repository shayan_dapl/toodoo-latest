<?php

namespace App\Observers;

use App\Models\ProcessRegulationTypes;
use Auth;

class ProcessRegulationTypesObserver
{
    public function created(ProcessRegulationTypes $procesregtype)
    {
        $procesregtype->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ProcessRegulationTypes $procesregtype)
    {
        $procesregtype->updated_at = date('Y-m-d H:i:s');
    }
}
