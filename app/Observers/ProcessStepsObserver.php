<?php

namespace App\Observers;

use App\Models\ProcessSteps;
use Auth;

class ProcessStepsObserver
{
    public function created(ProcessSteps $procestep)
    {
        $procestep->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ProcessSteps $procestep)
    {
        $procestep->updated_at = date('Y-m-d H:i:s');
    }
}
