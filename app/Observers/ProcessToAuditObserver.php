<?php

namespace App\Observers;

use App\Models\ProcessToAudit;
use Auth;

class ProcessToAuditObserver
{
    public function created(ProcessToAudit $processtoaudit)
    {
        $processtoaudit->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ProcessToAudit $processtoaudit)
    {
        $processtoaudit->updated_at = date('Y-m-d H:i:s');
    }
}
