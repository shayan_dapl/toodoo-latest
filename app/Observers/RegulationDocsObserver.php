<?php

namespace App\Observers;

use App\Models\RegulationDocs;
use Auth;

class RegulationDocsObserver
{
    public function created(RegulationDocs $regudoc)
    {
        $regudoc->created_at = date('Y-m-d H:i:s');
    }

    public function updated(RegulationDocs $regudoc)
    {
        $regudoc->updated_at = date('Y-m-d H:i:s');
    }
}
