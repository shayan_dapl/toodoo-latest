<?php

namespace App\Observers;

use App\Models\ReportIssueAction;
use Auth;

class ReportIssueActionObserver
{
    public function created(ReportIssueAction $repissact)
    {
        $repissact->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ReportIssueAction $repissact)
    {
        $repissact->updated_at = date('Y-m-d H:i:s');
    }
}
