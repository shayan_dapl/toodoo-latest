<?php

namespace App\Observers;

use App\Models\ReportedIssuesDetails;
use Auth;

class ReportedIssuesDetailsObserver
{
    public function created(ReportedIssuesDetails $repissdt)
    {
        $repissdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ReportedIssuesDetails $repissdt)
    {
        $repissdt->updated_at = date('Y-m-d H:i:s');
    }
}
