<?php

namespace App\Observers;

use App\Models\ReportedIssues;
use Auth;

class ReportedIssuesObserver
{
    public function created(ReportedIssues $repiss)
    {
        $repiss->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ReportedIssues $repiss)
    {
        $repiss->updated_at = date('Y-m-d H:i:s');
    }
}
