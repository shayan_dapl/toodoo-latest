<?php

namespace App\Observers;

use App\Models\ReportingSourceUserAccess;
use Auth;

class ReportingSourceUserAccessObserver
{
    public function created(ReportingSourceUserAccess $repusracc)
    {
        $repusracc->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ReportingSourceUserAccess $repusracc)
    {
        $repusracc->updated_at = date('Y-m-d H:i:s');
    }
}
