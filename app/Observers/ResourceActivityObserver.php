<?php

namespace App\Observers;

use App\Models\ResourceActivity;
use Auth;

class ResourceActivityObserver
{
    public function created(ResourceActivity $resact)
    {
        $resact->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ResourceActivity $resact)
    {
        $resact->updated_at = date('Y-m-d H:i:s');
    }
}
