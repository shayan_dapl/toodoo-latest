<?php

namespace App\Observers;

use App\Models\ResourceWiseActivity;
use Auth;

class ResourceWiseActivityObserver
{
    public function created(ResourceWiseActivity $resacty)
    {
        $resacty->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ResourceWiseActivity $resacty)
    {
        $resacty->updated_at = date('Y-m-d H:i:s');
    }
}
