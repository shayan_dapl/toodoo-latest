<?php

namespace App\Observers;

use App\Models\ResourcesDocs;
use Auth;

class ResourcesDocsObserver
{
    public function created(ResourcesDocs $resdocs)
    {
        $resdocs->created_at = date('Y-m-d H:i:s');
    }

    public function updated(ResourcesDocs $resdocs)
    {
        $resdocs->updated_at = date('Y-m-d H:i:s');
    }
}
