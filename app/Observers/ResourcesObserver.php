<?php

namespace App\Observers;

use App\Models\Resources;
use Auth;

class ResourcesObserver
{
    public function created(Resources $res)
    {
        $res->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Resources $res)
    {
        $res->updated_at = date('Y-m-d H:i:s');
    }
}
