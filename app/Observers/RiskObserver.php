<?php

namespace App\Observers;

use App\Models\Risk;
use Auth;

class RiskObserver
{
    public function created(Risk $risk)
    {
        $risk->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Risk $risk)
    {
        $risk->updated_at = date('Y-m-d H:i:s');
    }
}
