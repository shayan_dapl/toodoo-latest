<?php

namespace App\Observers;

use App\Models\RiskOpportunityActionComments;
use Auth;

class RiskOpportunityActionCommentsObserver
{
    public function created(RiskOpportunityActionComments $riskoptactcmt)
    {
        $riskoptactcmt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(RiskOpportunityActionComments $riskoptactcmt)
    {
        $riskoptactcmt->updated_at = date('Y-m-d H:i:s');
    }
}
