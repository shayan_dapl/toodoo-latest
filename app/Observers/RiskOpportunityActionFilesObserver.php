<?php

namespace App\Observers;

use App\Models\RiskOpportunityActionFiles;
use Auth;

class RiskOpportunityActionFilesObserver
{
    public function created(RiskOpportunityActionFiles $riskoptactfiles)
    {
        $riskoptactfiles->created_at = date('Y-m-d H:i:s');
    }

    public function updated(RiskOpportunityActionFiles $riskoptactfiles)
    {
        $riskoptactfiles->updated_at = date('Y-m-d H:i:s');
    }
}
