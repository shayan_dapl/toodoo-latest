<?php

namespace App\Observers;

use App\Models\RiskOpportunityActionLog;
use Auth;

class RiskOpportunityActionLogObserver
{
    public function created(RiskOpportunityActionLog $riskoptlog)
    {
        $riskoptlog->created_at = date('Y-m-d H:i:s');
    }

    public function updated(RiskOpportunityActionLog $riskoptlog)
    {
        $riskoptlog->updated_at = date('Y-m-d H:i:s');
    }
}
