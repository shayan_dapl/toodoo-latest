<?php

namespace App\Observers;

use App\Models\RiskOpportunityAction;
use Auth;

class RiskOpportunityActionObserver
{
    public function created(RiskOpportunityAction $riskoptact)
    {
        $riskoptact->created_at = date('Y-m-d H:i:s');
    }

    public function updated(RiskOpportunityAction $riskoptact)
    {
        $riskoptact->updated_at = date('Y-m-d H:i:s');
    }
}
