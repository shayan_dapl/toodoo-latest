<?php

namespace App\Observers;

use App\Models\Roles;
use Auth;

class RolesObserver
{
    public function created(Roles $rolesdt)
    {
        $rolesdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Roles $rolesdt)
    {
        $rolesdt->updated_at = date('Y-m-d H:i:s');
    }
}
