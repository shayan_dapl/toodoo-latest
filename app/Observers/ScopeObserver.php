<?php

namespace App\Observers;

use App\Models\Scope;
use Auth;

class ScopeObserver
{
    public function created(Scope $scopedt)
    {
        $scopedt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Scope $scopedt)
    {
        $scopedt->updated_at = date('Y-m-d H:i:s');
    }
}
