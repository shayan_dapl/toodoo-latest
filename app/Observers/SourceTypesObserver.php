<?php

namespace App\Observers;

use App\Models\SourceTypes;
use Auth;

class SourceTypesObserver
{
    public function created(SourceTypes $sourcedt)
    {
        $sourcedt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(SourceTypes $sourcedt)
    {
        $sourcedt->updated_at = date('Y-m-d H:i:s');
    }
}
