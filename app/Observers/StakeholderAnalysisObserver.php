<?php

namespace App\Observers;

use App\Models\StakeholderAnalysis;
use Auth;

class StakeholderAnalysisObserver
{
    public function created(StakeholderAnalysis $stkanlydt)
    {
        $stkanlydt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(StakeholderAnalysis $stkanlydt)
    {
        $stkanlydt->updated_at = date('Y-m-d H:i:s');
    }
}
