<?php

namespace App\Observers;

use App\Models\StakeholderCategory;
use Auth;

class StakeholderCategoryObserver
{
    public function created(StakeholderCategory $stkcatdt)
    {
        $stkcatdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(StakeholderCategory $stkcatdt)
    {
        $stkcatdt->updated_at = date('Y-m-d H:i:s');
    }
}
