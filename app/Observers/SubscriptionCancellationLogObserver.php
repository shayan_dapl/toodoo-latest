<?php

namespace App\Observers;

use App\Models\SubscriptionCancellationLog;

class SubscriptionCancellationLogObserver
{
    public function created(SubscriptionCancellationLog $actionlog)
    {
        $actionlog->created_at = date('Y-m-d H:i:s');
    }

    public function updated(SubscriptionCancellationLog $actionlog)
    {
        $actionlog->updated_at = date('Y-m-d H:i:s');
    }
}
