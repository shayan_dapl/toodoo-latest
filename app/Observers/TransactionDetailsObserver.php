<?php

namespace App\Observers;

use App\Models\TransactionDetails;
use Auth;

class TransactionDetailsObserver
{
    public function created(TransactionDetails $trdt)
    {
        $trdt->created_at = date('Y-m-d H:i:s');
    }

    public function updated(TransactionDetails $trdt)
    {
        $trdt->updated_at = date('Y-m-d H:i:s');
    }
}
