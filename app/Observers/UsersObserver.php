<?php

namespace App\Observers;

use App\Models\Users;
use Auth;

class UsersObserver
{
    public function created(Users $user)
    {
        $user->created_at = date('Y-m-d H:i:s');
    }

    public function updated(Users $user)
    {
        $user->updated_at = date('Y-m-d H:i:s');
    }
}
