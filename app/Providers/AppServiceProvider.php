<?php 

namespace App\Providers;

use App\Models\Opportunity;
use App\Models\Position;
use App\Models\Process;
use App\Models\Risk;
use App\Models\Users;
use App\Models\ProcessToAudit;
use App\Models\ProcessSteps;
use App\Models\StakeholderCategory;
use App\Models\TransactionDetails;
use App\Models\StakeholderAnalysis;
use App\Models\ContextStakeholderActionLog;
use App\Models\SourceTypes;
use App\Models\Scope;
use App\Models\Roles;
use App\Models\RiskOpportunityActionLog;
use App\Models\RiskOpportunityActionFiles;
use App\Models\RiskOpportunityActionComments;
use App\Models\RiskOpportunityAction;
use App\Models\ResourcesDocs;
use App\Models\Resources;
use App\Models\ResourceWiseActivity;
use App\Models\ResourceActivity;
use App\Models\ReportingSourceUserAccess;
use App\Models\ReportedIssuesDetails;
use App\Models\ReportedIssues;
use App\Models\ReportIssueAction;
use App\Models\RegulationDocs;
use App\Models\ProcessRegulationTypes;
use App\Models\ProcessDocumentTypes;
use App\Models\ProcessCategory;
use App\Models\Procedures;
use App\Models\ProcedureDocs;
use App\Models\PaymentCustomerid;
use App\Models\PasswordResets;
use App\Models\PackageUserPlan;
use App\Models\PackageStoragePlan;
use App\Models\PackagePurchaseMaster;
use App\Models\PackageMaster;
use App\Models\Output;
use App\Models\OrganizationStructure;
use App\Models\Languages;
use App\Models\Kpi;
use App\Models\IsoClause;
use App\Models\Input;
use App\Models\GeneralSettings;
use App\Models\ExternalIssueFixing;
use App\Models\EntryLog;
use App\Models\Department;
use App\Models\Countries;
use App\Models\ContextSubjects;
use App\Models\ContextStakeholderActionFiles;
use App\Models\ContextStakeholderActionComments;
use App\Models\Company;
use App\Models\AuditPlanMaster;
use App\Models\AuditPlanAuditors;
use App\Models\AuditPlanAuditees;
use App\Models\AuditExecutionDocs;
use App\Models\AuditExecution;
use App\Models\AuditDelegationDocs;
use App\Models\ActionMatrix;
use App\Models\ActionDelegateMember;
use App\Models\Branch;
use App\Models\PositionBranches;
use App\Models\Supplier;
use App\Models\SupplierParameter;
use App\Models\SupplierRating;
use App\Models\FindingTypes;
use App\Models\SubscriptionCancellationLog;

use App\Repositories\Home;

use Auth;
use Illuminate\Support\ServiceProvider;
use App\Observers\UsersObserver;
use App\Observers\ProcessToAuditObserver;
use App\Observers\TransactionDetailsObserver;
use App\Observers\StakeholderCategoryObserver;
use App\Observers\StakeholderAnalysisObserver;
use App\Observers\ContextStakeholderActionLogObserver;
use App\Observers\SourceTypesObserver;
use App\Observers\ScopeObserver;
use App\Observers\RolesObserver;
use App\Observers\RiskObserver;
use App\Observers\RiskOpportunityActionLogObserver;
use App\Observers\RiskOpportunityActionFilesObserver;
use App\Observers\RiskOpportunityActionCommentsObserver;
use App\Observers\RiskOpportunityActionObserver;
use App\Observers\ResourcesDocsObserver;
use App\Observers\ResourcesObserver;
use App\Observers\ResourceWiseActivityObserver;
use App\Observers\ResourceActivityObserver;
use App\Observers\ReportingSourceUserAccessObserver;
use App\Observers\ReportedIssuesDetailsObserver;
use App\Observers\ReportedIssuesObserver;
use App\Observers\ReportIssueActionObserver;
use App\Observers\RegulationDocsObserver;
use App\Observers\ProcessStepsObserver;
use App\Observers\ProcessRegulationTypesObserver;
use App\Observers\ProcessDocumentTypesObserver;
use App\Observers\ProcessCategoryObserver;
use App\Observers\ProcessObserver;
use App\Observers\ProceduresObserver;
use App\Observers\ProcedureDocsObserver;
use App\Observers\PaymentCustomeridObserver;
use App\Observers\PasswordResetsObserver;
use App\Observers\PackageUserPlanObserver;
use App\Observers\PackageStoragePlanObserver;
use App\Observers\PackagePurchaseMasterObserver;
use App\Observers\PackageMasterObserver;
use App\Observers\OutputObserver;
use App\Observers\OrganizationStructureObserver;
use App\Observers\OpportunityObserver;
use App\Observers\LanguagesObserver;
use App\Observers\KpiObserver;
use App\Observers\IsoClauseObserver;
use App\Observers\InputObserver;
use App\Observers\GeneralSettingsObserver;
use App\Observers\ExternalIssueFixingObserver;
use App\Observers\EntryLogObserver;
use App\Observers\DepartmentObserver;
use App\Observers\CountriesObserver;
use App\Observers\ContextSubjectsObserver;
use App\Observers\ContextStakeholderActionFilesObserver;
use App\Observers\ContextStakeholderActionCommentsObserver;
use App\Observers\CompanyObserver;
use App\Observers\AuditPlanMasterObserver;
use App\Observers\AuditPlanAuditorsObserver;
use App\Observers\AuditPlanAuditeesObserver;
use App\Observers\AuditExecutionDocsObserver;
use App\Observers\AuditExecutionObserver;
use App\Observers\AuditDelegationDocsObserver;
use App\Observers\ActionMatrixObserver;
use App\Observers\ActionDelegateMemberObserver;
use App\Observers\BranchObserver;
use App\Observers\SubscriptionCancellationLogObserver;

use App\Repositories\AuditAction;
use App\Repositories\AuditPlanning;
use App\Repositories\AuditPrepare;
use App\Repositories\AuditExecution as AuditExecute;
use App\Repositories\CompetentMetrix;
use App\Repositories\Stakeholder;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Users::observe(UsersObserver::class);
        ProcessToAudit::observe(ProcessToAuditObserver::class);
        TransactionDetails::observe(TransactionDetailsObserver::class);
        StakeholderCategory::observe(StakeholderCategoryObserver::class);
        StakeholderAnalysis::observe(StakeholderAnalysisObserver::class);
        ContextStakeholderActionLog::observe(ContextStakeholderActionLogObserver::class);
        SourceTypes::observe(SourceTypesObserver::class);
        Scope::observe(ScopeObserver::class);
        Roles::observe(RolesObserver::class);
        Risk::observe(RiskObserver::class);
        RiskOpportunityActionLog::observe(RiskOpportunityActionLogObserver::class);
        RiskOpportunityActionFiles::observe(RiskOpportunityActionFilesObserver::class);
        RiskOpportunityActionComments::observe(RiskOpportunityActionCommentsObserver::class);
        RiskOpportunityAction::observe(RiskOpportunityActionObserver::class);
        ResourcesDocs::observe(ResourcesDocsObserver::class);
        Resources::observe(ResourcesObserver::class);
        ResourceWiseActivity::observe(ResourceWiseActivityObserver::class);
        ResourceActivity::observe(ResourceActivityObserver::class);
        ReportingSourceUserAccess::observe(ReportingSourceUserAccessObserver::class);
        ReportedIssuesDetails::observe(ReportedIssuesDetailsObserver::class);
        ReportedIssues::observe(ReportedIssuesObserver::class);
        ReportIssueAction::observe(ReportIssueActionObserver::class);
        RegulationDocs::observe(RegulationDocsObserver::class);
        ProcessSteps::observe(ProcessStepsObserver::class);
        ProcessRegulationTypes::observe(ProcessRegulationTypesObserver::class);
        ProcessDocumentTypes::observe(ProcessDocumentTypesObserver::class);
        Process::observe(ProcessObserver::class);
        Procedures::observe(ProceduresObserver::class);
        ProcedureDocs::observe(ProcedureDocsObserver::class);
        PaymentCustomerid::observe(PaymentCustomeridObserver::class);
        PasswordResets::observe(PasswordResetsObserver::class);
        PackageUserPlan::observe(PackageUserPlanObserver::class);
        PackageStoragePlan::observe(PackageStoragePlanObserver::class);
        PackagePurchaseMaster::observe(PackagePurchaseMasterObserver::class);
        PackageMaster::observe(PackageMasterObserver::class);
        Output::observe(OutputObserver::class);
        OrganizationStructure::observe(OrganizationStructureObserver::class);
        Opportunity::observe(OpportunityObserver::class);
        Languages::observe(LanguagesObserver::class);
        Kpi::observe(KpiObserver::class);
        IsoClause::observe(IsoClauseObserver::class);
        Input::observe(InputObserver::class);
        GeneralSettings::observe(GeneralSettingsObserver::class);
        ExternalIssueFixing::observe(ExternalIssueFixingObserver::class);
        EntryLog::observe(EntryLogObserver::class);
        Department::observe(DepartmentObserver::class);
        Countries::observe(CountriesObserver::class);
        ContextSubjects::observe(ContextSubjectsObserver::class);
        ContextStakeholderActionFiles::observe(ContextStakeholderActionFilesObserver::class);
        ContextStakeholderActionComments::observe(ContextStakeholderActionCommentsObserver::class);
        Company::observe(CompanyObserver::class);
        AuditPlanMaster::observe(AuditPlanMaster::class);
        AuditPlanAuditors::observe(AuditPlanAuditorsObserver::class);
        AuditPlanAuditees::observe(AuditPlanAuditeesObserver::class);
        AuditExecutionDocs::observe(AuditExecutionDocsObserver::class);
        AuditExecution::observe(AuditExecutionObserver::class);
        AuditDelegationDocs::observe(AuditDelegationDocsObserver::class);
        ActionMatrix::observe(ActionMatrixObserver::class);
        ActionDelegateMember::observe(ActionDelegateMemberObserver::class);
        Branch::observe(BranchObserver::class);
        SubscriptionCancellationLog::observe(SubscriptionCancellationLogObserver::class);
    }

    public function HomeRepo()
    {
        $this->app->singleton('HomeRepo', function () {
            return new Home();
        });
    }

    public function auditActionRepo()
    {
        $this->app->singleton('actionRepo', function () {
            return new AuditAction();
        });
    }

    public function auditPlanningRepo()
    {
        $this->app->singleton('planningRepo', function () {
            return new AuditPlanning();
        });
    }

    public function auditPrepareRepo()
    {
        $this->app->singleton('prepareRepo', function () {
            return new AuditPrepare();
        });
    }

    public function auditExecutionRepo()
    {
        $this->app->singleton('executionRepo', function () {
            return new AuditExecute();
        });
    }

    public function competentRepo()
    {
        $this->app->singleton('competentRepo', function () {
            return new CompetentMetrix();
        });
    }

    public function stakeholderRepo()
    {
        $this->app->singleton('stakeholderRepo', function () {
            return new Stakeholder();
        });
    }
}
