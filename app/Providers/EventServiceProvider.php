<?php 

namespace App\Providers;

use App\Models\ActionMatrix;
use App\Models\Company;
use App\Models\Department;
use App\Models\Position;
use App\Models\CompanySettings;
use App\Models\ContextSubjects;
use App\Models\FindingTypes;
use App\Models\GeneralSettings;
use App\Models\RiskOpportunityAction;
use App\Models\SourceTypes;
use App\Models\StakeholderCategory;
use App\Models\Users;
use App\Models\ProcessDocumentTypes;
use App\Models\ProcessRegulationTypes;
use App\Models\OrganizationStructure;
use App\Models\Process;
use App\Models\Branch;
use App\Models\Rating;
use App\Models\DepartmentBranch;
use App\Models\ResourceActivity;
use App\Models\CompetentRatingActionType;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Config;
use Auth;
use Mail;
use App\Mail\CompanyCreation;
use App\Mail\EndUserCreation;
use App\Models\SupplierParameter;
use App\Models\SupplierRating;
use App\Models\PositionLog;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => ['App\Listeners\TrackUserLog@in'],
        'Illuminate\Auth\Events\Logout' => ['App\Listeners\TrackUserLog@out'],
        'App\Events\Company\DemoData' => [
            'App\Listeners\Company\DemoDirectories',
            'App\Listeners\Company\DemoStructure',
            'App\Listeners\Company\DemoStakeholderCategory',
            'App\Listeners\Company\DemoFindingTypes',
            'App\Listeners\Company\DemoSourceTypes',
            'App\Listeners\Company\DemoContextSubjects',
            'App\Listeners\Company\DemoCompanySettings',
            'App\Listeners\Company\DemoRiskOpportunityAction',
            'App\Listeners\Company\DemoProcessDocumentTypes',
            'App\Listeners\Company\DemoProcessRegulationTypes',
            'App\Listeners\Company\DemoResourceActivity',
            'App\Listeners\Company\DemoRating',
            'App\Listeners\Company\DemoCompetentRatingActionType',
            'App\Listeners\Company\DemoSupplierParameter',
            'App\Listeners\Company\DemoSupplierRating',
            'App\Listeners\Company\DemoPackageLocation',
            'App\Listeners\Company\DemoPackageSituation',
            'App\Listeners\Company\DemoPackageEffective',
            'App\Listeners\Company\DemoPackageCategory',
            'App\Listeners\Company\DemoPackageAspect',
            'App\Listeners\Company\DemoPackageTheme',
            'App\Listeners\Company\DemoPackageEvaluation',
            'App\Listeners\Company\DemoPackageParameter',
            'App\Listeners\Company\DemoPackageRating'
        ]
    ];

    protected $subscribe = [
        'App\Listeners\User\PositionSave',
        'App\Listeners\User\CheckOwnership',
        'App\Listeners\User\CheckAuditor'
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        Users::created(function ($event) {
            if (!empty($event->email) && $event->no_email == 0 && !empty($event->invite_user_token) && $event->exists == 1) {
                $lang = Config::get('settings.lang');
                $createdBy = ($event->type != 2) ? Auth::guard('customer')->user()->name : 1;
                $mailData = ['id' => $event->invite_user_token, 'created_by' => $createdBy, 'fname' => $event->fname, 'name' => $event->name, 'email' => $event->email, 'company' => $event->company->name, 'lang' => $lang[$event->language]];
                switch ($event->type) {
                    case '2':
                        return Mail::to($mailData['email'], $mailData['name'])->bcc(Config::get('mail.from.address'))->send(new CompanyCreation($mailData));
                    case '3':
                        return Mail::to($mailData['email'], $mailData['name'])->bcc(Config::get('mail.from.address'))->send(new EndUserCreation($mailData));
                }
            } else {
                return response()->json(false);
            }
        });
        OrganizationStructure::saved(function ($event) {
            $columns = $event->getDirty();
            if (isset($columns['user_id'])) {
                PositionLog::create(['company_id' => Auth::user()->company_id, 'user_id' => $columns['user_id'], 'position_id' => $event->position_id]);
            }
        });
    }
}
