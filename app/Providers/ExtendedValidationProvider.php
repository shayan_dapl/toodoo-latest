<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Auth;
use Validator;
use App\Models\Users;
use App\Models\PositionBranches;
use App\Models\Process;
use App\Models\Risk;
use App\Models\Opportunity;
use App\Models\ProcessSteps;
use App\Models\Supplier;
use App\Models\SupplierParameter;
use App\Models\SupplierRating;
use App\Models\FindingTypes;
use App\Models\ResourcesType;
use App\Models\SourceTypes;

class ExtendedValidationProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('sameadmin', function ($attribute, $value, $parameters) {
            $found = Users::where('email', $value)->where('type', 2)->where('email', '<>', '')->where('out_of_service', null)->count();
            if (!empty($parameters)) {
                $foundInComp = Users::where('company_id', $parameters[0])
                    ->where('email', $value)
                    ->where('email', '<>', '')
                    ->where('id', '<>', $parameters[1])
                    ->where('out_of_service', null)
                    ->where('type', 2)
                    ->count();
                $foundOutSide = Users::where('email', $value)
                    ->where('email', '<>', '')
                    ->where('id', '<>', $parameters[1])
                    ->where('out_of_service', null)
                    ->where('type', 2)
                    ->count();
                $found = $foundInComp + $foundOutSide;
            }
            return ($found == 0) ? 'unique:user_master,email' : '';
        });
        Validator::extend('sameposition', function ($attribute, $value, $parameters) {
            $posOnSameBranch = PositionBranches::where('branch_id', $parameters[1]);
            if (!empty($parameters[0])) {
                $posOnSameBranch = $posOnSameBranch->where('position_id', '<>', $parameters[0]);
            }
            $posOnSameBranch = $posOnSameBranch->with('position')->get()->pluck('position.name')->toArray();
            $found = in_array($value, $posOnSameBranch);
            return ($found == 0) ? 'unique:position,name' : '';
        });
        Validator::extend('sameemail', function ($attribute, $value, $parameters) {
            $found = Users::companyCheck()->where('email', $value)->where('out_of_service', null)->count();
            return ($found == 0) ? 'unique:user_master,email' : '';
        });
        Validator::extend('sameuser', function ($attribute, $value, $parameters) {
            $found = Users::companyCheck()->where('email', $value)->where('email', '<>', '')->where('out_of_service', null);
            if (!empty($parameters)) {
                $found = $found->where('id', '<>', $parameters[1]);
            }
            $found = $found->count();
            return ($found == 0) ? 'unique:user_master,email' : '';
        });
        Validator::extend('sameprocess', function ($attribute, $value, $parameters) {
            $found = Process::where('company_id', Auth::guard('customer')->user()->company_id)->where('name', $value)->where('status', 1);
            if (!empty($parameters)) {
                $found = $found->where('id', '<>', $parameters[0]);
            }
            $found = $found->count();
            return ($found == 0) ? 'unique:process,name' : '';
        });
        Validator::extend('conflicted', function ($attribute, $value, $parameters) {
            return false;
        });
        Validator::extend('samerisk', function ($attribute, $value, $parameters) {
            $found = Risk::where('process_id', $parameters[0])->where('risk', $value)->where('risk', '<>', '')->where('id', '<>', $parameters[1])->where('status', 1)->count();
            return ($found == 0) ? 'unique:risk,risk' : '';
        });
        Validator::extend('sameopportunity', function ($attribute, $value, $parameters) {
            $found = Opportunity::where('process_id', $parameters[0])->where('details', $value)->where('details', '<>', '')->where('id', '<>', $parameters[1])->where('status', 1)->count();
            return ($found == 0) ? 'unique:opportunity,details' : '';
        });
        Validator::extend('samestep', function ($attribute, $value, $parameters) {
            $found = ProcessSteps::where('process_id', $parameters[0])->where('step_no', $value)->where('status', 1);
            if (!empty($parameters[1])) {
                $found = $found->where('process_id', $parameters[0])->where('id', '<>', $parameters[1]);
            }
            $found = $found->count();
            return ($found == 0) ? 'unique:process_steps,step_no' : '';
        });
        Validator::extend('samesupplier', function ($attribute, $value, $parameters) {
            $found = Supplier::companyStatusCheck()->where('name', $value);
            if (!empty($parameters)) {
                $found = $found->where('id', '<>', $parameters[0]);
            }
            $found = $found->count();
            return ($found == 0) ? 'unique:supplier,name' : '';
        });
        Validator::extend('samesupplierparameters', function ($attribute, $value, $parameters) {
            $found = SupplierParameter::companyStatusCheck()->where('name', $value);
            if (!empty($parameters)) {
                $found = $found->where('id', '<>', $parameters[0]);
            }
            $found = $found->count();
            return ($found == 0) ? 'unique:supplier_parameter,name' : '';
        });
        Validator::extend('samesupplierratings', function ($attribute, $value, $parameters) {
            $found = SupplierRating::companyStatusCheck()->where('rating', $value);
            if (!empty($parameters)) {
                $found = $found->where('id', '<>', $parameters[0]);
            }
            $found = $found->count();
            return ($found == 0) ? 'unique:supplier_rating,rating' : '';
        });
        Validator::extend('samefindingtype', function ($attribute, $value, $parameters) {
            $found = FindingTypes::companyStatusCheck()->where('finding_type', $value);
            if (!empty($parameters)) {
                $found = $found->where('id', '<>', $parameters[0]);
            }
            $found = $found->count();
            return ($found == 0) ? 'unique:finding_type,finding_type' : '';
        });
        Validator::extend('sameresourcetypes', function ($attribute, $value, $parameters) {
            $found = ResourcesType::companyStatusCheck()->where('name', $value);
            if (!empty($parameters)) {
                $found = $found->where('id', '<>', $parameters[0]);
            }
            $found = $found->count();
            return ($found == 0) ? 'unique:resources_type,name' : '';
        });
        Validator::extend('samesourcetype', function ($attribute, $value, $parameters) {
            $found = SourceTypes::companyCheck()->where('source_type', $value)->where('status', 1);
            if (!empty($parameters)) {
                $found = $found->where('id', '<>', $parameters[1]);
            }
            $found = $found->count();
            return ($found == 0) ? 'unique:source_type,source_type' : '';
        });
        Validator::extend('samesourcetype', function ($attribute, $value, $parameters) {
            $found = SourceTypes::companyCheck()->where('source_type', $value)->where('status', 1);
            if (!empty($parameters)) {
                $found = $found->where('id', '<>', $parameters[1]);
            }
            $found = $found->count();
            return ($found == 0) ? 'unique:source_type,source_type' : '';
        });
    }
}
