<?php 

namespace App\Repositories;

use App\Models\AuditTypeMaster;
use App\Models\ReportedIssuesDetails;
use Auth;
use DB;

/**
 * AuditAction repository for fetching all acttion data
 */
class AuditAction
{
    public function getReferences($name)
    {
        return AuditTypeMaster::select(DB::raw("CONCAT(clause, ' ', $name) AS full_name"), 'id')
            ->where('status', 1)
            ->pluck('full_name', 'id')
            ->prepend(translate('form.reference'), '');
    }

    public function reportedIssuesOverview($getsourceTypesAccess)
    {
        $getSType = implode(',', $getsourceTypesAccess->toArray());
        $sourceTypeFilter = !empty($getsourceTypesAccess->toArray()) ? '`reported_issues`.`company_id` = ? and `source_type`.`id` in ('.$getSType.') or ' : '';
        
        return DB::select(
            'select 
                *,
                sum(open) as main_open,
                sum(close) as main_close 
            from (
                select 
                    `reported_issues_details`.`id` as `did`, 
                    `reported_issues`.`issue_code` as `issue_code`, 
                    `reported_issues`.`source_type` as `source_type_id`, 
                    `reported_issues`.`reference`, 
                    `process`.`name` as `process`, 
                    `user_master`.`name` as `responsible`, 
                    `source_type`.`source_type` as `source_type`, 
                    `reported_issues_details`.* , 
                    IF(`reported_issues_details`.`status`=2,1,0) as close, IF(`reported_issues_details`.`status`<>2,1,0) as open
                from 
                    `reported_issues_details` 
                        inner join 
                    `reported_issues` 
                        on 
                    `reported_issues_details`.`reported_issue_id` = `reported_issues`.`id` 
                        inner join 
                    `process` 
                        on 
                    `reported_issues_details`.`related_process` = `process`.`id` 
                        inner join 
                    `user_master` 
                        on 
                    `reported_issues_details`.`who` = `user_master`.`id` 
                        inner join 
                    `source_type` 
                        on 
                    `reported_issues`.`source_type` = `source_type`.`id` 
                where 
                    (  
                        ' . $sourceTypeFilter . '
                        `reported_issues_details`.`who` = ? 
                        or 
                        `reported_issues_details`.`created_by` = ?
                    )
                order by 
                    `reported_issues_details`.`id` 
                desc
            ) as a  
            group by 
                a.`reference`, 
                a.`source_type`',
            [Auth::guard('customer')->user()->company_id,  Auth::guard('customer')->user()->id, Auth::guard('customer')->user()->id]
        );
    }

    public function reportedIssuesDetails($getsourceTypesAccess=[], $sourceTypeId=0, $reference='')
    {
        $result = ReportedIssuesDetails::with('issues')
        ->join('reported_issues', 'reported_issues_details.reported_issue_id', '=', 'reported_issues.id')
        ->join('process', 'reported_issues_details.related_process', '=', 'process.id')
        ->join('user_master', 'reported_issues_details.who', '=', 'user_master.id')
        ->join('source_type', 'reported_issues.source_type', '=', 'source_type.id')
        ->select(
            'reported_issues_details.id as did',
            'reported_issues.issue_code as issue_code',
            'reported_issues.reference',
            'process.name as process',
            'user_master.name as responsible',
            'source_type.source_type as source_type',
            'reported_issues_details.*'
        )
        ->where(function ($query) use ($getsourceTypesAccess) {
            $query->where('reported_issues.company_id', Auth::guard('customer')->user()->company_id);
            $query->whereIn('source_type.id', $getsourceTypesAccess);
            $query->orWhere('reported_issues_details.who', Auth::guard('customer')->user()->id);
            $query->orWhere('reported_issues_details.created_by', Auth::guard('customer')->user()->id);
        });
        if ($sourceTypeId !== 0) {
            $result = $result->where('reported_issues.source_type', $sourceTypeId);
        }
        if ($reference !== '') {
            $result = $result->where('reported_issues.reference', 'like', '%'.$reference.'%');
        }
        $result = $result->orderBy('reported_issues_details.id', 'DESC')->get();
        return $result;
    }
}
