<?php 

namespace App\Repositories;

use DB;

/**
 * AuditCalender repository for fetching all audits of calender
 */
class AuditCalender
{
    public function getAuditees($process_id = 0, $lead_auditor = 0, $month = 0, $year = 0)
    {
        return DB::table('audit_plan_master')
            ->leftJoin('audit_plan_auditees', function ($join) {
                $join->on('audit_plan_master.id', '=', 'audit_plan_auditees.audit_id');
            })
            ->select('audit_plan_master.*', DB::raw('"autitee" as type'))
            ->where('audit_plan_auditees.user_id', $lead_auditor)
            ->where('audit_plan_master.process_to_audit', $process_id)
            ->where('audit_plan_master.plan_month', $month)
            ->where('audit_plan_master.plan_year', $year)
            ->get()
            ->toArray();
    }

    public function getAudits($process_id = 0, $month = 0, $year = 0)
    {
        return DB::table('audit_plan_master')
            ->where('process_to_audit', $process_id)
            ->where('plan_month', $month)
            ->where('plan_year', $year)
            ->select('audit_plan_master.*', DB::raw('"customarAdmin" as type'))
            ->get();
    }

    public function getleadAauditor($process_id = 0, $lead_auditor = 0, $month = 0, $year = 0)
    {
        return DB::table('audit_plan_master')
            ->where('process_to_audit', $process_id)
            ->where('choose_lead_auditor', $lead_auditor)
            ->where('plan_month', $month)
            ->where('plan_year', $year)
            ->select('audit_plan_master.*', DB::raw('"leadAuditor" as type'))
            ->get()
            ->toArray();
    }

    public function getAuditor($process_id = 0, $lead_auditor = 0, $month = 0, $year = 0)
    {
        return DB::table('audit_plan_master')
            ->leftJoin('audit_team_member', function ($join) {
                $join->on('audit_plan_master.id', '=', 'audit_team_member.audit_id');
            })
            ->select('audit_plan_master.*', DB::raw('"auditor" as type'))
            ->where('audit_team_member.auditor_id', $lead_auditor)
            ->where('audit_plan_master.process_to_audit', $process_id)
            ->where('audit_plan_master.plan_month', $month)
            ->where('audit_plan_master.plan_year', $year)
            ->get()
            ->toArray();
    }

    public function getExecutedAudit($audit_id)
    {
        return DB::table('audit_execution')
            ->where('audit_id', $audit_id)
            ->get();
    }
}
