<?php 

namespace App\Repositories;

use App\Models\AuditPlanAuditees;
use App\Models\Users;
use DB;
use Auth;

/**
 * AuditExecution repository for fetching all acttion data
 */
class AuditExecution
{
    public function getSelectedAuditee($audit_id)
    {
        return Users::whereIn('id', function ($query) use ($audit_id) {
            $query->select(DB::raw('user_id'))
                ->from('audit_plan_auditees')
                ->where('audit_plan_auditees.audit_id', $audit_id)
                ->pluck('user_id')->toArray();
        })->pluck('id')->toArray();
    }

    public function removeOldAuditee($audit_id)
    {
        return AuditPlanAuditees::where('audit_id', $audit_id)->delete();
    }

    public function insertNewAuditee($plan_auditees)
    {
        return AuditPlanAuditees::insert($plan_auditees);
    }
    public function getAuditResult()
    {
        return DB::table('audit_execution')
        ->join('audit_plan_master', 'audit_execution.audit_id', '=', 'audit_plan_master.id')
        ->join('process', 'audit_execution.process_id', '=', 'process.id')
        ->join('audit_action_matrix', 'audit_action_matrix.finding_type', '=', 'audit_execution.finding_type_id')
        ->join('user_master', 'process.owner_id', '=', 'user_master.id')
        ->join('finding_type', 'audit_execution.finding_type_id', '=', 'finding_type.id')
        ->select(
            'audit_execution.id as execution_id',
            'audit_execution.created_at as created_at',
            'audit_execution.observation as observation',
            'audit_plan_master.plan_date as plan_date',
            'audit_plan_master.category as category',
            'process.name as process',
            'process.id as processId',
            'process.owner_id as owner_id',
            'audit_plan_master.id as audit_id',
            'finding_type.finding_type as finding_type',
            'user_master.name as processowner',
            DB::raw(
                '`audit_action_matrix`.`immidiate_actions`,
                `audit_action_matrix`.`root_cause_analysis`,
                `audit_action_matrix`.`corrective_measure`,
                `audit_action_matrix`.`effectiveness`'
                ),
            'audit_execution.id'
            )
        ->where(function ($query) {
            $query->where('audit_action_matrix.immidiate_actions', '<>', 0)
            ->orWhere('audit_action_matrix.root_cause_analysis', '<>', 0)
            ->orWhere('audit_action_matrix.corrective_measure', '<>', 0)
            ->orWhere('audit_action_matrix.effectiveness', '<>', 0);
        })
        ->where('process.company_id', Auth::guard('customer')->user()->company_id)
        ->where('audit_plan_master.status', 2)
        ->get();
    }
}
