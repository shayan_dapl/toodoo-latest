<?php 

namespace App\Repositories;

use App\Models\AuditPlanAuditors;
use App\Models\Process;
use App\Models\ProcessToAudit;
use Auth;
use DB;

/**
 * AuditPlanning repository for fetching all panned audit data
 */
class AuditPlanning
{
    public function insertTeamMember($audit_team_mamber)
    {
        return AuditPlanAuditors::insert($audit_team_mamber);
    }

    public function getPlannedAudits($category, $branchId)
    {
        if ($branchId == 'all') {
            $allAuditData = Process::leftJoin('process_to_audit', function ($join) {
                $join->on('process.id', '=', 'process_to_audit.process_id');
            })
        ->leftJoin('audit_plan_master', function ($join) use ($category) {
            $join->on('process_to_audit.audit_id', '=', 'audit_plan_master.id');
            $join->where('audit_plan_master.category', '=', $category);
        })
        ->select('process.id as process_id', 'process.name as name', 'audit_plan_master.plan_month as plan_month', 'audit_plan_master.plan_year as plan_year', 'audit_plan_master.plan_date as audit_date', 'audit_plan_master.id', 'audit_plan_master.status as audit_status', 'audit_plan_master.original_month as original_month', 'audit_plan_master.original_year as original_year', 'audit_plan_master.release_month as release_month', 'audit_plan_master.release_year as release_year', 'audit_plan_master.release_date as release_date', 'audit_plan_master.choose_lead_auditor as choose_lead_auditor')
        ->where('process.company_id', '=', Auth::guard('customer')->user()->company_id)
        ->where('process.status', 1)
        ->get();
        } else {
            $allAuditData = Process::leftJoin('process_to_audit', function ($join) {
                $join->on('process.id', '=', 'process_to_audit.process_id');
            })
            ->leftJoin('audit_plan_master', function ($join) use ($category,$branchId) {
                $join->on('process_to_audit.audit_id', '=', 'audit_plan_master.id');
                $join->where('audit_plan_master.category', '=', $category);
            })
            ->leftJoin('audit_branches', function ($join) use ($branchId) {
                $join->on('audit_branches.audit_id', '=', 'audit_plan_master.id');
                $join->where('audit_branches.branch_id', '=', $branchId);
            })
             ->leftJoin('process_branches', function ($join) {
                 $join->on('process_branches.process_id', '=', 'process.id');
             })
            ->select('process.id as process_id', 'process.name as name', 'audit_plan_master.plan_month as plan_month', 'audit_plan_master.plan_year as plan_year', 'audit_plan_master.plan_date as audit_date', 'audit_plan_master.id', 'audit_plan_master.status as audit_status', 'audit_plan_master.original_month as original_month', 'audit_plan_master.original_year as original_year', 'audit_plan_master.release_month as release_month', 'audit_plan_master.release_year as release_year', 'audit_plan_master.release_date as release_date', 'audit_plan_master.choose_lead_auditor as choose_lead_auditor')
            ->where('process.company_id', '=', Auth::guard('customer')->user()->company_id)
            ->where('process.status', 1)
            ->where('process_branches.branch_id', $branchId)
            ->get();
        }
        return $allAuditData;
    }
}
