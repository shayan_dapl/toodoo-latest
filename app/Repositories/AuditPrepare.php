<?php 

namespace App\Repositories;

use App\Models\AuditPlanAuditors;
use App\Models\AuditPlanMaster;
use App\Models\Users;
use Auth;
use DB;

/**
 * AuditPrepare repository for fetching all panned audit data
 */
class AuditPrepare
{
    public function getPlannedAudits($companyId, $reqId)
    {
        return AuditPlanMaster::where('company_id', $companyId)
            ->with('audit_type')
            ->with('lead_auditor')
            ->with('process')
            ->where('id', $reqId)
            ->first();
    }

    public function getInternalAuditor($selectedProcessOwner)
    {
        return Users::where('id', '<>', Auth::guard('customer')->user()->id)
            ->where('company_id', Auth::guard('customer')->user()->company_id)
            ->where('is_auditor', 1)
            ->whereNotIn('id', $selectedProcessOwner)
            ->where('out_of_service', null)
            ->where('no_email', 0)
            ->orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->toArray();
    }

    public function selectedInternalAuditor($audit_id)
    {
        return Users::whereIn('id', function ($query) use ($audit_id) {
            $query->select(DB::raw('auditor_id'))
                ->from('audit_team_member')
                ->where('audit_team_member.audit_id', $audit_id)
                ->pluck('auditor_id')->toArray();
        })
            ->where('id', '<>', Auth::guard('customer')->user()->id)
            ->where('is_auditor', 1)
            ->pluck('id')
            ->toArray();
    }

    public function getAuditee($lead_auditor)
    {
        return Users::where('company_id', Auth::guard('customer')->user()->company_id)
            ->where('id', '<>', $lead_auditor)
            ->where('out_of_service', null)
            ->orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->toArray();
    }

    public function getSelectedAuditee($audit_id)
    {
        return Users::whereIn('id', function ($query) use ($audit_id) {
            $query->select(DB::raw('user_id'))
                ->from('audit_plan_auditees')
                ->where('audit_plan_auditees.audit_id', $audit_id)
                ->pluck('user_id')->toArray();
        })
            ->pluck('id')
            ->toArray();
    }

    public function deleteTeamMember($memberId)
    {
        return AuditPlanAuditors::where('auditor_type', '<>', 1)->where('audit_id', $memberId)->delete();
    }

    public function insertTeamMember($audit_team_mamber)
    {
        return AuditPlanAuditors::insert($audit_team_mamber);
    }
}
