<?php 

namespace App\Repositories;

use App\Models\Process;
use App\Models\Position;
use Auth;
use DB;

/**
 * CompetentMetrix repository for fetching all planned competent data
 */
class CompetentMetrix
{
    public function competentProcess($selectedRating = '', $positionId = '', $selectedBranch = '')
    {
        $everyOne = Position::companyCheck()->where('status', 4)->first()->id;
        $process = Process::join('process_steps', function ($join) {
            $join->on('process.id', '=', 'process_steps.process_id');
        })->join('roles', function ($join) {
            $join->on('process_steps.id', '=', 'roles.process_step_id');
        })->join('position', function ($join) {
            $join->on('position.id', '=', 'roles.position_id');
        })->leftJoin('process_branches', function ($join) {
            $join->on('process.id', '=', 'process_branches.process_id');
        })->leftJoin('position_branches', function ($join) {
            $join->on('position.id', '=', 'position_branches.position_id');
        });
        if ($selectedRating != '') {
            $process = $process->leftJoin('organization_structure', function ($join) {
                $join->on('roles.position_id', '=', 'organization_structure.position_id');
            })->leftJoin('competent_ratings', function ($join) use ($selectedRating) {
                $join->on('process.id', '=', 'competent_ratings.process_id');
                $join->on('process_steps.id', '=', 'competent_ratings.step_id');
            });
        }
        $process = $process->select('process.id', 'process.name', 'process_steps.step_name', 'process_steps.id as step_id', 'roles.position_id', 'position.name as positionname', 'position.status as positionstatus')->where('process.company_id', Auth::guard('customer')->user()->company_id)->where('process.status', '<>', 3)->where('roles.role_id', 1);
        if ($selectedRating != '') {
            $process = $process->where('competent_ratings.rating', $selectedRating)->where('competent_ratings.is_latest', 1);
        }
        if (($positionId != '') && ($positionId != 'all')) {
            $process = $process->whereIn('roles.position_id', [$positionId, $everyOne]);
        }
        if (($selectedBranch != '') && ($selectedBranch != 'all')) {
            $process = $process->where('process_branches.branch_id', $selectedBranch)->where(function ($query) use ($selectedBranch, $everyOne) {
                $query->where('position_branches.branch_id', $selectedBranch)->orWhere('position.id', $everyOne);
            });
        }
        $process = $process->distinct('roles.process_step_id')->orderBy('process_steps.id')->get();
        return $process;
    }
}
