<?php 

namespace App\Repositories;

use App\Models\ActionDelegateMember;
use App\Models\ReportIssueAction;
use Auth;
use DB;

/**
 * Delegation repository for fetching Delegation related data
 */
class Delegation
{
    public function getActionIds($ref_id)
    {
        return ReportIssueAction::select(DB::raw('CONVERT(GROUP_CONCAT(id) USING utf8) as ids'))
            ->where('company_id', Auth::guard('customer')->user()->company_id)
            ->where('ref_id', $ref_id)->first()->ids;
    }

    public function getDelegatedMembers($actionIds)
    {
        return ActionDelegateMember::whereIn('action_id', $actionIds)->get();
    }
    public function getDelegatedStatus($actionIds)
    {
        $totalAction = ActionDelegateMember::whereIn('action_id', $actionIds)->count();
        if ($totalAction == 0) {
            return 1;
        } else {
            return ActionDelegateMember::whereIn('action_id', $actionIds)->where('status', 0)->count();
        }
    }
}
