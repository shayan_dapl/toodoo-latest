<?php 

namespace App\Repositories;

use App;
use App\Models\ActionDelegateMember;
use App\Models\AuditExecution;
use App\Models\AuditPlanAuditees;
use App\Models\AuditPlanAuditors;
use App\Models\AuditPlanMaster;
use App\Models\FindingTypes;
use App\Models\Opportunity;
use App\Models\Process;
use App\Models\ReportedIssuesDetails;
use App\Models\Risk;
use App\Models\RiskOpportunityActionLog;
use App\Models\ContextStakeholderActionLog;
use App\Models\ContextAnalysis;
use App\Models\StakeholderAnalysis;
use App\Models\Users;
use App\Models\ProcessToAudit;
use App\Models\SourceTypes;
use App\Models\CompetentRatingsActionLog;
use App\Models\SupplierEvaluationActionLog;
use App\Models\SupplierCorrectiveActionLog;
use Auth;
use DB;

/**
 * Home repository for fetching all dashboard data
 */
class Home
{
    public function totalUnplannedAudits()
    {
        return AuditPlanMaster::where('company_id', Auth::guard('customer')->user()->company_id)
        ->where('choose_lead_auditor', Auth::guard('customer')->user()->id)
        ->where('plan_date', null)
        ->where('plan_time', null)
        ->with('process')
        ->with('lead_auditor')
        ->count();
    }

    public function auditPlans()
    {
        return AuditPlanMaster::leftJoin('audit_team_member', function ($join) {
            $join->on('audit_plan_master.id', '=', 'audit_team_member.audit_id');
        })->select('audit_plan_master.*', 'audit_team_member.auditor_id')
        ->where('audit_plan_master.company_id', Auth::guard('customer')->user()->company_id)
        ->where('audit_team_member.auditor_id', Auth::guard('customer')->user()->id)
        ->where(function ($query) {
            $query->where('audit_plan_master.status', 1)
            ->orWhere('audit_plan_master.status', 3);
        })
        ->orderby('audit_plan_master.plan_date', 'asc')
        ->get();
    }

    public function unplannedAudits()
    {
        return AuditPlanMaster::leftJoin('audit_team_member', function ($join) {
            $join->on('audit_plan_master.id', '=', 'audit_team_member.audit_id');
        })->select('audit_plan_master.*', 'audit_team_member.auditor_id')
        ->where('audit_plan_master.company_id', Auth::guard('customer')->user()->company_id)
        ->where('audit_team_member.auditor_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.plan_date', null)
        ->where('audit_plan_master.status', 1)->get();
    }

    public function unplannedAuditsMonthWise($month, $year)
    {
        return AuditPlanMaster::leftJoin('audit_team_member', function ($join) {
            $join->on('audit_plan_master.id', '=', 'audit_team_member.audit_id');
        })->select('audit_plan_master.*', 'audit_team_member.auditor_id')
        ->where('audit_plan_master.company_id', Auth::guard('customer')->user()->company_id)
        ->where('audit_team_member.auditor_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.plan_date', null)
        ->where('plan_month', $month)
        ->where('plan_year', $year)
        ->where('audit_plan_master.status', 1)->count();
    }

    public function plannedAuditsMonthWise($month, $year, $status)
    {
        return AuditPlanMaster::leftJoin('audit_team_member', function ($join) {
            $join->on('audit_plan_master.id', '=', 'audit_team_member.audit_id');
        })
        ->select('audit_plan_master.*', 'audit_team_member.auditor_id')
        ->where('audit_plan_master.company_id', Auth::guard('customer')->user()->company_id)
        ->where('audit_team_member.auditor_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.plan_date', '<>', null)
        ->where('plan_month', $month)
        ->where('plan_year', $year)
        ->where('audit_plan_master.status', $status)
        ->get();
    }

    public function releasedAuditPlans()
    {
        return AuditPlanMaster::leftJoin('audit_team_member', function ($join) {
            $join->on('audit_plan_master.id', '=', 'audit_team_member.audit_id');
        })->select('audit_plan_master.*', 'audit_team_member.auditor_id')
        ->where('audit_plan_master.company_id', Auth::guard('customer')->user()->company_id)
        ->where('audit_plan_master.choose_lead_auditor', Auth::guard('customer')->user()->id)
        ->orWhere('audit_team_member.auditor_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.status', 2)
        ->with('process')
        ->with('lead_auditor')
        ->get();
    }

    public function totalSpecialUser()
    {
        return Users::leftJoin('user_details', function ($join) {
            $join->on('user_master.id', '=', 'user_details.master_id');
        })
        ->where('user_master.company_id', '=', Auth::guard('customer')->user()->company_id)
        ->where('user_details.status', '=', 1)
        ->where('user_master.is_special_user', '=', 1)
        ->count();
    }

    public function totalLeftUser()
    {
        return Users::leftJoin('user_details', function ($join) {
            $join->on('user_master.id', '=', 'user_details.master_id');
        })
        ->where('user_master.company_id', '=', Auth::guard('customer')->user()->company_id)
        ->where('user_details.status', '=', 3)
        ->count();
    }

    public function internalPendingIssues()
    {
        return DB::table('audit_execution')
        ->join('audit_plan_master', 'audit_execution.audit_id', '=', 'audit_plan_master.id')
        ->join('process', 'audit_execution.process_id', '=', 'process.id')
        ->join('audit_action_matrix', 'audit_action_matrix.finding_type', '=', 'audit_execution.finding_type_id')
        ->join('user_master', 'process.owner_id', '=', 'user_master.id')
        ->join('finding_type', 'audit_execution.finding_type_id', '=', 'finding_type.id')
        ->select(
            'audit_execution.id as execution_id',
            'audit_execution.created_at as created_at',
            'audit_execution.observation as observation',
            'process.name as process',
            'process.id as processId',
            'audit_plan_master.id as audit_id',
            'finding_type.finding_type as finding_type',
            'user_master.name as responsible',
            DB::raw(
                '`audit_action_matrix`.`immidiate_actions`,
                `audit_action_matrix`.`root_cause_analysis`,
                `audit_action_matrix`.`corrective_measure`,
                `audit_action_matrix`.`effectiveness`'
                ),
            'audit_execution.id'
            )
        ->where(function ($query) {
            $query->where('audit_action_matrix.immidiate_actions', '<>', 0)
            ->orWhere('audit_action_matrix.root_cause_analysis', '<>', 0)
            ->orWhere('audit_action_matrix.corrective_measure', '<>', 0)
            ->orWhere('audit_action_matrix.effectiveness', '<>', 0);
        })
        ->where('process.owner_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.status', 2)
        ->get();
    }

    public function releasedExecutedAudits()
    {
        return AuditPlanMaster::leftJoin('audit_team_member', function ($join) {
            $join->on('audit_plan_master.id', '=', 'audit_team_member.audit_id');
        })->select('audit_plan_master.*', 'audit_team_member.auditor_id')
        ->where('audit_plan_master.company_id', Auth::guard('customer')->user()->company_id)
        ->where('audit_team_member.auditor_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.status', 2)
        ->orderby('audit_plan_master.release_date', 'asc')
        ->get();
    }

    public function plannedAudits()
    {
        return ProcessToAudit::leftJoin('audit_plan_master', function ($join) {
            $join->on('process_to_audit.audit_id', '=', 'audit_plan_master.id');
        })
        ->select(DB::raw('DISTINCT process.id as process_id'), 'process.name as name')
        ->where('process.company_id', '=', Auth::guard('customer')->user()->company_id)
        ->where('process.status', '=', 1)
        ->get();
    }

    public function plannedAuditsforMeeting($months, $category)
    {
        $auditors = ProcessToAudit::leftJoin('audit_plan_master', function ($join) use ($category) {
            $join->on('process_to_audit.audit_id', '=', 'audit_plan_master.id');
            $join->where('audit_plan_master.category', '=', $category);
        })
        ->leftJoin('process', function ($join) {
            $join->on('process.id', '=', 'process_to_audit.process_id');
        })
        ->leftJoin('audit_team_member', function ($join) {
            $join->on('audit_plan_master.id', '=', 'audit_team_member.audit_id');
        })
        ->select('process.id as process_id', 'process.name as name', 'audit_plan_master.plan_month as plan_month', 'audit_plan_master.plan_year as plan_year', DB::raw("CAST(`audit_plan_master`.`plan_date` as CHAR CHARACTER SET utf8) as audit_date"), DB::raw("'auditor' as type"), 'audit_plan_master.id', 'audit_team_member.auditor_type as auditor_type', 'audit_plan_master.status as audit_status', 'audit_plan_master.original_month as original_month', 'audit_plan_master.original_year as original_year', 'audit_plan_master.release_month as release_month', 'audit_plan_master.release_year as release_year', 'audit_plan_master.release_date as release_date', 'audit_plan_master.choose_lead_auditor as choose_lead_auditor')
        ->where('process.company_id', '=', Auth::guard('customer')->user()->company_id)
        ->where('process.status', '=', 1)
        ->where('audit_team_member.auditor_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.category', $category)
        ->whereBetween('audit_plan_master.plan_date', $months);

        $auditeesAndAuditors = ProcessToAudit::leftJoin('audit_plan_master', function ($join) use ($category) {
            $join->on('process_to_audit.audit_id', '=', 'audit_plan_master.id');
            $join->where('audit_plan_master.category', '=', $category);
        })
        ->leftJoin('process', function ($join) {
            $join->on('process.id', '=', 'process_to_audit.process_id');
        })
        ->leftJoin('audit_plan_auditees', function ($join) {
            $join->on('audit_plan_master.id', '=', 'audit_plan_auditees.audit_id');
        })
        ->select('process.id as process_id', 'process.name as name', 'audit_plan_master.plan_month as plan_month', 'audit_plan_master.plan_year as plan_year', DB::raw("CAST(`audit_plan_master`.`plan_date` as CHAR CHARACTER SET utf8) as audit_date"), DB::raw("'auditee' as type"), 'audit_plan_master.id', DB::raw("'NULL' as auditor_type"), 'audit_plan_master.status as audit_status', 'audit_plan_master.original_month as original_month', 'audit_plan_master.original_year as original_year', 'audit_plan_master.release_month as release_month', 'audit_plan_master.release_year as release_year', 'audit_plan_master.release_date as release_date', 'audit_plan_master.choose_lead_auditor as choose_lead_auditor')
        ->where('process.company_id', '=', Auth::guard('customer')->user()->company_id)
        ->where('process.status', '=', 1)
        ->where('audit_plan_auditees.user_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.category', $category)
        ->whereBetween('audit_plan_master.plan_date', $months)
        ->unionAll($auditors)
        ->get();

        $allAuditData = $auditeesAndAuditors;
        return $allAuditData;
    }

    public function requiredActionIssues() //Not needed now
    {
        return DB::table('action_delegate_members')
        ->leftJoin('report_issue_action', function ($join) {
            $join->on('action_id', '=', 'report_issue_action.id');
        })
        ->leftJoin('audit_execution', function ($join) {
            $join->on('report_issue_action.ref_id', '=', 'audit_execution.id');
        })
        ->leftJoin('audit_plan_master', function ($join) {
            $join->on('audit_execution.audit_id', '=', 'audit_plan_master.id');
        })
        ->leftJoin('process_to_audit', function ($join) {
            $join->on('audit_plan_master.audit_id', '=', 'process_to_audit.audit_id');
        })
        ->leftJoin('process', function ($join) {
            $join->on('process_to_audit.process_id', '=', 'process.id');
        })
        ->leftJoin('user_master', function ($join) {
            $join->on('process.owner_id', '=', 'user_master.id');
        })
        ->select(
            'audit_execution.observation',
            'audit_execution.id as exec_id',
            'action_delegate_members.*',
            'process.name as process_name',
            'user_master.name as owner_name'
            )
        ->where('action_delegate_members.member', Auth::guard('customer')->user()->id)
        ->groupBy('action_delegate_members.action_id')
        ->get();
    }

    public function currentCompanyFindings()
    {
        return DB::select(
            '
            SELECT
            test.id,
            test.finding_type,
            SUM(test.total) AS total
            FROM
            (
            SELECT
            ft.id,
            ft.finding_type,
            null AS total
            FROM
            `finding_type` AS ft
            WHERE
            ft.company_id = ?
            group by
            ft.id
            Union ALL
            SELECT
            ft.id,
            ft.finding_type,
            COUNT(ae.finding_type_id) AS total
            FROM
            `finding_type` AS ft
            left join
            `audit_execution` AS ae
            on
            ft.id = ae.finding_type_id
            WHERE
            ft.company_id = ?
            group by
            ft.id
            ) AS test
            group by
            test.id',
            [Auth::guard('customer')->user()->company_id, Auth::guard('customer')->user()->company_id]
            );
    }

    public function findingProcess()
    {
        return DB::Select(
            '
            SELECT
            test.id,
            test.name,
            SUM(test.total) AS total
            FROM
            (
            SELECT
            pr.id,
            pr.name,
            null AS total
            FROM
            `process` AS pr
            WHERE
            pr.company_id = ?
            GROUP BY
            pr.id
            UNION ALL
            SELECT
            pr.id,
            pr.name,
            COUNT(ae.finding_type_id) AS total
            FROM
            `process` AS pr
            LEFT JOIN
            `audit_plan_master` AS apm
            ON
            pr.id = apm.process_to_audit
            LEFT JOIN
            `audit_execution` AS ae
            ON
            apm.id = ae.audit_id
            WHERE
            pr.company_id = ?
            GROUP BY
            pr.id
            ) AS test
            GROUP BY
            test.id',
            [Auth::guard('customer')->user()->company_id, Auth::guard('customer')->user()->company_id]
            );
    }

    public function urgentIssues()
    {
        $urgentIssues = DB::Select("
         SELECT
         `audit_execution`.`observation` AS issue_title,
         `audit_execution`.`id` AS exec_id,
         `process`.`id` AS process_id,
         `process`.`name` AS process,
         `audit_plan_master`.`category` AS `type`,
         `finding_type`.`finding_type` AS sources,
         CAST(`action_delegate_members`.`deadline` AS CHAR CHARACTER SET utf8) AS urgency,
         `action_delegate_members`.`created_at` AS created_at,
         `action_delegate_members`.`issue_type` AS stats,
         `action_delegate_members`.`requested_action` AS next_action,
         GROUP_CONCAT(`action_delegate_members`.`issue_type`) AS action_pending,
         GROUP_CONCAT(`action_delegate_members`.`status`) AS member_status
         FROM
         `action_delegate_members`
         LEFT JOIN
         `report_issue_action` ON `action_delegate_members`.`action_id`=`report_issue_action`.`id`
         LEFT JOIN
         `audit_execution` ON `report_issue_action`.`ref_id`=`audit_execution`.`id`
         LEFT JOIN
         `finding_type` ON `audit_execution`.`finding_type_id` = `finding_type`.`id`
         LEFT JOIN
         `audit_plan_master` ON `audit_execution`.`audit_id`=`audit_plan_master`.`id`
         LEFT JOIN
         `process` ON `audit_execution`.`process_id`=`process`.`id`
         WHERE
         `action_delegate_members`.`member` = ?
         AND
          `process`.`status` = 1
         AND
         `action_delegate_members`.`complete_date` is NULL
         GROUP BY
         `action_delegate_members`.`action_id`
         UNION ALL
         SELECT
         `reported_issues_details`.`issue` AS issue_title,
         `reported_issues_details`.`id` AS exec_id,
         `process`.`id` AS process_id,
         `process`.`name` AS process,
         IFNULL(CONCAT(`source_type`.`source_type`,' / ',`supplier`.`name`), `source_type`.`source_type`) AS type,
         NULL AS sources,
         `reported_issues_details`.`deadline` AS urgency,
         `reported_issues_details`.`created_at` AS created_at,
         `reported_issues_details`.`status` AS stats,
         `reported_issues_details`.`action` AS next_action,
         'none' AS action_pending,
         'none' AS member_status
         FROM
         `reported_issues_details`
         LEFT JOIN
         `process` ON `process`.`id` = `reported_issues_details`.`related_process`
         LEFT JOIN
         `reported_issues` ON `reported_issues`.`id` = `reported_issues_details`.`reported_issue_id`
         LEFT JOIN
         `source_type` ON `source_type`.`id` = `reported_issues`.`source_type`
         LEFT JOIN
         `supplier` ON `reported_issues`.`supplier` = `supplier`.`id`
         WHERE
         `reported_issues_details`.`who` = ?
         AND
         `reported_issues_details`.`status` = 1
         AND
         `process`.`status` = 1
         UNION ALL
         SELECT
         `risk_opportunity_action_log`.`risk_opp_id` AS issue_title,
         `risk_opportunity_action_log`.`risk_opp_id` AS exec_id,
         `process`.`id` AS process_id,
         `process`.`name` AS process,
         `risk_opportunity_action_log`.`type` AS type,
         NULL AS sources,
         `risk_opportunity_action_log`.`deadline` AS urgency,
         `risk_opportunity_action_log`.`created_at` AS created_at,
         1 AS stats,
         `risk_opportunity_action_log`.`action` AS next_action,
         'none' AS action_pending,
         'none' AS member_status
         FROM
         `risk_opportunity_action_log`
         LEFT JOIN
         `process` ON `process`.`id` = `risk_opportunity_action_log`.`process_id`
         WHERE
         `risk_opportunity_action_log`.`delegate_to` = ?
         AND
         `risk_opportunity_action_log`.`closed` = 0
         AND
          `process`.`status` = 1
         UNION ALL
         SELECT
         `context_stakeholder_action_log`.`contxt_stakehold_id` AS issue_title,
         `context_stakeholder_action_log`.`id` AS exec_id,
         NULL AS process_id,
         NULL AS process,
         `context_stakeholder_action_log`.`type` AS type,
         '' AS sources,
         `context_stakeholder_action_log`.`deadline` AS urgency,
         `context_stakeholder_action_log`.`created_at` AS created_at,
         1 AS stats,
         `context_stakeholder_action_log`.`action` AS next_action,
         'none' AS action_pending,
         'none' AS member_status
         FROM
         `context_stakeholder_action_log`
         LEFT JOIN
         `company` ON `company`.`id` = `context_stakeholder_action_log`.`company_id`
         WHERE
         `context_stakeholder_action_log`.`deligate_to` = ?
         AND
         `context_stakeholder_action_log`.`closed` = 0
         UNION ALL
         SELECT
         `competent_rating_action_type`.`type` AS issue_title,
         `competent_ratings_action_log`.`id` AS exec_id,
         `process`.`id` AS process_id,
         `process`.`name` AS process,
         'competent-rating' AS type,
         '' AS sources,
         `competent_ratings_action_log`.`deadline` AS urgency,
         `competent_ratings_action_log`.`created_at` AS created_at,
         1 AS stats,
         `competent_ratings_action_log`.`action` AS next_action,
         'none' AS action_pending,
         'none' AS member_status
         FROM
         `competent_ratings_action_log`
         LEFT JOIN
         `competent_ratings` ON `competent_ratings`.`id` = `competent_ratings_action_log`.`rating_id`
         LEFT JOIN
         `process` ON `process`.`id` = `competent_ratings`.`process_id`
         LEFT JOIN
         `company` ON `company`.`id` = `competent_ratings_action_log`.`company_id`
         LEFT JOIN
         `competent_rating_action_type` ON `competent_rating_action_type`.`id` = `competent_ratings_action_log`.`action_type`
         WHERE
         `competent_ratings_action_log`.`delegate_to` = ?
         AND
         `competent_ratings_action_log`.`closed` = 0
         AND
          `process`.`status` = 1
          UNION ALL
         SELECT
         `supplier`.`name` AS issue_title,
         `supplier_evaluation_action_log`.`id` AS exec_id,
         NULL AS process_id,
         NULL AS process,
         'supplier-rating' AS type,
         '' AS sources,
         `supplier_evaluation_action_log`.`deadline` AS urgency,
         `supplier_evaluation_action_log`.`created_at` AS created_at,
         1 AS stats,
         `supplier_evaluation_action_log`.`action` AS next_action,
         'none' AS action_pending,
         'none' AS member_status
         FROM
         `supplier_evaluation_action_log`
         LEFT JOIN
         `supplier` ON `supplier`.`id` = `supplier_evaluation_action_log`.`supplier_id`
         WHERE
         `supplier_evaluation_action_log`.`delegate_to` = ?
         AND
         `supplier_evaluation_action_log`.`closed` = 0
         UNION ALL
         SELECT
         `supplier`.`name` AS issue_title,
         `supplier_corrective_action_log`.`id` AS exec_id,
         NULL AS process_id,
         NULL AS process,
         'corrective-action' AS type,
         '' AS sources,
         `supplier_corrective_action_log`.`deadline` AS urgency,
         `supplier_corrective_action_log`.`created_at` AS created_at,
         1 AS stats,
         NULL AS next_action,
         'none' AS action_pending,
         'none' AS member_status
         FROM
         `supplier_corrective_action_log`
         LEFT JOIN
         `supplier` ON `supplier`.`id` = `supplier_corrective_action_log`.`supplier_id`
         WHERE
         `supplier_corrective_action_log`.`delegate_to` = ?
         AND
         `supplier_corrective_action_log`.`closed` = 0
         UNION ALL
         SELECT
         CONCAT(`resources`.`brand`,' - ',`resources`.`type`,' - ',`resources`.`serial_no`) AS issue_title,
         `resource_delegation_action_log`.`id` AS exec_id,
         `process`.`id` AS process_id,
         `process`.`name` AS process,
         'resource' AS type,
         '' AS sources,
         `resource_delegation_action_log`.`deadline` AS urgency,
         `resource_delegation_action_log`.`created_at` AS created_at,
         1 AS stats,
         NULL AS next_action,
         'none' AS action_pending,
         'none' AS member_status
         FROM
         `resource_delegation_action_log`
         LEFT JOIN
         `resources` ON `resources`.`id` = `resource_delegation_action_log`.`resource_id`
         LEFT JOIN
         `process` ON `process`.`id` = `resources`.`process_id`
         WHERE
         `resource_delegation_action_log`.`delegate_to` = ?
         AND
         `resource_delegation_action_log`.`closed` = 0
         UNION ALL
         SELECT
         CONCAT(SUBSTRING(`addon_package`.`activity_desc`, 1, 20), '...') AS issue_title,
         `addon_package_action_log`.`id` AS exec_id,
         `process`.`id` AS process_id,
         `process`.`name` AS process,
         'iso-14001' AS type,
         '' AS sources,
         `addon_package_action_log`.`deadline` AS urgency,
         `addon_package_action_log`.`created_at` AS created_at,
         1 AS stats,
         `addon_package_action_log`.`action` AS next_action,
         'none' AS action_pending,
         'none' AS member_status
         FROM
         `addon_package_action_log`
         LEFT JOIN
         `addon_package` ON `addon_package`.`id` = `addon_package_action_log`.`package_id`
         LEFT JOIN
         `process` ON `process`.`id` = `addon_package_action_log`.`process_id`
         WHERE
         `addon_package_action_log`.`delegate_to` = ?
         AND
         `addon_package_action_log`.`closed` = 0
         ", [Auth::guard('customer')->user()->id, Auth::guard('customer')->user()->id, Auth::guard('customer')->user()->id, Auth::guard('customer')->user()->id, Auth::guard('customer')->user()->id, Auth::guard('customer')->user()->id, Auth::guard('customer')->user()->id, Auth::guard('customer')->user()->id, Auth::guard('customer')->user()->id]);
        $sort = [];
        foreach ($urgentIssues as $key => $part) {
            $sort[$key] = strtotime($part->urgency);
        }
        array_multisort($sort, SORT_ASC, $urgentIssues);
        return $urgentIssues;
    }

    public function getUserDetail($userId)
    {
        return Users::where('id', $userId)->first();
    }

    public function pendingAuditsAditee($checkPending = 0, $duration)
    {
        return AuditPlanAuditees::leftJoin('audit_plan_master', function ($join) {
            $join->on('audit_plan_auditees.audit_id', '=', 'audit_plan_master.id');
        })->select('audit_plan_master.audit_name', 'audit_plan_master.category', 'audit_plan_master.plan_date', 'audit_plan_master.status as auditStatus', 'audit_plan_auditees.audit_id as audit_id', DB::raw("'Auditee' as type"))
        ->where('audit_plan_auditees.user_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.status', '<>', $checkPending)
        ->whereBetween('audit_plan_master.plan_date', $duration)
        ->get();
    }

    public function pendingAuditsAditor($checkPending = 0, $duration)
    {
        return AuditPlanAuditors::leftJoin('audit_plan_master', function ($join) {
            $join->on('audit_team_member.audit_id', '=', 'audit_plan_master.id');
        })->select('audit_plan_master.audit_name', 'audit_plan_master.category', 'audit_plan_master.plan_date', 'audit_plan_master.status as auditStatus', 'audit_team_member.audit_id as audit_id', DB::raw("'Auditor' as type"))
        ->where('audit_team_member.auditor_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.status', '<>', $checkPending)
        ->whereBetween('audit_plan_master.plan_date', $duration)
        ->get();
    }

    public function pendingAuditsAditorAuditee($checkPending = 0)
    {
        $auditors = AuditPlanAuditors::leftJoin('audit_plan_master', function ($join) {
            $join->on('audit_team_member.audit_id', '=', 'audit_plan_master.id');
        })->select('audit_plan_master.audit_name', 'audit_plan_master.choose_lead_auditor', DB::raw(" IFNULL( `audit_plan_master`.`release_date` , `audit_plan_master`.`plan_date` ) as audit_date"), 'audit_plan_master.status as auditStatus', 'audit_team_member.audit_id as audit_id', DB::raw("'Auditor' as type"))
        ->where('audit_team_member.auditor_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.plan_date', '<>', null)
        ->where('audit_plan_master.status', '<>', $checkPending);

        $auditeesAndAuditors = AuditPlanAuditees::leftJoin('audit_plan_master', function ($join) {
            $join->on('audit_plan_auditees.audit_id', '=', 'audit_plan_master.id');
        })->select('audit_plan_master.audit_name', 'audit_plan_master.choose_lead_auditor', DB::raw(" IFNULL( `audit_plan_master`.`release_date` , `audit_plan_master`.`plan_date` ) as audit_date"), 'audit_plan_master.status as auditStatus', 'audit_plan_auditees.audit_id as audit_id', DB::raw("'Auditee' as type"))
        ->where('audit_plan_auditees.user_id', Auth::guard('customer')->user()->id)
        ->where('audit_plan_master.plan_date', '<>', null)
        ->where('audit_plan_master.status', '<>', $checkPending)
        ->unionAll($auditors);

        $getTotData = $auditeesAndAuditors->orderby('audit_date', 'desc')->get();
        return $getTotData;
    }

    public function getRiskOrOptTitle($id, $type)
    {
        if ($type == 'risk') {
            $detail = Risk::find($id);
            return $detail->risk;
        } else {
            $detail = Opportunity::find($id);
            return $detail->details;
        }
    }

    public function getContextStakeholderTitle($id, $type)
    {
        if ($type == 'context') {
            $detail = ContextAnalysis::find($id);
            return $detail->issue;
        } else {
            $detail = StakeholderAnalysis::find($id);
            return $detail->stakeholder_name.'/'.$detail->needs;
        }
    }

    public function openAndClosedIssues($category, $type, $months, $viewType = '', $userId = '')
    {
        $issues = [];
        $issues['months'] = [];
        $status = ($type == "open") ? 0 : 1;
        $externalStatus = ($type == "open") ? 1 : 2;
        $gap = monthGap($months);
        $userFilter= Auth::guard('customer')->user()->id;
        if ($userId != '') {
            $userFilter = $userId;
        }
        for ($m = $gap; $m >= 0; $m--) {
            $issues['months'][date('F', strtotime(date('Y-m') . ' -' . $m . ' month'))] = 0;
        }
        switch ($category) {
            case 'risk':
                $risks = RiskOpportunityActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $risks = $risks->where('delegate_to', $userFilter);
                }
                $risks = $risks->where('closed', $status)->where('type', 'risk')->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($risks as $risk) {
                    $issues['months'][$risk->months] = $risk->occurrence;
                }
                return $issues;
            case 'opportunity':
                $opportunities = RiskOpportunityActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $opportunities = $opportunities->where('delegate_to', $userFilter);
                }
                $opportunities = $opportunities->where('closed', $status)->where('type', 'opportunity')->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($opportunities as $opportunity) {
                    $issues['months'][$opportunity->months] = $opportunity->occurrence;
                }
                return $issues;
            case 'context':
                $contexts = ContextStakeholderActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $contexts = $contexts->where('deligate_to', $userFilter);
                }
                $contexts = $contexts->where('closed', $status)->where('type', 'context')->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($contexts as $context) {
                    $issues['months'][$context->months] = $context->occurrence;
                }
                return $issues;
            case 'stakeholder':
                $stakeholders = ContextStakeholderActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $stakeholders = $stakeholders->where('deligate_to', $userFilter);
                }
                $stakeholders = $stakeholders->where('closed', $status)->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($stakeholders as $stakeholder) {
                    $issues['months'][$stakeholder->months] = $stakeholder->occurrence;
                }
                return $issues;
            case 'external_issues':
                $externalIssues = ReportedIssuesDetails::leftJoin('reported_issues', function ($join) {
                    $join->on('reported_issues_details.reported_issue_id', '=', 'reported_issues.id');
                })->select(DB::raw('MONTHNAME(`reported_issues_details`.`created_at`) as months, count(`reported_issues_details`.`created_at`) as occurrence, reported_issues.company_id'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $externalIssues = $externalIssues->where('who', $userFilter);
                }
                $externalIssues = $externalIssues->where('reported_issues_details.status', $externalStatus)->whereBetween('reported_issues_details.created_at', $months)->groupBy('months')->get();
                foreach ($externalIssues as $externalIssue) {
                    $issues['months'][$externalIssue->months] = $externalIssue->occurrence;
                }
                return $issues;
            case 'competent_ratings':
                $competents = CompetentRatingsActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $competents = $competents->where('delegate_to', $userFilter);
                }
                $competents = $competents->where('closed', $status)->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($competents as $competent) {
                    $issues['months'][$competent->months] = $competent->occurrence;
                }
                return $issues;
            case 'supplier_ratings':
                $suppliers = SupplierEvaluationActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $suppliers = $suppliers->where('delegate_to', $userFilter);
                }
                $suppliers = $suppliers->where('closed', $status)->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($suppliers as $supplier) {
                    $issues['months'][$supplier->months] = $supplier->occurrence;
                }
                return $issues;
            case 'corrective_action':
               $correctives = SupplierCorrectiveActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $correctives = $correctives->where('delegate_to', $userFilter);
                }
                $correctives = $correctives->where('closed', $status)->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($correctives as $corrective) {
                    $issues['months'][$corrective->months] = $corrective->occurrence;
                }
                return $issues;
            case 'internal_issues':
                $internalIssues = [];

                if ($type == "open") {
                    $internalIssues = ActionDelegateMember::leftJoin('report_issue_action', function ($join) {
                        $join->on('action_delegate_members.action_id', '=', 'report_issue_action.id');
                    })->select(DB::raw('MONTHNAME(action_delegate_members.created_at) as months, count(action_delegate_members.created_at) as occurrence'))->where('action_delegate_members.status', $status)->where('company_id', Auth::guard('customer')->user()->company_id);
                    if (($viewType == '') || ($viewType != '' && $userId != '')) {
                        $internalIssues = $internalIssues->where('member', $userFilter);
                    }
                    $internalIssues = $internalIssues->whereBetween('action_delegate_members.created_at', $months)->groupBy('months')->get();
                }
                if ($type == "closed") {
                    $internalIssues = ActionDelegateMember::leftJoin('report_issue_action', function ($join) {
                        $join->on('action_delegate_members.action_id', '=', 'report_issue_action.id');
                    })->select(DB::raw('MONTHNAME(action_delegate_members.complete_date) as months, count(action_delegate_members.complete_date) as occurrence'))->where('action_delegate_members.status', $status)->where('company_id', Auth::guard('customer')->user()->company_id);
                    if (($viewType == '') || ($viewType != '' && $userId != '')) {
                        $internalIssues = $internalIssues->where('member', $userFilter);
                    }
                    $internalIssues = $internalIssues->whereBetween('action_delegate_members.complete_date', $months)->groupBy('months')->get();
                }
                foreach ($internalIssues as $internalIssue) {
                    $issues['months'][$internalIssue->months] = $internalIssue->occurrence;
                }
                return $issues;
            default:
                /// Risk Data
                $risks = RiskOpportunityActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $risks = $risks->where('delegate_to', $userFilter);
                }
                $risks = $risks->where('closed', $status)->where('type', 'risk')->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($risks as $risk) {
                    $issues['months'][$risk->months] += $risk->occurrence;
                }
                ///Opportunity Data
                $opportunities = RiskOpportunityActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $opportunities = $opportunities->where('delegate_to', $userFilter);
                }
                $opportunities = $opportunities->where('closed', $status)->where('type', 'opportunity')->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($opportunities as $opportunity) {
                    $issues['months'][$opportunity->months] += $opportunity->occurrence;
                }
                /// Context Data
                $contexts = ContextStakeholderActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $contexts = $contexts->where('deligate_to', $userFilter);
                }
                $contexts = $contexts->where('closed', $status)->where('type', 'context')->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($contexts as $context) {
                    $issues['months'][$context->months] += $context->occurrence;
                }
                // Stakeholder Data
                $stakeholders = ContextStakeholderActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $stakeholders = $stakeholders->where('deligate_to', $userFilter);
                }
                $stakeholders = $stakeholders->where('closed', $status)->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($stakeholders as $stakeholder) {
                    $issues['months'][$stakeholder->months] += $stakeholder->occurrence;
                }
                ///Reported Issue
                $externalIssues = ReportedIssuesDetails::leftJoin('reported_issues', function ($join) {
                    $join->on('reported_issues_details.reported_issue_id', '=', 'reported_issues.id');
                })->select(DB::raw('MONTHNAME(`reported_issues_details`.`created_at`) as months, count(`reported_issues_details`.`created_at`) as occurrence, reported_issues.company_id'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $externalIssues = $externalIssues->where('who', $userFilter);
                }
                $externalIssues = $externalIssues->where('reported_issues_details.status', $externalStatus)->whereBetween('reported_issues_details.created_at', $months)->groupBy('months')->get();
                foreach ($externalIssues as $externalIssue) {
                    $issues['months'][$externalIssue->months] += $externalIssue->occurrence;
                }
                //Competent rating
                $competents = CompetentRatingsActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $competents = $competents->where('delegate_to', $userFilter);
                }
                $competents = $competents->where('closed', $status)->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($competents as $competent) {
                    $issues['months'][$competent->months] += $competent->occurrence;
                }
                //Suplier Evalution
                $suppliers = SupplierEvaluationActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $suppliers = $suppliers->where('delegate_to', $userFilter);
                }
                $suppliers = $suppliers->where('closed', $status)->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($suppliers as $supplier) {
                    $issues['months'][$supplier->months] += $supplier->occurrence;
                }
                //Supplier Corrective
                $correctives = SupplierCorrectiveActionLog::select(DB::raw('MONTHNAME(created_at) as months, count(created_at) as occurrence'))->where('company_id', Auth::guard('customer')->user()->company_id);
                if (($viewType == '') || ($viewType != '' && $userId != '')) {
                    $correctives = $correctives->where('delegate_to', $userFilter);
                }
                $correctives = $correctives->where('closed', $status)->whereBetween('created_at', $months)->groupBy('months')->get();
                foreach ($correctives as $corrective) {
                    $issues['months'][$corrective->months] = $corrective->occurrence;
                }
                //Audit
                if ($type == "open") {
                    $internalIssues = ActionDelegateMember::leftJoin('report_issue_action', function ($join) {
                        $join->on('action_delegate_members.action_id', '=', 'report_issue_action.id');
                    })->select(DB::raw('MONTHNAME(action_delegate_members.created_at) as months, count(action_delegate_members.created_at) as occurrence'))->where('action_delegate_members.status', $status)->where('company_id', Auth::guard('customer')->user()->company_id);
                    if (($viewType == '') || ($viewType != '' && $userId != '')) {
                        $internalIssues = $internalIssues->where('member', $userFilter);
                    }
                    $internalIssues = $internalIssues->whereBetween('action_delegate_members.created_at', $months)->groupBy('months')->get();
                }

                if ($type == "closed") {
                    $internalIssues = ActionDelegateMember::leftJoin('report_issue_action', function ($join) {
                        $join->on('action_delegate_members.action_id', '=', 'report_issue_action.id');
                    })->select(DB::raw('MONTHNAME(action_delegate_members.complete_date) as months, count(action_delegate_members.complete_date) as occurrence'))->where('action_delegate_members.status', $status)->where('company_id', Auth::guard('customer')->user()->company_id);
                    if (($viewType == '') || ($viewType != '' && $userId != '')) {
                        $internalIssues = $internalIssues->where('member', $userFilter);
                    }
                    $internalIssues = $internalIssues->whereBetween('action_delegate_members.complete_date', $months)->groupBy('months')->get();
                }

                foreach ($internalIssues as $internalIssue) {
                    $issues['months'][$internalIssue->months] += $internalIssue->occurrence;
                }
                return $issues;
        }
    }

    public function insigntsPerProcess($months)
    {
        $data = [];
        $user = Auth::guard('customer')->user()->id;
        $companyId = Auth::guard('customer')->user()->company_id;
        $process = Process::where('owner_id', $user)->get();
        $findingTypes = FindingTypes::where('company_id', $companyId)->get();
        $processName = [];
        $insightData = [];


        $totData = ProcessToAudit::leftJoin('audit_plan_master', function ($join) {
            $join->on('process_to_audit.audit_id', '=', 'audit_plan_master.id');
        })
        ->leftJoin('process', function ($join) {
            $join->on('process.id', '=', 'process_to_audit.process_id');
        })
        ->leftJoin('audit_execution', function ($join) {
            $join->on('audit_plan_master.id', '=', 'audit_execution.audit_id');
        })
        ->leftJoin('finding_type', function ($join) {
            $join->on('audit_execution.finding_type_id', '=', 'finding_type.id');
        })
        ->leftJoin('report_issue_action', function ($join) {
            $join->on('report_issue_action.ref_id', '=', 'audit_execution.id');
        })
        ->leftJoin('action_delegate_members', function ($join) {
            $join->on('report_issue_action.id', '=', 'action_delegate_members.action_id');
        })
        ->select('process.id as processId', 'audit_execution.finding_type_id')
        ->where('process.owner_id', Auth::guard('customer')->user()->id)
        ->whereBetween('action_delegate_members.deadline', $months)
        ->get()
        ->toArray();
        foreach ($findingTypes as $k => $eachFinding) {
            foreach ($process as $eachProcess) {
                if ($k == 0) {
                    $processName[] = $eachProcess->name;
                }
                $insightData[$eachFinding->finding_type][] = $this->countFindingTypeByProcess($totData, array('processId' => $eachProcess->id,'finding_type_id' => $eachFinding->id));
            }
        }

        $data = ['process' => $processName, 'insight' => $insightData];
        return $data;
    }

    public function countFindingTypeByProcess($arr, $search)
    {
        $totFinding = 0;
        foreach ($arr as $k=>$v) {
            if (($v['processId'] == $search['processId']) && ($v['finding_type_id'] == $search['finding_type_id'])) {
                $totFinding++;
            }
        }
        return $totFinding;
    }

    public function openAndClosedIssuesPerInsight($months)
    {
        $user = Auth::guard('customer')->user()->id;
        $companyId = Auth::guard('customer')->user()->company_id;
        $findingTypes = FindingTypes::where('company_id', $companyId)->get();
        $processInsight = [];
        $totalData = AuditExecution::leftJoin('report_issue_action', function ($join) {
            $join->on('audit_execution.id', '=', 'report_issue_action.ref_id');
        })
        ->leftJoin('action_delegate_members', function ($join) {
            $join->on('report_issue_action.id', '=', 'action_delegate_members.action_id');
        })
        ->leftJoin('finding_type', function ($join) {
            $join->on('audit_execution.finding_type_id', '=', 'finding_type.id');
        })
        ->select('action_delegate_members.id', 'audit_execution.finding_type_id', 'action_delegate_members.complete_date')
        ->where('action_delegate_members.member', $user)
        ->whereBetween('action_delegate_members.deadline', $months)
        ->get()
        ->toArray();
        $findingNames = [];
        foreach ($findingTypes as $eachFinding) {
            $findingNames[] = $eachFinding->finding_type;
            $processInsight['open'][] = $this->countOpenDataPerInsight($totalData, array('finding_type' => $eachFinding->id));
            $processInsight['closed'][] = $this->countClosedDatePerInsight($totalData, array('finding_type' => $eachFinding->id));
        }

        $data = ['findingType' => $findingNames, 'processInsight' => $processInsight];
        return $data;
    }

    public function countOpenDataPerInsight($arr, $search)
    {
        $totFinding = 0;
        foreach ($arr as $k => $v) {
            if (($v['finding_type_id'] == $search['finding_type']) && ($v['complete_date'] == '')) {
                $totFinding++;
            }
        }
        return $totFinding;
    }

    public function countClosedDatePerInsight($arr, $search)
    {
        $totFinding = 0;
        foreach ($arr as $k => $v) {
            if (($v['finding_type_id'] == $search['finding_type']) && ($v['complete_date'] != '')) {
                $totFinding++;
            }
        }
        return $totFinding;
    }

    public function issuesPieChart($type, $months, $viewType = '', $userId = '')
    {
        $issues = [];
        $status = ($type == "open") ? 0 : 1;
        $externalStatus = ($type == "open") ? 1 : 2;
        $userFilter= Auth::guard('customer')->user()->id;
        if ($userId != '') {
            $userFilter = $userId;
        }
        $origins = SourceTypes::companyStatusCheck()->pluck('source_type', 'id');
        //Risk Issue
        $riskIssue = RiskOpportunityActionLog::companyCheck()->where('closed', $status)->where('type', 'risk')->whereBetween('created_at', $months);
        if (($viewType == '') || ($viewType != '' && $userId != '')) {
            $riskIssue = $riskIssue->where('delegate_to', $userFilter);
        }
        $issues[translate('words.risk')] =  $riskIssue->count();
        //Opportunity Issue
        $opportunityIssue = RiskOpportunityActionLog::companyCheck()->where('closed', $status)->where('type', 'opportunity')->whereBetween('created_at', $months);
        if (($viewType == '') || ($viewType != '' && $userId != '')) {
            $opportunityIssue = $opportunityIssue->where('delegate_to', $userFilter);
        }
        $issues[translate('words.opportunity')] = $opportunityIssue->count();
        //Context issue
        $contextIssue = ContextStakeholderActionLog::companyCheck()->where('closed', $status)->where('type', 'context')->whereBetween('created_at', $months);
        if (($viewType == '') || ($viewType != '' && $userId != '')) {
            $contextIssue = $contextIssue->where('deligate_to', $userFilter);
        }
        $issues[translate('words.context')] = $contextIssue->count();
        //Stackholder Issue
        $stackIssue = ContextStakeholderActionLog::companyCheck()->where('closed', $status)->where('type', 'stakeholder')->whereBetween('created_at', $months);
        if (($viewType == '') || ($viewType != '' && $userId != '')) {
            $stackIssue = $stackIssue->where('deligate_to', $userFilter);
        }
        $issues[translate('words.stakeholder')] = $stackIssue->count();
        //Audit Issue
        $issues['internal_issues'] = 0;
        if ($type == "open") {
            $auditReportedIssue = ActionDelegateMember::leftJoin('report_issue_action', function ($join) {
                $join->on('action_delegate_members.action_id', '=', 'report_issue_action.id');
            })->where('action_delegate_members.status', $status)->whereBetween('action_delegate_members.created_at', $months)->where('company_id', Auth::guard('customer')->user()->company_id);
            if (($viewType == '') || ($viewType != '' && $userId != '')) {
                $auditReportedIssue = $auditReportedIssue->where('member', $userFilter);
            }
            $issues[translate('words.audit_reported_issues')] = $auditReportedIssue->count();
        }

        if ($type == "closed") {
            $auditReportedIssue = ActionDelegateMember::leftJoin('report_issue_action', function ($join) {
                $join->on('action_delegate_members.action_id', '=', 'report_issue_action.id');
            })->where('action_delegate_members.status', $status)->whereBetween('action_delegate_members.created_at', $months)->where('company_id', Auth::guard('customer')->user()->company_id);
            if (($viewType == '') || ($viewType != '' && $userId != '')) {
                $auditReportedIssue = $auditReportedIssue->where('member', $userFilter);
            }
            $issues[translate('words.audit_reported_issues')] = $auditReportedIssue->count();
        }
        //Competent Issue
        $competentIssue = CompetentRatingsActionLog::companyCheck()->where('closed', $status)->whereBetween('created_at', $months);
        if (($viewType == '') || ($viewType != '' && $userId != '')) {
            $competentIssue = $competentIssue->where('delegate_to', $userFilter);
        }
        $issues[translate('words.competent')] = $competentIssue->count();
        //Supplier Issue
        $supplierIssue = SupplierEvaluationActionLog::companyCheck()->where('closed', $status)->whereBetween('created_at', $months);
        if (($viewType == '') || ($viewType != '' && $userId != '')) {
            $supplierIssue = $supplierIssue->where('delegate_to', $userFilter);
        }
        $issues[translate('words.supplier_action')] = $supplierIssue->count();
        // Supplier Corrective Issue
        $supplierCorrectiveIssue = SupplierCorrectiveActionLog::companyCheck()->where('closed', $status)->whereBetween('created_at', $months);
        if (($viewType == '') || ($viewType != '' && $userId != '')) {
            $supplierCorrectiveIssue = $supplierCorrectiveIssue->where('delegate_to', $userFilter);
        }
        $issues[translate('words.scar')] = $supplierCorrectiveIssue->count();
        //ReportedIssue
        foreach ($origins as $originKey => $originValue) {
            $reportedIssue = ReportedIssuesDetails::leftJoin('reported_issues', function ($join) {
                $join->on('reported_issues_details.reported_issue_id', '=', 'reported_issues.id');
            })->where('reported_issues.source_type', $originKey)
            ->where('reported_issues_details.status', $externalStatus)
            ->whereBetween('reported_issues_details.created_at', $months)
            ->where('reported_issues.company_id', Auth::guard('customer')->user()->company_id);
            if (($viewType == '') || ($viewType != '' && $userId != '')) {
                $reportedIssue = $reportedIssue->where('reported_issues_details.who', $userFilter);
            }
            $issues[$originValue] = $reportedIssue->count();
        }
        $legend = [];
        $issueCount =[];
        $backgroundColors =[];
        foreach ($issues as $eachlegend => $eachValue) {
            if ($eachValue > 0) {
                $legend [] = $eachlegend;
                $issueCount [] = $eachValue;
                $backgroundColors[] = generateRgb();
            }
        }
        $issues = implode(",", $issueCount);
        $legends = json_encode($legend);
        $backgroundColors = json_encode($backgroundColors);
        return array('issues' => $issues, 'legends' => $legends, 'backgroundColors' => $backgroundColors);
    }
}
