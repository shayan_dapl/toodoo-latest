<?php 

namespace App\Repositories;

use App\Models\Languages;

/**
 * Language repository for fetching Language related data
 */
class Language
{
    public function getLanguage($segment, $field)
    {
        return Languages::where('segment', $segment)->where('field', $field);
    }
}
