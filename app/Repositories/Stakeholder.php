<?php 

namespace App\Repositories;

use App;
use Auth;
use DB;

/**
 * Stakeholder repository for fetching all dashboard data
 */
class Stakeholder
{
    public function getstackholderdata()
    {
        $result = DB::select(
            'select stakeholder_analysis.id as id, "0" as occurrence, concat(stakeholder_category.name, "-", stakeholder_analysis.stakeholder_name) as name, detail as category, impact as price, interest as rating, user_master.name as delegate, context_stakeholder_action_log.deadline as deadline, context_stakeholder_action_log.closed as status from `stakeholder_analysis` 
                inner join `stakeholder_category` on `stakeholder_category`.`id` = `stakeholder_analysis`.`category_id` 
                left join `context_stakeholder_action_log` on `stakeholder_analysis`.`id` =
                CASE when(`context_stakeholder_action_log`.`type` ="stakeholder") THEN
                `context_stakeholder_action_log`.`contxt_stakehold_id` end
                left join `user_master` on `context_stakeholder_action_log`.`deligate_to` = `user_master`.`id`
                where `stakeholder_analysis`.`company_id` = ' . Auth::guard('customer')->user()->company_id . ' order by impact, interest'
            );

        $arr = [];
        foreach ($result as $row) {
            $arr[] = (array) $row;
        }
        
        return $arr;
    }
}
