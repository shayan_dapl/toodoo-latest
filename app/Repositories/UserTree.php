<?php 

namespace App\Repositories;

use App\Models\Position;
use Auth;
use DB;

/**
 * UserTree repository for fetching all user tree related data
 */
class UserTree
{
    public function getPositionByCompany($positionId)
    {
        return Position::where('company_id', Auth::guard('customer')->user()->company_id)
            ->where('id', $positionId);
    }

    public function getOrgStructByID($id)
    {
        return DB::table('organization_structure')->where('id', $id);
    }

    public function getOrgStructByParent($id)
    {
        return DB::table('organization_structure')->where('parent', $id);
    }

    public function getOrgStructByCompany($user_id, $company_id)
    {
        return DB::table('organization_structure')
            ->where('user_id', $user_id)
            ->where('company_id', $company_id);
    }

    public function getPositionById($id)
    {
        return Position::where('id', $id);
    }

    public function getPositionByParent($id)
    {
        return Position::where('parent_id', $id);
    }
}
