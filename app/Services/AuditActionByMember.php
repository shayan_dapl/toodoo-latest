<?php 

namespace App\Services;

use Config;

class AuditActionByMember
{
    public function iso($lang)
    {
        $langs = Config::get('settings.lang');
        return 'iso_no_' . $langs[$lang];
    }

    public function name($lang)
    {
        $langs = Config::get('settings.lang');
        return 'name_' . $langs[$lang];
    }
}
