<?php 

namespace App\Services;

use Config;

class AuditExecuteForm
{
    public function iso_no($lang)
    {
        $langs = Config::get('settings.lang');
        return 'iso_no_' . $langs[$lang];
    }

    public function initials($str)
    {
        $ret = '';
        if (preg_match('!\(([^\)]+)\)!', $str, $match)) {
            $ret = $match[1];
        } else {
            foreach (explode(' ', $str) as $word) {
                $ret .= strtoupper($word[0]);
            }
        }
           
        return $ret;
    }

    public function status($val)
    {
        $statusList = [
            '0' => translate('words.open'),
            '1' => translate('words.closed'),
        ];
        return $statusList[$val];
    }
}
