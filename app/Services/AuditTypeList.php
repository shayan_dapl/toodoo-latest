<?php 

namespace App\Services;

class AuditTypeList
{
    public function clause($language, $auditType)
    {
        $clause = [
            'nl' => ($auditType->clause . ' ' . $auditType->name_nl),
            'en' => ($auditType->clause . ' ' . $auditType->name_en),
            'fr' => ($auditType->clause . ' ' . $auditType->name_fr),
        ];

        return $clause[$language];
    }

    public function clausetitle($language, $auditType)
    {
        $clause = [
            'nl' => ($auditType->name_nl),
            'en' => ($auditType->name_en),
            'fr' => ($auditType->name_fr),
        ];

        return $clause[$language];
    }

    public function status($stat)
    {
        $status = array('0' => 'Inactive', '1' => 'Active', '2' => 'Blocked');
        return $status[$stat];
    }
}
