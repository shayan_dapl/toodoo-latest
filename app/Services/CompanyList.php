<?php 

namespace App\Services;

class CompanyList
{
    public function status($val)
    {
        $statusList = [
            '0' => translate('words.inactive'),
            '1' => translate('words.active'),
            '2' => translate('words.blocked'),
            '3' => translate('words.removed'),
        ];
        return $statusList[$val];
    }
}
