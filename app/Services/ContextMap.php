<?php 

namespace App\Services;

class ContextMap
{
    public function color($status)
    {
        switch ($status) {
            case 'low':
                return "#239b56";
            case 'medium':
                return "#e67e22";
            case 'high':
                return "#dc2132";
        }
    }
}
