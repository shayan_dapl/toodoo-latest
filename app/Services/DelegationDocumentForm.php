<?php 

namespace App\Services;

class DelegationDocumentForm
{
    public function file($doc)
    {
        $extension = extension($doc);
        return getfileType($extension, $doc, 'delegation_docs');
    }
}
