<?php 

namespace App\Services;

class ExecDocumentForm
{
    public function file($doc)
    {
        $extension = extension($doc);
        return getfileType($extension, $doc, 'execution_docs');
    }

    public function efile($doc)
    {
        $extension = extension($doc);
        return getfileType($extension, $doc, 'execution_docs');
    }
}
