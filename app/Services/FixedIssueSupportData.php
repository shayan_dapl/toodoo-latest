<?php 

namespace App\Services;

class FixedIssueSupportData
{
    public function file($doc)
    {
        $extension = extension($doc);
        return getfileType($extension, $doc, 'external_issue_fixing_docs');
    }
}
