<?php 

namespace App\Services;

class HelperPositionTree
{
    public function technician($type)
    {
        switch ($type) {
            case '1':
                return 'text-danger';
            default:
                return 'text-system';
        }
    }
}
