<?php 

namespace App\Services;

class HomeDashboard
{
    public function clauses($eachline)
    {
        $clauses = [];
        if ($eachline->immidiate_actions) {
            $clauses[] = 'immidiate_action';
        }
        if ($eachline->root_cause_analysis) {
            $clauses[] = 'root_cause';
        }
        if ($eachline->corrective_measure) {
            $clauses[] = 'corrective_action';
        }
        if ($eachline->effectiveness) {
            $clauses[] = 'effectiveness';
        }

        return $clauses;
    }

    public function findPending($each)
    {
        $findPending = 'none';
        if ($each->member_status != 'none') {
            $memberStatus = explode(',', $each->member_status);
            $findPending = array_search(0, $memberStatus);
        }

        return $findPending;
    }

    public function actionPending($each)
    {
        $actionPending = '';
        if ($each->member_status != 'none') {
            $memberStatus = explode(',', $each->member_status);
            $findPending = array_search(0, $memberStatus);
            if ($each->action_pending != 'none') {
                $actionPending = explode(',', $each->action_pending)[$findPending];
            }
        }

        return $actionPending;
    }

    public function divwdth()
    {
        $isProcessOwner = \Auth::guard('customer')->user()->can_process_owner;
        $isAuditor = \Auth::guard('customer')->user()->is_auditor;
        if ($isProcessOwner == 1 && $isAuditor == 1) {
            $divwidth = 'col-sm-3';
        } elseif ($isProcessOwner == 1 || $isAuditor == 1) {
            $divwidth = 'col-sm-4';
        } else {
            $divwidth = 'col-sm-6';
        }
        return $divwidth;
    }
}
