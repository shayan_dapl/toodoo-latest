<?php 

namespace App\Services;

class IsoClauseList
{
    public function iso($language, $clauseDetail)
    {
        $iso = [
            'nl' => $clauseDetail->iso_no_nl,
            'en' => $clauseDetail->iso_no_en,
            'fr' => $clauseDetail->iso_no_fr,
        ];

        return $iso[$language];
    }
}
