<?php 

namespace App\Services;

use App\Models\MasterKpiData;
use App\Models\ProcessKpiData;

class KpiUnit
{
    public function kpiunit($language, $kpidata)
    {
        $kpiunit = [
            'nl' => $kpidata->name_nl,
            'en' => $kpidata->name_en,
            'fr' => $kpidata->name_fr,
        ];

        return $kpiunit[$language];
    }

    public function weeks($from, $to)
    {
        $startTime = strtotime($from);
        $endTime = strtotime($to);
        $weeks = [];
        while ($startTime < $endTime) {
            $weeks[] = date('Y-W', $startTime);
            $startTime += strtotime('+1 week', 0);
        }
        return $weeks;
    }

    public function months($from, $to)
    {
        $startTime = strtotime($from);
        $endTime = strtotime($to);
        $months = [];
        while ($startTime < $endTime) {
            $months[] = date('Y-m', $startTime);
            $startTime += strtotime('+1 month', 0);
        }
        return $months;
    }

    public function quarters($from, $to)
    {
        $startTime = strtotime($from);
        $endTime = strtotime($to);
        $quarters = [];
        while ($startTime < $endTime) {
            $year = date('Y', $startTime);
            $month = date('n', $startTime);
            $quarters[] = $year .'-'. ceil($month / 3);
            $startTime = strtotime('+3 months', $startTime);
        }
        return $quarters;
    }
}
