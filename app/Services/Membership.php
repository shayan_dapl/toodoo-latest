<?php 

namespace App\Services;

use Auth;

class Membership
{
    public function user($details)
    {
        if (!empty($details) && $details->purchased()->where('type', 'user')->first()) {
            return $details->purchased()->where('type', 'user')->first()->package_id;
        } else {
            return null;
        }
    }

    public function storage($details)
    {
        if (!empty($details) && $details->purchased()->where('type', 'storage')->first()) {
            return $details->purchased()->where('type', 'storage')->first()->package_id;
        } else {
            return null;
        }
    }

    public function space($size)
    {
        return $size/1024;
    }

    public function spacemb($size)
    {
        return $size/(1024*1024);
    }

    public function calcwidth($now, $total)
    {
        return ($now / $total)*100;
    }

    public function total($data, $type = null, $tenure = 1)
    {
        $total = 0;
        $vat = 0 ;
        $nextSubscription = Auth::guard('customer')->user()->company->next_renewal_date;
        $today = date_create(date("Y-m-d"));
        $nextSubscription = date_create($nextSubscription);
        $remainingDays = date_diff($today, $nextSubscription);
        //Only for addon
        $currentSubscriptionAmt = Auth::guard('customer')->user()->company->subscription_amount;
        $nextSubscriptionAmtWithoutVat = Auth::guard('customer')->user()->company->subscription_amount;
        $nextSubscriptionAmt = 0;
        $per = \Config::get('settings.membership_monthly_per');
        switch ($type) {
            case 'iso':
                if (!empty($data['iso'])) {
                    foreach ($data['iso'] as $each) {
                        $calday = ($tenure == 1) ? 30 : 365;
                        $calcPrice = ($tenure == 1) ? $each->price + ($each->price * $per)/100 : $each->price * 12;
                        $total += (($calcPrice * 1)/$calday) * $remainingDays->days;
                        $nextSubscriptionAmt += $calcPrice;
                    }
                }
                break;
            case 'user':
                if (!empty($data['user'])) {
                    $calday = ($tenure == 1) ? 30 : 365;
                    $calcPrice = ($tenure == 1) ? $data['user']->price_per_month + ($data['user']->price_per_month * $per)/100 : $data['user']->price_per_month * 12;
                    $total += (($calcPrice * 1)/$calday) * $remainingDays->days;
                    $nextSubscriptionAmt += $calcPrice;
                }
                break;
            case 'storage':
                if (!empty($data['storage'])) {
                    $calday = ($tenure == 1) ? 30 : 365;
                    $calcPrice = ($tenure == 1) ? $data['storage']->price_per_month + ($data['storage']->price_per_month * $per)/100 : $data['storage']->price_per_month * 12;
                    $total += (($calcPrice * 1)/$calday) * $remainingDays->days;
                    $nextSubscriptionAmt += $calcPrice;
                }
                break;
            default:
                if (!empty($data['iso'])) {
                    foreach ($data['iso'] as $each) {
                        $total += $each->price * 1;
                    }
                }
                if (!empty($data['user'])) {
                    $total += $data['user']->price_per_month * 1;
                }
                if (!empty($data['storage'])) {
                    $total += $data['storage']->price_per_month * 1;
                }
                break;
        }
        if ($type == '') {
            if ($tenure == 1) {
                $per = \Config::get('settings.membership_monthly_per');
                $total = $total + ($total * $per)/100;
                $nextSubscriptionAmt = $nextSubscriptionAmt + ($nextSubscriptionAmt * $per)/100;
            } else {
                $total = $total * 12 ;
                $nextSubscriptionAmt = $nextSubscriptionAmt * 12;
            }
        }
        $nextSubscriptionAmtWithoutVat = $currentSubscriptionAmt + $nextSubscriptionAmt;
        $nextSubscriptionAmt += $currentSubscriptionAmt;
        if ($data['company']->company->is_vat == 0) {
            $vat = ($total * $data['company']->company->countries->vat_percent) / 100;
            $nextSubscriptionAmt += ($nextSubscriptionAmt * $data['company']->company->countries->vat_percent) / 100;
        }
        if ($type == null) {
            $nextSubscriptionAmt = 0;
        }
        $returnArr = ['total'=> $total, 'vat' => number_format($vat, 2), 'nextPayment' => $nextSubscriptionAmt, 'nextPaymentWithoutVat' => $nextSubscriptionAmtWithoutVat, 'remaining' => $remainingDays->days];
        return $returnArr;
    }

    public function isoPrice($data)
    {
        $nextSubscription = Auth::guard('customer')->user()->company->next_renewal_date;
        $today = date_create(date("Y-m-d"));
        $nextSubscription = date_create($nextSubscription);
        $remainingDays = date_diff($today, $nextSubscription);
        return (($data->price * 1)/30) * $remainingDays->days;
    }

    public function countryName($name)
    {
        $langName = 'name_'.session('lang');
        return $name->$langName;
    }

    public function packageprice($data, $tenure = 1)
    {
        $packagePrice = 0;
        if (!empty($data)) {
            foreach ($data as $each) {
                $packagePrice = $packagePrice + $each->price;
            }
            if ($tenure == 1) {
                $per = \Config::get('settings.membership_monthly_per');
                $packagePrice = $packagePrice + ($packagePrice * $per)/100;
            } else {
                $packagePrice = $packagePrice * 12;
            }
        }
        return $packagePrice;
    }

    public function monthlyPkgCalc($data)
    {
        $result = 0;
        $per = \Config::get('settings.membership_monthly_per');
        if (!empty($data)) {
            $calc = ($data * $per)/100;
            $result = $data + $calc;
        }
        return $result;
    }

    public function tenureprice($price, $tenure)
    {
        if ($tenure == 1) {
            $per = \Config::get('settings.membership_monthly_per');
            $price = $price + ($price * $per)/100;
        } else {
            $price = $price * 12;
        }
        return number_format($price, 2);
    }

    public function tenurepricenoformet($price, $tenure)
    {
        if ($tenure == 1) {
            $per = \Config::get('settings.membership_monthly_per');
            $price = $price + ($price * $per)/100;
        } else {
            $price = $price * 12;
        }
        return $price;
    }

    public function packageName($name)
    {
        $langName = 'name_'.session('lang');
        return $name->$langName;
    }

    public function packageDescription($desc)
    {
        $langDesc = 'description_'.session('lang');
        return $desc->$langDesc;
    }
}
