<?php 

namespace App\Services;

class ProcessCategorized
{
    public function name($language, $eachDetail)
    {
        $name = [
            'nl' => $eachDetail->name_nl,
            'en' => $eachDetail->name_en,
            'fr' => $eachDetail->name_fr,
        ];

        return $name[$language];
    }

    public function processes($eachDetail)
    {
        return $eachDetail->processes()->where('company_id', \Auth::guard('customer')->user()->company_id)->where('status', 1)->where('parent_id', 0)->orderBy('serial', 'asc')->get();
    }

    public function inactiveprocesses($eachDetail)
    {
        return $eachDetail->processes()->where('company_id', \Auth::guard('customer')->user()->company_id)->where('status', 0)->where('parent_id', 0)->orderBy('serial', 'asc')->get();
    }
}
