<?php 

namespace App\Services;

class ProcessCompetent
{
    public function rating($ratings, $position, $process, $step, $org)
    {
        if (!empty($ratings)) {
            foreach ($ratings as $each) {
                if ($each['process_id'] == $process && $each['step_id'] == $step && $each['org_id'] == $org && $each['is_latest'] == 1) {
                    return $each['rating_name'];
                }
            }
        } else {
            return null;
        }
    }

    public function ratingid($ratings, $position, $process, $step, $org)
    {
        if (!empty($ratings)) {
            foreach ($ratings as $each) {
                if ($each['process_id'] == $process && $each['step_id'] == $step && $each['org_id'] == $org && $each['is_latest'] == 1) {
                    return $each['rating'];
                }
            }
        } else {
            return 0;
        }
    }

    public function fieldwidth($dtcount)
    {
        return $dtwdth = 100/$dtcount;
    }
}
