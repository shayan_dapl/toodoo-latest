<?php 

namespace App\Services;

class ProcessDetailView
{
    public function lang($val)
    {
        $lang = [
            "nl" => translate('words.nl'),
            "en" => translate('words.en'),
            "fr" => translate('words.fr'),
            "ml" => translate('words.ml')
        ];
        return $lang[$val];
    }

    public function level($val)
    {
        $level = [
            "H" => translate('form.high'),
            "M" => translate('form.medium'),
            "L" => translate('form.low')
        ];
        return $level[$val];
    }

    public function file($doc, $path)
    {
        $extension = extension($doc);
        return getfileType($extension, $doc, $path);
    }
}
