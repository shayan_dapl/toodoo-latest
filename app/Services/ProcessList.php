<?php 

namespace App\Services;

class ProcessList
{
    public function status($val)
    {
        $statusList = [
            '0' => translate('words.inactive'),
            '1' => translate('words.active'),
            '2' => translate('words.blocked'),
            '3' => translate('words.removed'),
        ];
        return $statusList[$val];
    }

    public function name($language, $eachDetail)
    {
        $name = [
            'nl' => $eachDetail->category->name_nl,
            'en' => $eachDetail->category->name_en,
            'fr' => $eachDetail->category->name_fr,
        ];

        return $name[$language];
    }
}
