<?php 

namespace App\Services;

class ProcessOpportunityMap
{
    public function color($status)
    {
        switch ($status) {
            case 'L':
                return "#239b56";
            case 'M':
                return "#e67e22";
            case 'H':
                return "#dc2132";
        }
    }

    public function riskstat($status)
    {
        switch ($status) {
            case 'L':
                return translate('form.low_risk');
            case 'M':
                return translate('form.medium_risk');
            case 'H':
                return translate('form.high_risk');
        }
    }

    public function oppstat($status)
    {
        switch ($status) {
            case 'L':
                return translate('form.low_opportuity');
            case 'M':
                return translate('form.medium_opportuity');
            case 'H':
                return translate('form.high_opportuity');
        }
    }
}
