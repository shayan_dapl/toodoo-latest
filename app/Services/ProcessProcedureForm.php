<?php 

namespace App\Services;

class ProcessProcedureForm
{
    public function file($doc, $path)
    {
        $extension = extension($doc);
        return getfileType($extension, $doc, $path);
    }
}
