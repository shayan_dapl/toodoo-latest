<?php 

namespace App\Services;

class ProcessRaciMap
{
    public function role($val)
    {
        $rolesArr = explode("|", $val);
        $roles = [];

        if (in_array('1', $rolesArr)) {
            $roles[] = "R";
        }
        if (in_array('3', $rolesArr)) {
            $roles[] = "C";
        }
        if (in_array('4', $rolesArr)) {
            $roles[] = "I";
        }

        return implode(",", $roles);
    }
}
