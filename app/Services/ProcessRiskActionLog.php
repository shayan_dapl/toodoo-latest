<?php 

namespace App\Services;

class ProcessRiskActionLog
{
    public function status($stat)
    {
        $status = [
            '' => '',
            'L' => translate('form.low'),
            'M' => translate('form.medium'),
            'H' => translate('form.high'),
        ];
        return $status[$stat];
    }

    public function file($doc, $path)
    {
        $extension = extension($doc);
        return getfileType($extension, $doc, $path);
    }
}
