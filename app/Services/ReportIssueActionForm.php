<?php 

namespace App\Services;

use Config;

class ReportIssueActionForm
{
    public function iso($language)
    {
        $lang = Config::get('settings.lang');
        $iso = 'iso_no_' . $lang[$language];

        return $iso;
    }

    public function name($language)
    {
        $lang = Config::get('settings.lang');
        $name = 'name_' . $lang[$language];

        return $name;
    }
}
