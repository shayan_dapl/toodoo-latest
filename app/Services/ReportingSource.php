<?php 

namespace App\Services;

use Config;

class ReportingSource
{
    public function assignedusers($assignedData)
    {
        $data = '';
        foreach ($assignedData as $eachData) {
            $data .= '<b>'.$eachData->user->name."</b></br>";
        }
        return $data;
    }
}
