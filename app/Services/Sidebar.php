<?php 

namespace App\Services;

class Sidebar
{
    public function segment()
    {
        return \Request::segment(1) . '/' . \Request::segment(2);
    }

    public function strategySegment()
    {
        return [
            'context/context-analysis-form',
            'context/context-map',
            'stakeholder-analysis/list',
            'stakeholder-analysis/map',
            'process/riskmap',
            'process/opportunitymap',
        ];
    }

    public function contextsSegment()
    {
        return [
            'context/context-analysis-form',
            'context/context-map'
        ];
    }

    public function stakeholderSegment()
    {
        return [
            'stakeholder-analysis/list',
            'stakeholder-analysis/map',
        ];
    }

    public function organizationSegment()
    {
        return [
            'user/structure',
            'process/racimap',
            'process/job-description-metrix'
        ];
    }

    public function processSegment()
    {
        return [
            'process/categorized',
            'process/documentmap',
            'process/regulationmap'
        ];
    }

    public function auditSegment()
    {
        return [
            'audit/report-issue'
        ];
    }

    public function monitoringSegment()
    {
        return [
            'audit/supplier-evaluation',
            'master-kpi/kpi-relation-graph',
            'process/resourcemap',
            'process/resource-calender'
        ];
    }

    public function adminSegment()
    {
        return [
            'department/list',
            'position/list',
            'user/list',
            'process/list',
            'process/doctype',
            'process/regulationtype',
            'audit/finding-type-list',
            'audit/source-type-list',
            'context/list',
            'stakeholder-analysis/category',
            'settings/risk-opp-action',
            'settings/company',
            'process-step/resources',
            'branch/list',
            'rating/list',
            'master-kpi/list',
            'rating/action-type',
            'supplier/list',
            'supplier/parameters',
            'supplier/ratings',
            'process/resourcestype',
            'package/location',
            'package/situation',
            'package/effective-env',
            'package/category',
            'package/aspect-type',
            'package/theme-env',
            'package/evaluation',
            'package/parameter',
            'package/rating',
            'package/'
        ];
    }

    public function contextSegment()
    {
        return [
            'context/context-subject-list',
            'context/context-analysis-form',
        ];
    }

    public function stakeSegment()
    {
        return [
            'stakeholder-analysis/category',
            'rating/list'
        ];
    }

    public function loggedUserStatus()
    {
        $type = [];
        if (\Auth::guard('customer')->check()) {
            if (\Auth::guard('customer')->user()->type == 2) {
                $type[] = 1;
            }

            if (\Auth::guard('customer')->user()->can_process_owner == 1) {
                $type[] = 2;
            }

            if (\Auth::guard('customer')->user()->is_auditor == 1) {
                $type[] = 3;
            }
        }
        return implode(",", $type);
    }
}
