<?php 

namespace App\Services;

class SupplierEvaluation
{
    public function average($each, $parameters)
    {
        $result = 0;
        if ($parameters->count() > 0) {
            foreach ($parameters as $eachParam) {
                if (!empty($each->action())) {
                    foreach ($each->action()->rates as $eachRate) {
                        if ($eachRate->parameter_id == $eachParam->id) {
                            $result += $eachRate->rate->rating;
                        }
                    }
                }
            }

            return number_format($result/count($parameters), 1, ',', null);
        } else {
            return $result;
        }
    }

    public function percent($each, $parameters)
    {
        $calc = 0;
        $result = 0;
        if ($parameters->count() > 0) {
            foreach ($parameters as $eachParam) {
                if (!empty($each->action())) {
                    foreach ($each->action()->rates as $eachRate) {
                        if ($eachRate->parameter_id == $eachParam->id) {
                            $result += $eachRate->rate->rating;
                        }
                    }
                }
            }
            $maxRate = \App\Models\SupplierRating::companyStatusCheck()->orderBy('rating', 'DESC')->first()->rating;
            $calc = (($result/count($parameters)) / $maxRate) * 100;

            return number_format($calc, 1, ',', null) . '%';
        } else {
            return $calc;
        }
    }
}
