<?php 

namespace App\Services;

class UserForm
{
    public function issueType($type)
    {
        $typeArr = [
            'immidiate_action' => translate('form.immidiate_actions'),
            'root_cause' => translate('form.root_cause_analysis'),
            'corrective_action' => translate('form.corrective_action'),
            'effectiveness' => translate('form.effectiveness'),
        ];

        return $typeArr[$type];
    }
}
