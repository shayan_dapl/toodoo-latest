<?php 

namespace App\Services;

class UserList
{
    public function status($val)
    {
        $status = [
            '0' => translate('words.inactive'),
            '1' => translate('words.active'),
            '2' => translate('words.blocked'),
            '3' => translate('form.out_of_service'),
        ];
        return $status[$val];
    }
}
