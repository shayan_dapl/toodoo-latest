<?php 

namespace App\Services;

class VideoCategoryList
{
    public function vidCat($language, $catDetail)
    {
        $vidCat = [
            'nl' => $catDetail->vid_cat_nl,
            'en' => $catDetail->vid_cat_en,
            'fr' => $catDetail->vid_cat_fr,
        ];

        return $vidCat[$language];
    }
}
