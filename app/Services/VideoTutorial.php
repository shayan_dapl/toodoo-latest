<?php 

namespace App\Services;

class VideoTutorial
{
    public function category($language, $eachDetail)
    {
        $title = [
            'nl' => $eachDetail['vid_cat_nl'],
            'en' => $eachDetail['vid_cat_en'],
            'fr' => $eachDetail['vid_cat_fr'],
        ];

        return $title[$language];
    }

    public function title($language, $eachDetail)
    {
        $title = [
            'nl' => $eachDetail['title_nl'],
            'en' => $eachDetail['title_en'],
            'fr' => $eachDetail['title_fr'],
        ];

        return $title[$language];
    }

    public function description($language, $eachDetail)
    {
        $description = [
            'nl' => $eachDetail['description_nl'],
            'en' => $eachDetail['description_en'],
            'fr' => $eachDetail['description_fr'],
        ];

        return $description[$language];
    }

    public function path($language, $eachDetail)
    {
        if ($eachDetail['media_type'] == 'video') {
            $title = [
                'nl' => $eachDetail['path_nl'],
                'en' => $eachDetail['path_en'],
                'fr' => $eachDetail['path_fr'],
            ];
        } else {
            $title = [
                'nl' => ($eachDetail['path_nl'] != null && \File::isFile('storage/tutorial/'.$eachDetail['path_nl'])) ? 'storage/tutorial/'.$eachDetail['path_nl'] : 'image/no_image.png',
                'en' => ($eachDetail['path_en'] != null && \File::isFile('storage/tutorial/'.$eachDetail['path_en'])) ? 'storage/tutorial/'.$eachDetail['path_en'] : 'image/no_image.png',
                'fr' => ($eachDetail['path_fr'] != null && \File::isFile('storage/tutorial/'.$eachDetail['path_fr'])) ? 'storage/tutorial/'.$eachDetail['path_fr'] : 'image/no_image.png',
            ];
        }

        return $title[$language];
    }

    public function limitText($text, $limit = 50)
    {
        if (str_word_count(strip_tags($text), 0) > $limit) {
            $words = str_word_count(strip_tags($text), 2);
            $pos = array_keys($words);
            $text = substr(strip_tags($text), 0, $pos[$limit]) . '... ';
        }
        return strip_tags($text);
    }
}
