<?php

namespace App\Traits;

use Auth;

trait ActionDelegateMemberTraits
{
    public function scopeActionScope($query, $action)
    {
        return $query->whereIn('action_id', $action);
    }

    public function scopeCheckIssueType($query, $type)
    {
        return $query->where('issue_type', $type);
    }

    public function scopeCheckMember($query)
    {
        return $query->where('member', Auth::guard('customer')->user()->id);
    }
}
