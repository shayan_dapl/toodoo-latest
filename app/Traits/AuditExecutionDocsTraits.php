<?php

namespace App\Traits;

use Auth;

trait AuditExecutionDocsTraits
{
    public function scopeCheckExecutionId($query, $executionId)
    {
        return $query->where('execution_id', $executionId);
    }
}
