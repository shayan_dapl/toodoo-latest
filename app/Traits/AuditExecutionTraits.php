<?php

namespace App\Traits;

use Auth;

trait AuditExecutionTraits
{
    public function scopeCheckAuditId($query, $auditId)
    {
        return $query->where('audit_id', $auditId);
    }

    public function scopeCheckExecutionId($query, $executionId)
    {
        return $query->where('id', $executionId);
    }
}
