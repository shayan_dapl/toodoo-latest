<?php

namespace App\Traits;

use Auth;

trait AuditPlanMasterTraits
{
    public function scopeCheckAuditId($query, $auditId)
    {
        return $query->where('id', $auditId);
    }
}
