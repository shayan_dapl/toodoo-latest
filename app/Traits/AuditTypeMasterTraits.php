<?php

namespace App\Traits;

trait AuditTypeMasterTraits
{
    public function scopeParentType($query, $refId)
    {
        return $query->where('parent_id', 0)->where('iso_ref_id', $refId);
    }
}
