<?php

namespace App\Traits;

use App\Models\DepartmentBranch;

trait BranchTraits
{
    public function scopeUnused($query)
    {
        $departmentBranch = DepartmentBranch::companyCheck()->pluck('branch_id')->toArray();
        return $query->whereNotIn('id', $departmentBranch)->where('status', '<>', 4);
    }
}
