<?php

namespace App\Traits;

use Auth;

trait CompanyCheckTraits
{
    public function scopeCompanyCheck($query)
    {
        return $query->where('company_id', Auth::user()->company_id);
    }

    public function scopeCompanyStatusCheck($query)
    {
        return $query->where('company_id', Auth::user()->company_id)->where('status', 1);
    }
}
