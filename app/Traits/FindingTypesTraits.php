<?php

namespace App\Traits;

use Auth;

trait FindingTypesTraits
{
    public function scopeCheckFindingId($query, $findingId)
    {
        return $query->where('id', $findingId);
    }
}
