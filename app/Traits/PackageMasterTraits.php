<?php

namespace App\Traits;

use Auth;

trait PackageMasterTraits
{
    public function scopeBasePrice($query)
    {
        return $query->where('is_base', 1)->pluck('price');
    }
}
