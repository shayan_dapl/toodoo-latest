<?php

namespace App\Traits;

use Auth;

trait PositionTraits
{
    public function scopeUnusedPositions($query)
    {
        return $query->where('is_special_position', 0)->get()->pluck('name_with_details', 'id')->prepend(translate('form.position'), 0);
    }

    public function scopeUnusedSpecialPositions($query)
    {
        return $query->where('parent_id', '<>', null)->where('is_special_position', 1)->get()->pluck('name_with_details', 'id')->prepend(translate('form.position'), 0);
    }
}
