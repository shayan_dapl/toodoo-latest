<?php

namespace App\Traits;

use Auth;

trait ProcessTraits
{
    public function scopeActive($query)
    {
        return $query->where('company_id', Auth::guard('customer')->user()->company_id)->where('status', 1);
    }
}
