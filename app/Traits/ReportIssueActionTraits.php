<?php

namespace App\Traits;

use Auth;

trait ReportIssueActionTraits
{
    public function scopeWithRef($query, $ref)
    {
        return $query->where('company_id', Auth::guard('customer')->user()->company_id)->where('ref_id', $ref);
    }
}
