<?php

namespace App\Traits;

use Auth;

trait RiskOpportunityActionLogTraits
{
    public function scopeCheckRiskId($query, $riskId, $type)
    {
        return $query->where('risk_opp_id', $riskId)->where('type', $type);
    }

    public function scopeCheckOpptId($query, $oppId, $type)
    {
        return $query->where('risk_opp_id', $oppId)->where('type', $type);
    }
}
