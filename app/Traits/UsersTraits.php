<?php

namespace App\Traits;

use Auth;

trait UsersTraits
{
    public function scopeInServiceWithEmail($query)
    {
        return $query->where('out_of_service', null)->where('no_email', 0);
    }

    public function scopeNonAssociatedUser($query)
    {
        return $query->where('id', '<>', 1)->companyCheck()->where('is_special_user', 0)->where('out_of_service', null)->get();
    }
    
    public function scopeNonAssociatedSpecialUser($query)
    {
        return $query->where('id', '<>', 1)->companyCheck()->where('is_special_user', 1)->where('out_of_service', null)->get();
    }

    public function scopeActiveSpecialUsers($query)
    {
        return $query->companyCheck()->where('type', '<>', 1)->where('out_of_service', null)->where('is_top_person', 0)->where('is_special_user', 1);
    }
}
