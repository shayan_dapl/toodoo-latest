<?php 

return  [
    'dashed_date' => 'd-m-Y',
    'dashed_date_time' => 'd-m-Y H:i:s',
    'slashed_date' => 'd/m/Y',
    'db_date' => 'Y-m-d',
    'associate_user' => 'gert@fuller.be',
    'devsite_url' => 'https://devsite.too-doo.be',
    'interval' => '1 month',
    'payment_desc' => 'Monthly payment',
    'ip' => '127.0.0.1',
    'lang' => ['nl' => 'nl', 'en' => 'en', 'fr' => 'fr'],
    'img_extentions' => ["jpeg", "jpg", "png"],
    'trial_user_limit' => 10,
    'gravatar_url' => 'https://www.gravatar.com/avatar/',
    'raci_arr' => ['1' => 'R', '3' => 'C', '4' => 'I'],
    'extra_customer_admin' => 1,
    'risk_opp_level' => ['H' => [], 'M' => [], 'L' => []],
    'trial_storage_limit' => 10240,
    'policy_url' => [
        'privacy' => [
            'nl' => 'https://devsite.too-doo.be/privacy/',
            'en' => 'https://devsite.too-doo.be/privacy/',
            'fr' => 'https://devsite.too-doo.be/fr/intimite/'
        ],
        'cookie' => [
            'nl' => 'https://devsite.too-doo.be/cookieverklaring/',
            'en' => 'https://devsite.too-doo.be/cookieverklaring/',
            'fr' => 'https://devsite.too-doo.be/fr/declaration-de-cookie/'
        ],
        'verkoop' => [
            'nl' => 'https://devsite.too-doo.be/algemene-voorwaarden/',
            'en' => 'https://devsite.too-doo.be/algemene-voorwaarden/',
            'fr' => 'https://devsite.too-doo.be/fr/termes-et-conditions/'
        ]
    ],
    'intercom_app_id' => env('INTERCOM_APP_ID'),
    'intercom_secret' => env('INTERCOM_SECRET'),
    'audit_category' => ['internal' => 'internal audit', 'external' => 'external audit', 'supplier-rating' => 'internal supplier action', 'corrective-action' => 'supplier corrective action'],
    'whatfix_url' => env('WHATFIX_URL'),
    'membership_monthly_per' => 0
];
