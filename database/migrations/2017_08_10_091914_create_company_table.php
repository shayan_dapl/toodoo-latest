<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->text('address_line1')->size('long');
            $table->text('address_line2')->size('long')->nullable();
            $table->text('street')->size('long')->nullable();
            $table->text('nr')->size('long')->nullable();
            $table->text('bus')->size('long')->nullable();
            $table->string('zip', 20)->nullable();
            $table->string('city', 255)->nullable();
            $table->integer('country')->nullable();
            $table->string('tva_no', 20)->nullable();
            $table->string('photo', 255)->default('placeholder.png');
            $table->tinyInteger('status');
            $table->integer('is_trial');
            $table->integer('loaded_data');
            $table->date('trial_start')->nullable();
            $table->date('trial_end')->nullable();
            $table->integer('vat_applicable');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('company');
    }
}
