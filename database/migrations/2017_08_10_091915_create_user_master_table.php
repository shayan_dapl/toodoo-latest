<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('user_master', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onUpdate('no action')->onDelete('cascade');
            $table->string('name', 255);
            $table->string('email', 255)->nullable();
            $table->text('password')->size('long')->nullable();
            $table->string('remember_token', 100)->nullable();
            $table->tinyInteger('type')->comment = "1 -> 'SA', 2 -> 'CA', 3 -> 'U'";
            $table->integer('is_auditor')->default(0);
            $table->date('out_of_service')->nullable();
            $table->integer('can_process_owner')->default(0);
            $table->integer('is_top_person')->default(0)->comment = "person from whom org chart starts";
            $table->integer('is_special_user')->default(0)->comment =  "Labour peoples";
            $table->integer('no_email')->default(0)->comment = "People without login facility";
            $table->integer('not_yet_logged')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_master');
    }
}
