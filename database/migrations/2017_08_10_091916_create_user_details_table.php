<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('master_id')->unsigned();
            $table->foreign('master_id')->references('id')->on('user_master')->onUpdate('no action')->onDelete('cascade');
            $table->string('fname', 255);
            $table->string('lname', 255);
            $table->string('email', 255)->nullable();
            $table->string('photo', 255)->default('placeholder.png');
            $table->text('street')->size('long')->nullable();
            $table->string('nr', 255)->nullable();
            $table->string('zip', 20)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('country', 255)->nullable();
            $table->string('language', 20)->nullable();
            $table->tinyInteger('status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_details');
    }
}
