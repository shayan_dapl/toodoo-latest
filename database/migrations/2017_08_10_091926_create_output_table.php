<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutputTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('output', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('process_id')->unsigned();
            $table->foreign('process_id')->references('id')->on('process')->onUpdate('no action')->onDelete('cascade');
            $table->integer('supporting_process_id')->unsigned();
            $table->foreign('supporting_process_id')->references('id')->on('process')->onUpdate('no action')->onDelete('cascade');
            $table->text('comment')->size('long')->nullable();
            $table->text('output')->size('long')->nullable();
            $table->tinyInteger('status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('output');
    }
}
