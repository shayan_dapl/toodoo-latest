<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntryLogTable extends Migration
{
    public function up()
    {
        Schema::create('entry_log', function (Blueprint $table) {
            $table->increments('id', true);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('user_master')->onUpdate('no action')->onDelete('cascade');
            $table->string('ip_addr', 255)->nullable();
            $table->timestamp('in_time')->nullable();
            $table->timestamp('out_time')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('entry_log');
    }
}
