<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsoReferenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iso_reference', function (Blueprint $table) {
            $table->increments('id');
            $table->string('iso_no_du', 50);
            $table->string('iso_no_en', 50);
            $table->string('iso_no_fr', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('iso_reference');
    }
}
