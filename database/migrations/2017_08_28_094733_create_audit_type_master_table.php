<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTypeMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_type_master', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->integer('iso_ref_id')->unsigned();
            $table->foreign('iso_ref_id')->references('id')->on('iso_reference')->onDelete('cascade')->onUpdate('no action');
            $table->string('clause', 100)->nullable();
            $table->string('name_du', 255)->nullable();
            $table->text('description_du')->size('long')->nullable();
            $table->string('name_en', 255)->nullable();
            $table->text('description_en')->size('long')->nullable();
            $table->string('name_fr', 255)->nullable();
            $table->text('description_fr')->size('long')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit_type_master');
    }
}
