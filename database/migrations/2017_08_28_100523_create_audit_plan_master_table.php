<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditPlanMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_plan_master', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onUpdate('no action')->onDelete('cascade');
            $table->text('audit_name')->size('long')->nullable();
            $table->integer('plan_month')->default('0');
            $table->integer('plan_year')->default('0');
            $table->string('plan_date', 50)->nullable();
            $table->string('plan_time', 50)->nullable();
            $table->integer('original_month');
            $table->integer('original_year');

            $table->integer('process_to_audit')->unsigned();
            $table->foreign('process_to_audit')->references('id')->on('process')->onUpdate('no action')->onDelete('cascade');

            $table->integer('choose_lead_auditor')->unsigned();
            $table->foreign('choose_lead_auditor')->references('id')->on('user_master')->onUpdte('no action')->onDelete('cascade');

            $table->integer('type_of_audit')->unsigned();
            $table->foreign('type_of_audit')->references('id')->on('iso_reference')->onUpdte('no action')->onDelete('cascade');

            $table->text('objective')->size('long')->nullable();
            $table->string('visited_location', 255)->nullable();
            $table->text('conclusion')->size('long')->nullable();
            $table->tinyInteger('status')->default(0)->comment = "0=>pending,1=>processing,2=>completed";
            $table->string('release_date', 50)->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit_plan_master');
    }
}
