<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTeamMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_team_member', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('audit_id')->unsigned();
            $table->foreign('audit_id')->references('id')->on('audit_plan_master')->onUpdate('no action')->onDelete('cascade');
            $table->integer('auditor_id')->unsigned();
            $table->foreign('auditor_id')->references('id')->on('user_master')->onUpdate('no action')->onDelete('cascade');
            $table->tinyInteger('auditor_type')->default('0'); //If auditor is lead one then only value will be 1
            $table->tinyInteger('status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit_team_member');
    }
}
