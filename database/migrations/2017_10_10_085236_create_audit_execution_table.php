<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditExecutionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_execution', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('audit_id')->unsigned();
            $table->foreign('audit_id')->references('id')->on('audit_plan_master')->onUpdate('no action')->onDelete('cascade');
            $table->string('observation', 255)->nullable();
            $table->integer('iso_no')->unsigned();
            $table->foreign('iso_no')->references('id')->on('iso_reference')->onUpdate('no action')->onDelete('cascade');
            $table->integer('finding_type_id')->unsigned();
            $table->foreign('finding_type_id')->references('id')->on('finding_type')->onUpdate('no action')->onDelete('cascade');
            $table->integer('reference_id')->unsigned();
            $table->foreign('reference_id')->references('id')->on('audit_type_master')->onUpdate('no action')->onDelete('cascade');
            $table->string('evidence', 255)->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit_execution');
    }
}
