<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditExecutionDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_execution_docs', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('execution_id')->unsigned();
            $table->foreign('execution_id')->references('id')->on('audit_execution')->onUpdate('no action')->onDelete('cascade');
            
            $table->string('doc_name', 255);
            $table->tinyInteger('status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit_execution_docs');
    }
}
