<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportedIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reported_issues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('issue_code', 50);
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onUpdate('no action')->onDelete('cascade');
            $table->integer('source_type')->unsigned();
            $table->foreign('source_type')->references('id')->on('source_type')->onUpdate('no action')->onDelete('cascade');
            $table->string('reference', 255)->nullable();
            $table->tinyInteger('status')->comment = "0=>pending,1=>on process,2=>completed";
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reported_issues');
    }
}
