<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportedIssuesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reported_issues_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reported_issue_id')->unsigned();
            $table->foreign('reported_issue_id')->references('id')->on('reported_issues')->onDelete('cascade')->onUpdate('no action');
            $table->string('issue', 255)->nullable();
            $table->string('action', 255)->nullable();
            $table->integer('who')->unsigned();
            $table->foreign('who')->references('id')->on('user_master')->onDelete('cascade')->onUpdate('no action');
            $table->date('deadline');
            $table->integer('related_process')->unsigned();
            $table->foreign('related_process')->references('id')->on('process')->onDelete('cascade')->onUpdate('no action');
            $table->integer('created_by');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reported_issues_details');
    }
}
