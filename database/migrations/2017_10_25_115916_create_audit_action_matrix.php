<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditActionMatrix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_action_matrix', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('finding_type')->unsigned();
            $table->foreign('finding_type')->references('id')->on('finding_type')->onUpdate('no action')->onDelete('cascade');
            $table->integer('immidiate_actions')->default(0);
            $table->integer('root_cause_analysis')->default(0);
            $table->integer('corrective_measure')->default(0);
            $table->integer('effectiveness')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit_action_matrix');
    }
}
