<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportIssueActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_issue_action', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onUpdate('no action')->onDelete('cascade');
            $table->integer('ref_id')->unsigned();
            $table->foreign('ref_id')->references('id')->on('audit_execution')->onUpdate('no action')->onDelete('cascade');
            $table->string('action_code', 100);
            $table->text('info')->size('long')->nullable();
            $table->tinyInteger('status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_issue_action');
    }
}
