<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionDelegateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_delegate_members', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('action_id')->unsigned();
            $table->foreign('action_id')->references('id')->on('report_issue_action')->onUpdate('no action')->onDelete('cascade');
            
            $table->integer('member')->unsigned();
            $table->foreign('member')->references('id')->on('user_master')->onUpdate('no action')->onDelete('cascade');

            $table->string('issue_type', 50);
            $table->text('requested_action')->size('long')->nullable();
            $table->date('deadline');
            $table->text('action_taken')->size('long')->nullable();
            $table->date('complete_date');
            $table->tinyInteger('status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('action_delegate_members');
    }
}
