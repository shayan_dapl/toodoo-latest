<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContextAnalysisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('context_analysis', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onUpdate('no action')->onDelete('cascade');

            $table->integer('process_id')->unsigned();
            $table->foreign('process_id')->references('id')->on('process')->onUpdate('no action')->onDelete('cascade');

            $table->integer('owner')->unsigned();
            $table->foreign('owner')->references('id')->on('user_master')->onUpdate('no action')->onDelete('cascade');

            $table->string('issue', 255);
            $table->string('issue_type', 50);
            $table->integer('subject');
            $table->text('monitoring_info')->size('long');
            $table->text('possible_influence')->size('long');
            $table->string('impact', 50);
            $table->string('risk_or_opportunity', 50);
            $table->text('description')->size('long');
            $table->tinyInteger('status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('context_analysis');
    }
}
