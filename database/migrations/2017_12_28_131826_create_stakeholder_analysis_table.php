<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStakeholderAnalysisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stakeholder_analysis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onDelete('cascade')->onUpdate('no action');
            $table->string('type', 100)->nullable();
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('stakeholder_category')->onUpdate('no action')->onDelete('cascade');
            $table->string('stakeholder_name', 255)->nullable();
            $table->string('needs', 255)->nullable();
            $table->text('detail')->size('long')->nullable();
            $table->integer('impact');
            $table->integer('interest');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stakeholder_analysis');
    }
}
