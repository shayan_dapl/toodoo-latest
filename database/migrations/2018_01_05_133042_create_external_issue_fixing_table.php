<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExternalIssueFixingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_issue_fixing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('issue_detail_id')->unsigned();
            $table->foreign('issue_detail_id')->references('id')->on('reported_issues_details')->onUpdate('no action')->onDelete('cascade');
            $table->text('action_description')->size('long')->nullable();
            $table->string('supporting_doc', 255)->nullable();
            $table->date('fixed_on');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('external_issue_fixing');
    }
}
