<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskOpportunityActionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_opportunity_action_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onUpdate('no action')->onDelete('cascade');
            $table->integer('process_id')->unsigned();
            $table->foreign('process_id')->references('id')->on('process')->onUpdate('no action')->onDelete('cascade');
            $table->string('type', 20)->nullable();
            $table->integer('risk_opp_id')->default(0);
            $table->string('action', 255)->nullable();
            $table->string('action_status', 20)->nullable();
            $table->date('deadline');
            $table->tinyInteger('closed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('risk_opportunity_action_log');
    }
}
