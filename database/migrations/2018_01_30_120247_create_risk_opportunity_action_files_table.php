<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskOpportunityActionFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_opportunity_action_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comment_id')->unsigned();
            $table->foreign('comment_id')->references('id')->on('risk_opportunity_action_comments')->onUpdate('no action')->onDelete('cascade');
            $table->string('document_name', 255)->nullable();
            $table->integer('submitted_by')->unsigned();
            $table->foreign('submitted_by')->references('id')->on('user_master')->onUpdate('no action')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('risk_opportunity_action_files');
    }
}
