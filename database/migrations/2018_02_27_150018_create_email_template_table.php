<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_template', function (Blueprint $table) {
            $table->increments('id');
            $table->string('segment', 255)->nullable();
            $table->text('subject_Dutch')->size('long')->nullable();
            $table->text('subject_English')->size('long')->nullable();
            $table->text('subject_French')->size('long')->nullable();
            $table->text('template_Dutch')->size('long')->nullable();
            $table->text('template_English')->size('long')->nullable();
            $table->text('template_French')->size('long')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email_template');
    }
}
