<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_master', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iso_id')->unsigned();
            $table->foreign('iso_id')->references('id')->on('iso_reference')->onUpdate('no action')->onDelete('cascade');
            $table->text('description_en')->size('long');
            $table->text('description_fr')->size('long');
            $table->text('description_du')->size('long');
            $table->integer('is_base')->unsigned();
            $table->decimal('price', 10, 2)->unsigned();
            $table->integer('status')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('package_master');
    }
}
