<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSupportingProcessIdInOutputTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('output', function (Blueprint $table) {
            $table->dropForeign(['supporting_process_id']);
        });

        Schema::table('output', function (Blueprint $table) {
            $table->integer('supporting_process_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('output', function (Blueprint $table) {
        });
    }
}
