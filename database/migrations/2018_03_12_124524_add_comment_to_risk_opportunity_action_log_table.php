<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentToRiskOpportunityActionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('risk_opportunity_action_log', function (Blueprint $table) {
            $table->text('comment')->nullable()->after('closed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('risk_opportunity_action_log', function (Blueprint $table) {
            $table->dropColumn('comment');
        });
    }
}
