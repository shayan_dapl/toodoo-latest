<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContextStakeholderActionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('context_stakeholder_action_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onUpdate('no action')->onDelete('cascade');
            $table->string('type', 20)->nullable();
            $table->integer('contxt_stakehold_id')->default(0);
            $table->string('action', 255)->nullable();
            $table->integer('deligate_to')->unsigned();
            $table->foreign('deligate_to')->references('id')->on('user_master')->onUpdate('no action')->onDelete('cascade');
            $table->date('deadline')->nullable();
            $table->tinyInteger('closed')->default(0);
            $table->date('closed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('context_stakeholder_action_log');
    }
}
