<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyPackagePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_package_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('used_package_id')->unsigned();
            $table->foreign('used_package_id')->references('id')->on('company_subscribed_package')->onUpdate('no action')->onDelete('cascade');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onUpdate('no action')->onDelete('cascade');
            $table->string('transaction_id', 255)->nullable();
            $table->string('profile_id', 255)->nullable();
            $table->date('payment_date')->nullable();
            $table->decimal('payment_amount', 10, 2)->default(0.00);
            $table->date('payment_added')->nullable();
            $table->string('status', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_package_payment');
    }
}
