<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetAddressDetailsNullOnCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company', function (Blueprint $table) {
            $table->text('address_line1')->size('long')->nullable()->change();
            $table->string('zip', 20)->nullable()->change();
            $table->string('city', 255)->nullable()->change();
            $table->integer('country')->nullable()->change();
            $table->string('tva_no', 20)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
