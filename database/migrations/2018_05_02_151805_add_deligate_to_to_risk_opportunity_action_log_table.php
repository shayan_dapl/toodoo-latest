<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeligateToToRiskOpportunityActionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('risk_opportunity_action_log', function (Blueprint $table) {
            $table->integer('delegate_to')->default(0)->after('action_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('risk_opportunity_action_log', function (Blueprint $table) {
            $table->dropColumn('delegate_to');
        });
    }
}
