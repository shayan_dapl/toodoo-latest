<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVatAmountToCompanyPackagePayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_package_payment', function (Blueprint $table) {
            $table->decimal('vat_amount', 10, 2)->default('0.00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_package_payment', function (Blueprint $table) {
            $table->dropColumn('vat_amount');
        });
    }
}
