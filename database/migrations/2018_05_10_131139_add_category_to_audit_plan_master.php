<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryToAuditPlanMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audit_plan_master', function (Blueprint $table) {
            $table->string('category', 255)->nullable()->after('audit_name');
            $table->string('audit_company', 255)->nullable()->after('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audit_plan_master', function (Blueprint $table) {
            $table->dropColumn('category');
            $table->dropColumn('audit_company');
        });
    }
}
