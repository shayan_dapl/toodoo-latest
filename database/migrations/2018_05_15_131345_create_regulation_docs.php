<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegulationDocs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regulation_docs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('regulation_id')->unsigned();
            $table->foreign('regulation_id')->references('id')->on('procedures')->onUpdate('no action')->onDelete('cascade');
            $table->integer('source_id');
            $table->string('doc_name', 255);
            $table->text('location_doc')->nullable();
            $table->tinyInteger('status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('regulation_docs');
    }
}
