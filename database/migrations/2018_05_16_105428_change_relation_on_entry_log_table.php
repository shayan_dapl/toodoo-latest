<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRelationOnEntryLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('entry_log', function (Blueprint $table) {
            $table->dropForeign('entry_log_user_id_foreign');
            $table->dropColumn('user_id');
            $table->integer('company_id')->unsigned()->after('id');
            $table->foreign('company_id')->references('id')->on('company')->onDelete('cascade')->onUpdate('no action');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entry_log', function (Blueprint $table) {
            $table->dropForeign('entry_log_company_id_foreign');
            $table->dropColumn('company_id');
        });
    }
}
