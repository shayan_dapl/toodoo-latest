<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMultilingualCountryNameInCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->renameColumn('name', 'name_en');
            $table->string('name_du', 255)->nullable()->after('id');
            $table->string('name_fr', 255)->nullable()->after('name_du');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->renameColumn('name_en', 'name');
            $table->dropColumn('name_du');
            $table->dropColumn('name_fr');
        });
    }
}
