<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReleasedataToAuditPlanMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audit_plan_master', function (Blueprint $table) {
            $table->integer('release_month')->default(0)->after('status');
            $table->integer('release_year')->default(0)->after('release_month');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audit_plan_master', function (Blueprint $table) {
            $table->dropColumn('release_month');
            $table->dropColumn('release_year');
        });
    }
}
