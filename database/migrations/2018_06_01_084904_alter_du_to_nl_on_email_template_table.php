<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDuToNlOnEmailTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_template', function (Blueprint $table) {
            $table->renameColumn('subject_Dutch', 'subject_nl');
            $table->renameColumn('template_Dutch', 'template_nl');
            $table->renameColumn('subject_English', 'subject_en');
            $table->renameColumn('template_English', 'template_en');
            $table->renameColumn('subject_French', 'subject_fr');
            $table->renameColumn('template_French', 'template_fr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
