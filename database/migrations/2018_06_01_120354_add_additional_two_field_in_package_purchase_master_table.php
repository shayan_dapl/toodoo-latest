<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalTwoFieldInPackagePurchaseMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_purchase_master', function (Blueprint $table) {
            $table->integer('day_count')->nullable()->after('type');
            $table->decimal('amount', 10, 2)->nullable()->after('day_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_purchase_master', function (Blueprint $table) {
            $table->dropColumn('day_count');
            $table->dropColumn('amount');
        });
    }
}
