<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropCompanyPackagePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_package_payment', function (Blueprint $table) {
            $table->dropForeign('company_package_payment_company_id_foreign');
            $table->dropForeign('company_package_payment_used_package_id_foreign');
        });
        Schema::drop('company_package_payment');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
