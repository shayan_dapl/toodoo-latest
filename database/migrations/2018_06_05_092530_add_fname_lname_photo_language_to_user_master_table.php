<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFnameLnamePhotoLanguageToUserMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_master', function (Blueprint $table) {
            $table->string('fname', 255)->nullable()->after('name');
            $table->string('lname', 255)->nullable()->after('fname');
            $table->string('photo', 255)->default('placeholder.png')->after('lname');
            $table->string('language', 20)->nullable()->after('photo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
