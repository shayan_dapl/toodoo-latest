<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnNamesOnLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('languages', function (Blueprint $table) {
            $table->renameColumn('Nederlands', 'nl');
            $table->renameColumn('English', 'en');
            $table->renameColumn('Francais', 'fr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('languages', function (Blueprint $table) {
            $table->renameColumn('nl', 'Nederlands');
            $table->renameColumn('en', 'English');
            $table->renameColumn('fr', 'Francais');
        });
    }
}
