<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_master', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en', 50);
            $table->string('title_nl', 50);
            $table->string('title_fr', 50);
            $table->text('description_en')->size('long');
            $table->text('description_nl')->size('long');
            $table->text('description_fr')->size('long');
            $table->string('cover_img', 255);
            $table->string('video_link', 255);
            $table->integer('status')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('video_master');
    }
}
