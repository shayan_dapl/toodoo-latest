<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetentRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('competent_ratings', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onDelete('cascade')->onUpdate('no action');
            $table->integer('process_id')->unsigned();
            $table->foreign('process_id')->references('id')->on('process')->onDelete('cascade')->onUpdate('no action');
            $table->integer('step_id')->unsigned();
            $table->foreign('step_id')->references('id')->on('process_steps')->onDelete('cascade')->onUpdate('no action');
            $table->integer('org_id')->unsigned();
            $table->foreign('org_id')->references('id')->on('organization_structure')->onDelete('cascade')->onUpdate('no action');
            $table->integer('rating')->unsigned();
            $table->foreign('rating')->references('id')->on('ratings')->onDelete('cascade')->onUpdate('no action');
            $table->text('comment')->size('long')->nullable();
            $table->tinyInteger('is_action')->default(0);
            $table->tinyInteger('is_latest')->default(0);
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competent_ratings');
    }
}
