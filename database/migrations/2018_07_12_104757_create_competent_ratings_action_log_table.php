<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetentRatingsActionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competent_ratings_action_log', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onDelete('cascade')->onUpdate('no action');
            $table->integer('rating_id')->unsigned();
            $table->foreign('rating_id')->references('id')->on('competent_ratings')->onDelete('cascade')->onUpdate('no action');
            $table->string('action', 255)->nullable();
            $table->integer('delegate_to')->unsigned();
            $table->foreign('delegate_to')->references('id')->on('user_master')->onDelete('cascade')->onUpdate('no action');
            $table->date('deadline')->nullable();
            $table->tinyInteger('closed')->default(0);
            $table->date('closed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competent_ratings_action_log');
    }
}
