<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFieldsToKpiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('kpi',function(Blueprint $table){
       		$table->string('input', 255)->nullable()->after('name');
       		$table->integer('master_kpi')->unsigned()->after('input');
            $table->foreign('master_kpi')->references('id')->on('master_kpi')->onUpdate('no action')->onDelete('cascade');
            $table->integer('kpi_unit_id')->unsigned()->after('master_kpi');
            $table->foreign('kpi_unit_id')->references('id')->on('kpi_unit')->onUpdate('no action')->onDelete('cascade');
            $table->string('frequency',255)->nullable()->after('kpi_unit_id');
            $table->date('valid_from')->nullable()->after('frequency');
            $table->date('valid_to')->nullable()->after('valid_from');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
