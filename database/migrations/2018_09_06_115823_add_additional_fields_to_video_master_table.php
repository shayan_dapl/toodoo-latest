<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFieldsToVideoMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_master',function(Blueprint $table){
            $table->dropColumn('video_link');
            $table->string('media_type', 255)->nullable()->after('description_fr');
            $table->string('path_nl', 255)->nullable()->after('media_type');
            $table->string('path_en', 255)->nullable()->after('path_nl');
            $table->string('path_fr', 255)->nullable()->after('path_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
