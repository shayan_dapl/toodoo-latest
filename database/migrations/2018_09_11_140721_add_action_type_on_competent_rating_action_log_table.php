<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActionTypeOnCompetentRatingActionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('competent_ratings_action_log', function (Blueprint $table) {
            $table->integer('action_type')->unsigned()->nullable()->after('action');
            $table->foreign('action_type')->references('id')->on('competent_rating_action_type')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
