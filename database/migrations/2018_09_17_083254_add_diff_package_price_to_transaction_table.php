<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiffPackagePriceToTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_details',function(Blueprint $table){
            $table->decimal('package_price', 8, 2)->default('0.00')->after('profile_id');
            $table->decimal('user_package_price', 8, 2)->default('0.00')->after('package_price');
            $table->decimal('storage_package_price', 8, 2)->default('0.00')->after('user_package_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
