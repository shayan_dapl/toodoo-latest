<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToPriceIncreaseRequestLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('price_increase_request_log',function(Blueprint $table){
            $table->decimal('current_user_price', 8, 2)->default('0.00')->after('current_package_price');
            $table->decimal('current_storage_price', 8, 2)->default('0.00')->after('current_user_price');
            $table->integer('package_price_inc_req')->default(0)->after('current_storage_price');
            $table->integer('user_price_inc_req')->default(0)->after('package_price_inc_req');
            $table->integer('storage_price_inc_req')->default(0)->after('user_price_inc_req');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
