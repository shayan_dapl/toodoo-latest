<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeriodOnProcessKpiDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('process_kpi_data', function(Blueprint $table) {
            $table->string('period', 255)->nullable()->after('kpi_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('process_kpi_data', function(Blueprint $table) {
            $table->dropColumn('period');
        });
    }
}
