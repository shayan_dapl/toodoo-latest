<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onDelete('cascade')->onUpdate('no action');
            $table->integer('transaction_id')->unsigned();
            $table->foreign('transaction_id')->references('id')->on('transaction_details')->onDelete('cascade')->onUpdate('no action');
            $table->string('username', 255)->nullable();
            $table->string('company_logo', 255)->nullable();
            $table->string('company_name', 255)->nullable();
            $table->text('street')->size('long')->nullable();
            $table->text('nr')->size('long')->nullable();
            $table->text('bus')->size('long')->nullable();
            $table->string('zip', 20)->nullable();
            $table->string('city', 255)->nullable();
            $table->integer('country')->nullable();
            $table->string('tva_no', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_data');
    }
}
