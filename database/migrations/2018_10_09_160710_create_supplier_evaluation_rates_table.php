<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierEvaluationRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_evaluation_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onDelete('cascade')->onUpdate('no action');
            $table->integer('log_id')->unsigned();
            $table->foreign('log_id')->references('id')->on('supplier_evaluation_action_log')->onDelete('cascade')->onUpdate('no action');
            $table->integer('parameter_id')->unsigned();
            $table->foreign('parameter_id')->references('id')->on('supplier_parameter')->onDelete('cascade')->onUpdate('no action');
            $table->integer('rating_id')->unsigned();
            $table->foreign('rating_id')->references('id')->on('supplier_rating')->onDelete('cascade')->onUpdate('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_evaluation_rates');
    }
}
