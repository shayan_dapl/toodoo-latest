<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFieldsToSourceTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('source_type',function(Blueprint $table){
            $table->integer('deletable')->default(1)->after('type');
            $table->integer('source_status')->default(0)->after('deletable')->comment = " 1 => 'Customer complaint', 2 => 'Supplier complaint', 3 => 'Meeting' ";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
