<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupplierOnReportedIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reported_issues', function (Blueprint $table) {
            $table->integer('supplier')->unsigned()->nullable()->after('source_type');
            $table->foreign('supplier')->references('id')->on('supplier')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reported_issues', function (Blueprint $table) {
            $table->dropForeign('reported_issues_supplier_foreign');
            $table->dropColumn('supplier');
        });
    }
}
