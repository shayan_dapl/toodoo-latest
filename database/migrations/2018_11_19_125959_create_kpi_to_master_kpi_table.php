<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpiToMasterKpiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpi_to_master_kpi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('master_kpi_id')->unsigned();
            $table->foreign('master_kpi_id')->references('id')->on('master_kpi')->onDelete('cascade')->onUpdate('no action');
            $table->integer('kpi_id')->unsigned();
            $table->foreign('kpi_id')->references('id')->on('kpi')->onDelete('cascade')->onUpdate('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpi_to_master_kpi');
    }
}
