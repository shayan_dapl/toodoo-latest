<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierCorrectiveActionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_corrective_action_log', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company')->onDelete('cascade')->onUpdate('no action');
            $table->integer('supplier_id')->unsigned();
            $table->foreign('supplier_id')->references('id')->on('supplier')->onDelete('cascade')->onUpdate('no action');
            $table->text('parameters')->nullable();
            $table->text('message')->size('long')->nullable();
            $table->date('deadline')->nullable();
            $table->integer('delegate_to')->unsigned()->nullable();
            $table->foreign('delegate_to')->references('id')->on('user_master')->onDelete('cascade')->onUpdate('no action');
            $table->tinyInteger('closed')->default(0);
            $table->date('closed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_corrective_action_log');
    }
}
