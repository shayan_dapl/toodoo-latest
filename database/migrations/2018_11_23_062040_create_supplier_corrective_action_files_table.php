<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierCorrectiveActionFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_corrective_action_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comment_id')->unsigned();
            $table->foreign('comment_id')->references('id')->on('supplier_corrective_action_comments')->onDelete('cascade')->onUpdate('no action');
            $table->string('doc_name', 255)->nullable();
            $table->integer('submitted_by')->unsigned();
            $table->foreign('submitted_by')->references('id')->on('user_master')->onDelete('cascade')->onUpdate('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_corrective_action_files');
    }
}
