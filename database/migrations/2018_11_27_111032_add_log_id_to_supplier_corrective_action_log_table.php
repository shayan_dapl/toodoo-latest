<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogIdToSupplierCorrectiveActionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supplier_corrective_action_log', function (Blueprint $table) {
            $table->integer('log_id')->unsigned()->nullable()->after('company_id');
            $table->foreign('log_id')->references('id')->on('supplier_evaluation_action_log')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
