<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company')->insert([
            'name' => 'too-doo',
            'address_line1' => 'Berringstraat 22',
            'address_line2' => '9270 Laarne',
            'street' => 'Mahler Building Gustav Mahlerlaan',
            'nr' => '1000',
            'bus' => '9270 Laarne',
            'zip' => '1025 1082',
            'city' => 'MK Amsterdam',
            'country' => 2,
            'is_vat' => 0,
            'tva_no' => 'toodoo123',
            'photo' => 'placeholder.png',
            'status' => 1,
            'is_trial' => 1,
            'loaded_data' => 1,
            'trial_start' => date('Y-m-d'),
            'trial_end' => date('Y-m-d'),
            'subscription_id' => 'xxxxx',
            'next_renewal_date' => date('Y-m-d'),
            'subscription_amount' => 0.00,
            'next_subscription_date' => date('Y-m-d'),
            'vat_applicable' => 0,
            'getstarted' => 1,
            'created_by' => 1,
            'updated_by' => 1
        ]);
    }
}
