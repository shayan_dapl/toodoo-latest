<?php

use Illuminate\Database\Seeder;

class ContextSubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('context_subject')->insert([
            [
                'company_id' => 1,
                'subject' => 'Political',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'company_id' => 1,
                'subject' => 'Economic',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'company_id' => 1,
                'subject' => 'Social',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'company_id' => 1,
                'subject' => 'Technological',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'company_id' => 1,
                'subject' => 'Ecological',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'company_id' => 1,
                'subject' => 'Legal',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ]
        ]);
    }
}
