<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            [
                'name_nl' => 'België',
                'name_en' => 'Belgium',
                'name_fr' => 'Belgique',
            ],
            [
                'name_nl' => 'Holland',
                'name_en' => 'Holland',
                'name_fr' => 'Hollande',
            ],
            [
                'name_nl' => 'Frankrijk',
                'name_en' => 'France',
                'name_fr' => 'France',
            ],
            [
                'name_nl' => 'Zwitserland',
                'name_en' => 'Swiss',
                'name_fr' => 'Suisse',
            ],
            [
                'name_nl' => 'Duitsland',
                'name_en' => 'Germany',
                'name_fr' => 'Allemagne',
            ],
            [
                'name_nl' => 'UK',
                'name_en' => 'UK',
                'name_fr' => 'Royaume-Uni',
            ],
            [
                'name_nl' => 'Turkije',
                'name_en' => 'Turkey',
                'name_fr' => 'Turquie',
            ]
        ]);
    }
}
