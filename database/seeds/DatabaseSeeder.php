<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CompanyTableSeeder::class);
        $this->call(UserMasterTableSeeder::class);
        $this->call(ProcessCategoryTableSeeder::class);
        $this->call(ProcessDocumentTypesTableSeeder::class);
        $this->call(FindingTypeTableSeeder::class);
        $this->call(SourceTypeTableSeeder::class);
        $this->call(ContextSubjectTableSeeder::class);
        $this->call(LanguageTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StakeholderCategoryTableSeeder::class);
        $this->call(PackageUserPlanTableSeeder::class);
        $this->call(PackageStoragePlanTableSeeder::class);
        $this->call(GeneralSettingsTableSeeder::class);
    }
}
