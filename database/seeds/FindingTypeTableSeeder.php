<?php

use Illuminate\Database\Seeder;

class FindingTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('finding_type')->insert([
            [
                'company_id' => 1,
                'finding_type' => 'Positive finding',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'company_id' => 1,
                'finding_type' => 'Opportunity for improvement (OFI)',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'company_id' => 1,
                'finding_type' => 'Minor Non Conformity (Minor)',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'company_id' => 1,
                'finding_type' => 'Major Non Conformity (Major)',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ]
        ]);
    }
}
