<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('language')->insert([
            [
                'name' => 'nl',
                'display_text' => 'Nederlands',
            ],
            [
                'name' => 'en',
                'display_text' => 'English',
            ],
            [
                'name' => 'fr',
                'display_text' => 'Francais',
            ]
        ]);
    }
}
