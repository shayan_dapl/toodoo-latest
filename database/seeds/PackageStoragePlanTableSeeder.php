<?php

use Illuminate\Database\Seeder;

class PackageStoragePlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('package_storage_plan')->insert([
            [
                'space' => 10240,
                'price_per_month' => 0.00,
                'status' => 1,
                'is_base' => 1,
            ],
            [
                'space' => 10240,
                'price_per_month' => 10.00,
                'status' => 1,
                'is_base' => 0,
            ]
        ]);
    }
}
