<?php

use Illuminate\Database\Seeder;

class PackageUserPlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('package_user_plan')->insert([
            [
                'user_count' => 10,
                'price_per_month' => 150.00,
                'is_max' => 0,
                'status' => 1,
            ],
            [
                'user_count' => 15,
                'price_per_month' => 175.00,
                'is_max' => 0,
                'status' => 1,
            ],
            [
                'user_count' => 20,
                'price_per_month' => 200.00,
                'is_max' => 0,
                'status' => 1,
            ],
            [
                'user_count' => 25,
                'price_per_month' => 225.00,
                'is_max' => 0,
                'status' => 1,
            ],
            [
                'user_count' => 30,
                'price_per_month' => 250.00,
                'is_max' => 0,
                'status' => 1,
            ],
            [
                'user_count' => 30,
                'price_per_month' => 0.00,
                'is_max' => 1,
                'status' => 1,
            ]
        ]);
    }
}
