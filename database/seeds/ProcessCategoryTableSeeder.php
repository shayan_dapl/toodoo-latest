<?php

use Illuminate\Database\Seeder;

class ProcessCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('process_category')->insert([
            [
                'name_nl' => 'Management proces',
                'name_en' => 'Management Process',
                'name_fr' => 'Processus de gestion',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'name_nl' => 'Kernproces',
                'name_en' => 'Core Process',
                'name_fr' => 'Processus de base',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'name_nl' => 'Ondersteunend proces',
                'name_en' => 'Supporting Process',
                'name_fr' => 'Processus de soutien',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ]
        ]);
    }
}
