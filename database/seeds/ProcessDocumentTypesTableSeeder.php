<?php

use Illuminate\Database\Seeder;

class ProcessDocumentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('source_type')->insert([
            [
                'company_id' => 1,
                'source_type' => 'procedure',
                'status' => 1
            ],
        ]);
    }
}
