<?php

use Illuminate\Database\Seeder;

class SourceTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('source_type')->insert([
            [
                'company_id' => 1,
                'source_type' => 'Supplier complaint',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'company_id' => 1,
                'source_type' => 'Customer complaint',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'company_id' => 1,
                'source_type' => 'Meeting',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ]
        ]);
    }
}
