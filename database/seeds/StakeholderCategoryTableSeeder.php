<?php

use Illuminate\Database\Seeder;

class StakeholderCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stakeholder_category')->insert([
            [
                'company_id' => 1,
                'name' => 'Shareholder',
            ],
            [
                'company_id' => 1,
                'name' => 'Employees',
            ],
            [
                'company_id' => 1,
                'name' => 'Suppliers',
            ],
            [
                'company_id' => 1,
                'name' => 'Customer',
            ]
        ]);
    }
}
