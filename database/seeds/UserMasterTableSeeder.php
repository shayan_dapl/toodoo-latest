<?php

use Illuminate\Database\Seeder;

class UserMasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_master')->insert([
            'company_id' => 1,
            'name' => 'Gert Van De Vijver',
            'fname' => 'Gert Van',
            'lname' => 'De Vijver',
            'language' => 'nl',
            'email' => 'support@too-doo.be',
            'password' => bcrypt('Gert@td2018!'),
            'remember_token' => str_random(10),
            'type' => 1,
            'can_process_owner' => 0,
            'is_top_person' => 0,
            'created_by' => 1,
            'updated_by' => 1
        ]);
    }
}
