$(document).ready(function() {
    var newtable = $('#datatable-result').DataTable(dtLangs);
    // Event listener to the two range filtering inputs to redraw on input
    $('#minDate, #maxDate, #actions, #processOwner').change( function() {
        setTimeout(() => {
            newtable.draw();
        }, 500);
    });

    if ($.fn.dataTable.isDataTable('#datatable-result')) {
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                if ($('#minDate').val() != "") {
                    var startDate = $('#minDate').val().split("-").reverse().join("/");
                    var min = new Date(startDate).getTime();
                    if ($('#maxDate').val() == "") {
                        var tomorrow = new Date(startDate);
                        tomorrow.setDate(tomorrow.getDate() + 1);
                        var dateNo = tomorrow.getDate();
                        var day = (dateNo < 10) ? '0' + dateNo : dateNo;
                        var month = parseInt(tomorrow.getMonth() + 1);
                        var day = (month < 10) ? day + '-' + '0' + month : day + '-' + month;
                        var day = day + '-' + tomorrow.getFullYear();
                        $('#maxDate').val(day);
                    }
                    var endDate = $('#maxDate').val().split("-").reverse().join("/");
                    var max = new Date(endDate).getTime();
                }
                var checkDate = data[0].split("-").reverse().join("/");
                var checkDateTimeStamp = new Date(checkDate).getTime();
                var actionStatus = $('#actions').val();
                var processOwner = $('#processOwner').val();
                if ( 
                    ( isNaN( min ) && isNaN( max ) ) || 
                    ( isNaN( min ) && checkDateTimeStamp <= max ) || 
                    ( min <= checkDateTimeStamp   && isNaN( max ) ) || 
                    ( min <= checkDateTimeStamp   && checkDateTimeStamp <= max )
                ) {
                    if ( ((actionStatus == '') || (actionStatus != '' && data[6] == actionStatus)) &&
                          ((processOwner == '') || (processOwner != '' && data[5] == processOwner)) ) {
                        return true;
                    }
                } else {
                    if(actionStatus != '' && data[6] == actionStatus && processOwner == '') {
                        return true;
                    } else if(processOwner != '' && data[5] == processOwner && actionStatus == '') {
                        return true;
                    } else if (actionStatus != '' && data[6] == actionStatus && processOwner != '' && data[5] == processOwner) {
                        return true;
                    }
                }
                return false;
            }
        );
    }
});