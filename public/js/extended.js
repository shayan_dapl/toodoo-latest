jQuery(document).ready(function() {
	"use strict";
	Demo.init(); 
	Core.init();

	$('#multiselect2').multiselect({
		includeSelectAllOption: true
	});
	$('.admin-panels').adminpanel({
		grid: '.admin-grid',
		draggable: true,
		preserveGrid: true,
		onStart: function() {

		},
		onFinish: function() {
			$('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

			demoHighCharts.init();
			runVectorMaps();
		},
		onSave: function() {
			$(window).trigger('resize');
		}
	});

	$('#datatable2').dataTable({
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': [-1]
		}],
		"oLanguage": {
			"oPaginate": {
				"sPrevious": "",
				"sNext": ""
			}
		},
		"iDisplayLength": 10,
		"aLengthMenu": [
		[5, 10, 25, 50, -1],
		[5, 10, 25, 50, "All"]
		],
		"sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
		"oTableTools": {
			"sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
		}
	});
});