// call the method below
showScatterPlot(items);

function showScatterPlot(data) {
    // just to have some space around items. 
    var margins = {
        "left": 40,
        "right": 30,
        "top": 30,
        "bottom": 30
    };
    
    var width = $("#scatter-load").parent().width();
    var height = 700;
    
    // this will be our colour scale. An Ordinal scale.
    var colors = d3.scale.category10();

    // we add the SVG component to the scatter-load div
    var svg = d3.select("#scatter-load").append("svg").attr("width", width).attr("height", height).append("g")
        .attr("transform", "translate(" + margins.left + "," + margins.top + ")");

    // this sets the scale that we're using for the X axis. 
    // the domain define the min and max variables to show. In this case, it's the min and max prices of items.
    // this is made a compact piece of code due to d3.extent which gives back the max and min of the price variable within the dataset
    var x = d3.scale.linear()
        .domain(d3.extent(data, function (d) {
        return d.price;
    }))
    // the range maps the domain to values from 0 to the width minus the left and right margins (used to space out the visualization)
        .range([0, width - margins.left - margins.right]);

    // this does the same as for the y axis but maps from the rating variable to the height to 0. 
    var y = d3.scale.linear()
        .domain(d3.extent(data, function (d) {
        return d.rating;
    }))
    // Note that height goes first due to the weird SVG coordinate system
    .range([height - margins.top - margins.bottom, 0]);

    // we add the axes SVG component. At this point, this is just a placeholder. The actual axis will be added in a bit
    svg.append("g").attr("class", "x axis").attr("transform", "translate(0," + y.range()[0] + ")");
    svg.append("g").attr("class", "y axis");

    // this is our X axis label. Nothing too special to see here.
    svg.append("text")
        .attr("fill", "#414241")
        .attr("text-anchor", "end")
        .attr("x", width / 2)
        .attr("y", height - 35)
        .text("Impact");

    svg.append("text")
        .attr("fill", "#414241")
        .attr("text-anchor", "end")
        .style("transform", "rotate(-90deg)")
        .attr("x", "-296")
        .attr("y", "-29")
        .text("Interest");

    //Horizonotal quad-line
    svg.append("line")
        .attr("x1", 0)
        .attr("y1", height/2 - 30)
        .attr("x2", width)
        .attr("y2", height/2 - 30)
        .attr("stroke-width", 2)
        .attr("stroke", "#CECECE");

    //Vertical quad-line
    svg.append("line")
        .attr("x1", width/2 - 35)
        .attr("y1", 0)
        .attr("x2", width/2 - 35)
        .attr("y2", height)
        .attr("stroke-width", 2)
        .attr("stroke", "#CECECE");

    //Four quad-segments
    svg.append("rect")
        .attr("height", 25)
        .attr("width", 190)
        .style("fill", "#4D8DCE")
        .attr("x", 100)
        .attr("y", height - 130);

    svg.append("text")
        .attr("height", 20)
        .attr("width", 90)
        .style("fill", "#fff")
        .attr("x", 105)
        .attr("y", height - 113)
        .text("LOW IMPACT - LOW INTEREST");

    svg.append("rect")
        .attr("height", 25)
        .attr("width", 190)
        .style("fill", "#4D8DCE")
        .attr("x", 100)
        .attr("y", 50);

    svg.append("text")
        .attr("height", 20)
        .attr("width", 90)
        .style("fill", "#fff")
        .attr("x", 103)
        .attr("y", 67)
        .text("LOW IMPACT - HIGH INTEREST");

    svg.append("rect")
        .attr("height", 25)
        .attr("width", 197)
        .style("fill", "#4D8DCE")
        .attr("x", width/2 + 120)
        .attr("y", 50);

    svg.append("text")
        .attr("height", 20)
        .attr("width", 90)
        .style("fill", "#fff")
        .attr("x", width/2 + 123)
        .attr("y", 67)
        .text("HIGH IMPACT - HIGH INTEREST");

    svg.append("rect")
        .attr("height", 25)
        .attr("width", 190)
        .style("fill", "#4D8DCE")
        .attr("x", width/2 + 123)
        .attr("y", height - 130);

    svg.append("text")
        .attr("height", 20)
        .attr("width", 90)
        .style("fill", "#fff")
        .attr("x", width/2 + 126)
        .attr("y", height - 113)
        .text("HIGH IMPACT - LOW INTEREST");

    // this is the actual definition of our x and y axes. The orientation refers to where the labels appear - for the x axis, below or above the line, and for the y axis, left or right of the line. Tick padding refers to how much space between the tick and the label. There are other parameters too - see https://github.com/mbostock/d3/wiki/SVG-Axes for more information
    var xAxis = d3.svg.axis().scale(x).orient("bottom").tickPadding(2);
    var yAxis = d3.svg.axis().scale(y).orient("left").tickPadding(2);

    // this is where we select the axis we created a few lines earlier. See how we select the axis item. in our svg we appended a g element with a x/y and axis class. To pull that back up, we do this svg select, then 'call' the appropriate axis object for rendering.    
    svg.selectAll("g.y.axis").call(yAxis);
    svg.selectAll("g.x.axis").call(xAxis);

    // now, we can get down to the data part, and drawing stuff. We are telling D3 that all nodes (g elements with class node) will have data attached to them. The 'key' we use (to let D3 know the uniqueness of items) will be the name. Not usually a great key, but fine for this example.
    var chocolate = svg.selectAll("g.node").data(data, function (d) {
        return d.name;
    });

    // we 'enter' the data, making the SVG group (to contain a circle and text) with a class node. This corresponds with what we told the data it should be above.
    
    var chocolateGroup = chocolate.enter().append("g").attr("class", "node")
    // this is how we set the position of the items. Translate is an incredibly useful function for rotating and positioning items 
    .attr('transform', function (d) {
        return "translate(" + x(d.price) + "," + y(d.rating) + ")";
    })
    .attr('data-detail', function(d) {
        return d.category;
    })
    .attr('data-name', function(d) {
        return d.name;
    });

    // we add our first graphics element! A circle! 
    chocolateGroup.append("circle")
        .attr("r", 5)
        .attr("class", "dot")
        .style("fill", function (d) {
            // remember the ordinal scales? We use the colors scale to get a colour for our category. Now each node will be coloured
            // by who makes the chocolate. 
            return colors(d.category);
    });

    // now we add some text, so we can see what each item is.
    chocolateGroup.append("text")
        .style("text-anchor", "middle")
        .attr("dy", -10)
        .text(function (d) {
            // this shouldn't be a surprising statement.
            return d.name;
    });
}

$(document).ready(function() {
    $(".node").each(function(i, v) {
        var position = $(this).attr("transform");
        var a = position.split(")");
        var b = a[0].split("(");
        var c = b[1].split(",");

        var prevElem = $(this).prev('g').attr("transform");
        if(typeof(prevElem) != "undefined") {
            var d = prevElem.split(")");
            var e = d[0].split("(");
            var f = e[1].split(",");
        } else {
            f = [0,0];
        }

        if(c[0] == f[0] && c[1] == f[1]) {
            $(this).find('text').text('');
            $(this).prev('g').find('text').text('');
        }
    });

    $(".node").mouseover(function(e) {
        var position = $(this).attr("transform");
        var a = position.split(")");
        var b = a[0].split("(");
        var c = b[1].split(",");
        var left = + c[0] + 130;
        var top =  + c[1] + 210;
        var sameData = "";

        $(".node").each(function(i, v) {
            if(position == $(this).attr("transform")) {
                sameData += $(this).data('name') +': '+ $(this).data('detail') + "<br><br>";
            }
        });

        var tooltip = d3.selectAll(".tooltip:not(.css)");
        var HTMLfixedTip = d3.select("div.tooltip.fixed");
        
        tooltip.style("opacity", "1");
        tooltip.style("color", this.getAttribute("fill") );
        var matrix = this.getScreenCTM().translate(+this.getAttribute("cx"),+this.getAttribute("cy"));    
        HTMLfixedTip .style("left", left + "px").style("top",top + "px").html(sameData);
    }).mouseout(function(){
        var tooltip = d3.selectAll(".tooltip:not(.css)");
        tooltip.style("opacity", "0");
    });
});