<?php return $arr = array (
  'lead_auditor_chosen' => 'There is an audit pre-planned where you are lead auditor',
  'admin_profile_creation' => 'Your administrator-profile has been created',
  'end_user_creation' => 'Your user-profile has been created',
  'internal_auditor_assigned' => 'There is an upcoming audit where you will be support-auditor',
  'auditee_assigned' => 'There is an upcoming audit where you will be interviewed as auditee',
  'package_price_increase_request' => 'Package Price increase Request',
  'doc_change_nofitication' => 'Doc  added/modified in the process',
); ?>