<?php return $arr = array (
  'lead_auditor_chosen' => 'Il y a un audit à venir où vous serez l’auditeur principal',
  'admin_profile_creation' => 'Votre profil administrateur a été créé  (too-doo®)',
  'end_user_creation' => 'Votre profil utilisateur a été créé  (too-doo®)',
  'internal_auditor_assigned' => 'Il y a un audit à venir où vous serez un auditeur de soutien',
  'auditee_assigned' => 'Il y a un audit à venir au cours duquel vous serez interviewé en tant qu\'entité vérifiée',
  'package_price_increase_request' => 'Package Price increase Request',
  'doc_change_nofitication' => 'Doc. ajouté / modifié dans le processus',
); ?>