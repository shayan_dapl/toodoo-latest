<?php return $arr = array (
  'password' => 'Mots de passe doivent compter au moins six caractères et correspondre à la confirmation',
  'reset' => 'Votre mot de passe a été changé!',
  'sent' => 'Nous avons envoyé par mail un lien pour changer votre mot de passe.',
  'token' => 'Le code pour changer le mot de passe n\'est pas valide.',
  'user' => 'Nous ne trouvons pas d’utilisateur avec cette adresse e-mail.',
); ?>