<?php return $arr = array (
  'started_heading' => 'Commencer à utiliser too-doo : en 3 étapes nous vous aidons à travers les premiers paramètres',
  'branch_and_department' => '1: Branches & Départements',
  'branch_and_department_text' => 'Définissez ici les branches et Départements de votre entreprise',
  'function' => '2: Créer vos fonctions',
  'function_text' => 'Créer ici les différentes fonctions que comporte votre entreprise',
  'user' => '2a: Créer des utilisateurs',
  'user_text' => 'Définissez ici vos utilisateurs',
  'process' => '3: Créer des processus',
  'process_text' => 'Définissez les processus de votre entreprise',
  'no_thanks' => 'Remontrer ce menu la prochaine fois',
  'close' => 'Fermer',
  'branch_heading' => '1. Créer des branches ; Introduisez ci-dessous les noms des différentes branches. Dans le cas ou vous n’avez qu\'un branche, l\'option par défaut est le nom de votre entreprise. Vous pouvez changer ce nom si vous le voulez.',
  'edit' => 'Modifier',
  'delete' => 'Supprimer',
  'save' => 'Sauver',
  'next' => 'Prochain',
  'department_heading' => 'Créer des départements pour',
  'what_department_is' => 'Un département fait partie de l\'entreprise. Un département est responsable pour un ensemble de taches/responsabilité. Il existe différents départements comme la vente, l\'achat, la production,.......',
  'copy_department_from' => 'Copier les départements de',
  'copy' => 'Copier',
  'or' => 'OU',
  'start_from_scratch' => 'Commencer depuis le début (Template vide)',
  'function_heading' => 'Créer des fonctions: Une fonction(poste, rôle, profession) est un ensemble de taches, droits et obligations qu\'une personne doit exécuter dans un certain domaine. Par exemple dans le cadre d\'une entreprise ou association, mais aussi dans le cadre d\'un projet.',
  'want_to_create_function' => 'Je veux créer une fonction dans',
  'create_function' => 'Créer fonction',
  'every_branch' => 'Chaque branche',
  'create_function_in' => 'Créer des fonctions dans',
  'function_name' => 'Fonction',
  'parent_function' => 'Fonction apparenter',
  'add' => 'Ajouter',
  'see_existing_function' => 'Voir les fonctions existantes',
  'what_assistant_is' => 'When this person a personal assistant is, shift button to the right',
  'what_groupfunction_is' => 'When multiple persons have this function (eg. Customer Service operator), shift button to the right',
  'overview_functions' => 'Aperçu des fonctions existantes',
  'user_heading' => 'Créer un utilisateur : un utilisateur est un employé qui fait partie de l\'organisation. Avec une adresse email, la personne en question à accès au webtool. Dans le cas où vous n\'introduisez pas d\'email, la personne n\'apparaîtras que dans l\'organigram (sans avoir les droits d’accès au webtool)',
  'who_is_processowner' => 'Le propriétaire d\'un processus est un utilisateur qui a la responsabilité finale d\'un ou plusieurs processus',
  'who_is_auditor' => 'Un auditeur est une personne qui s\'occupe des audit intérieure et de les traiter dans too-doo',
  'what_is_invitation' => 'Quand il y a une invitation, l\'utilisateur en question est averti par email',
  'want_to_create_process' => 'Je veux créer un processus dans',
  'create_process' => 'Créer un processus',
  'see_existing_process' => 'Voir les processus existants',
  'process_heading' => 'Créer un processus : un processus est une suite d’activité logique',
  'what_process_is' => 'Un processus est une suite d’activité logique',
  'create_process_in' => 'Créer un processus dans',
  'parent_process' => 'Processus apparenter',
  'what_no_turtle_process_is' => 'Un processus qui n\'à pas de "diagramme tortue"',
  'see_existing_users' => 'Voir les utilisateurs existants',
  'no_turtle_process' => 'Process with turtle',
  'why_branch_disabled' => 'Certaines branches sont désactivé par-ce qu\'elles ne contiennes pas de département',
  'default_created_branch' => 'Par défaut, nous avons déjà créé la division "Management". Vous pouvez modifier le nom (p. ex. la direction, etc...) en cliquant sur "Modify" (Modifier)',
  'head_of_department' => 'Create first the head of department',
); ?>