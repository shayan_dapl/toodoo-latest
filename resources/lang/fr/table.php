<?php return $arr = array (
  'status' => 'STATUT',
  'operations' => 'ACTIONS',
  'no_data' => 'Pas de données trouvé',
  'photo' => 'PHOTO',
  'company' => 'SOCIETE',
  'tva_no' => 'TVA no.',
  'name' => 'NOM',
  'email' => 'E-MAIL',
  'address' => 'ADRESSE',
  'joined_on' => 'REJOINT LE',
  'process_name' => 'PROCESSUS',
  'process_owner' => 'PROPRIETAIRE',
  'category' => 'CATEGORIE',
  'doc_ref' => 'REF. DOC',
  'version' => 'VERSION',
  'input' => 'INPUT',
  'output' => 'OUTPUT',
  'language' => 'LANGUE',
  'target' => 'CIBLE',
  'risk' => 'RISQUE',
  'level' => 'NIVEAU',
  'opportunity' => 'OPPORTUNITE',
  'position' => 'POSITION',
  'responsible' => 'RESPONSABLE',
  'accountable' => 'RESPONSABLE PRINCIPAL',
  'consulted' => 'CONSULTE',
  'informed' => 'INFORME',
  'active' => 'Actif',
  'make_active' => 'Activer',
  'inactive' => 'Inactif',
  'make_inactive' => 'Inactiver',
  'block' => 'Bloquer',
  'make_block' => 'Indiquer comme bloqué',
  'blocked' => 'Bloqué',
  'make_unblock' => 'Débloquer',
  'remove' => 'Retirer',
  'no_assigned_process' => 'Vous n\'avez aucun processus assigné',
  'plan_year' => 'An',
  'plan_month' => 'Mois',
  'plan_date' => 'Date',
  'plan_time' => 'Heure',
  'clause' => 'Clause',
  'clause_desc' => 'Description Clause',
  'action' => 'Action',
  'doctype_name' => 'Nom type de document',
  'parent_clause' => 'Clause parent',
  'audit_program_audit_plan' => 'Programme Audit - Plan Audit',
  'root_cause_analysis' => 'Analyse cause',
  'corrective_measure' => 'Mesure corrective',
  'effectiveness' => 'Efficacité',
  'issue' => 'Problème',
  'issue_type' => 'Type de problème',
  'subject' => 'Sujet',
  'owner' => 'Propriétaire',
  'process' => 'Processus',
  'immidiate_actions' => 'Action(s) immédiate(s)',
  'created_at' => 'Date de Téléchargement',
  'attach' => 'Attachement',
  'package_name' => 'NOM PAQUET',
  'package_details' => 'DÉTAILS',
  'package_price' => 'PRIX',
  'package_member' => 'MEMBRES',
  'package_duration' => 'DURÉE',
  'immidiate_actions_info' => 'Information',
  'delegate_to' => 'Délégué à',
  'deadline' => 'Date limite',
  'mark_as_done' => 'Marqué comme terminé',
  'pending' => 'En attente',
  'done' => 'Terminé',
  'describe_action' => 'Décrire l\'action',
  'completed' => 'Complété',
  'on_going' => 'En cours',
  'not_fixed' => 'Ouvert',
  'required_action' => 'Action requise',
  'top_department' => 'Sommet',
  'special_user' => 'Utilisateur spécial',
  'admin' => 'Administrateur',
  'head_person' => 'Patron',
  'iso_reference' => 'Référence Iso',
  'trial_period' => 'Période d\'essai',
  'space' => 'Espace Server',
  'invoice_no' => 'N° de facture',
  'transaction_id' => 'Id. de Transaction',
  'payment_date' => 'Date de Payement',
  'payment_amount' => 'Montant',
  'view' => 'Voir',
  'extra_user' => 'Utilisateur extra',
  'per_month' => 'Par moi',
  'period' => 'Période',
  'vat_percent' => 'Pourcentage TVA',
  'country' => 'Pays',
  'last_login' => 'DERNIER LOGIN',
  'regulation_type_name' => 'Nom du type de régulation',
  'corporate' => 'Entreprise',
  'video' => 'Vidéo',
  'title' => 'Titre',
  'description' => 'Description',
  'branch' => 'Filiale',
  'Pending' => 'En attente',
  'not_started' => 'Pas démarré',
  'closed' => 'Fermé',
  'current_package_amount' => 'Montant actuel du paquet',
  'current_subscription_amount' => 'Montant actuel de l\'abonnement',
  'last_request_sent' => 'Dernière demande a été envoyée',
  'current_user_amount' => 'Montant d\'utilisateur actuel',
  'current_storage_amount' => 'Montant du stockage actuel',
  'package_price_change' => 'Modification du prix forfaitaire',
  'user_price_change' => 'Variation du prix pour l\'utilisateur',
  'storage_price_change' => 'Variation du prix de stockage',
  'proposed_percent' => 'Pourcentage proposé',
  'approve_status' => 'Approuver le statut',
  'approve_date' => 'Date d\'approbation',
  'active_since' => 'Active Since',
  'inactive_since' => 'Inactive since',
  'created_since' => 'Created Since',
  'last_login_since' => 'Last login since',
  'telephone_nr' => 'Téléphone',
  'invoice_date' => 'Invoice Date',
  'amount' => 'Amount',
  'vat_amount' => 'Vat Amount',
  'total_amount' => 'Total amount',
  'postal_code' => 'Postal Code',
  'city' => 'City',
); ?>