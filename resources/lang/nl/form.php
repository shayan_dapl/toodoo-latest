<?php return $arr = array (
  'title' => 'too-doo®',
  'login_username' => 'E-mail adres',
  'login_password' => 'Wachtwoord',
  'enter_username' => 'Vul Email in',
  'enter_password' => 'Vul wachtwoord in',
  'new_password' => 'Vul nieuw wachtwoord in',
  'conf_password' => 'Bevestig wachtwoord',
  'forgot_password' => 'Wachtwoord vergeten?',
  'sign_in' => 'Inloggen',
  'back' => 'Terug naar de lijst',
  'select' => 'Selecteer',
  'personal_details' => 'Gebruikersinfo',
  'company_details' => 'Bedrijfsgegevens',
  'email' => 'E-mail adres',
  'email_exists' => 'Dit e-mail adres bestaat al',
  'file' => 'Kies bestand',
  'picture' => 'Profielfoto',
  'image' => 'Selecteer een bestand',
  'first_name' => 'Voornaam',
  'last_name' => 'Achternaam',
  'password' => 'Wachtwoord',
  'confirm_password' => 'Bevestig wachtwoord',
  'company_name' => 'Bedrijfsnaam',
  'company_tva' => 'Bevestig BTW nr.',
  'addr1' => 'Adreslijn 1',
  'addr2' => 'Adreslijn 2',
  'street' => 'Straat',
  'zip' => 'Postcode',
  'city' => 'Stad',
  'country' => 'Land',
  'comp_city' => 'Stad',
  'comp_country' => 'Land',
  'language' => 'Taal',
  'save_button' => 'Opslaan',
  'name' => 'Input',
  'category' => 'Type proces',
  'process_name' => 'Naam van het proces',
  'process_owner' => 'Proceseigenaar',
  'assistant' => 'Assistent',
  'scope' => 'Details doel',
  'doc_ref' => 'Ref doc',
  'upload_doc' => 'Document opladen',
  'version' => 'Versie',
  'target' => 'Doel',
  'step_name' => 'Naam stap',
  'output' => 'Output',
  'risk' => 'Risico of kans',
  'risk_level' => 'Risico/Opp-niveau',
  'low' => 'Laag',
  'medium' => 'Medium',
  'high' => 'Hoog',
  'resource' => 'Middelen',
  'parent_position' => 'Bovenliggende positie',
  'position_name' => 'Naam functie',
  'info' => 'Informatie',
  'position' => 'Functie',
  'cancel' => 'Annuleer',
  'reporting_person' => 'Rapporteringslijn',
  'opportunity' => 'Omschrijf opportuniteit',
  'opportunity_level' => 'Niveau',
  'roles_responsible' => 'Verantwoordelijk (uitvoerder)',
  'roles_accountable' => 'Eindverantwoordelijk',
  'roles_consulted' => 'Geraadpleegd',
  'roles_informed' => 'Geïnformeerd',
  'only_one_parent_position' => 'Er kan maar een hoogste niveau zijn in de organisatie.',
  'company_picture' => 'Bedrijfslogo',
  'disable' => 'Uitzetten',
  'enable' => 'Aanzetten',
  'org_struct_exists' => 'Geef een gebruiker de hoogste functie hiervoor.',
  'handover' => 'Overdracht',
  'choose_process_to_view_turtle' => 'Kies een proces om het procesdiagramma te zien.',
  'input' => 'Input',
  'is_auditor' => 'Auditor',
  'linked_process' => 'Verbonden proces',
  'comment' => 'Opmerking',
  'no_turtle_process' => 'Proces zonder diagramma',
  'special_position' => 'Groepsfunctie',
  'special_user' => 'Teamlid',
  'no_email' => 'Gebruiker heeft geen email',
  'out_of_service' => 'Buiten dienst',
  'audit_define' => 'Bepaal de audit',
  'choose_lead_auditor' => 'Kies de lead-auditor',
  'choose_process' => 'Kies proces',
  'clause' => 'Artikel',
  'choose_audit_month' => 'Kies maand van de audit',
  'choose_audit_date' => 'Kies datum van de audit',
  'audit_title' => 'Titel van de audit',
  'clause_desc' => 'Beschrijving van het artikel',
  'subprocess' => 'Subproces',
  'doctype' => 'Type document',
  'clause_parent' => 'Bovenliggend niveau',
  'lead_auditor' => 'Lead-auditor',
  'process_to_be_audited' => 'Te auditen proces',
  'choose_audit_time' => 'Kies tijd voor de audit',
  'audit_preparation' => 'Voorbereiding audit',
  'objective' => 'Doel',
  'assist_as' => 'Ondersteunt als',
  'clause_nr' => 'Nr paragraaf',
  'text' => 'Tekst',
  'iso_reference' => 'ISO referentie',
  'visited_location' => 'Bezochte locaties',
  'observation' => 'Observaties',
  'finding_type' => 'aType bevinding',
  'reference' => 'Referentie norm',
  'evidence' => 'Bewijs',
  'audit_finding_type' => 'Type audit-bevinding',
  'release_button' => 'Vrijgeven',
  'related_process' => 'Gerelateerd proces',
  'report_issue' => 'Rapporteer bevinding',
  'source_type' => 'Bron van melding',
  'issue' => 'Bevinding',
  'describe_here_the_issue_you_want_to_report' => 'Beschrijf hier de bevinding die je wilt melden',
  'like_to_change_password' => 'Wachtwoord veranderen?',
  'update' => 'Update',
  'skip' => 'Overslaan',
  'make_customer_admin' => 'Admin',
  'audit_source_type' => 'bron melding',
  'reference_std' => 'Std referentie',
  'root_cause_analysis' => 'Analyse oorzaak',
  'define_corrective_action' => 'Correctieve actie bepalen',
  'effectiveness' => 'Effectiviteit',
  'describe_here_root_cause_analysis' => 'Beschrijf hier de analuse van de oorzaak',
  'describe_here_corrective_action' => 'Beschrijf hier de correctieve actie',
  'describe_here_effectiveness' => 'Beschrijf hier de effectiviteit',
  'report_delegation' => 'Meld overdracht',
  'user_with_position' => 'Gebruiker met deze functie',
  'documents' => 'Documenten',
  'special_users' => 'Teamleden',
  'not_in_list' => 'Niet in de lijst',
  'special_user_demo_file_note' => 'Download een procesformulier hierboven om de schildpad te zien',
  'context_subject' => 'Context onderwerp',
  'subject' => 'Onderwerp',
  'context_analysis' => 'Analyse context',
  'describe_issue' => 'Beschrijf belangrijke punt (issue)',
  'internal_or_external' => 'Intern/Extern',
  'processlink' => 'Gerelateerd proces',
  'owner' => 'Eigenaar',
  'monitoring_info' => 'Hoe volg je deze issue op',
  'possible_influence' => 'Hoe kan deze issue je organisatie beïnvloeden ?',
  'impact' => 'Hoe groot is de impact ?',
  'description' => 'Beschrijving',
  'context_analysis_list' => 'Lijst Analyse Context',
  'report_finding' => 'Rapporteer bevinding',
  'immidiate_actions' => 'Directe acties',
  'describe_here_immidiate_actions' => 'Beschrijf hier de directe acties',
  'upload' => 'Upload',
  'new_version' => 'Nieuwe versie',
  'risk_list' => 'Lijst risico\'s',
  'details' => 'Details',
  'price' => 'Prijs',
  'member' => 'Lid',
  'duration' => 'Duur',
  'special_single_user' => 'Een gebruiker',
  'special_multiple_user' => 'Meerdere gebruikers',
  'internal_external' => 'Internal/External',
  'stakeholder_internal' => 'Intern',
  'stakeholder_external' => 'Extern',
  'stakeholder_name' => 'Naam belanghebbende',
  'needs_expectations' => 'Noden en verwachtingen',
  'detail' => 'Detail',
  'impacts' => 'Impact',
  'influence' => 'Invloed',
  'action' => 'Actie',
  'deadline' => 'Deadline',
  'reporting_source' => 'rapportagebron',
  'action_taken_by' => 'Actie uitgevoerd door',
  'taken_action_description' => 'Omschrijving actie',
  'closed' => 'Afgesloten',
  'report_finding_list' => 'Lijst bevindingen',
  'fixed_on' => 'Vastgelegd op',
  'who' => 'Wie',
  'define_action' => 'Bepaal actie',
  'define_deadline' => 'Bepaal deadline',
  'add_action' => 'Actie toevoegen',
  'comment_on' => 'Datum',
  'comment_by' => 'Opmerking door',
  'top_person' => 'Hoofd van de organisatie',
  'describe_here_requested_actions' => 'beschrijf hier de verwachte actie',
  'reported_by' => 'Gerapporteerd door',
  'this_is_parent_position' => 'Dit is de afdelingsverantwoordelijke',
  'organization_chart' => 'Organigram',
  'opportunity_list' => 'Lijst opportuniteiten',
  'base_package' => 'Basis pakket',
  'package_desc' => 'Pakket omschrijving',
  'mail_body' => 'Mail inhoud',
  'step_no' => 'Stapnr',
  'created_at' => 'Aangemaakt op',
  'trial_period_days' => 'Test periode (#dagen)',
  'max_package' => 'Max pakket',
  'risk_exist' => 'Naam risico bestaat al',
  'opportunity_exist' => 'Opportuniteit bestaat al',
  'select_process' => 'Selecteer proces',
  'process_owner_accountable' => 'proceseigenaar/eindveranwoordelijke',
  'document_link' => 'Document Link',
  'invite_user' => 'Verstuur uitnodiging',
  'resent_invitation_mail' => 'Verstuur de invitatiemail',
  'remark' => 'Opmerking',
  'old_version' => 'Oude versie',
  'create_password' => 'Creëer paswoord',
  'upload_date' => 'Datum upload',
  'yes' => 'Ja',
  'no' => 'Neen',
  'phone_optional' => 'Telefoonnr (optioneel)',
  'bus' => 'Bus',
  'my_company_applies' => 'BTW plichtig',
  'just_some_formalities' => 'Gewoon nog enkele formaliteiten',
  'convinced' => 'Overtuigd',
  'just_checking_your_address' => 'We controleren je adres',
  'checking_address' => 'We controleren je adres',
  'vat_no_please' => 'Voeg je BTW nr toe',
  'data_still_correct' => 'Is deze informatie nog steeds correct?',
  'bad' => 'Slecht',
  'weak' => 'Zwak',
  'fair' => 'Aanvaardbaar',
  'good' => 'Goed',
  'very_good' => 'Zeer goed',
  'not_matching' => 'Niet correct',
  'matched' => 'Is correct',
  'tva_no' => 'BTW nr',
  'position_function_details' => 'Functie details',
  'corrective_action' => 'Corretieve actie',
  'add_step' => 'Voeg stap toe',
  'report_none' => 'Reports No one',
  'resource_activity' => 'Resource activiteit',
  'select_activity' => 'Selecteer activiteit',
  'activity' => 'Activiteit',
  'brand' => 'Merk',
  'type' => 'Type',
  'serial_no' => 'Serienr',
  'does_not_applied_vat' => 'Mijn bedrijf heeft geen BTW nr',
  'is_this_member_auditor' => 'Is deze medewerker ook een interne auditor ?',
  'regulation_ref' => 'Regulatie ref',
  'regulation_type' => 'Type regelgeving',
  'choose_audit_responsible' => 'Kies auditverantwoordelijke',
  'low_risk' => 'Laag risico',
  'medium_risk' => 'Medium risico',
  'high_risk' => 'Hoog risico',
  'low_opportuity' => 'Kleine opportuniteit',
  'medium_opportuity' => 'Medium opportuniteit',
  'high_opportuity' => 'Grote opportuniteit',
  'who_is_auditor' => 'Een auditor is iemand die een proces evalueert',
  'who_is_processowner' => 'Proceseigenaar is de persoon die een proces beheert',
  'what_is_process_type' => 'Management, kern en ondersteunende processen',
  'month' => 'Maand',
  'photo' => 'Gebruikersfoto',
  'audit_handover' => 'Enkel doorgeven',
  'video_title' => 'Title',
  'video_desc' => 'Omschrijving',
  'discard' => 'Verder',
  'kpi_name' => 'KPI naam',
  'valid_from' => 'Geldig',
  'valid_to' => 'Geldig tot',
  'kpi_unit' => 'KPI eenheid',
  'assign_user' => 'Wijs gebruiker toe',
  'person' => 'gebruiker(s)',
  'auditors' => 'Auditoren',
  'nr' => 'Nr',
  'zip_code' => 'Postcode',
  'frequency' => 'Frequentie',
  'no_of_user' => 'Aantal gebruikers',
  'vat_percent' => 'BTW %',
  'audit_category' => 'Audit categorie',
  'branch' => 'Vestiging/locatie',
  'audit_company' => 'Audit organisatie',
  'process_step_list' => 'Lijst processtappen',
  'location_doc' => 'Locatie doc',
  'master_kpi' => 'Master KPI',
  'video_link' => 'Video Link',
  'inform_user' => 'Informeer gebruiker',
  'day' => 'Dag',
  'weeknumber' => 'Weeknummer',
  'monthnumber' => 'maandnummer',
  'quarternumber' => 'Kwartallnummer',
  'supplier_name' => 'Leveranciersnaam',
  'product_services' => 'Producten/diensten',
  'evaluated' => 'te evalueren',
  'update_deadline' => 'Update Deadline',
  'year' => 'Year',
  'message_to_supplier' => 'Message to supplier',
  'package_name' => 'Package Name',
  'end_subscription' => 'End subscription',
); ?>