<?php return $arr = array (
  'password' => 'Wachtwoorden moeten minstens 6 karakters lang zijn en overeenstemmen met de bevestiging.',
  'reset' => 'Uw wachtwoord is gewijzigd!',
  'sent' => 'We hebben u een e-mail gestuurd met een link om uw wachtwoord te wijzigen.',
  'token' => 'Deze code voor het wijzigen van uw paswoord is ongeldig.',
  'user' => 'We kunnen geen gebruiker met dat e-mail adres vinden.',
); ?>