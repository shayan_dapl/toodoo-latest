<?php

return $arr = [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'De :attribute Moet worden geaccepteerd.',
    'active_url'           => 'De :attribute Is geen geldige URL.',
    'after'                => 'De :attribute Moet een datum zijn :date.',
    'after_or_equal'       => 'De :attribute Moet een datum zijn of gelijk aan :date.',
    'alpha'                => 'De :attribute Mag alleen letters bevatten.',
    'alpha_dash'           => 'De :attribute Mag alleen letters, cijfers en streepjes bevatten.',
    'alpha_num'            => 'De :attribute Mag alleen letters en cijfers bevatten.',
    'array'                => 'De :attribute must be an array.',
    'before'               => 'De :attribute must be a date before :date.',
    'before_or_equal'      => 'De :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'De :attribute must be between :min and :max.',
        'file'    => 'De :attribute must be between :min and :max kilobytes.',
        'string'  => 'De :attribute must be between :min and :max characters.',
        'array'   => 'De :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'De :attribute field must be true or false.',
    'confirmed'            => 'De :attribute komt niet overeen.',
    'date'                 => 'De :attribute Is geen geldige datum.',
    'date_format'          => 'De :attribute does not match the format :format.',
    'different'            => 'De :attribute and :other must be different.',
    'digits'               => 'De :attribute must be :digits digits.',
    'digits_between'       => 'De :attribute must be between :min and :max digits.',
    'dimensions'           => 'De :attribute has invalid image dimensions.',
    'distinct'             => 'De :attribute field has a duplicate value.',
    'email'                => 'De :attribute Moet een geldig e-mail adres zijn.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'De :attribute must be a file.',
    'filled'               => 'De :attribute field must have a value.',
    'image'                => 'De :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'De :attribute field does not exist in :other.',
    'integer'              => 'De :attribute Moet een geheel getal zijn.',
    'ip'                   => 'De :attribute must be a valid IP address.',
    'json'                 => 'De :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'De :attribute may not be greater than :max.',
        'file'    => 'De :attribute may not be greater than :max kilobytes.',
        'string'  => 'De :attribute may not be greater than :max characters.',
        'array'   => 'De :attribute may not have more than :max items.',
    ],
    'mimes'                => 'De :attribute must be a file of type: :values.',
    'mimetypes'            => 'De :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'De :attribute must be at least :min.',
        'file'    => 'De :attribute must be at least :min kilobytes.',
        'string'  => 'De :attribute must be at least :min characters.',
        'array'   => 'De :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'De :attribute must be a number.',
    'present'              => 'De :attribute field must be present.',
    'regex'                => 'De :attribute format is invalid.',
    'required'             => 'De :attribute veld is verplicht.',
    'required_if'          => 'De :attribute field is required when :other is :value.',
    'required_unless'      => 'De :attribute field is required unless :other is in :values.',
    'required_with'        => 'De :attribute field is required when :values is present.',
    'required_with_all'    => 'De :attribute field is required when :values is present.',
    'required_without'     => 'De :attribute field is required when :values is not present.',
    'required_without_all' => 'De :attribute field is required when none of :values are present.',
    'same'                 => 'De :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'De :attribute must be :size.',
        'file'    => 'De :attribute must be :size kilobytes.',
        'string'  => 'De :attribute must be :size characters.',
        'array'   => 'De :attribute must contain :size items.',
    ],
    'string'               => 'De :attribute must be a string.',
    'timezone'             => 'De :attribute must be a valid zone.',
    'unique'               => 'De :attribute has already been taken.',
    'uploaded'             => 'De :attribute failed to upload.',
    'url'                  => 'De :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
