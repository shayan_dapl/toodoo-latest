@extends('layouts.main')

@section('content')
<i>{!! Html::link(url('/'), translate('words.back_to_login_screen'), array('class' => 'text-system pull-right pr15')) !!}</i>
<form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
    {{ csrf_field() }}
    <div class="p15 pt25">
        @if (session('status'))
            <div class="alert alert-success">
                {{ translate('passwords.sent') }}
            </div>
        @else
            <div class="alert alert-micro alert-border-left alert-info pastel alert-dismissable mn">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="fa fa-info pr10"></i> {{translate('words.fgt_pwd_msg')}}
            </div>
        @endif
    </div>
    <div class="panel-footer p25 pv15">
        <div class="section mn" style="display: inline-flex; width: 100%;">
            <div class="smart-widget sm-right smr-80" style="float: left; width: 90%;">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">{{translate('words.email')}}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{translate('passwords.user')}}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-6">
                        <button type="submit" class="btn btn-system">
                            {{translate('words.send')}}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
