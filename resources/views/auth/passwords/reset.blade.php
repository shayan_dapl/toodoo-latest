@extends('layouts.main')
@section('content')
    {!! Html::style('css/extra-styles/password_reset.css') !!}
    <div class="row" id="passwordresetpassword" v-cloak>
        <div class="col-md-12">
            <h2 class="text-center text-system">{{translate('words.reset_password')}}</h2>

            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">{{translate('form.email')}}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{translate('passwords.user')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">
                            {{translate('form.new_password')}}
                            <i class="fa fa-info-circle text-danger pointer" data-container="body" data-toggle="popover" data-placement="top" data-content="{{translate('words.password_strength')}}" data-original-title="" title=""></i>
                        </label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required v-model="password" v-on:keyup="passStrength()">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                            <div class="progress mt10" v-if="password != ''">
                                <div v-bind:class="passclass" role="progressbar" v-bind:aria-valuenow="passval" aria-valuemin="0" aria-valuemax="100">@{{passval}}</div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="col-md-4 control-label">{{translate('form.conf_password')}}</label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required v-model="confpassword" v-on:keyup="passMatch()">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                            <div class="progress mt10" v-if="confpassword != ''">
                                <div v-bind:class="confpassclass" role="progressbar" v-bind:aria-valuenow="confpassval" aria-valuemin="0" aria-valuemax="100">@{{confpasstext}}</div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-6">
                            <button type="submit" class="btn btn-system" :disabled="!enable">
                                {{translate('words.reset_password')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('includes.scripts.password_script')
@endsection
