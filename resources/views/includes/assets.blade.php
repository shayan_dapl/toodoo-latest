@push('styles')
<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
<link rel="stylesheet" type="text/css" href="{{asset('css/theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/admin-forms.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.orgchart.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.timepicker.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('js/vendor/plugins/select2/css/core.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/style-extended.css')}}">
@endpush
@push('header_scripts')
<script src="{{asset('js/vendor/jquery/jquery-3.1.0.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery/jquery_ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery/jquery_ui/jquery-ui-i18n.min.js')}}"></script>
<script type="text/javascript">
    var dtOption = $.extend({},
    $.datepicker.regional["{{session('lang')}}"], { 
        dateFormat: "dd-mm-yy",
        minDate: 0
    });
</script>
@endpush
@push('body_scripts')
<script src="{{asset('js/vendor/plugins/datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('js/vendor/plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script> 
<script src="{{asset('js/vendor/plugins/pnotify/pnotify.js')}}"></script>
<script src="{{asset('js/vendor/plugins/jquerymask/jquery.maskedinput.min.js')}}"></script>
<script src="{{asset('js/jquery-ui-monthpicker.min.js')}}"></script>
<script src="{{asset('js/vendor/plugins/duallistbox/jquery.bootstrap-duallistbox.min.js')}}"></script>
<script src="{{asset('js/jquery.timepicker.js')}}"></script>
<script src="{{asset('js/tree.js')}}"></script>
<script src="{{asset('js/vendor/plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('js/assets/js/utility/utility.js')}}"></script>
<script src="{{asset('js/assets/js/main.js')}}"></script>
<script src="{{asset('js/assets/js/filterDropDown.js')}}"></script> 

<script src="{{asset('js/assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/assets/js/pdfmake.min.js')}}"></script> 
<script src="{{asset('js/assets/js/vfs_fonts.js')}}"></script> 
<script src="{{asset('js/assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/data-table-exnd/script.js')}}"></script>
<!-- Google Tag Manager -->
<script>
    (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer',  "{{env('GOOGLE_TAG_MANAGER_KEY')}}");

    var dtLangs = {
        "language": {
            "lengthMenu": "{{translate('pagination.display')}} _MENU_ {{translate('pagination.record_per_page')}}",
            "zeroRecords": "{{translate('pagination.nothing_found')}}",
            "info": "{{translate('pagination.showing_page')}} _PAGE_ {{translate('pagination.of')}} _PAGES_",
            "infoEmpty": "{{translate('pagination.no_records')}}",
            "infoFiltered": "({{translate('pagination.filterd_from')}} _MAX_ {{translate('pagination.total_records')}})",
            "oPaginate" : {
                "sFirst": "{{translate('pagination.first')}}",
                "sLast": "{{translate('pagination.last')}}",
                "sNext": "{{translate('pagination.next')}}",
                "sPrevious": "{{translate('pagination.previous')}}"
            },
            "sSearch" : "{{translate('pagination.search')}}"
        }
    }
</script>
@endpush