<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-6">
            <span class="footer-legal">&copy; {{date("Y")}} too-doo&reg;</span>
        </div>
        <div class="col-xs-6 text-right">
            <a href="#content" class="footer-return-top">
                <span class="fa fa-arrow-up"></span>
            </a>
        </div>
    </div>
</footer>
</section>
</div>

<!-- Theme Javascript -->
@stack('body_scripts')
@include('includes.scripts')
@inject('loggedUserStatus', 'App\Services\Sidebar')
@if (Auth::guard('customer')->check())
<script type="text/javascript">
    var tdWfx = {
        fName: "{{Auth::guard('customer')->user()->fname}}",
        lName: "{{Auth::guard('customer')->user()->lname}}",
        language: "{{!empty(session('langName')) ? session('langName') : Auth::guard('customer')->user()->language}}",
        emailId: "{{Auth::guard('customer')->user()->email}}",
        role: [{{$loggedUserStatus->loggedUserStatus()}}],
        dateJoined: "{{date('m/d/Y', strtotime(Auth::guard('customer')->user()->created_at))}}",
        regSince : "{{dayDifference(Auth::guard('customer')->user()->created_at)}}",
        returnUser: "{{(Auth::guard('customer')->user()->not_yet_logged == 0) ? 'no' : 'yes'}}",
    };
</script>
@endif
</body>
</html>