<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>too-doo&reg;</title>
    <meta name="keywords" content="Fuller Opex Tool" />
    <meta name="description" content="Full Opex Admin Tool">
    <meta name="author" content="Fuller">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include('includes.assets')
    @stack('styles')
    @stack('header_scripts')
    <!-- End Google Tag Manager -->
    @if(\Config::get('app.env') != "local")
        <!-- HOTJAR CODE -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:1126896,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
    @endif
</head>
<body class="dashboard-page">	
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NCTWFHP" height="0" width="0" class="hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="main">
        <header class="navbar navbar-fixed-top navbar-shadow bg-system">
            <div class="navbar-branding">
                <a class="navbar-brand" href="{{url('/home')}}">
                    <img src="{{asset('image/too-doo-logo.png')}}" height="50">
                </a>
                <span id="toggle_sidemenu_l" class="fa fa-align-justify text-tealt"></span>
            </div>

            @if(Auth::guard('customer')->check())
                <span class="dashboard-logo">
                    @if(Auth::guard('customer')->check())
                        @if(\File::isFile('storage/company/'.Auth::guard('customer')->user()->company_id.'/image/'.Auth::guard('customer')->user()->logo))
                            <img id="imageid" src="{{asset('storage/company/'.Auth::guard('customer')->user()->company_id.'/image/'.Auth::guard('customer')->user()->logo.'?'.rand(0,9))}}" height="50" class="fileupload-preview thumbnail">
                        @else
                            <img id="imageid" src="{{asset('/image/placeholder.png')}}" height="50" class="fileupload-preview thumbnail">
                        @endif
                        <span class="company-name">
                            {{ Auth::guard('customer')->user()->company->name }}
                        </span>
                    @endif
                </span>
            @endif
            
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown menu-merge">
                    <div class="navbar-btn btn-group">
                        <button data-toggle="dropdown" class="btn btn-sm dropdown-toggle" title="{{ !empty(session('langName'))?session('langName'):translate('words.nl') }}" id="header-language">
                            {{ !empty(session('langName')) ? translate('words.'.session('langName')) : translate('words.nl') }}
                        </button>
                        <ul class="dropdown-menu pv5 animated animated-short white-drop flipInX" role="menu">
                            <li>
                                <a class="chgLang" data-id="nl" href="javascript:void(0);">
                                    {{translate('words.nl')}}
                                </a>
                            </li>
                            <li>
                                <a class="chgLang" data-id="en" href="javascript:void(0);">
                                    {{translate('words.en')}} 
                                </a>
                            </li>
                            <li>
                                <a class="chgLang" data-id="fr" href="javascript:void(0);">
                                    {{translate('words.fr')}} 
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="menu-divider hidden-xs">
                </li>

                <li class="dropdown menu-merge">
                    <a href="#" class="dropdown-toggle fw700 p15" data-toggle="dropdown">
                        @if(Auth::guard('customer')->check())
                            @if(\File::isFile('storage/company/'.Auth::guard('customer')->user()->company_id.'/image/users/'.Auth::guard('customer')->user()->photo))
                                <img src="{{asset('storage/company/'.Auth::guard('customer')->user()->company_id.'/image/users/'.Auth::guard('customer')->user()->photo)}}" alt="avatar" class="mw30 br64">
                            @else
                                <img src="{{asset('/image/placeholder.png')}}" alt="avatar" class="mw30 br64">
                            @endif
                        @elseif(Auth::guard('web')->check())
                            @if(\File::isFile('storage/company/'.Auth::guard('web')->user()->company_id.'/image/users/'.Auth::guard('web')->user()->photo))
                                <img src="{{asset('storage/company/'.Auth::guard('web')->user()->company_id.'/image/users/'.Auth::guard('web')->user()->photo)}}" alt="avatar" class="mw30 br64">
                            @else
                                <img src="{{asset('/image/placeholder.png')}}" alt="avatar" class="mw30 br64">
                            @endif
                        @endif
                        <span class="hidden-xs pl15 pr5" id="header-profile">
                            @if(Auth::guard('customer')->check())
                                {{Auth::guard('customer')->user()->name}}
                            @else
                                {{Auth::guard('web')->user()->name}}
                            @endif
                        </span>
                        <span class="fa fa-angle-down hidden-xs"></span>
                    </a>
                    <ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
                        <li class="list-group-item">
                            <a href="{{url('/profile')}}" class="animated animated-short fadeInUp">
                                <span class="fa fa-user"></span> {{translate('words.profile')}}
                            </a>
                        </li>
                        @if(Auth::guard('customer')->check() and Auth::guard('customer')->user()->type == 2)
                        <li class="list-group-item">
                            <a href="{{url('/membership/account')}}" class="animated animated-short fadeInUp">
                                <span class="fa fa-credit-card"></span> {{translate('words.account_information')}}
                            </a>
                        </li>
                        @endif
                        <li class="list-group-item">
                            <a href="{{url('/policy')}}" class="animated animated-short fadeInUp">
                                <span class="fa fa-shield"></span> {{translate('words.policy')}}
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{url('/password')}}" class="animated animated-short fadeInUp">
                                <span class="fa fa-cogs"></span> {{translate('words.change_password')}}
                            </a>
                        </li>
                        <li class="dropdown-footer">
                            <a href="{{url('/out')}}" class=""> {{translate('words.logout')}} </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </header>