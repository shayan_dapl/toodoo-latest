@push('styles')
<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
<link rel="stylesheet" type="text/css" href="{{asset('css/theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/admin-forms.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('fonts/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/style-extended.css')}}">
@endpush
@push('scripts')
    <script src="{{asset('js/vendor/jquery/jquery-3.1.0.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery/jquery_ui/jquery-ui.min.js')}}"></script>
    <!-- CanvasBG Plugin(creates mousehover effect) -->
    <script src="{{asset('js/vendor/plugins/canvasbg/canvasbg.js')}}"></script>
    <!-- Theme Javascript -->
    <script src="{{asset('js/vendor/plugins/pnotify/pnotify.js')}}"></script>
    <script src="{{asset('js/assets/js/utility/utility.js')}}"></script>
    <script src="{{asset('js/assets/js/demo/demo.js')}}"></script>
    <script src="{{asset('js/assets/js/main.js')}}"></script>
@endpush