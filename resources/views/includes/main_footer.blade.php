@stack('body_scripts')
 <script>
    (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', "{{env('GOOGLE_TAG_MANAGER_KEY')}}");
 </script>

<!-- Page Javascript -->
<script type="text/javascript">
jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core      
    Core.init();

    // Init Demo JS
    Demo.init();

    // Init CanvasBG and pass target starting location
    CanvasBG.init({
        Loc: {
            x: window.innerWidth / 2,
            y: window.innerHeight / 3.3
        },
    });
});

//Forgot password on forgot_password.blade.php
$("#chgPassword").click(function () {
    $(this).html('<i class="fa fa-refresh fa-spin fa-1x fa-fw"></i>');
    var email = $('#email').val();
    var token = $('#token').val();
    if (email == "")
    {
        new PNotify({
            title: "{{translate('alert.reset_password')}}",
            text: "{{translate('alert.enter_email_address')}}",
            addclass: 'stack_top_right',
            type: 'danger',
            width: '290px',
            delay: 2000
        });
        $(this).html('Send');
    }
    else
    {
        $.ajax({
            url: "{{url('/forgot-password')}}",
            type: "POST",
            data: {'email': email, '_token': token},
            success: function (data) {
                if (data == 'success') {
                    $("#chgPassword").html('Sent');
                    new PNotify({
                        title: "{{translate('alert.reset_password')}}",
                        text: "{{translate('alert.mail_sent_successfully')}}",
                        addclass: 'stack_top_right',
                        type: 'success',
                        width: '290px',
                        delay: 2000
                    });
                }
                if (data == 'faliure') {
                    $("#chgPassword").html('Send');
                    new PNotify({
                        title: "{{translate('alert.reset_password')}}",
                        text: "{{translate('alert.mail_not_sent')}}",
                        addclass: 'stack_top_right',
                        type: 'danger',
                        width: '290px',
                        delay: 2000
                    });
                }
            }
        });
    }
});

//Site language changing on header.blade.php
$(".chgLang").click(function () {
    var el = $(this);
    var innerEl = $(this).html();
    $(this).html(innerEl + ' <i class="fa fa-refresh fa-spin fa-1x"></i>');
    var langName = $(this).data("id");
    var term = $(this).text();
    $.ajax({
        url: "{{url('/lang')}}/" + langName,
        type: "GET",
        success: function (data) {
            el.find('i').remove();
            new PNotify({
                title: '{{translate("alert.language")}} : ' + term,
                text: "{{translate('alert.updated_successfully')}}",
                addclass: 'stack_top_right',
                type: 'success',
                width: '290px',
                delay: 2000
            });
            $(document).delay(1000).queue(function () {
                location.reload();
            });
        }
    });
});
</script>
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
</body>
</html>