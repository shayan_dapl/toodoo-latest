<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{translate('form.title')}}</title>
        <meta name="keywords" content="Fuller Opex" />
        <meta name="description" content="Fuller Opex Admin Tool">
        <meta name="author" content="Opex">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @include('includes.assets')
        @include('includes.main_assets')
        @stack('styles')
        @stack('header_scripts')
        <!-- End Google Tag Manager -->
        @if(\Config::get('app.env') != "local")
            <!-- HOTJAR CODE -->
            <script>
                (function(h,o,t,j,a,r){
                    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                    h._hjSettings={hjid:1126896,hjsv:6};
                    a=o.getElementsByTagName('head')[0];
                    r=o.createElement('script');r.async=1;
                    r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                    a.appendChild(r);
                })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
            </script>
        @endif
        <link rel="stylesheet" href="{{asset('css/app.css')}}" />
    </head>