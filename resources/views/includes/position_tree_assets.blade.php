@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('js/position-tree/jqx.base.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('js/position-tree/jqx.energyblue.css')}}">
@endpush
@push('scripts')
<script type="text/javascript" src="{{asset('js/position-tree/jqx-all.js')}}"></script>
@endpush
