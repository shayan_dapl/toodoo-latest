<!-- Get started popup -->
@if(Auth::guard('customer')->check() and Auth::guard('customer')->user()->type == 2 and Auth::guard('customer')->user()->company->getstarted == 0)
    <div id="get-started-popup" class="modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-dialog-started mxWidth670">
            <div class="modal-content">
                <div class="modal-header bg-system text-default">
                    <button type="button" id="backHome" class="close" data-id="temporary">&times;</button>
                    <h4 class="modal-title">&nbsp;</h4>
                </div>
                <div class="modal-body" id="get-started-popup-body">
                </div>
                <div class="modal-footer">
                    <div class="switch switch-system switch-started round switch-inline pull-left mt10">
                        <input id="permanent-close" class="dismiss" data-id="permanent" @if(empty(session('dismiss_started'))) checked="" @endif type="checkbox">
                        <label for="permanent-close"></label>
                    </div>
                    <span class="pull-left mt5 ml5">{{translate('started.no_thanks')}}</span>
                </div>
            </div>
        </div>
    </div>
@endif

<script type="text/javascript">
    function preview(input, output, outputname) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var type = input.files[0].name.split(".")[1];
                var extnsions = ['jpeg', 'jpg', 'png'];
                if (extnsions.indexOf(type.toLowerCase()) !== -1) {
                    $('#' + output).attr('src', e.target.result);
                    $('#' + outputname).val(input.files[0].name);
                } else {
                    new PNotify({
                        title: "{{translate('alert.not_supported_file')}}",
                        text: "",
                        addclass: 'stack_top_right',
                        type: 'error',
                        width: '290px',
                        delay: 2000
                    });
                    input.value = "";
                    return false;
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function setMonth(year, month) {
        var currentDay = $("#datepicker").datepicker("getDate") || new Date();
        var day = currentDay.getDay();
        newDate = new Date(year, month, day);
        $("#datepicker").datepicker("setDate", newDate);
    }

    function select_preview_month(yr, mnth, dat) {
        var now = new Date(yr, mnth, dat);
        var first_day = new Date(now.getFullYear(), now.getMonth(), 1);
        var last_day = new Date(now.getFullYear(), now.getMonth(), 0);

        var d = new Date();
        var n = "{{date('m')}}";
        var dt = d.getDate();
        if (mnth == 12) {
            var startDate = yr + '-' + mnth + '-' + first_day.getDate();
            var endDate = yr + '-' + mnth + '-' + last_day.getDate();
        } else if (mnth == n) {
            var startDate = yr + '-' + mnth + '-' + dt;
            var endDate = yr + '-' + mnth + '-' + last_day.getDate();
        } else {
            var startDate = first_day.getFullYear() + '-' + first_day.getMonth() + '-' + first_day.getDate();
            var endDate = last_day.getFullYear() + '-' + first_day.getMonth() + '-' + last_day.getDate();
        }

        var dateRange = { 'startDate': startDate, 'endDate': endDate };
        return dateRange;
    }

    function removeTrialBanner() {
        $.ajax({
            url : "{{url('/remove-trial-banner')}}",
            type : "GET",
            success : function(data) {
                if (data == true) {
                    document.getElementById('package-trial').remove();
                }
            }
        });
    }

    jQuery(document).ready(function() {
        "use strict";
        // Init Theme Core
        Core.init();
        $('#resent_mail').click(function() {
            var userId = $(this).attr('data-id');
            $('#ajxLoader').removeClass('hide');
            $.ajax({
                url: "{{url('/resend_invitation_mail')}}/" + userId,
                type: "GET",
                success: function (data) {
                    setTimeout(function(){ $('#ajxLoader').addClass('hide'); }, 500);
                    new PNotify({
                        text: "{{translate('alert.mail_sent')}}",
                        addclass: 'stack_top_right',
                        type: 'success',
                        width: '290px',
                        delay: 2000
                    });
                }
            });
        });

        $('#skin-toolbox .panel-heading').on('click', function() {
            $('#skin-toolbox').toggleClass('toolbox-open');
        });

        //Get started popup
        @if(Auth::guard('customer')->check() and Auth::guard('customer')->user()->type == 2 and Auth::guard('customer')->user()->company->getstarted == 0 and empty(session('close_started')))
            $.ajax({
                url: "{{url('/getstarted/started')}}",
                type: "GET",
                success: function (data) {
                    $("#get-started-popup-body").html(data);
                    $("#get-started-popup").modal();
                }
            });

            $('.dismiss').click(function () {
                let type = $(this).data('id');
                $.ajax({
                    url: "{{url('/getstarted/dismiss')}}/" + type,
                    type: "GET",
                    success: function (data) {
                        if (type == "permanent") {    
                            if (data === 1) {
                                new PNotify({
                                    title: "",
                                    text: "{{translate('alert.get_started_dismissed')}}",
                                    addclass: 'stack_top_right',
                                    type: 'warning',
                                    delay: 2000
                                });
                            }
                            if (data === 0) {
                                new PNotify({
                                    title: "",
                                    text: "{{translate('alert.get_started_not_dismissed')}}",
                                    addclass: 'stack_top_right',
                                    type: 'warning',
                                    delay: 2000
                                });
                            }
                        }
                    }
                });
            });

            $('#backHome').click(function () {
                if ($("#get-started-popup-body").find('#dismiss-here')[0]) {
                    let type = $(this).data('id');
                    $.ajax({
                        url: "{{url('/getstarted/dismiss')}}/" + type,
                        type: "GET",
                        success: function (data) {
                            if (type == "permanent") {    
                                if (data === 1) {
                                    new PNotify({
                                        title: "",
                                        text: "{{translate('alert.get_started_dismissed')}}",
                                        addclass: 'stack_top_right',
                                        type: 'warning',
                                        delay: 2000
                                    });
                                }
                                if (data === 0) {
                                    new PNotify({
                                        title: "",
                                        text: "{{translate('alert.get_started_not_dismissed')}}",
                                        addclass: 'stack_top_right',
                                        type: 'warning',
                                        delay: 2000
                                    });
                                }
                            }
                        }
                    });
                    $("#get-started-popup").modal('toggle');
                    var s = document.createElement("script");
                    s.type = "text/javascript";
                    s.async = true;
                    s.language = "javascript";
                    s.src = "{{Config::get('settings.whatfix_url')}}";
                    // Use any selector
                    $("head").append(s);
                } else {
                    $.ajax({
                        url: "{{url('/getstarted/started')}}",
                        type: "GET",
                        success: function (data) {
                            $("#get-started-popup-body").html(data);
                            $("#get-started-popup").modal();
                        }
                    });
                }
            });
        @endif

        $("#user_photo").change(function () {
            preview(this, 'user_preview', 'uploader1');
        });

        $("#company_photo").change(function () {
            preview(this, 'company_preview', 'uploader2');
        });

        $("#special_user_photo").change(function () {
            preview(this, 'company_preview', 'uploader2');
        });

        //Datatables function on all list pages
        if ($.fn.dataTable.isDataTable('#datatable2')) {
            var table = $('#datatable2').DataTable(dtLangs); //dtLangs declared in assets.blade page
        } else {
            var table = $('#datatable2').DataTable({
                "iDisplayLength": 10,
                "language": {
                    "lengthMenu": "{{translate('pagination.display')}} _MENU_ {{translate('pagination.record_per_page')}}",
                    "zeroRecords": "{{translate('pagination.nothing_found')}}",
                    "info": "{{translate('pagination.showing_page')}} _PAGE_ {{translate('pagination.of')}} _PAGES_",
                    "infoEmpty": "{{translate('pagination.no_records')}}",
                    "infoFiltered": "({{translate('pagination.filterd_from')}} _MAX_ {{translate('pagination.total_records')}})",
                    "oPaginate" : {
                        "sFirst": "{{translate('pagination.first')}}",
                        "sLast": "{{translate('pagination.last')}}",
                        "sNext": "{{translate('pagination.next')}}",
                        "sPrevious": "{{translate('pagination.previous')}}"
                    },
                    "sSearch" : "{{translate('pagination.search')}}"
                }
            });
        }

        if ($.fn.dataTable.isDataTable('#datatable-dashboard')) {
            var table = $('#datatable-dashboard').DataTable({"order": []});
        } else {
            var table = $('#datatable-dashboard').DataTable({
                "iDisplayLength": 10,
                aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [ -1, -2, -8 ]
                }],
                "order": [],
                "language": {
                    "lengthMenu": "{{translate('pagination.display')}} _MENU_ {{translate('pagination.record_per_page')}}",
                    "zeroRecords": "{{translate('pagination.nothing_found')}}",
                    "info": "{{translate('pagination.showing_page')}} _PAGE_ {{translate('pagination.of')}} _PAGES_",
                    "infoEmpty": "{{translate('pagination.no_records')}}",
                    "infoFiltered": "({{translate('pagination.filterd_from')}} _MAX_ {{translate('pagination.total_records')}})",
                    "oPaginate" : {
                        "sFirst": "{{translate('pagination.first')}}",
                        "sLast": "{{translate('pagination.last')}}",
                        "sNext": "{{translate('pagination.next')}}",
                        "sPrevious": "{{translate('pagination.previous')}}"
                    },
                    "sSearch" : "{{translate('pagination.search')}}"
                }
            });
        }

        //For every alert message after operation complete
        if ($(".alert").length != 0) {
            setTimeout(function () {
                $(".alert").fadeOut('slow');
            }, 4000);
        }

        //Site language changing on header.blade.php
        $(".chgLang").click(function () {
            var el = $(this);
            var innerEl = $(this).html();
            $(this).html(innerEl + ' <i class="fa fa-refresh fa-spin fa-1x"></i>');
            var langName = $(this).data("id");
            var term = $(this).text();
            $.ajax({
                url: "{{url('/lang')}}/" + langName,
                type: "GET",
                success: function (data) {
                    el.find('i').remove();
                    new PNotify({
                        title: "{{translate('alert.language')}}: " + term,
                        text: "{{translate('alert.updated_successfully')}}",
                        addclass: 'stack_top_right',
                        type: 'success',
                        width: '290px',
                        delay: 2000
                    });
                    $(document).delay(1000).queue(function () {
                        location.reload();
                    });
                }
            });
        });

        //Changing status on all list pages
        $(".statusChange").on("click", function () {
            var model = $(this).data("model");
            var id = $(this).data("id");
            var status = $(this).data("status");
            $.ajax({
                url: "{{url('/statuschange')}}/" + model + "/" + id + "/" + status,
                type: "GET",
                success: function (data) {
                    if (data == "success") {
                        new PNotify({
                            title: "{{translate('alert.status')}}",
                            text: "{{translate('alert.updated_successfully')}}",
                            addclass: 'stack_top_right',
                            type: 'success',
                            width: '290px',
                            delay: 2000
                        });
                    } else {
                        if (data == "faliure" && model == "Process") {
                            new PNotify({
                                title: "{{translate('alert.parent_process_inactive')}}",
                                text: '',
                                addclass: 'stack_top_right',
                                type: 'error',
                                width: '290px',
                                delay: 2000
                            });
                        } else {
                            new PNotify({
                                title: data,
                                text: '',
                                addclass: 'stack_top_right',
                                type: 'error',
                                width: '290px',
                                delay: 2000
                            });
                        }
                    }

                    $(document).delay(1000).queue(function () {
                        location.reload();
                    });
                }
            });
        });

        $(document).on('click', '.auditmonth', function () {
            var month = $(this).data('id');
        });

        $('.date').mask('99/99/9999');

        $("#monthpicker1").monthpicker({
            changeYear: false,
            stepYears: 1,
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            beforeShow: function (input, inst) {
                var newclass = 'admin-form';
                var themeClass = $(this).parents('.admin-form').attr('class');
                var smartpikr = inst.dpDiv.parent();
                if (!smartpikr.hasClass(themeClass)) {
                    inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
                }
            }
        }).attr('readonly', 'readonly');

        $('#test').click(function () {
            $('#myDateDiv').datepicker('setDate', ListDate[0]);
        });

        //Select month for the calender picker
        var selectedMonth = $('#selectedMonth').val();
        var selectedYear = $('#selectedYear').val();
        var selectRange = select_preview_month(selectedYear, selectedMonth, 1);
        var auditDateOpt = $.extend({},
        $.datepicker.regional["{{session('lang')}}"], { 
            dateFormat: "dd-mm-yy",
            minDate: new Date(selectRange.startDate)
        });
        $("#datetimepicker1").datepicker(auditDateOpt);
        $("#datetimepicker2").datepicker(dtOption);
        $("#datetimepicker3").datepicker(dtOption);
        $(".datetimepicker4").datepicker(dtOption);
        $(".deadlinedate").datepicker(dtOption);

        $(function () {
            $('#timepicker1').timepicker({minTime: '08:00:00'});
        });

        var demo1 = $('.internal_auditor').bootstrapDualListbox({
            nonSelectedListLabel: "{{translate('words.audit_team_members')}}",
            selectedListLabel: "{{translate('words.alocated_auditors')}}",
            preserveSelectionOnMove: 'moved',
            moveOnSelect: true,
        });

        var demo2 = $('.auditees').bootstrapDualListbox({
            nonSelectedListLabel: "{{translate('words.audit_team_members')}}",
            selectedListLabel: "{{translate('words.auditees')}}",
            preserveSelectionOnMove: 'moved',
            moveOnSelect: true,
        });

        demo1.on('change', function () {
            demo1.find(":selected").each(function (i, v) {
                demo2.find(":selected").each(function (ind, sel) {
                    if (sel.value == v.value) {
                        demo2.find('option[value=' + sel.value + ']').prop("selected", false);
                        demo2.bootstrapDualListbox('refresh', true);
                    }
                });
            });
        });

        demo2.on('change', function () {
            demo2.find(":selected").each(function (i, v) {
                demo1.find(":selected").each(function (ind, sel) {
                    if (sel.value == v.value) {
                        demo1.find('option[value=' + sel.value + ']').prop("selected", false);
                        demo1.bootstrapDualListbox('refresh', true);
                    }
                });
            });
        });

        var demo3 = $('.delegate').bootstrapDualListbox({
            nonSelectedListLabel: "{{translate('words.team_members')}}",
            selectedListLabel: "{{translate('words.team_members')}}",
            preserveSelectionOnMove: 'moved',
            moveOnSelect: true,
        });

        var demo4 = $('.user_access').bootstrapDualListbox({
            nonSelectedListLabel: "{{translate('words.choose_members_for_source_action_view')}}",
            selectedListLabel: "{{translate('words.members_can_see_source_action')}}",
            preserveSelectionOnMove: 'moved',
            moveOnSelect: true,
        });

        $(".select2-single").select2();

        $('.riskOpportunityCls').click(function () {
            sessionStorage.setItem("riskOrOpporId", $(this).data('riskoptid'));
            sessionStorage.setItem("riskOrOptType", $(this).data('type'));
            var processId = $(this).data("processid");
            $(location).attr('href', '{{url("/process/turtle")}}/' + processId);
        });
        
        $(".select-branch").select2({
            placeholder: "{{translate('words.select_branch')}}"
        });

        $(".select-frequency").select2({
            placeholder: "{{translate('words.select_frequency')}}"
        });

        $(".select-kpi-unit").select2({
            placeholder: "{{translate('form.kpi_unit')}}",
            width: '100%'
        });

        $('#save-btn').click(function() {
            if ($('#branch').val() == '') {
                $('.branch').find('.select2-selection').addClass('error');  
            } else {
                $('.branch').find('.select2-selection').removeClass('error');
            }
        });

        $('#video-category').DataTable({
            "order": []
        });
    });

    (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/APP_ID';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()

    window.intercomSettings = {
        app_id: "{{Config::get('settings.intercom_app_id')}}",
        @auth('customer')
            name: "{{Auth::guard('customer')->user()->name}}",
            user_id: "{{Auth::guard('customer')->user()->id}}",
            email: "{{Auth::guard('customer')->user()->email}}",
            created_at: "{{strtotime(Auth::guard('customer')->user()->created_at)}}",
            company: {
                    id: "{{Auth::guard('customer')->user()->company->id}}",
                    name: "{!! Auth::guard('customer')->user()->company->name !!}",
                    created_at: "{{strtotime(Auth::guard('customer')->user()->created_at)}}",
                    plan: "{{(Auth::guard('customer')->user()->company->is_trial == 1) ? 'Trial' : 'Subscribed'}}",
                    users_count: "{{Auth::guard('customer')->user()->company->users()->count()}}",
                    processes_count: "{{Auth::guard('customer')->user()->company->processes()->count()}}",
                    branches_count:"{{Auth::guard('customer')->user()->company->branches()->count()}}",
                    departments_count:"{{Auth::guard('customer')->user()->company->departments()->count()}}",
                    number_of_functions:"{{Auth::guard('customer')->user()->company->positions()->count()}}",
                    context_analys_count:"{{Auth::guard('customer')->user()->company->contexts()->count()}}",
                    stakeholder_analysis_count:"{{Auth::guard('customer')->user()->company->stakeholders()->count()}}",
                    report_finding:"{{Auth::guard('customer')->user()->company->reportfindings()->count()}}"
                  },
            user_hash: "{{generateIntercomHMAC()}}",
            admin: "{{(Auth::guard('customer')->user()->type == 2) ? True : False}}",
            pre_planned_audit: "{{Auth::guard('customer')->user()->audit_preperation}}",
            user_language: "{{strtoupper(Auth::guard('customer')->user()->language)}}",
            user_role:"{{Auth::guard('customer')->user()->userroles()}}"
        @endauth
    };
</script>
@auth('customer')
    @if ((Auth::user()->type == 2 and (Auth::user()->company->getstarted == 1 or !empty(session('close_started')))) or Auth::user()->type == 3)
        <script language='javascript' async='true' type='text/javascript' src="{{Config::get('settings.whatfix_url')}}"></script>
    @endif
@endauth