<script type="text/javascript">
    $(document).ready(function() {
        $('#tabline li.proactive').prevAll().addClass('proactive');
        $('.select2').css('width', '100%');

        $(document).on('keyup', '#info', function() {
            $('.hid-info').val($(this).val());
        });

        $(document).on('click', '.action-submit', function(data) {
            var data = $(this).data('type');
            var issue = $('[name="'+data+'"]').val();
            var deadline = $('[name="'+data+'_deadline"]').val();
            var member = $('[name="delegate_member_'+data+'"]').val();
            if(member == "")
            {
                $('[name="delegate_member_'+data+'"]').next('span').css('border','1px solid red');
            }
            else
            {
                $('[name="delegate_member_'+data+'"]').next('span').css('border','none');
            }
        });

        $('#tabline li').click(function(event) {
            var link=$(this).find('a').attr('href');
            if(link!='#')
            {
                $(this).addClass('proactive');
            }
            else
            {
                $(this).removeClass('proactive');
            }
            $(this).nextAll().removeClass('proactive');
        });
    });

</script>