<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '.functions', function() {
            var id = $(this).data("id");
            var observation = $('#observation-' + id).val();
            var iso = $('#iso-' + id).val();
            var reference = $('#reference-sl-' + id).val();
            var processsl = $('#process-sl-' + id).val();
            var findingtype = $('#finding-type-' + id).val();
            var evidence = $('#evidence-' + id).val();
            var savedId = 0;
            if ((observation != "" && observation.match(/^\s+/) == null) && iso != "" && reference != "" && findingtype != "" && processsl != "") {
                 $.ajax({
                    url: "{{url('/audit/execute/rowsave')}}",
                    type: "POST",
                    data: {
                        "audit_id" : $('#audit_id').val(),
                        "process" : processsl,
                        "observation" : observation,
                        "iso" : iso,
                        "references" : reference,
                        "findingtype" : findingtype,
                        "evidence" : evidence,
                        "_token" : "{{csrf_token()}}"
                    },
                    success: function(res) {
                        savedId = res;
                        new PNotify({
                            title: "{{translate('words.success_message')}}",
                            text: "",
                            addclass: 'stack_top_right',
                            type: "success",
                            width: '290px',
                            delay: 2000
                        });
                    }
                });
                setTimeout(() => {
                    $('#observation-' + id).css("border-color", "").data('id', id).data('hid', savedId);
                    $('#iso-' + id).css("border-color", "").data('id', id).data('hid', savedId);
                    $('#reference-sl-' + id).css("border-color", "").data('id', id).data('hid', savedId);
                    $('#process-sl-' + id).css("border-color", "").data('id', id).data('hid', savedId);
                    $('#finding-type-' + id).css("border-color", "").data('id', id).data('hid', savedId);
                    var nextId = + id + 1;
                    var nextEl = '\
                    <div class="row repeter" id="output-' + nextId + '">\
                        <div class="col-sm-2">\
                            <textarea class="gui-input referenceCls" placeholder="{{translate('form.observation')}}" name="observation[]" id="observation-' + nextId + '" cols="50" rows="10"></textarea>\
                        </div>\
                        <div class="col-sm-9">\
                            <div class="col-sm-4 form-group">\
                                <label class="field select">\
                                    <select class="iso_ref" name="iso[]" id="iso-' + nextId + '" data-row="' + nextId + '">\
                                        @foreach($iso_refs as $l=>$iso_ref)\
                                        <option value="{{$l}}" @if($audit_detail['audit_type']['id']==$l) selected @endif>\
                                            {{$iso_ref}}\
                                        </option>\
                                        @endforeach\
                                    </select>\
                                    <i class="arrow"></i>\
                                </label>\
                            </div>\
                            <div class="col-sm-8 form-group">\
                                <label class="field select" id="reference-' + nextId + '">\
                                    <select name="references[]" id="reference-sl-' + nextId + '">\
                                        @if(!empty($references)) \
                                        @foreach($references as $id=>$name) \
                                        <option value="{{$id}}">{{$name}}</option>\
                                        @endforeach \
                                        @endif \
                                    </select>\
                                    <i class="arrow"></i>\
                                </label>\
                            </div>\
                            <div class="col-sm-3 form-group">\
                                <label class="field select" id="process-' + nextId + '">\
                                    <select name="process[]" id="process-sl-' + nextId + '">\
                                        @if(!empty($porcessData)) \
                                        @foreach($porcessData as $id=>$name) \
                                        <option value="{{$id}}" @if($selectedProcess==$id) selected @endif>{{$name}}</option>\
                                        @endforeach \
                                        @endif \
                                    </select>\
                                    <i class="arrow"></i>\
                                </label>\
                            </div>\
                            <div class="col-sm-4 form-group">\
                                <label class="field select">\
                                    <select name="finding_type[]" id="finding-type-' + nextId + '">\
                                        @if(!empty($findingType)) \
                                        @foreach($findingType as $id=>$name) \
                                        <option value="{{$id}}">{{$name}}</option>\
                                        @endforeach \
                                        @endif \
                                    </select>\
                                    <i class="arrow"></i>\
                                </label>\
                            </div>\
                            <div class="col-sm-3 form-group">\
                                <label class="field select">\
                                    <input class="gui-input" placeholder="{{translate('form.evidence')}}" name="evidence[]" value="" id="evidence-' + nextId + '" type="text">\
                                </label>\
                            </div>\
                            <div class="col-sm-2 form-group">\
                                <button type="button" class="btn p5 ml5 executionDoc" data-id="" data-new="' + nextId + '">\
                                        <i class="fa fa-upload"></i>\
                                    </button>\
                            </div>\
                        </div>\
                        <div class="col-sm-1">\
                            <div class="col-sm-4 col-xs-2 btn-span form-group pt30">\
                                <button type="button" class="btn btn-system p10 functions" data-id="' + nextId + '">\
                                    <i class="fa fa-plus"></i>\
                                </button>\
                                <input name="id[]" value="" type="hidden">\
                            </div>\
                        </div>\
                    </div>';
                    $("#output-" + id).before(nextEl);
                    $(".functions[data-id=" + id + "]").removeClass('btn-system').removeClass('functions').addClass('btn-danger').addClass('functions-del').removeClass('p10').addClass('p12').html('<i class="fa fa-minus"></i>');

                }, 1000);
            } else {
                if (observation == '' || observation.match(/^\s+/) != null) {
                    $('#observation-' + id).css("border-color", "#f00").focus();
                    return false;
                } else {
                    $('#observation-' + id).css("border-color", "");
                }

                if (iso == '') {
                    $('#iso-' + id).css("border-color", "#f00").focus();
                    return false;
                } else {
                    $('#iso-' + id).css("border-color", "");
                }

                if (reference == '') {
                    $('#reference-sl-' + id).css("border-color", "#f00").focus();
                    return false;
                } else {
                    $('#reference-sl-' + id).css("border-color", "");
                }

                if (processsl == '') {
                    $('#process-sl-' + id).css("border-color", "#f00").focus();
                    return false;
                } else {
                    $('#process-sl-' + id).css("border-color", "");
                }

                if (findingtype == '') {
                    $('#finding-type-' + id).css("border-color", "#f00").focus();
                    return false;
                } else {
                    $('#finding-type-' + id).css("border-color", "");
                }
            }
        });

        $('.updateData').blur(function() {
            var id = $(this).data("id");
            var hid = $(this).data("hid");
            var observation = $('#observation-' + id).val();
            var iso = $('#iso-' + id).val();
            var reference = $('#reference-sl-' + id).val();
            var processsl = $('#process-sl-' + id).val();
            var findingtype = $('#finding-type-' + id).val();
            var evidence = $('#evidence-' + id).val();
            if ((observation != "" && observation.match(/^\s+/) == null) && iso != "" && reference != "" && findingtype != "" && processsl != "" && hid != '') {
                $.ajax({
                    url: "{{url('/audit/execute/rowsave')}}",
                    type: "POST",
                    data: {
                        "audit_id" : $('#audit_id').val(),
                        "process" : processsl,
                        "observation" : observation,
                        "iso" : iso,
                        "references" : reference,
                        "findingtype" : findingtype,
                        "evidence" : evidence,
                        "id" : hid,
                        "_token" : "{{csrf_token()}}"
                    },
                    success: function(data) {  
                        new PNotify({
                            title: "{{translate('words.success_message')}}",
                            text: "",
                            addclass: 'stack_top_right',
                            type: "success",
                            width: '290px',
                            delay: 2000
                        });
                    }
                });
            }
        });

        $(document).on('click', '.functions-del', function() {
            var confirmed = confirm("{{translate('alert.are_you_sure')}}");
            if (confirmed == true) {
                var id = $(this).data('id');
                $('#output-' + id).remove();
            }
        });

        $(document).on('change', '.iso_ref', function() {
            var ref_id = $(this).val();
            var row = $(this).data('row');
            $.ajax({
                url: "{{url('/audit/exec-audit-type')}}/" + ref_id + '/references[]',
                async: true,
                cache: false,
                type: "GET",
                success: function(data) {
                    $('#reference-' + row).html(data);
                    $('#reference-' + row).find('select').attr('id', 'reference-sl-' + row);
                }
            });
        });

        $(document).on('click', '.executionDoc', function(e) {
            var el = $(this);
            var executionId = $(this).data('id');
            var newrow = $(this).data('new');
            $('#docpanel').html('<i class="fa fa-cog fa-spin fa-4x text-system"></i>');
            if (executionId != "") {
                if ($(this).data('existingrow')) {
                    var existingrow = el.data('existingrow');
                    var evidence = $('#evidence-' + existingrow).val();
                    if (evidence == '' || evidence.match(/^\s+/) != null) {
                        $('#evidence-' + existingrow).css("border-color", "#f00").focus();
                        return false;
                    } else {
                        $("#myModal").modal();
                        $.ajax({
                            url: "{{url('/audit/execute/document')}}/" + executionId + "/0/" + evidence,
                            type: "GET",
                            success: function(data) {
                                $('#docpanel').html(data);
                            }
                        });
                    }
                } else {
                    $("#myModal").modal();
                    $.ajax({
                        url: "{{url('/audit/execute/document')}}/" + executionId + "/0",
                        type: "GET",
                        success: function(data) {
                            $('#docpanel').html(data);
                        }
                    });
                }
            }

            if (newrow != "") {
                var observation = $('#observation-' + newrow).val();
                var iso = $('#iso-' + newrow).val();
                var reference = $('#reference-sl-' + newrow).val();
                var findingtype = $('#finding-type-' + newrow).val();
                var evidence = $('#evidence-' + newrow).val();
                if (observation == '' || observation.match(/^\s+/) != null) {
                    $('#observation-' + newrow).css("border-color", "#f00").focus();
                    return false;
                } else {
                    $('#observation-' + newrow).css("border-color", "");
                }

                if (iso == '') {
                    $('#iso-' + newrow).css("border-color", "#f00").focus();
                    return false;
                } else {
                    $('#iso-' + newrow).css("border-color", "");
                }

                if (reference == '') {
                    $('#reference-sl-' + newrow).css("border-color", "#f00").focus();
                    return false;
                } else {
                    $('#reference-sl-' + newrow).css("border-color", "");
                }

                if (findingtype == '') {
                    $('#finding-type-' + newrow).css("border-color", "#f00").focus();
                    return false;
                } else {
                    $('#finding-type-' + newrow).css("border-color", "");
                }

                if (evidence == '') {
                    $('#evidence-' + newrow).css("border-color", "#f00").focus();
                    return false;
                } else {
                    $('#evidence-' + newrow).css("border-color", "");
                }

                if (observation != '' && iso != '' && reference != '' && findingtype != '' && evidence != '') {
                    $('#finding-type-' + newrow).css("border-color", "");
                    $("#myModal").modal();
                    $.ajax({
                        url: "{{url('/audit/execute/document')}}/0/" + newrow,
                        type: "GET",
                        success: function(data) {
                            $('#docpanel').html(data);
                        }
                    });
                    return true;
                }
            }
        });

        $(document).on('click', '#docSave', function() {
            $('#root').after('<i class="fa fa-cog fa-spin text-system"></i>');
            //While updating existing execution row with document
            if ($('#hid').val() != "" && $('#fileData').val() != "") {
                $.ajax({
                    url: "{{url('/audit/execute/documentsave')}}",
                    type: "POST",
                    data: {
                        "file" : $('#fileData').val(),
                        "filename" : $('#fileName').val(),
                        "fileextension" : $('#fileExt').val(),
                        "executionid" : $('#hid').val(),
                        "evidence" : $('#evidence').val(),
                        "_token" : "{{csrf_token()}}"
                    },
                    success: function(data) {
                        $('#root').next('i').remove();
                        if (data == "Failed") {
                            var title = "{{translate('alert.document_not_uploaded')}}";
                            var status = "error";
                        } else {
                            var title = "{{translate('alert.document_uploaded')}}";
                            var status = "success";
                        }

                        new PNotify({
                            title: title,
                            text: "",
                            addclass: 'stack_top_right',
                            type: status,
                            width: '290px',
                            delay: 2000
                        });
                        $.ajax({
                            url: "{{url('/audit/execute/document')}}/" + data + "/0",
                            type: "GET",
                            success: function(data) {
                                $('#docpanel').html(data);
                            }
                        });
                    }
                });
            }
            //While adding new execution details row with document
            if ($('#new').val() != "" && $('#fileData').val() != "") {
                var row = $('#new').val();
                var processs =$('#process-sl-' + row).val();
                var observation = $('#observation-' + row).val();
                var iso = $('#iso-' + row).val();
                var reference = $('#reference-sl-' + row).val();
                var findingtype = $('#finding-type-' + row).val();
                var evidence = $('#evidence-' + row).val();
                $.ajax({
                    url: "{{url('/audit/execute/documentsave')}}",
                    type: "POST",
                    data: {
                        "file" : $('#fileData').val(),
                        "filename" : $('#fileName').val(),
                        "fileextension" : $('#fileExt').val(),
                        "newdata" : row,
                        "audit_id" : $('#audit_id').val(),
                        "process" : processs,
                        "observation" : observation,
                        "iso" : iso,
                        "references" : reference,
                        "findingtype" : findingtype,
                        "evidence" : evidence,
                        "_token" : "{{csrf_token()}}"
                    },
                    success: function(data) {
                        $('#root').next('i').remove();
                        $('#new').val("");
                        $('#hid').val(data);
                        $('#executionDoc-' + row).data('new', '');
                        $('#executionDoc-' + row).data('id', data);
                        $('#executionDoc-' + row).after(' <span class="fa fa-file-o fa-2x icon-span"></span>');
                        if (data == "Failed") {
                            var title = "{{translate('alert.document_not_uploaded')}}";
                            var status = "error";
                        } else {
                            var title = "{{translate('alert.document_uploaded')}}";
                            var status = "success";
                        }

                        new PNotify({
                            title: title,
                            text: "",
                            addclass: 'stack_top_right',
                            type: status,
                            width: '290px',
                            delay: 2000
                        });
                        $.ajax({
                            url: "{{url('/audit/execute/document')}}/" + data + "/0",
                            type: "GET",
                            success: function(data) {
                                $('#docpanel').html(data);
                            }
                        });
                    }
                });
            }

            if ($('#fileData').val() == "") {
                new PNotify({
                    title: "{{translate('alert.document_not_uploaded')}}",
                    text: "",
                    addclass: 'stack_top_right',
                    type: "error",
                    width: '290px',
                    delay: 2000
                });
                $('#root').next('i').remove();
            }
        });

        $("#myModal").on('hide.bs.modal', function () {
            location.reload();
        });

        $(document).on('click', '.executionDocView', function() {
            var executionId = $(this).data('id');
            if (executionId != "") {
                $.ajax({
                    url: "{{url('/audit/execute/documents')}}/" + executionId,
                    type: "GET",
                    success: function(data) {
                        $('#docpanel').html(data);
                    }
                });
            }
        });

        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });

        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }

        @if($audit_detail->status == 2)
            $(document).ready(function() {
                $(document).find('.filter').attr('disabled', true);
                $(document).find('.moveall').attr('disabled', true);
                $(document).find('.removeall').attr('disabled', true);
                $(document).find('input[type="text"]').attr('disabled', true);
                $(document).find('textarea').attr('disabled', true);
                $(document).find('select').attr('disabled', true);
            });
        @endif

        $(document).on('click', '#exec-preview', function() {
            $.ajax({
                url : "{{url('/audit/audit-execute/preview')}}/" + $(this).data('audit'),
                type : "GET",
                success : function(data) {
                    $('#exec-preview-body').html(data);
                    $('#execution-preview').modal();
                }
            });
        });

        var releaseDateOption = $.extend({},
        $.datepicker.regional["{{session('lang')}}"], { 
            dateFormat: "dd-mm-yy"
        });
        $("#release_date").datepicker(releaseDateOption);
        $("#release_date").keypress(function(event) {event.preventDefault();});
    });
</script>
