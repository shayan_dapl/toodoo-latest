@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
	Vue.component('ratings', {
		props : ['list'],
		template : `
		<select class="form-control rateOptions" ref="pushList" @change="addRate">
			<option v-for="each in list" :value="each.id" :data-rate="each.rating" :data-name="each.description">@{{each.rating}}</option>
		</select>
		`,
		methods: {
			addRate : function () {
				vm.parameters = [];
                vm.rates = [];
				vm.ratenames = [];
                vm.rateratings = [];
				$('.rateOptions').each(function (i, v) {
                    if (v.value == "") {
                        $(this).addClass("error");
                    } else {
                        $(this).removeClass("error");
                    }
					vm.parameters.push(v.dataset.parameter);
                    vm.rates.push(v.value);
                    vm.ratenames.push(v.selectedOptions[0].dataset.name);
					vm.rateratings.push(v.selectedOptions[0].dataset.rate);
					$(this).parent('div').siblings('div .rating-name').html("");
					$(this).parent('div').siblings('div .rating-name').html(v.selectedOptions[0].dataset.name);
				});
			}
		}
	});

	var vm = new Vue({
        el : '#app',
        data : {
            ratings : {!! $ratings !!}, 
            parameters : [],
            rates : [],
            ratenames : [],
            rateratings : [],
            supplier : '',
            supplierName : '',
            list : [],
            is_action : false,
            action : '',
            actionClass : 'form-control',
            delegate : '',
            delegateClass : 'form-control',
            deadline : '',
            deadlineClass : 'form-control DatePicker4 pointer',
            comment : '',
            threshold : "{{$thresholdLimit}}",
            thresholdParams : [],
            thresholdParamIds : [],
            is_sarc : true,
            sarcdelegate : '',
            sarcdelegateClass : 'form-control',
            sarcdeadline : '',
            sarcdeadlineClass : 'form-control DatePicker5 pointer',
            enabled : false
        },
        mounted() {
            var comptDtOptions = $.extend({},
            $.datepicker.regional["{{session('lang')}}"], { 
                dateFormat: "dd-mm-yy",
                onSelect: function(dateText) {
                    vm.deadline = dateText;
                }
            });
            $(".DatePicker4").datepicker(comptDtOptions);

            var sarccomptDtOptions = $.extend({},
            $.datepicker.regional["{{session('lang')}}"], { 
                dateFormat: "dd-mm-yy",
                onSelect: function(dateText) {
                    vm.sarcdeadline = dateText;
                }
            });
            $(".DatePicker5").datepicker(sarccomptDtOptions);
        },
        watch : {
            action : function (val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.actionClass = 'form-control';
                }
            },
            delegate : function (val) {
                if (val != "") {
                    this.delegateClass = 'form-control';
                }
            },
            deadline : function (val) {
                if (val != "") {
                    this.deadlineClass = 'form-control pointer';
                }
            },
            is_action : function (val) {
                if (val === false) {
                    this.action = this.delegate = this.deadline = '';
                }
            },
            is_sarc : function (val) {
                if (val === false) {
                    this.sarcaction = this.sarcdelegate = this.sarcdeadline = '';
                }
            },
            sarcdelegate : function (val) {
                if (val != "") {
                    this.sarcdelegateClass = 'form-control';
                }
            },
            sarcdeadline : function (val) {
                if (val != "") {
                    this.sarcdeadlineClass = 'form-control pointer';
                }
            }
        },
        methods : {
        	openModal : function (id, suppName) {
                this.supplierName = suppName;
                this.is_action = false;
                this.action = '';
                this.actionClass = 'form-control';
                this.delegate = '';
                this.delegateClass = 'form-control';
                this.deadline = '';
                this.deadlineClass = 'form-control';
                this.comment = '';
                this.enabled = false;
                this.thresholdParams = [];
                this.thresholdParamIds = [];
                this.is_sarc = true;

                vm.supplier = id;
        		vm.parameters = [];
				vm.rates = [];
                vm.ratenames = [];
                vm.rateratings = [];
        		$('.rateOptions').each(function (i, v) {
                    $(this).removeClass("error");
                    $('.rateOptions').val(v.options[0].value);
					vm.parameters.push(v.dataset.parameter);
					vm.rates.push(v.value);
                    vm.ratenames.push(v.selectedOptions[0].dataset.name);
                    vm.rateratings.push(v.selectedOptions[0].dataset.rate);
					$(this).parent('div').siblings('div .rating-name').html(v.selectedOptions[0].dataset.name);
				});
                this.fetchData(id);
                $('#supplierModal').modal();
        	},
            fetchData : function (supplier) {
                axios.get("{{url('/audit/supplier-evaluation/list')}}/" + supplier)
                .then(response => {
                    vm.list = response.data;
                })
                .then(() => {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            },
            saveRating : function () {
                let errors = 0;
                vm.thresholdParams = [];
                vm.thresholdParamIds = [];
                $('.rateOptions').each(function (i, v) {
                    if (v.value == "") {
                        $(this).addClass("error");
                        $('.supplier-parameters-height').animate({
                            scrollTop: $(this).offset().top
                        }, 1000);
                        errors = 1;
                    } else {
                        if (v.selectedOptions[0].dataset.rate < vm.threshold) {
                            vm.thresholdParams.push(v.dataset.name);
                            vm.thresholdParamIds.push(v.dataset.parameter);
                        }
                    }
                });
                if (vm.thresholdParams.length == 0) {
                    this.is_sarc = false;
                    this.sarcdelegate = this.sarcdeadline = '';
                }
                if (this.is_action === true) {
                    if (this.action == "" || this.action.match(/^\s+/) != null) {
                        this.actionClass = "form-control error";
                        errors = 1;
                    }
                    if (this.delegate == "") {
                        this.delegateClass = "form-control error";
                        errors = 1;
                    }
                    if (this.deadline == "") {
                        this.deadlineClass = "form-control pointer error";
                        errors = 1;
                    }
                }
                if (this.is_sarc === true) {
                    if (this.sarcdeadline == "") {
                        this.sarcdeadlineClass = "form-control pointer error";
                        errors = 1;
                    }
                    if (this.sarcdelegate == "") {
                        this.sarcdelegateClass = "form-control error";
                        errors = 1;
                    }
                }
                if (errors == 0) {
                    this.enabled = true;
                    axios.post("{{url('/audit/supplier-rating-save')}}", {
                        'supplier_id' : this.supplier,
                        'parameters' : this.parameters,
                        'rates' : this.rates,
                        'comment' : this.comment,
                        'isAction' : (this.is_action === true) ? 1 : 0,
                        'action' : this.action,
                        'delegateTo' : this.delegate,
                        'deadline' : this.deadline,
                        'threshold' : (this.thresholdParamIds.length > 0) ? this.thresholdParamIds.join(',') : '',
                        'sarcdeadline' : this.sarcdeadline,
                        'sarcdelegate' : this.sarcdelegate,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        this.enabled = false;
                        if (response.data === true) {
                            vm.parameters.map((v, i) => {
                                vm.$refs[vm.supplier + '-' + v].innerText = vm.rateratings[i];
                                vm.$refs[vm.supplier + '-' + v].className = "bg-success";
                                setTimeout(function () {
                                    vm.$refs[vm.supplier + '-' + v].className = "";
                                }, 2000);
                            });
                            vm.$refs[vm.supplier + '-average'].innerText = "{{translate('words.reload_to_see')}}";
                            vm.$refs[vm.supplier + '-percent'].innerText = "{{translate('words.reload_to_see')}}";
                            $('#supplierModal').modal('toggle');
                        }
                    })
                    .catch(console.log);
                }
            },
            actionStatement : function (action) {
                if (action == null) {
                    return null;
                } else {
                    var status = (action.closed == 0) ? "{{translate('words.open')}}" : "{{translate('words.closed')}}";
                    var deadline = action.deadline.split('-').reverse().join('-');
                    return "<b>{{translate('form.action')}} : </b>" + action.action + "<br/><b>{{translate('form.deadline')}} : </b>" + deadline + "<br/><b>{{translate('words.delegated_to')}} : </b>" + action.user + "<br/><b>{{translate('words.status')}} : </b>" + status;
                }
            },
            actionSCARStatement : function (action) {
                if (action == null) {
                    return null;
                } else {
                    var status = (action.closed == 0) ? "{{translate('words.open')}}" : "{{translate('words.closed')}}";
                    return "<b>{{translate('form.deadline')}} : </b>" + action.deadline + "<br/><b>{{translate('words.delegated_to')}} : </b>" + action.deligated_user + "<br/><b>{{translate('words.status')}} : </b>" + status;
                }
            }
        }
    });
</script>