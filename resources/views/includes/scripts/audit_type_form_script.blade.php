<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('change', '#iso_reference', function () {
            var ref_id = $(this).val();
            $.ajax({
                url: "{{url('/audit/get-audit-type')}}/" + ref_id + '/parent_audit_type',
                async: true,
                cache: false,
                type: "GET",
                success: function (data) {
                    $('#parent_type_import').html(data);
                }
            });
        });

        $(document).on('click', '#audit-type-save-btn', function () {
            var languages = {'Nederlands': 'nl', 'English': 'en', 'Francais': 'fr'};
            $.each(languages, function (index, data) {
                var name = $('#name_' + data).attr('required', false);
                var description = $('#description_' + data).attr('required', false);
            });
            $('form').submit();
        });

        $(document).on('click', '#audit-type-btn', function () {
            var languages = {'Nederlands': 'nl', 'English': 'en', 'Francais': 'fr'};
            $.each(languages, function (index, data) {
                $('#name_' + data).attr('required', true);
                $('#description_' + data).attr('required', true);

                var name = $('#name_' + data).val();
                var description = $('#description_' + data).val();

                if ($('#clause').val() == "" || name == "" || description == "")
                {
                    $('.tab-pane').removeClass('active');
                    $('#' + index).addClass('active');
                    $('.lang-links').removeClass('active');
                    $('.' + index).addClass('active');
                }
            });
        });
    });
</script>