@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    new Vue({
        el: '#branch',
        data: {
            branches: [
                { name: "{{translate('table.no_data')}}" }
            ],
            pages: '',
            name: '',
            hid: '',
            nodata: 1,
            is_corporate: 0
        },
        created() {
            this.fetchData(0);
        },
        methods: {
            fetchData: function(val) {
                axios.get("{{url('/branch/data/')}}/"+val).then(response => {
                    const responseData = [];
                    response.data.data.forEach((branch, index) => {
                        responseData.push({id: branch.id, name : branch.name, isCorporate : branch.is_corporate});
                    });
                    if(response.data.total > 0) {
                        this.branches = responseData;
                        this.nodata = 0;
                    }
                    
                    const pageCount = Math.ceil(response.data.total / 5);
                    const pageNums = [];
                    for(var i = 0; i < pageCount; i++) {
                        var offset = i * 5;
                        if(i == 0) {
                            this.isActive = true;
                            pageNums.push({set : offset, number: (+i+1), class: 'paging btn btn-system text-default mr5'});
                        } else {
                            pageNums.push({set : offset, number: (+i+1), class: 'paging btn btn-default mr5'});
                        }
                    }
                    if(pageNums.length > 0) {
                        this.pages = pageNums;    
                    }
                });
            },
            paginate: function(offset) {
                $('.paging').removeClass('btn-system text-default').addClass('btn-default');
                this.fetchData(offset);
            },
            saveData: function() {
                if(this.name != "" && this.name.match(/^\s+/) == null) {
                    axios.post("{{ url('/branch/') }}", 
                    { 
                        'name' : this.name,
                        'is_corporate' : this.is_corporate,
                        'hid' : this.hid
                    })
                    .then(response => {
                        this.is_corporate = false;

                        if(response.data === false) {
                            new PNotify({
                                title: "{{translate('words.branch')}}",
                                text: "{{translate('alert.already_exist')}}",
                                addclass: 'stack_top_right',
                                type: "error",
                                width: '290px',
                                delay: 2000
                            })
                        }
                        if(response.data === true) {
                            new PNotify({
                                title: "{{translate('words.success_message')}}",
                                text: "",
                                addclass: 'stack_top_right',
                                type: "success",
                                width: '290px',
                                delay: 2000
                            })
                        }
                    })
                    .then(() => {
                        this.name = '';
                        this.hid = '';
                        return Promise.resolve();
                    })
                    .then(() => {
                        this.pages = '';
                        return Promise.resolve();
                    })
                    .then(() => {
                        return this.fetchData(0);
                    })
                    .catch(console.log);
                } else {
                    new PNotify({
                        title: "{{translate('words.branch')}}",
                        text: "{{translate('alert.not_allowed')}}",
                        addclass: 'stack_top_right',
                        type: "error",
                        width: '290px',
                        delay: 2000
                    })
                }
            },
            editData: function(branch) {
                this.name = branch.name;
                this.is_corporate = branch.isCorporate;
                this.hid = branch.id;
            },
            removeData: function(val) {
                var confirmed = confirm("{{translate('alert.are_you_sure')}}");
                if (confirmed == true) {
                    axios.post("{{ url('/branch/remove') }}",
                    {
                        'id' : val,
                        '_token' : "{{csrf_token()}}"
                    }
                    ).then(response => {
                        if(response.data === true) {
                            new PNotify({
                                title: "{{translate('words.success_message')}}",
                                text: "",
                                addclass: 'stack_top_right',
                                type: "success",
                                width: '290px',
                                delay: 2000
                            })
                        }

                        if(response.data === 'child') {
                            new PNotify({
                                title: "{{translate('alert.child_element_exists')}}",
                                text: "",
                                addclass: 'stack_top_right',
                                type: "error",
                                width: '290px',
                                delay: 2000
                            })
                        }

                        if(response.data === false) {
                            new PNotify({
                                title: "{{translate('words.branch')}}",
                                text: "{{translate('words.remove')}} {{translate('alert.not_allowed')}}",
                                addclass: 'stack_top_right',
                                type: "error",
                                width: '290px',
                                delay: 2000
                            })
                        }
                    })
                    .then(() => {
                        this.pages = ''
                        return Promise.resolve();
                    })
                    .then(() => {
                        this.fetchData(0);
                        return Promise.resolve();
                    }); 
                }
            },
            checkCorporate: function() {
                if (this.is_corporate) {
                    axios.get("{{url('/branch/check-corporate')}}").then(response => {
                        if(response.data === false) {
                            this.is_corporate = false;
                            new PNotify({
                                title: "{{translate('words.corporate_branch')}}",
                                text: "{{translate('alert.already_exist')}}",
                                addclass: 'stack_top_right',
                                type: "error",
                                width: '290px',
                                delay: 2000
                            })
                        }
                    })
                    .catch(console.log);
                }
            }
        }
    });
</script>