<script type="text/javascript">
    $(document).ready(function() {    
        var startTime;
        var gap = 0;
        window.startT = function () {
            startTime = new Date();
        }

        window.endT = function (el, dat) {
            gap = (new Date() - startTime);
            if (gap < 200) {
                $(el).trigger(refer(dat));
            }
        }

        window.refer = function (dat) {
            location.href = "{{ url('/process/turtle') }}/" + dat;
        };

        $(".process-sortable-icon").sortable({
            stop: function () {
                var listElements = $(this).children("li");
                var ids = listElements.map(function () {
                    return $(this).data("id");
                }).get().join();
                $.ajax({
                    url: "{{url('/process/processarrange/')}}",
                    type: "POST",
                    data: {"ids": ids, "_token": "{{csrf_token()}}"},
                    success: function (data) {
                        new PNotify({
                            title: "{{translate('alert.steps')}}",
                            text: "{{translate('alert.rearranged_successfully')}}",
                            addclass: 'stack_top_right',
                            type: 'success',
                            width: '290px',
                            delay: 2000
                        });
                    }
                })
            }
        });

        $(document).on('click', '.showchildren', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            $('#' + id).toggle('slow');
        });

        $(document).on('change', '#branch-opt', function() {
            $('#loaderDv').show();
            var branchId = $(this).val();
            if(branchId != "0"){
                $(location).attr('href', '{{url("/process/categorized")}}/'+branchId);
            } else{
                $(location).attr('href', '{{url("/process/categorized")}}');
            }
        });
    });
</script>