<script type="text/javascript">
    $(document).ready(function() {     
        $('#useraddonsection input:radio').click(function() {
            var price = $(this).attr('data-price');
            var maxValue = $(this).attr('data-max');
            var tenure = $('#subscriptionTenure').val();
            if (maxValue == 0) {
                if(tenure == 1){
                    $('#userCountPrice').html('€ '+price+' {{translate("words.per_month")}}');
                } else {
                    $('#userCountPrice').html('€ '+price+' {{translate("words.per_year")}}');
                }
                
            } else {
                $('#userCountPrice').html('<a href="{{Config::get('settings.devsite_url')}}/contact/" class="text-system btn" target="_blank">Contact us</a>');
            }    
        });

        setTimeout(function() { 
            $(":radio:checked").trigger("click");
        }, 500);
    });
</script>