@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    Vue.component('list-table', {
        props : ['list'],
        template : `
            <table class="table table-striped table-hover" id="branch-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th v-if="list.length > 0">
                            {{translate('table.corporate')}}
                        </th>
                        <th>{{translate('table.name')}}</th>
                        <th>{{translate('table.operations')}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(each, i) in list">
                        <td>@{{i+1}}.</td>
                        <td v-if="list.length > 0">
                            <div v-if="each.is_corporate == 1">
                                {{translate('form.yes')}}
                            </div>
                        </td>
                        <td>@{{each.name}}</td>
                        <td>
                            <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="editData(each)" title="{{translate('words.edit')}}">
                                <span class="text-success fa fa-pencil"></span>
                            </a>
                            <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="removeData(each.id)" title="{{translate('words.remove')}}" v-if="each.is_corporate!= 1">
                                <span class="text-danger fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        `,
        methods : {
            editData : function (each) {
                vm.name = each.name;
                vm.is_corporate = each.is_corporate;
                vm.hid = each.id;
            },
            removeData : function (id) {
                var confirmed = confirm("{{translate('alert.are_you_sure')}}");
                if (confirmed == true) {
                    axios.post("{{ url('/branch/remove') }}", {
                        'id' : id,
                        '_token' : "{{csrf_token()}}"
                    }).then(response => {
                        new PNotify({
                            title: (response.data === false) ? "{{translate('words.branch')}}" : ((response.data === 'child') ? "{{translate('alert.child_element_exists')}}" : "{{translate('words.success_message')}}"),
                            text: (response.data === false) ? "{{translate('words.remove')}} {{translate('alert.not_allowed')}}" : "",
                            addclass: 'stack_top_right',
                            type: (response.data === false || response.data === 'child') ? "error" : "success",
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        vm.fetchData();
                    })
                    .catch(console.log); 
                }
            }
        }
    });

    var vm = new Vue({
        el : '#branch',
        data : {
            branches : [],
            name : '',
            nameClass : 'form-control',
            hid : '',
            is_corporate : 0,
            enabled : false
        },
        created () {
            this.fetchData();
        },
        watch : {
            branches : function (newVal, oldVal) {
                if (newVal != oldVal && $.fn.dataTable.isDataTable('#branch-table')) {
                    $('#branch-table').DataTable().destroy();
                }
                setTimeout(() => {
                    $('#branch-table').DataTable({
                        "language": {
                            "lengthMenu": "{{translate('pagination.display')}} _MENU_ {{translate('pagination.record_per_page')}}",
                            "zeroRecords": "{{translate('pagination.nothing_found')}}",
                            "info": "{{translate('pagination.showing_page')}} _PAGE_ {{translate('pagination.of')}} _PAGES_",
                            "infoEmpty": "{{translate('pagination.no_records')}}",
                            "infoFiltered": "({{translate('pagination.filterd_from')}} _MAX_ {{translate('pagination.total_records')}})",
                            "oPaginate" : {
                                "sFirst": "{{translate('pagination.first')}}",
                                "sLast": "{{translate('pagination.last')}}",
                                "sNext": "{{translate('pagination.next')}}",
                                "sPrevious": "{{translate('pagination.previous')}}"
                            },
                            "sSearch" : "{{translate('pagination.search')}}"
                        }
                    });
                }, 1000);
            },
            name : function(val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.nameClass = 'form-control';
                }
            }
        },
        methods: {
            fetchData : function () {
                axios.get("{{url('/branch/data/')}}")
                .then(response => {
                    return new Promise((resolve, reject) => {
                        this.branches = response.data;
                        resolve();
                    })
                })
                .catch(console.log);
            },
            saveData : function () {
                var errors = 0;
                if(this.name == "" || this.name.match(/^\s+/) != null) {
                    this.nameClass = 'form-control error';
                    errors = 1;
                }
                if (errors == 0) {
                    this.enabled = true;
                    axios.post("{{ url('/branch/') }}", { 
                        'name' : this.name,
                        'is_corporate' : this.is_corporate,
                        'hid' : this.hid
                    })
                    .then(response => {
                        this.is_corporate = false;
                        this.enabled = false;
                        new PNotify({
                            title: (response.data === false) ? "{{translate('words.branch')}}" : "{{translate('words.success_message')}}",
                            text: (response.data === false) ? "{{translate('alert.already_exist')}}" : "",
                            addclass: 'stack_top_right',
                            type: (response.data === false) ? "error" : "success",
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        this.name = this.hid = '';
                        this.fetchData();
                    })
                    .catch(console.log);
                }
            },
            checkCorporate : function() {
                if (this.is_corporate) {
                    axios.get("{{url('/branch/check-corporate')}}")
                    .then(response => {
                        if(response.data === false) {
                            this.is_corporate = false;
                            new PNotify({
                                title: "{{translate('words.corporate_branch')}}",
                                text: "{{translate('alert.already_exist')}}",
                                addclass: 'stack_top_right',
                                type: "error",
                                width: '290px',
                                delay: 2000
                            })
                        }
                    })
                    .catch(console.log);
                }
            }
        }
    });
</script>