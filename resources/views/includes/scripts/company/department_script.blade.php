@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    Vue.component('list-table', {
        props : ['list'],
        template : `
            <table class="table table-striped table-hover" id="department-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th class="text-center">{{translate('table.top_department')}}</th>
                        <th>{{translate('table.branch')}}</th>
                        <th>{{translate('table.name')}}</th>
                        <th>{{translate('table.operations')}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(each, i) in list">
                        <td>@{{i+1}}</td>
                        <td align="center">
                            <span v-if="each.is_top == 1">{{translate('form.yes')}}</span>
                        </td>
                        <td>@{{each.branchNames}}</td>
                        <td>@{{each.name}}</td>
                        <td>
                            <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="editData(each.name, each.branchIds, each.id)" title="{{translate('words.edit')}}">
                                <span class="text-success fa fa-pencil"></span>
                            </a>
                            <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="removeData(each.id)" title="{{translate('words.remove')}}" v-if="each.is_top != 1">
                                <span class="text-danger fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        `,
        methods : {
            editData : function (name, branch, id) {
                vm.name = name;
                vm.hid = id;
                vm.branch = branch;
            },
            removeData : function (id) {
                var confirmed = confirm("{{translate('alert.are_you_sure')}}");
                if (confirmed == true) {
                    axios.post("{{ url('/department/remove') }}", {
                        'id' : id,
                        '_token' : "{{csrf_token()}}"
                    }).then(response => {
                        var title = (response.data === true) ? "{{translate('words.success_message')}}" : ((response.data === 'child') ? "{{translate('alert.child_posiiton_exists')}}" : "{{translate('words.department')}}");
                        var text = (response.data === true) ? "" : ((response.data === 'child') ? "{{translate('alert.child_posiiton_exists')}}" : "{{translate('words.remove')}} {{translate('alert.not_allowed')}}");
                        var type = (response.data === true) ? "success" : "error";
                        new PNotify({
                            title: title,
                            text: text,
                            addclass: 'stack_top_right',
                            type: type,
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        vm.fetchData();
                    })
                    .catch(console.log); 
                }
            }
        }
    });

    var vm = new Vue({
        el : '#departments',
        data : {
            list : [],
            name : '',
            nameClass : 'form-control',
            hid : '',
            branch : "{{array_keys($branches)[0]}}",
            enabled : false
        },
        created() {
            this.fetchData();
        },
        watch : {
            list : function (newVal, oldVal) {
                if (newVal != oldVal && $.fn.dataTable.isDataTable('#department-table')) {
                    $('#department-table').DataTable().destroy();
                }
                setTimeout(() => {
                    $('#department-table').DataTable({
                        "language": {
                            "lengthMenu": "{{translate('pagination.display')}} _MENU_ {{translate('pagination.record_per_page')}}",
                            "zeroRecords": "{{translate('pagination.nothing_found')}}",
                            "info": "{{translate('pagination.showing_page')}} _PAGE_ {{translate('pagination.of')}} _PAGES_",
                            "infoEmpty": "{{translate('pagination.no_records')}}",
                            "infoFiltered": "({{translate('pagination.filterd_from')}} _MAX_ {{translate('pagination.total_records')}})",
                            "oPaginate" : {
                                "sFirst": "{{translate('pagination.first')}}",
                                "sLast": "{{translate('pagination.last')}}",
                                "sNext": "{{translate('pagination.next')}}",
                                "sPrevious": "{{translate('pagination.previous')}}"
                            },
                            "sSearch" : "{{translate('pagination.search')}}"
                        }
                    });
                }, 1000);
            },
            name : function (val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.nameClass = 'form-control';
                }
            }
        },
        methods : {
            fetchData: function () {
                axios.get("{{url('/department/data/')}}")
                .then(response => {
                    return new Promise((resolve, reject) => {
                        this.list = response.data;
                        resolve();
                    })
                })
                .catch(console.log);
            },
            saveData : function () {
                var errors = 0;
                if (this.name == "" || this.name.match(/^\s+/) != null) {
                    this.nameClass = 'form-control error';
                    errors = 1;
                }
                if (errors == 0) {
                    this.enabled = true;
                    axios.post("{{ url('/department/') }}", { 
                        'name' : this.name,
                        'hid' : this.hid,
                        'branch': this.branch
                    })
                    .then(response => {
                        this.enabled = false;
                        new PNotify({
                            title: (response.data === false) ? "{{translate('words.department')}}" : "{{translate('words.success_message')}}",
                            text: (response.data === false) ? "{{translate('alert.already_exist')}}" : "",
                            addclass: 'stack_top_right',
                            type: (response.data === false) ? "error" : "success",
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        this.name = '';
                        this.hid = '';
                        this.branch = "{{array_keys($branches)[0]}}";
                        this.fetchData();
                    })
                    .catch(console.log);
                }
            }
        }
    });
</script>