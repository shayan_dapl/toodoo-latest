@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    Vue.component('list-table', {
        props : ['list'],
        template : `
            <table class="table table-striped table-hover" id="action-type-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>{{strtoupper(translate('form.type'))}}</th>
                        <th class="actionwdth">{{translate('table.operations')}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="each in list">
                        <td>@{{each.type}}</td>
                        <td>
                            <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="editData(each)" title="{{translate('words.edit')}}">
                                <span class="text-success fa fa-pencil"></span>
                            </a>
                            <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="removeData(each.id)" title="{{translate('words.remove')}}">
                                <span class="text-danger fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        `,
        methods : {
            editData: function(type) {
                vm.name = type.type;
                vm.hid = type.id;
            },
            removeData: function(val) {
                var confirmed = confirm("{{translate('alert.are_you_sure')}}");
                if (confirmed == true) {
                    axios.post("{{ url('/rating/action-type/remove') }}", {
                        'id' : val,
                        '_token' : "{{csrf_token()}}"
                    }).then(response => {
                        var title = (response.data === false) ? "{{translate('words.remove')}} {{translate('alert.not_allowed')}}" : "{{translate('words.success_message')}}";
                        var type = (response.data === false) ? "error" : "success";
                        this.disable = false;
                        new PNotify({
                            title: title,
                            text: "",
                            addclass: 'stack_top_right',
                            type: type,
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        vm.fetchData();
                    })
                    .catch(console.log);
                }
            }
        }
    });

    var vm = new Vue({
        el: '#rating',
        data: {
            types : [],
            name : '',
            nameClass : 'form-control',
            hid : '',
            disable : false
        },
        created() {
            this.fetchData();
        },
        watch : {
            types : function (newVal, oldVal) {
                if (newVal != oldVal && $.fn.dataTable.isDataTable('#action-type-table')) {
                    $('#action-type-table').DataTable().destroy();
                }
                setTimeout(() => {
                    $('#action-type-table').DataTable({
                        "language": {
                            "lengthMenu": "{{translate('pagination.display')}} _MENU_ {{translate('pagination.record_per_page')}}",
                            "zeroRecords": "{{translate('pagination.nothing_found')}}",
                            "info": "{{translate('pagination.showing_page')}} _PAGE_ {{translate('pagination.of')}} _PAGES_",
                            "infoEmpty": "{{translate('pagination.no_records')}}",
                            "infoFiltered": "({{translate('pagination.filterd_from')}} _MAX_ {{translate('pagination.total_records')}})",
                            "oPaginate" : {
                                "sFirst": "{{translate('pagination.first')}}",
                                "sLast": "{{translate('pagination.last')}}",
                                "sNext": "{{translate('pagination.next')}}",
                                "sPrevious": "{{translate('pagination.previous')}}"
                            },
                            "sSearch" : "{{translate('pagination.search')}}"
                        }
                    });
                }, 1000);
            },
            name : function (val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.nameClass = 'form-control';
                }
            }
        },
        methods: {
            fetchData: function() {
                axios.get("{{url('/rating/action-type/data')}}")
                .then(response => {
                    return new Promise((resolve, reject) => {
                        this.types = response.data;
                        resolve();
                    });
                })
                .catch(console.log);
            },
            saveData: function() {
                var errors = 0;
                if(this.name == "" || this.name.match(/^\s+/) != null) {
                    this.nameClass = 'form-control error';
                    errors = 1;
                }
                if (errors == 0) {
                    this.disable = true;
                    axios.post("{{ url('/rating/action-type/save') }}", { 
                        'type' : this.name,
                        'hid' : this.hid,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        var title = (response.data === false) ? "{{translate('alert.already_exist')}}" : "{{translate('words.success_message')}}";
                        var type = (response.data === false) ? "error" : "success";
                        this.disable = false;
                        new PNotify({
                            title: title,
                            text: "",
                            addclass: 'stack_top_right',
                            type: type,
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        this.name = this.hid = '';
                        this.fetchData();
                    })
                    .catch(console.log);
                }
            }
        }
    });
</script>