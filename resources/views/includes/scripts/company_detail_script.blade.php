<script type="text/javascript">
	$(document).ready(function() {
		if($('#is_apply_vat').is(':checked')){
			$('#tva').prop('readonly',true);
			$('#tva').prop('required',false);
			$('#tva_label').removeClass('required');
			$('#tva').val('');
		}
		$(document).on('click', '#is_apply_vat', function() {
			if($(this).is(':checked') == true){
				$('#tva').prop('readonly',true);
				$('#tva').prop('required',false);
				$('#tva_label').removeClass('required');
				$('#tva').val('');
			}
			else{
				$('#tva').prop('readonly',false);
				$('#tva').prop('required',true);
				$('#tva_label').addClass('required');
			}
		})
		$('#cmpDetailFrm').on("submit", function()
		{ 
			var tva_no = $('#tva').val();
			var country = $('#country').val();
			var zip_code = $('#zip_code').val();
			var zip_validate = Validate_Zipcode(country,zip_code);
			var tva_validate = Validate_VATNumber(country,tva_no);
			if($('#is_apply_vat').is(':checked') == true){
				tva_validate = true;
			}
			if ((zip_validate == true && tva_validate == true) || (country != 1 && country != 2)) {
				$('#zip-error').html('');
				$('#tva-error').html('');
			} else {
				if (zip_validate == false) {
					if(country == 1){
						$('#zip-error').html("{{translate('words.zip_code_not_valid_for_belgium')}}");
					} else if(country == 2) {
						$('#zip-error').html("{{translate('words.zip_code_not_valid_for_netherland')}}");
					}
				} else {
					$('#zip-error').html('');
				}
				if (tva_validate == false) {
					if(country == 1){
						$('#tva-error').html("{{translate('words.tva_not_valid_for_belgium')}}");
					} else if(country == 2) {
						$('#tva-error').html("{{translate('words.tva_not_valid_for_netherland')}}");
					}
				} else {
					$('#tva-error').html('');
				}
				return false;
			}	
		})
	});

	function Validate_VATNumber(CountryCode, UsersVAT) {
		var VATreg = '';

		var validator = {
			'1' : function(){
				var reg = new RegExp(/^(BE)?\s?(\d\.?\-?\s?){1,}\d+$/ig);
				return reg.test(UsersVAT) && (!!UsersVAT.match(/\d/g) && UsersVAT.match(/\d/g).length == 10);
			},
			'2' : function(){
				var reg = new RegExp(/^(NL)?\s?(\d\.?\-?\s?){1,}(B\d{2})$/ig);
				return reg.test(UsersVAT) && (!!UsersVAT.match(/\d/g) && UsersVAT.match(/\d/g).length == 11);
			}
		};

		if (validator[CountryCode] && validator[CountryCode]()) {
			return true;
		} else {
			return false;
		}
	}

	function Validate_Zipcode(CountryCode, UsersVAT) {
		var VATreg = '';

		var validator = {
			'1' : function(){
				var reg = new RegExp(/^\d{4}$/ig);
				return reg.test(UsersVAT);
			},
			'2' : function(){
				var reg = new RegExp(/^\d{4}?\s?[a-z]{2}$/ig);
				return reg.test(UsersVAT);
			}
		};

		if(validator[CountryCode] && validator[CountryCode]()) {
			return true;
		}else {
			return false;
		}
	}
</script>