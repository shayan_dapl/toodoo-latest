@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $("#position-filter").change(function () {
            var branchId = $('#branch-filter').val();
            var positionId = $(this).val();
            var ratingId = $('#rating-filter').val();
            if(positionId != ''){
                var positionId = '/'+ positionId;
            } else {
                var positionId = ((ratingId != '') || (branchId != '')) ? '/all' : '';
            }
            if(branchId != ''){
                var branchId = '/'+ branchId;
            } else {
                var branchId = (ratingId != '') ? '/all' : '';
            }
            if(ratingId != ''){
                var ratingId = '/'+ ratingId;
            } else {
                var ratingId = '';
            }
            var url = "{{url('/process/competent-metrix/')}}"+positionId + branchId + ratingId;
            $(location).attr('href',url);
        })

        $('#branch-filter').change(function(){
            var branchId = $(this).val();
            var positionId = $('#position-filter').val();
            var ratingId = $('#rating-filter').val();
            if (positionId != '') {
                var positionId = '/'+ positionId;
            } else {
                var positionId = ((ratingId != '') || (branchId != '')) ? '/all' : '';
            }
            if (branchId != '') {
                var branchId = '/'+ branchId;
            } else {
                var branchId = (ratingId != '') ? '/all' : '';
            }
            if(ratingId != ''){
                var ratingId = '/'+ ratingId;
            } else {
                var ratingId = '';
            }
            var url = "{{url('/process/competent-metrix/')}}"+positionId + branchId + ratingId;
            $(location).attr('href',url);
        })

        $('#rating-filter').change(function(){
            var branchId = $('#branch-filter').val();
            var positionId = $('#position-filter').val();
            var ratingId = $(this).val();
            if (positionId != '') {
                var positionId = '/'+ positionId;
            } else {
                var positionId = ((ratingId != '') || (branchId != '')) ? '/all' : '';
            }
            if (branchId != '') {
                var branchId = '/'+ branchId;
            } else {
                var branchId = (ratingId != '') ? '/all' : '';
            }
            if(ratingId != ''){
                var ratingId = '/'+ ratingId;
            } else {
                var ratingId = '';
            }
            var url = "{{url('/process/competent-metrix/')}}"+positionId + branchId + ratingId;
            $(location).attr('href',url);
        }) 
    });

    var vm = new Vue({
        el : '#app',
        data : {
            list : [],
            actionType : '',
            actionTypeClass : 'form-control',
            level : '',
            levelClass : 'form-control',
            levelText : '',
            rating : 0,
            prev : '',
            is_action : false,
            action : '',
            actionClass : 'form-control',
            delegate : '',
            delegateClass : 'form-control',
            deadline : '',
            deadlineClass : 'form-control DatePicker4 pointer',
            comment : '',
            positionId : 0,
            processId : 0,
            stepId : 0,
            orgId : 0
        },
        mounted() {
            var comptDtOptions = $.extend({},
            $.datepicker.regional["{{session('lang')}}"],  // Dynamically      
            { 
                dateFormat: "dd-mm-yy",
                onSelect: function(dateText) {
                    vm.deadline = dateText;
                }
            });
            $(".DatePicker4").datepicker(comptDtOptions);
        },
        watch : {
            action : function (val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.actionClass = 'form-control';
                }
            },
            delegate : function (val) {
                if (val != "") {
                    this.delegateClass = 'form-control';
                }
            },
            deadline : function (val) {
                if (val != "") {
                    this.deadlineClass = 'form-control pointer';
                }
            },
            level : function (val) {
                if (val != '') {
                    this.levelClass = 'form-control';
                }
            },
            actionType : function (val) {
                if (val != '') {
                    this.actionTypeClass = 'form-control';
                }
            }
        },
        methods : {
            fetchData : function (process, step, org) {
                axios.get("{{url('/process/competent-rating')}}/"+process+"/"+step+"/"+org)
                .then(response => {
                    vm.list = response.data;
                })
                .then(() => {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            },
            pushRating : function (prevRating) {
                this.level = '';
                this.is_action = false;
                this.comment = '';
                this.action = '';
                this.deadline = '';
                this.delegate = '';
                this.prev = prevRating;
                this.positionId = this.$refs[prevRating].dataset.position;
                this.processId = this.$refs[prevRating].dataset.process;
                this.stepId = this.$refs[prevRating].dataset.step;
                this.orgId = this.$refs[prevRating].dataset.org;
                this.fetchData(this.processId, this.stepId, this.orgId);
                $('#competentModal').modal();
            },
            saveRating : function () {
                let errors = 0;
                if (this.level == "") {
                    errors = 1;
                    this.levelClass = 'form-control error';
                }
                if (this.level != "") {
                    this.levelText = this.$refs[this.level].selectedOptions[0].dataset.rating;
                }
                if (this.is_action === true) {
                    if (this.action == "" || this.action.match(/^\s+/) != null) {
                        this.actionClass = "form-control error";
                        errors = 1;
                    }
                    if (this.delegate == "") {
                        this.delegateClass = "form-control error";
                        errors = 1;
                    }
                    if (this.deadline == "") {
                        this.deadlineClass = "form-control pointer error";
                        errors = 1;
                    }
                    if (this.actionType == "") {
                        this.actionTypeClass = "form-control error";
                        errors = 1;
                    }
                }
                if (errors == 0) {
                    axios.post("{{url('/process/competent-rating-save')}}", {
                        'process' : this.processId,
                        'step' : this.stepId,
                        'org' : this.orgId,
                        'rating' : this.level,
                        'comment' : this.comment,
                        'isAction' : (this.is_action === true) ? 1 : 0,
                        'action' : this.action,
                        'action_type' : this.actionType,
                        'delegateTo' : this.delegate,
                        'deadline' : this.deadline,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        this.$refs[this.prev].dataset.ratingid = this.level;
                        this.$refs[this.prev].innerText = this.levelText;
                        $('#competentModal').modal('toggle');
                        this.$refs[this.prev].className = "text-center pointer bg-success";
                        let self = this.$refs[this.prev];
                        setTimeout(function () {
                            self.className = "text-center pointer";
                        }, 1000);
                    })
                    .catch(console.log);
                }
            },
            actionStatement : function (action, user) {
                if (action == null) {
                    return null;
                } else {
                    var status = (action.closed == 0) ? "{{translate('words.open')}}" : "{{translate('words.closed')}}";
                    var deadline = new Date(action.deadline);
                    var deadline = deadline.getDay() +'-'+ deadline.getMonth() + '-' + deadline.getFullYear();
                    return "<b>{{translate('form.action')}} : </b>" + action.action + "<br/><b>{{translate('words.action_type')}} : </b>" + action.type_name + "<br/><b>{{translate('form.deadline')}} : </b>" + deadline + "<br/><b>{{translate('words.delegated_to')}} : </b>" + user + "<br/><b>{{translate('words.status')}} : </b>" + status;
                }
            }
        }
    });
</script>