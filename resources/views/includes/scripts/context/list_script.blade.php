@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
	Vue.component('list-table', {
		props : ['list'],
		template : `
			<table class="table table-striped table-hover" id="context-table" cellspacing="0" width="100%">
	            <thead>
	                <tr>
	                    <th>{{translate('table.name')}}</th>
	                    <th>{{translate('table.operations')}}</th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr v-for="each in list">
	                    <td>@{{each.subject}}</td>
	                    <td>
	                        <a href="javascript:void(0)" class="btn btn-default" @click="editData(each)" title="{{translate('words.edit')}}">
	                            <span class="text-success fa fa-pencil"></span>
	                        </a>
	                        <a href="javascript:void(0)" class="btn btn-default" @click="removeData(each.id)" title="{{translate('words.remove')}}">
	                            <span class="text-danger fa fa-trash"></span>
	                        </a>
	                    </td>
	                </tr>
	            </tbody>
	        </table>
		`,
		methods : {
			editData : function (each) {
				vm.name = each.subject;
				vm.hid = each.id;
			},
			removeData : function (id) {
				var confirmed = confirm("{{translate('alert.are_you_sure')}}");
	            if (confirmed == true) {
	                axios.get("{{url('/context/remove')}}/" + id)
	                .then(response => {
	                    new PNotify({
	                        title : (response.data === false) ? "{{translate('words.faliure_message')}}" : "{{translate('words.success_message')}}",
	                        text : "",
	                        addclass : 'stack_top_right',
	                        type : (response.data === false) ? "error" : "success",
	                        width : '290px',
	                        delay : 2000
	                    });
	                })
	                .then(() => {
	                    vm.fetchData();
	                });
	            }
			}
		}
	});

	var vm = new Vue({
		el : '#context',
		data : {
			list : [],
			name : '',
			nameClass : 'form-control',
			nameError : '',
			hid : '',
			enabled : false
		},
		created() {
			this.fetchData();
		},
		watch : {
			list : function (newVal, oldVal) {
				if (newVal != oldVal && $.fn.dataTable.isDataTable('#context-table')) {
					$('#context-table').DataTable().destroy();
				}
				setTimeout(() => {
					$('#context-table').DataTable(dtLangs);
				});
			},
			name : function (val) {
				this.nameError = '';
				if (val != "" && val.match(/^\s+/) == null) {
					this.nameClass = 'form-control';
				}
			}
		},
		methods : {
			fetchData : function () {
				axios.get("{{url('/context/data')}}")
				.then(response => {
					return new Promise((resolve, reject) => {
						this.list = response.data;
						resolve();
					});
				})
				.catch(console.log);
			},
			saveData : function () {
				var errors = 0;
				if (this.name == "" || this.name.match(/^\s+/) != null) {
					this.nameClass = 'form-control error';
					errors = 1;
				}
				if (errors == 0) {
					this.enabled = true;
					axios.post("{{url('/context/save')}}", {
						'subject' : this.name,
						'hid' : this.hid,
						'_token' : "{{csrf_token()}}"
					})
					.then(response => {
						this.enabled = false;
						new PNotify({
	                        title : (response.data === false) ? "{{translate('words.faliure_message')}}" : "{{translate('words.success_message')}}",
	                        text : "",
	                        addclass : 'stack_top_right',
	                        type : (response.data === false) ? "error" : "success",
	                        width : '290px',
	                        delay : 2000
	                    });
	                }).then(() => {
	                	this.name = this.hid = '';
	                    this.fetchData();
	                })
	                .catch(error => {
						this.nameError = error.response.data.name[0];
						this.nameClass = "form-control error";
						this.enabled = false;
					});
				}
			}
		}
	});
</script>