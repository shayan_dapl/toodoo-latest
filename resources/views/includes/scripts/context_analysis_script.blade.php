<script type="text/javascript">
 	$(document).ready(function(){
	 	$('.select2-single').css('width', '100%');
		$('[name="action_yes_no"]').click(function() {
	        var seen = {'Yes' : 'row actionDiv', 'No' : 'row actionDiv hide'};
	        var require = {'Yes' : true, 'No' : false};
	        var vl = $(this).val();
	        $('.actionDiv').attr('class', seen[vl]);
	        $('.action-field').attr('required', require[vl]);
	    });
	    $("#branch").on('change', function () {
            var selectedBranch = [];
            for (var i = 0; i < this.selectedOptions.length; i++) {
                selectedBranch.push(this.selectedOptions[i].value);
            }

            if (selectedBranch.length == 0) {
                setTimeout(() => {
                    $('#processlink').next('span').addClass('error');
                }, 500);
            } else {
                setTimeout(() => {
                    $('#processlink').next('span').removeClass('error');
                }, 500);
            }

            $.ajax({
                url: "{{url('/context/check-process-branch')}}",
                type: "POST",
                data: {
                    "selectedBranch":JSON.stringify(selectedBranch),
                    "_token" : "{{csrf_token()}}"
                },
                success: function (data) {
                    var $select = $('#processlink');
                    var items = [];
                    $select.html('');
                    var options = $select.data('select2').options.options;
                    for (var i = 0; i < data.length; i++) 
                    {
                        items.push({
                            "id": data[i]['id'],
                            "text": data[i]['text']
                        });
                        $select.append("<option value=\"" + data[i]['id'] + "\">" + data[i]['text'] + "</option>");
                    }
                    options.data = items;
                    $select.select2(options);
                }
            });
        }); 

        $('#save-btn').on('click',function(){
            if ($('#processlink').val() == '') {
                $('.processlink').find('.select2-selection').addClass('error');  
            } else {
                $('.processlink').find('.select2-selection').removeClass('error');
            }
        });
 	});
</script>