@include('includes.chart_assets')
@stack('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.riskOpportunityCls').click(function () {
            sessionStorage.setItem("riskOrOpporId", $(this).data('riskoptid'));
            sessionStorage.setItem("riskOrOptType", $(this).data('type'));
            var processId = $(this).data("processid");
            $(location).attr('href', '{{url("/process/turtle")}}/' + processId);
        });
        $(document).on('click', '#planningTab', function () {
            $(location).attr('href', '{{url("/home/planning")}}');
        });
        $(document).on('click', '#cockpitTab', function () {
            $(location).attr('href', '{{url("/home")}}');
        });
        $(document).on('click', '#auditIssues', function () {
            $(location).attr('href', '{{url("/home/issues")}}');
        });
        $(document).on('click', '#auditMeeting', function () {
            $(location).attr('href', '{{url("/home/meeting")}}');
        });
        $(document).on('click', '#procesIssues', function () {
            $(location).attr('href', '{{url("/home/process_improvement")}}');
        });
        $(document).on('click', '#auditResult', function () {
            $(location).attr('href', '{{url("/home/audit_results")}}');
        });
        $(document).on('click', '#companyView', function () {
            $(location).attr('href', '{{url("/home/company-view")}}');
        });

        $('#not_started').click(function() {
            $('#closed').prop('checked', false);
            $('#ongoing').prop('checked', false);
            $('.not_started').show();
            if ($(this).is(':checked') == true) {
                $('.closed').hide();
                $('.ongoing').hide();
            }
            if ($(this).is(':checked') == false) {
                $('.closed').show();
                $('.ongoing').show();
            }
        });

        $('#ongoing').click(function() {
            $('#closed').prop('checked', false);
            $('#not_started').prop('checked', false);
            $('.ongoing').show();
            if ($(this).is(':checked') == true) {
                $('.closed').hide();
                $('.not_started').hide();
            }
            if ($(this).is(':checked') == false) {
                $('.closed').show();
                $('.not_started').show();
            }
        });

        $('#closed').click(function() {
            $('#not_started').prop('checked', false);
            $('#ongoing').prop('checked', false);
            $('.closed').show();
            if ($(this).is(':checked') == true) {
                $('.not_started').hide();
                $('.ongoing').hide();
            }
            if ($(this).is(':checked') == false) {
                $('.not_started').show();
                $('.ongoing').show();
            }
        });

        var dtsearchOption = $.extend({},
        $.datepicker.regional["{{session('lang')}}"], {
            dateFormat: "dd-mm-yy",
        });
        $(function() {
            $("#minDate").datepicker(dtsearchOption);
            $("#maxDate").datepicker(dtsearchOption);
        });
    });
</script>