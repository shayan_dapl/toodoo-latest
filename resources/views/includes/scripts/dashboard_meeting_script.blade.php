@include('includes.chart_assets')
@stack('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '#planningTab', function () {
            $(location).attr('href', '{{url("/home/planning")}}');
        });
        $(document).on('click', '#cockpitTab', function () {
            $(location).attr('href', '{{url("/home")}}');
        });
        $(document).on('click', '#auditIssues', function () {
            $(location).attr('href', '{{url("/home/issues")}}');
        });
        $(document).on('click', '#auditMeeting', function () {
            $(location).attr('href', '{{url("/home/meeting")}}');
        });
        $(document).on('click', '#procesIssues', function () {
            $(location).attr('href', '{{url("/home/process_improvement")}}');
        });
        $(document).on('click', '#auditResult', function () {
            $(location).attr('href', '{{url("/home/audit_results")}}');
        });
        $(document).on('click', '#companyView', function () {
            $(location).attr('href', '{{url("/home/company-view")}}');
        });
        $('#pending').click(function() {
            $('#closed').prop('checked', false);
            $('.pending').show();
            if ($(this).is(':checked') == true) {
                $('.closed').hide();
            }
            if ($(this).is(':checked') == false) {
                $('.closed').show();
            }
        });

        $('#closed').click(function() {
            $('#pending').prop('checked', false);
            $('.closed').show();
            if ($(this).is(':checked') == true) {
                $('.pending').hide();
            }
            if ($(this).is(':checked') == false) {
                $('.pending').show();
            }
        });
    });
</script>