@include('includes.chart_assets')
@stack('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '#cockpitTab', function () {
            $(location).attr('href', '{{url("/home")}}');
        });
        $(document).on('click', '#auditIssues', function () {
            $(location).attr('href', '{{url("/home/issues")}}');
        });
        $(document).on('click', '#auditMeeting', function () {
            $(location).attr('href', '{{url("/home/meeting")}}');
        });
        $(document).on('click', '#procesIssues', function () {
            $(location).attr('href', '{{url("/home/process_improvement")}}');
        });
        $(document).on('click', '#auditResult', function () {
            $(location).attr('href', '{{url("/home/audit_results")}}');
        });
        $(document).on('click', '#companyView', function () {
            $(location).attr('href', '{{url("/home/company-view")}}');
        });
    });
</script>