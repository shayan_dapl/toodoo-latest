@include('includes.chart_assets')
@stack('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.openPlaning').click(function () {
            $('#planningTab').trigger('click');
        });
        $('.openIssues').click(function () {
            $('#auditIssues').trigger('click');
        });
        $('.openMetting').click(function () {
            $('#auditMeeting').trigger('click');
        });
        $('.procesIssuess').click(function () {
            $('#procesIssues').trigger('click');
        });
        $('.riskOpportunityCls').click(function () {
            sessionStorage.setItem("riskOrOpporId", $(this).data('riskoptid'));
            sessionStorage.setItem("riskOrOptType", $(this).data('type'));
            var processId = $(this).data("processid");
            $(location).attr('href', '{{url("/process/turtle")}}/' + processId);
        });
        $(document).on('click', '.overview-segment-check', function () {
            $('.overview-segment-head').removeClass('active');
            $(this).closest('.overview-segment-head').addClass('active');
            $('.overview-segments').addClass('hide');
            $($(this).data('open')).removeClass('hide');
        });
        @auth('customer')
            /*====== Open and Closed Issues ======*/
            var config = {
                type: 'bar',
                data: {
                    labels: {!! $issueBarChart['months'] !!},
                    datasets: [{
                        label: 'Closed',
                        borderColor: window.chartColors.red,
                        backgroundColor: window.chartColors.red,
                        data: {!! $issueBarChart['closedIssues'] !!},
                    }, {
                        label: 'Open',
                        borderColor: window.chartColors.yellow,
                        backgroundColor: window.chartColors.yellow,
                        data: {!! $issueBarChart['openIssues'] !!},
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Open and closed issues'
                    },
                    tooltips: {
                        mode: 'index',
                    },
                    hover: {
                        mode: 'index'
                    },
                    scales: {
                        xAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: true,
                                labelString: ''
                            }
                        }],
                        yAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: true,
                                labelString: ''
                            }
                        }]
                    }
                }
            };
            /*====================================*/

            /*====== Open and Closed Issues Pie chart =========*/
            var openPie = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [{{$openPieChart['issues']}}],
                        backgroundColor: {!! $openPieChart['backgroundColors'] !!},
                        label: 'Dataset 1'
                    }],
                    labels: {!! $openPieChart['legends'] !!}
                },
                options: {
                    responsive: true
                }
            };

            var closedPie = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [{{$closedPieChart['issues']}}],
                        backgroundColor: {!! $closedPieChart['backgroundColors'] !!},
                        label: 'Dataset 1'
                    }],
                    labels: {!! $closedPieChart['legends'] !!}
                },
                options: {
                    responsive: true
                }
            };
            /*=================================================*/

            if(document.getElementById('open-closed-issues-chart') != null) {
                var ctx = document.getElementById('open-closed-issues-chart').getContext('2d');
                window.myLine = new Chart(ctx, config);
            }
            if(document.getElementById('open-issues-pie-chart') != null) {
                var opie = document.getElementById('open-issues-pie-chart').getContext('2d');
                window.myPie = new Chart(opie, openPie);
            }
            if(document.getElementById('closed-issues-pie-chart') != null) {
                var cpie = document.getElementById('closed-issues-pie-chart').getContext('2d');
                window.myPieClosed = new Chart(cpie, closedPie);
            }
            $('.upfilter, .issuecategory').change(function () {
                $('#loaderDv').show();
                var output = $('.upfilter').data('filter');
                var filterData = $('.upfilter').val() +'|'+ $('.issuecategory').val();
                $.fn.filterGraph(output,filterData); 
            });
            $('.downfilter').change(function () {
                $('#loaderDv').show();
                var output = $('.downfilter').data('filter');
                var filterData = $('.downfilter').val() +'| all';
                $.fn.filterGraph(output,filterData); 
            });
            $.fn.filterGraph = function(output,filterData){
              $.ajax({
                        url: "{{url('/home/chart/filter')}}/" + output + "/" + filterData,
                        type: 'GET',
                        success: function (data) {
                            if (output == 'open-closed-issues') {
                                window.myLine.destroy();

                                var config = {
                                    type: 'bar',
                                    data: {
                                        labels: data.months,
                                        datasets: [{
                                            label: 'Closed',
                                            borderColor: window.chartColors.red,
                                            backgroundColor: window.chartColors.red,
                                            data: data.closedIssues,
                                        }, {
                                            label: 'Open',
                                            borderColor: window.chartColors.yellow,
                                            backgroundColor: window.chartColors.yellow,
                                            data: data.openIssues,
                                        }]
                                    },
                                    options: {
                                        responsive: true,
                                        title: {
                                            display: true,
                                            text: 'Open and closed issues'
                                        },
                                        tooltips: {
                                            mode: 'index',
                                        },
                                        scales: {
                                            xAxes: [{
                                                stacked: true,
                                                scaleLabel: {
                                                    display: true,
                                                    labelString: ''
                                                }
                                            }],
                                            yAxes: [{
                                                stacked: true,
                                                scaleLabel: {
                                                    display: true,
                                                    labelString: ''
                                                }
                                            }]
                                        }
                                    }
                                };

                                var ctx = document.getElementById('open-closed-issues-chart').getContext('2d');
                                window.myLine = new Chart(ctx, config);
                            }

                            if (output == "open-closed-issues-pie") {
                                window.myPie.destroy();
                                window.myPieClosed.destroy();
                                var op = data.open.issues.split(",");
                                var openPie = {
                                    type: 'pie',
                                    data: {
                                        datasets: [{
                                            data: op,
                                            backgroundColor: jQuery.parseJSON(data.open.backgroundColors),
                                            label: 'Dataset 1'
                                        }],
                                        labels: jQuery.parseJSON(data.open.legends)
                                    },
                                    options: {
                                        responsive: true
                                    }
                                };
                                var cp = data.closed.issues.split(",");
                                var closedPie = {
                                    type: 'pie',
                                    data: {
                                        datasets: [{
                                            data: cp,
                                            backgroundColor: jQuery.parseJSON(data.closed.backgroundColors),
                                            label: 'Dataset 1'
                                        }],
                                        labels: jQuery.parseJSON(data.closed.legends)
                                    },
                                    options: {
                                        responsive: true
                                    }
                                };
                                var opie = document.getElementById('open-issues-pie-chart').getContext('2d');
                                window.myPie = new Chart(opie, openPie);
                                var cpie = document.getElementById('closed-issues-pie-chart').getContext('2d');
                                window.myPieClosed = new Chart(cpie, closedPie);
                            }

                            setTimeout(function(){ $('#loaderDv').hide(); }, 500);
                        }
                    });
            };
        @endauth
        
        $(document).on('click', '#planningTab', function () {
            $(location).attr('href', '{{url("/home/planning")}}');
        });
        $(document).on('click', '#cockpitTab', function () {
            $(location).attr('href', '{{url("/home")}}');
        });
        $(document).on('click', '#auditIssues', function () {
            $(location).attr('href', '{{url("/home/issues")}}');
        });
        $(document).on('click', '#auditMeeting', function () {
            $(location).attr('href', '{{url("/home/meeting")}}');
        });
        $(document).on('click', '#procesIssues', function () {
            $(location).attr('href', '{{url("/home/process_improvement")}}');
        });
        $(document).on('click', '#cockpit-issues', function () {
            $(location).attr('href', '{{url("/home")}}');
        });
        $(document).on('click', '#auditResult', function () {
            $(location).attr('href', '{{url("/home/audit_results")}}');
        });
        $(document).on('click', '#companyView', function () {
            $(location).attr('href', '{{url("/home/company-view")}}');
        });

        $(document).on('click', '#cockpit-unplanned-audit', function () {
            $(location).attr('href', '{{url("/home/cockpit/unplanned_audit")}}');
        });
        $(document).on('click', '#cockpit-insight-process', function () {
            $(location).attr('href', '{{url("/home/cockpit/insight_process")}}');
        });
        $(document).on('click', '#cockpit-upcoming-meeting', function () {
            $(location).attr('href', '{{url("/home/cockpit/upcoming_meeting")}}');
        });
        $(document).on('click', '.show-cmps', function () {
            var type = $(this).data("type");
            $.ajax({
                url: "{{url('/home/show-companies')}}/" + type ,
                type: 'GET',
                success: function (data) {
                    $("#list-company").html(data);
                    $("#companyListModal").modal('show');
                }
            });
        }); 
    });
</script>