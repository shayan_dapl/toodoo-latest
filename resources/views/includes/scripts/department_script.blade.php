@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    let vm = new Vue({
        el: '#departments',
        data: {
            list : [
                { name: "{{translate('table.no_data')}}" }
            ],
            nodata: 1,
            name : '',
            nameClass : 'gui-input',
            hid : '',
            branch : "{{array_keys($branches)[0]}}"
        },
        created() {
            this.fetchData();
        },
        watch : {
            name : function (val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.nameClass = 'gui-input';
                }
            }
        },
        methods: {
            fetchData: function() {
                axios.get("{{url('/department/data/')}}")
                .then(response => {
                    return new Promise((resolve, reject) => {
                        if (response.data.length > 0) {
                            this.nodata = 0;
                            this.list = response.data;
                            resolve();
                        } else {
                            reject();
                        }
                    })
                })
                .then(() => {
                    setTimeout(() => {
                        $('#datatable2').DataTable();
                    }, 1000);
                })
                .catch(console.log);
            },
            saveData: function() {
                let errors = 0;
                if(this.name == "" || this.name.match(/^\s+/) != null) {
                    this.nameClass = 'gui-input error';
                    errors = 1;
                }
                if (errors == 0) {
                    axios.post("{{ url('/department/') }}", { 
                        'name' : this.name,
                        'hid' : this.hid,
                        'branch': this.branch
                    })
                    .then(response => {
                        new PNotify({
                            title: (response.data === false) ? "{{translate('words.department')}}" : "{{translate('words.success_message')}}",
                            text: (response.data === false) ? "{{translate('alert.already_exist')}}" : "",
                            addclass: 'stack_top_right',
                            type: (response.data === false) ? "error" : "success",
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        $('#datatable2').DataTable().destroy();
                        this.name = '';
                        this.hid = '';
                        this.branch = "{{array_keys($branches)[0]}}";
                    })
                    .then(() => {
                        this.fetchData();
                    })
                    .catch(console.log);
                }
            },
            editData: function(name, branch, id) {
                this.name = name;
                this.hid = id;
                this.branch = branch;
            },
            removeData: function(val) {
                var confirmed = confirm("{{translate('alert.are_you_sure')}}");
                if (confirmed == true) {
                    axios.post("{{ url('/department/remove') }}", {
                        'id' : val,
                        '_token' : "{{csrf_token()}}"
                    }).then(response => {
                        let title = (response.data === true) ? "{{translate('words.success_message')}}" : ((response.data === 'child') ? "{{translate('alert.child_posiiton_exists')}}" : "{{translate('words.department')}}");
                        let text = (response.data === true) ? "" : ((response.data === 'child') ? "{{translate('alert.child_posiiton_exists')}}" : "{{translate('words.remove')}} {{translate('alert.not_allowed')}}");
                        let type = (response.data === true) ? "success" : "error";

                        new PNotify({
                            title: title,
                            text: text,
                            addclass: 'stack_top_right',
                            type: type,
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        $('#datatable2').DataTable().destroy();
                        this.list = [
                        { name: "{{translate('table.no_data')}}" }
                        ];
                    })
                    .then(() => {
                        this.fetchData();
                    })
                    .catch(console.log); 
                }
            }
        }
    });
</script>