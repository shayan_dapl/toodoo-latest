<script src="{{asset('js/chart/Chart.bundle.js')}}"></script>
<script src="{{asset('js/chart/utils.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '.process-name', function () {
            if (typeof $(this).data('auditid') !== 'undefined') {
                var processname = $(this).data('process');
                var auditid = $(this).data('auditid');
                $('.modal-title').html(processname);
                $('.modal-body').html('<i class="fa fa-cog fa-spin fa-3x text-system mleft46"></i>');
                $.ajax({
                    url: "{{url('/audit/executed-issue-details')}}/" + auditid,
                    type: "GET",
                    success: function (data) {
                        $('.modal-body').html(data);
                    }
                });
            }
        });
        $('.openPlaning').click(function () {
            $('#planningTab').trigger('click');
        });
        $('.openIssues').click(function () {
            $('#auditIssues').trigger('click');
        });
        $('.openMetting').click(function () {
            $('#auditMeeting').trigger('click');
        });
        $('.procesIssuess').click(function () {
            $('#procesIssues').trigger('click');
        });
        $('.riskOpportunityCls').click(function () {
            sessionStorage.setItem("riskOrOpporId", $(this).data('riskoptid'));
            sessionStorage.setItem("riskOrOptType", $(this).data('type'));
            var processId = $(this).data("processid");
            $(location).attr('href', '{{url("/process/turtle")}}/' + processId);
        });
        $(document).on('click', '.overview-segment-check', function () {
            $('.overview-segment-head').removeClass('active');
            $(this).closest('.overview-segment-head').addClass('active');
            $('.overview-segments').addClass('hide');
            $($(this).data('open')).removeClass('hide');
        });
    });
    @auth('customer')


    /*======= Process Insight =======*/
    var barChartData = {
        labels: {!! $processData !!},
        datasets: {!! $insightData !!}
    };
    var barChartData1 = {
        labels: {!! $findingNames !!},
        datasets: [{
            label: 'Open',
            backgroundColor: window.chartColors.red,
            data: {!! $openFindings !!}
        }, {
            label: 'Closed',
            backgroundColor: window.chartColors.blue,
            data: {!! $closedFindings !!}
        }]
    };
    /*===============================*/

    window.onload = function() {
        var ctxinsight = document.getElementById('insightProcess').getContext('2d');
        window.myBar = new Chart(ctxinsight, {
            type: 'bar',
            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: 'Insights to Process'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            min: 0,
                            stepSize: 1
                        }
                    }]
                }
            }
        });
        var ctx1 = document.getElementById('openClosedPerinsight').getContext('2d');
        window.myBar = new Chart(ctx1, {
            type: 'bar',
            data: barChartData1,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            min: 0,
                            stepSize: 1
                        }
                    }]
                }
            }
        });
    };

    $(document).on('change', '#filter', function() {
        $('#loaderDv').show();
        var output = $(this).data('filter');
        if ($(this).val() != "") {
            $.ajax({
                url: "{{url('/home/chart/filter')}}/" + output + "/" + $(this).val(),
                type: 'GET',
                success: function (data) {
                    if (output == 'open-closed-issues') {
                        var config = {
                            type: 'line',
                            data: {
                                labels: data.issueMonths,
                                datasets: [{
                                    label: 'Closed',
                                    borderColor: window.chartColors.red,
                                    backgroundColor: window.chartColors.red,
                                    data: data.closedIssuesChartData,
                                }, {
                                    label: 'Open',
                                    borderColor: window.chartColors.yellow,
                                    backgroundColor: window.chartColors.yellow,
                                    data: data.openIssuesChartData,
                                }]
                            },
                            options: {
                                responsive: true,
                                title: {
                                    display: true,
                                    text: 'Open and closed issues'
                                },
                                tooltips: {
                                    mode: 'index',
                                },
                                hover: {
                                    mode: 'index'
                                },
                                scales: {
                                    xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: ''
                                        }
                                    }],
                                    yAxes: [{
                                        stacked: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: ''
                                        }
                                    }]
                                }
                            }
                        };
                        var ctx = document.getElementById('open-closed-issues-chart').getContext('2d');
                        window.myLine = new Chart(ctx, config);
                    }

                    if (output == 'unplanned-audit') {
                        var Auditconfig = {
                            type: 'line',
                            data: {
                                labels: data.monthArr,
                                datasets: [{
                                    label: 'Unplanned Audits',
                                    borderColor: window.chartColors.red,
                                    backgroundColor: window.chartColors.red,
                                    data: data.unplannedAuditsMonthWise,
                                }, {
                                    label: 'Planned Audits',
                                    borderColor: window.chartColors.blue,
                                    backgroundColor: window.chartColors.blue,
                                    data: data.plannedAuditsMonthWise,
                                }, {
                                    label: 'Ongoing Audits',
                                    borderColor: window.chartColors.green,
                                    backgroundColor: window.chartColors.green,
                                    data: data.ongoingAuditsMonthWise,
                                }, {
                                    label: 'Finished Audits',
                                    borderColor: window.chartColors.yellow,
                                    backgroundColor: window.chartColors.yellow,
                                    data: data.finishedAuditsMonthWise,
                                }]
                            },
                            options: {
                                responsive: true,
                                title: {
                                    display: true,
                                    text: '{{translate("words.audit_planning")}}'
                                },
                                tooltips: {
                                    mode: 'index',
                                },
                                hover: {
                                    mode: 'index'
                                },
                                scales: {
                                    xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Month'
                                        }
                                    }],
                                    yAxes: [{
                                        stacked: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Value'
                                        }
                                    }]
                                }
                            }
                        };
                        var Auditctx = document.getElementById('unplannedAudit').getContext('2d');
                        window.myLine = new Chart(Auditctx, Auditconfig);
                    }

                    if (output == 'insight-process') {
                        var barChartData = {
                            labels: data.processData,
                            datasets: data.insightData
                        };
                        var ctxinsight = document.getElementById('insightProcess').getContext('2d');
                        window.myBar = new Chart(ctxinsight, {
                            type: 'bar',
                            data: barChartData,
                            options: {
                                title: {
                                    display: true,
                                    text: 'Insights to Process'
                                },
                                tooltips: {
                                    mode: 'index',
                                    intersect: false
                                },
                                responsive: true,
                                scales: {
                                    xAxes: [{
                                        stacked: true,
                                    }],
                                    yAxes: [{
                                        stacked: true
                                    }]
                                }
                            }
                        });
                    }

                    if (output == 'open-closed-per-insight') {
                        var barChartData1 = {
                            labels: data.findingNames,
                            datasets: [{
                                label: 'Open',
                                backgroundColor: window.chartColors.red,
                                data: data.openFindings
                            }, {
                                label: 'Closed',
                                backgroundColor: window.chartColors.blue,
                                data: data.closedFindings
                            }]
                        };
                        var ctx1 = document.getElementById('openClosedPerinsight').getContext('2d');
                        window.myBar = new Chart(ctx1, {
                            type: 'bar',
                            data: barChartData1,
                            options: {
                                title: {
                                    display: true,
                                    text: ''
                                },
                                tooltips: {
                                    mode: 'index',
                                    intersect: false
                                },
                                responsive: true,
                                scales: {
                                    xAxes: [{
                                        stacked: true,
                                    }],
                                    yAxes: [{
                                        stacked: true
                                    }]
                                }
                            }
                        });
                    }

                    if (output == 'audit-meeting-cal'){
                        $(meetingCalender).html(data);
                    }

                    setTimeout(function(){ $('#loaderDv').hide(); }, 500);
                }
            });
        }
    });
@endauth
</script>
<script type="text/javascript">
    @if(Session::has('open-popup'))
    $(document).ready(function() {
        $.ajax({
            url: "{{url('/getstarted/one')}}",
            type: "GET",
            success: function(data) {
                $("#get-started-popup-body").html(data);
                $("#get-started-popup").modal();
            }
        });
    });
    @endif
    $(document).ready(function() {
        $(document).on('click', '#planningTab', function () {
            $(location).attr('href', '{{url("/home/planning")}}');
        })
        $(document).on('click', '#cockpitTab', function () {
            $(location).attr('href', '{{url("/home")}}');
        })
        $(document).on('click', '#auditIssues', function () {
            $(location).attr('href', '{{url("/home/issues")}}');
        })
        $(document).on('click', '#auditMeeting', function () {
            $(location).attr('href', '{{url("/home/meeting")}}');
        })
        $(document).on('click', '#procesIssues', function () {
            $(location).attr('href', '{{url("/home/process_improvement")}}');
        })
        $(document).on('click', '#cockpit-issues', function () {
            $(location).attr('href', '{{url("/home")}}');
        })
        $(document).on('click', '#cockpit-unplanned-audit', function () {
            $(location).attr('href', '{{url("/home/cockpit/unplanned_audit")}}');
        })
        $(document).on('click', '#cockpit-insight-process', function () {
            $(location).attr('href', '{{url("/home/cockpit/insight_process")}}');
        })
        $(document).on('click', '#cockpit-upcoming-meeting', function () {
            $(location).attr('href', '{{url("/home/cockpit/upcoming_meeting")}}');
        })
    });
</script>