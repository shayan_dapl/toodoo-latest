<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(() => {
            $('.select2').css('width', '100%');
        }, 100);
        $(document).on('keyup', '#info', function() {
            $('.hid-info').val($(this).val());
        });

        $(document).on('click', '.action-submit', function(data) {
            var data = $(this).data('type');
            var issue = $('[name="'+data+'"]').val();
            var deadline = $('[name="'+data+'_deadline"]').val();
            var member = $('[name="delegate_member_'+data+'"]').val();
            if(member == "")
            {
                $('[name="delegate_member_'+data+'"]').next('span').css('border','1px solid red');
            }
            else
            {
                $('[name="delegate_member_'+data+'"]').next('span').css('border','none');
            }
        });

        $('#tabline li').click(function(event) {
            $(this).addClass('proactive');
        });

        $('#tabline li').click(function(event) {
            var link=$(this).find('a').attr('href');
            if(link!='#')
            {
                $('#tabline li').prevAll().addClass('proactive');
                $(this).addClass('proactive');
            }
            else
            {
               $(this).removeClass('proactive');
           }
           $(this).nextAll().removeClass('proactive');
       });
        
        $(document).on('change', '#delegate-1', function() {
            var process_owner = '{{Auth::guard('customer')->user()->id}}';
            if(process_owner == $(this).val())
            {
                $('#ia').prop("disabled", false);
            }
            else
            {
                $('#ia').val('');
                $('#ia').prop("disabled", true);
                $('#btn1').html('<i class="fa fa-plus"></i>');
            }
        });

        $('#ia').blur(function() {
            if($(this).val()!='')
            {
                curDate = $.datepicker.formatDate('mm/dd/yy', new Date()) ;
                $('#btn1').text('Complete');
                $('#deadline-1').val(curDate);
            }
            else
            {
                $('#btn1').html('<i class="fa fa-plus"></i>');
                $('#deadline-1').val('');
            }
        });

        $(document).on('change', '#delegate-2', function() {
            var process_owner = '{{ Auth::guard('customer')->user()->id }}';
            if(process_owner == $(this).val())
            {
                $('#rca').prop("disabled", false);
            }
            else
            {
                $('#rca').val('');
                $('#rca').prop("disabled", true);
                $('#btn2').html('<i class="fa fa-plus"></i>');
            }
        });

        $('#rca').blur(function() {
            if($(this).val()!='')
            {
                curDate = $.datepicker.formatDate('mm/dd/yy', new Date()) ;
                $('#btn2').text('Complete');
                $('#datetimepicker2').val(curDate);
            }
            else
            {
                $('#btn2').html('<i class="fa fa-plus"></i>');
                $('#datetimepicker2').val('');
            }
        });

        $(document).on('change', '#delegate-3', function() {
            var process_owner = '{{ Auth::guard('customer')->user()->id }}';
            if(process_owner == $(this).val())
            {
                $('#dc').prop("disabled", false);
            }
            else
            {
                $('#dc').val('');
                $('#dc').prop("disabled", true);
                $('#btn3').html('<i class="fa fa-plus"></i>');
            }
        });

        $('#dc').blur(function() {
            if($(this).val()!='')
            {
                curDate = $.datepicker.formatDate('mm/dd/yy', new Date()) ;
                $('#btn3').text('Complete');
                $('#deadline3').val(curDate);
            }
            else
            {
                $('#btn3').html('<i class="fa fa-plus"></i>');
                $('#deadline3').val('');
            }
        });

        $(document).on('change', '#delegate-4', function() {
            var process_owner = '{{Auth::guard('customer')->user()->id}}';
            if(process_owner == $(this).val())
            {
                $('#effect').prop("disabled", false);
            }
            else
            {
                $('#effect').val('');
                $('#effect').prop("disabled", true);
                $('#btn4').html('<i class="fa fa-plus"></i>');
            }
        });

        $('#effect').blur(function() {
            if($(this).val()!='')
            {
                curDate = $.datepicker.formatDate('mm/dd/yy', new Date()) ;
                $('#btn4').text('Complete');
                $('#datetimepicker3').val(curDate);
            }
            else
            {
                $('#btn4').html('<i class="fa fa-plus"></i>');
                $('#datetimepicker3').val('');
            }
        });
    });
</script>