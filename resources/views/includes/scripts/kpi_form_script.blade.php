<script type="text/javascript">
    $(document).ready(function() {
        var kpi_unit = '{{ isset($details->kpi_unit_id) ? $details->kpi_unit_id : "" }}';
        if (kpi_unit == 1 || kpi_unit == 2) {
            $('#txtOpt').removeClass('hide');
            $('#boolOpt-field').removeAttr('required');
            $('#trendOpt-field').removeAttr('required');
        } else if (kpi_unit == 3) {
            $('#boolOpt').removeClass('hide');
            $('#trendOpt-field').removeAttr('required');
            $('#txtOpt-field').removeAttr('required');
        } else if (kpi_unit == 4) {
            $('#trendOpt').removeClass('hide');
            $('#boolOpt-field').removeAttr('required');
            $('#txtOpt-field').removeAttr('required');
        }
        $('#kpi_unit').on("change", function (e) { 
            var selectedKpiUnit = $(this).val();
            if (selectedKpiUnit == 3) {
                $('#boolOpt').removeClass('hide');
                $('#trendOpt-field').removeAttr('required');
                $('#txtOpt-field').removeAttr('required');
                $('#txtOpt-field').val('');
                $('#trendOpt-field').find('option:selected').removeAttr('selected');
                $('#trendOpt').addClass('hide');
                $('#txtOpt').addClass('hide');
            }
            if (selectedKpiUnit == 4) {
                $('#trendOpt').removeClass('hide');
                $('#boolOpt-field').removeAttr('required');
                $('#txtOpt-field').removeAttr('required');
                $('#txtOpt-field').val('');
                $('#boolOpt-field').find('option:selected').removeAttr('selected');
                $('#boolOpt').addClass('hide');
                $('#txtOpt').addClass('hide');
            }
            if (selectedKpiUnit == 1 || selectedKpiUnit == 2) {
                $(".select-target").select2();
                $('#boolOpt-field').removeAttr('required');
                $('#trendOpt-field').removeAttr('required');
                $('#txtOpt').removeClass('hide');
                $('#boolOpt').removeClass('hide');
                $('#trendOpt').removeClass('hide');
                $('#boolOpt').addClass('hide');
                $('#trendOpt').addClass('hide');
            } 
            $(".select-target").select2({
                placeholder: "{{translate('form.target')}}",
                width: '100%'
            });
        }); 
        $( "#profile-btn" ).click(function( ) {
            var frequency = $('#frequency').val();
            var kpi_unit = $('#kpi_unit').val();

            if (frequency == '') {
                $('#select2-frequency-container').parent('span').addClass('error');
            } else {
                $('#select2-frequency-container').parent('span').removeClass('error');
            }
            if (kpi_unit == '') {
                $('#select2-kpi_unit-container').parent('span').addClass('error');
            } else {
                $('#select2-kpi_unit-container').parent('span').removeClass('error');
            }
        }); 
        var validFromOpt = $.extend({},
        $.datepicker.regional["{{session('lang')}}"], {
            dateFormat: "dd-mm-yy",
            onSelect: function () {
                var dt2 = $('#valid-to');
                var minDate = $(this).datepicker('getDate');
                dt2.datepicker('option', 'minDate', minDate);
            }
        });
        var validToOpt = $.extend({},
        $.datepicker.regional["{{session('lang')}}"], {
            dateFormat: "dd-mm-yy"
        });
        $("#valid-from").datepicker(validFromOpt);
        $('#valid-to').datepicker(validToOpt);
    })  
    var validNumber = new RegExp(/^\d*\.?\d*$/);
    var lastValid = document.getElementById("txtOpt-field").value;
    function validateNumber(elem) {
        if (validNumber.test(elem.value)) {
            lastValid = elem.value;
        } else {
            elem.value = lastValid;
            new PNotify({
                title: "{{translate('alert.only_numeric_data_allowed')}}",
                text: "",
                addclass: 'stack_top_right',
                type: 'error',
                width: '290px',
                delay: 2000
            });
        }
    }
</script>