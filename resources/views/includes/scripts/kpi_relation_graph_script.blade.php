@include('includes.chart_assets')
@stack('scripts')
<script type="text/javascript">
$(document).ready(function() {
	    $.fn.extend({
	  	treeview: function() {
	    return this.each(function() {
	      // Initialize the top levels;
	      var tree = $(this);
	      
	      tree.addClass('treeview-tree');
	      tree.find('li').each(function() {
	        var stick = $(this);
	      });
	      tree.find('li').has("ul").each(function () {
	        var branch = $(this); //li with children ul
	        
	        branch.prepend("<i class='tree-indicator glyphicon glyphicon-chevron-right'></i>");
	        branch.addClass('tree-branch');
	        branch.on('click', function (e) {
	          if (this == e.target) {
	            var icon = $(this).children('i:first');
	            
	            icon.toggleClass("glyphicon-chevron-down glyphicon-chevron-right");
	            $(this).children().children().toggle();
	          }
	        })
	        branch.children().children().toggle();
	        branch.click();
	        branch.children('.tree-indicator, button, a').click(function(e) {
	          branch.click();
	          
	          e.preventDefault();
	        });
	      });
	    });
	  }
	});

	$(document).on("click", ".line-cls", function () {
	    $(".turtle-title").html("");
	    $(".turtle-body").html('<center><i class="fa fa-cog fa-spin fa-3x fa-fw text-system"></i></center>');
	    var id = $(this).data("id");
	    var model = $(this).data("model");
	    $.ajax({
	        url: "{{url('/process/turtle/graph')}}/" + id ,
	        type: "GET",
	        success: function (data) {
	        	$(".turtle-title").html(model);
	            $(".turtle-body").html(data);
	        }
	    });
	});
});

$(window).on('load', function () {
  $('.treeview').each(function () {
    var tree = $(this);
    tree.treeview();
  })
})
</script>