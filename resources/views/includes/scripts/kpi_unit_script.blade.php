@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
	new Vue({
		el : '#kpi-unit',
		data : {
			units : {},
			edit : false,
			name_nl : '',
			name_en : '',
			name_fr : '',
			id : ''
		},
		created () {
			this.list();
		},
		filters : {
			format : function (val) {
				let lang = "{{session('lang')}}";
				switch(lang) {
					case "en": {
					  	return val.name_en;
					  	break;
					}
					case "fr": {
					  	return val.name_fr;
					  	break;
					}
					default: {
					  	return val.name_nl;
					  	break;
					}
				}
			}
		},
		methods : {
			list : function () {
				axios.get("{{url('/kpi-unit/data')}}")
				.then(response => {
					this.units = response.data;
				})
				.catch(console.log);
			},
			editData : function (each, i) {
				each.edit = true;
			},
			save : function (each, i) {
				each.edit = false;
				axios.post("{{url('/kpi-unit')}}", {
					'id' : each.id,
					'name_nl' : each.name_nl,
					'name_en' : each.name_en,
					'name_fr' : each.name_fr,
					'_token' : "{{csrf_token()}}"
				})
				.then(response => {
					let title = (response.data === false) ? "{{translate('words.faliure_message')}}" : "{{translate('words.success_message')}}";
                    let type = (response.data === false) ? "error" : "success";
                    new PNotify({
                        title: title,
                        text: "",
                        addclass: 'stack_top_right',
                        type: type,
                        width: '290px',
                        delay: 2000
                    });
				})
				.catch(console.log);
			}
		}
	});
</script>