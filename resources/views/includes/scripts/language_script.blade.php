@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$(".minimize").click(function() {
			if($(this).find('i').hasClass('fa-minus')) {
				$(this).find('i').removeClass('fa-minus').addClass('fa-plus');
			}else if($(this).find('i').hasClass('fa-plus')) {
				$(this).find('i').removeClass('fa-plus').addClass('fa-minus');
			}
			var segment = $(this).data('segment');
			$("#" + segment).toggle();
		});
	});

	new Vue({
		el : '#app',
		data : {
			disable : false,
			wait : false,
			waitmessage : '',
			success : ''
		},
		watch : {
			success : function (val) {
				if (val != '') {
					setTimeout(() => {
						this.success = '';
					}, 1000);
				}
			}
		},
		methods : {
			restore : function () {
				var r = confirm("{{translate('alert.may_take_some_time')}}");
				if (r === true) {
				    this.disable = true;
					this.wait = true;
					this.waitmessage = "{{translate('alert.wait_until_complete')}}";
					axios.post("{{url('/language/restore')}}", {
						'_token' : "{{csrf_token()}}"
					})
					.then(response => {
						if (response.data === true) {
							this.disable = false;
							this.wait = false;
							this.waitmessage = '';
							this.success = "{{translate('words.success_message')}}";
						}
					})
					.catch(console.log);
				}
			}
		}
	});
</script>