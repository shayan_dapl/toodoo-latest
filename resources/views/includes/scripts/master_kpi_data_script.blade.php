@include('includes.chart_assets')
@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    Vue.component('period', {
        props: ['value'],
        template : `<input type="text" :value="value" />`,
        mounted : function () {
            var option = $.extend({},
            $.datepicker.regional["{{session('lang')}}"], {
                dateFormat: "dd-mm-yy",
                minDate: "{{date(Config::get('settings.dashed_date'), strtotime($details->valid_from))}}",
                maxDate: "{{date(Config::get('settings.dashed_date'), strtotime(date('Y-m-d')))}}", 
                onSelect: function () {
                    vm.period = this.value;
                }
            });
            $(this.$el).datepicker(option);
        }
    });

    Vue.component('kpi-list', {
        props : ['list', 'frequency'],
        data : function () {
            return {
                kpiUnit : "{{$details->kpi_unit_id}}",
                boolopt : {
                    '' : "{{translate('form.select')}}", 
                    '1' : "{{translate('words.true')}}", 
                    '0' : "{{translate('words.false')}}"
                },
                trendopt : {
                    '' : "{{translate('form.select')}}", 
                    '1' : "{{translate('words.up')}}", 
                    '0' : "{{translate('words.down')}}"
                }
            }
        },
        template : `
            <table class="table table-striped table-hover" id="datatable22" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>{{translate('words.kpi_value')}}</th>
                        <th v-if="frequency == 'daily'">{{translate('form.day')}}</th> 
                        <th v-if="frequency=='weekly'">{{translate('form.weeknumber')}}</th> 
                        <th v-if="frequency=='monthly'">{{translate('form.monthnumber')}}</th> 
                        <th v-if="frequency=='quarterly'">{{translate('form.quarternumber')}}</th>
                        <th>{{translate('words.entry_date')}}</th>
                        <th>{{translate('table.operations')}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="each in list">
                        <td v-if="kpiUnit == 3">@{{boolopt[each.kpi_value]}}</td>
                        <td v-if="kpiUnit == 4">@{{trendopt[each.kpi_value]}}</td>
                        <td v-if="kpiUnit != 3 && kpiUnit != 4">@{{each.kpi_value}}</td>
                        <td>@{{each.period}}</td>
                        <td>@{{each.created_at}}</td>
                        <td>
                            <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="editData(each)">
                                <span class="text-success fa fa-pencil"></span>
                            </a>
                            <a href="javascript:void(0);" class="btn btn-default btn-sm" @click="removeData(each.id)">
                                <span class="text-danger fa fa-trash"></span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        `,
        methods : {
            editData : function (data) {
                vm.edited = true;
                vm.target = data.kpi_value;
                vm.period = data.period;
                vm.targetBool = data.kpi_value;
                vm.trend = data.kpi_value;
                vm.hid = data.id;
                this.$nextTick(function () {
                    $('html, body').animate({
                        scrollTop: $("#kpidata").offset().top
                    }, 1000);
                    setTimeout(() => {
                        vm.targetClass = vm.targetBoolClass = vm.trendClass = 'form-control border-system';
                    }, 1000);
                });
            },
            removeData : function (id) {
                var confirmed = confirm("{{translate('alert.are_you_sure')}}");
                if (confirmed == true) {
                    axios.get("{{url('/master-kpi/dataremove')}}/" + id)
                    .then(response => {
                        new PNotify({
                            title : (response.data === false) ? "{{translate('words.you_dont_have_access')}}" : "{{translate('words.success_message')}}",
                            text : "",
                            addclass : 'stack_top_right',
                            type : (response.data === false) ? "error" : "success",
                            width : '290px',
                            delay : 2000
                        });
                    })
                    .then(() => {
                        vm.fetchData();
                        vm.fetchGraph();
                        vm.fetchRestricted();
                    });
                }
            }
        }
    });

    var vm = new Vue({
        el : '#kpidata',
        data : {
            list : [],
            frequency : "{{$details->frequency}}",
            weeks : {!! json_encode($weeks->weeks($details->valid_from, $details->valid_to)) !!},
            months : {!! json_encode($months->months($details->valid_from, $details->valid_to)) !!},
            quarters : {!! json_encode($quarters->quarters($details->valid_from, $details->valid_to)) !!},
            period : '{{($details->frequency == "daily" and strtotime($details->valid_from) < strtotime(date("d-m-Y")) and strtotime(date("d-m-Y")) < strtotime($details->valid_to)) ? date("d-m-Y") : ''}}',
            periodClass : 'form-control',
            restricted : [],
            masterId : "{{$details->id}}",
            kpiUnit : "{{$details->kpi_unit_id}}",
            boolOptions : {
                '' : "{{translate('form.select')}}", 
                '1' : "{{translate('words.true')}}", 
                '0' : "{{translate('words.false')}}"
            },
            trendOptions : {
                '' : "{{translate('form.select')}}", 
                '1' : "{{translate('words.up')}}", 
                '0' : "{{translate('words.down')}}"
            },
            target : '', targetClass : 'form-control', targetErr : '',
            targetBool : '', targetBoolClass : 'form-control',
            trend : '', trendClass : 'form-control',
            enabled : false,
            edited : false,
            hid : ''
        },
        created() {
            this.fetchData();
            this.fetchGraph();
            this.fetchRestricted();
        },
        watch : {
            list : function (val) {
                if ($.fn.dataTable.isDataTable('#datatable22')) {
                    $('#datatable22').DataTable().destroy();
                }
                setTimeout(() => {
                    $('#datatable22').DataTable(dtLangs);
                }, 100);
            },
            target : function (val) {
                if (val != '' && val.match(/^\d*\.?\d*$/) == null) {
                    this.targetClass = 'form-control error';
                    this.targetErr = "{{translate('alert.only_numeric_data_allowed')}}";
                } else {
                    this.targetClass = 'form-control';
                    this.targetErr = "";
                }
            },
            targetBool : function (val) {
                if (val != '') {
                    this.targetBoolClass = 'form-control';
                }
            },
            trend : function (val) {
                if (val != '') {
                    this.trendClass = 'form-control';
                }
            },
            period : function (val) {
                if (val != '') {
                    this.periodClass = 'form-control';
                }
            }
        },
        methods : {
            fetchRestricted : function () {
                axios.post("{{url('/master-kpi/datalist/restricted')}}", {
                    'segment' : 'master',
                    'id' : {{$details->id}},
                    'type' : "{{$details->frequency}}",
                    'from' : "{{$details->valid_from}}",
                    'to' : "{{$details->valid_to}}",
                    '_token' : "{{csrf_token()}}"
                })
                .then(response => {
                    this.restricted = response.data;
                })
                .catch(console.log);
            },
            fetchData: function () {
                axios.get("{{url('/master-kpi/datalist/'.$details->id.'/data')}}")
                .then(response => {
                    return new Promise((resolve, reject) => {
                        this.list = response.data;
                        resolve();
                    })
                })
                .catch(console.log);
            },
            fetchGraph : function () {
                axios.get("{{url('/master-kpi/datalist/'.$details->id.'/graph')}}")
                .then(response => {
                    this.label = response.data.label;
                    this.detail = response.data.detail;
                    var ctx = document.getElementById('master-kpi-graph').getContext('2d');
                    new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: response.data.label,
                            datasets: [{
                                label: "{{$details->name.' '.translate('words.graph')}}",
                                backgroundColor: 'rgb(255, 99, 132)',
                                borderColor: 'rgb(255, 99, 132)',
                                data: response.data.detail,
                            }]
                        },
                        options: {}
                    });
                });
            },
            saveData : function () {
                if (this.kpiUnit == 3 || this.kpiUnit == 4) {
                    this.target = '';
                }
                if (this.kpiUnit == 1 || this.kpiUnit == 2 || this.kpiUnit == 4) {
                    this.targetBool = '';
                }
                if (this.kpiUnit == 1 || this.kpiUnit == 2 || this.kpiUnit == 3) {
                    this.trend = '';
                }
                var errors = 0;
                if (this.period == "") {
                    this.periodClass = 'form-control error';
                    errors = 1;
                }
                if (this.kpiUnit == 1 || this.kpiUnit == 2) {
                    if (this.target == "" || this.target.match(/^\d*\.?\d*$/) == null) {
                        this.targetClass = 'form-control error';
                        errors = 1;
                    }
                }
                if (this.kpiUnit == 3) {
                    if (this.targetBool == '') {
                        this.targetBoolClass = 'form-control error';
                        errors = 1;
                    }
                }
                if (this.kpiUnit == 4) {
                    if (this.trend == '') {
                        this.trendClass = 'form-control error';
                        errors = 1;
                    }
                }
                if (errors == 0) {
                    this.enabled = true;
                    this.edited = false;
                    axios.post("{{url('/master-kpi/dataform')}}", {
                        'target' : this.target,
                        'targetBool' : this.targetBool,
                        'targetTrend' : this.trend,
                        'period' : this.period,
                        'hid' : this.hid,
                        'masterKpiId' : this.masterId,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        this.enabled = false;
                        new PNotify({
                            title : (response.data === false) ? "{{translate('words.faliure_message')}}" : "{{translate('words.success_message')}}",
                            text : "",
                            addclass : 'stack_top_right',
                            type : (response.data === false) ? "error" : "success",
                            width : '290px',
                            delay : 2000
                        });
                        this.fetchData();
                        this.fetchGraph();
                        this.fetchRestricted();
                        this.target = this.targetBool = this.trend = this.hid = this.period = '';
                    });
                }    
            }
        }
    });
</script>