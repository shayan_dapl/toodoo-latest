@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
Vue.component('valid-date', {
	props: ['value'],
	template : `<input type="text" :value="value" />`,
    mounted : function () {
        setTimeout(() => {
            $('#valid-to').datepicker('option', 'minDate', $('#valid-from').datepicker('getDate'));
        }, 500);
		var option = $.extend({},
        $.datepicker.regional["{{session('lang')}}"], {
            dateFormat: "dd-mm-yy",
            onSelect: function () {
 				if (this.dataset.type == "from") {
                    $('#valid-to').datepicker('option', 'minDate', this.value);
            		vm.validFrom = this.value;
 				}
 				if (this.dataset.type == "to") {
            		vm.validTo = this.value;
 				}
            }
        });
		$(this.$el).datepicker(option);
	}
});

Vue.component('list-table', {
    props : ['list', 'booloptions', 'trendoptions'],
    template : `
        <table class="table table-striped table-hover" id="datatable22" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>{{translate('form.kpi_name')}}</th>
                    <th>{{translate('form.valid_from')}}</th>
                    <th>{{translate('form.valid_to')}}</th>
                    <th>{{translate('form.frequency')}}</th>
                    <th>{{translate('words.kpi_unit')}}</th>
                    <th>{{translate('words.target')}}</th>
                    <th>{{translate('table.operations')}}</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="each in list">
                    <td>@{{each.name}}</td>
                    <td>@{{getDate(each.valid_from)}}</td>
                    <td>@{{getDate(each.valid_to)}}</td>
                    <td>@{{each.frequency | capitalize}}</td>
                    <td>@{{getName(each.kpiunit)}}</td>
                    <td v-if="each.kpi_unit_id == 3">@{{booloptions[each.target]}}</td>
                    <td v-if="each.kpi_unit_id == 4">@{{trendoptions[each.target]}}</td>
                    <td v-if="each.kpi_unit_id != 3 && each.kpi_unit_id != 4">@{{each.target}}</td>
                    <td>
                        <div class="disp-flex">
                            <a :href="kpiDataUrl(each.id)" class="btn btn-default btn-sm mr5" title="{{translate('words.manage_data')}}">
                                <span class="text-system fa fa-cog"></span>
                            </a>
                            <a href="javascript:void(0)" class="btn btn-default btn-sm mr5" @click="editData(each)" title="{{translate('words.edit')}}">
                                <span class="text-success fa fa-pencil"></span>
                            </a>
                            <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="removeData(each.id)" title="{{translate('words.remove')}}">
                                <span class="text-danger fa fa-times"></span>
                            </a>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    `,
    filters : {
        capitalize : function (val) {
            if (!val) return ''
            val = val.toString()
            return val.charAt(0).toUpperCase() + val.slice(1)
        }
    },
    methods : {
        getName : function (data) {
            var names = {
                'nl' : data.name_nl,
                'en' : data.name_en,
                'fr' : data.name_fr
            };
            return names["{{session('lang')}}"];
        },
        getDate : function (val) {
            return val.split("-").reverse().join("-");
        },
        kpiDataUrl : function (id) {
            return "{{url('/master-kpi/datalist/')}}/" + id;
        },
        editData : function (data) {
            vm.kpiName = data.name;
            vm.validFrom = data.valid_from.split("-").reverse().join("-");
            vm.validTo = data.valid_to.split("-").reverse().join("-");
            vm.frequency = data.frequency;
            vm.kpiUnit = data.kpi_unit_id;
            vm.target = data.target;
            vm.targetBool = data.target;
            vm.trend = data.target;
            vm.hid = data.id;
        },
        removeData : function (id) {
            var confirmed = confirm("{{translate('alert.are_you_sure')}}");
            if (confirmed == true) {
                axios.get("{{url('/master-kpi/remove')}}/" + id)
                .then(response => {
                    new PNotify({
                        title : (response.data === false) ? "{{translate('words.you_dont_have_access')}}" : "{{translate('words.success_message')}}",
                        text : "",
                        addclass : 'stack_top_right',
                        type : (response.data === false) ? "error" : "success",
                        width : '290px',
                        delay : 2000
                    });
                })
                .then(() => {
                    vm.fetchData();
                });
            }
        }
    }
});

var vm = new Vue ({
	el : '#masterkpi',
	data : {
		list : [],
		kpiName : '', kpiNameClass : 'form-control',
		validFrom : '{{date("01-01-Y")}}', validFromClass : 'form-control pointer',
		validTo : '{{date("31-12-Y")}}', validToClass : 'form-control pointer',
		frequency : '', frequencyClass : 'form-control',
		kpiUnit : '', kpiUnitClass : 'form-control',
		target : '', targetClass : 'form-control', targetErr : '',
		targetBool : '', targetBoolClass : 'form-control',
		boolOptions : {
			'' : "{{translate('form.select')}}", 
			'1' : "{{translate('words.true')}}", 
			'0' : "{{translate('words.false')}}"
		},
		trendOptions : {
			'' : "{{translate('form.select')}}", 
			'1' : "{{translate('words.up')}}", 
			'0' : "{{translate('words.down')}}"
		},
		trend : '', trendClass : 'form-control',
		enabled : false,
		hid : ''
	},
    created : function () {
        this.fetchData();
    },
    watch : {
        list : function (val) {
            if ($.fn.dataTable.isDataTable('#datatable22')) {
                $('#datatable22').DataTable().destroy();
            }
            setTimeout(() => {
                $('#datatable22').DataTable(dtLangs);
            }, 100);
        },
    	target : function (val) {
    		if (val.match(/^\d*\.?\d*$/) == null) {
    			this.targetErr = "{{translate('alert.only_numeric_data_allowed')}}";
    		} else {
    			this.targetErr = "";
    		}
    	}
    },
	methods : {
		fetchData: function () {
            axios.get("{{url('/master-kpi/data/')}}")
            .then(response => {
                return new Promise((resolve, reject) => {
                    this.list = response.data;
                    resolve();
                })
            })
            .catch(console.log);
        },
        saveData : function () {
        	var validated = new Array();
			this.kpiNameClass = (this.kpiName == "" || this.kpiName.match(/^\s+/) != null) ? 'form-control error' : 'form-control';
			validated.push((this.kpiName == "" || this.kpiName.match(/^\s+/) != null) ? 1 : 0);
			this.frequencyClass = (this.frequency == "") ? 'form-control error' : 'form-control';
			validated.push((this.frequency == "") ? 1 : 0);
			this.kpiUnitClass = (this.kpiUnit == "") ? 'form-control error' : 'form-control';
			validated.push((this.kpiUnit == "") ? 1 : 0);
			if (this.kpiUnit == 1 || this.kpiUnit == 2) {
				this.targetClass = (this.target == "" || this.target.match(/^\d*\.?\d*$/) == null) ? 'form-control error' : 'form-control';
				validated.push((this.target == "" || this.target.match(/^\d*\.?\d*$/) == null) ? 1 : 0);
			}
			if (this.kpiUnit == 3) {
				this.targetBoolClass = (this.targetBool == "") ? 'form-control error' : 'form-control';
				validated.push((this.targetBool == "") ? 1 : 0);
			}
			if (this.kpiUnit == 4) {
				this.trendClass = (this.trend == "") ? 'form-control error' : 'form-control';
				validated.push((this.trend == "") ? 1 : 0);
			}
			if (($.inArray(1, validated) == -1)) {
                if (this.kpiUnit == 3 || this.kpiUnit == 4) {
                    this.target = '';
                }
                if (this.kpiUnit == 1 || this.kpiUnit == 2 || this.kpiUnit == 4) {
                    this.targetBool = '';
                }
                if (this.kpiUnit == 1 || this.kpiUnit == 2 || this.kpiUnit == 3) {
                    this.targetTrend = '';
                }
				this.enabled = true;
				axios.post("{{url('/master-kpi/form')}}", {
					'kpiName' : this.kpiName,
					'validFrom' : this.validFrom,
					'validTo' : this.validTo,
					'frequency' : this.frequency,
					'kpiUnit' : this.kpiUnit,
					'target' : this.target,
					'targetBool' : this.targetBool,
					'targetTrend' : this.trend,
					'hid' : this.hid,
					'_token' : "{{csrf_token()}}"
				})
				.then(response => {
					this.enabled = false;
					new PNotify({
                        title : (response.data === false) ? "{{translate('words.faliure_message')}}" : "{{translate('words.success_message')}}",
                        text : "",
                        addclass : 'stack_top_right',
                        type : (response.data === false) ? "error" : "success",
                        width : '290px',
                        delay : 2000
                    });
                }).then(() => {
                	this.kpiName = this.frequency = this.kpiUnit = this.target = this.targetBool = this.trend = '';
					this.validFrom = '{{date("01-01-Y")}}';
					this.validTo = '{{date("31-12-Y")}}';
                    this.fetchData();
                });
			}
        }
	}
});
</script>