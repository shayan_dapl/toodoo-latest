@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
	new Vue({
		el : '#modify',
		data : {
			companyname : "{{Auth::guard('customer')->user()->company->name}}",
			firstname : "{{Auth::guard('customer')->user()->fname}}",
			lastname : "{{Auth::guard('customer')->user()->lname}}",
			phone : "{{Auth::guard('customer')->user()->phone}}",
			email : "{{Auth::guard('customer')->user()->email}}",
			enabled : true,
			companyname_class : '',
			firstname_class : '',
			lastname_class : '',
			email_class : '',
		},
		created () {
			this.companyname = this.decodeHtml(this.companyname);
			this.firstname = this.decodeHtml(this.firstname);
			this.lastname = this.decodeHtml(this.lastname);
			this.phone = this.decodeHtml(this.phone);
			this.email = this.decodeHtml(this.email);
		},
		methods : {
			decodeHtml: function (html) {
                var txt = document.createElement("textarea");
                txt.innerHTML = html;
                return txt.value;
            },
			validate : function() {
				let validated = new Array;
				let elemClassMap = {
					'companyname': "companyname_class",
					'firstname' : "firstname_class",
					'lastname' : "lastname_class",
					'email' : "email_class"
				};
				Object.keys(elemClassMap).forEach(elem => {
					this[elemClassMap[elem]] = (!this[elem].trim()) ?  'error' : '';
					validated.push((!this[elem].trim()) ?  1 : 0);
				});
				this.enabled = ($.inArray(1, validated) == -1) ? true : false;
			}
		}
	});
</script>