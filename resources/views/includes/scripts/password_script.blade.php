@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
	new Vue({
		el: '#passwordresetpassword',
		data: {
			password: '',
			passtype: 'password',
			passeye: 'fa fa-eye-slash',
			confpassword: '',
			confpasstype: 'password',
			confpasseye: 'fa fa-eye-slash',
			passclass: 'progress-bar',
			confpassclass: 'progress-bar progress-bar-danger per100',
			passval: 0,
			confpassval: 100,
			confpasstext: "{{translate('form.not_matching')}}",
			enable: false
		},
		methods: {
			passStrength: function(e) {
				if(this.password != "") {
					var counter = new Array;
					var strenthClass = {
						"1" : "progress-bar progress-bar-danger per20",
						"2" : "progress-bar progress-bar-warning per40",
						"3" : "progress-bar progress-bar-info per60",
						"4" : "progress-bar progress-bar-info per80",
						"5" : "progress-bar progress-bar-success per100"
					};
					var strenthText = {
						"1" : "{{translate('form.bad')}}",
						"2" : "{{translate('form.weak')}}",
						"3" : "{{translate('form.fair')}}",
						"4" : "{{translate('form.good')}}",
						"5" : "{{translate('form.very_good')}}"
					};

					counter.push("wordlen");
					if(this.password.length >= 8) {
						if(/[a-z]/.test(this.password) == true) {
							counter.push("lcword");
						}
						if(/[A-Z]/.test(this.password) == true) {
							counter.push("ucword");
						}
						if(/[0-9]/.test(this.password) == true) {
							counter.push("int");
						}
						if(/[!@#$%^&*()_+|]/.test(this.password) == true) {
							counter.push("scword");
						}
					}
					
					this.passclass = strenthClass[counter.length];
					this.passval = strenthText[counter.length];
					
					if(this.confpassword != "") {
						this.confpassclass = (this.confpassword == this.password) ? 'progress-bar progress-bar-success per100' : 'progress-bar progress-bar-danger per100';
						this.confpasstext = (this.confpassword == this.password) ? "{{translate('form.matched')}}" : "{{translate('form.not_matching')}}";
						this.enable = (this.confpassword == this.password && this.passval != "{{translate('form.bad')}}") ? true : false;
					}
				}
			},

			passMatch: function(e) {
				this.confpassclass = (this.confpassword == this.password) ? 'progress-bar progress-bar-success per100' : 'progress-bar progress-bar-danger per100';
				this.confpasstext = (this.confpassword == this.password) ? "{{translate('form.matched')}}" : "{{translate('form.not_matching')}}";
				this.enable = (this.confpassword == this.password && this.passval != "{{translate('form.bad')}}") ? true : false;
			},

			showHide: function(e) {
				if(e == "password") {
					this.passtype = (this.passtype == "password") ? "text" : "password";
					this.passeye = (this.passtype == "password") ? "fa fa-eye-slash" : "fa fa-eye";
				}
				if(e == "confpassword") {
					this.confpasstype = (this.confpasstype == "password") ? "text" : "password";
					this.confpasseye = (this.confpasstype == "password") ? "fa fa-eye-slash" : "fa fa-eye";
				}
			}
		}
	});
</script>