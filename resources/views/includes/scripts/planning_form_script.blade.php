<script type="text/javascript">
    $(document).ready(function() {
        $(".select-leadauditor").select2({
            placeholder: "{{translate('words.select_lead_auditor')}}"
        });

        if ($('#audit_category').val() == 'external') {
            $('#company').removeClass('hide');
            $('#lead_auditor').addClass('hide');
            $('#audit_company').prop('required',true);
        } else {
            $('#audit_company').prop('required',false);
            $('#company').addClass('hide');
            $('#audit_responsible').addClass('hide');
        }

        $('#audit_category').change(function () {
            var category = $(this).val();
            if (category == 'external') {
                $('#company').removeClass('hide');
                $('#audit_responsible').removeClass('hide');
                $('#lead_auditor').addClass('hide');
                $('#audit_company').prop('required',true);
                setTimeout(() => {
                    $('#porcess_audit').next('span').addClass('error');
                }, 500);
            } else {
                $('#audit_company').prop('required',false);
                $('#company').addClass('hide');   
                $('#audit_responsible').addClass('hide');
                $('#lead_auditor').removeClass('hide');
            }
            $(".select2-single").select2();
            $("#branch").val(null).trigger("change");
            var $select = $('#porcess_audit');
            var items = [];
            var data = [];
            $select.html('');
            var options = $select.data('select2').options.options;
            for (var i = 0; i < data.length; i++) 
            {
                items.push({
                    "id": data[i]['id'],
                    "text": data[i]['text']
                });
                $select.append("<option value=\"" + data[i]['id'] + "\">" + data[i]['text'] + "</option>");
            }
            options.data = items;
            $select.select2(options);
        })

        $('#branch').change(function (e) { 
            var selectedBranch = $(this).val();
            $.ajax({
                url: "{{url('/audit/check-process-branch')}}/" + selectedBranch,
                type: "GET",
                success: function (data) {
                    var $select = $('#porcess_audit');
                    var items = [];
                    $select.html('');
                    var options = $select.data('select2').options.options;
                    for (var i = 0; i < data.length; i++) 
                    {
                        items.push({
                            "id": data[i]['id'],
                            "text": data[i]['text']
                        });
                        $select.append("<option value=\"" + data[i]['id'] + "\">" + data[i]['text'] + "</option>");
                    }
                    options.data = items;
                    $select.select2(options);
                    setTimeout(() => {
                        $('#porcess_audit').next('span').addClass('error');
                    }, 500);
                }
            });
        });

        $("#porcess_audit").on('change', function () {
            var selectedProcess = [];
            var auditType = $('#audit_category').val();
            for (var i = 0; i < this.selectedOptions.length; i++) {
                selectedProcess.push(this.selectedOptions[i].value);
            }

            if (selectedProcess.length == 0) {
                setTimeout(() => {
                    $('#porcess_audit').next('span').addClass('error');
                }, 500);
            } else {
                setTimeout(() => {
                    $('#porcess_audit').next('span').removeClass('error');
                }, 500);
            }

            $.ajax({
                url: "{{url('/audit/check-process-owner')}}",
                type: "POST",
                data: {
                    "selectedProcess":JSON.stringify(selectedProcess),
                    "type" : auditType,
                    "_token" : "{{csrf_token()}}"
                },
                success: function (data) {
                    var $select = $('#choose_lead_auditor');
                    var items = [];
                    $select.html('');
                    var options = $select.data('select2').options.options;
                    for (var i = 0; i < data.length; i++) 
                    {
                        items.push({
                            "id": data[i]['id'],
                            "text": data[i]['text']
                        });
                        $select.append("<option value=\"" + data[i]['id'] + "\">" + data[i]['text'] + "</option>");
                    }
                    options.data = items;
                    $select.select2(options);
                }
            });
        }); 

        $('#audit_save').on('click',function(){
            if ($('#branch').val() == '') {
                $('.branch').find('.select2-selection').addClass('error');  
            } else {
                $('.branch').find('.select2-selection').removeClass('error');
            }
            if ($('#porcess_audit').val() == '') {
                $('#select2-porcess_audit-container').addClass('error');
            } else {
                $('#select2-porcess_audit-container').removeClass('error');
            }
        });
    })  

</script>