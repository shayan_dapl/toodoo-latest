<script type="text/javascript">
    $('.select2').css('width', '100%');

    $(document).ready(function () {
        $(document).on('change', '#department_id', function() {
            $('#parent-pos').html('<label class="col-md-9 control-label pt5"><i class="fa fa-cog fa-2x fa-spin text-system"></i></label>');

            var id = 0;
            if($(this).val() != "") {
                var id = $(this).val();
            }

            var hidpos = ($('#hidpos').val() == '') ? 0 : $('#hidpos').val();

            $.ajax({
                url: "{{url('/department/positions')}}/" + id + '/' + hidpos,
                type: "GET",
                success: function(data) {
                    $('#parent-pos').html(data);
                    if($('#parent_id').children('option').length == 1) {
                        $('#assistant').prop('checked', false);
                        $('#assistant-div').addClass('hide');
                        $('#special-user-div').addClass('hide');
                        if($('[name="is_special_position"]').is(':checked') == true) {
                            $('[name="is_special_position"]').trigger('click');
                        }
                    }

                    if($('#parent_id').children('option').length > 1) {
                        if($('#assistant-div').hasClass('hide')) {
                            $('#assistant-div').removeClass('hide');
                        }

                        if($('#special-user-div').hasClass('hide')) {
                            $('#special-user-div').removeClass('hide');
                        }
                    }

                    if(id == 0) {
                        if($('#assistant-div').hasClass('hide')) {
                            $('#assistant-div').removeClass('hide');
                        }

                        if($('#special-user-div').hasClass('hide')) {
                            $('#special-user-div').removeClass('hide');
                        }
                    }
                }
            });
        });

        $(document).on('click', '#assistant', function() {
            if($(this).is(':checked') == true) {
                if($('[name="is_special_position"]').is(':checked') == true) {
                    $(this).prop('checked', false);
                    alert("{{translate('alert.assistant_cant_be_special')}}");
                } else {
                    $('#position-name-label').html("{{translate('form.assist_as')}}");
                    $('#name').prop('required', false);
                    $('#position-name-label').removeClass('required');
                }
            }

            if($(this).is(':checked') == false) {
                $('#position-name-label').html("{{translate('form.position_name')}}");
                $('#name').prop('required', true);
                $('#position-name-label').addClass('required');
            }
        });

        $(document).on('click', '[name="is_special_position"]', function () {
            $('.select2').css('width', '100%');
            if ($('#users-panel').hasClass('hide')) {
                $('#users-panel').removeClass('hide');
            }

            //Reseting user dropdown value
            $('[name="user"]').val("").trigger('change');

            if ($(this).is(':checked') == true) {
                if($('#assistant').is(':checked') == true) {
                    $(this).prop('checked', false);
                    alert("{{translate('alert.assistant_cant_be_special')}}");
                } else {
                    $('#normal-span').addClass('hide');
                    $('#special-span').removeClass('hide');
                    if ($('#normal-user-checkbox').length > 0) {
                        $('#normal-user-checkbox').addClass('hide');
                        $('#special-user-checkbox').removeClass('hide');

                        $('#special-user').addClass('hide');
                        $('#normal-user').addClass('hide');

                        $('#manage-normal-user').prop('checked', false);
                        $('#manage-special-user').prop('checked', false);
                    }
                }
            }
            if ($(this).is(':checked') == false) {
                $('#normal-span').removeClass('hide');
                $('#special-span').addClass('hide');
                $('[name="user_special"]').val("").trigger("change");
                if ($('#normal-user-checkbox').length > 0) {
                    $('#special-user-checkbox').addClass('hide');
                    $('#normal-user-checkbox').removeClass('hide');

                    $('#special-user').addClass('hide');
                    $('#normal-user').addClass('hide');

                    $('#manage-normal-user').prop('checked', false);
                    $('#manage-special-user').prop('checked', false);
                }
            }
        });

        $(document).on('click', '#manage-normal-user', function () {
            $('.select2').css('width', '100%');
            //Reseting user dropdown value
            $('[name="user"]').val("").trigger('change');

            if ($(this).is(':checked') == true) {
                $('#users-panel').addClass('hide');
                $('#special-user').addClass('hide');
                $('#normal-user').removeClass('hide');
            }

            if ($(this).is(':checked') == false) {
                $('#users-panel').removeClass('hide');
                $('#normal-user').addClass('hide');
            }
        });

        $(document).on('click', '#manage-special-user', function () {
            $('.select2').css('width', '100%');
            //Reseting user dropdown value
            $('[name="user"]').val("").trigger('change');

            if ($(this).is(':checked') == true) {
                $('#users-panel').addClass('hide');
                $('#normal-user').addClass('hide');
                $('#special-user').removeClass('hide');
            }

            if ($(this).is(':checked') == false) {
                $('#users-panel').removeClass('hide');
                $('#special-user').addClass('hide');
            }
        });

        $(document).on('click', '#no_email', function () {
            if ($(this).is(':checked') == true)
            {
                $('#user-email').addClass('hide');
                $('#user-password').addClass('hide');

                $('#is-auditor').addClass('hide');
                $('#can-process-owner').addClass('hide');

                $('[name="is_auditor"]').prop('checked', false);
                $('[name="can_process_owner"]').prop('checked', false);

                $('#image-div').removeClass('col-md-6').addClass('col-md-12');
                $('#language-div').removeClass('col-md-6').addClass('col-md-12');
            }
            if ($(this).is(':checked') == false)
            {
                $('#user-email').removeClass('hide');
                $('#user-password').removeClass('hide');

                $('#is-auditor').removeClass('hide');
                $('#can-process-owner').removeClass('hide');

                $('#image-div').removeClass('col-md-12').addClass('col-md-6');
                $('#language-div').removeClass('col-md-12').addClass('col-md-6');
            }
        });

        $(document).on('click', '#special_no_email', function () {
            if ($(this).is(':checked') == true)
            {
                $('#special-user-email').addClass('hide');
                $('#special-user-password').addClass('hide');

                $('#is-special-auditor').addClass('hide');
                $('#can-special-process-owner').addClass('hide');

                $('[name="is_special_auditor"]').prop('checked', false);
                $('[name="can_special_process_owner"]').prop('checked', false);

                $('#special-image-div').removeClass('col-md-6').addClass('col-md-12');
                $('#special-language-div').removeClass('col-md-6').addClass('col-md-12');
            }
            if ($(this).is(':checked') == false)
            {
                $('#special-user-email').removeClass('hide');
                $('#special-user-password').removeClass('hide');

                $('#is-special-auditor').removeClass('hide');
                $('#can-special-process-owner').removeClass('hide');

                $('#special-image-div').removeClass('col-md-12').addClass('col-md-6');
                $('#special-language-div').removeClass('col-md-12').addClass('col-md-6');
            }
        });

        //Special user single/multiple activation
        $(document).on('click', '.special-user-check', function () {
            if ($(this).is(':checked') == false) {
                $('[data-id="special-multi-user-block"]').removeClass('hide');
                $('[data-id="special-single-user-block"]').addClass('hide');
                $(this).next('label').next('span').html("<span>{{translate('form.special_multiple_user')}}</span>");
                $('[name="special_user_check"]').val("special_multiple");
            }
            if ($(this).is(':checked') == true) {
                $('[data-id="special-multi-user-block"]').addClass('hide');
                $('[data-id="special-single-user-block"]').removeClass('hide');
                $(this).next('label').next('span').html("<span>{{translate('form.special_single_user')}}</span>");
                $('[name="special_user_check"]').val("special_single");
            }
        });

        $('form :input').change(function() {
            $('#save-btn').prop('disabled', false);
        });

        $('form').on('submit', function () {
            $('#save-btn').prop('disabled', true);
        });
        
        $(document).on('click', '#parent_id', function() {
            $('#save-btn').prop('disabled', false);
        });
    });
</script>
@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    var vm = new Vue({
        el : "#position-form",
        data : {
            branch : "{{!empty($selectedBranches) ? $selectedBranches : array_keys($branches)[0]}}",
            department : "{{isset($details) ? $details->department_id : ''}}",
            departments : {!! $department !!},
            email : "{{old('email')}}",
            image : "{{old('photo')}}",
            imageData : '',
            imageUrl : "{{asset('/image/placeholder.png')}}",
            imgsrc : "{{!empty(old('photo')) ? old('photo') : asset('/image/placeholder.png')}}",
            existing : false,
            isityou : false,
            specialEmail : "{{old('special_email')}}",
            specialImage : "{{old('special_photo')}}",
            specialImageData : '',
            specialImageUrl : "{{asset('/image/placeholder.png')}}",
            specialImgsrc : "{{!empty(old('special_photo')) ? old('special_photo') : asset('/image/placeholder.png')}}",
            specialExisting : false,
            specialIsityou : false
        },
        methods : {
            changeBranch : function () {
                axios.get("{{url('/getstarted/departments')}}/" + this.branch)
                .then(response => {
                    this.departments = response.data;
                    this.department = '';
                })
                .catch(console.log);
            },
            getImage(e) {
                if (this.email != "" && this.email.match(/^\s+/) == null && vm.image == '') {
                    axios.post("{{url('/getstarted/getimage')}}", {
                        'email' : this.email,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        if (response.data === false) {
                            vm.image = '';
                            vm.imgsrc = "{{asset('/image/placeholder.png')}}";
                        } else {
                            var img = new Image();
                            img.crossOrigin = 'anonymous';
                            img.src = response.data;
                            var canvas = document.createElement("canvas");
                            var ctx = canvas.getContext("2d");
                            canvas.width = 100;
                            canvas.height = 100;
                            img.onload = function() {
                                ctx.drawImage(img, 0, 0);
                                var url = canvas.toDataURL("image/jpeg", 1.0);
                                vm.imageData = url;
                            };
                            vm.imageUrl = response.data;
                            if (vm.image == '') {
                                setTimeout(() => {
                                    vm.image = vm.imageData;
                                }, 1000);
                                vm.imgsrc = vm.imageUrl;
                                vm.isityou = true;
                            }
                        }
                        return Promise.resolve();
                    })
                    .catch(console.log);
                }
            },
            onImageChange(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                    return;
                var reader = new FileReader();
                var vm = this;
                reader.onload = (e) => {
                    vm.image = e.target.result;
                };
                reader.readAsDataURL(files[0]);
                this.imgsrc = "{{asset('/image/placeholder.png')}}";
            },
            confirmIt : function (val) {
                if (val == 'no') {
                    this.image = '';
                    this.imgsrc = "{{asset('/image/placeholder.png')}}";
                }
                this.isityou = false;
            },
            specialGetImage(m) {
                if (this.specialEmail != "" && this.specialEmail.match(/^\s+/) == null && vm.specialImage == '') {
                    axios.post("{{url('/getstarted/getimage')}}", {
                        'email' : this.specialEmail,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        if (response.data === false) {
                            vm.specialImage = '';
                            vm.specialImage = "{{asset('/image/placeholder.png')}}";
                        } else {
                            var img = new Image();
                            img.crossOrigin = 'anonymous';
                            img.src = response.data;
                            var canvas = document.createElement("canvas");
                            var ctx = canvas.getContext("2d");
                            canvas.width = 100;
                            canvas.height = 100;
                            img.onload = function() {
                                ctx.drawImage(img, 0, 0);
                                var url = canvas.toDataURL("image/jpeg", 1.0);
                                vm.specialImageData = url;
                            };
                            vm.specialImageUrl = response.data;
                            if (vm.specialImage == '') {
                                setTimeout(() => {
                                    vm.specialImage = vm.specialImageData;
                                }, 1000);
                                vm.specialImgsrc = vm.specialImageUrl;
                                vm.specialIsityou = true;
                            }
                        }
                        return Promise.resolve();
                    })
                    .catch(console.log);
                }
            },
            specialOnImageChange(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                    return;
                var reader = new FileReader();
                var vm = this;
                reader.onload = (e) => {
                    vm.specialImage = e.target.result;
                };
                reader.readAsDataURL(files[0]);
                this.specialImgsrc = "{{asset('/image/placeholder.png')}}";
            },
            specialConfirmIt : function (val) {
                if (val == 'no') {
                    this.specialImage = '';
                    this.specialImgsrc = "{{asset('/image/placeholder.png')}}";
                }
                this.specialIsityou = false;
            }
        }
    });
</script>