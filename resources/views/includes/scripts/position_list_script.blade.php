@include('includes.position_tree_assets')
@stack('styles')
@stack('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $('#loader').addClass('hide');
    $('.jqxtree').removeClass('hide');    
    $('.jqxtree').jqxTree({
        height: '500px',
        width: '100%',
        theme: 'energyblue',
        allowDrag: true,
        allowDrop: true
    });
    $('.jqxtree').on('dragEnd', function (event){
        var element = $(event.args.originalEvent.element).parents('li:first');
        var child = element.attr('id');
        var newParent = element.parents('li').attr('id');
        var newParentIfAssistant = element.parents('li').attr('data-assistant');
        var newParentIfSpecial = element.parents('li').attr('data-special');
        var ifThisTopDepartment = element.attr('data-topdept');
        var ifThisAssistant = element.attr('data-assistant');
        var ifThisSpecial = element.attr('data-special');
        var ifNewParentTopDepartment = element.parents('li').attr('data-topdept');
        var ifNewParentHasAssistant = element.parents('li').attr('data-childassistant');

        if(ifThisTopDepartment == 1 && ifNewParentTopDepartment == 0) {
            //If top department position merged into a non top department position
            new PNotify({
                title: "{{translate('alert.not_assign_top_to_following_position')}}",
                text: "",
                addclass: 'stack_top_right',
                type: 'error',
                width: '290px',
                delay: 2000
            });
            setTimeout(function() {
                location.reload();
            }, 2000);
        
        } else if(newParentIfAssistant == 1) {
            //If destination position is a assistant position
            new PNotify({
                title: "{{translate('alert.not_assign_general_to_assistant_position')}}",
                text: "",
                addclass: 'stack_top_right',
                type: 'error',
                width: '290px',
                delay: 2000
            });
            setTimeout(function() {
                location.reload();
            }, 2000);
        
        } else if(ifThisSpecial == 1 && newParentIfAssistant == 1) {
            //If destination position is a assistant position
            new PNotify({
                title: "{{translate('alert.not_assign_special_to_assistant_position')}}",
                text: "",
                addclass: 'stack_top_right',
                type: 'error',
                width: '290px',
                delay: 2000
            });
            setTimeout(function() {
                location.reload();
            }, 2000);
        
        } else if(ifThisAssistant == 1 && newParentIfSpecial == 1) {
            //If destination position is a assistant position
            new PNotify({
                title: "{{translate('alert.not_assign_assistant_to_special_position')}}",
                text: "",
                addclass: 'stack_top_right',
                type: 'error',
                width: '290px',
                delay: 2000
            });
            setTimeout(function() {
                location.reload();
            }, 2000);
        
        } else if(newParentIfSpecial == 1) {
            //If destination position is a special position
            new PNotify({
                title: "{{translate('alert.not_assign_general_to_special_position')}}",
                text: "",
                addclass: 'stack_top_right',
                type: 'error',
                width: '290px',
                delay: 2000
            });
            setTimeout(function() {
                location.reload();
            }, 2000);
        
        } else if(ifThisAssistant == 1 && ifNewParentHasAssistant == 1) {
            //If destination position is a special position
            new PNotify({
                title: "{{translate('alert.cant_add_multiple_assistant')}}",
                text: "",
                addclass: 'stack_top_right',
                type: 'error',
                width: '290px',
                delay: 2000
            });
            setTimeout(function() {
                location.reload();
            }, 2000);
        
        } else {
            //Position assignment proceeded
            $.ajax({
                url: "{{url('/position/rearrange/')}}/" + child + "/" + newParent,
                type: "GET",
                success: function(data) {
                    if(data === true) {
                        new PNotify({
                            title: "{{translate('alert.position_rearranged')}}",
                            text: "",
                            addclass: 'stack_top_right',
                            type: 'success',
                            width: '290px',
                            delay: 2000
                        });
                    }
                    if (data === false) {
                        new PNotify({
                            title: "{{translate('alert.position_has_parent')}}",
                            text: "",
                            addclass: 'stack_top_right',
                            type: 'error',
                            width: '290px',
                            delay: 2000
                        });
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }
                }
            });
        }
    });
});
</script>