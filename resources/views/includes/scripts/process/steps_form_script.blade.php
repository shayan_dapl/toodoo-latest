<script type="text/javascript">
    $(document).ready(function() {
        //Adding dynamic roles segment for process step in process/steps_form.blade.php
        $(document).on('click', '.roles-add', function() {
            var id = $(this).data('id');
            if ($('[name="roles['+id+'][position]"]').val() == '') {
                $('[name="roles['+id+'][position]"]').focus();
            } else {
                var errors = 0;
                if ($('#position-id-' + id).val() != "" && $('#checkboxExample' + id + '1').is(':checked') === false && $('#checkboxExample' + id + '2').is(':checked') === false && $('#checkboxExample' + id + '3').is(':checked') === false) {
                    $('#error-' + id).text("{{translate('alert.choose_responsibility')}}");
                    errors++;
                } else {
                    $('#error-' + id).text('');
                }
                if (errors == 0) {
                    var nextId = id + 1;
                    var nextEl = '<br clear="all">\
                    <div class="row" id="roles-pos-' + nextId + '">\
                        <label for="inputStandard" class="col-md-3 col-md-offset-2 col-sm-2 control-label">\
                            {{translate("form.position")}}\
                        </label>\
                        \
                        <div class="col-md-4 col-xs-10">\
                            <div class="section">\
                                <label class="field select">\
                                    <select id="position-id-' + nextId + '" class="positions" name="roles[' + nextId + '][position]" data-sl="' + nextId + '">\
                                        @if(isset($position))\
                                        @foreach($position as $key=>$val)\
                                        <option value="{{$key}}">{{$val}}</option>\
                                        @endforeach\
                                        @endif\
                                    </select>\
                                    <i class="arrow"></i>\
                                </label>\
                            </div>\
                        </div>\
                        \
                        <div class="col-md-5">\
                            <button type="button" class="btn btn-system roles-add" data-id="' + nextId + '"><i class="fa fa-plus"></i></button>\
                            <span class="text-danger fs12" id="error-' + nextId + '"></span>\
                        </div>\
                        \
                        <div class="col-md-3 col-md-offset-3 process-roles-extended">\
                            <div class="checkbox-custom checkbox-system mb5 side-extended">\
                                <input id="checkboxExample' + nextId + '1" name="roles[' + nextId + '][responsibility][]" value="1" data-sl="' + nextId + '" type="checkbox">\
                                <label for="checkboxExample' + nextId + '1">{{translate("form.roles_responsible")}}</label>\
                            </div>\
                            <div class="checkbox-custom checkbox-system mb5 side-extended">\
                                <input id="checkboxExample' + nextId + '2" name="roles[' + nextId + '][responsibility][]" value="3" data-sl="' + nextId + '" type="checkbox">\
                                <label for="checkboxExample' + nextId + '2">{{translate("form.roles_consulted")}}</label>\
                            </div>\
                            <div class="checkbox-custom checkbox-system mb5 side-extended">\
                                <input id="checkboxExample' + nextId + '3" name="roles[' + nextId + '][responsibility][]" value="4" data-sl="' + nextId + '" type="checkbox">\
                                <label for="checkboxExample' + nextId + '3">{{translate("form.roles_informed")}}</label>\
                            </div>\
                        </div>\
                        <input name="roles[' + nextId + '][id]" value="" type="hidden">\
                    </div><br clear="all">';
                    $(this).removeClass("btn-system").removeClass("roles-add").addClass("btn-danger").addClass("roles-remove").html('<i class="fa fa-minus"></i>');
                    $("#roles-pos-" + id).before(nextEl);
                }
            }
        });

        //Checking for same role data on process/steps_form.blade.php
        var previousVal;
        var previousTxt;
        $(document).on('focus', '.positions', function () {
            previousVal = $(this).val();
            previousTxt = $(this).find('option:selected').text();
        }).on('change', '.positions', function() {
            var val = $(this).val();
            var sl = $(this).data('sl');
            var allSelectedPositions = $('.positions').map(function() {
                if ($(this).val() != "") {
                    return $(this).val();
                }
            }).get().join();
            var selectedPosArr = allSelectedPositions.split(",");
            var sameValCounter = 0;
            $.each(selectedPosArr, function(index, value) {
                if (val == value) {
                    sameValCounter++;
                }
            });
            if (sameValCounter > 1) {
                new PNotify({
                    title: "{{translate('alert.not_allowed')}}",
                    text: "{{translate('alert.please_try_again')}}",
                    addclass: 'stack_top_right',
                    type: 'error',
                    width: '290px',
                    delay: 2000
                });
                $(this).val(previousVal);
            }
        });

        //Removing dynamic roles segment for process step in process/steps_form.blade.php
        $(document).on('click', '.roles-remove', function() {
            var id = $(this).data('id');
            var row_id = $('[name="roles[' + id + '][id]"]').val();
            var processId = $(this).data('processid');
            var stepId = $(this).data('stepid');
            var positionId = $(this).data('positionid');
            if (row_id != "") {
                $.ajax({
                    url: "{{url('/process-step/roles/remove-roles')}}/" + processId + "/" + stepId + "/" + positionId,
                    async: true,
                    type: "GET",
                    cache: false,
                    success: function(data) {
                        new PNotify({
                            title: "{{translate('alert.role')}}",
                            text: "{{translate('alert.removed_successfully')}}",
                            addclass: 'stack_top_right',
                            type: 'success',
                            width: '290px',
                            delay: 2000
                        });
                        $("#roles-pos-" + id).prev('br').remove();
                        $("#roles-pos-" + id).remove();
                    }
                });
            } else {
                $("#roles-pos-" + id).prev('br').remove();
                $("#roles-pos-" + id).remove();
            }
        });

        $.fn.uncheckPos = function (segment, notAllowed) {
            var checker = 0;
            $('.positions').each(function () {
                var row = $(this).data('sl');
                if (row !== notAllowed && $('#checkboxExample' + row + segment).is(':checked') === true) {
                    $('#checkboxExample' + row + segment).prop('checked', false);
                    checker++;
                }
            });
            if (checker > 0) {
                new PNotify({
                    title: "{{translate('alert.everyone_rci_notified')}}",
                    text: "",
                    addclass: 'stack_top_right',
                    type: "error",
                    width: '290px',
                    delay: 1000
                });
            }
        }

        $.fn.iteratePos = function () {
            $('input[type=checkbox]').each(function () {
                var rowNo = $(this).data('sl');
                if ($('#position-id-' + rowNo).val() != "" && $('#position-id-' + rowNo).val() == $('#everyone-pos').val()) {
                    for (var inc = 1; inc <= 3; inc++) {
                        if ($('#checkboxExample' + rowNo + inc).is(':checked') === true) {
                            $.fn.uncheckPos(inc, rowNo);
                        }
                    }
                }
            });
        }

        $(document).on('change', '.positions', function () {
            $.fn.iteratePos();
        });

        $(document).on('click', 'input[type=checkbox]', function () {
            $.fn.iteratePos();
        });

        $('form').submit(function () {
            var errors = 0;
            $(this).find('input[type=checkbox]').each(function () {
                var rowNo = $(this).data('sl');
                if ($('#position-id-' + rowNo).val() != "" && $('#checkboxExample' + rowNo + '1').is(':checked') === false && $('#checkboxExample' + rowNo + '2').is(':checked') === false && $('#checkboxExample' + rowNo + '3').is(':checked') === false) {
                    $('#error-' + rowNo).text("{{translate('alert.choose_responsibility')}}");
                    errors++;
                } else {
                    $('#error-' + rowNo).text('');
                }
            });

            if (errors == 0) {
                return true;
            } else {
                return false;
            }
        });
    });
</script>