<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '#make-sub-process', function () {
            if ($(this).is(':checked') == true)
            {
                $('#parent-process').removeClass('hide');
                $('#process-category').addClass('hide');
                $('#parentProcess').prop('required', true);
                $('#category_id').prop('required', false);
            }
            if ($(this).is(':checked') == false)
            {
                $('#parent-process').addClass('hide');
                $('#process-category').removeClass('hide');
                $('#parentProcess').prop('required', false);
                $('#category_id').prop('required', true);
            }
        });
    });
</script>