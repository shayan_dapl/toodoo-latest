<script type="text/javascript">
    $(document).ready(function() {
        //Adding functions input field dynamically on user_form.blade.php and profile.blade.php
        $(document).on('click', '.functions', function() {
            var row = $(this).data("row");
            if ($("#position-"+row).val() != 0) {
                if ($("#position-"+row).hasClass('error')) {
                    $("#position-"+row).removeClass('error');
                }
                var nextId = +row+1;
                var nextEl = '\
                <div class="row repeter" id="output-'+nextId+'">\
                    <div class="col-md-5 col-sm-5 col-xs-12">\
                        <label class="field select">\
                            <select data-id="'+nextId+'" class="position" id="position-'+nextId+'" name="functions[position][]">\
                                @if(!empty($positions))\
                                @foreach($positions as $id => $name) \
                                    <option value="{{$id}}">{{$name}}</option>\
                                @endforeach\
                                @endif\
                            </select>\
                            <i class="arrow"></i>\
                        </label>\
                    </div>\
                    <div class="col-md-6 col-sm-6 col-xs-10">\
                        <h3 id="holdername-'+nextId+'">{{translate("form.parent_position")}}</h3>\
                        <input id="parentpos-'+nextId+'" name="functions[parent][]" value="0" type="hidden">\
                    </div>\
                    <div class="col-sm-1 col-xs-2">\
                        <button type="button" class="btn btn-system functions" data-row="'+nextId+'" data-id="">\
                            <i class="fa fa-plus"></i>\
                        </button>\
                    </div>\
                </div>';
                $("#output-"+row).after(nextEl);
                $("button[data-row="+row+"]").removeClass('btn-system').removeClass('functions').addClass('btn-danger btn-sm').addClass('functions-del').html('<i class="fa fa-minus"></i>');
            } else {
                if ($("#position-"+row).val() == 0) {
                    $("#position-"+row).addClass('error');
                    return false;
                }
            }
        });
        
        //Removing position/function row on user_form.blade.php and profile.blade.php
        $(document).on('click', '.functions-del', function() {
            var confirmed = confirm("{{translate('alert.are_you_sure')}}");
            if (confirmed == true) {
                var id = $(this).data('id');
                var row = $(this).data('row');
                if (id == "") {
                    $('#output-'+row).remove();
                } else {
                    $.ajax({
                        url: "{{url('/user/structure/remove')}}/"+id,
                        type: "GET",
                        success: function(data) {
                            if(data == "yes") {
                                $('#output-'+row).remove();
                                location.reload();
                            }
                            if(data == "no") {
                                new PNotify({
                                    title: "{{translate('alert.user_position')}}",
                                    text: "{{translate('alert.cannot_remove_child_exist')}}",
                                    addclass: 'stack_top_right',
                                    type: 'error',
                                    width: '290px',
                                    delay: 2000
                                });
                            }   
                        }
                    });
                }
            }
        });

        //getting reporting head name for particular position on user_form.blade.php
        $(document).on('change', '.position', function() {
            var row = $(this).data('id');
            var val = $(this).val();
            if (val == 0) {
                $("#position-"+row+" option[value='0']").prop("selected", true);
                $("#holdername-"+row).html("{{translate('form.parent_position')}}");
                $("#parentpos-"+row).val("");
            }
            if (val != 0) {
                $.ajax({
                    url: "{{url('/user/find_immidiate_parent')}}/"+val,
                    async: true,
                    type: "GET",
                    cache: false,
                    success: function(data) {   
                        if (data == "no_parent") {
                            new PNotify({
                                title: "{{translate('alert.user_not_exist')}}",
                                text: "{{translate('alert.on_parent_position')}}",
                                addclass: 'stack_top_right',
                                type: 'error',
                                width: '290px',
                                delay: 2000
                            });
                            $("#position-"+row+" option[value='0']").prop("selected", true);
                            $("#holdername-"+row).html("{{translate('form.parent_position')}}");
                            $("#parentpos-"+row).val("");
                        } else {
                            $("#holdername-"+row).html(data.name);
                            $("#parentpos-"+row).val(data.id);
                        }
                    }
                });
            }
        });

        //Remove border color if user chooses an option after selecting assistant position
        $(document).on('change', '[name="function[]"]', function() {
            //Knowing that user worked on this section
            $("#org-pos-activate").val("1");
            if ($(this).val() != "0") {
                $(this).css("border-color", "");
            }
        });
    });
</script>