<script type="text/javascript">
    function showracimap(val)
    {
        if (val != "")
        {
            document.getElementById("map-div").innerHTML = '<i class="fa fa-cog fa-spin fa-5x text-system"></i>';
            window.location = "{{url('/process/racimap/')}}/" + val;
        }
    }
    $(document).ready(function() {
        $(document).on('click', '.raci', function () {
            $('#position-name').html($(this).html());
            $('.raci-result').html('<i class="fa fa-cog fa-spin fa-5x text-system"></i>');
            var posname = $(this).data('name');
            var processid = $(this).data('process');
            $.ajax({
                url: "{{url('/process/racimap/details')}}/" + processid + "/" + posname,
                async: true,
                type: "GET",
                cache: false,
                success: function (data) {
                    $('.raci-result').html(data);
                }
            });
        });
    });
</script>