@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    new Vue({
        el: '#rating',
        data: {
            types : [
                { name: "{{translate('table.no_data')}}" }
            ],
            name : '',
            nameClass : 'gui-input',
            hid : '',
            nodata : 1,
            disable : false
        },
        created() {
            this.fetchData();
        },
        watch : {
            name : function (val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.nameClass = 'gui-input';
                }
            }
        },
        methods: {
            fetchData: function() {
                axios.get("{{url('/rating/action-type/data')}}")
                .then(response => {
                    if(response.data.length > 0) {
                        this.types = response.data;
                        this.nodata = 0;
                    }
                })
                .then(() => {
                    setTimeout(() => {
                        $('#datatable2').DataTable();
                    }, 1000);
                });
            },
            saveData: function() {
                let errors = 0;
                if(this.name == "" || this.name.match(/^\s+/) != null) {
                    this.nameClass = 'gui-input error';
                    errors = 1;
                }
                if (errors == 0) {
                    this.disable = true;
                    axios.post("{{ url('/rating/action-type/save') }}", { 
                        'type' : this.name,
                        'hid' : this.hid,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        let title = (response.data === false) ? "{{translate('alert.already_exist')}}" : "{{translate('words.success_message')}}";
                        let type = (response.data === false) ? "error" : "success";
                        this.disable = false;
                        new PNotify({
                            title: title,
                            text: "",
                            addclass: 'stack_top_right',
                            type: type,
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        $('#datatable2').DataTable().destroy();
                        this.name = this.hid = '';
                        this.fetchData();
                    })
                    .catch(console.log);
                }
            },
            editData: function(type) {
                this.name = type.type;
                this.hid = type.id;
            },
            removeData: function(val) {
                var confirmed = confirm("{{translate('alert.are_you_sure')}}");
                if (confirmed == true) {
                    axios.post("{{ url('/rating/action-type/remove') }}", {
                        'id' : val,
                        '_token' : "{{csrf_token()}}"
                    }).then(response => {
                        let title = (response.data === false) ? "{{translate('words.remove')}} {{translate('alert.not_allowed')}}" : "{{translate('words.success_message')}}";
                        let type = (response.data === false) ? "error" : "success";
                        this.disable = false;
                        new PNotify({
                            title: title,
                            text: "",
                            addclass: 'stack_top_right',
                            type: type,
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        this.nodata = 1;
                        $('#datatable2').DataTable().destroy();
                        this.name = this.hid = '';
                        this.fetchData();
                    })
                    .catch(console.log);
                }
            }
        }
    });
</script>