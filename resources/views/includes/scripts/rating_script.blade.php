@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    new Vue({
        el: '#rating',
        data: {
            ratings : [
                { name: "{{translate('table.no_data')}}" }
            ],
            name : '',
            nameClass : 'gui-input',
            desc : '',
            hid : '',
            nodata : 1,
            disable : false
        },
        created() {
            this.fetchData();
        },
        watch : {
            name : function (val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.nameClass = 'gui-input';
                }
            }
        },
        methods: {
            fetchData: function() {
                axios.get("{{url('/rating/data')}}")
                .then(response => {
                    if(response.data.length > 0) {
                        this.ratings = response.data;
                        this.nodata = 0;
                    }
                })
                .then(() => {
                    setTimeout(() => {
                        $('#datatable2').DataTable();
                    }, 1000);
                });
            },
            saveData: function() {
                let errors = 0;
                if(this.name == "" || this.name.match(/^\s+/) != null) {
                    this.nameClass = 'gui-input error';
                    errors = 1;
                }
                if (errors == 0) {
                    this.disable = true;
                    axios.post("{{ url('/rating/') }}", { 
                        'name' : this.name,
                        'desc' : this.desc,
                        'hid' : this.hid
                    })
                    .then(response => {
                        let title = (response.data === false) ? "{{translate('words.rating')}}" : "{{translate('words.success_message')}}";
                        let text = (response.data === false) ? "{{translate('alert.already_exist')}}" : "";
                        let type = (response.data === false) ? "error" : "success";
                        this.disable = false;
                        new PNotify({
                            title: title,
                            text: text,
                            addclass: 'stack_top_right',
                            type: type,
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        $('#datatable2').DataTable().destroy();
                        this.name = this.hid = this.desc = '';
                        this.fetchData();
                    })
                    .catch(console.log);
                }
            },
            editData: function(rat) {
                this.name = rat.name;
                this.desc = rat.description;
                this.hid = rat.id;
            },
            removeData: function(val) {
                var confirmed = confirm("{{translate('alert.are_you_sure')}}");
                if (confirmed == true) {
                    axios.post("{{ url('/rating/remove') }}", {
                        'id' : val,
                        '_token' : "{{csrf_token()}}"
                    }).then(response => {
                        let title = (response.data === false) ? "{{translate('words.branch')}}" : "{{translate('words.success_message')}}";
                        let text = (response.data === false) ? "{{translate('words.remove')}} {{translate('alert.not_allowed')}}" : "";
                        let type = (response.data === false) ? "error" : "success";
                        this.disable = false;
                        new PNotify({
                            title: title,
                            text: text,
                            addclass: 'stack_top_right',
                            type: type,
                            width: '290px',
                            delay: 2000
                        });
                    })
                    .then(() => {
                        this.nodata = 1;
                        $('#datatable2').DataTable().destroy();
                        this.name = this.hid = this.desc = '';
                        this.fetchData();
                    })
                    .catch(console.log);
                }
            }
        }
    });
</script>