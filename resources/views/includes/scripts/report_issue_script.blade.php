
<script type="text/javascript">
	$(document).ready(function() {
        var base64 = getBase64Image(document.getElementById("imageid"));
        $.fn.dataTableExt.sErrMode = 'throw'; 
        $('#datatable3').dataTable({ 
            "order": [],
            "language": {
                "lengthMenu": "{{translate('pagination.display')}} _MENU_ {{translate('pagination.record_per_page')}}",
                "zeroRecords": "{{translate('pagination.nothing_found')}}",
                "info": "{{translate('pagination.showing_page')}} _PAGE_ {{translate('pagination.of')}} _PAGES_",
                "infoEmpty": "{{translate('pagination.no_records')}}",
                "infoFiltered": "({{translate('pagination.filterd_from')}} _MAX_ {{translate('pagination.total_records')}})",
                "oPaginate" : {
                    "sFirst": "{{translate('pagination.first')}}",
                    "sLast": "{{translate('pagination.last')}}",
                    "sNext": "{{translate('pagination.next')}}",
                    "sPrevious": "{{translate('pagination.previous')}}"
                },
                "sSearch" : "{{translate('pagination.search')}}"
            },
            "filterDropDown": {                                       
                "columns": [
                    { 
                        "idx": 7
                    }
                ],
                "bootstrap": true
            },
            "dom": 'Bfrtip',
            "columnDefs": [{
                "targets": 8,
                "orderable": false
            }],
            buttons: [{
                text: "<i class='fa fa-print'></i>",
                extend: "pdfHtml5",
                title: function () { return "{{translate('words.overview_actions')}}"; },
                orientation: 'landscape',
                pageSize: 'A4',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7]
                },
                customize: function(doc) {
                    console.log(doc.content);
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'right',
                        image: base64
                    });
                    doc.content[2].table.widths = ["*", "*", "*","*", "*", "*", "*", "*"];
                    doc.defaultStyle.alignment = 'left';
                    doc.styles.tableHeader.alignment = 'left';
                }
            }]
        }); 

        $(document).on('click', '.add-action', function() {
            if($('[name="issue"]').val().match(/^\s+/) == null || $('[name="action"]').val().match(/^\s+/) == null ) {
                return true;
            }
            if($('[name="reference"]').val().match(/^\s+/) != null) {
                $('[name="reference"]').css('box-shadow', 'red 0px 0px 1.5px 1px');
                $('[name="reference"]').focus();
                return false;
            } else {
                $('[name="reference"]').css('box-shadow', '');
            }
        });

        $(document).on('click', '#get-issue-fixing-data', function() {
            var id = $(this).data('id');
            $('#fixing-detail-data').html('<i class="fa fa-cog fa-spin fa-3x text-system" style="margin-left: 47%"></i>');
            $.ajax({
                "url" : "{{url('/audit/report-issue/data/')}}/" + id,
                "type" : "GET",
                success: function(data) {
                    $('#fixing-detail-data').html(data);
                }
            });
        });

        $(".report_issue_deadline").datepicker(dtOption);

        $("#source_type").change(function () {
            var sourceTypeId = $(this).val();
            $.ajax({
                url: "{{url('/audit/get-sourcetype-access')}}/" + sourceTypeId,
                type: "GET",
                success: function (data) {
                    $('#teamNotify').html(data);
                }
            });
        });
    });
    
    function getBase64Image(img) {
        var canvas = document.createElement("canvas");
        canvas.width = 100;
        canvas.height = 100;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        return dataURL;
    }
</script>