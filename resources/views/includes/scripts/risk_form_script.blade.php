<script type="text/javascript">
	$(document).ready(function() {
		$(".DatePicker4").datepicker(dtOption);

		$("#level").on('change', function(){
			var actions = $('#defined-actions').val();
			if(actions != "") {
				var actionFound = actions.search($(this).val());
				if(actionFound >= 0 && $(this).val() != '') {
					$("#action-segment").removeClass("hide");
					$("#action").prop("required", true);
					$("#who").prop("required", true);
					$("#deadline").prop("required", true);
					$("#has-action").val("has");
				} else {
					$("#action-segment").addClass("hide");
					$("#action").val("");
					$("#deadline").val("");
					$("#action").prop("required", false);
					$("#who").prop("required", false);
					$("#deadline").prop("required", false);
					$("#has-action").val("");
				}
			}
		});
	});
</script>