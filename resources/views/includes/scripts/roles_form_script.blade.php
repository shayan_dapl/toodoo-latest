<script type="text/javascript">
    $(document).ready(function() {
        $(document).on("change", "#position_id", function () {
            $(".roles").prop("checked", false);
        });

        $(document).on("click", ".roles", function () {
            if ($(this).is(":checked") == true)
            {
                var val = $(this).val();
                var sl = $(this).data("sl");
                var position = $('#position-id-' + sl).val();

                if (val == 2)
                {
                    if (position != "")
                    {
                        $(".roles").prop('checked', false);
                        $(this).prop('checked', true);
                    }
                    else
                    {
                        $('#position-id-' + sl).focus();
                        $(this).prop('checked', false);
                    }
                }
            }
        });
    });
</script>