@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
	Vue.component('list-table', {
		props : ['list'],
		template : `
			<table class="table table-striped table-hover" id="parameter-table" cellspacing="0" width="100%">
	            <thead>
	                <tr>
	                    <th>{{translate('words.ratings')}}</th>
	                    <th>{{translate('form.description')}}</th>
	                    <th>{{translate('table.operations')}}</th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr v-for="each in list">
	                    <td>@{{each.rating}}</td>
	                    <td>@{{each.description}}</td>
	                    <td>
	                        <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="editData(each)" title="{{translate('words.edit')}}">
	                            <span class="text-success fa fa-pencil"></span>
	                        </a>
	                        <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="removeData(each.id)" title="{{translate('words.remove')}}">
	                            <span class="text-danger fa fa-trash"></span>
	                        </a>
	                    </td>
	                </tr>
	            </tbody>
	        </table>
		`,
		methods : {
			editData : function (each) {
				vm.name = each.rating;
				vm.description = each.description;
				vm.hid = each.id;
			},
			removeData : function (id) {
				var confirmed = confirm("{{translate('alert.are_you_sure')}}");
	            if (confirmed == true) {
	                axios.get("{{url('/supplier/ratings/remove')}}/" + id)
	                .then(response => {
	                    new PNotify({
	                        title : (response.data === false) ? "{{translate('words.faliure_message')}}" : "{{translate('words.success_message')}}",
	                        text : "",
	                        addclass : 'stack_top_right',
	                        type : (response.data === false) ? "error" : "success",
	                        width : '290px',
	                        delay : 2000
	                    });
	                })
	                .then(() => {
	                    vm.fetchData();
	                });
	            }
			}
		}
	});

	var vm = new Vue({
		el : '#supplier',
		data : {
			list : [],
			name : '',
			nameClass : 'form-control',
			nameError : '',
			description : '',
			descriptionClass : 'form-control',
			hid : '',
			enabled : false
		},
		created () {
			this.fetchData();
		},
		watch : {
			list : function (newVal, oldVal) {
				if (newVal != oldVal && $.fn.dataTable.isDataTable('#parameter-table')) {
					$('#parameter-table').DataTable().destroy();
				}
				setTimeout(() => {
					$('#parameter-table').DataTable({
				        "language": {
				            "lengthMenu": "{{translate('pagination.display')}} _MENU_ {{translate('pagination.record_per_page')}}",
				            "zeroRecords": "{{translate('pagination.nothing_found')}}",
				            "info": "{{translate('pagination.showing_page')}} _PAGE_ {{translate('pagination.of')}} _PAGES_",
				            "infoEmpty": "{{translate('pagination.no_records')}}",
				            "infoFiltered": "({{translate('pagination.filterd_from')}} _MAX_ {{translate('pagination.total_records')}})",
				            "oPaginate" : {
				                "sFirst": "{{translate('pagination.first')}}",
				                "sLast": "{{translate('pagination.last')}}",
				                "sNext": "{{translate('pagination.next')}}",
				                "sPrevious": "{{translate('pagination.previous')}}"
				            },
				            "sSearch" : "{{translate('pagination.search')}}"
				        }
				    });
				}, 1000);
			},
			name : function (val) {
				this.nameError = '';
				if (val != "" && val.toString().match(/^\s+/) == null && this.name.toString().match(/\w+/) != null) {
					this.nameClass = 'form-control';
				}
			},
			description : function (val) {
				if (val != "" && val.match(/^\s+/) == null) {
					this.descriptionClass = 'form-control';
				}
			}
		},
		methods : {
			limitText : function () {
				if ((this.name !== '') && (this.name.indexOf('.') === -1)) {
			        this.name = Math.max(Math.min(this.name, 9), 1);
			    }
			},
			fetchData : function () {
				axios.get("{{url('/supplier/ratings/list/data')}}")
				.then(response => {
					return new Promise((resolve, reject) => {
						this.list = response.data;
						resolve();
					});
				})
				.catch(console.log);
			},
			saveData : function () {
				var errors = 0;
				if (this.name == "" || this.name.toString().match(/^\s+/) != null || this.name.toString().match(/\w+/) == null) {
					this.nameClass = 'form-control error';
					errors = 1;
				}
				if (this.description == "" || this.description.match(/^\s+/) != null) {
					this.descriptionClass = 'form-control error';
					errors = 1;
				}
				if (errors == 0) {
					this.enabled = true;
					axios.post("{{url('/supplier/ratings/save')}}", {
						'rating' : this.name,
						'description' : this.description,
						'id' : this.hid,
						'_token' : "{{csrf_token()}}"
					})
					.then(response => {
						this.enabled = false;
						new PNotify({
	                        title : (response.data === false) ? "{{translate('words.faliure_message')}}" : "{{translate('words.success_message')}}",
	                        text : "",
	                        addclass : 'stack_top_right',
	                        type : (response.data === false) ? "error" : "success",
	                        width : '290px',
	                        delay : 2000
	                    });
	                }).then(() => {
	                	this.name = this.description = this.hid = '';
	                    this.fetchData();
	                })
	                .catch(error => {
						this.nameError = error.response.data.rating[0];
						this.nameClass = "form-control error";
						this.enabled = false;
					});
				}
			}
		}
	});
</script>