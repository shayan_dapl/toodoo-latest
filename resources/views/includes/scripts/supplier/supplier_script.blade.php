@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
	Vue.component('list-status', {
		props : ['type', 'id', 'segment', 'value'],
		template : `
			<div class="switch switch-system switch-sm round switch-modified">
			  	<input :id="type+segment" :checked="value == 1" ref="status" type="checkbox" @click="changeStatus(type, id)">
			  	<label :for="type+segment"></label>
			</div>
		`,
		methods : {
			changeStatus : function (type, id) {
				var isActive = 0;
				if (this.$refs.status.checked == true) {
					isActive = 1;
				}
				axios.post("{{url('/supplier/status')}}", {
					'type' : type,
					'id' : id,
					'make' : isActive,
					'_token' : "{{csrf_token()}}"
				})
				.then(response => {
					new PNotify({
						title : (response.data === false) ? "{{translate('words.faliure_message')}}" : "{{translate('words.success_message')}}",
						text : "",
						addclass : 'stack_top_right',
						type : (response.data === false) ? "error" : "success",
						width : '290px',
						delay : 2000
					});
				})
				.catch(console.log);
			}
		}
	});

	Vue.component('list-table', {
		props : ['list'],
		data : function () {
			return {
				evaluated : 'evaluated',
				active : 'active'
			}
		},
		template : `
			<table class="table table-striped table-hover" id="supplier-table" cellspacing="0" width="100%">
	            <thead>
	                <tr>
	                    <th>{{translate('form.supplier_name')}}</th>
	                    <th>{{translate('form.product_services')}}</th>
	                    <th>{{translate('form.evaluated')}}</th>
	                    <th>{{translate('words.active')}}</th>
	                    <th>{{translate('words.comment')}}</th>
	                    <th>{{translate('table.operations')}}</th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr v-for="each in list">
	                    <td>@{{each.name}}</td>
	                    <td>@{{each.service}}</td>
	                    <td><list-status :type="evaluated" :id="each.id" :segment="each.id" :value="each.is_eval" /></td>
	                    <td><list-status :type="active" :id="each.id" :segment="each.id" :value="each.is_active" /></td>
	                    <td>@{{each.comment}}</td>
	                    <td>
	                        <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="editData(each)" title="{{translate('words.edit')}}">
	                            <span class="text-success fa fa-pencil"></span>
	                        </a>
	                        <a href="javascript:void(0)" class="btn btn-default btn-sm" @click="removeData(each.id)" title="{{translate('words.remove')}}">
	                            <span class="text-danger fa fa-trash"></span>
	                        </a>
	                    </td>
	                </tr>
	            </tbody>
	        </table>
		`,
		methods : {
			editData : function (each) {
				vm.name = each.name;
				vm.service = each.service;
				vm.comment = each.comment;
				vm.hid = each.id;
			},
			removeData : function (id) {
				var confirmed = confirm("{{translate('alert.are_you_sure')}}");
	            if (confirmed == true) {
	                axios.get("{{url('/supplier/remove')}}/" + id)
	                .then(response => {
	                    new PNotify({
	                        title : (response.data === false) ? "{{translate('words.faliure_message')}}" : "{{translate('words.success_message')}}",
	                        text : "",
	                        addclass : 'stack_top_right',
	                        type : (response.data === false) ? "error" : "success",
	                        width : '290px',
	                        delay : 2000
	                    });
	                })
	                .then(() => {
	                    vm.fetchData();
	                });
	            }
			}
		}
	});

	var vm = new Vue({
		el : '#supplier',
		data : {
			list : [],
			name : '',
			nameClass : 'form-control',
			nameError : '',
			service : '',
			serviceClass : 'form-control',
			comment : '',
			hid : '',
			enabled : false
		},
		created () {
			this.fetchData();
		},
		watch : {
			list : function (newVal, oldVal) {
				if (newVal != oldVal && $.fn.dataTable.isDataTable('#supplier-table')) {
					$('#supplier-table').DataTable().destroy();
				}
				setTimeout(() => {
					$('#supplier-table').DataTable({
				        "language": {
				            "lengthMenu": "{{translate('pagination.display')}} _MENU_ {{translate('pagination.record_per_page')}}",
				            "zeroRecords": "{{translate('pagination.nothing_found')}}",
				            "info": "{{translate('pagination.showing_page')}} _PAGE_ {{translate('pagination.of')}} _PAGES_",
				            "infoEmpty": "{{translate('pagination.no_records')}}",
				            "infoFiltered": "({{translate('pagination.filterd_from')}} _MAX_ {{translate('pagination.total_records')}})",
				            "oPaginate" : {
				                "sFirst": "{{translate('pagination.first')}}",
				                "sLast": "{{translate('pagination.last')}}",
				                "sNext": "{{translate('pagination.next')}}",
				                "sPrevious": "{{translate('pagination.previous')}}"
				            },
				            "sSearch" : "{{translate('pagination.search')}}"
				        }
				    });
				}, 1000);
			},
			name : function (val) {
				this.nameError = '';
				if (val != "" && val.match(/^\s+/) == null) {
					this.nameClass = 'form-control';
				}
			},
			service : function (val) {
				if (val != "" && val.match(/^\s+/) == null) {
					this.serviceClass = 'form-control';
				}
			}
		},
		methods : {
			fetchData : function () {
				axios.get("{{url('/supplier/list/data')}}")
				.then(response => {
					return new Promise((resolve, reject) => {
						this.list = response.data;
						resolve();
					});
				})
				.catch(console.log);
			},
			saveData : function () {
				var errors = 0;
				if (this.name == "" || this.name.match(/^\s+/) != null) {
					this.nameClass = 'form-control error';
					errors = 1;
				}
				if (this.service == "" || this.service.match(/^\s+/) != null) {
					this.serviceClass = 'form-control error';
					errors = 1;
				}

				if (errors == 0) {
					this.enabled = true;
					axios.post("{{url('/supplier/save')}}", {
						'name' : this.name,
						'service' : this.service,
						'comment' : this.comment,
						'id' : this.hid,
						'_token' : "{{csrf_token()}}"
					})
					.then(response => {
						this.enabled = false;
						new PNotify({
	                        title : (response.data === false) ? "{{translate('words.faliure_message')}}" : "{{translate('words.success_message')}}",
	                        text : "",
	                        addclass : 'stack_top_right',
	                        type : (response.data === false) ? "error" : "success",
	                        width : '290px',
	                        delay : 2000
	                    });
	                }).then(() => {
	                	this.name = this.service = this.comment = this.hid = '';
	                    this.fetchData();
	                })
	                .catch(error => {
						this.nameError = error.response.data.name[0];
						this.nameClass = "form-control error";
						this.enabled = false;
					});
				}
			}
		}
	});
</script>