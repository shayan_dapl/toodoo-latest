@include('includes.tutorial_assets')
@stack('scripts')
<script type="text/javascript">
$(document).ready(function() {
    //Showing detail info for each turtle on process/turtle_board.blade.php
    $(document).on("click", ".line-cls", function () {
        $(".turtle-title").html("");
        $(".turtle-body").html('<center><i class="fa fa-cog fa-spin fa-3x fa-fw text-system"></i></center>');
        var id = $(this).data("id");
        var model = $(this).data("model");
        var processOwnerId = $('#process_owner').val();
        $.ajax({
            url: "{{url('/process/turtle/details')}}/" + model + "/" + id + "/" + processOwnerId,
            type: "GET",
            success: function (data) {
                $(".turtle-title").html(model);
                $(".turtle-body").html(data);
            }
        });
    });

    //sorting process steps on process/turtle_board.blade.php
    $("#sortable-icons").sortable({
        stop: function () {
            var listElements = $(this).children("li");
            var ids = listElements.map(function () {
                return $(this).data("id");
            }).get().join();
            $.ajax({
                url: "{{url('/process/steparrange/')}}",
                type: "POST",
                data: {"ids": ids, "_token": "{{csrf_token()}}"},
                success: function (data) {
                    new PNotify({
                        title: "{{translate('alert.steps')}}",
                        text: "{{translate('alert.rearranged_successfully')}}",
                        addclass: 'stack_top_right',
                        type: 'success',
                        width: '290px',
                        delay: 2000
                    });
                }
            })
        }
    });

    $(document).on('click', '#post-comment', function() {
        if($(".comment-class").hasClass("hide")) {
            $(".comment-class").removeClass("hide");
            $(".addaction-class").addClass("hide");
        } else {
            $(".comment-class").addClass("hide");
        }
    });

    $(document).on('click', '#add-action', function() {
        if($(".addaction-class").hasClass("hide")) {
            $(".addaction-class").removeClass("hide");
            $(".comment-class").addClass("hide");
            $(".DatePicker4").datepicker(dtOption);
        } else {
            $(".addaction-class").addClass("hide");
        }
    });

    $(document).on('click', '#event-log', function() {
        var riskid = $(this).data('riskid');
        var type = $(this).data('type');
        $(".event-log-body").html('<center><i class="fa fa-cog fa-spin fa-3x fa-fw text-system"></i></center>');
        $.ajax({
            url: "{{url('/process-step/risk/action/eventlog')}}/" + riskid + '/' + type,
            type: "GET",
            success: function(data) {
                $(".event-log-body").html(data);
            }
        });
    });

    $('#turtle-details').on('shown.bs.modal', function (event) {
        if(document.getElementById('process-kpi-graph') != null) {
            var ctx = document.getElementById('process-kpi-graph').getContext('2d');
            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'line',

                // The data for our dataset
                data: {
                    labels: ["January", "February", "March", "April", "May", "June", "July"],
                    datasets: [{
                        label: "My First dataset",
                        backgroundColor: 'rgb(255, 99, 132)',
                        borderColor: 'rgb(255, 99, 132)',
                        data: [0, 10, 5, 2, 20, 30, 45],
                    }]
                },

                // Configuration options go here
                options: {}
            });
        }
    })
});
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var riskOrOptId = sessionStorage.getItem("riskOrOpporId");
        var riskOrOptType = sessionStorage.getItem("riskOrOptType");
        if(riskOrOptType == 'risk'){
            $('#riskArea').find("li[data-id='" + riskOrOptId + "']").trigger('click');
        } else {
            $('#opportunityArea').find("li[data-id='" + riskOrOptId + "']").trigger('click');
        }
        sessionStorage.setItem("riskOrOpporId","");
        sessionStorage.setItem("riskOrOptType","");
    })

    function setCookie(cname, cvalue) {
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires;
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
</script>