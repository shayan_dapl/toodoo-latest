<script type="text/javascript">
	$(document).ready(function() {
		$('.select2-single').css('width', '100%');
		//User position show / hide for special users on user_form.blade.php
		$(document).on('click', '#make_special_user', function() {
			if ($(this).is(':checked') == true) {
				@if(isset($details) and $details->positions()->count() > 0)
					var confirmed = confirm("{{translate('alert.want_to_delete')}}");
					if (confirmed == true) {
						$.ajax({
							url: "{{url('/user/structure/allremove')}}/{{$details->id}}/{{$details->company->id}}",
							type: "GET",
							success: function(data) {
								if(data == "yes") {
									var ntitle = "{{translate('alert.user_position')}}";
									var ntext = "{{translate('alert.removed_successfully')}}";
									var ntype = 'success';						
								}
								if(data == "no") {
									var ntitle = "{{translate('alert.user_position')}}";
									var ntext = "{{translate('alert.cannot_remove_child_exist')}}";
									var ntype = 'error';
								}

								new PNotify({
									title: ntitle,
									text: ntext,
									addclass: 'stack_top_right',
									type: ntype,
									width: '290px',
									delay: 2000
								});
								setTimeout(function() {
									location.reload();
								}, 2000);
							}
						});
					} else {
						$(this).prop('checked', false);
					}
				@else
					$('#position-list').addClass('hide');
					$('#special-position-list').removeClass('hide');

					$('#language').prop('required',false);
					$('#email').prop('required',false);
					$('#password').prop('required',false);
					$('#password_confirmation').prop('required',false);

					$('#language-label').removeClass('required');
					$('#email-label').removeClass('required');
					$('#password-label').removeClass('required');
					$('#password_confirmation-label').removeClass('required');

					if($('#no_email').is(':checked') == true)
					{
						$('#language-div').removeClass('col-md-offset-2 col-md-4').addClass('col-md-offset-2 col-md-8');
					}
				@endif
			}
			if($(this).is(':checked') == false) {
				@if(isset($details) and $details->is_special_user == 1 and !empty($special))
					var confirmed = confirm("{{translate('alert.want_to_delete')}}");
	        		if (confirmed == true) {
	        			$.ajax({
	        				url: "{{url('/user/structure/specialposremove')}}/{{$details->company->id}}/{{$details->id}}",
	        				type: "GET",
	        				success: function(data) {
	        					if(data == "yes") {
	        						new PNotify({
										title: "{{translate('alert.user_position')}}",
										text: "{{translate('alert.removed_successfully')}}",
										addclass: 'stack_top_right',
										type: 'success',
										width: '290px',
										delay: 2000
									});
									setTimeout(function() {
										location.reload();
									}, 2000);
	        					}
	        				}
	        			});
	        		} else {
	        			$(this).prop('checked', true);
	        		}
				@else	
					$('#position-list').removeClass('hide');
					$('#special-position-list').addClass('hide');
					$('#language').prop('required',true);
					$('#email').prop('required',true);
	        		if ($('#hid').val() == "") {
	        			$('#password').prop('required',true);
	        			$('#password_confirmation').prop('required',true);

	        			$('#password-label').addClass('required');
	        			$('#password_confirmation-label').addClass('required');
	        		}
	        		$('#language-label').addClass('required');
	        		$('#email-label').addClass('required');
	        		if ($('#no_email').is(':checked') == false) {
	        			$('#is-auditor').removeClass('hide');
	        			$('#can-process-owner').removeClass('hide');
	        		}
	        	@endif
	        }
	    });

	    //user_form.blade.php
	    $(document).on('click', '#no_email', function() {
	    	if($(this).is(':checked') == true) {
	    		$('#invite_user').prop('checked', true);
	    		$('#language').prop('required',false);

	    		$('#email').prop('required',false);
	    		$('#password').prop('required',false);
	    		$('#password_confirmation').prop('required',false);
	    		
	    		$('[name="can_process_owner"]').prop('checked',false);
	    		$('[name="is_auditor"]').prop('checked',false);

	    		$('#user-email').addClass('hide');
	    		$('#user-password').addClass('hide');

	    		$('#is-auditor').addClass('hide');
	    		$('#can-process-owner').addClass('hide');
	    		$('#invite-div').addClass('hide');
	    		
	    		$('#image-div').removeClass('col-md-2').addClass('col-md-offset-2 col-md-8');
	    		$('#language-div').removeClass('col-md-4').addClass('col-md-offset-2 col-md-8');
	    	}
	    	if($(this).is(':checked') == false) {
	    		$('#language').prop('required',true);
	    		if($('#make_special_user').is(':checked') == false) {
	    			$('#email').prop('required',true);
	    			$('#password').prop('required',true);
	    			$('#password_confirmation').prop('required',true);

	    			$('#email-label').addClass('required');
	    			$('#password-label').addClass('required');
	    			$('#password_confirmation-label').addClass('required');
	    		}
	    		$('#user-email').removeClass('hide');
	    		$('#user-password').removeClass('hide');
	    		$('#is-auditor').removeClass('hide');
	    		$('#can-process-owner').removeClass('hide');
	    		$('#invite-div').removeClass('hide');
	    		$('#image-div').removeClass('col-md-offset-2 col-md-8').addClass('col-md-2');
	    		$('#language-div').removeClass('col-md-offset-2 col-md-8').addClass('col-md-offset-2 col-md-4');
	    	}
	    });

		//Adding functions input field dynamically on user_form.blade.php and profile.blade.php
		$(document).on('click', '.functions', function() {
			var row = $(this).data("row");
			if ($("#position-"+row).val() != 0) {
				if ($("#position-"+row).hasClass('error')) {
					$("#position-"+row).removeClass('error');
				}
				var nextId = +row+1;
				var nextEl = '\
				<div class="row repeter" id="output-'+nextId+'">\
					<div class="col-md-5 col-sm-5 col-xs-12">\
						<label class="field select">\
							<select data-id="'+nextId+'" class="position" id="position-'+nextId+'" name="functions[position][]">\
								@if(!empty($positions))\
								@foreach($positions as $id => $name) \
									<option value="{{$id}}">{{$name}}</option>\
								@endforeach\
								@endif\
							</select>\
							<i class="arrow"></i>\
						</label>\
					</div>\
					<div class="col-md-6 col-sm-6 col-xs-10">\
						<h3 id="holdername-'+nextId+'">{{translate("form.parent_position")}}</h3>\
						<input id="parentpos-'+nextId+'" name="functions[parent][]" value="0" type="hidden">\
					</div>\
					<div class="col-sm-1 col-xs-2">\
						<button type="button" class="btn btn-system functions" data-row="'+nextId+'" data-id="">\
							<i class="fa fa-plus"></i>\
						</button>\
					</div>\
				</div>';
				$("#output-"+row).after(nextEl);
				$("button[data-row="+row+"]").removeClass('btn-system').removeClass('functions').addClass('btn-danger btn-sm').addClass('functions-del').html('<i class="fa fa-minus"></i>');
			} else {
				if ($("#position-"+row).val() == 0) {
					$("#position-"+row).addClass('error');
					return false;
				}
			}
		});
		
		//Removing position/function row on user_form.blade.php and profile.blade.php
		$(document).on('click', '.functions-del', function() {
			var confirmed = confirm("{{translate('alert.are_you_sure')}}");
			if (confirmed == true) {
				var id = $(this).data('id');
				var row = $(this).data('row');
				if (id == "") {
					$('#output-'+row).remove();
				} else {
					$.ajax({
						url: "{{url('/user/structure/remove')}}/"+id,
						type: "GET",
						success: function(data) {
							if(data == "yes") {
								$('#output-'+row).remove();
								location.reload();
							}
							if(data == "no") {
								new PNotify({
									title: "{{translate('alert.user_position')}}",
									text: "{{translate('alert.cannot_remove_child_exist')}}",
									addclass: 'stack_top_right',
									type: 'error',
									width: '290px',
									delay: 2000
								});
							}	
						}
					});
				}
			}
		});

		//getting reporting head name for particular position on user_form.blade.php
		$(document).on('change', '.position', function() {
			var row = $(this).data('id');
			var val = $(this).val();
			if (val == 0) {
				$("#position-"+row+" option[value='0']").prop("selected", true);
				$("#holdername-"+row).html("{{translate('form.parent_position')}}");
				$("#parentpos-"+row).val("");
			}
			if (val != 0) {
				$.ajax({
					url: "{{url('/user/find_immidiate_parent')}}/"+val,
					async: true,
					type: "GET",
					cache: false,
					success: function(data) {	
						if (data == "no_parent") {
							new PNotify({
								title: "{{translate('alert.user_not_exist')}}",
								text: "{{translate('alert.on_parent_position')}}",
								addclass: 'stack_top_right',
								type: 'error',
								width: '290px',
								delay: 2000
							});
							$("#position-"+row+" option[value='0']").prop("selected", true);
							$("#holdername-"+row).html("{{translate('form.parent_position')}}");
							$("#parentpos-"+row).val("");
						} else {
							$("#holdername-"+row).html(data.name);
							$("#parentpos-"+row).val(data.id);
						}
					}
				});
			}
		});

		$('#outofservice-date').click(function() {
			var id = $("#hid").val();
			$("#service-position-handover").modal("toggle");
			return false;
		});

		$('[name="audit_handover"]').click(function() {
            if ($(this).is(':checked') == true) {
                $('#service-handover-date').prop('required', false);
                $('#out-of-service-date-panel').addClass('hide');
                $('.user-position').prop('required', false);
                $('.user-process').prop('required', false);
                if ($('.handover-positions').hasClass('error')) {
                	$('.handover-positions').removeClass('error');
                }
            }
            if ($(this).is(':checked') == false) {
                $('#service-handover-date').prop('required', true);
                $('#out-of-service-date-panel').removeClass('hide');
                $('.user-position').prop('required', true);
                $('.user-process').prop('required', true);
            }
        });

        var outSetvicedtOption = $.extend({}, $.datepicker.regional["{{session('lang')}}"], { 
            dateFormat: "dd-mm-yy"
        });

        $("#service-handover-date").datepicker(outSetvicedtOption);

        $("#service-position-handover-btn").click(function () {
    		var allow = 0;
    		if ($('[name="audit_handover"]').is(':checked') === false) {
	    		if ($('.handover-positions').length > 0) {
		    		$('.handover-positions').each(function(i, v) {
		    			if ($(this).val() == "") {
		    				$(this).addClass("error");
		    				allow++;
		    			} else {
		    				$(this).removeClass("error");
		    			}
		    		});
		    		if ($('[name="out_service_date"]').val() == '') {
		    			allow++;
		    			$('[name="out_service_date"]').focus();
		    		}
	    		} else {
	    			if ($('[name="out_service_date"]').val() == '') {
		    			allow++;
		    			$('[name="out_service_date"]').focus();
		    		}
	    		}
    		}
    		if (allow == 0) {
        		$('form').submit();
        	}
        });
        
        $("#service-handover-date").keypress(function(event) {event.preventDefault();});
    });
</script>
@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    var vm = new Vue({
        el : "#user-form",
        data : {
            email : "{{isset($details->email) ? $details->email : old('email')}}",
            image : "{{old('photo')}}",
            imageData : '',
            imageUrl : "{{asset('/image/placeholder.png')}}",
            imgsrc : "{{asset(((isset($details->photo) and $details->photo != 'placeholder.png') ? ('storage/company/'.$details->company->id.'/image/users/'.$details->photo.'?'.rand(0,9)) : '/image/placeholder.png'))}}",
            existing : false,
            isityou : false
        },
        methods : {
            getImage(e) {
                if (this.email != "" && this.email.match(/^\s+/) == null && vm.image == '') {
                    axios.post("{{url('/getstarted/getimage')}}", {
                        'email' : this.email,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        if (response.data === false) {
                            vm.image = '';
                            vm.imgsrc = "{{asset('/image/placeholder.png')}}";
                        } else {
                            var img = new Image();
                            img.crossOrigin = 'anonymous';
                            img.src = response.data;
                            var canvas = document.createElement("canvas");
                            var ctx = canvas.getContext("2d");
                            canvas.width = 100;
                            canvas.height = 100;
                            img.onload = function() {
                                ctx.drawImage(img, 0, 0);
                                var url = canvas.toDataURL("image/jpeg", 1.0);
                                vm.imageData = url;
                            };
                            vm.imageUrl = response.data;
                            if (vm.image == '') {
                                setTimeout(() => {
                                    vm.image = vm.imageData;
                                }, 1000);
                                vm.imgsrc = vm.imageUrl;
                                vm.isityou = true;
                            }
                        }
                        return Promise.resolve();
                    })
                    .catch(console.log);
                }
            },
            onImageChange(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                    return;
                var reader = new FileReader();
                var vm = this;
                reader.onload = (e) => {
                    vm.image = e.target.result;
                };
                reader.readAsDataURL(files[0]);
                this.imgsrc = "{{asset('/image/placeholder.png')}}";
            },
            confirmIt : function (val) {
                if (val == 'no') {
                    this.image = '';
                    this.imgsrc = "{{asset('/image/placeholder.png')}}";
                }
                this.isityou = false;
            }
        }
    });
</script>