<script type="text/javascript">
    $(document).ready(function(){
        $('.make-cadmin').click(function () {
            $('.make-cadmin').prop('disabled', true);
            var elem = $(this);
            if ($(this).is(':checked') == true) {
                var id = $(this).data('id');
                var onoff = "on";
            }
            if ($(this).is(':checked') == false) {
                var id = $(this).data('id');
                var onoff = "off";
            }
            $.ajax({
                url: "{{url('/make-customer-admin')}}/" + id + "/" + onoff,
                type: "GET",
                async: true,
                cache: false,
                success: function (data) {
                    if (onoff == "on" && data === false) {
                        if (elem.is(':checked') == true) {
                            elem.prop("checked", false);
                            new PNotify({
                                title: "{{translate('alert.additional_admin_exists')}}",
                                text: "",
                                addclass: 'stack_top_right',
                                type: "error",
                                width: '290px',
                                delay: 2000
                            });
                        }
                    } else if (onoff == "off" && data === false) {
                        if (elem.is(':checked') == false) {
                            elem.prop("checked", true);
                            new PNotify({
                                title: "{{translate('alert.only_admin')}}",
                                text: "",
                                addclass: 'stack_top_right',
                                type: "error",
                                width: '290px',
                                delay: 2000
                            });
                        }
                    } else {
                        new PNotify({
                            title: "{{translate('words.success_message')}}",
                            text: "",
                            addclass: 'stack_top_right',
                            type: "success",
                            width: '290px',
                            delay: 2000
                        });
                    }
                    $('.make-cadmin').prop('disabled', false);
                }
            });
        });
    });
</script>