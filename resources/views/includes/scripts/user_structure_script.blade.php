@include('includes.orgstructure_assets')
@stack('scripts')
@if(!empty($structure))
<script type="text/javascript">
    (function($) {
        $(function() {
            var datascource = {!! $structure !!};
            $('#chart-container').orgchart({
                'data' : datascource[0],
                'nodeContent': 'title',
                'nodeID': 'id',
                'createNode': function($node, data) {
                    var secondMenuIcon = $('<i>', {
                        'class': 'fa fa-info-circle second-menu-icon',
                        click: function() {
                            $(this).siblings('.second-menu').toggle();
                        }
                    });
                    if (data.special_user == 1)
                    {
                        var secondMenu = '<div class="second-menu second-menu-extended">' + data.photo + '</div>';
                    }
                    else
                    {
                        if(data.photo == 'placeholder.png') {
                            var secondMenu = '<div class="second-menu"><img class="avatar" src="{{asset('image/placeholder.png')}}"></div>';
                        } else {
                            var secondMenu = '<div class="second-menu"><img class="avatar" src="{{asset('storage/company/'.Auth::guard('customer')->user()->company_id.'/image/users')}}/' + data.photo + '"></div>';
                        }
                    }

                    $node.append(secondMenuIcon).append(secondMenu);
                }
            });
        });
    })(jQuery);
</script>
@endif
<script type="text/javascript">
    $(function() {
        $("#btnSave").click(function() {
            $('.second-menu').show();
            $('.bottomEdge').hide();
            var src = $('#fileupload-preview').attr('src');
            $('.orgchart').before('<h2>{{translate("form.organization_chart")}}<h2><hr>');
            $('.orgchart table > tr:first').before('<tr><td colspan="100%"><img src="{{asset('storage/company/'.Auth::guard('customer')->user()->company_id.'/image/'.Auth::guard('customer')->user()->logo)}}" class="chart-print-logo"></td></tr>');
            var dt = new Date();
            var day = dt.getDate();
            var month = dt.getMonth();
            var year = dt.getFullYear().toString().substr( - 2);
            $('.orgchart table:first').after('<hr><span class="chart-print-date">' + day + '/' + month + '/' + year + '</span>');
            var chartHeight = $('.orgchart').css('height').replace('px', '');
            chartHeight = + chartHeight + 100;
            chartHeight += 'px';
            $('#chart-container').css('height', chartHeight);
            html2canvas($("#chart-container"), {
                onrendered: function(canvas) {
                    theCanvas = canvas;
                    document.body.appendChild(canvas);
                    canvas.toBlob(function(blob) {
                        saveAs(blob, "Organigram.png");
                        location.reload();
                    });
                }
            });
        });
    });
    $(document).ready(function() {
        $('#branch, #department').change(function () {
            window.location.href="{{url('/user/structure')}}/" + $(this).val() + "/all";
        });

        $('#department').change(function () {
            window.location.href="{{url('/user/structure')}}/" + $('#branch').val() + "/" + $(this).val();
        });

        $('.second-menu').show();
        $(document).on('mouseenter mouseleave', '.assistant', function(event) {
            var $assistPic = $(this).children('.showassistpic');
            if (event.type === 'mouseenter') {
                if ($assistPic.length) {
                    $assistPic.css('visibility', 'visible');
                }
            } else {
                if ($assistPic.length) {
                    $assistPic.css('visibility', 'hidden');
                }
            }
        });

        $(document).on('click', '#btnShowPic', function() {
            $('.second-menu').toggle();
        });
    });
</script>