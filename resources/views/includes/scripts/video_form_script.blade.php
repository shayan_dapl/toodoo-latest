<script type="text/javascript">
    jQuery(document).ready(function() {
        var parentCategory =  $('#category').val();
        var selectedCategory = {{isset($details->category_id) ? $details->category_id : 0}};
        if (parentCategory != 0) {
             $.ajax({
                url: "{{url('/video/get-video-sub-category')}}/" + parentCategory +'/' + selectedCategory,
                type: "GET",
                success: function (data) {
                    $('#video_sub_cat').html(data); 
                }
            });
        }

        setTimeout(function(){
            $('input[name=media_type]:checked').trigger("click");
         }, 500);

        $("#image_nl").change(function () {
            preview(this, 'image_nl_preview', 'uploader1');
        });

        $("#image_en").change(function () {
            preview(this, 'image_en_preview', 'uploader2');
        });

        $("#image_fr").change(function () {
            preview(this, 'image_fr_preview', 'uploader3');
        });

        $('#category').change(function(){
            var catId = $(this).val();
            $.ajax({
                url: "{{url('/video/get-video-sub-category')}}/" + catId,
                type: "GET",
                success: function (data) {
                    $('#video_sub_cat').html(data); 
                }
            });
        })
        $('[name="media_type"]').click(function() {
            var choose_media = $('input[name=media_type]:checked').val();
            if ( choose_media == 'video') {
                $('#video-tab').removeClass('hide');
                if (!$('#image-tab').hasClass('hide')) {
                    $('#image-tab').addClass('hide');
                } 
            } else if(choose_media == 'image') {
                $('#image-tab').removeClass('hide');
                if(!$('#video-tab').hasClass('hide')){
                    $('#video-tab').addClass('hide');
                } 
            }
        });

        $(document).on('click', '#vid-save-btn', function () {
            var languages = {'Nederlands': 'nl', 'English': 'en', 'Francais': 'fr'};
            if ($('#category').val() == '') {        
                $(window).scrollTop($('#category').position().top);
            }
            $.each(languages, function (index, data) {
                $('#title_' + data).attr('required', true);
                $('#description_' + data).attr('required', true);

                var title = $('#title_' + data).val();
                var description = $('#description_' + data).val();

                if (title == "" || description == "")
                {
                    $('.tab-pane').removeClass('active');
                    $('#' + index).addClass('active');
                    $('.lang-links').removeClass('active');
                    $('.' + index).addClass('active');
                    if(description == ""){
                        $('.note-editor').addClass('error');
                    } else {
                        $('.note-editor').removeClass('error');
                    }
                }
            });
        });
    })
</script>