@inject('segment', 'App\Services\Sidebar')
<aside id="sidebar_left" class="nano nano-dark affix">
	<div class="sidebar-left-content nano-content">
		<ul class="nav sidebar-menu">
			<li class="sidebar-label pt36">Menu</li>
			<li @if( Request::segment(1) == 'home' ) class="active" @endif >
				<a href="{{url('/home')}}">
					<span class="fa fa-tachometer"></span>
					<span class="sidebar-title">{{ucfirst(translate('words.home'))}}</span>
				</a>
			</li>

			@if(Auth::guard("web")->check())
				@if(Auth::guard("web")->user()->type != 4)
					<li class="{{Request::segment(1) == 'list' ? 'active' : ''}}">
						<a href="{{url('/list')}}">
							<span class="fa fa-user"></span> 
							<span class="sidebar-title">{{ucfirst(translate('words.customer'))}}</span>
						</a>
					</li>

					<li class="{{($segment->segment() == 'audit/iso') ? 'active' : ''}}">
						<a class="accordion-toggle {{(Request::segment(1) == 'audit')?'menu-open':''}}" href="#">
							<span class="fa fa-columns"></span>
							<span class="sidebar-title">{{ucfirst(translate('words.audit'))}}</span>
							<span class="caret"></span>
						</a>
						<ul class="nav sub-nav">
							<li class="{{($segment->segment() == 'audit/iso') ? 'active' : ''}}">
								<a href="{{url('/audit/iso/list')}}">
									<span class="fa fa fa fa-th-list"></span>
									{{ucfirst(translate('words.standerd_iso_clause'))}}
								</a>
							</li>
						</ul>
					</li>
				@endif

				<li class="{{Request::segment(1) == 'language' ? 'active' : ''}}">
					<a href="{{url('/language')}}">
						<span class="fa fa-book"></span>
						<span class="sidebar-title">{{ucfirst(translate('words.language'))}}</span>
					</a>
				</li>
				<li class="{{Request::segment(2) == 'show-invoice' ? 'active' : ''}}">
					<a href="{{url('/home/show-invoice')}}">
						<span class="fa fa-usd"></span>
						<span class="sidebar-title">{{ucfirst(translate('words.invoice_list'))}}</span>
					</a>
				</li>

				@if(Auth::guard("web")->user()->type != 4)	
					<li class="{{Request::segment(1) == 'package' ? 'active' : ''}}">
						<a class="accordion-toggle {{(Request::segment(1) == 'package')?'menu-open':''}}" href="#">
							<span class="fa fa-diamond"></span>
							<span class="sidebar-title">{{ucfirst(translate('words.package'))}}</span>
							<span class="caret"></span>
						</a>
						<ul class="nav sub-nav">
							<li class="{{($segment->segment() == 'package/list') ? 'active' : ''}}">
								<a href="{{url('/package/list')}}">
									<span class="fa fa fa fa-th-list"></span>
									{{ucfirst(translate('words.package_list'))}}
								</a>
							</li>
							<li class="{{($segment->segment() == 'package/user-plan') ? 'active' : ''}}">
								<a href="{{url('/package/user-plan')}}">
									<span class="fa fa fa fa-th-list"></span>
									{{ucfirst(translate('words.user_plans'))}}
								</a>
							</li>
						</ul>
					</li>
					<li class="{{Request::segment(1) == 'trial-period' ? 'active' : ''}}">
						<a class="accordion-toggle {{((Request::segment(1) == 'trial-period') || (Request::segment(1) == 'vat-settings'))?'menu-open':''}}" href="#">
							<span class="fa fa-cog"></span>
							<span class="sidebar-title">{{ucfirst(translate('words.general_settings'))}}</span>
							<span class="caret"></span>
						</a>
						<ul class="nav sub-nav">
							<li class="{{($segment->segment() == 'trial-period/') ? 'active' : ''}}">
								<a href="{{url('/trial-period')}}">
									<span class="fa fa-th-list"></span>
									{{ucfirst(translate('words.trial_period_settings'))}}
								</a>
							</li>
							<li class="{{($segment->segment() == 'vat-settings/') ? 'active' : ''}}">
								<a href="{{url('/vat-settings')}}">
									<span class="fa fa-th-list"></span>
									{{ucfirst(translate('words.vat_settings'))}}
								</a>
							</li>
							<li class="{{($segment->segment() == '/kpi-unit') ? 'active' : ''}}">
								<a href="{{url('/kpi-unit')}}">
									<span class="fa fa-th-list"></span>
									{{ucfirst(translate('words.kpi_unit'))}}
								</a>
							</li>
						</ul>
					</li>
				@endif
			@endif

			@if(Auth::guard("customer")->check())
				@inject('strategySegment', 'App\Services\Sidebar')
				<li class="{{in_array($segment->segment(), $strategySegment->strategySegment())?'active':''}}">
					<a class="accordion-toggle {{in_array($segment->segment(), $strategySegment->strategySegment())?'menu-open':''}}" href="#">
						<span class="fa fa-sun-o"></span>
						<span class="sidebar-title">{{ucfirst(translate('words.strategy'))}}</span>
						<span class="caret"></span>
					</a>
					<ul class="nav sub-nav">
						<li class="{{$segment->segment() == 'context/context-map' ? 'active':''}}">
							<a class="accordion-toggle {{in_array($segment->segment(), $strategySegment->contextsSegment())?'menu-open':''}}" href="#">
								<span class="fa fa-map"></span>
								<span class="">
									{{ucfirst(translate('words.context'))}}
								</span>
								<span class="caret"></span>
							</a>
							<ul class="nav sub-nav">
								<li class="{{(Request::segment(1).'/'.Request::segment(2)== 'context/context-map')?'active':''}}">
									<a href="{{url('/context/context-map')}}" class="pl50">
										<span class="fa fa-map"></span>
										<span class="">
											{{ucfirst(translate('words.context_map'))}}
										</span>
									</a>
								</li>
								@if(Auth::guard('customer')->user()->type == 2)
								<li class="{{(Request::segment(1).'/'.Request::segment(2)== 'context/context-analysis-form')?'active':''}}">
									<a href="{{url('/context/context-analysis-form')}}" class="pl50">
										<span class="fa fa-columns"></span>
										<span class="">{{ucfirst(translate('words.context_detail'))}}</span>
									</a>
								</li>
								@endif
							</ul>
						</li>
						<li class="{{$segment->segment() == 'stakeholder-analysis/map' ? 'active':''}}">
							<a class="accordion-toggle {{in_array($segment->segment(), $strategySegment->stakeholderSegment())?'menu-open':''}}" href="#">
								<span class="fa fa-columns"></span>
								<span class="">{{ucfirst(translate('words.stakeholder'))}}</span>
								<span class="caret"></span>
							</a>
							<ul class="nav sub-nav">
								<li class="{{(Request::segment(1).'/'.Request::segment(2)== 'stakeholder-analysis/map')?'active':''}}">
									<a href="{{url('/stakeholder-analysis/map')}}" class="pl50">
										<span class="fa fa-map"></span>
										<span class="">
											{{ucfirst(translate('words.stakeholder_analysis_map'))}}
										</span>
									</a>
								</li>
								@if(Auth::guard('customer')->user()->type == 2)
								<li class="{{(Request::segment(1).'/'.Request::segment(2)== 'stakeholder-analysis/list')?'active':''}}">
									<a href="{{url('/stakeholder-analysis/list')}}" class="pl50">
										<span class="fa fa-columns"></span>
										<span class="">{{ucfirst(translate('words.stakeholder_detail'))}}</span>
									</a>
								</li>
								@endif
							</ul>
						</li>
						<li class="{{$segment->segment() == 'process/riskmap' ? 'active':''}}">
							<a href="{{url('/process/riskmap')}}">
								<span class="fa fa-bar-chart"></span>
								<span class="">
									{{ucfirst(translate('words.risk_map'))}}
								</span>
							</a>
						</li>
						<li class="{{$segment->segment() == 'process/opportunitymap' ? 'active':''}}">
							<a href="{{url('/process/opportunitymap')}}">
								<span class="fa fa-bar-chart"></span>
								<span class="">
									{{ucfirst(translate('words.opportunity_map'))}}
								</span>
							</a>
						</li>
					</ul>
				</li>
				@inject('organizationSegment', 'App\Services\Sidebar')
				<li class="{{in_array($segment->segment(), $organizationSegment->organizationSegment())?'active':''}}">
					<a class="accordion-toggle {{in_array($segment->segment(), $organizationSegment->organizationSegment())?'menu-open':''}}" href="#">
						<span class="fa fa-sitemap"></span>
						<span class="sidebar-title">{{ucfirst(translate('words.organization'))}}</span>
						<span class="caret"></span>
					</a>
					<ul class="nav sub-nav">
						<li class="{{($segment->segment() == 'user/structure') ? 'active' : ''}}">
							<a href="{{url('/user/structure')}}">
								<span class="fa fa-sitemap"></span>
								<span class="">{{ucfirst(translate('words.structure'))}}</span>
							</a>
						</li>
						<li class="{{$segment->segment() == 'process/racimap' ? 'active':''}}">
							<a href="{{url('/process/racimap')}}">
								<span class="fa fa-mortar-board"></span>
								<span class="">
									{{ucfirst(translate('words.raci_map'))}}
								</span>
							</a>
						</li>
						<li class="{{$segment->segment() == 'process/job-description-metrix' ? 'active':''}}">
							<a href="{{url('/process/job-description-metrix')}}">
								<span class="fa fa-mortar-board"></span>
								<span class="">
									{{ucfirst(translate('words.job_description_metrix'))}}
								</span>
							</a>
						</li>
					</ul>
				</li>
				@inject('processSegment', 'App\Services\Sidebar')
				<li class="{{in_array($segment->segment(), $processSegment->processSegment())?'active':''}}">
					<a class="accordion-toggle {{in_array($segment->segment(), $processSegment->processSegment())?'menu-open':''}}" href="#">
						<span class="fa fa-cogs"></span>
						<span class="sidebar-title">{{ucfirst(translate('words.process'))}}</span>
						<span class="caret"></span>
					</a>
					<ul class="nav sub-nav">
						<li class="{{$segment->segment() == 'process/categorized' ? 'active':''}}">
							<a href="{{url('/process/categorized')}}">
								<span class="fa fa-map"></span>
								<span class="">
									{{ucfirst(translate('words.company_map'))}}
								</span>
							</a>
						</li>
						<li class="{{$segment->segment() == 'process/documentmap' ? 'active':''}}">
							<a href="{{url('/process/documentmap')}}">
								<span class="fa fa-bar-chart"></span>
								<span class="">
									{{ucfirst(translate('words.document_map'))}}
								</span>
							</a>
						</li>
						<li class="{{$segment->segment() == 'process/regulationmap' ? 'active':''}}">
							<a href="{{url('/process/regulationmap')}}">
								<span class="fa fa-bar-chart"></span>
								<span class="">
									{{ucfirst(translate('words.regulation_map'))}}
								</span>
							</a>
						</li>
					</ul>
				</li>
				@inject('auditSegment', 'App\Services\Sidebar')
				<li class="{{in_array($segment->segment(), $auditSegment->auditSegment()) ? 'active' : ''}}">
					<a class="accordion-toggle {{in_array($segment->segment(), $auditSegment->auditSegment())?'menu-open':''}}" href="#">
						<span class="fa fa-bullhorn"></span>
						<span class="sidebar-title">{{ucfirst(translate('form.action'))}}</span>
						<span class="caret"></span>
					</a>
					<ul class="nav sub-nav">
						<li class="{{($segment->segment() == 'audit/report-issue') ? 'active' : ''}}">
							<a href="{{url('/audit/report-issue')}}">
								<span class="fa fa-lightbulb-o"></span>
								<span class="">{{translate('form.report_issue')}}</span>
							</a>
						</li>
					</ul>
					@if(Auth::guard('customer')->user()->type == 2)
					<li class="{{($segment->segment() == 'audit/planning-list') ? 'active' : ''}}">
						<a href="{{url('/audit/planning-list/all')}}">
							<span class="fa fa-calendar"></span>
							<span class="">{{translate('words.audit_calendar')}}</span>
						</a>
					</li>
					@endif
					@inject('monitoringSegment', 'App\Services\Sidebar')
					<li class="{{in_array($segment->segment(), $monitoringSegment->monitoringSegment()) ? 'active':''}}">
						<a class="accordion-toggle {{in_array($segment->segment(), $monitoringSegment->monitoringSegment())?'menu-open':''}}" href="#">
							<span class="fa fa-eye"></span>
							<span class="sidebar-title">
								{{ucfirst(translate('words.monitoring'))}}
							</span>
							<span class="caret"></span>
						</a>
						<ul class="nav sub-nav">
							<li class="{{($segment->segment() == 'audit/supplier-evaluation') ? 'active' : ''}}">
								<a href="{{url('/audit/supplier-evaluation')}}">
									<span class="fa fa-balance-scale"></span>
									<span class="">{{ucfirst(translate('words.supplier_evaluation'))}}</span>
								</a>
							</li>
							
							<li class="{{(Request::segment(1).'/'.Request::segment(2)== 'master-kpi/kpi-relation-graph')?'active':''}}">
								<a href="{{url('master-kpi/kpi-relation-graph')}}">
									<span class="fa fa-line-chart"></span>
									<span class="">{{ucfirst(translate('words.kpi_relation_graph'))}}</span>
								</a>
							</li>
							
						</ul>
					</li>
				</li>

				<li><hr class="nogap"></li>

				@inject('adminSegment', 'App\Services\Sidebar')
				@if(Auth::guard('customer')->user()->type == 2)
				<li class="{{in_array($segment->segment(), $adminSegment->adminSegment()) ? 'active' : ''}}">

					<a class="accordion-toggle {{in_array($segment->segment(), $adminSegment->adminSegment()) ? 'menu-open' : ''}}" href="#">
						<span class="fa fa-lock"></span>
						<span class="sidebar-title">{{ucfirst(translate('words.admin'))}}</span>
						<span class="caret"></span>
					</a>
					<ul class="nav sub-nav">
						<li class="{{(Request::segment(1) == 'branch') ? 'active':''}}">
							<a href="{{url('/branch/list')}}">
								<span class="fa fa-map-marker"></span> 
								<span class="">{{ucfirst(translate('words.branch'))}}</span>
							</a>
						</li>	
						<li class="{{(Request::segment(1) == 'department') ? 'active':''}}">
							<a href="{{url('/department/list')}}">
								<span class="fa fa-th-large"></span> 
								<span class="">{{ucfirst(translate('words.department'))}}</span>
							</a>
						</li>	
						<li class="{{(Request::segment(1) == 'position' and Request::segment(2) == 'list') ? 'active':''}}">
							<a href="{{url('/position/list')}}">
								<span class="fa fa-cubes"></span>
								<span class="">{{ucfirst(translate('words.position'))}}</span>
							</a>
						</li>
						<li class="{{(Request::segment(1) == 'user' and Request::segment(2) == 'list') ? 'active' : ''}}">
							<a href="{{url('/user/list')}}">
								<span class="fa fa-users"></span> 
								<span class="">{{ucfirst(translate('words.user'))}}</span>
							</a>
						</li>
						@if(Auth::guard('customer')->user()->can_process_owner == 1 or Auth::guard('customer')->user()->type == 2)
						<li class="{{Request::segment(1).'/'.Request::segment(2) == 'process/list'?'active':''}}">
							<a href="{{url('/process/list')}}">
								<span class="fa fa-tasks"></span> 
								<span class="">{{ucfirst(translate('words.process'))}}</span>
							</a>
						</li>
						@endif

						@if(Auth::guard('customer')->user()->type == 2)
						<li class="{{Request::segment(1).'/'.Request::segment(2) == 'process/doctype'?'active':''}}">
							<a href="{{url('/process/doctype')}}">
								<span class="fa fa-object-ungroup"></span> 
								<span class="">{{ucfirst(translate('words.doctype'))}}</span>
							</a>
						</li>

						<li class="{{Request::segment(1).'/'.Request::segment(2) == 'process/regulationtype'?'active':''}}">
							<a href="{{url('/process/regulationtype')}}">
								<span class="fa fa-object-ungroup"></span> 
								<span class="">{{ucfirst(translate('words.regulationtype'))}}</span>
							</a>
						</li>

						<li class="{{(Request::segment(1).'/'.Request::segment(2) == 'audit/finding-type-list')?'active':''}}">
							<a href="{{url('/audit/finding-type-list')}}">
								<span class="fa fa-object-ungroup"></span>
								<span class="">{{ucfirst(translate('words.finding_type'))}}</span>
							</a>
						</li>

						<li class="{{(Request::segment(1).'/'.Request::segment(2) == 'audit/source-type-list')?'active':''}}">
							<a href="{{url('/audit/source-type-list')}}">
								<span class="fa fa-object-group"></span>
								<span class="">{{ucfirst(translate('words.reporting_source'))}}</span>
							</a>
						</li>

						@inject('contextSegment', 'App\Services\Sidebar')
						<li class="{{(Request::segment(1).'/'.Request::segment(2) == 'context/list')?'active':''}}">
							<a href="{{url('/context/list')}}">
								<span class="fa fa-columns"></span>
								<span class="">{{ucfirst(translate('words.context_subject'))}}</span>
							</a>
						</li>

						@inject('stakeSegment', 'App\Services\Sidebar')
						<li class="{{(Request::segment(1).'/'.Request::segment(2) == 'stakeholder-analysis/category')?'active':''}}">
							<a href="{{url('/stakeholder-analysis/category')}}">
								<span class="fa fa-columns"></span>
								<span class="">{{ucfirst(translate('words.stakeholder_subject'))}}</span>
							</a>
						</li>
						<li class="{{($segment->segment() == 'settings/risk-opp-action') ? 'active' : ''}}">
							<a href="{{url('/settings/risk-opp-action')}}">
								<span class="fa fa-volume-up"></span>
								<span class="">{{translate('words.risk_opp_action')}}</span>
							</a>
						</li>
						<li class="{{($segment->segment() == 'settings/company') ? 'active' : ''}}">
							<a href="{{url('/settings/company')}}">
								<span class="fa fa-wrench"></span>
								<span class="">{{translate('words.company_settings')}}</span>
							</a>
						</li>
						<li class="{{(Request::segment(3).'/'.Request::segment(4) == 'activity/list') ? 'active' : ''}}">
							<a href="{{url('/process-step/resources/activity/list')}}">
								<span class="fa fa-th-large"></span> 
								<span class="">{{ucfirst(translate('words.resource_activity'))}}</span>
							</a>
						</li>
						<li class="{{(Request::segment(1) == 'rating') ? 'active':''}}">
							<a class="accordion-toggle {{(Request::segment(1) == 'rating') ? 'menu-open' : ''}}" href="#">
								<span class="fa fa-bullhorn"></span>
								<span class="sidebar-title">{{ucfirst(translate('words.competence'))}}</span>
								<span class="caret"></span>
							</a>
							<ul class="nav sub-nav">
								<li class="{{(Request::segment(1).'/'.Request::segment(2) == 'rating/list') ? 'active':''}}">
									<a href="{{url('/rating/list')}}" class="pl50">
										<span class="fa fa-star"></span> 
										<span class="">{{ucfirst(translate('words.rating'))}}</span>
									</a>
								</li>
								<li class="{{(Request::segment(1).'/'.Request::segment(2) == 'rating/action-type') ? 'active':''}}">
									<a href="{{url('/rating/action-type')}}" class="pl50">
										<span class="fa fa-file-text-o"></span> 
										<span class="">{{ucfirst(translate('words.action_type'))}}</span>
									</a>
								</li>
							</ul>
						</li>
						<li class="{{(Request::segment(1).'/'.Request::segment(2) == 'master-kpi/list') ? 'active':''}}">
							<a href="{{url('/master-kpi/list')}}">
								<span class="fa fa-sliders"></span> 
								<span class="">{{ucfirst(translate('words.master_kpi'))}}</span>
							</a>
						</li>
						<li class="{{(Request::segment(1) == 'supplier') ? 'active':''}}">
							<a class="accordion-toggle {{(Request::segment(1) == 'supplier') ? 'menu-open':''}}" href="#">
								<span class="fa fa-handshake-o"></span>
								<span class="sidebar-title">{{ucfirst(translate('words.supplier_evaluation'))}}</span>
								<span class="caret"></span>
							</a>
							<ul class="nav sub-nav">
								<li class="{{(Request::segment(1).'/'.Request::segment(2) == 'supplier/list') ? 'active':''}}">
									<a href="{{url('/supplier/list')}}" class="pl50">
										<span class="fa fa-bars"></span> 
										<span class="">{{ucfirst(translate('words.list'))}}</span>
									</a>
								</li>
								<li class="{{(Request::segment(1).'/'.Request::segment(2) == 'supplier/parameters') ? 'active':''}}">
									<a href="{{url('/supplier/parameters')}}" class="pl50">
										<span class="fa fa-sort-alpha-asc"></span> 
										<span class="">{{ucfirst(translate('words.parameters'))}}</span>
									</a>
								</li>
								<li class="{{(Request::segment(1).'/'.Request::segment(2) == 'supplier/ratings') ? 'active':''}}">
									<a href="{{url('/supplier/ratings')}}" class="pl50">
										<span class="fa fa-star-half-o"></span> 
										<span class="">{{ucfirst(translate('words.ratings'))}}</span>
									</a>
								</li>
							</ul>
						</li>
						@endif
					</ul>
				</li>
				@endif
			@endif
		</ul>
	</div>
</aside>