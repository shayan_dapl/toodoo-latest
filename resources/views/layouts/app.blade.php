@include('includes.main_header')
<body class="external-page sb-l-c sb-r-c">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NCTWFHP"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="main" class="animated fadeIn">
        <section id="content_wrapper">
            <div id="canvas-wrapper">
                <canvas id="demo-canvas"></canvas>
            </div>
            <section id="content">
                <div class="admin-form theme-info" id="login1">
                    <div class="row mb15 table-layout">
                        <div class="col-xs-10 text-right va-b pr5"></div>
                        <div class="col-xs-2 text-right va-b pr5">
                            <button data-toggle="dropdown" class="btn btn-sm dropdown-toggle" title="{{!empty(session('langName'))?session('langName'):translate('words.nl')}}">
                                {{!empty(session('langName')) ? translate('words.'.session('langName')) : translate('words.nl')}}
                            </button>
                            <ul class="dropdown-menu pv5 animated animated-short white-drop flipInX" role="menu">
                                <li>
                                    <a class="chgLang" data-id="nl" href="javascript:void(0);">
                                        {{translate('words.nl')}}
                                    </a>
                                </li>
                                <li>
                                    <a class="chgLang" data-id="en" href="javascript:void(0);">
                                        {{translate('words.en')}} 
                                    </a>
                                </li>
                                <li>
                                    <a class="chgLang" data-id="fr" href="javascript:void(0);">
                                        {{translate('words.fr')}} 
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel panel-info mt10 br-n">
                        <div class="panel-heading heading-border">
                            @if(Request::segment(1) == 'entry-panel')
                            <div class="row">
                                <div class="col-md-6 text-dark fw600 fs24">
                                    {{translate('words.associated_companies')}}
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="{{url('/out')}}" class="btn btn-danger">{{translate('words.logout')}}</a>
                                </div>
                            </div>
                            @endif
                        </div>
                        @yield('content')
                    </div>
                </div>
            </section>
        </section>
    </div>
    @include('includes.main_footer')