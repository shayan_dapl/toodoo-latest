@include('includes.header')
@include('includes.sidebar')
<section id="content_wrapper">
    <header id="topbar" class="alt">
        <div class="topbar-left">
            <ol class="breadcrumb">
				<li class="crumb-icon">
					<a href="{{url('/home')}}">
						<span class="fa fa-home"></span>
					</a>
				</li>
				@foreach ($breadcrumbs as $link=>$name)
				<li class="crumb-trail" id="breadcrumbTitle">
					<a href="{{url($link)}}">{{translate($name)}}</a>
				</li>
				@endforeach
			</ol>
        </div>
        
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable margin-extended @if(Session::has('error')) default-page-width-extended @endif">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check pr10"></i>
            {{Session::get('success')}}
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable margin-extended @if(Session::has('success')) alert-width-extended @endif">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-times pr10"></i>
            {{Session::get('error')}}
        </div>
        @endif
        @if(Session::has('email_exists'))
        <div class="alert alert-danger alert-dismissable margin-extended">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-times pr10"></i>
            {{Session::get('email_exists')}}
        </div>
        @endif
        @if(Session::has('success_position'))
        <div class="alert alert-success alert-dismissable margin-extended @if(Session::has('error')) default-page-width-extended @endif">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check pr10"></i>
            {{Session::get('success_position')}}
        </div>
        @endif

        <!-- Package trial message -->
        @if(!Session::has('banner'))
        @if(!isset(Auth::guard('customer')->user()->company->subscription_id))
        @if(!empty(Auth::guard('customer')->user()->company->trial_days) and !empty(Auth::guard('customer')->user()->company->trial_count))
            @if(Auth::guard('customer')->user()->company->trial_days > (Auth::guard('customer')->user()->company->trial_count * 2))
                <div class="label bg-warning dark pull-right mr30" id="package-trial">
                    {{translate('alert.trial_period_message')}} : {{(Auth::guard('customer')->user()->company->trial_days)}} {{translate('words.day_left')}}
                    <i class="fa fa-times ml20 pointer" onclick="removeTrialBanner()"></i>
                </div>
            @endif
            @if(Auth::guard('customer')->user()->company->trial_days <= (Auth::guard('customer')->user()->company->trial_count * 2) and Auth::guard('customer')->user()->company->trial_days > Auth::guard('customer')->user()->company->trial_count)
                <div class="label bg-warning dark pull-right mr30" id="package-trial">
                    {{translate('alert.trial_period_message')}} : {{(Auth::guard('customer')->user()->company->trial_days)}} {{translate('words.day_left')}}
                    <i class="fa fa-times ml20 pointer" onclick="removeTrialBanner()"></i>
                </div>
            @endif
            @if(Auth::guard('customer')->user()->company->trial_days <= (Auth::guard('customer')->user()->company->trial_count))
                <div class="label bg-danger pull-right mr30" id="package-trial">
                    {{translate('alert.trial_period_message')}} : {{(Auth::guard('customer')->user()->company->trial_days)}} {{translate('words.day_left')}}
                    <i class="fa fa-times ml20 pointer" onclick="removeTrialBanner()"></i>
                </div>
            @endif
        @endif
        @endif
        @endif
    </header>
    <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
            @yield('content')
        </div>
    </section>
    @include('includes.footer')