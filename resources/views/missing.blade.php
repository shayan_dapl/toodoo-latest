@extends('layouts.main')
@section('content')
{!! Html::style('css/extra-styles/missing.css') !!}
<div class="panel-body bg-light p30">
    <div class="row">
        <div class="col-md-12 text-center fs90 fw700 text-danger">
        	404
        	<br clear="all">
        	<small class="fs50">{{translate('words.page_not_found')}}</small>
        </div>
    </div>
</div>
@stop
































