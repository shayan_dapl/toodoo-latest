@extends('layouts.default')
@section('content')
@inject('file', 'App\Services\DelegationDocumentForm')
<div class="admin-form" id="action-by-member-form">
    <div class="panel">
        <div class="panel-body bg-light">
            <a href="{{url('/home')}}" class="btn btn-system text-default pull-right">
                {{translate('form.cancel')}}
            </a>
            {!! Form::open(array('method' => 'post', 'url' => '/audit/issue-action-save', 'class' => 'form-horizontal')) !!}
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.report_delegation')}}</span>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('source_type', translate('form.source_type').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}

                    <div class="col-md-4">
                        {{translate('words.internal_audit')}}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('audit_date', translate('words.audit_date').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}

                    <div class="col-md-4">
                        {{date(Config::get('settings.dashed_date'), strtotime($reported_issues->audit->plan_date))}}
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="form-group">
                    {!! Form::label('finding_type', translate('form.finding_type').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}

                    <div class="col-md-4">
                        {{$reported_issues['finding_type']['finding_type']}}	
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('related_process', translate('form.related_process').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}

                    <div class="col-md-4">
                        {{$reported_issues['process']['name']}}	
                    </div>
                </div>
            </div>
			
			<div class="row">
                <div class="form-group">
                    {!! Form::label('process_owner', translate('form.process_owner').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}

                    <div class="col-md-4">
                        {{$processOwnerName}} 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('issue', translate('form.issue').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}
                    <div class="col-md-4">		
                        <label class="field">
                            @if($source_type == "finding-issue")
                            {{$reported_issues['issue_desc']}}
                            @else
                            {{$reported_issues['observation']}}
                            @endif	
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('iso', translate('form.iso_reference').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}
                    <div class="col-md-4">
                        @if(!empty($reported_issues['iso']))
                        @inject('iso', 'App\Services\AuditActionByMember')
                        {{$reported_issues['iso'][$iso->iso(session('lang'))]}}
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('iso', translate('form.reference').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}
                    <div class="col-md-4">
                        @if(!empty($reported_issues['reference']))
                        @inject('name', 'App\Services\AuditActionByMember')
                        {{$reported_issues['reference']['clause']}}
                        {{$reported_issues['reference'][$name->name(session('lang'))]}}
                        @endif
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-8 col-md-offset-4">
                    @if(isset($reported_issues->docs))
                    @inject('file', 'App\Services\ExecDocumentForm')
                    <ul class="docContainercls">
                        @foreach($reported_issues->docs as $eachDoc)
                        <li>
                            <span class="doctypecls"><i class="fa {{$file->file($eachDoc->doc_name)['icon']}} fa-3x text-system-faded"></i>
                                <a href="{{$file->file($eachDoc->doc_name)['url']}}" target="_blank">
                                    <i class="fa fa-eye fa-1x doc-del-btn" aria-hidden="true"></i>
                                </a>
                            </span>
                            <p>{{$eachDoc->doc_name}}</p>
                        </li>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="section-divider mv5" id="spy2">
        </div>

        @if(($metrix_data->immidiate_actions == 1) || ($metrix_data->root_cause_analysis == 1) || ($metrix_data->corrective_measure == 1) || ($metrix_data->effectiveness == 1))
        <div class="tab-block mb25">
            <div class="timeline_tab">
                <ul id="tabline" class="nav nav-tabs {{$metrixClass}}">
                    @if($metrix_data->immidiate_actions == 1)
                    <li class="tab-links proactive {{$immidiateActive}} {{$immidiatAactiveCls}}">
                        <a href="#immidiate_actions" data-toggle="tab">{{translate('form.immidiate_actions')}}</a>
                    </li>
                    @endif
                    @if($metrix_data->root_cause_analysis == 1)
                    <li class="tab-links {{$rootcauseActive}} {{$rootCauseActiveCls}}">
                        <a href="#root_cause_analysis" data-toggle="tab">{{translate('form.root_cause_analysis')}}</a>
                    </li>
                    @endif
                    @if($metrix_data->corrective_measure == 1)
                    <li class="tab-links {{$corrective_measureActive}}">
                        <a href="{{$define_corrective_link}}" data-toggle="tab">{{translate('form.define_corrective_action')}}</a>
                    </li>
                    @endif
                    @if($metrix_data->effectiveness == 1)
                    <li class="tab-links {{$effectivenessActive}}">
                        <a href="{{$effectiveness_link}}" data-toggle="tab">{{translate('form.effectiveness')}}</a>
                    </li>
                    @endif
                </ul>
            </div>
            <div class="tab-content">
                @if($metrix_data->immidiate_actions == 1)
                <div class="tab-pane {{$immidiateActive}}" id="immidiate_actions">
                    <div class="row mb20">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                <thead class="bg-system text-default">
                                    <tr>
                                        <th>{{translate('table.delegate_to')}}</th>
                                        <th>{{translate('table.deadline')}}</th>
                                        <th>{{translate('table.required_action')}}</th>
                                        <th>{{translate('table.describe_action')}}</th>
                                        <th>{{translate('table.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($immidiateActions) > 0)
                                        @foreach($immidiateActions as $eachIA)
                                            @if($eachIA->status==0)
                                            {!! Form::open(array('method' => 'post', 'url' => '/audit/action-by-member-save', 'class' => 'form-horizontal')) !!}
                                            <tr>
                                                <td>{{$eachIA->user->name}}</td>
                                                <td>{{$eachIA->formattedDate()}} {!!$eachIA->dayCount()!!}</td>
                                                <td>{{$eachIA->requested_action}}</td>
                                                <td>{{ Form::textarea('member_feedback','', ['class' => 'gui-textarea','id'=>'ia', 'value' => '', 'placeholder' => translate('form.describe_here_immidiate_actions') ,'required'=>'required' ,($eachIA->member != Auth::guard('customer')->user()->id) ? 'disabled' :'']) }}
                                                @if($eachIA->member == Auth::guard('customer')->user()->id)
                                                <button type="button" class="btn-system btn-xs actionDelegationDoc" data-toggle="modal" data-target="#myModal" data-id="{{$eachIA->id}}" data-new="" data-existingrow="{{$loop->iteration}}">
                                                    <i class="fa fa-upload"></i>
                                                </button>
                                                @endif
                                                </td>
                                                <td>
                                                @if($eachIA->member == Auth::guard('customer')->user()->id)
                                                    <button class="btn btn-system text-default" type="submit">
                                                        {{translate('table.mark_as_done')}}
                                                    </button>
                                                @endif
                                                </td>
                                            </tr>
                                            {!! csrf_field() !!}
                                                {!! Form::hidden('delegate_id', $eachIA->id) !!}
                                                {!! Form::hidden('ref_id', isset($ref_id) ? $ref_id : '') !!}
                                            {!! Form::close() !!}
                                            @else
                                            <tr>
                                                <td>{{$eachIA->user->name}}</td>
                                                <td>{{$eachIA->formattedDate()}}</td>
                                                <td>{{$eachIA->requested_action}}</td>
                                                <td>{{ $eachIA->action_taken}}
                                                @if(!empty($eachIA->docs))
                                                  @foreach($eachIA->docs as $eachDoc) 
                                                    <span class="delegatedoctypecls"><i class="fa {{$file->file($eachDoc->doc_name)['icon']}} fa-2x text-system-faded"></i>
                                                        <a href="{{$file->file($eachDoc->doc_name)['url']}}" target="_blank">
                                                            <i class="fa fa-eye fa-1x doc-del-btn" aria-hidden="true"></i>
                                                        </a>
                                                    </span>
                                                  @endforeach
                                                @endif
                                                </td>
                                                <td>
                                                    <span class="text-success fw700">
                                                        {{translate('table.completed')}}
                                                    </span>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
                @if($metrix_data->root_cause_analysis == 1)
                <div class="tab-pane {{$rootcauseActive}}" id="root_cause_analysis">
                    <div class="row mb20">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                <thead class="bg-system text-default">
                                    <tr>
                                        <th>{{translate('table.delegate_to')}}</th>
                                        <th>{{translate('table.deadline')}}</th>
                                        <th>{{translate('table.required_action')}}</th>
                                        <th>{{translate('table.describe_action')}}</th>
                                        <th>{{translate('table.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($rootCause) > 0)
                                    @foreach($rootCause as $eachRC)
									 @if($eachRC->status==0)
                                        {!! Form::open(array('method' => 'post', 'url' => '/audit/action-by-member-save', 'class' => 'form-horizontal')) !!}
                                        <tr>
                                            <td>{{$eachRC->user->name}}</td>
                                            <td>{{$eachRC->formattedDate()}} {!!$eachRC->dayCount()!!}</td>
                                            <td>{{$eachRC->requested_action}}</td>
                                            <td>{{ Form::textarea('member_feedback','', ['class' => 'gui-textarea','id'=>'rca', 'placeholder' => translate('form.describe_here_root_cause_analysis'),'required'=>'required',($eachRC->member != Auth::guard('customer')->user()->id)? 'disabled' :'']) }}
                                            @if($eachRC->member == Auth::guard('customer')->user()->id)
                                            <button type="button" class="btn-system btn-xs actionDelegationDoc" data-toggle="modal" data-target="#myModal" data-id="{{$eachRC->id}}" data-new="" data-existingrow="{{$loop->iteration}}">
                                                    <i class="fa fa-upload"></i>
                                            </button>
                                            @endif
                                            </td>
                                            <td>
                                            @if($eachRC->member == Auth::guard('customer')->user()->id)
                                                <button class="btn btn-system text-default" type="submit">
                                                        {{translate('table.mark_as_done')}}
                                                </button>
                                            @endif
                                            </td>
                                        </tr>
                                        {!! csrf_field() !!}
                                                {!! Form::hidden('delegate_id', $eachRC->id) !!}
                                                {!! Form::hidden('ref_id', isset($ref_id) ? $ref_id : '') !!}
                                        {!! Form::close() !!}
										@else
											<tr>
                                                <td>{{$eachRC->user->name}}</td>
                                                <td>{{$eachRC->formattedDate()}}</td>
                                                <td>{{$eachRC->requested_action}}</td>
                                                <td>{{$eachRC->action_taken}}
                                                @if(!empty($eachRC->docs))
                                                  @foreach($eachRC->docs as $eachDoc) 
                                                    <span class="delegatedoctypecls"><i class="fa {{$file->file($eachDoc->doc_name)['icon']}} fa-2x text-system-faded"></i>
                                                        <a href="{{$file->file($eachDoc->doc_name)['url']}}" target="_blank">
                                                            <i class="fa fa-eye fa-1x doc-del-btn" aria-hidden="true"></i>
                                                        </a>
                                                    </span>
                                                  @endforeach
                                                @endif
                                                </td>
                                                <td>
                                                    <span class="text-success fw700">
                                                        {{translate('table.completed')}}
                                                    </span>
                                                </td>
                                            </tr>
										@endif
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
                @if($metrix_data->corrective_measure == 1)
                <div class="tab-pane {{$corrective_measureActive}}" id="define_corrective">
                    <div class="row mb20">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                <thead class="bg-system text-default">
                                    <tr>
                                        <th>{{translate('table.delegate_to')}}</th>
                                        <th>{{translate('table.deadline')}}</th>
                                        <th>{{translate('table.required_action')}}</th>
                                        <th>{{translate('table.describe_action')}}</th>
                                        <th>{{translate('table.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($correctiveMeasure) > 0)
                                        @foreach($correctiveMeasure as $eachCM)
										@if($eachCM->status==0)
                                        {!! Form::open(array('method' => 'post', 'url' => '/audit/action-by-member-save', 'class' => 'form-horizontal')) !!}
                                            <tr>
                                                <td>{{$eachCM->user->name}}</td>
                                                <td>{{$eachCM->formattedDate()}} {!!$eachCM->dayCount()!!}</td>
                                                <td>{{$eachCM->requested_action}}</td>
                                                <td>{{ Form::textarea('member_feedback','', ['class' => 'gui-textarea', 'placeholder' => translate('form.describe_here_corrective_action'),'required'=>'required' ,($eachCM->member != Auth::guard('customer')->user()->id)? 'disabled' :'']) }}
                                                @if($eachCM->member == Auth::guard('customer')->user()->id)
                                                 <button type="button" class="btn-system btn-xs actionDelegationDoc" data-toggle="modal" data-target="#myModal" data-id="{{$eachCM->id}}" data-new="" data-existingrow="{{$loop->iteration}}">
                                                    <i class="fa fa-upload"></i>
                                                </button>
                                                @endif
                                                </td>
                                                <td>
                                                @if($eachCM->member == Auth::guard('customer')->user()->id)
                                                     <button class="btn btn-system text-default" type="submit">
                                                        {{translate('table.mark_as_done')}}
													</button>
                                                @endif
                                                </td>
                                            </tr>
                                        {!! csrf_field() !!}
                                            {!! Form::hidden('delegate_id', $eachCM->id) !!}
                                            {!! Form::hidden('ref_id', isset($ref_id) ? $ref_id : '') !!}
                                        {!! Form::close() !!}
										@else
											<tr>
                                                <td>{{$eachCM->user->name}}</td>
                                                <td>{{$eachCM->formattedDate()}}</td>
                                                <td>{{$eachCM->requested_action}}</td>
                                                <td>{{$eachCM->action_taken}}
                                                @if(!empty($eachCM->docs))
                                                  @foreach($eachCM->docs as $eachDoc) 
                                                    <span class="delegatedoctypecls"><i class="fa {{$file->file($eachDoc->doc_name)['icon']}} fa-2x text-system-faded"></i>
                                                        <a href="{{$file->file($eachDoc->doc_name)['url']}}" target="_blank">
                                                            <i class="fa fa-eye fa-1x doc-del-btn" aria-hidden="true"></i>
                                                        </a>
                                                    </span>
                                                  @endforeach
                                                @endif
                                                </td>
                                                <td>
                                                    <span class="text-success fw700">
                                                        {{translate('table.completed')}}
                                                    </span>
                                                </td>
                                            </tr>
										@endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
                @if($metrix_data->effectiveness == 1)
                <div class="tab-pane {{$effectivenessActive}}" id="effectiveness">
                    @if(count($effectiveness) > 0)
                    <div class="row mb20">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                <thead class="bg-system text-default">
                                    <tr>
                                        <th>{{translate('table.delegate_to')}}</th>
                                        <th>{{translate('table.deadline')}}</th>
                                        <th>{{translate('table.required_action')}}</th>
                                        <th>{{translate('table.describe_action')}}</th>
                                        <th>{{translate('table.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($effectiveness as $eachEF)
									@if($eachEF->status==0)
                                     {!! Form::open(array('method' => 'post', 'url' => '/audit/action-by-member-save', 'class' => 'form-horizontal')) !!}
                                        <tr>
                                            <td>{{$eachEF->user->name}}</td>
                                            <td>{{$eachEF->formattedDate()}} {!!$eachEF->dayCount()!!}</td>
                                            <td>{{$eachEF->requested_action}}</td>
                                            <td>{{ Form::textarea('member_feedback','', ['class' => 'gui-textarea','id'=>'issue', 'placeholder' => translate('form.describe_here_effectiveness'),'required'=>'required',($eachEF->member != Auth::guard('customer')->user()->id)? 'disabled' :'']) }}
                                            @if($eachEF->member == Auth::guard('customer')->user()->id)
                                            <button type="button" class="btn-system btn-xs actionDelegationDoc" data-toggle="modal" data-target="#myModal" data-id="{{$eachEF->id}}" data-new="" data-existingrow="{{$loop->iteration}}">
                                            <i class="fa fa-upload"></i>
                                            </button>
                                            @endif
                                            </td>
                                            <td>
                                            @if($eachEF->member == Auth::guard('customer')->user()->id)
                                                 <button class="btn btn-system text-default" type="submit">
                                                        {{translate('table.mark_as_done')}}
                                                </button>
                                            @endif
                                            </td>
                                        </tr>
                                       {!! csrf_field() !!}
                                        {!! Form::hidden('delegate_id', $eachEF->id) !!}
                                        {!! Form::hidden('ref_id', isset($ref_id) ? $ref_id : '') !!}
                                    {!! Form::close() !!}
									@else
										<tr>
                                                <td>{{$eachEF->user->name}}</td>
                                                <td>{{$eachEF->formattedDate()}}</td>
                                                <td>{{$eachEF->requested_action}}</td>
                                                <td>{{$eachEF->action_taken}}
                                                @if(!empty($eachEF->docs))
                                                  @foreach($eachEF->docs as $eachDoc) 
                                                    <span class="delegatedoctypecls"><i class="fa {{$file->file($eachDoc->doc_name)['icon']}} fa-2x text-system-faded"></i>
                                                        <a href="{{$file->file($eachDoc->doc_name)['url']}}" target="_blank">
                                                            <i class="fa fa-eye fa-1x doc-del-btn" aria-hidden="true"></i>
                                                        </a>
                                                    </span>
                                                  @endforeach
                                                @endif
                                                </td>
                                                <td>
                                                    <span class="text-success fw700">
                                                        {{translate('table.completed')}}
                                                    </span>
                                                </td>
                                        </tr>
									@endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                </div>
                @endif
        </div>
        @endif
    </div>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header panel-heading bg-system p20">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{translate('form.documents')}}</h4>
            </div>
            <div class="modal-body text-center" id="docpanel">
                
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

         $('#tabline li').click(function(event) {
            $(this).addClass('proactive');
         });
         $(document).on('click', '.actionDelegationDoc', function(e) {
            var el = $(this);
            var delegationId = $(this).data('id');
            var newrow = $(this).data('new');
            $('#docpanel').html('<i class="fa fa-cog fa-spin fa-4x text-system"></i>');
            if (delegationId != "")
            {   
                    $("#myModal").one('shown.bs.modal', function () {
                        setTimeout(function(){
                            $.ajax({
                                url: "{{url('/audit/delegation/document-form')}}/" + delegationId,
                                type: "GET",
                                success: function(data)
                                {
                                    $('#docpanel').html(data);
                                }
                            });
                        }, 500);
                    });         
            }
        }); 

         $(document).on('click', '#docSave', function() {
        $('#root').after('<i class="fa fa-cog fa-spin text-system"></i>');
        //While updating existing execution row with document
        if ($('#hid').val() != "")
        {
            var delegationId = $('#hid').val();
            $.ajax({
                url: "{{url('/audit/delegation/document-form-save')}}",
                type: "POST",
                data: {
                    "file" : $('#fileData').val(),
                    "filename" : $('#fileName').val(),
                    "fileextension" : $('#fileExt').val(),
                    "hid" : $('#hid').val(),
                    "_token" : "{{csrf_token()}}"
                },
                success: function(data) {
                    $('#root').next('i').remove();
                    if (data == "Failed")
                    {
                        var title = "{{translate('alert.document_not_uploaded')}}";
                        var status = "error";
                    }
                    else
                    {
                        var title = "{{translate('alert.document_uploaded')}}";
                        var status = "success";
                    }

                    new PNotify({
                        title: title,
                        text: "",
                        addclass: 'stack_top_right',
                        type: status,
                        width: '290px',
                        delay: 2000
                    });
                    $.ajax({
                        url: "{{url('/audit/delegation/document-form')}}/" + delegationId ,
                        type: "GET",
                        success: function(data)
                        {
                            $('#docpanel').html(data);
                        }
                    });
                }
            });
        }
    });  
    });
    $(window).on('load', function () {
        $('#tabline li.activecls').find('a').trigger( "click" );
    });
</script>
@include('includes.scripts.action_by_member_script')
@stop
