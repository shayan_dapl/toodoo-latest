@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <span class="panel-title">
                    <span class="fa fa-table"></span>{{ucfirst(translate('words.action_matrix'))}}</span>
                </div>
                <div class="panel-body pn">
                    {!! Form::open(array('method' => 'post', 'url' => '/audit/action-matrix-save', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>{{translate('table.immidiate_actions')}}</th>
                                <th>{{translate('table.root_cause_analysis')}}</th>
                                <th>{{translate('table.corrective_measure')}}</th>
                                <th>{{translate('table.effectiveness')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($finding_types as $k => $v)
                            <input type="hidden" id="finding_type_id" name="finding_type_id[]" value="{{$v->id}}">
                            <tr>
                                <td>{{$v->finding_type}}</td>
                                <td align="center">
                                    <div class="checkbox-custom checkbox-custom-extended checkbox-system mb25">
                                    <input id="checkboxExample{{$k+10}}" name="immidiate_actions[{{$k}}][]" value="1" type="checkbox" @if($v->immidiate_actions) checked @endif>
                                    <label for="checkboxExample{{$k+10}}"></label>
                                    </div>
                                </td>
                                <td align="center">
                                    <div class="checkbox-custom checkbox-custom-extended checkbox-system mb25">
                                    <input id="checkboxExample{{$k+20}}" name="root_cause_analysis[{{$k}}][]" value="1" type="checkbox" @if($v->root_cause_analysis) checked @endif>
                                    <label for="checkboxExample{{$k+20}}"></label>
                                    </div>
                                </td>
                                <td align="center">
                                    <div class="checkbox-custom checkbox-custom-extended checkbox-system mb25">
                                    <input id="checkboxExample{{$k+30}}" name="corrective_measure[{{$k}}][]" value="1" type="checkbox" @if($v->corrective_measure) checked @endif>
                                    <label for="checkboxExample{{$k+30}}"></label>
                                    </div>
                                </td>
                                <td align="center">
                                    <div class="checkbox-custom checkbox-custom-extended checkbox-system mb25">
                                    <input id="checkboxExample{{$k+40}}" name="effectiveness[{{$k}}][]" value="1" type="checkbox" @if($v->effectiveness) checked @endif>
                                    <label for="checkboxExample{{$k+40}}"></label>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row mt20 mb30">
                        <div class="col-md-offset-4 col-md-4 text-center">
                            {!! csrf_field() !!}
                            <input type="hidden" id="num_rows" name="num_rows" value="{{ count($finding_types) }}">
                            {!! Html::link(url('/audit/finding-type-list'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                            {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn')) !!}
                            <br clear="all">
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @stop