@extends('layouts.default')
@section('content')
<div class="row" id="audit-execute-form">
	<div class="col-md-12">
		<div class="panel panel-visible" id="spy2">
			<div class="panel-heading exhight mb25">
				<div class="panel-title hidden-xs">
					<div class="row">
						<div class="col-md-6">
							<h3>
								{{translate('words.audit_name')}} : {{$audit_detail['audit_name']}}
							</h3>
						</div>
						<div class="col-md-2">
							<h3>
								({{date('d-m-Y',strtotime($audit_detail['plan_date']))}})
							</h3>
						</div>
						<div class="col-md-4 text-right">
							<span class="fs18">
								{{translate('words.lead_auditor')}} : {{$audit_detail['lead_auditor']['name']}}
							</span>
							@if($audit_detail->status == 2)
							<a href="{{url('/audit/report-print/'.$audit_detail['id'])}}" class="btn btn-system ml5">
								<i class="fa fa-print"></i>
							</a>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="tab-block">
				<ul class="nav nav-tabs" id="myTab">
					<li class="active">
						<a href="#tab6_1" id="genTab" data-toggle="tab">{{translate('words.general')}}</a>
					</li>
					<li>
						<a href="#tab6_2" data-toggle="tab">{{translate('words.details')}}</a>
					</li>
				</ul>
				<div class="tab-content {{($audit_detail->status != 2) ? 'audittabContent' : ''}}">
					<div id="tab6_1" class="tab-pane admin-form active">
						{!! Form::open(['method' => 'post', 'url' => '/audit/audit-execute-general-save', 'autocomplete' => 'off']) !!}
						<div class="row">
							<div class="col-md-2 col-md-offset-2">
								{!! Form::label('reference_std', translate('form.reference_std'), array('class' => 'field-label fs15 mb5 fw700')) !!}
							</div>
							<div class="col-md-2">
								@inject('iso_no', 'App\Services\AuditExecuteForm')
								{{$audit_detail['audit_type'][$iso_no->iso_no(session('lang'))]}}
							</div>
							<div class="col-md-2">
								{!! Form::label('process_name', translate('form.process_name'), array('class' => 'field-label fs15 mb5 fw700')) !!}
							</div>
							<div class="col-md-2 Detailbadge">
								@foreach($audit_detail->process as $eachProcess)
								<span class="bg-primary text-default">{{$eachProcess->processDetail->name}}</span>
								@endforeach	
							</div>
						</div>
						<div class="row">
							<div class="col-md-2 col-md-offset-2">
								{!! Form::label('process_owner', translate('form.process_owner'), array('class' => 'field-label fs15 mb5 fw700')) !!}
							</div>
							<div class="col-md-2 Detailbadge">
								@foreach($audit_detail->process as $eachProcess)
								{{$eachProcess->processDetail->owner->name}}
								@if(!$loop->last) , @endif
								@endforeach
							</div>
						</div>
						<div class="row">
							<div class="col-md-2 col-md-offset-2">
								{!! Form::label('lead_auditor', translate('form.lead_auditor'), array('class' => 'field-label fs15 mb5 fw700')) !!}
							</div>
							<div class="col-md-2">
								{{$audit_detail['lead_auditor']->name}}
							</div>
							<div class="col-md-2">
								{!! Form::label('auditors', translate('form.auditors'), array('class' => 'field-label fs15 mb5 fw700')) !!}
							</div>
							<div class="col-md-2 Detailbadge">
								@foreach($audit_detail->auditors as $eachAuditor)
								{{$eachAuditor->user->name}}
								@if(!$loop->last) , @endif
								@endforeach	
							</div>
						</div>
						<br clear="all">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<select class="auditees" multiple="multiple" name="auditee[]">
									@foreach($auditees as $l=>$auditees)
									<option value="{{$l}}" @if(in_array($l,$auditees_selected)) selected @endif>
										{{$auditees}}
									</option> 
									@endforeach
								</select>
							</div>
						</div>
						<br clear="all">
						<div class="row">
							<div class="col-md-4 col-md-offset-2">
								<div class="section">
									{!! Form::label('visited_location', translate('form.visited_location'), array('class' => 'field-label fs15 mb5')) !!}
									<textarea class="gui-textarea" id="visited_location" name="visited_location" placeholder="{{translate('form.visited_location')}}">{{$audit_detail['visited_location']}}</textarea>
								</div>
							</div>
						</div>
						<br clear="all">
						{!! csrf_field() !!}
						{!! Form::hidden('hid', ($audit_detail['id']) ? $audit_detail['id'] : '') !!}
						{!! Form::hidden('audit_date', ($audit_detail['plan_date']) ? $audit_detail['plan_date'] : '') !!}
						@if($audit_detail->status != 2)
						{!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-system pull-right ml5']) !!}
						{!! Html::link(url('/home'), translate('form.cancel'), ['class' => 'btn btn-hover btn-system pull-right']) !!}
						@endif
						{!! Form::close() !!}
						<br clear="all">
						<br clear="all">
						@if(($audit_detail->status != 2) and ($audit_detail->choose_lead_auditor == Auth::guard('customer')->user()->id))
						{!! Form::button(translate('form.release_button'), ['class' => 'btn btn-system btn-lg pull-right', 'id' => 'exec-preview', 'data-audit' => $audit_detail['id']]) !!}
						@endif
					</div>
					<div id="tab6_2" class="tab-pane admin-form">
						@if($audit_detail->status != 2)
						{!! Form::open(array('method' => 'post', 'autocomplete' => 'off', 'class'=>'executionCls executionCls-extended')) !!}
						@else
						{!! Form::open(array('method' => 'post', 'autocomplete' => 'off', 'class'=>'executionCls')) !!}
						@endif
						@if($audit_detail->status != 2)
						<div class="row repeter" id="output-{{$execution_count+1}}">
							<div class="col-sm-2">
								{{ Form::textarea('observation[]', '', ['data-id' => ($execution_count+1), 'data-hid' => '', 'class' => 'gui-input referenceCls updateData', 'id' => 'observation-'.($execution_count+1), 'placeholder' => translate('form.observation')]) }}
							</div>
							<div class="col-sm-9">
								<div class="col-md-4 form-group">
									<label class="field select">
										{!! Form::select('iso[]', $iso_refs, $audit_detail['audit_type']['id'], ['data-id' => ($execution_count+1), 'data-hid' => '', 'class' => 'iso_ref updateData',  'id' => 'iso-'.($execution_count+1), 'data-row' => ($execution_count+1)]) !!}
										<i class="arrow"></i>
									</label>
								</div>
								<div class="col-sm-8 form-group">
									<label class="field select" id="reference-{{$execution_count+1}}">
										{!! Form::select('references[]', $references, '', ['data-id' => ($execution_count+1), 'data-hid' => '', 'id' => 'reference-sl-'.($execution_count+1), 'class' => 'updateData']) !!}
										<i class="arrow"></i>
									</label>
								</div>
								<div class="col-sm-3 form-group">
									<label class="field select" id="process-{{$execution_count+1}}">
										{!! Form::select('process[]', $porcessData, isset($selectedProcess) ? $selectedProcess : '', ['data-id' => ($execution_count+1), 'data-hid' => '', 'id' => 'process-sl-'.($execution_count+1), 'class' => 'updateData']) !!}
										<i class="arrow"></i>
									</label>
								</div>
								<div class="col-sm-4 form-group">
									<label class="field select">
										{!! Form::select('finding_type[]', $findingType, '', ['data-id' => ($execution_count+1), 'data-hid' => '', 'id' => 'finding-type-'.($execution_count+1), 'class' => 'updateData']) !!}
										<i class="arrow"></i>
									</label>
								</div>
								<div class="col-sm-3 form-group">
									<label class="field select">
										{!! Form::text('evidence[]', '', ['data-id' => ($execution_count+1), 'data-hid' => '', 'class' => 'gui-input updateData', 'id' => 'evidence-'.($execution_count+1), 'placeholder' => translate('form.evidence')]) !!}
									</label>
								</div>
								<div class="col-sm-2 form-group">
									@if($audit_detail->status != 2)
									<button type="button" class="btn executionDoc p5 ml5" id="executionDoc-{{$execution_count+1}}" data-id="" data-new="{{$execution_count+1}}">
										<i class="fa fa-upload"></i>
									</button>
									@endif
								</div>
							</div>
							<div class="col-sm-1">
								<div class="col-sm-4 col-xs-2 btn-span form-group pt30">
									@if($audit_detail->status!=2)
									<button type="button" class="btn btn-system p10 system-btn functions" data-id="{{$execution_count+1}}">
										<i class="fa fa-plus 2x"></i>
									</button>
									@endif
									{!! Form::hidden('id[]', '') !!}
								</div>
							</div>
						</div>
						@endif
						@if($execution_details->count() > 0)
						@foreach($execution_details as $each)		
						<div class="row repeter" id="output-{{$loop->iteration}}">
							<div class="col-sm-2">
								{{ Form::textarea('observation[]', $each->observation, ['data-id' => $loop->iteration, 'data-hid' => $each->id, 'class' => 'gui-input referenceCls updateData', 'id' => 'observation-'.$loop->iteration, 'placeholder' => translate('form.observation')]) }}
							</div>
							<div class="col-sm-9">
								<div class="col-sm-4 form-group">
									<label class="field select">
										{!! Form::select('iso[]', $iso_refs, $each->iso_no, ['data-id' => $loop->iteration, 'data-hid' => $each->id, 'class' => 'iso_ref updateData', 'id' => 'iso-'.$loop->iteration, 'data-row' => $loop->iteration]) !!}
										<i class="arrow"></i>
									</label>
								</div>
								<div class="col-sm-8 form-group">
									<label class="field select" id="reference-{{$loop->iteration}}">
										{!! Form::select('references[]', $each->references, $each->reference_id, ['data-id' => $loop->iteration, 'data-hid' => $each->id, 'class' => 'updateData', 'id' => 'reference-sl-'.$loop->iteration]) !!}
										<i class="arrow"></i>
									</label>
								</div>
								<div class="col-sm-3 form-group">
									<label class="field select" id="process-{{$loop->iteration}}">
										{!! Form::select('process[]', $porcessData, $each->process_id, ['data-id' => $loop->iteration, 'data-hid' => $each->id, 'class' => 'updateData', 'id' => 'process-sl-'.$loop->iteration]) !!}
										<i class="arrow"></i>
									</label>
								</div>
								<div class="col-sm-4 form-group">
									<label class="field select">
										{!! Form::select('finding_type[]', $findingType, $each->finding_type_id, ['data-id' => $loop->iteration, 'data-hid' => $each->id, 'class' => 'updateData', 'id' => 'finding-type-'.$loop->iteration]) !!}
										<i class="arrow"></i>
									</label>
								</div>
								<div class="col-sm-3 form-group">
									<label class="field select">
										{!! Form::text('evidence[]', $each->evidence, ['data-id' => $loop->iteration, 'data-hid' => $each->id, 'class' => 'gui-input updateData', 'id' => 'evidence-'.$loop->iteration, 'placeholder' => translate('form.evidence')]) !!}
									</label>
								</div>
								<div class="col-sm-2 form-group">
									@if($audit_detail->status != 2)
									<button type="button" class="btn p5 ml5 executionDoc" data-id="{{$each->id}}" data-new="" data-existingrow="{{$loop->iteration}}">
										<i class="fa fa-upload"></i>
									</button>
									@endif
									@if($each->docs->count() > 0)
									<span class="fa fa-file-o fa-2x icon-span pointer executionDocView" data-toggle="modal" data-target="#myModal" data-id="{{$each->id}}" class="pointer">
										<span class="text-danger icon-text">
											{{sprintf("%02d", $each->docs->count())}}
										</span>
									</span>
									@endif
								</div>
							</div>
							<div class="col-md-1">
								<div class="col-sm-4 col-xs-2 btn-span form-group pt10">
									@if($audit_detail->status != 2)
									<a href="{{url('/audit/audit-execute/remove/'.$each->id)}}" class="btn btn-danger p10 danger-btn" onclick="return confirm('{{translate('alert.are_you_sure')}}')">
										<i class="fa fa-minus"></i>
									</a>
									@endif
									
									{!! Form::hidden('id[]', $each->id) !!}
								</div>
							</div>
						</div>		
						@endforeach
						@endif
						
						<div class="row">
							<div class="col-md-6 pull-right">
								{!! csrf_field() !!}
								{!! Form::hidden('execution_count', $execution_details->count(), ['id' => 'execution_count']) !!}
								{!! Form::hidden('audit_id', ($audit_detail['id']) ? $audit_detail['id'] : '', ['id' => 'audit_id']) !!}
								@if($audit_detail->status != 2)
								{!! Html::link(url('/home'), translate('form.cancel'), ['class' => 'btn btn-hover btn-system pull-right']) !!}
								@endif
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header panel-heading bg-system p20">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{translate('form.documents')}}</h4>
			</div>
			<div class="modal-body text-center" id="docpanel">
				
			</div>
		</div>
	</div>
</div>

<div id="execution-preview" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header panel-heading bg-system p20">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{translate('words.execution_preview')}}</h4>
			</div>
			<div class="modal-body text-right">
				<span id="exec-preview-body" class="text-left">
					
				</span>
				{!! Form::open(['method' => 'post', 'url' => '/audit/audit-release', 'class' => 'form-inline']) !!}
				{!! csrf_field() !!}
				{!! Form::hidden('audit_id', ($audit_detail['id']) ? $audit_detail['id'] : '') !!}
				@if(Auth::guard('customer')->user()->type == 2)
				<div class="row">
					<div class="col-md-9 admin-form">
						  <div class="row">
						  	 <div class="col-md-4">
						  	 	{!! Form::label('email', translate('words.release_date'), array('class' => 'pt10')) !!}
						  	 </div>
						  	 <div class="col-md-8">
						  	 	 <label class="field append-icon">
							   	{!! Form::text('release_date', date('d-m-Y'), ['class' => 'form-control pointer widthFull', 'id' => 'release_date', 'autocomplete' => 'off'] ) !!}
		                        <label for="release_date" class="field-icon">
									<i class="fa fa-calendar"></i>
								</label>
							</label>
						  	 </div>
						  </div>
					</div>
				</div>
				@endif
				{!! Form::button(translate('words.not_ok'), ['class' => 'btn btn-system btn-sm', 'data-dismiss' => 'modal']) !!}
				{!! Form::submit(translate('words.ok'), ['class' => 'btn btn-system btn-sm']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@include('includes.scripts.audit.audit_execute_form_script')
@stop