@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/audit/finding-type-form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/audit/finding-type-list'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.audit_finding_type')}}</span>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('audit_finding_type', translate('form.audit_finding_type'), array('class' => 'col-lg-5 col-md-5 col-sm-5 control-label required')) !!}
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="section">
                            {!! Form::text('audit_finding_type',(isset($audit_finding_type->finding_type)) ? $audit_finding_type->finding_type : old('finding_type'), array('class' => 'gui-input', 'id' => 'audit_finding_type', 'placeholder' => translate('form.text'), 'required' => 'required', 'tabindex' => '1')) !!}
                            <p class="text-danger">{{ Session::get('audit_type_error') }}</p>
                        </div>
                    </div>
                </div>
            </div>		
            <div class="section-divider mv40" id="spy2">
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    <input type="hidden" id="hid" name="hid" value="{{ $audit_finding_type->id or '' }}">
                    {!! Html::link(url('/audit/finding-type-list'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn')) !!}
                    <br clear="all">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
