@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    {!! Html::link(url('/audit/action-matrix'), translate('words.action_matrix'), ['class' => 'btn btn-system text-default']) !!}
                    {!! Html::link(url('/audit/finding-type-form'), translate('words.add'), ['class' => 'btn btn-system text-default']) !!}
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.name')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($finding_types as $k => $v)
                        <tr>
                            <td>
                                {{$v->finding_type}}
                            </td>
                            <td>
                                <a class="btn btn-default" href="{{ url('/audit/finding_type_edit/'.$v->id) }}">
                                    <span class="text-success fa fa-pencil"></span> {{translate('words.edit')}}
                                </a>
                                @if($v->type != 1)
                                    <a class="btn btn-default" href="{{ url('/audit/finding-type-delete/'.$v->id) }}" onclick="return confirm('{{translate('alert.are_you_sure')}}')">
                                        <span class="text-danger fa fa-trash"></span> {{translate('words.remove')}}
                                    </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop