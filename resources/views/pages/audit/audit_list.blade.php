@extends('layouts.default')
@section('content')
<div class="form-group mt15 disp-flex">
    <div class="form-group posfilter disp-flex pull-right">
    <span>{{ucfirst(translate('words.filter'))}} :</span>
    {!! Form::select('branch', $branches, isset($selectedBranch) ? $selectedBranch : '', ['class' => 'form-control w200 ml5 issuecategory', 'id' => 'branch-opt']) !!}
    </div>
    <input type="hidden" id="selectedYear" value ="{{$selectedYear}}"/>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-left">
                    <h3>{{translate('table.audit_program_audit_plan')}} ({{translate('words.internal')}})</h3>
                </div>
            </div>
            <div class="panel-body pn">
                <div class="head clearfix">
                    <div class="period">
                        <a href="{{url('/audit/planning-list/'.$selectedBranch.'/'.$prvYear)}}" class="prev w-a"><span>&lt; {{translate('words.prev')}}</span></a>
                        <span class="month">{{ $curYear }}</span>
                        <a href="{{url('/audit/planning-list/'.$selectedBranch.'/'.$nxtYear)}}" class="next w-a"><span>{{translate('words.next')}} &gt;</span></a>
                        <div class="legend"> 
                            <span class="cross">{{ucfirst(translate('words.pre_planned'))}}</span>
                            <span class="dtecls">{{ucfirst(translate('words.date_fixed'))}}</span>
                            <span class="orangecls">{{ucfirst(translate('words.under_execution'))}}</span>
                            <span class="greencls">{{ucfirst(translate('words.finished_sudit'))}}</span> 
                        </div>
                    </div>
                    @if(!empty($plannedAudit))
                    <span class="text-danger">{{translate('words.click_to_add_audit')}}</span>
                    @endif
                </div>
                <div class="calendar table-responsive">
                    <table class="table table-bordered seperated">
                        <thead>
                            <tr class="bg-system">
                                <td>{{ucfirst(translate('words.process'))}}</td>
                                @foreach($months as $month)
                                <td>{{$month}}</td>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($plannedAudit as $eachAudit)
                            <tr>
                                <td>{{$eachAudit['process']}}</td>
                                @foreach($eachAudit['months'] as $eachMonth)
                                <td class="{{$eachMonth['tdCls']}}">
                                    <a href="{{$eachMonth['link']}}" title="{{$eachMonth['title']}}" class="{{$eachMonth['aCls']}}">{{$eachMonth['auditDate']}}</a>
                                </td>
                                @endforeach
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-left">
                    <h3>{{translate('table.audit_program_audit_plan')}} ({{translate('words.external')}})</h3>
                </div>
            </div>
            <div class="panel-body pn">
                <div class="head clearfix">
                    <div class="period">
                        <a href="{{url('/audit/planning-list/'.$selectedBranch.'/'.$prvYear)}}" class="prev w-a"><span>&lt; {{translate('words.prev')}}</span></a>
                        <span class="month">{{ $curYear }}</span>
                        <a href="{{url('/audit/planning-list/'.$selectedBranch.'/'.$nxtYear)}}" class="next w-a"><span>{{translate('words.next')}} &gt;</span></a>
                        <div class="legend"> 
                            <span class="cross">{{ucfirst(translate('words.pre_planned'))}}</span>
                            <span class="dtecls">{{ucfirst(translate('words.date_fixed'))}}</span>
                            <span class="orangecls">{{ucfirst(translate('words.under_execution'))}}</span>
                            <span class="greencls">{{ucfirst(translate('words.finished_sudit'))}}</span> 
                        </div>
                    </div>
                    @if(!empty($plannedExternalAudits))
                    <span class="text-danger">{{translate('words.click_to_add_audit')}}</span>
                    @endif
                </div>
                <div class="calendar table-responsive">
                    <table class="table table-bordered seperated">
                        <thead>
                            <tr class="bg-system">
                                <td>{{ucfirst(translate('words.process'))}}</td>
                                @foreach($months as $month)
                                <td>{{$month}}</td>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($plannedExternalAudit as $eachAudit)
                            <tr>
                                <td>{{$eachAudit['process']}}</td>
                                @foreach($eachAudit['months'] as $eachMonth)
                                <td class="{{$eachMonth['tdCls']}}">
                                    <a href="{{$eachMonth['link']}}" title="{{$eachMonth['title']}}" class="{{$eachMonth['aCls']}}">{{$eachMonth['auditDate']}}</a>
                                </td>
                                @endforeach
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {    
    $(document).on('change', '#branch-opt', function() {
        var branchId = $(this).val();
        var selectedYear = $('#selectedYear').val();
        if(selectedYear != ''){
            $(location).attr('href', '{{url("/audit/planning-list")}}/'+branchId+'/'+selectedYear);
        } else{
            $(location).attr('href', '{{url("/audit/planning-list")}}/'+branchId);
        }
    });
});

</script>
@stop