@extends('layouts.default')
@section('content')
<div class="admin-form" id="audit-prepare-edit">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/audit/preparation-save', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/home'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
            <div class="section-divider mb20 mt30" id="spy1">
                <span>{{translate('form.audit_preparation')}}</span>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section auditPreparation">
                        {!! Form::label('audit_title', translate('form.audit_title'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field append-icon">
                            @if($plannedAudits->count() > 0)
                            {{$plannedAudits->audit_name}}
                            @endif
                        </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section auditPreparation">
                        {!! Form::label('lead_auditor', translate('form.lead_auditor'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field append-icon">
                            @if($plannedAudits->count() > 0)
                            {{$plannedAudits->lead_auditor->name}}
                            @endif
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel-body p1 memberbox">
                        <select class="internal_auditor" multiple="multiple" name="internal_auditor[]">
                            @foreach($internalAuditors as $k=>$auditor)
                            <option value="{{$k}}" @if(in_array($k,$internalAuditors_selected)) selected @endif>
                                    {{$auditor}}
                            </option> 
                            @endforeach
                        </select> 
                    </div>	
                </div>
            </div>
            <div class="row mt10">
                <div class="col-md-8 col-md-offset-2 Detailbadge">
                    <div class="section audit-edt auditPreparation">
                        {!! Form::label('process_to_be_audited', translate('form.process_to_be_audited'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field append-icon">
                            @foreach($selectedProcessNames as $eachProcess)
                                <span class="bg-primary pr5 mr5 pl5 text-default">{{$eachProcess}}</span>
                            @endforeach
                        </label>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section audit-edt">
                        {!! Form::label('objective', translate('form.objective'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            <textarea class="gui-textarea" id="objective" name="objective" placeholder="Objective" required>{{$plannedAudits->objective}}</textarea>
                            <label for="comment" class="field-icon">
                                <i class="fa fa-comments"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('porcess', translate('error.language_req'))}}</p>
                    </div>
                </div>	
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('choose_audit_date', translate('form.choose_audit_date') .'('.$plannedAudits->plan_month.'/'.$plannedAudits->plan_year.')', array('class' => 'field-label fs15 mb5 required')) !!}
                        <label for="monthpicker1" class="field prepend-icon">
                            <input type="text" name="audit_date" class="form-control" id="datetimepicker1" value="{{$plannedAudits->plan_date or ''}}" required="">
                            <label class="field-icon">
                                <i class="fa fa-calendar-o"></i>
                            </label>
                        </label>
                    </div> 
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('choose_audit_time', translate('form.choose_audit_time'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label for="monthpicker1" class="field prepend-icon">
                            <input type="text" name="audit_time" class="form-control" id="timepicker1" value="{{$plannedAudits->plan_time}}" required="">
                            <label class="field-icon">
                                <i class="fa fa-clock-o"></i>
                            </label>
                        </label>
                    </div>
                </div>	
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel-body p1 memberbox">
                        <select class="auditees" multiple="multiple" name="auditee[]">
                            @foreach($auditees as $l => $auditees)
                                <option value="{{$l}}" @if(in_array($l,$auditeesSelected)) selected @elseif(in_array($l,$selectedProcessOwner)) selected @endif>
                                    {{$auditees}}
                                </option> 
                            @endforeach
                        </select>
                    </div>	
                </div>
            </div>
            <div class="row mt10">
                <div class="col-md-8 col-md-offset-2">
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" value="1" name="invitation">{{translate('words.invitation_sent')}}
                    </label>
                </div>
            </div>
            <div class="section-divider mb20 mt30" id="spy2">
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', isset($plannedAudits->id) ? $plannedAudits->id: '', array('id' => 'hid'))!!}
                    {!! Form::hidden('compid', isset($plannedAudits->company_id) ? $plannedAudits->company_id : '') !!}
                    {!! Form::hidden('selectedMonth', $plannedAudits->plan_month, array('id'=>'selectedMonth')) !!}
                    {!! Form::hidden('selectedYear', $plannedAudits->plan_year, array('id'=>'selectedYear')) !!}
                    @if($editable)
                    {!! Html::link(url('/home'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn')) !!}
                    @endif
                    <br clear="all">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {   
    $("#datetimepicker1").keypress(function(event) {event.preventDefault();});
    $("#timepicker1").keypress(function(event) {event.preventDefault();});
    @if(!$editable)
        $(document).ready(function() {
            $(document).find('.filter').attr('disabled', true);
            $(document).find('.moveall').attr('disabled', true);
            $(document).find('.removeall').attr('disabled', true);
            $(document).find('input[type="text"]').attr('disabled', true);
            $(document).find('textarea').attr('disabled', true);
            $(document).find('select').attr('disabled', true);
            $("#inlineCheckbox1").prop("disabled", true);
        });
    @endif
})
</script>
@stop
