@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/audit/source-type-form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/audit/source-type-list'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.audit_source_type')}}</span>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-2">
                        <div class="section">
                            {!! Form::label('audit_source_type', translate('form.audit_source_type'), array('class' => 'control-label required')) !!}
                            {!! Form::text('audit_source_type',(isset($audit_source_type->source_type)) ? $audit_source_type->source_type : old('source_type'), array('class' => 'gui-input', 'id' => 'audit_source_type', 'placeholder' => translate('form.text'), 'required' => 'required', 'tabindex' => '3')) !!}
                        </div>
                    </div>
                </div>
            </div>
            @if(empty($audit_source_type) || $audit_source_type->type != 1 )
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel-body p1 memberbox">
                        <select class="user_access" multiple="multiple" name="user_access[]">
                            @foreach($users as $k=>$user)
                            <option value="{{$k}}" @if(in_array($k,$usersSelected)) selected @endif>
                                    {{$user}}
                            </option> 
                            @endforeach
                        </select> 
                    </div>  
                </div>
            </div>
            @endif  	
            <div class="section-divider mv40" id="spy2">
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    <input type="hidden" id="hid" name="hid" value="{{ $audit_source_type->id or '' }}">
                    {!! Html::link(url('/audit/source-type-list'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn')) !!}
                    <br clear="all">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
