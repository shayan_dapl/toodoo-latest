@extends('layouts.default')
@section('content')
@inject('assignedusers', 'App\Services\ReportingSource')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    <button type="button" class="btn btn-hover btn-system" onclick="location.href ='{{url('/audit/source-type-form')}}'">{{ucfirst(translate('words.add'))}}</button>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.name')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($source_types as $eachSourceType)
                        <tr>
                            <td>
                                {{$eachSourceType->source_type}}
                                @if($eachSourceType->assignedusers()->count()>0)
                                <i class="fa fa-users" data-html="true" data-toggle="tooltip" data-placement="top" title="{{$assignedusers->assignedusers($eachSourceType->assignedusers)}}"></i>
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-default" href="{{ url('/audit/source-type-edit/'.$eachSourceType->id) }}">
                                    <span class="text-success fa fa-pencil"></span>
                                </a>
                                @if($eachSourceType->deletable == 1 )
                                <a class="btn btn-default" href="{{ url('/audit/source-type-delete/'.$eachSourceType->id) }}" onclick="return confirm('{{translate('alert.are_you_sure')}}')">
                                    <span class="text-danger fa fa-trash"></span>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop