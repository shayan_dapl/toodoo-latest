@extends('layouts.default')
@section('content')
<div class="admin-form" id="audit-type-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/audit/type/save', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/audit/type/'.$ref_id), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>

            <div class="section-divider mb40" id="spy1">
                <span>{{translate('words.audit_type')}}</span>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('iso_reference', translate('form.iso_reference'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field select">
                            {!! Form::select('iso_reference',$isoReference,(isset($auditType->iso_ref_id)) ? $auditType->iso_ref_id : $selectedAuditType, ['id' => 'iso_reference', 'required' => 'required','tabindex' => '1']) !!}
                            <i class="arrow"></i>
                        </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('parent_type', translate('form.clause_parent'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field select" id="parent_type_import">
                            <select name="parent_audit_type" id="parent_type" tabindex="2">
                                @foreach($parentAuditType as $k => $v)
                                    <option value="{{$k}}" class="@if(in_array($k, $topTypes))fw700 @endif" @if(isset($auditType->parent_id) and ($auditType->parent_id == $k)) selected @endif>
                                        {{$v}}
                                    </option>
                                @endforeach
                            </select>
                            <i class="arrow"></i>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('clause', translate('form.clause_nr'), array('class' => 'field-label fs15 mb5 required')) !!}

                        {!! Form::text('clause',(isset($auditType->clause)) ? $auditType->clause : old('clause'), array('class' => 'gui-input', 'id' => 'clause', 'placeholder' => translate('form.clause_nr'), 'required' => 'required', 'tabindex' => '3')) !!}

                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('clause_nl')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <label>&nbsp;</label>
                    <div class="tab-block mb25">
                        <ul class="nav nav-tabs">
                            <li class="lang-links Nederlands @if(session('lang') == 'nl') active @endif">
                                <a href="#Nederlands" data-toggle="tab">{{translate('words.nl')}}</a>
                            </li>
                            <li class="lang-links English @if(session('lang') == 'en') active @endif">
                                <a href="#English" data-toggle="tab">{{translate('words.en')}}</a>
                            </li>
                            <li class="lang-links Francais @if(session('lang') == 'fr') active @endif">
                                <a href="#Francais" data-toggle="tab">{{translate('words.fr')}}</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="Nederlands" class="tab-pane @if(session('lang') == 'nl') active @endif">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('name_nl', translate('form.clause'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            {!! Form::text('name_nl',(isset($auditType->name_nl)) ? $auditType->name_nl : old('name_nl'), array('class' => 'gui-input', 'id' => 'name_nl', 'placeholder' => translate('form.clause'), 'required' => 'required', 'tabindex' => '3')) !!}
                                        </div>
                                    </div>
                                </div>	
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('description_nl', translate('form.clause_desc'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            <label class="field prepend-icon">
                                                {{ Form::textarea('description_nl', (isset($auditType->description_nl) ? $auditType->description_nl : old('description_nl')), ['class' => 'gui-textarea','id'=>'description_nl','required' => 'required', 'placeholder' => translate('form.clause_desc'), 'tabindex' => '4']) }}
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="English" class="tab-pane @if(session('lang') == 'en') active @endif">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('name_en', translate('form.clause'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            {!! Form::text('name_en',(isset($auditType->name_en)) ? $auditType->name_en : old('name_en'), array('class' => 'gui-input', 'id' => 'name_en', 'placeholder' => translate('form.clause'), 'required' => 'required', 'tabindex' => '3')) !!}
                                        </div>
                                    </div>
                                </div>	
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('description_en', translate('form.clause_desc'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            <label class="field prepend-icon">
                                                {{ Form::textarea('description_en', (isset($auditType->description_en) ? $auditType->description_en : old('description_en')), ['class' => 'gui-textarea','id'=>'description_en','required' => 'required', 'placeholder' => translate('form.clause_desc'), 'tabindex' => '4']) }}
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Francais" class="tab-pane @if(session('lang') == 'fr') active @endif">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('name_fr', translate('form.clause'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            {!! Form::text('name_fr',(isset($auditType->name_fr)) ? $auditType->name_fr : old('name_fr'), array('class' => 'gui-input', 'id' => 'name_fr', 'placeholder' => translate('form.clause'), 'required' => 'required', 'tabindex' => '3')) !!}
                                        </div>
                                    </div>
                                </div>	
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('description_fr', translate('form.clause_desc'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            <label class="field prepend-icon">
                                                {{ Form::textarea('description_fr', (isset($auditType->description_fr) ? $auditType->description_fr : old('description_fr')), ['class' => 'gui-textarea','id'=>'description_fr','required' => 'required', 'placeholder' => translate('form.clause_desc'), 'tabindex' => '4']) }}
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>			                
            <div class="section-divider mv40" id="spy2"></div>

            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    <input type="hidden" id="hid" name="hid" value="{{ $auditType->id or '' }}">
                    {!! Form::button(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'audit-type-save-btn', 'tabindex' => '5')) !!}
                    {!! Html::link(url('/audit/type/'.$ref_id), translate('form.cancel'), array('class' => 'btn btn-hover btn-system', 'tabindex' => '5')) !!}
                    {!! Form::submit(translate('form.release_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'audit-type-btn', 'tabindex' => '5')) !!}
                    <br clear="all">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('includes.scripts.audit_type_form_script')
@stop