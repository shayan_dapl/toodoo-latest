@extends('layouts.default')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-visible" id="spy2">
			<div class="panel-heading exhight">
				<div class="panel-title hidden-xs pull-left fw700">
					{{$isoDetail->iso_no_en}}
				</div>
				<div class="panel-title hidden-xs pull-right">
					{!! Html::link(url('/audit/type/form/'.$isoDetail->id), translate('words.add'), array('class' => 'btn btn-hover btn-system')) !!}
				</div>
			</div>
			<div class="panel-body">
				<table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>{{translate('table.clause')}}</th>
							<th>{{translate('table.parent_clause')}}</th>
							<th>{{translate('table.status')}}</th>
							<th>{{translate('table.action')}}</th>
						</tr>
					</thead>
					<tbody>
						@inject('status', 'App\Services\AuditTypeList')
						@inject('clause', 'App\Services\AuditTypeList')								
						@foreach($auditTypes as $auditType)
							@if($auditType->status != 3)
								<tr>
									<td>{{$clause->clause(session('lang'),$auditType)}}</td>
									<td>{{$auditType->parent}}</td>
									<td>{{$status->status($auditType->status)}}</td>
									<td>
										<a href="{{ url('/audit/type/edit/'.$auditType->id) }}" class="btn btn-default">
                                    	<span class="text-success fa fa-pencil"></span>
		                                </a>
		                                <a href="{{ url('/audit/type/delete/'.$auditType->id) }}" onclick="return confirm('{{translate('alert.are_you_sure')}}')" class="btn btn-default">
		                                    <span class="text-danger fa fa-times"></span>
		                                </a>
									</td>
								</tr>
							@endif
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop