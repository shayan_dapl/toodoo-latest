@if($data)
<div class="panel-heading exhight mb20">
    <div class="panel-title hidden-xs">
        <h3 class="text-center">
            <span class="pull-left">
                {{translate('words.view')}}
            </span>
        </h3>
    </div>
</div>
@if($data->count() > 0)
@inject('file', 'App\Services\ExecDocumentForm')
<ul class="docContainercls">
    @foreach($data as $eachDoc)
    <li>
        <span class="doctypecls"><i class="fa {{$file->file($eachDoc->doc_name)['icon']}} fa-3x text-system-faded"></i>
            <a href="{{$file->file($eachDoc->doc_name)['url']}}" target="_blank">
                <i class="fa fa-eye fa-1x doc-del-btn" aria-hidden="true"></i>
            </a>
        </span>
        <p>{{$eachDoc->doc_name}}</p>
    </li>
    @endforeach
</ul>
@else
<h2>{{translate('table.no_data')}}</h2>
@endif

@endif

@if(!isset($noShowForm))
<div class="panel-heading exhight mt20">
    <div class="panel-title hidden-xs">
        <h3 class="text-center">
            <span class="pull-left">
                {{translate('words.add')}}/{{translate('words.edit')}}
            </span>
        </h3>
    </div>
</div>

{!! Form::open(['method' => 'post', 'url' => '/audit/audit-execute-general-save', 'autocomplete' => 'off']) !!}
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            <div class="row">
                <div class="form-group">
                    {!! Form::label('user_photo', translate('form.upload_doc'), array('class' => 'col-lg-3 col-md-3 control-label p10')) !!}
                    <div class="col-md-7">
                        <div class="section">
                            <label class="field prepend-icon append-button file" id="root">
                                <span class="button">{{translate('form.file')}}</span>
                                {!! Form::file('upload_doc', array('class' => 'gui-file', 'id' => 'docFile')) !!}
                                {!! Form::text('', '', array('class' => 'gui-input', 'placeholder' => translate('form.image'), 'id' => 'fileName')) !!}
                                <label class="field-icon">
                                    <i class="fa fa-upload"></i>
                                </label>
                            </label>
                            {!! Form::hidden('', '', ['id' => 'fileData']) !!}
                            {!! Form::hidden('', '', ['id' => 'fileExt']) !!}
                        </div>
                    </div>
                    <div class="col-md-2 p5">
                        {!! csrf_field() !!}
                        {!! Form::hidden('hid', $hid, ['id' => 'hid']) !!}
                        {!! Form::hidden('new', $row, ['id' => 'new']) !!}
                        {!! Form::hidden('evidence', $evidence, ['id' => 'evidence']) !!}
                        {!! Form::button(translate('form.upload'), ['class' => 'btn btn-system', 'id' => 'docSave']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endif
<script type="text/javascript">
    function renderImg(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#fileData").val(e.target.result);
                $("#fileName").val(input.files[0].name);
                $("#fileExt").val(input.files[0].type);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#docFile").change(function () {
        renderImg(this);
    });
</script>