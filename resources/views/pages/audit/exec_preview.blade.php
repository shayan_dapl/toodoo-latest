@if(!empty($findings))
	<div class="turtleBox">
		<ul>
			@foreach($findings as $each)
				<li>{{$each->finding_type}} : {{$each->executedTypes($audit)}}</li>
			@endforeach
		</ul>
	</div>
@endif