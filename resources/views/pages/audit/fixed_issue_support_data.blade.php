<label>
	{{translate('form.taken_action_description')}} : 
</label>
@if(!empty($data->action_description))
	{{$data->action_description}}
@else
	{{translate('table.no_data')}}
@endif

<hr class="mt20 mb20">
	<label>
		{{translate('form.upload_doc')}} :
	</label>
@if(!empty($data->supporting_doc))
	@inject('file', 'App\Services\FixedIssueSupportData')

	<a href="{{$file->file($data->supporting_doc)['url']}}" target="_blank">
	    <i class="fa {{$file->file($data->supporting_doc)['icon']}} fa-3x text-system-faded"></i>
	</a>
	<p>{{$data->supporting_doc}}</p>
@else
	{{translate('table.no_data')}}
@endif

<hr class="mt20 mb20">
<label>
	{{translate('form.fixed_on')}} : {{$data->created_at}}
</label>