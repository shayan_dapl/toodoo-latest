@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/audit/iso/form', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/audit/iso/list'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('words.iso_clause')}}</span>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="section">
                        {!! Form::label('iso_no_nl', (translate('words.iso_clause') . ' ('.translate('words.nl').')'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('iso_no_nl',(isset($details->iso_no_nl)) ? $details->iso_no_nl : old('iso_no_nl'), array('class' => 'gui-input', 'id' => 'iso_no_nl', 'placeholder' => translate('words.iso_clause'), 'required' => 'required', 'tabindex' => '1')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('iso_no_nl')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="section">
                        {!! Form::label('iso_no_en', (translate('words.iso_clause') . ' ('.translate('words.en').')'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('iso_no_en',(isset($details->iso_no_en)) ? $details->iso_no_en : old('iso_no_en'), array('class' => 'gui-input', 'id' => 'iso_no_en', 'placeholder' => translate('words.iso_clause'), 'required' => 'required', 'tabindex' => '2')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('iso_no_en')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="section">
                        {!! Form::label('iso_no_fr', (translate('words.iso_clause') . ' ('.translate('words.fr').')'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('iso_no_fr',(isset($details->iso_no_fr)) ? $details->iso_no_fr : old('iso_no_fr'), array('class' => 'gui-input', 'id' => 'iso_no_fr', 'placeholder' => translate('words.iso_clause'), 'required' => 'required', 'tabindex' => '3')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('iso_no_fr')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="section-divider mv40" id="spy2">
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', isset($details) ? $details->id : '', ['id' => 'hid']) !!}
                    {!! Html::link(url('/audit/iso/list'), translate('form.cancel'), ['class' => 'btn btn-hover btn-system', 'tabindex' => '5']) !!}
                    {!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'id' => 'profile-btn', 'tabindex' => '4']) !!}
                    <br clear="all">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
