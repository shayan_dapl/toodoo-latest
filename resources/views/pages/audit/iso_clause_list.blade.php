@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    {!! Html::link(url('/audit/iso/form'), translate('words.add'), ['class' => 'btn btn-system text-default pointer']) !!}
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('words.iso_clause')}}</th>
                            <th>{{translate('table.action')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('iso' , App\Services\IsoClauseList)
                        @foreach($details as $each)
                        <tr>
                            <td>{{$iso->iso(session('lang'),$each)}}</td>
                            <td>
                                <a href="{{ url('/audit/type/'.$each->id) }}" class="btn btn-default" title="{{translate('words.manage_clause')}}">
                                    <span class="text-system fa fa-cog"></span>
                                </a>
                                <a href="{{ url('/audit/iso/edit/'.$each->id) }}" class="btn btn-default">
                                    <span class="text-success fa fa-pencil"></span>
                                </a>
                                <a href="{{ url('/audit/iso/delete/'.$each->id) }}" class="btn btn-default" onclick="return confirm('{{translate('alert.are_you_sure')}}')">
                                    <span class="text-danger fa fa-trash"></span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop