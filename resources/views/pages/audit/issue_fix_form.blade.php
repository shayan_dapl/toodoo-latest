@extends('layouts.default')
	@section('content')
	<div class="admin-form">
	    <div class="panel">
	        <div class="panel-body bg-light">
	            <a href="{{url('/home')}}" class="btn btn-system text-default pull-right">
	                {{translate('form.cancel')}}
	            </a>
	            {!! Form::open(array('method' => 'post', 'url' => '/audit/report-issue-fixing/'.$id, 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) !!}
	            <div class="section-divider mb40" id="spy1">
	                <span>{{translate('form.report_delegation')}}</span>
	            </div>

	            <div class="row">
	                <div class="form-group">
	                    {!! Form::label('source_type', translate('form.reporting_source').' :', array('class' => 'col-md-2 text-right')) !!}
	                    <div class="col-md-2">
	                        {{$data->issues->source->source_type}}
	                    </div>
	                    {!! Form::label('source_type', translate('form.finding_type').' :', array('class' => 'col-md-2 text-right')) !!}
	                    <div class="col-md-2">
	                        {{$data->issues->reference}}
	                    </div>
	                    {!! Form::label('source_type', translate('form.action_taken_by').' :', array('class' => 'col-md-2 text-right')) !!}
	                    <div class="col-md-2">
	                        {{$data->user->name}}
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="form-group">
	                    {!! Form::label('source_type', translate('form.issue').' :', array('class' => 'col-md-2 text-right')) !!}
	                    <div class="col-md-2">
	                        {{$data->issue}}
	                    </div>
	                    {!! Form::label('source_type', translate('form.action').' :', array('class' => 'col-md-2 text-right')) !!}
	                    <div class="col-md-2">
	                        {{$data->action}}
	                    </div>
	                    {!! Form::label('source_type', translate('form.deadline').' :', array('class' => 'col-md-2 text-right')) !!}
	                    <div class="col-md-2">
	                        {{date(Config::get('settings.dashed_date'), strtotime($data->deadline))}}
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="form-group">
	                    {!! Form::label('reported_by', translate('form.reported_by').' :', array('class' => 'col-md-2 text-right')) !!}
	                    <div class="col-md-2">
	                        {{$data->reportedBy->name}}
	                    </div>
	                </div>
	            </div>
	            <div class="section-divider mb40" id="spy1">
	                
	            </div>
	            <div class="row">
	                <div class="form-group">
	                    {!! Form::label('source_type', translate('form.taken_action_description').' :', array('class' => 'col-md-3 text-right')) !!}
	                    <div class="col-md-3">
	                        {!! Form::textarea('action_description', '', ['class' => 'form-control', 'placeholder' => translate('form.taken_action_description'), 'required' => 'required']) !!}
	                    </div>
	                    {!! Form::label('source_type', translate('form.upload_doc').' :', array('class' => 'col-md-2 text-right')) !!}
	                    <div class="col-md-3">
	                        <label class="field prepend-icon append-button file">
	                            <span class="button">{{translate('form.file')}}</span>
	                            {!! Form::file('upload_doc', array('class' => 'gui-file', 'onChange' => 'document.getElementById("uploader1").value = this.files[0].name;')) !!}
	                            {!! Form::text('', '', array('class' => 'gui-input', 'id' => 'uploader1', 'placeholder' => translate('form.image'))) !!}
	                            <label class="field-icon">
	                                <i class="fa fa-upload"></i>
	                            </label>
	                        </label>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="form-group">
	                	<div class="col-md-3 col-md-offset-9">
	                		{!! Form::hidden('hid', $id) !!}
	                		{!! csrf_field() !!}
	                		{!! Form::submit(translate('form.closed'), array('class' => 'btn btn-system text-default', 'id' => 'profile-btn')) !!}
	                	</div>
	                </div>
	            </div>
	            {!! Form::close() !!}
	        </div>
	    </div>
	</div>
@stop