@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/audit/planning-save', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off', 'id' => 'planning-form')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/audit/planning-list/all'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.audit_define')}}</span>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('category', translate('form.audit_category'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field select">
                            {!! Form::select('audit_category',$audit_category, isset($auditPlan) ? $auditPlan->category : $currentAuditcat, ['id' => 'audit_category', 'required' => 'required', 'tabindex' => '1']) !!}
                            <i class="arrow"></i>
                        </label>
                        <p class="text-danger mn">{{$errors->first('audit_category')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                     <div class="section">
                        {!! Form::label('audit_title', translate('form.audit_title'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('audit_title',isset($auditPlan) ? $auditPlan->audit_name : 'Audit '.$selected_process_title, ['class' => 'gui-input', 'id' => 'audit_title', 'placeholder' => translate('form.audit_title'), 'required' => 'required', 'tabindex' => '2']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('audit_title')}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                   <div class="section branch">
                        {!! Form::label('branch', translate('form.branch'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field select">
                            {!! Form::select('branch',$branches, isset($auditPlan) ? $auditPlan->branch : old('process_to_audit'), ['class'=>'select2-single select-branch error','id' => 'branch','required' => 'required', 'tabindex' => '3']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('branch')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('choose_process', translate('form.choose_process'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field select">
                            {!! Form::select('choose_process[]',$porcess, isset($auditPlan) ? $auditPlan->process_to_audit : ((isset($selected_process)) ? $selected_process : old('process_to_audit')), ['class' => 'select2-single select-process error', 'id' => 'porcess_audit', 'required' => 'required', 'multiple' => 'multiple', 'tabindex' => '4']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('choose_process[]')}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('iso_ref', translate('form.iso_reference'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field select">
                            {!! Form::select('iso_ref', $audit_type, isset($auditPlan) ? $auditPlan->type_of_audit : $getBasePackageIso, ['id' => 'audit_type','required' => 'required', 'tabindex' => '5']) !!}
                            <i class="arrow"></i>
                        </label>
                        <p class="text-danger mn">{{$errors->first('iso_ref')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('choose_audit_month', translate('form.choose_audit_month'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label for="monthpicker1" class="field prepend-icon">
                            <input type="text" id="monthpicker1" name="choose_audit_month" class="gui-input" placeholder="Choose audit Month" value="{{$monthyear}}" required="required" tabindex="6">
                            <label class="field-icon">
                                <i class="fa fa-calendar-o"></i>
                            </label>
                        </label>
                         <p class="text-danger mn">{{$errors->first('choose_audit_month')}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('choose_lead_auditor', translate('form.choose_lead_auditor'), array('class' => 'field-label fs15 mb5 required','id' => 'lead_auditor')) !!}

                        {!! Form::label('choose_lead_auditor', translate('form.choose_audit_responsible'), array('class' => 'field-label fs15 mb5 required' ,'id' => 'audit_responsible')) !!}
                        <label class="field select">
                            {!! Form::select('choose_lead_auditor', $auditors,isset($auditPlan) ? $auditPlan->choose_lead_auditor : old('choose_lead_auditor'), ['class'=>'select2-single select-leadauditor','id' => 'choose_lead_auditor','required' => 'required', 'tabindex' => '7']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('choose_lead_auditor')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section hide" id="company">
                        {!! Form::label('audit_company', translate('form.audit_company'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('audit_company',isset($auditPlan) ? $auditPlan->audit_company : old('audit_company'), ['class' => 'gui-input', 'id' => 'audit_company', 'placeholder' => translate('form.audit_company'), 'tabindex' => '8']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('audit_company')}}</p>
                    </div> 
                </div>	
            </div>
            <div class="section-divider mv40" id="spy2">
            </div>
            @if(!isset($auditPlan->id))
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('compid', isset($company->id) ? $company->id : '') !!}
                    <input type="hidden" id="hid" name="hid" value="{{ $auditPlan->id or '' }}">
                    {!! Html::link(url('/audit/planning-list/all'), translate('form.cancel'), ['class' => 'btn btn-hover btn-system', 'tabindex' => '10']) !!}
                    {!! Form::button(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'id' => 'audit_save', 'tabindex' => '9']) !!}
                    <br clear="all">
                </div>
            </div>
            @endif
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('includes.scripts.audit.planning_form_script')
@stop
