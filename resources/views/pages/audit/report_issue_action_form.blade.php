@extends('layouts.default')
@section('content')
@inject('file', 'App\Services\DelegationDocumentForm')
<div class="admin-form" id="report-issue-action">
    <div class="panel">
        <div class="panel-body bg-light">
            <a href="{{url('/home')}}" class="btn btn-system text-default pull-right">
                {{translate('form.cancel')}}
            </a>
            {!! Form::open(array('method' => 'post', 'url' => '/audit/issue-action-save', 'class' => 'form-horizontal')) !!}
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.report_delegation')}}</span>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('source_type', translate('form.source_type').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}

                    <div class="col-md-4">
                        {{translate('words.internal_audit')}}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('audit_date', translate('words.audit_date').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}

                    <div class="col-md-4">
                        {{date(Config::get('settings.dashed_date'), strtotime($reported_issues->audit->plan_date))}}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('source_type', translate('form.no').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}

                    <div class="col-md-4">
                        {{date(Config::get('settings.dashed_date'), strtotime($reported_issues->created_at))}}/{{$reported_issues->id}}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('finding_type', translate('form.finding_type').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}

                    <div class="col-md-4">
                        {{$reported_issues['finding_type']['finding_type']}}	
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('related_process', translate('form.related_process').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}

                    <div class="col-md-4">
                        {{$reported_issues['process']['name']}}	
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('process_owner', translate('form.process_owner').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}

                    <div class="col-md-4">
                        {{$processOwnerName}} 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('issue', translate('form.issue').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}
                    <div class="col-md-4">		
                        <label class="field">
                            @if($source_type == "finding-issue")
                            {{$reported_issues['issue_desc']}}
                            @else
                            {{$reported_issues['observation']}}
                            @endif	
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('iso', translate('form.iso_reference').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}
                    <div class="col-md-4">
                        @if(!empty($reported_issues['iso']))
                         @inject('iso' , App\Services\ReportIssueActionForm)
                        {{$reported_issues['iso'][$iso->iso(session('lang'))]}}
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('iso', translate('form.reference').' :', array('class' => 'col-md-4 col-md-offset-2 text-right')) !!}
                    <div class="col-md-4">
                        @if(!empty($reported_issues['reference']))
                        @inject('name' , App\Services\ReportIssueActionForm)
                        {{$reported_issues['reference']['clause']}}
                        {{$reported_issues['reference'][$name->name(session('lang'))]}}
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-4">
                    @if(isset($reported_issues->docs))
                    @inject('efile', 'App\Services\ExecDocumentForm')
                    <ul class="docContainercls">
                        @foreach($reported_issues->docs as $eachDoc) 
                        <li>
                            <span class="doctypecls"><i class="fa {{$efile->efile($eachDoc->doc_name)['icon']}} fa-3x text-system-faded"></i>
                                <a href="{{$efile->efile($eachDoc->doc_name)['url']}}" target="_blank">
                                    <i class="fa fa-eye fa-1x doc-del-btn" aria-hidden="true"></i>
                                </a>
                            </span>
                            <p>{{$eachDoc->doc_name}}</p>
                        </li>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="section-divider mv5" id="spy2">
        </div>

        @if(($metrix_data->immidiate_actions == 1) || ($metrix_data->root_cause_analysis == 1) || ($metrix_data->corrective_measure == 1) || ($metrix_data->effectiveness == 1))
        <div class="tab-block mb25">
            <div class="timeline_tab">
                <ul id="tabline" class="nav nav-tabs {{$metrixClass}}">
                    @if($metrix_data->immidiate_actions == 1)
                    <li class="tab-links proactive {{$immidiateActive}} {{$immidiatAactiveCls}}">
                        <a href="#immidiate_actions" data-toggle="tab">
                        {{translate('form.immidiate_actions')}}</a>
                    </li>
                    @endif
                    @if($metrix_data->root_cause_analysis == 1)
                    <li class="tab-links {{$rootcauseActive}} {{$rootCauseActiveCls}}">
                        <a href="#root_cause_analysis" data-toggle="tab">{{translate('form.root_cause_analysis')}}</a>
                    </li>
                    @endif
                    @if($metrix_data->corrective_measure == 1)
                    <li class="tab-links {{$corrective_measureActive}}">
                        <a href="{{$define_corrective_link}}" data-toggle="tab">{{translate('form.define_corrective_action')}}</a>
                    </li>
                    @endif
                    @if($metrix_data->effectiveness == 1)
                    <li class="tab-links {{$effectivenessActive}}">
                        <a href="{{$effectiveness_link}}" data-toggle="tab">{{translate('form.effectiveness')}}</a>
                    </li>
                    @endif
                </ul>
            </div>
            <div class="tab-content">
                @if($metrix_data->immidiate_actions == 1)
                <div class="tab-pane {{$immidiateActive}}" id="immidiate_actions">
                    @if(count($immidiateActions) > 0)
                    <div class="row mb20">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                <thead class="bg-system text-default">
                                    <tr>
                                        <th>{{translate('table.delegate_to')}}</th>
                                        <th>{{translate('table.deadline')}}</th>
                                        <th>{{translate('table.required_action')}}</th>
                                        <th>{{translate('table.describe_action')}}</th>
                                        <th>{{translate('table.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>   
                                        @foreach($immidiateActions as $eachIA)
                                            <tr>
                                                <td>{{$eachIA->user->name}}</td>
                                                <td>{{$eachIA->formattedDate()}} 
                                                @if($eachIA->status==0)
                                                {!! $eachIA->dayCount() !!}
                                                @endif
                                                </td>
                                                <td>{{$eachIA->requested_action}}</td>
                                                <td>{{$eachIA->action_taken}}
                                                @if(!empty($eachIA->docs))
                                                  @foreach($eachIA->docs as $eachDoc) 
                                                    <span class="delegatedoctypecls"><i class="fa {{$file->file($eachDoc->doc_name)['icon']}} fa-2x text-system-faded"></i>
                                                        <a href="{{$file->file($eachDoc->doc_name)['url']}}" target="_blank">
                                                            <i class="fa fa-eye fa-1x doc-del-btn" aria-hidden="true"></i>
                                                        </a>
                                                    </span>
                                                  @endforeach
                                                @endif
                                                </td>
                                                <td>
                                                    @if($eachIA->status==0)
                                                       <span style="color:#ff9900"> {{translate('table.pending')}} </span>
                                                    @else
                                                       <span style="color:green">{{translate('table.completed')}}<span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                    @if(count($immidiateActions) < 3)
                    {!! Form::open(array('method' => 'post', 'url' => '/audit/issue-action-save/immidiate-action', 'class' => 'form-horizontal', 'id'=> 'immidiateFrm')) !!}
                        <div class="row mb20">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                    <thead class="bg-system text-default">
                                        <tr>
                                            <th width="24%">{{translate('table.required_action')}}</th>
                                            <th width="26%">{{translate('table.describe_action')}}</th>
                                            <th width="25%">{{translate('table.delegate_to')}}</th>
                                            <th width="20%">{{translate('table.deadline')}}</th>
                                            <th width="20%">{{translate('table.action')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="row repeter">
                            <div class="col-sm-3">
                                {{ Form::textarea('requested_action','', ['class' => 'gui-textarea','id'=>'requested_immidiate_action', 'value' => '', 'placeholder' => translate('form.describe_here_requested_actions'),'required' => 'required']) }}
                            </div>

                            <div class="col-sm-3">
                                {{ Form::textarea('immidiate_actions','', ['class' => 'gui-textarea','id'=>'ia', 'value' => '', 'placeholder' => translate('form.describe_here_immidiate_actions'),'disabled'=>'disabled']) }}
                            </div>

                            <div class="col-md-3 pt30">
                                <label class="field select">
                                    <select class="delegates select2-single" id="delegate-1" name="delegate_member_immidiate_actions" required>
                                        <option value="">Delegate to</option>
                                        @foreach($users as $k)
                                        <option value="{{$k->id}}">
                                            {{$k->name}}
                                        </option> 
                                        @endforeach
                                    </select> 
                                </label>
                            </div>

                            <div class="col-sm-2 pt30">
                                <label for="monthpicker2" class="field prepend-icon">
                                    <input type="text" name="immidiate_actions_deadline" class="form-control datetimepicker4" id="deadline-1" value="" required="" placeholder="Deadline">
                                    <label class="field-icon">
                                        <i class="fa fa-calendar-o"></i>
                                    </label>
                                </label>
                            </div>

                            <div class="col-sm-1 col-xs-3 pt30 pl30">
                                <button data-id="1" class="btn btn-system action-submit" data-type="immidiate_actions" type="submit" id="btn1">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        {!! csrf_field() !!}
                        {!! Form::hidden('issue_type', isset($source_type) ? $source_type : '') !!}
                        {!! Form::hidden('ref_id', isset($ref_id) ? $ref_id : '') !!}
                        {!! Form::hidden('company_id', isset($reported_issues['company_id']) ? $reported_issues['company_id'] : $reported_issues['source_type']['company_id']) !!}
                        {!! Form::hidden('info', '', ['class' => 'hid-info']) !!}
                    {!! Form::close() !!}
                    @endif
                </div>
                @endif
                @if($metrix_data->root_cause_analysis == 1)
                <div class="tab-pane {{$rootcauseActive}}" id="root_cause_analysis">
                    @if(count($rootCause) > 0)
                    <div class="row mb20">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                <thead class="bg-system text-default">
                                    <tr>
                                        <th>{{translate('table.delegate_to')}}</th>
                                        <th>{{translate('table.deadline')}}</th>
                                        <th>{{translate('table.required_action')}}</th>
                                        <th>{{translate('table.describe_action')}}</th>
                                        <th>{{translate('table.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rootCause as $eachRC)
                                        <tr>
                                            <td>{{$eachRC->user->name}}</td>
                                            <td>{{$eachRC->formattedDate()}}
                                            @if($eachRC->status==0)
                                             {!!$eachRC->dayCount()!!}
                                            @endif
                                            </td>
                                            <td>{{$eachRC->requested_action}}</td>
                                            <td>{{$eachRC->action_taken}}
                                            @if(!empty($eachRC->docs))
                                                  @foreach($eachRC->docs as $eachDoc) 
                                                    <span class="delegatedoctypecls"><i class="fa {{$file->file($eachDoc->doc_name)['icon']}} fa-2x text-system-faded"></i>
                                                        <a href="{{$file->file($eachDoc->doc_name)['url']}}" target="_blank">
                                                            <i class="fa fa-eye fa-1x doc-del-btn" aria-hidden="true"></i>
                                                        </a>
                                                    </span>
                                                  @endforeach
                                            @endif
                                            </td>
                                            <td>
                                                 @if($eachRC->status==0)
                                                    <span style="color:#ff9900"> {{translate('table.pending')}} </span>
                                                 @else
                                                       <span style="color:green">{{translate('table.completed')}}<span>
                                                 @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @else
                    {!! Form::open(array('method' => 'post', 'url' => '/audit/issue-action-save/root-cause', 'class' => 'form-horizontal')) !!}
                        <div class="row mb20">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                    <thead class="bg-system text-default">
                                        <tr>
                                            <th width="24%">{{translate('table.required_action')}}</th>
                                            <th width="26%">{{translate('table.describe_action')}}</th>
                                            <th width="25%">{{translate('table.delegate_to')}}</th>
                                            <th width="20%">{{translate('table.deadline')}}</th>
                                            <th width="20%">{{translate('table.action')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="row repeter">
                            <div class="col-sm-3">
                                {{ Form::textarea('requested_action','', ['class' => 'gui-textarea','id'=>'requested_rootcause_action', 'value' => '', 'placeholder' => translate('form.describe_here_requested_actions')]) }}
                            </div>
                            <div class="col-sm-3">
                                {{ Form::textarea('root_cause_analysis','', ['class' => 'gui-textarea','id'=>'rca', 'placeholder' => translate('form.describe_here_root_cause_analysis'),'disabled'=>'disabled']) }}
                            </div>

                            <div class="col-md-3 pt30">
                                <label class="field select">
                                    <select class="delegates select2-single select2" id="delegate-2" name="delegate_member_root_cause_analysis" required>
                                        <option value="">Delegate to</option>
                                        @foreach($users as $k)
                                        <option value="{{$k->id}}">
                                            {{$k->name}}
                                        </option> 
                                        @endforeach
                                    </select> 
                                </label>
                            </div>

                            <div class="col-sm-2 pt30">
                                <label for="monthpicker2" class="field prepend-icon">
                                <input type="text" name="root_cause_analysis_deadline" class="form-control" id="datetimepicker2" value="" required="" placeholder="Deadline">
                                <label class="field-icon">
                                    <i class="fa fa-calendar-o"></i>
                                </label>
                            </div>

                            <div class="col-sm-1 col-xs-2 pt30 pl30">
                                <button data-id="1" class="btn btn-system action-submit" data-type="root_cause_analysis" type="submit" id="btn2">
                                    {{translate('form.save_button')}}
                                </button>
                            </div>
                        </div>
                        {!! csrf_field() !!}
                        {!! Form::hidden('issue_type', isset($source_type) ? $source_type : '') !!}
                        {!! Form::hidden('ref_id', isset($ref_id) ? $ref_id : '') !!}
                        {!! Form::hidden('company_id', isset($reported_issues['company_id']) ? $reported_issues['company_id'] : $reported_issues['source_type']['company_id']) !!}
                        {!! Form::hidden('info', '', ['class' => 'hid-info']) !!}
                    {!! Form::close() !!}
                    @endif
                </div>
                @endif
                @if($metrix_data->corrective_measure == 1)
                <div class="tab-pane {{$corrective_measureActive}}" id="define_corrective">
                    @if(count($correctiveMeasure) > 0)
                    <div class="row mb20">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                <thead class="bg-system text-default">
                                    <tr>
                                        <th>{{translate('table.delegate_to')}}</th>
                                        <th>{{translate('table.deadline')}}</th>
                                        <th>{{translate('table.required_action')}}</th>
                                        <th>{{translate('table.describe_action')}}</th>
                                        <th>{{translate('table.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @foreach($correctiveMeasure as $eachCM)
                                            <tr>
                                                <td>{{$eachCM->user->name}}</td>
                                                <td>{{$eachCM->formattedDate()}}
                                                @if($eachCM->status==0)
                                                 {!!$eachCM->dayCount()!!}
                                                @endif
                                                </td>
                                                <td>{{$eachCM->requested_action}}</td>
                                                <td>{{$eachCM->action_taken}}
                                                @if(!empty($eachCM->docs))
                                                  @foreach($eachCM->docs as $eachDoc) 
                                                    <span class="delegatedoctypecls"><i class="fa {{$file->file($eachDoc->doc_name)['icon']}} fa-2x text-system-faded"></i>
                                                        <a href="{{$file->file($eachDoc->doc_name)['url']}}" target="_blank">
                                                            <i class="fa fa-eye fa-1x doc-del-btn" aria-hidden="true"></i>
                                                        </a>
                                                    </span>
                                                  @endforeach
                                                @endif
                                                </td>
                                                <td>
                                                     @if($eachCM->status==0)
                                                      <span style="color:#ff9900"> {{translate('table.pending')}} </span>
                                                     @else
                                                      <span style="color:green">{{translate('table.completed')}}<span>
                                                     @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                    @if(count($correctiveMeasure) < 3)
                    {!! Form::open(array('method' => 'post', 'url' => '/audit/issue-action-save/corrective-measure', 'class' => 'form-horizontal')) !!}
                        <div class="row mb20">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                    <thead class="bg-system text-default">
                                        <tr>
                                            <th width="24%">{{translate('table.required_action')}}</th>
                                            <th width="26%">{{translate('table.describe_action')}}</th>
                                            <th width="25%">{{translate('table.delegate_to')}}</th>
                                            <th width="20%">{{translate('table.deadline')}}</th>
                                            <th width="20%">{{translate('table.action')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="row repeter">
                            <div class="col-sm-3">
                                {{ Form::textarea('requested_action','', ['class' => 'gui-textarea','id'=>'requested_corrective_action', 'value' => '', 'placeholder' => translate('form.describe_here_requested_actions')]) }}
                            </div>
                            <div class="col-sm-3">
                                {{ Form::textarea('define_corrective','', ['class' => 'gui-textarea', 'id'=>'dc', 'placeholder' => translate('form.describe_here_corrective_action'), 'disabled'=>'disabled']) }}
                            </div>

                            <div class="col-md-3 pt30">
                                <label class="field select">
                                    <select class="delegates select2-single select2" name="delegate_member_define_corrective" id="delegate-3">
                                        <option value="">Delegate to</option>
                                        @foreach($users as $k)
                                        <option value="{{$k->id}}">
                                            {{$k->name}}
                                        </option> 
                                        @endforeach
                                    </select> 
                                </label>
                            </div>

                            <div class="col-sm-2 pt30">
                                <label for="monthpicker2" class="field prepend-icon">
                                    <input type="text" name="define_corrective_deadline" class="form-control deadlinedate" value="" required="" placeholder="Deadline" id ="deadline3">
                                    <label class="field-icon">
                                        <i class="fa fa-calendar-o"></i>
                                    </label>
                                </label>
                            </div>

                            <div class="col-sm-1 pt30 col-xs-2 pl30">
                                <button data-id="1" class="btn btn-system action-submit" data-type="define_corrective" type="submit" id="btn3">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        {!! csrf_field() !!}
                        {!! Form::hidden('issue_type', isset($source_type) ? $source_type : '') !!}
                        {!! Form::hidden('ref_id', isset($ref_id) ? $ref_id : '') !!}
                        {!! Form::hidden('company_id', isset($reported_issues['company_id']) ? $reported_issues['company_id'] : $reported_issues['source_type']['company_id']) !!}
                        {!! Form::hidden('info', '', ['class' => 'hid-info']) !!}
                    {!! Form::close() !!}
                    @endif
                </div>
                @endif
                @if($metrix_data->effectiveness == 1)
                <div class="tab-pane {{$effectivenessActive}}" id="effectiveness">
                    @if(count($effectiveness) > 0)
                    <div class="row mb20">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                <thead class="bg-system text-default">
                                    <tr>
                                        <th>{{translate('table.delegate_to')}}</th>
                                        <th>{{translate('table.deadline')}}</th>
                                        <th>{{translate('table.required_action')}}</th>
                                        <th>{{translate('table.describe_action')}}</th>
                                        <th>{{translate('table.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($effectiveness as $eachEF)
                                        <tr>
                                            <td>{{$eachEF->user->name}}</td>
                                            <td>{{$eachEF->formattedDate()}}
                                            @if($eachEF->status==0)
                                             {!! $eachEF->dayCount() !!}
                                            @endif
                                            </td>
                                            <td>{{$eachEF->requested_action}}</td>
                                            <td>{{$eachEF->action_taken}}
                                               @if(!empty($eachEF->docs))
                                                  @foreach($eachEF->docs as $eachDoc) 
                                                    <span class="delegatedoctypecls"><i class="fa {{$file->file($eachDoc->doc_name)['icon']}} fa-2x text-system-faded"></i>
                                                        <a href="{{$file->file($eachDoc->doc_name)['url']}}" target="_blank">
                                                            <i class="fa fa-eye fa-1x doc-del-btn" aria-hidden="true"></i>
                                                        </a>
                                                    </span>
                                                  @endforeach
                                                @endif
                                            </td>
                                            <td>
                                                    @if($eachEF->status==0)
                                                       <span style="color:#ff9900"> {{translate('table.pending')}} </span>
                                                    @else
                                                        <span style="color:green">{{translate('table.completed')}}<span>
                                                    @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @else
                    {!! Form::open(array('method' => 'post', 'url' => '/audit/issue-action-save/effectiveness', 'class' => 'form-horizontal')) !!}
                        <div class="row mb20">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                    <thead class="bg-system text-default">
                                        <tr>
                                            <th width="24%">{{translate('table.required_action')}}</th>
                                            <th width="26%">{{translate('table.describe_action')}}</th>
                                            <th width="25%">{{translate('table.delegate_to')}}</th>
                                            <th width="20%">{{translate('table.deadline')}}</th>
                                            <th width="20%">{{translate('table.action')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="row repeter">
                            <div class="col-sm-3">
                                {{ Form::textarea('requested_action','', ['class' => 'gui-textarea','id'=>'requested_effectiveness_action', 'value' => '', 'placeholder' => translate('form.describe_here_requested_actions')]) }}
                            </div>
                            <div class="col-sm-3">
                                {{ Form::textarea('effectiveness','', ['class' => 'gui-textarea','id'=>'effect', 'placeholder' => translate('form.describe_here_effectiveness'),'disabled'=>'disabled']) }}
                            </div>

                            <div class="col-md-3 pt30">
                                <label class="field select">
                                    <select class="delegates select2-single select2"  name="delegate_member_effectiveness" required id="delegate-4">
                                        <option value="">Delegate to</option>
                                        @foreach($users as $k)
                                        <option value="{{$k->id}}">
                                            {{$k->name}}
                                        </option> 
                                        @endforeach
                                    </select> 
                                </label>
                            </div>

                            <div class="col-sm-2 pt30">
                                <label for="monthpicker2" class="field prepend-icon">
                                    <input type="text" name="effectiveness_deadline" class="form-control" id="datetimepicker3" value="" required="" placeholder="Deadline">
                                    <label class="field-icon">
                                        <i class="fa fa-calendar-o"></i>
                                    </label>
                                </label>
                            </div>

                            <div class="col-sm-1 col-xs-2 pt30 pl30">
                                <button data-id="1" class="btn btn-system action-submit" data-type="effectiveness" type="submit" id="btn4">
                                    {{translate('form.save_button')}}
                                </button>
                            </div>
                        </div>
                        {!! csrf_field() !!}
                        {!! Form::hidden('issue_type', isset($source_type) ? $source_type : '') !!}
                        {!! Form::hidden('ref_id', isset($ref_id) ? $ref_id : '') !!}
                        {!! Form::hidden('company_id', isset($reported_issues['company_id']) ? $reported_issues['company_id'] : $reported_issues['source_type']['company_id']) !!}
                        {!! Form::hidden('info', '', ['class' => 'hid-info']) !!}
                    {!! Form::close() !!}
                    @endif
                </div>
                @endif
            </div>
        </div>
        @endif
    </div>
</div>
</div>
<script type="text/javascript">
    $(window).on('load', function () {
        $('#tabline li.active.proactive').find('a').trigger( "click" );
    });
</script>
@include('includes.scripts.issue_delegate_form_script')
@stop
