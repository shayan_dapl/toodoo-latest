@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/audit/report-issue-save', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}
                <div class="section-divider mb40" id="spy1">
                    <span>{{translate('form.report_issue')}}</span>
                </div>

                <div class="row mb20">
                    <div class="form-group">
                        {!! Form::label('source_type', translate('form.source_type'), array('class' => 'col-md-2 control-label required')) !!}
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-10">
                                    <label class="field select">
                                        {!! Form::select('source_type',$sourceTypes, Session::has('source') ? Session::get('source') : '', ['id' => 'source_type', 'required' => 'required', 'tabindex' => '1']) !!}
                                        <i class="arrow"></i>
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <span id="teamNotify"></span>
                                </div>
                            </div>
                        </div>
                    
                        {!! Form::label('finding_type', translate('form.finding_type'), array('class' => 'col-md-3 control-label required')) !!}
                        <div class="col-md-3">
                            <label class="field select">
                                {!! Form::text('reference', Session::has('reference') ? Session::get('reference') : '', ['class' => 'form-control', 'required' => 'required', 'tabindex' => '2']) !!}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 text-center fw600">
                        {{translate('form.issue')}}
                    </div>
                    <div class="col-md-2 text-center fw600">
                        {{translate('form.action')}}
                    </div>
                    <div class="col-md-2 text-center fw600">
                        {{translate('form.who')}}
                    </div>
                    <div class="col-md-2 text-center fw600">
                        {{translate('form.deadline')}} 
                    </div>
                    <div class="col-md-2 text-center fw600">
                        {{translate('form.related_process')}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        {!! Form::text('issue', '', ['class' => 'form-control', 'placeholder' => translate('form.describe_issue'), 'required' => 'required', 'tabindex' => '3']) !!}
                    </div>
                    <div class="col-md-2">
                        {!! Form::textarea('action', '', ['rows' => '1', 'cols' => '1', 'class' => 'form-control min-h-35', 'placeholder' => translate('form.define_action'), 'required' => 'required', 'tabindex' => '4']) !!}
                    </div>
                    <div class="col-md-2">
                        <label class="field select">
                            {!! Form::select('who', $users, '', ['required' => 'required', 'tabindex' => '5']) !!}
                            <i class="arrow"></i>
                        </label>
                    </div>
                    <div class="col-md-2">
                        <label for="monthpicker2" class="field prepend-icon">
                            {!! Form::text('deadline', date('d-m-Y'), ['class' => 'form-control report_issue_deadline pointer', 'placeholder' => translate('form.define_deadline'), 'required' => 'required', 'tabindex' => '6', 'readonly' => 'readonly']) !!}
                            <label class="field-icon">
                                <i class="fa fa-calendar-o"></i>
                            </label>
                        </label>
                    </div>
                    <div class="col-md-2">
                        <label class="field select">
                            {!! Form::select('related_process', $processes, '', ['required' => 'required', 'tabindex' => '7']) !!}
                            <i class="arrow"></i>
                        </label>
                    </div>
                    <div class="col-md-1 pt5 text-center">
                        <button class="btn btn-system add-action" data-row="1" tabindex="8" type="sybmit">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                {!! csrf_field() !!}
            {!! Form::close() !!}

            <div class="section-divider" id="spy2">
                <span>{{translate('form.report_finding_list')}}</span>
            </div>

            <div class="panel-body pn">
                <table class="table table-striped table-hover" id="datatable3" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('words.issue_code')}}</th>
                            <th>{{translate('words.source_type')}}</th>
                            <th>{{translate('words.issue_title')}}</th>
                            <th>{{translate('words.process')}}</th>
                            <th>{{translate('words.responsible')}}</th>
                            <th>{{translate('words.action')}}</th>
                            <th>{{translate('words.deadline')}}</th>
                            <th>{{translate('words.status')}}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($yourReportedIssues as $k=>$v)
                        <tr>
                            <td>
                                {{$v->issue_code}}
                            </td>
                            <td>
                                {{$v->source_type}}
                            </td>
                            <td>
                                {{$v->issue}}
                            </td>
                            <td>
                                {{$v->process}}
                            </td>
                            <td>
                                {{$v->responsible}}
                            </td>
                            <td>
                                {{$v->action}}
                            </td>
                            <td>
                                 {{date('d-m-Y',strtotime($v->deadline))}}
                            </td>
                            <td class="{{($v->status == 2) ? 'text-success' : 'text-danger'}}">
                                @if($v->status == 2)                                   
                                    {{translate('table.done')}}   
                                @else
                                    {{translate('table.pending')}}
                                @endif
                            </td>
                            @if($v->status == 2)
                            <td>
                                <a href="javascript:void(0);" class="text-system" data-toggle="modal" data-target="#fixing-details" data-id="{{$v->id}}" id="get-issue-fixing-data" >
                                    <i class="fa fa-info-circle"></i>
                                </a>
                            </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="section-divider mv40" id="spy2"></div>

            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! Html::link(url('/home'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    <br clear="all">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="fixing-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p10 bg-system exhight">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {{translate('form.details')}}
            </div>
            <div class="modal-body justified" id="fixing-detail-data">
                <p>Please wait for until data gets loaded...</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.report_issue_script')
@stop

