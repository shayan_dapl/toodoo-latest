@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/audit/report-issue-save', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}

            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.report_issue')}}</span>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('source_type', translate('form.source_type'), array('class' => 'col-md-4 col-md-offset-2 control-label')) !!}

                    <div class="col-md-4">
                        <label class="field pt10">
                            {{$details[0]->source_type->source_type}}
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('finding_type', translate('form.finding_type'), array('class' => 'col-md-4 col-md-offset-2 control-label')) !!}

                    <div class="col-md-4">
                        <label class="field pt10">
                            {{$details[0]->finding_type->finding_type}}
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('related_process', translate('form.related_process'), array('class' => 'col-md-4 col-md-offset-2 control-label')) !!}

                    <div class="col-md-4">
                        <label class="field pt10">
                            {{$details[0]->process->name}}
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('issue', translate('form.issue'), array('class' => 'col-md-4 col-md-offset-2 control-label')) !!}

                    <div class="col-md-4">		
                        <label class="field pt10">
                            {{$details[0]->issue_desc}}
                        </label>
                    </div>
                </div>
            </div>	
            <div class="section-divider mv40" id="spy2">
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}

                    {!! Html::link(url('/home'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    <br clear="all">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
