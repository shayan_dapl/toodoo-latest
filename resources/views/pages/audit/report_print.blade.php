@inject('initials', 'App\Services\AuditExecuteForm')
@inject('clausetitle', 'App\Services\AuditTypeList')
<style type="text/css">
	table {
		width: 90%;
		margin: 0 auto 50px;
	}
	th {background: #e3e3e3;}
	td,th {padding: 5px;}
	.bodyContent { margin: 30px auto; width: 90%;  }
	.bodyContent h3 { font-family: arial; color: #4d8dce; margin: 0 0 10px; font-size: 18px; font-weight: bold;  }
	.bodyContent p { margin-bottom: 20px; }
	.logo { text-align: right; margin-bottom: 20px; padding: 0 20px; }
</style>
<div class="logo">
	<img src="{{public_path('storage/company/'.Auth::guard('customer')->user()->company_id.'/image/'.Auth::guard('customer')->user()->logo)}}" height="100">
</div>
<div class="row">
	<div class="col-md-12">
		<h2 style="color: #4d8dce; text-align: center; text-transform: capitalize; font-weight: bold; margin-bottom: 30px;">
		@if($auditData->category == 'internal')
			{{translate('words.internal_audit_report')}}
		@elseif($auditData->category == 'external')
			{{translate('words.external_audit_report')}}
		@endif

		</h2>
	</div>
</div>

<table border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th width="50%">{{translate('words.planned_date')}} : {{date(Config::get('settings.dashed_date'), strtotime($auditData->plan_date))}}</th>
			<th width="50%">{{translate('words.release_date')}}  : {{date(Config::get('settings.dashed_date'), strtotime($auditData->release_date))}}</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td width="50%">{{translate('words.audited_process')}} </td>
			<td width="50%">@foreach($auditedProcess as $eachProcess)
				{{$eachProcess}}
				@if(!$loop->last)
				/
				@endif
				@endforeach
			</td>
		</tr>
		<tr>
			<td width="50%">{{translate('words.location')}}</td>
			<td width="50%">{{$auditData->branch->name}}</td>
		</tr>
	</tbody>
</table>

<table border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th width="50%">{{translate('words.team_member')}}</th>
			<th width="50%"></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td width="50%">{{translate('words.lead_auditor')}}</td>
			<td width="50%">{{$auditData->lead_auditor->name}}</td>
		</tr>
		<tr>
			<td width="50%">{{translate('words.audit_team_member')}}</td>
			<td width="50%">@foreach($auditData->auditors as $eachAuditor)
				{{$eachAuditor->user->name}} 
				@if(!$loop->last)
				/
				@endif
				@endforeach
			</td>
		</tr>
	</tbody>
</table>

<table border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th colspan="2" align="center" style="text-align: center;">{{translate('words.audit_findings')}}</th>
		</tr>
	</thead>
	<tbody>
		@foreach($auditFindings as $k=>$v)
		<tr>
			<td width="50%">{{$k}}</td>
			<td width="50%">{{$v}}</td>
		</tr>
		@endforeach
	</tbody>
</table>

<table border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th colspan="2" style="text-align: center;">{{translate('words.report_distribution')}}</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="2">
				@foreach($auditData->auditees as $eachAuditee)
				{{$eachAuditee->user->name}} 
				@if(!$loop->last)
				/
				@endif
				@endforeach
			</td>
		</tr>
	</tbody>
</table>

<div class="bodyContent"><h3>{{translate('words.participants')}}:</h3></div>

<table border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th width="50%">{{translate('words.name')}}</th>
			<th width="50%">{{translate('words.function')}}</th>
		</tr>
	</thead>
	<tbody>
		@foreach($auditData->auditees as $eachAuditee)
		<tr>
			<td width="50%">{{$eachAuditee->user->name}}</td>
			<td width="50%">
				@foreach($eachAuditee->user->positions() as $eachPosition)
				{{$eachPosition->position->name}}
				@if(!$loop->last)
				/
				@endif
				@endforeach
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<div class="bodyContent">
	<h3>{{translate('words.references')}}:</h3>
	<p>{{translate('words.the_audit_has_been_audited_according')}}
		@foreach($reference as $eachRef)
		{{$eachRef}}
		@if(!$loop->last)
		/
		@endif
		@endforeach
	</p>

	<h3>{{translate('form.objective')}}:</h3>
	<p>{{$auditData->objective}}</p>

	<h3>{{translate('words.verified_docs')}}:</h3>
	@if(!empty($executionDocs))
	<ul>
		@foreach($executionDocs as $eachdoc)
		<li>{{$eachdoc}}</li>
		@endforeach 
	</ul>
	@endif

	<h3>{{translate('words.audit_findings')}}:</h3>
	<p>
		@foreach($allFindings as $eachKey => $eachVal)
			{{$eachVal}} ({{$initials->initials($eachKey)}})
			@if(!$loop->last)
			,
			@endif
		@endforeach
	</p>
</div>

<table border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th>{{translate('form.no')}}.</th>
			@foreach($findings as $eachFinding)
			<th>{{$initials->initials($eachFinding->finding_type)}}</th>
			@endforeach
			<th>{{translate('words.finding')}}</th>
			<th>{{translate('words.process')}}</th>
		</tr>
	</thead>
	<tbody>
		@foreach($auditData->executions as $eachExec)
		<tr>
			<td>{{date(Config::get('settings.dashed_date'), strtotime($eachExec->created_at))}}/{{$eachExec->id}}</td>
			@foreach($findings as $eachFinding)
			@if($eachExec->finding_type_id == $eachFinding->id)
			<td>X</td>
			@else
			<td></td>
			@endif
			@endforeach
			<td>{{$eachExec->observation}}</td>
			<td>{{$eachExec->process->name}}</td>
		</tr>
		@endforeach
		<tr>
			<td>{{translate('words.total')}}</td>
			@foreach($totalfinding as $key=>$val)
			<td>{{$val}}</td>
			@endforeach
			<td></td>
			<td></td>
		</tr>
	</tbody>
</table>
@foreach($executedIsos as $eachiso=>$eachIsoClases)
<div class="bodyContent"><h3>{{translate('words.link_to')}} {{$eachiso}} {{translate('words.clauses')}}</h3></div>
<table border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th>{{translate('form.clause')}}</th>
			<th>{{translate('form.description')}}</th>
			<th>{{translate('words.checked_part')}}</th>
		</tr>
	</thead>
	<tbody>
		@foreach($eachIsoClases as $eachClause)
		<tr>
			<td>{{$eachClause->clause}}</td>
			<td>{{$clausetitle->clausetitle(session('lang'),$eachClause)}}</td>
			@if(in_array($eachClause->id, $executedClause))
			<td>X</td>
			@else
			<td></td>
			@endif 
		</tr>
		@endforeach
	</tbody>
</table>
@endforeach
<script type="text/php">
    if (isset($pdf)) {
        $x = 500;
        $y = 760;
        $text = "Page {PAGE_NUM} of {PAGE_COUNT}";
        $font = null;
        $size = 14;
        $color = array(0,0,0);

        $a = 50;
        $b = 760;
        $dttext = date('d-m-Y');

        $pdf->page_text($x, $y, $text, $font, $size, $color);
        $pdf->page_text($a, $b, $dttext, $font, $size, $color);
    }
</script>