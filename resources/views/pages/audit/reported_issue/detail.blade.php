@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            @include('pages.audit.reported_issue.form')

            <div class="section-divider" id="spy2">
                <span>{{translate('form.report_finding_list')}}</span>
            </div>

            <div class="panel-body pn">
                <div class="tab-block">
                    <ul class="nav nav-tabs">
                        <li>
                            <a href="{{url('/audit/report-issue')}}">{{translate('words.overview')}}</a>
                        </li>
                        <li class="active">
                            <a href="#tab6_2" data-toggle="tab">{{translate('form.detail')}}</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab6_2" class="tab-pane active">
                            <table class="table table-striped table-hover" id="datatable3" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>{{translate('words.issue_code')}}</th>
                                        <th>{{translate('words.source_type')}}</th>
                                        <th>{{translate('words.reference')}}</th>
                                        <th></th>
                                        <th>{{translate('words.issue_title')}}</th>
                                        <th>{{translate('words.process')}}</th>
                                        <th>{{translate('words.responsible')}}</th>
                                        <th>{{translate('words.action')}}</th>
                                        <th>{{translate('words.deadline')}}</th>
                                        <th>{{translate('words.status')}}</th>
                                        <th></th>
                                        @if($isDeadlineEditable)
                                            <th>{{translate('table.action')}}</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($reportedIssuesDetails as $k=>$v)
                                    <tr>
                                        <td>{{$v->issue_code}}</td>
                                        <td>{{$v->source_type}}</td>
                                        <td>{{$v->reference}}</td>
                                        <td>
                                            @if(!empty($v->issues->supplier))
                                                <a href="#" id="team-details" data-toggle="tooltip" data-html="true" data-placement="top" title="{{$v->issues->suppliername->name}}">
                                                    <i class="fa fa-info-circle text-system"></i>
                                                </a>
                                            @endif
                                        </td>
                                        <td>{{$v->issue}}</td>
                                        <td>{{$v->process}}</td>
                                        <td>{{$v->responsible}}</td>
                                        <td>{{$v->action}}</td>
                                        <td>{{date('d-m-Y',strtotime($v->deadline))}}</td>
                                        <td class="{{($v->status == 2) ? 'text-success' : 'text-danger'}}">
                                            @if($v->status == 2)                                   
                                                {{translate('table.done')}}   
                                            @else
                                                {{translate('table.pending')}}
                                            @endif
                                        </td>
                                        @if($v->status == 2)
                                            <td>
                                                <a href="javascript:void(0);" class="text-system mr5" data-toggle="modal" data-target="#fixing-details" data-id="{{$v->id}}" id="get-issue-fixing-data" >
                                                    <i class="fa fa-info-circle"></i>
                                                </a>
                                            </td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($isDeadlineEditable)
                                            <td>
                                                @if(($v->created_by == Auth::guard('customer')->user()->id) and ($v->status != 2))
                                                    <button class="btn btn-default" id="edit-deadline" data-id="{{$v->did}}" data-deadline="{{date(Config::get('settings.dashed_date'), strtotime($v->deadline))}}" title="{{translate('form.update_deadline')}}">
                                                        <i class="text-success fa fa-pencil"></i>
                                                    </button>
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-divider mv40" id="spy2"></div>

            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! Html::link(url('/home'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    <br clear="all">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deadline-pop" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p10 bg-system exhight">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {{translate('form.update_deadline')}}
            </div>
            {!! Form::open(['method' => 'post', 'url' => '/audit/report-issue-deadline-save', 'class' => 'form-inline']) !!}
            {!! csrf_field() !!}
            {!! Form::hidden('issue_id','', ['id' => 'issue_id']) !!}
            <div class="modal-body justified">
                <div class="row">
                    <div class="col-md-9 admin-form">
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('deadline', translate('words.deadline'), array('class' => 'pt10')) !!}
                            </div>
                            <div class="col-md-8">
                                <label class="field append-icon">
                                    {!! Form::text('deadline', date('d-m-Y'), ['class' => 'form-control pointer widthFull', 'id' => 'deadline', 'autocomplete' => 'off'] ) !!}
                                    <label for="deadline" class="field-icon">
                                        <i class="fa fa-calendar"></i>
                                    </label>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">   
                <button type="button" class="btn btn-default" data-dismiss="modal">{{translate('started.close')}}</button>
                {!! Form::submit(translate('form.update'), ['class' => 'btn btn-system btn-sm']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div id="fixing-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p10 bg-system exhight">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {{translate('form.details')}}
            </div>
            <div class="modal-body justified" id="fixing-detail-data">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{translate('form.cancel')}}</button>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.audit.report_issue_script')
@stop

