{!! Form::open(array('method' => 'post', 'url' => '/audit/report-issue-save', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}
    <div class="section-divider mb40" id="spy1">
        <span>{{translate('form.report_issue')}}</span>
    </div>

    <div class="row mb20">
        {!! Form::label('source_type', translate('form.source_type'), array('class' => 'col-md-2 pt10 required')) !!}
        <div class="col-md-3">
            <label class="field select">
                {!! Form::select('source_type',$sourceTypes, Session::has('source') ? Session::get('source') : '', ['id' => 'source_type', 'required' => 'required', 'tabindex' => '1']) !!}
                <i class="arrow"></i>
            </label>
        </div>
        <div class="col-md-2" id="teamNotify">
            @if (Session::has('supplier'))
            <label class="field select">
                {!! Form::select('supplier', $suppliers, '', ['required' => 'required']) !!}
                <i class="arrow"></i>
            </label>
            @endif
        </div>
        {!! Form::label('reference', translate('words.reference'), array('class' => 'col-md-2 control-label required')) !!}
        <div class="col-md-3">
            <label class="field select">
                {!! Form::text('reference', Session::has('reference') ? Session::get('reference') : '', ['class' => 'form-control', 'required' => 'required', 'tabindex' => '2']) !!}
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 text-center fw600">
            {{translate('form.issue')}}
        </div>
        <div class="col-md-2 text-center fw600">
            {{translate('form.action')}}
        </div>
        <div class="col-md-2 text-center fw600">
            {{translate('form.who')}}
        </div>
        <div class="col-md-2 text-center fw600">
            {{translate('form.deadline')}} 
        </div>
        <div class="col-md-2 text-center fw600">
            {{translate('form.related_process')}}
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            {!! Form::textarea('issue', '', ['rows' => '1', 'cols' => '1', 'class' => 'form-control min-h-39', 'placeholder' => translate('form.describe_issue'), 'required' => 'required', 'tabindex' => '3']) !!}
        </div>
        <div class="col-md-2">
            {!! Form::textarea('action', '', ['rows' => '1', 'cols' => '1', 'class' => 'form-control min-h-39', 'placeholder' => translate('form.define_action'), 'required' => 'required', 'tabindex' => '4']) !!}
        </div>
        <div class="col-md-2">
            <label class="field select">
                {!! Form::select('who', $users, '', ['required' => 'required', 'tabindex' => '5']) !!}
                <i class="arrow"></i>
            </label>
        </div>
        <div class="col-md-2">
            <label for="monthpicker2" class="field prepend-icon">
                {!! Form::text('deadline', date('d-m-Y'), ['class' => 'form-control report_issue_deadline pointer', 'placeholder' => translate('form.define_deadline'), 'required' => 'required', 'tabindex' => '6', 'readonly' => 'readonly']) !!}
                <label class="field-icon">
                    <i class="fa fa-calendar-o"></i>
                </label>
            </label>
        </div>
        <div class="col-md-2">
            <label class="field select">
                {!! Form::select('related_process', $processes, '', ['required' => 'required', 'tabindex' => '7']) !!}
                <i class="arrow"></i>
            </label>
        </div>
        <div class="col-md-1 pt5 text-center">
            <button class="btn btn-system add-action" data-row="1" tabindex="8" type="submit">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    {!! csrf_field() !!}
{!! Form::close() !!}