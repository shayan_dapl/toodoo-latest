@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            @include('pages.audit.reported_issue.form')

            <div class="section-divider" id="spy2">
                <span>{{translate('form.report_finding_list')}}</span>
            </div>

            <div class="panel-body pn">
                <div class="tab-block">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab6_1" data-toggle="tab">{{translate('words.overview')}}</a>
                        </li>
                        <li>
                            <a href="{{url('/audit/report-issue/detail/')}}">{{translate('form.detail')}}</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab6_1" class="tab-pane active">
                            <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>{{translate('table.plan_date')}}</th>
                                        <th>{{translate('words.source_type')}}</th>
                                        <th>{{translate('words.reference')}}</th>
                                        <th>{{translate('words.open_action')}}</th>
                                        <th>{{translate('words.closed_action')}}</th>
                                        <th>{{translate('form.detail')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($reportedIssuesOverview as $each)
                                    <tr>
                                        <td>{{date(Config::get('settings.dashed_date'), strtotime($each->created_at))}}</td>
                                        <td>{{$each->source_type}}</td>
                                        <td>{{$each->reference}}</td>
                                        <td>{{$each->main_open}}</td>
                                        <td>{{$each->main_close}}</td>
                                        <td>
                                            <a class="text-system fs16" href="{{url('/audit/report-issue/detail/'. $each->source_type_id.'/'.base64_encode($each->reference))}}">
                                                {{translate('form.detail')}}
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-divider mv40" id="spy2"></div>

            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! Html::link(url('/home'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    <br clear="all">
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.audit.report_issue_script')
@stop

