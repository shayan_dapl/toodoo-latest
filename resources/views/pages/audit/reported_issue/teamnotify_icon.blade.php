@inject('assignedusers', 'App\Services\ReportingSource')
@if ($assignedUsersCount > 0)
	<a href="#" id="team-details" data-toggle="tooltip" data-html="true" data-placement="top" title="{!! $assignedusers->assignedusers($assignedUsers) !!}">
		<i class="fa fa-users mt10"></i>
	</a>
	<script type="text/javascript">
		$(document).ready(() => {
			$('#team-details').tooltip();
			$('#team-details').hover(() => {
				setTimeout(() => {
					$('.admin-form .tooltip').css('z-index', 'unset');
					$('.admin-form .tooltip').css('opacity', 1);
				}, 100);
			});
		});
	</script>
@endif
@if ($sourceData->source_status == 2)
	<label class="field select">
	    {!! Form::select('supplier', $suppliers, '', ['required' => 'required']) !!}
	    <i class="arrow"></i>
	</label>
@endif
