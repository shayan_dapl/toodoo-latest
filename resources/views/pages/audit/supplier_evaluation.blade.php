@extends('layouts.default')
	@section('content')
	@inject('average', 'App\Services\SupplierEvaluation')
	@inject('percent', 'App\Services\SupplierEvaluation')
	<div class="panel panel-default" id="app">
		<div class="panel-heading exhight">
			{{translate('words.supplier_evaluation')}}
			@if(Auth::user()->type == 2)
				<span class="text-danger fs12">[{{translate('words.click_on_supplier')}}]</span>
			@endif
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12 table-responsive">
					@if(!empty($legend))
						[ @foreach($legend as $each)
							<span class="mr10">{{$each['rating']}}: {{$each['description']}}</span>
						@endforeach]
					@endif
					<table class="table table-bordered">
						<thead>
							<tr class="bg-system rotateHeader">
								<th>{{translate('words.supplier')}}</th>
								<th>{{translate('form.product_services')}}</th>
								@foreach($parameters as $each)
									<th width="40">
										<span class="supplier-text-align">
											{{$each->name}}
										</span>
									</th>
								@endforeach
								<th width="40">
									<span class="supplier-text-align">
										{{translate('words.average')}}
									</span>
								</th>
								<th>%</th>
							</tr>
						</thead>
						<tbody>
							@foreach($suppliers as $each)
							<tr>
								<td @if(Auth::user()->type == 2) class="pointer" @click="openModal({{$each->id}}, `{{$each->name}}`)" @endif>
									<strong>{{$each->name}}</strong>
								</td>
								<td>{{$each->service}}</td>
								@foreach($parameters as $eachParam)
									<td ref="{{$each->id . '-' . $eachParam->id}}">
										@if (!empty($each->action()))
											@foreach($each->action()->rates as $eachRate)
												@if($eachRate->parameter_id == $eachParam->id)
													{{$eachRate->rate->rating}}
												@endif
											@endforeach
										@endif
									</td>
								@endforeach
								<td ref="{{$each->id}}-average">{{$average->average($each, $parameters)}}</td>
								<td ref="{{$each->id}}-percent">{{$percent->percent($each, $parameters)}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div id="supplierModal" class="modal fade" role="dialog">
		        <div class="modal-dialog supplier-evaluation-modal">
		            <div class="modal-content">
		                <div class="modal-header text-default back4d">
		                    <button type="button" class="close" data-dismiss="modal">&times;</button>
		                    <h4 class="modal-title">{{translate('words.change_rating')}} : @{{supplierName}}</h4>
		                </div>
		                <div class="modal-body">
		                    <div class="row">
		                        <div class="col-md-8">
		                            <label>{{translate('words.choose_rating')}}</label>
		                            <div class="supplier-parameters-height">
			                            @if (!empty($parameters))
			                            	@foreach ($parameters as $each)
			                            		<div class="row mb5">
			                            			<div class="col-md-5 pt10">{{$each->name}}</div>
			                            			<div class="col-md-3">
			                            				<ratings :list="ratings" data-name="{{$each->name}}" data-parameter="{{$each->id}}" />
			                            			</div>
			                            			<div class="col-md-4 pt10 rating-name"></div>
			                            		</div>
			                            	@endforeach
			                            @endif
			                        </div>
		                        </div>
		                        <div class="col-md-4">
		                            <label>
		                                {{translate('form.comment')}}
		                            </label>
		                            {!! Form::textarea('', '', ['class' => 'gui-input rating-comment', 'v-model' => 'comment', 'placeholder' => translate('form.comment'), 'rows' => '1', 'tabindex' => '2']) !!}
		                        </div>
		                    </div>
		                    <div class="row mt10">
		                        <div class="col-md-2">
	                                <label>{{translate('words.action_required')}}</label>
	                                <label class="switch-extended switch-success rankSwitchAction mt10">
	                                    {!! Form::checkbox('', 0, '', ['v-model' => 'is_action']) !!}
	                                    <div class="slider-extended round"></div>
	                                </label>
		                        </div>
		                        <div class="col-md-4" v-show="is_action" id="action-segment">
                                    <label class="required">{{translate('form.action')}}</label>
                                    {!! Form::text('', '', [':class' => 'actionClass', 'v-model' => 'action', 'placeholder' => translate('form.define_action'), 'tabindex' => '1']) !!}
                                </div>
                                <div class="col-md-4" v-show="is_action" id="action-segment">
                                    <label class="required">{{translate('table.delegate_to')}}</label>
                                    {!! Form::select('', $users, '', [':class' => 'delegateClass', 'v-model' =>'delegate','tabindex' => '2']) !!}
                                </div>
                                <div class="col-md-2" v-show="is_action" id="action-segment">
                                    <label class="required">{{translate('form.deadline')}}</label>
                                    {!! Form::text('', '', [':class' => 'deadlineClass', 'v-model' => 'deadline', 'placeholder' => translate('form.define_deadline'), 'readonly' => 'readonly', 'tabindex' => '3']) !!}
                                </div>
		                    </div>
		                    <div class="row mt10" v-show="thresholdParams.length > 0">
		                    	<div class="col-md-6">
		                    		<label>
		                    			{{translate('words.there_are')}} 
		                    			@{{thresholdParams.length}} 
		                    			{{translate('words.parameter_below_threshold')}}
		                    		</label>
		                    		<ol>
		                    			<li v-for="each in thresholdParams">@{{each}}</li>
		                    		</ol>
		                    	</div>
		                    	<div class="col-md-6">
	                                <label>{{translate('words.supplier_corrective_action_required')}}</label>
	                                <label class="switch-extended switch-success rankSwitchAction mt10">
	                                    {!! Form::checkbox('', 0, '', ['v-model' => 'is_sarc']) !!}
	                                    <div class="slider-extended round"></div>
	                                </label>
		                        </div>
		                    </div>
		                    <div class="row" v-show="is_sarc && thresholdParams.length > 0">
                                <div class="col-md-4" id="action-segment">
                                    <label class="required">{{translate('form.deadline')}}</label>
                                    {!! Form::text('', '', [':class' => 'sarcdeadlineClass', 'v-model' => 'sarcdeadline', 'placeholder' => translate('form.define_deadline'), 'readonly' => 'readonly', 'tabindex' => '3']) !!}
                                </div>
                                <div class="col-md-4" id="action-segment">
                                    <label class="required">{{translate('table.delegate_to')}}</label>
                                    {!! Form::select('', $users, '', [':class' => 'sarcdelegateClass', 'v-model' =>'sarcdelegate','tabindex' => '2']) !!}
                                </div>
		                    </div>
		                    <div class="row mt25">
		                    	<div class="col-md-4 border-rad-5 col-md-offset-8">
		                            {!! Form::button(translate('form.save_button'), ['class' => 'btn btn-hover btn-system btn-block py11', 'v-on:click' => 'saveRating', ':disabled' => 'enabled']) !!}
		                        </div>
		                    </div>
		                   	<div class="row">
		                        <div class="col-md-12">
		                            <h4 class="text-system"><strong>{{translate('words.rating_log')}}</strong></h4>
		                            <table class="table table-bordered table-striped table-hover tablScroll">
		                                <thead class="bg-secondary">
		                                    <tr>
		                                        <th width="50%">{{translate('words.rating')}}</th>
		                                        <th>{{translate('table.plan_date')}}</th>
		                                        <th>{{translate('words.comment')}}</th>
		                                        <th>{{translate('words.is_action')}}</th>
		                                        <th>{{translate('words.is_scar_action')}}</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
		                                    <tr v-if="list.length > 0" v-for="(each, i) in list">
		                                        <td width="50%">
		                                        	<div v-for="rate in each.rates">
		                                        		<strong>@{{rate.parameter_name}} :</strong> @{{rate.rating_name}}
		                                        	</div>
		                                        </td>
		                                        <td>@{{each.created_at}}</td>
		                                        <td>@{{each.comment}}</td>
		                                        <td v-if="each.action != null">
		                                        	{{translate('form.yes')}}
		                                            <a href="javascript:void(0);" class="text-system" data-html="true" data-toggle="tooltip" data-placement="top" :title="actionStatement(each)">
		                                                <i class="fa fa-info-circle"></i>
		                                            </a>
		                                            <span v-if="each.closed == 1 && each.files.length > 0">
		                                                <a class="mr5" :href="file.url" v-for="file in each.files" target="_blank">
		                                                    <i :class="'fa ' + file.icon"></i>
		                                                </a>
		                                            </span>
		                                        </td>
		                                        <td v-if="each.action == null"></td>
		                                        <td v-if="each.correctives != null && each.correctives.deadline != ''">
		                                        	{{translate('form.yes')}}
		                                            <a href="javascript:void(0);" class="text-system" data-html="true" data-toggle="tooltip" data-placement="top" :title="actionSCARStatement(each.correctives)">
		                                                <i class="fa fa-info-circle"></i>
		                                            </a>
		                                            <span v-if="each.correctives.closed == 1 && each.correctives.files.length > 0">
		                                                <a class="mr5" :href="file.url" v-for="file in each.correctives.files" target="_blank">
		                                                    <i :class="'fa ' + file.icon"></i>
		                                                </a>
		                                            </span>
		                                        </td>
		                                        <td v-if="each.correctives == null"></td>
		                                    </tr>
		                                    <tr v-if="list.length == 0">
		                                        <td colspan="3" align="center">
		                                            {{translate('table.no_data')}}
		                                        </td>
		                                    </tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	@include('includes.scripts.audit.supplier_evaluation_script')
@stop