@extends('layouts.default')
@section('content')
@inject('space', 'App\Services\Membership')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible mt10 mb10" id="spy2">
            <div class="panel-heading exhight">
                {{translate('words.account_information')}} 
                {!! Html::link(url('/list'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
            </div>
            <div class="panel-body">         
                <div class="col-md-7 pt5 pb5">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="fw700 fs16"> {{translate('words.actual_plan')}} : </span>
                             {{$plans['iso']}}
                        </div>
                    </div>                
                    <div class="row">
                        <div class="col-md-12">
                            <span class="fw700 fs16"> {{translate('words.user')}} : </span> 
                            {{$userAdded}} / {{$plans['user']}}
                            @if(!empty($company->subscription_id))
                                {!! Html::link('/membership/user', translate('words.add_more'), ['class' => 'btn btn-xs text-system ml5']) !!}
                            @endif
                             <div class="progress progCustom">
                                <div class="progress-bar progress-bar-{{($space->calcwidth($userAdded, $plans['user']) < 33) ? 'warning' : (($space->calcwidth($userAdded, $plans['user']) > 33 && $space->calcwidth($userAdded, $plans['user'])< 66) ? 'warning dark' : 'danger')}}" role="progressbar" aria-valuenow="{{$userAdded}}" aria-valuemin="0" aria-valuemax="{{$plans['user']}}" style="width: {{$space->calcwidth($userAdded, $plans['user'])}}%;">
                                    <span class="sr-only"></span>
                                </div>
                            </div>
                        </div>      
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <span class="fw700 fs16"> {{translate('words.storage')}} : </span>
                             {{formatSize($fileSize)}} / {{$space->space($plans['storage'])}} GB
                            <div class="progress progCustom">
                                <div class="progress-bar progress-bar-{{($space->calcwidth($space->spacemb($fileSize), $plans['storage']) < 33) ? 'warning' : (($space->calcwidth($space->spacemb($fileSize), $plans['storage']) > 33 && $space->calcwidth($space->spacemb($fileSize), $plans['storage']) < 66) ? 'warning dark' : 'danger')}}" role="progressbar" aria-valuenow="{{$space->spacemb($fileSize)}}" aria-valuemin="0" aria-valuemax="{{$plans['storage']}}" style="width: {{$space->calcwidth($space->spacemb($fileSize), $plans['storage'])}}%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 pt5 pb5">
                @if(!empty($company->subscription_id))
                    <span class="fw700 fs16"> {{translate('words.subscription_id')}} :</span> {{!empty($company->subscription_id) ? $company->subscription_id : translate('words.na')}}
                    <br clear="all">
                    <span class="fw700 fs16"> {{translate('words.subscription_amount')}} :</span> &euro; {{$company->subscription_amount}} ({{translate('words.vat_excluded')}}) 
                    <br clear="all">
                    <span class="fw700 fs16">{{translate('words.next_renewal_date')}} :</span> {{date(\Config::get('settings.dashed_date'), strtotime($company->next_renewal_date))}}
                    <br clear="all">
                    <span class="fw700 fs16">{{translate('form.tva_no')}} :</span> {{$company->tva_no}}
                    <br clear="all">
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@if($company->transactions->count() > 0)
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
            {{translate('words.payment_history')}}
            </div>
            <div class="panel-body pn">
                <table class="table table-striped table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.invoice_no')}}</th>
                            <th>{{translate('table.payment_date')}}</th>
                            <th>{{translate('table.transaction_id')}}</th>
                            <th>{{translate('table.payment_amount')}}</th>
                            <th>{{translate('table.status')}}</th>
                            <th>{{translate('table.view')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($company->transactions->count() > 0)
                            @foreach($company->transactions as $invoice)
                                @if($invoice->transaction_id != null)
                                <tr>
                                    <td>{{$invoice->invoice_no}}</td>
                                    <td>{{date(\Config::get('settings.dashed_date'), strtotime($invoice->payment_date))}}</td>
                                    <td>{{$invoice->transaction_id}}</td>
                                    <td>&euro; {{$invoice->total_amount}}</td>
                                    <td>{{$invoice->status}}</td>
                                    <td>
                                        <a  class="btn btn-success" href="{{ url('/membership/transaction-details/'.$invoice->id) }}" title="{{translate('words.see_invoice')}}">
                                            <span class="fa fa-credit-card"></span> 
                                        </a>
                                        <a href="{{url('/membership/invoice-print/'.$invoice->id)}}" class="ml20 btn btn-system" title = "{{translate('words.print_invoice')}}">
                                            <span class="fa fa-print"></span>
                                        </a>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" align="center">{{translate('table.no_data')}}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
            {{translate('form.company_details')}}
            </div>
            <div class="panel-body pn">
                <div class="row">
                    <div class="col-md-5 col-md-offset-1">
                        <span class="fw700 fs16"> {{translate('form.company_name')}} : </span> 
                        {{$company->name}}
                    </div>
                    <div class="col-md-6">
                        <span class="fw700 fs16"> {{translate('form.company_tva')}} : </span> 
                        {{$company->tva_no}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-md-offset-1">
                        <span class="fw700 fs16"> {{translate('form.street')}} : </span> 
                        {{$company->street}}
                    </div>
                    <div class="col-md-6">
                        <span class="fw700 fs16"> {{translate('form.nr')}} : </span> 
                        {{$company->nr}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-md-offset-1">
                        <span class="fw700 fs16"> {{translate('form.bus')}} : </span> 
                        {{$company->bus}}
                    </div>
                    <div class="col-md-6">
                       <span class="fw700 fs16"> {{translate('form.zip')}} : </span> 
                        {{$company->zip}}
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-5 col-md-offset-1">
                        <span class="fw700 fs16"> {{translate('form.city')}} : </span> 
                        {{$company->city}}
                    </div> 
                     <div class="col-md-6">
                        <span class="fw700 fs16"> {{translate('form.country')}} : </span> 
                        {{!empty($company->countries) ? $company->countries->name_en : ''}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
            {{translate('words.admin_details')}}
            </div>
            <div class="panel-body pn">
                <table class="table table-striped table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('form.type')}}</th>
                            <th>{{translate('table.name')}}</th>
                            <th>{{translate('table.email')}}</th>
                            <th>{{translate('table.telephone_nr')}}</th>
                            <th>{{translate('words.function')}}</th>
                            <th>{{translate('table.last_login')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($company->customeradmins->count() > 0)
                            @foreach($company->customeradmins as $eachAdmin)
                                <tr>
                                    <td>{{translate('table.admin')}}</td>
                                    <td>{{$eachAdmin->name}}</td>
                                    <td>{{$eachAdmin->email}}</td>
                                    <td>{{$eachAdmin->phone}}</td>
                                    <td>
                                        @if(!empty($eachAdmin->positions()))
                                            @foreach($eachAdmin->positions() as $eachPos)
                                            {{$eachPos->position->name}}
                                            @if (!$loop->last)
                                            ,
                                            @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>{{!empty($eachAdmin->company->log) ? $eachAdmin->company->log->in_time : ''}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" align="center">{{translate('table.no_data')}}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
            {{translate('words.data')}}
            </div>
            <div class="panel-body pn">
                <div class="row">
                    <div class="col-md-5 col-md-offset-1">
                        <span class="fw700 fs16"> {{translate('form.branch')}} : </span> 
                        {{$branchAdded}}
                    </div>
                    <div class="col-md-6">
                        <span class="fw700 fs16"> {{translate('words.department')}} : </span> 
                        {{$departmentAdded}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-md-offset-1">
                        <span class="fw700 fs16"> {{translate('words.user')}} : </span> 
                        {{$userAdded}}
                    </div>
                    <div class="col-md-6">
                        <span class="fw700 fs16"> {{translate('words.process')}} : </span> 
                        {{$processAdded}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop