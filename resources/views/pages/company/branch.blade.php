@extends('layouts.default')
	@section('content')
	<div class="row" id="branch">
	    <div class="col-md-7">
	        <div class="panel panel-visible" id="spy2">
	            <div class="panel-heading">
	                <div class="panel-title hidden-xs">
	                    {{translate('words.list')}}
	                </div>
	            </div>
	            <div class="panel-body">
	            	<list-table :list="branches" />
	            </div>
	        </div>
	    </div>
	    <div class="col-md-5">
	        <div class="panel panel-visible" id="spy2">
	            <div class="panel-heading">
	                <div class="panel-title hidden-xs">
	                    {{translate('words.add')}} / {{translate('words.edit')}}
	                </div>
	            </div>
	            <div class="panel-body bg-light">
	                <div class="admin-form">
	                    <div class="row">
	                        <div class="form-group">
	                            {!! Form::label('name', translate('words.branch'), ['class' => 'col-lg-4 control-label required pt10']) !!}
	                            <div class="col-md-8">
	                                <div class="section">
	                                    {!! Form::text('', '', ['v-model' => 'name', ':class' => 'nameClass', 'tabindex' => '1']) !!}
	                                </div>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-lg-4 control-label pt10">
		                            {{translate('words.is_corporate')}}
		                            <i class="fa fa-info-circle text-danger pointer" data-toggle="tooltip" data-placement="top" title="{{translate('words.is_corporate_tooltip_text')}}"></i>
	                            </label>
	                            <div class="col-md-8">
	                                <div class="section">
	                                    <label class="switch-extended switch-success">
					                    	{!! Form::checkbox('is_corporate', 1, '', ['v-model' => 'is_corporate', 'v-on:change' => 'checkCorporate()'] ) !!} 
					                        <div class="slider-extended round"></div>
					                    </label> 
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="col-md-12 text-right">
	                            <input type="hidden" v-model="hid" />
	                            {!! Form::button(translate('form.save_button'), ['class' => 'btn btn-system', 'tabindex' => '2', '@click' => 'saveData', ':disabled' => 'enabled']) !!}
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	@include('includes.scripts.company.branch_script')
@stop