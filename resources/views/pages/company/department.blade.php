@extends('layouts.default')
@section('content')
<div class="row" id="departments" v-cloak>
	<div class="col-md-7">
		<div class="panel panel-visible" id="spy2">
			<div class="panel-heading">
				<div class="panel-title hidden-xs">
					{{translate('words.list')}}
				</div>
			</div>
			<div class="panel-body">
				<list-table :list="list" />
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="panel panel-visible" id="spy2">
			<div class="panel-heading">
				<div class="panel-title hidden-xs">
					{{translate('words.add')}} / {{translate('words.edit')}}
				</div>
			</div>
			<div class="panel-body bg-light">
				<div class="admin-form">
					<div class="row">
						<div class="form-group">
							{!! Form::label('branch', translate('words.branch'), ['class' => 'col-lg-4 control-label required pt10']) !!}
							<div class="col-md-8">
								<div class="section branch">
									{!! Form::select('branch', $branches, '', ['class'=>'form-control', 'id' => 'branch', 'v-model' => 'branch']) !!}
									<p class="text-danger mn">{{$errors->first('branches')}}</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							{!! Form::label('name', translate('words.department'), ['class' => 'col-lg-4 control-label required pt10']) !!}
							<div class="col-md-8">
								<div class="section">
									{!! Form::text('name', '', ['v-model' => 'name', ':class' => 'nameClass', 'tabindex' => '1']) !!}
									<p class="text-danger mn">{{$errors->first('name')}}</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							{!! Form::button(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'tabindex' => '2', 'v-on:click' => 'saveData', ':disabled' => 'enabled']) !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('includes.scripts.company.department_script')
@stop