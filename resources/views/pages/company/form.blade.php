@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(['method' => 'post', 'url' => '/form', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) !!}
            
            <div class="row">
                <div class="col-md-6 text-right">
                    <i class="fa fa-cog fa-spin text-system fa-2x hide" id="ajxLoader"></i>
                </div>
                <div class="col-md-6">
                    {!! Html::link(url('/list'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                    @if(!empty($user->invite_user_token))
                        <a href="javascript:void(0)" class="btn btn-system pull-right mr5" id='resent_mail' data-id='{{$user->id}}'>
                            {{translate('form.resent_invitation_mail')}}
                        </a>
                    @endif
                </div>
            </div>

            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.personal_details')}}</span>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    @if(isset($company) and $company->trial_start != null and empty($company->subscription_id))
                       <span class="lbl switch-custom-pos">{{translate('form.trial_period_days')}}</span>
                       <label for="monthpicker1" class="field prepend-icon">
                            <input type="text" name="trail_end_date" class="form-control" id="datetimepicker1" value="{{$company->trial_end_date or ''}}" required="">
                            <label class="field-icon">
                                <i class="fa fa-calendar-o"></i>
                            </label>
                        </label>
                    @endif
                </div>
                <div class="col-md-4">
                    {{ Html::image(asset(((isset($user) and ($user->photo != 'placeholder.png')) ? ('storage/company/'.$company->id.'/image/users/'.$user->photo.'?'.rand(0,9)) : '/image/placeholder.png')), 'User Image', array('class' => 'thumbnail pull-right', 'id' => 'user_preview', 'height' => '100', 'width' => '100')) }}
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('email', translate('form.email'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {!! Form::email('email', isset($user) ? $user->email : old('email'), array('class' => 'gui-input', 'id' => 'email', 'placeholder' => translate('form.email'), 'required' => 'required', 'tabindex' => '1')) !!}
                            <label for="email" class="field-icon">
                                <i class="fa fa-envelope"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('email')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('user_photo', translate('form.picture'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field prepend-icon append-button file">
                            <span class="button">{{translate('form.file')}}</span>
                            {!! Form::file('photo', array('class' => 'gui-file', 'id' => 'user_photo', 'accept' => '.jpeg,.jpg,.png')) !!}
                            {!! Form::text('', '', array('class' => 'gui-input', 'id' => 'uploader1', 'placeholder' => translate('form.image'))) !!}
                            <label class="field-icon">
                                <i class="fa fa-upload"></i>
                            </label>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('fname', translate('form.first_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('fname', isset($user) ? $user->fname : old('fname'), array('class' => 'gui-input', 'id' => 'fname', 'placeholder' => translate('form.first_name'), 'required' => 'required', 'tabindex' => '2')) !!}
                            <label for="fname " class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('fname')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('lname', translate('form.last_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('lname', isset($user) ? $user->lname : old('lname'), array('class' => 'gui-input', 'id' => 'lname', 'placeholder' => translate('form.last_name'), 'required' => 'required', 'tabindex' => '3')) !!}
                            <label for="lname" class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('lname')}}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('language', translate('form.language'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field select">
                            {!! Form::select('language', $language, isset($user->language) ? $user->language : old('language'), array('id' => 'language', 'required' => 'required', 'tabindex' => '6')) !!}
                            <i class="arrow"></i>
                        </label>
                        <p class="text-danger mn">
                            {{$errors->first('language', translate('error.language_req'))}}
                        </p>
                    </div>
                </div>
            </div>

            @if(!empty($company->subscription_id))
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <label class="switch-extended switch-success">
                        {!! Form::checkbox('can_process_owner', 1, (isset($user) and $user->can_process_owner==1)) !!}
                        <div class="slider-extended round"></div>
                    </label>
                    <span class="lbl switch-custom-pos">{{translate('form.process_owner')}}</span>
                </div>
                <div class="col-md-4">
                    <label class="switch-extended switch-success">
                        {!! Form::checkbox('is_auditor', 1, (isset($user) and $user->is_auditor==1)) !!}
                        <div class="slider-extended round"></div>
                    </label>
                    <span class="lbl switch-custom-pos">{{translate('form.is_auditor')}}</span>
                </div>
            </div>
            @endif

            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.company_details')}}</span>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('name', translate('form.company_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('name', isset($company) ? $company->name : old('name'), array('class' => 'gui-input', 'id' => 'name', 'placeholder' => translate('form.company_name'), 'required' => 'required', 'tabindex' => '7')) !!}
                            <label for="name" class="field-icon">
                                <i class="fa fa-bank"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('name')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                @if(!empty($company->subscription_id))
                    <div class="section">
                        {!! Form::label('tva_no', translate('form.company_tva'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('tva_no', isset($company) ? $company->tva_no : old('tva_no'), array('class' => 'gui-input', 'id' => 'tva_no', 'placeholder' => translate('form.company_tva'), 'required' => 'required', 'tabindex' => '8')) !!}
                            <label for="tva_no" class="field-icon">
                                <i class="fa fa-bank"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('tva_no')}}</p>
                    </div>
                @endif
                </div>
            </div>

            @if(!empty($company->subscription_id))
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('address_line1', translate('form.addr1'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('address_line1', (isset($company->address_line1)) ? $company->address_line1 : old('address_line1'), array('class' => 'gui-input', 'id' => 'address_line1', 'placeholder' => translate('form.addr1'), 'required' => 'required', 'tabindex' => '9')) !!}
                            <label for="address_line1" class="field-icon">
                                <i class="fa fa-map-marker"></i>
                            </label>
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('address_line1')}}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('address_line2', translate('form.addr2'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field append-icon">
                            {!! Form::text('address_line2', (isset($company->address_line2)) ? $company->address_line2 : old('address_line2'), array('class' => 'gui-input', 'id' => 'address_line2', 'placeholder' => translate('form.addr2'), 'tabindex' => '10')) !!}
                            <label for="address_line2" class="field-icon">
                                <i class="fa fa-map-marker"></i>
                            </label>
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('address_line2')}}</p>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('zip', translate('form.zip'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('comp_zip', isset($company) ? $company->zip : old('comp_zip'), array('class' => 'gui-input', 'id' => 'zip', 'placeholder' => translate('form.zip'), 'required' => 'required', 'tabindex' => '11')) !!}
                            <label for="comp_zip" class="field-icon">
                                <i class="fa fa-map-marker"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('comp_zip')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('city', translate('form.city'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('comp_city', isset($company) ? $company->city : old('comp_city'), array('class' => 'gui-input', 'id' => 'city', 'placeholder' => translate('form.city'), 'required' => 'required', 'tabindex' => '12')) !!}
                            <label for="comp_city" class="field-icon">
                                <i class="fa fa-map-marker"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('comp_city')}}</p>
                    </div>
                </div>
            </div>
            @endif

            @if(!empty($company->subscription_id))
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('country', translate('form.country'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field select">
                            {!! Form::select('comp_country', $countries, isset($company->country) ? $company->country : '', array('id' => 'country', 'required' => 'required', 'tabindex' => '13')) !!}
                            <i class="arrow"></i>
                        </label>
                        <p class="text-danger mn">
                            {{$errors->first('comp_country', translate('error.comp_country_req'))}}
                        </p>
                    </div>
                    <br clear="all">
                    @if(!empty($user->id))
                    <span class="lbl fs16 pos-relative top11">{{translate('form.enable')}}</span>
                    <label class="switch-extended switch-success">
                        {!! Form::checkbox('block', 0, (isset($company) and $company->status == 2)) !!}
                        <div class="slider-extended round" ></div>
                    </label>
                    <span class="lbl fs16 pos-relative top11">{{translate('form.disable')}}</span>
                    @endif
                </div>
                <div class="col-md-4">
                    {{ Html::image(asset(((isset($company) and ($company->photo != 'placeholder.png')) ? ('storage/company/'.$company->id.'/image/'.$company->photo.'?'.rand(0,9)) : '/image/placeholder.png')), 'Company Logo', array('class' => 'thumbnail pull-right', 'id' => 'company_preview', 'height' => '100', 'width' => '100')) }}

                    {!! Form::label('company_photo', translate('form.company_picture'), array('class' => 'field-label fs15 mb5')) !!}
                    <label class="field prepend-icon append-button file">
                        <span class="button">{{translate('form.file')}}</span>
                        {!! Form::file('company_photo', array('class' => 'gui-file', 'id' => 'company_photo', 'accept' => '.jpeg,.jpg,.png')) !!}
                        {!! Form::text('', '', array('class' => 'gui-input', 'id' => 'uploader2', 'placeholder' => translate('form.image'))) !!}
                        <label class="field-icon">
                            <i class="fa fa-upload"></i>
                        </label>
                    </label>
                </div>
            </div>
            @endif

            @if(!isset($user->id))
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <label class="switch-extended switch-success">
                        {!! Form::checkbox('invite_user', 1, '',['checked']) !!}
                        <div class="slider-extended round"></div>
                    </label>
                    <span class="lbl switch-custom-pos">{{translate('form.invite_user')}}</span>
                </div>
                <div class="col-md-4"></div>
            </div>
            @endif

            <div class="section-divider mv40" id="spy2"></div>

            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('masterid', isset($user) ? $user->id : '') !!}
                    {!! Form::hidden('compid', isset($company->id) ? $company->id : '') !!}
                    {!! Form::hidden('subscription_id', isset($company->subscription_id) ? $company->subscription_id : '') !!}
                    {!! Html::link(url('/list'), translate('form.cancel'), ['class' => 'btn btn-hover btn-system']) !!}
                    {!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'id' => 'profile-btn']) !!}
                    <br clear="all">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop