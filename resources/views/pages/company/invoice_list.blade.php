@extends('layouts.default')
@section('content')
@inject('countryName', 'App\Services\Membership')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-8 pt5">
                        {{translate('words.invoice_list')}}
                    </div>
                    <div class="col-md-1 pt5 text-right">
                        {{ucfirst(translate('words.filter'))}} : 
                    </div>
                    <div class="col-md-2">
                        {!! Form::select('company', $companies, isset($selectedCompany) ? $selectedCompany : '', ['class' => 'form-control', 'id' => 'company-id']) !!}  
                    </div>
                    <div class="col-md-1 pt3">
                        <a class="btn btn-system" href="{{($selectedCompany == '') ? url('/home/download-invoice-list') : url('/home/download-invoice-list/'.$selectedCompany)}}"><i class="fa fa-download"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body pn">
                @if($transactionDetail->count() > 0)
                <table class="table table-striped table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.invoice_date')}}</th>
                            <th>{{translate('table.invoice_no')}}</th>               
                            <th>{{translate('table.amount')}}</th>
                            <th>{{translate('table.vat_amount')}}</th>
                            <th>{{translate('table.total_amount')}}</th>
                            <th>{{translate('table.company')}}</th>
                            <th>{{translate('table.address')}}</th>
                            <th>{{translate('table.zip')}}</th>
                            <th>{{translate('table.city')}}</th>
                            <th>{{translate('table.country')}}</th>
                            <th>{{translate('table.tva_no')}}</th>
                        </tr>
                    </thead>
                    <tbody> 
                        @foreach($transactionDetail as $invoice)
                            @if($invoice->transaction_id != null)
                            <tr>
                                <td>{{date(\Config::get('settings.dashed_date'), strtotime($invoice->payment_date))}}</td>
                                <td>{{$invoice->invoice_no}}</td>
                                <td>&euro; {{$invoice->amount}}</td>
                                <td>&euro; {{$invoice->vat_amount}}</td>
                                <td>&euro; {{$invoice->total_amount}}</td>
                                <td>{{$invoice->company->name}}</td>
                                <td>{{$invoice->invoicedata->street}}, {{$invoice->invoicedata->nr}}, {{$invoice->invoicedata->bus}}</td>
                                <td>{{$invoice->invoicedata->zip}}</td>
                                <td>{{$invoice->invoicedata->city}}</td>
                                <td>{{$countryName->countryName($invoice->company->countries)}}</td>  
                                <td>{{$invoice->invoicedata->tva_no}}</td>
                            </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
                @else
                    <div class="row">
                        <div class="col-md-12 text-center fs20 p10">{{translate('table.no_data')}}</div>
                    </dov>
                @endif
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {    
    $(document).on('change', '#company-id', function() {
        var companyId = $(this).val();
        if(companyId != ''){
            $(location).attr('href', '{{url("/home/show-invoice")}}/'+companyId);
        } else{
            $(location).attr('href', '{{url("/home/show-invoice")}}');
        }
    });
});
</script>
@stop