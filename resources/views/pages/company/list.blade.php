@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">          
                    {!! Html::link(url('/form'), translate('words.add'), ['class' => 'btn btn-system btn-hover']) !!}
                </div>
            </div>
            <div class="p15 table-responsive">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.photo')}}</th>
                            <th>{{translate('table.company')}}</th>
                            <th>{{translate('table.tva_no')}}</th>
                            <th>{{translate('table.name')}}</th>
                            <th>{{translate('table.email')}}</th>
                            <th>{{translate('table.last_login')}}</th>
                            <th>{{translate('table.trial_period')}}</th>
                            <th width="10%">{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('status', 'App\Services\CompanyList')
                        @foreach($users as $eachUser)
                            <tr>
                                <td>
                                    @if(\File::isFile('storage/company/'.$eachUser->company->id.'/image/'.$eachUser->company->photo))
                                        <img src="{{asset('storage/company/'.$eachUser->company->id.'/image/'.$eachUser->company->photo)}}" height="50">
                                    @else
                                        <img src="{{asset('image/placeholder.png')}}" height="50">
                                    @endif
                                </td>
                                <td>{{$eachUser->company->name}}</td>
                                <td>{{$eachUser->company->tva_no}}</td>
                                <td>{{$eachUser->name}}</td>
                                <td>{{$eachUser->email}}</td>
                                <td>{{isset($eachUser->company->log->in_time) ? $eachUser->company->log->in_time : ''}}</td>
                                <td>
                                    @if(empty($eachUser->company->subscription_id))
                                    {{$eachUser->company->trial_days}} {{translate('words.day_left')}}
                                    @else
                                    {{translate('words.na')}}
                                    @endif
                                </td>
                                <td>
                                    <div class="disp-flex">
                                        <a href="{{ url('/edit/'.$eachUser->id) }}" class="btn btn-default mr5" title="{{translate('words.edit')}}">
                                            <span class="text-success fa fa-pencil"></span>
                                        </a>
                                        <a href="{{ url('/account-info/'.$eachUser->id) }}" class="btn btn-default mr5" title="{{translate('words.account_info')}}">
                                            <span class="text-success fa fa-eye"></span>
                                        </a>
                                        <a href="{{ url('/remove/'.$eachUser->company->id) }}" class="btn btn-default" title="{{translate('words.remove')}}" onclick="return confirm('{{translate('alert.company_removal_message')}}')">
                                            <span class="text-danger fa fa-trash"></span>
                                        </a>
                                    </div>
                                </td>
                            </tr>	
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop