@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    <button type="button" class="btn btn-hover btn-system" onclick="location.href ='{{url('/subscription-list')}}'">{{ucfirst(translate('form.back'))}}</button>
                </div>
            </div>
            <div class="p15 table-responsive">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.package_price_change')}}</th>
                            <th>{{translate('table.user_price_change')}}</th>
                            <th>{{translate('table.storage_price_change')}}</th>
                            <th>{{translate('table.proposed_percent')}}</th>
                            <th>{{translate('table.approve_status')}}</th>
                            <th>{{translate('table.approve_date')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('status', 'App\Services\CompanyList')
                        @foreach($priceIncLog as $eachLog)
                            <tr>
                                <td>&euro; {{$eachLog['current_pkg_price']}} <i class="glyphicon glyphicon-arrow-right"></i> &euro; {{$eachLog['new_pkg_price']}}</td>
                                <td>&euro; {{$eachLog['current_user_price']}}  <i class="glyphicon glyphicon-arrow-right"></i> &euro;{{$eachLog['new_user_price']}}</td>
                                <td>&euro; {{$eachLog['current_storage_price']}}  <i class="glyphicon glyphicon-arrow-right"></i> &euro; {{$eachLog['new_storage_price']}}</td>
                                <td>{{$eachLog['proposed_percent']}} %</td>
                                <td>{{$eachLog['status']}}</td>
                                <td>{{$eachLog['approve_date']}}</td>
                            </tr>	
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop