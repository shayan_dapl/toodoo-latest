@if($parentRequired == '')
    <label class="col-md-10 control-label fs16 fw700 pt5">
        {{translate('form.this_is_parent_position')}}
    </label>
    <select id="parent_id" name="parent_id" class="hide">
        <option value="">
            &nbsp;
        </option>
    </select>
@else
<label for="parent_id" class="col-md-4 control-label {{$parentRequired}}">{{translate('form.parent_position')}}</label>
<div class="col-md-8">
    <label class="field select">
        <select id="parent_id" tabindex="1" name="parent_id" {{$parentRequired}}>
        	@if(!empty($parent))
        		@foreach($parent as $val => $text)
        			<option value="{{$val}}">{{$text}}</option>
        		@endforeach
    		@endif
        </select>
        <i class="arrow"></i>
    </label>
    <p class="text-danger mn">{{$errors->first('parent_id')}}</p>
</div>
@endif