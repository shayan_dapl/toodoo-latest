@extends('layouts.default')
@section('content')
<div class="admin-form" id="position-form" v-cloak>
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(['method' => 'post', 'url' => '/position/form', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off', 'class' => 'form-horizontal']) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/position/list'), translate('form.back'), ['class' => 'btn btn-hover btn-system pull-right']) !!}
                </div>
            </div>

            <div class="section-divider mb40" id="spy1">
                <span></span>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel-body">
                        @if(isset($details) and $details->parent_id == null)
                            {!! Form::hidden('branch', $selectedBranches) !!}
                            {!! Form::hidden('department_id', $details->department_id) !!}
                            <div class="row">
                                <div class="form-group">
                                    {!! Form::label('branch', translate('form.branch'), ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-8">
                                        <label class="control-label fs16 fw700 pt10">
                                            {{$details->branches->branch->name}}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    {!! Form::label('department', translate('words.department'), ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-8">
                                        <label class="control-label fs16 fw700 pt10">
                                            {{$details->department->name}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        @else
                        <!-- Branches -->
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('branch', translate('form.branch'), ['class' => 'col-md-4 control-label required']) !!}
                                <div class="col-md-8">
                                    <label class="field select">
                                        {!! Form::select('branch', $branches, '', ['id' => 'branch', 'required' => 'required', 'v-model' => 'branch', 'v-on:change' => 'changeBranch']) !!}
                                        <i class="arrow"></i>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- Department -->
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('department', translate('words.department'), ['class' => 'col-md-4 control-label required']) !!}
                                <div class="col-md-8">
                                    <label class="field select">
                                        <select name="department_id" id="department_id" tabindex="2" required="required" v-model="department">
                                            <option value="">{{translate('form.select')}}</option>
                                            <option v-for="each in departments" :value="each.id">@{{each.name}}</option>
                                        </select>
                                        <i class="arrow"></i>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- Parent position -->
                        <div class="row">
                            <div class="form-group" id="parent-pos">
                                @if(isset($details) and $details->parent_id == null)
                                    <select id="parent_id" name="parent_id" class="hide">
                                        <option value="">
                                            &nbsp;
                                        </option>
                                    </select>
                                @else
                                    {!! Form::label('parent_id', translate('form.parent_position'), ['class' => 'col-md-4 control-label required']) !!}
                                    <div class="col-md-8">
                                        @if (isset($details) and $details->department_id == $details->parent->department_id)
                                            <label class="field select">
                                                {!! Form::select('parent_id', $parent, (isset($details) ? $details->parent_id : old('parent_id')), ['id' => 'parent_id', 'tabindex' => '1', 'required' => 'required']) !!}
                                                <i class="arrow"></i>
                                            </label>
                                        @else
                                            @if (isset($details))
                                                <label class="control-label fs16 fw700 pt10">{{$details->parent->name}}</label>
                                                {!! Form::hidden('parent_id', $details->parent_id) !!}
                                            @else
                                                <label class="field select">
                                                    {!! Form::select('parent_id', $parent, (isset($details) ? $details->parent_id : old('parent_id')), ['id' => 'parent_id', 'tabindex' => '1', 'required' => 'required']) !!}
                                                    <i class="arrow"></i>
                                                </label>
                                            @endif
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                        <!-- Position name -->
                        <div class="row">
                            <div class="form-group">
                                @if(isset($details) and $details->is_assistant == 1)
                                    {!! Form::label('name', translate('form.assist_as'), ['class' => 'col-md-4 control-label required', 'id' => 'position-name-label']) !!}
                                @else
                                    {!! Form::label('name', !empty(old('assistant')) ? translate('form.assist_as') : translate('form.position_name'), ['class' => 'col-md-4 control-label required', 'id' => 'position-name-label']) !!}
                                @endif
                                <div class="col-md-8">
                                    <label class="field prepend-icon">
                                        {!! Form::text('name', (isset($details->name) ? $details->name : old('name')), ['class' => 'gui-input', 'id' => 'name', 'placeholder' => translate('form.name'), 'required' => 'required', 'tabindex' => '1']) !!}
                                        <label for="name" class="field-icon">
                                            <i class="fa fa-cube"></i>
                                        </label>
                                    </label>
                                    <p class="text-danger mn">
                                        {{$errors->first('name')}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- Is Assistant Position -->
                        @if( !isset($details) )
                        <div class="row" id="assistant-div">
                            <div class="form-group">
                                {!! Form::label('assistant', translate('form.assistant'), ['class' => 'col-lg-4 control-label']) !!}
                                <div class="col-md-8">
                                    <label class="switch-extended switch-success">
                                        {!! Form::checkbox('assistant', 1, '', ['id' => 'assistant']) !!}
                                        <div class="slider-extended round"></div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-8 col-lg-offset-4 fs16 fw700">
                                    @if(isset($details) and $details->is_assistant == 1)
                                        {{translate('form.assistant')}} {{translate('form.position')}}
                                        {!! Form::hidden('assistant', $details->is_assistant) !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- Is special Position -->
                        @if(!isset($details) or (isset($details) and $details->parent_id != null and $details->is_special_position == 1))
                        <div class="row" id="special-user-div">
                            <div class="form-group">
                                {!! Form::label('special_position', translate('form.special_position'), ['class' => 'col-lg-4 control-label']) !!}
                                <div class="col-md-8">
                                    <label class="switch-extended switch-success">
                                        {!! Form::checkbox('is_special_position', 1, (isset($details->is_special_position) and $details->is_special_position == 1)) !!}
                                        <div class="slider-extended round"></div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- Information -->
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('info', translate('form.info'), ['class' => 'col-md-4 control-label']) !!}
                                <div class="col-md-8">
                                    <div class="section">
                                        <label class="field prepend-icon">
                                            {{ Form::textarea('info', (Session::has('positionInfo')) ? Session::get('positionInfo') : (isset($details->info) ? $details->info : old('info')), ['class' => 'gui-input', 'placeholder' => translate('form.info'), 'tabindex' => '2']) }}
                                            <label for="name" class="field-icon">
                                                <i class="fa fa-newspaper-o"></i>
                                            </label>
                                        </label>
                                        <p class="text-danger mn">{{$errors->first('info')}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Existing users list -->
                        <div class="row {{(old('manage_normal_user') or old('manage_special_user') or (isset($details->is_special_position) and $details->is_special_position == 1)) ? 'hide' : ''}}" id="users-panel">
                            <div class="form-group">
                                {!! Form::label('info', translate('form.user_with_position'), ['class' => 'col-md-4 control-label']) !!}
                                <div class="col-md-8">
                                    <div class="section">
                                        <label class="field prepend-icon">
                                            <span id="normal-span" class="@if((isset($details->is_special_position) and $details->is_special_position==1)) hide @endif">
                                                <select name="user_normal" class="select2-single form-control">
                                                    <option value="">{{translate('form.select')}}</option>
                                                    @if(!empty($users))
                                                    @foreach($users as $user)
                                                    @if($user->status != 3)
                                                    <option value="{{$user->id}}" @if(isset($holdingUser) and $holdingUser == $user->id) selected @endif>
                                                        {{$user->name}}
                                                    </option>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </span>
                                            <span id="special-span" class="@if((isset($details->is_special_position) and $details->is_special_position==0) || !isset($details->is_special_position)) hide @endif">
                                                <select name="user_special" class="select2-single form-control">
                                                    <option value="">{{translate('form.select')}}</option>
                                                    @if(!empty($specialUsers))
                                                    @foreach($specialUsers as $user)
                                                    @if($user->status != 3)
                                                    <option value="{{$user->id}}" @if(isset($holdingUser) and $holdingUser == $user->id) selected @endif>
                                                        {{$user->name}}
                                                    </option>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Not in list -->
                        @if(!isset($details))
                            <div class="row">
                                <div class="form-group {{(old('manage_special_user') or (isset($details->is_special_position) and $details->is_special_position==1)) ? 'hide' : ''}}" id="normal-user-checkbox">
                                    {!! Form::label('info', translate('form.not_in_list'), ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-8">
                                        <label class="switch-extended switch-success">
                                            {!! Form::checkbox('manage_normal_user', 1, '', ['id' => 'manage-normal-user']) !!}
                                            <div class="slider-extended round"></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group {{(old('manage_special_user')) ? '' : 'hide'}}" id="special-user-checkbox">
                                    {!! Form::label('info', translate('form.not_in_list'), ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-8">
                                        <label class="switch-extended switch-success">
                                            {!! Form::checkbox('manage_special_user', 1, '', ['id' => 'manage-special-user']) !!}
                                            <div class="slider-extended round"></div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-6 {{(old('manage_normal_user')) ? '' : 'hide'}}" id="normal-user">
                    <div class="panel-heading bg-system p10 text-default">
                        {{translate('words.user')}} - {{translate('form.personal_details')}}
                    </div>
                    <div class="panel-body bl">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="section">
                                    <span id="no-email-span">
                                        {!! Form::label('no_email', translate('form.no_email'), array('class' => 'field-label fs15 mb5')) !!}
                                        <label class="switch-extended switch-success">
                                            {!! Form::checkbox('no_email', 1, (isset($details->master->no_email) and $details->master->no_email==1), ['id' => 'no_email']) !!}
                                            <div class="slider-extended round"></div>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>

                        <div class="row">
                            <div id="email-div" class="col-md-6">
                                <span id="user-email" class="{{(old('no_email')) ? 'hide' : ''}}">
                                    {!! Form::label('email', translate('form.email'), array('class' => 'field-label fs15 mb5 required', 'id' => 'email-label')) !!}

                                    <label class="field prepend-icon">
                                        {!! Form::email('email', old('email'), ['class' => 'gui-input', 'id' => 'email', 'placeholder' => translate('form.email'), 'tabindex' => '4', 'v-model' => 'email', 'v-on:blur' => 'getImage']) !!}

                                        <label for="email" class="field-icon">
                                            <i class="fa fa-envelope"></i>
                                        </label>
                                    </label>
                                    <p class="text-danger mn">{{$errors->first('email')}}</p>
                                </span>
                            </div>
                            <div id="image-div" class="{{(old('no_email')) ? 'col-md-12' : 'col-md-6'}}">
                                <label for="user_photo" class="pull-left userPhoto">
                                    <i class="fa fa-cloud-upload pos-relative user-img-icon fs20 pointer"></i> 
                                    {{ Html::image('', '', [':src' => 'imgsrc', 'class' => 'thumbnail pointer', 'id' => 'user_preview', 'height' => '100', 'width' => '100']) }}
                                </label>
                                <input type="file" id="user_photo" class="hide" accept=".jpeg,.jpg,.png" v-on:change="onImageChange" />
                                <input type="hidden" name="photo" :value="image" />
                                <div class="col-md-12">
                                    <div v-if="isityou">
                                        <p class="text-danger">{{translate('alert.is_it_you')}}</p>
                                        <button type="button" class="btn btn-success" @click="confirmIt('yes')">{{translate('form.yes')}}</button>
                                        <button type="button" class="btn btn-danger" @click="confirmIt('no')">{{translate('form.no')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="section">
                                    {!! Form::label('fname', translate('form.first_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                                    <label class="field prepend-icon">
                                        {!! Form::text('fname', (isset($details->fname)) ? $details->fname : old('fname'), array('class' => 'gui-input', 'id' => 'fname', 'placeholder' => translate('form.first_name'), 'tabindex' => '5')) !!}
                                        <label for="fname" class="field-icon">
                                            <i class="fa fa-user"></i>
                                        </label>
                                    </label>
                                    <p class="text-danger mn">{{$errors->first('fname')}}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="section">
                                    {!! Form::label('last_name', translate('form.last_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                                    <label class="field append-icon">
                                        {!! Form::text('last_name', (isset($details->lname)) ? $details->lname : old('lname'), array('class' => 'gui-input', 'id' => 'last_name', 'placeholder' => translate('form.last_name'), 'tabindex' => '6')) !!}
                                        <label for="last_name" class="field-icon">
                                            <i class="fa fa-user"></i>
                                        </label>
                                    </label>
                                    <p class="text-danger mn">{{$errors->first('last_name')}}</p>
                                </div>
                            </div>
                        </div>      
                        <div class="row">
                            <div id="language-div" class="{{(old('no_email')) ? 'col-md-12' : 'col-md-6'}}">
                                <div class="section">
                                    {!! Form::label('language', translate('form.language'), array('id' => 'language-label', 'class' => 'field-label fs15 mb5 required')) !!}
                                    <label class="field select">
                                        {!! Form::select('language', $language, old('language'), array('id' => 'language', 'tabindex' => '9')) !!}
                                        <i class="arrow"></i>
                                    </label>
                                    <p class="text-danger mn">{{$errors->first('language')}}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <span id="is-auditor" class="{{(old('no_email')) ? 'hide' : ''}}">
                                    {!! Form::label('is_auditor', '&nbsp;', array('class' => 'field-label fs15 mb5')) !!}
                                    <label class="switch-extended switch-success">
                                        {!! Form::checkbox('is_auditor', 1, '', ['id' => 'is_auditor']) !!}
                                        <div class="slider-extended round"></div>
                                    </label>
                                    <span class="lbl switch-custom-pos">{{translate('form.is_auditor')}}</span>
                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <span id="can-process-owner" class="{{(old('no_email')) ? 'hide' : ''}}">
                                    <label class="switch-extended switch-success">
                                        {!! Form::checkbox('can_process_owner', 1, '', ['id' => 'can_process_owner']) !!}
                                        <div class="slider-extended round"></div>
                                    </label>
                                    <span class="lbl fs16 pos-relative top11">
                                        {{translate('form.process_owner')}}
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 {{(old('manage_special_user')) ? '' : 'hide'}}" id="special-user">
                    <!-- Special single user -->
                    <label class="switch switch-round block mt15">
                        <input type="checkbox" name="special_user" class="special-user-check" id="sp1" value="special_single" {{(old('special_user_check') == 'special_multiple') ? '' : 'checked'}}>
                        <label for="sp1" data-on="YES" data-off="NO"></label>
                        <span>{{(old('special_user_check') == 'special_multiple') ? translate('form.special_multiple_user') : translate('form.special_single_user')}}</span>
                    </label>
                    {!! Form::hidden('special_user_check', 'special_single') !!}

                    <div class="panel-heading bg-system p10 text-default {{(old('special_user_check') == 'special_multiple') ? 'hide' : ''}}" data-id="special-single-user-block">
                        {{translate('words.user')}} - {{translate('form.personal_details')}}
                    </div>
                    <div class="panel-body bl {{(old('special_user_check') == 'special_multiple') ? 'hide' : ''}}" data-id="special-single-user-block">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="section">
                                    <span id="no-email-span">
                                        {!! Form::label('no_email', translate('form.no_email'), array('class' => 'field-label fs15 mb5')) !!}
                                        <label class="switch-extended switch-success">
                                            {!! Form::checkbox('special_no_email', 1, (isset($details->master->no_email) and $details->master->no_email==1), ['id' => 'special_no_email']) !!}
                                            <div class="slider-extended round"></div>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                
                            </div>
                        </div>

                        <div class="row">
                            <div id="email-div" class="col-md-6">
                                <span id="special-user-email" class="{{(old('special_no_email')) ? 'hide' : ''}}">
                                    {!! Form::label('special_email', translate('form.email'), array('class' => 'field-label fs15 mb5', 'id' => 'email-label')) !!}

                                    <label class="field prepend-icon">
                                        {!! Form::email('special_email', old('special_email'), ['class' => 'gui-input', 'id' => 'special_email', 'placeholder' => translate('form.email'), 'tabindex' => '4', 'v-model' => 'specialEmail', 'v-on:blur' => 'specialGetImage']) !!}

                                        <label for="special_email" class="field-icon">
                                            <i class="fa fa-envelope"></i>
                                        </label>
                                    </label>
                                    <p class="text-danger mn">{{$errors->first('special_email')}}</p>
                                </span>
                            </div>
                            <div id="special-image-div" class="{{(old('special_no_email')) ? 'col-md-12' : 'col-md-6'}}">
                                <label for="special_user_photo" class="pull-left userPhoto">
                                    <i class="fa fa-cloud-upload pos-relative user-img-icon fs20 pointer"></i> 
                                    {{ Html::image('', 'User Image', array(':src' => 'specialImgsrc', 'id' => 'company_preview', 'class' => 'thumbnail pull-right pointer', 'height' => '100', 'width' => '100')) }}
                                </label>
                                <input type="file" class="hide" id="special_user_photo" accept=".jpeg,.jpg,.png" v-on:change="specialOnImageChange" />
                                <input type="hidden" name="special_photo" :value="specialImage" />
                                <div class="col-md-12">
                                    <div v-if="specialIsityou">
                                        <p class="text-danger">{{translate('alert.is_it_you')}}</p>
                                        <button type="button" class="btn btn-success" @click="specialConfirmIt('yes')">{{translate('form.yes')}}</button>
                                        <button type="button" class="btn btn-danger" @click="specialConfirmIt('no')">{{translate('form.no')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="section">
                                    {!! Form::label('special_first_name', translate('form.first_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                                    <label class="field prepend-icon">
                                        {!! Form::text('special_first_name', (isset($details->special_first_name)) ? $details->special_first_name : old('special_first_name'), array('class' => 'gui-input', 'id' => 'special_first_name', 'placeholder' => translate('form.first_name'), 'tabindex' => '5')) !!}
                                        <label for="special_first_name" class="field-icon">
                                            <i class="fa fa-user"></i>
                                        </label>
                                    </label>
                                    <p class="text-danger mn">
                                        {{$errors->first('special_first_name', translate('error.required_field'))}}
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="section">
                                    {!! Form::label('special_last_name', translate('form.last_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                                    <label class="field append-icon">
                                        {!! Form::text('special_last_name', (isset($details->lname)) ? $details->lname : old('lname'), array('class' => 'gui-input', 'id' => 'special_last_name', 'placeholder' => translate('form.last_name'), 'tabindex' => '6')) !!}
                                        <label for="special_last_name" class="field-icon">
                                            <i class="fa fa-user"></i>
                                        </label>
                                    </label>
                                    <p class="text-danger mn">
                                        {{$errors->first('special_last_name', translate('error.required_field'))}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="special-language-div" class="{{(old('special_no_email')) ? 'col-md-12' : 'col-md-6'}}">
                                <div class="section">
                                    {!! Form::label('special-language', translate('form.language'), array('id' => 'special-language-label', 'class' => 'field-label fs15 mb5')) !!}
                                    <label class="field select">
                                        {!! Form::select('special_language', $language, old('special_language'), array('id' => 'special-language', 'tabindex' => '9')) !!}
                                        <i class="arrow"></i>
                                    </label>
                                    <p class="text-danger mn">{{$errors->first('special_language')}}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <span id="is-special-auditor" class="{{(old('special_no_email')) ? 'hide' : ''}}">
                                    {!! Form::label('is_special_auditor', '&nbsp;', array('class' => 'field-label fs15 mb5')) !!}
                                    <label class="switch-extended switch-success">
                                        {!! Form::checkbox('is_special_auditor', 1, '', ['id' => 'is_special_auditor']) !!}
                                        <div class="slider-extended round"></div>
                                    </label>
                                    <span class="lbl switch-custom-pos">{{translate('form.is_auditor')}}</span>
                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <span id="can-special-process-owner" class="{{(old('special_no_email')) ? 'hide' : ''}}">
                                    <label class="switch-extended switch-success">
                                        {!! Form::checkbox('can_special_process_owner', 1, '', ['id' => 'can_special_process_owner']) !!}
                                        <div class="slider-extended round"></div>
                                    </label>
                                    <span class="lbl fs16 pos-relative top11">
                                        {{translate('form.process_owner')}}
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- Special multi user -->
                    <div class="panel-heading bg-system p10 text-default {{(old('special_user_check') == 'special_multiple') ? '' : 'hide'}}" data-id="special-multi-user-block">
                        {{translate('form.special_users')}} - {{translate('form.upload_doc')}}
                    </div>
                    <div class="panel-body bl {{(old('special_user_check') == 'special_multiple') ? '' : 'hide'}}" data-id="special-multi-user-block">
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('user_photo', translate('form.upload_doc'), array('class' => 'col-lg-3 col-md-3 control-label p10')) !!}
                                <div class="col-md-9">
                                    <label class="field prepend-icon append-button file">
                                        <span class="button">{{translate('form.file')}}</span>
                                        <input type="file" class="gui-file" name="doc_file" accept=".csv,.xls,.xlsx" onchange="document.getElementById('docname').value = this.files[0].name">
                                        <input type="text" class="gui-input" id="docname" placeholder="{{translate('form.image')}}">
                                        <label class="field-icon">
                                            <i class="fa fa-upload"></i>
                                        </label>
                                    </label>
                                    <p class="text-danger mn">{{$errors->first('doc_file')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row pl10">
                            <div class="col-md-2">
                                <a href="{{asset('special_user_datasheet/Special_position_holders.xls')}}" target="_blank"><i class="fa fa-file-excel-o fa-3x text-system"></i></a>
                            </div>
                            <div class="col-md-10">
                                <h6 class="text-danger">[{{translate('form.special_user_demo_file_note')}}]</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-divider mv40" id="spy2">
            </div>

            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', isset($details->id) ? $details->id : '', ['id' => 'hidpos']) !!}
                    {!! Html::link(url('/position/list'), translate('form.cancel'), ['class' => 'btn btn-hover btn-system', 'tabindex' => '11']) !!}
                    {!! Form::submit(translate('form.save_button'), ['class' => 'btn text-default btn-system', 'id' => 'save-btn', 'tabindex' => '10']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('includes.scripts.position_form_script')
@stop