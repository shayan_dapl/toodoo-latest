@extends('layouts.default')
@section('content')
<div class="row" id="position-list">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    <a href="{{url('/user/structure')}}" class="btn btn-system text-default">
                        <i class="fa fa-sitemap"></i>
                    </a>
                    @if(Auth::guard('customer')->user()->type != 3)
                        <a class="btn btn-system text-default" href ="{{url('/position/form')}}">
                            {{ucfirst(translate('words.add'))}}
                        </a>
                    @endif
                </div>
            </div>
            <div class="panel-body pn">
                <div class="progress mt10 mb10 ml50 mr50" id="loader">
                    <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        <span class="sr-only"></span>
                    </div>
                </div>
                <div class="jqxtree hide">
                    @if($positions == '')
                        <h2 class="text-center">{{translate('table.no_data')}}</h2>
                    @else
                        [ <i class="fa fa-user text-danger mb5"></i> = {{translate('words.special_position')}} ]
                        [ <i class="fa fa-square text-info light mb5"></i> = {{translate('form.assistant')}} ]
                        <ul class="mt10">
                            {!! $positions !!}
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.position_list_script')
@stop