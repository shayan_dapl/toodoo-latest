@extends('layouts.default')
	@section('content')
	<div class="panel">
		<div class="panel-heading">
			{{translate('words.user_preview')}}
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-bordered table-stripped">
						<tr>
							<th>{{translate('form.email')}}</th>
							<th>{{translate('form.first_name')}}</th>
							<th>{{translate('form.last_name')}}</th>
							<th>{{translate('form.language')}}</th>
							<th>{{translate('words.status')}}</th>
						</tr>
						@if (count($userPreview['users']) > 0)
							@foreach ($userPreview['users'] as $each)
								<tr>
									<td>{{$each['email']}}</td>
									<td>{{$each['fname']}}</td>
									<td>{{$each['lname']}}</td>
									<td>{{$each['language']}}</td>
									<td>
										@if ($each['remark'] == null)
											<span class="text-success">{{translate('words.ok')}}</span>
										@else
											<span class="text-danger">{{$each['remark']}}</span>
										@endif
									</td>
								</tr>
							@endforeach
						@endif
					</table>
					{!! Form::open(['method' => 'post', 'url' => '/position/special/save']) !!}
						{!! Form::hidden('data', json_encode($userPreview)) !!}
						{!! csrf_field() !!}
						{!! Form::submit(translate('form.save_button'), ['class' => 'btn text-default btn-system pull-right mt10', 'id' => 'save-btn']) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$('form').on('submit', function () {
            $('#save-btn').prop('disabled', true);
        });
	</script>
@stop