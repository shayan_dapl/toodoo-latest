@extends('layouts.default')
@section('content')
<div class="row" id="rating">
	<div class="col-md-7">
		<div class="panel panel-visible" id="spy2">
			<div class="panel-heading">
				<div class="panel-title hidden-xs">
					{{translate('words.list')}}
				</div>
			</div>
			<div class="panel-body">
				<list-table :list="types" />
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="panel panel-visible" id="spy2">
			<div class="panel-heading">
				<div class="panel-title hidden-xs">
					{{translate('words.add')}} / {{translate('words.edit')}}
				</div>
			</div>
			<div class="panel-body bg-light">
				<div class="admin-form">
					<div class="row">
						<div class="form-group">
							{!! Form::label('name', translate('form.type'), ['class' => 'col-lg-3 pt10 control-label required']) !!}
							<div class="col-md-9">
								<div class="section">
									{!! Form::text('name', '', ['v-model' => 'name', ':class' => 'nameClass', 'tabindex' => '1']) !!}
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							<input type="hidden" v-model="hid">
							{!! Form::button(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'tabindex' => '3', '@click' => 'saveData', ':disabled' => 'disable']) !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('includes.scripts.company.rating_action_type_script')
@stop