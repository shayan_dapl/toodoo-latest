@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    <button type="button" class="btn btn-hover btn-system" onclick="location.href ='{{url('/list')}}'">{{ucfirst(translate('form.back'))}}</button>
                </div>
            </div>
             {!! Form::open(['method' => 'post', 'url' => '/send-price-increase-request', 'onsubmit' => "return validate_form();"]) !!}
            <div class="p15 table-responsive">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>
                              <input name="mobileos" type="checkbox" id="selectall">
                              <span class="checkbox"></span>
                            </th>
                            <th>{{translate('table.company')}}</th>
                            <th>{{translate('table.current_package_amount')}}</th>
                            <th>{{translate('table.current_user_amount')}}</th>
                            <th>{{translate('table.current_storage_amount')}}</th>
                            <th>{{translate('table.last_request_sent')}}</th>
                            <th width="10%">{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('status', 'App\Services\CompanyList')
                        @foreach($subscribedCompany as $eachCompany)
                            <tr>
                                <td>
                                <label class="option block mn">
                                    <input type="checkbox" name="cmpid[]" class="case" value="{{$eachCompany->id}}" />
                                </label>
                                </td>
                                <td>{{$eachCompany->name}}</td>
                                <td>{{$eachCompany->package_price}}</td>
                                <td>{{$eachCompany->user_package_price}}</td>
                                <td>{{$eachCompany->storage_package_price}}</td>
                                <td>{{!empty ($eachCompany->priceincreaseproposal()->first()) ? date(Config::get('settings.dashed_date'), strtotime($eachCompany->priceincreaseproposal()->first()->request_date)) : translate('words.na')}}</td>
                                <td>
                                    <a href="{{ url('/view-detail/'.$eachCompany->id) }}" class="btn btn-default">
                                        <span class="text-success fa fa-pencil"></span> {{translate('words.view')}}
                                    </a>
                                </td>
                            </tr>	
                        @endforeach
                    </tbody>
                </table>
            </div>
            <a href="javascript:void(0);" class="btn btn-success dark text-default mb5" id="check-selected" title="{{translate('words.define_percentage')}}">
            {{translate('words.define_percentage')}}
            </a>
            <div id="propose-percent" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content turtle-graph-modal-content">
                        <div class="modal-header text-default back4d">
                            {{translate('words.define_proposed_percentage')}}
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title turtle-title"></h4>
                        </div>
                        <div class="modal-body turtle-body row justified w-wrap-extended">
                            <div class="admin-form p20 mt30">
                                <div class="row">
                                    {!! Form::label('item', translate('words.choose_item_to_increase'), ['class' => 'col-md-5 text-right pt10 control-label required']) !!}
                                    <div class="col-md-5">
                                        <div class="section">
                                            <div class="checkbox-custom checkbox-info mb10">
                                                <input type="checkbox" name="item[]" class="item" id="package" value="package">
                                                <label for="package">{{translate('words.package')}}</label>
                                            </div>
                                            <div class="checkbox-custom checkbox-info mb10">
                                                <input type="checkbox" name="item[]" class="item" id="user" value="user">
                                                <label for="user">{{translate('words.user')}}</label>
                                            </div>
                                            <div class="checkbox-custom checkbox-info mb10">
                                                <input type="checkbox" name="item[]" class="item" id="storage" value="storage">
                                                <label for="storage">{{translate('words.storage')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    {!! Form::label('name', translate('words.define_proposed_percentage'), ['class' => 'col-md-5 text-right pt10 control-label required']) !!}
                                    <div class="col-md-5">
                                        <div class="section">
                                            {!! Form::text('increase_percent', '', ['class' => 'gui-input', 'id' => 'increase_percent', 'required' => 'required', 'tabindex' => '1']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-4 text-right">
                                        <input type ="submit" name="Submit" class="btn btn-system" value="{{translate('words.send_proposal')}}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! csrf_field() !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
    $('#check-selected').click(function(){
        var selectedcmp = $(".case:checked").length;
        if(selectedcmp > 0){
            $('#propose-percent').modal('show');
        } else {
             new PNotify({
                            title: "",
                            text: "{{translate('alert.select_company')}}",
                            addclass: 'stack_top_right',
                            type: "error",
                            width: '290px',
                            delay: 2000
                        })
        }
    });
    // add multiple select / deselect functionality
    $("#selectall").click(function () {
          $('.case').attr('checked', this.checked);
    });
    // if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".case").click(function(){

        if($(".case").length == $(".case:checked").length) {
            $("#selectall").attr("checked", "checked");
        } else {
            $("#selectall").removeAttr("checked");
        }

    });
    $("input[id*='increase_percent']").keydown(function (event) {
            if (event.shiftKey == true) {
                new PNotify({
                        title: "",
                        text: "{{translate('alert.please_select_number_or_decimal')}}",
                        addclass: 'stack_top_right',
                        type: "error",
                        width: '290px',
                        delay: 2000
                    })
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

            } else {
                new PNotify({
                        title: "",
                        text: "{{translate('alert.please_select_number_or_decimal')}}",
                        addclass: 'stack_top_right',
                        type: "error",
                        width: '290px',
                        delay: 2000
                    })
                event.preventDefault();
            }
            
            if($(this).val().indexOf('.') !== -1 && event.keyCode == 190){
                new PNotify({
                        title: "",
                        text: "{{translate('alert.please_select_number_or_decimal')}}",
                        addclass: 'stack_top_right',
                        type: "error",
                        width: '290px',
                        delay: 2000
                    })
                event.preventDefault();
            }

        });
});
function validate_form()
{
    valid = true;
    if($(".item:checked").length == 0)
    {
        new PNotify({
                        title: "",
                        text: "{{translate('alert.please_select_one_or_more_item')}}",
                        addclass: 'stack_top_right',
                        type: "error",
                        width: '290px',
                        delay: 2000
                    })
        valid = false;
    }

    return valid;
}
</script>
@stop