<div class="row" id="risk-action-event-log">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-body event-log">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>{{translate('form.action')}}</th>
                                <th>{{translate('table.delegate_to')}}</th>
                                <th>{{translate('form.deadline')}}</th>
                                <th width="40%">&nbsp;</th>
                                <th>{{translate('form.created_at')}}</th>
                                <th>{{translate('form.closed')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @inject('status', 'App\Services\ProcessRiskActionLog')
                            @inject('file', 'App\Services\ProcessRiskActionLog')
                            @foreach($data as $eachData)
                                <tr>
                                    <td>{{$eachData->action}}</td>
                                    <td>{{$eachData->deligate_to}}</td>
                                    @if(!empty($eachData->deadline))
                                    <td>{{date('d-m-Y',strtotime($eachData->deadline))}}</td>
                                    @else
                                    <td></td>
                                    @endif
                                    <td class="p1 segment">
                                    @if(!empty($eachData->comments))
                                        <ul class="h-a">
                                        @foreach($eachData->comments as $comment)
                                            <li class="process-cls">
                                                <b class="fs12">{{translate('form.comment')}}</b> : {{$comment->reply}}
                                                <br clear="all">
                                                <b class="fs12">{{translate('form.comment_on')}}</b> : {{date(Config::get('settings.dashed_date'), strtotime($comment->created_at))}}
                                                <br clear="all">
                                                <b class="fs12">{{translate('form.documents')}}</b> : 
                                                @if(!empty($comment->files()))
                                                    @foreach($comment->files() as $eachFile)
                                                        <a href="{{$file->file($eachFile->document_name, 'risk_opp_action_docs')['url']}}" target="_blank">
                                                            <i class="fa {{$file->file($eachFile->document_name, 'risk_opp_action_docs')['icon']}} text-system dark"></i>
                                                        </a>
                                                    @endforeach
                                                @else
                                                    --
                                                @endif
                                            </li>
                                        @endforeach
                                        </ul> 
                                        @endif
                                    </td>
                                    <td>{{date('d-m-Y',strtotime($eachData->created_at))}}</td>
                                    <td>
                                        @if($eachData->closed == 1) Yes @else -- @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>