@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/context/context-analysis-form', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}

            <div class="section-divider mb20 mt30" id="spy1">
                <span>{{translate('form.context_analysis')}}</span>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('describe_issue', translate('form.describe_issue'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('issue', (isset($details->issue)) ? $details->issue : old('issue'), array('class' => 'gui-input', 'id' => 'issue', 'placeholder' => translate('form.issue'), 'required' => 'required', 'tabindex' => '1')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('issue')}}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section branch">
                        {!! Form::label('branch', translate('form.branch'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {!! Form::select('branches[]',$branches, !empty($selectedBranches) ? $selectedBranches : '' , array('class'=>'select2-single select-branch','id' => 'branch','required' => 'required','multiple' => 'multiple')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('branches[]')}}</p>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('subject', translate('form.subject'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            <label class="field select">
                                {!! Form::select('subject', $subjects, isset($details->subject) ? $details->subject : old('subject'), ['id' => 'subject', 'tabindex' => '2', 'required' => 'required', '']) !!}
                                <i class="arrow"></i>
                            </label>
                        </label>		
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('issue_type', translate('form.internal_or_external'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            <label class="field select">
                                {!! Form::select('issue_type', array(''=>'Select','internal'=>translate('words.internal'),'external'=>translate('words.external')),isset($details->issue_type) ? $details->issue_type : old('issue_type'), ['id' => 'issue_type', 'tabindex' => '3','required' => 'required', '']) !!}
                                <i class="arrow"></i>
                            </label>
                        </label>		
                    </div>
                </div> 
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section processlink">
                        {!! Form::label('processlink', translate('form.processlink'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            <label class="field select">
                                {!! Form::select('process_id', $process,isset($details->process_id) ? $details->process_id : old('process_id'), ['class'=>'select2-single select-process error','id' => 'processlink', 'tabindex' => '4','required' => 'required', '']) !!}
                            </label>
                        </label>		
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('owner', translate('form.owner'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            <label class="field select">
                                {!! Form::select('owner', $users, isset($details->owner) ? $details->owner : old('owner'), ['id' => 'owner', 'tabindex' => '5','required' => 'required', '']) !!}
                                <i class="arrow"></i>
                            </label>
                        </label>		
                    </div>
                </div> 
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('monitoring_info', translate('form.monitoring_info'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {{ Form::textarea('monitoring_info',isset($details->monitoring_info) ? $details->monitoring_info : old('monitoring_info'), ['class' => 'gui-input', 'placeholder' => translate('form.monitoring_info'), 'tabindex' => '6' ,'required' => 'required']) }}

                            <label for="fname " class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('monitoring_info')}}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('possible_influence', translate('form.possible_influence'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {{ Form::textarea('possible_influence', isset($details->possible_influence) ? $details->possible_influence : old('possible_influence'), ['class' => 'gui-input', 'placeholder' => translate('form.possible_influence'), 'tabindex' => '7','required' => 'required']) }}
                            <label for="fname " class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('possible_influence')}}</p>
                        @endif
                    </div>
                </div> 
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('risk', translate('form.risk'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            <label class="field select">
                                {!! Form::select('risk', array(''=>'Select','risk'=>translate('words.risk'),'opportunity'=>translate('words.opportunity')), isset($details->risk_or_opportunity) ? $details->risk_or_opportunity : old('risk'), ['id' => 'risk', 'tabindex' => '9','required' => 'required', '']) !!}
                                <i class="arrow"></i>
                            </label>
                        </label>        
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('impact', translate('form.impact'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            <label class="field select">
                                {!! Form::select('impact', array(''=>'Select','low'=>translate('form.low'),'medium'=>translate('form.medium'),'high'=>translate('form.high')), isset($details->impact) ? $details->impact : old('impact'), ['id' => 'impact', 'tabindex' => '8','required' => 'required','']) !!}
                                <i class="arrow"></i>
                            </label>
                        </label>		
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('description', translate('form.description'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {{ Form::textarea('description', isset($details->description) ? $details->description : old('description'), ['class' => 'gui-input', 'placeholder' => translate('form.description'), 'tabindex' => '10','required' => 'required']) }}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('fname')}}</p>
                        @endif
                    </div>
                </div>
            </div>
           
            <div class="row" id="context-analysis-form">
                <div class="col-md-8 col-md-offset-2">
                    {!! Form::label('actionyesno', translate('form.action'), ['class' => 'field-label fs15 mb5']) !!}
                    <div class="section">
                        <div class="radio-custom radio-system pt10">
                            <input type="radio" id="is-action-yes" name="action_yes_no" value="Yes">
                            <label for="is-action-yes">{{translate('form.yes')}}</label>

                            <input type="radio" id="is-action-no" name="action_yes_no" value="No" checked="">
                            <label for="is-action-no">{{translate('form.no')}}</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row actionDiv hide">
                <div class="col-md-8 col-md-offset-2">
                    {!! Form::label('action', translate('table.action'), ['class' => 'field-label fs15 mb5 required']) !!}
                    <div class="section">
                        {!! Form::text('action', '', ['class' => 'form-control action-field', 'id' => 'action', 'placeholder' => translate('form.define_action'), 'tabindex' => '5']) !!}
                    </div>
                </div>
            </div>

            <div class="row actionDiv hide">
                <div class="col-md-8 col-md-offset-2">
                    {!! Form::label('actiondelegateto', translate('table.delegate_to'), ['class' => 'field-label fs15 mb5 required']) !!}
                    <div class="section">
                        <label class="field select">
                            {!! Form::select('who', $users, '', ['class' => 'action-field select2-single', 'tabindex' => '5']) !!}
                        </label>
                    </div>
                </div>
            </div>

            <div class="row actionDiv hide">
                <div class="col-md-8 col-md-offset-2">
                    {!! Form::label('actiondelegateto', translate('form.deadline'), ['class' => 'field-label fs15 mb5 required']) !!}
                    <div class="section">
                        {!! Form::text('deadline', '', ['class' => 'form-control action-field', 'id' => 'datetimepicker2', 'placeholder' => translate('form.define_deadline'), 'tabindex' => '6']) !!}
                    </div>
                </div>
            </div>
             @if(!empty($contextActionLog) && ($contextActionLog->count() > 0 ))      
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                <div class="section-divider" id="spy2">
                    <span>{{translate('words.old_actions')}}</span>
                </div>
                <table class="table table-striped table-hover table-bordered mb20" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('form.define_action')}}</th>
                            <th>{{translate('table.delegate_to')}}</th>
                            <th>{{translate('form.deadline')}}</th>
                            <th>{{translate('table.status')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($contextActionLog as $eachLog)
                        <tr>
                            <td>{{$eachLog->action}}</td>
                            <td>{{$eachLog->deligate_to}}</td>
                            <td>{{$eachLog->deadline}}</td>
                            <td>{{($eachLog->closed == 0) ? translate('table.pending') : translate('table.closed')}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    <button type="button" class="btn btn-hover btn-system" onclick="location.href ='{{url('/context/context-analysis-form')}}'">{{translate('form.cancel')}}</button>
                    <button type="submit" class="btn btn-hover btn-system" tabindex="11" id="save-btn">{{translate('form.save_button')}}</button>
                    <br clear="all">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <input type="hidden" id="hid" name="hid" value="{{ $details->id or '' }}">
                    <input type="hidden" id="cmpid" name="compid" value="{{ $company->id or '' }}">
                    <input type="hidden" name="detailid" value="{{ $details->id or '' }}">
                </div>
            </div>
            {!! Form::close() !!}

            <div class="section-divider" id="spy2">
                <span>{{translate('form.context_analysis_list')}}</span>
            </div>

            <div class="panel-body pn">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.issue')}}</th>
                            <th>{{translate('table.issue_type')}}</th>
                            <th>{{translate('table.subject')}}</th>
                            <th>{{translate('table.owner')}}</th>
                            <th>{{translate('table.process')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($allContexts as $k=>$v)
                        <tr>
                            <td>
                                {{$v['issue']}}
                            </td>
                            <td>
                                {{$v['issue_type']}}
                            </td>
                            <td>
                                {{$v['subject']['subject']}}
                            </td>
                            <td>
                                {{$v['owner']['name']}}
                            </td>
                            <td>
                                {{$v['process']['name']}}
                            </td>
                            <td>
                                <a href="{{ url('/context/context-analysis-edit/'.$v['id']) }}" class="btn btn-default">
                                    <span class="text-success fa fa-pencil"></span>
                                </a>
                                <a href="{{ url('/context/context-analysis-delete/'.$v['id']) }}" onclick="return confirm('{{translate('alert.are_you_sure_all_related_actions_will_also_be_deleted')}}')" class="btn btn-default">
                                    <span class="text-danger fa fa-trash"></span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.context_analysis_script')
@stop