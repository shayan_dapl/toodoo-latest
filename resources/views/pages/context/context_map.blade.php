@extends('layouts.default')
@section('content')
@inject('color', 'App\Services\ContextMap')
<div class="panel">
    <div class="panel-heading exhight">
    </div>
    <div class="form-group mt25 disp-flex">
        {!! Form::select('branch', $branches, isset($selectedBranch) ? $selectedBranch : 'all', ['class' => 'form-control w200 ml5 issuecategory', 'id' => 'branch-opt']) !!}
    </div>
    <div id="loaderDv" style="display:none;"><i class="fa fa-cog fa-spin text-default fa-5x"></i></div>
    <div class="panel-body" id="contextMap">
        <div class="row blueBoxline">
            <div class="col-md-6">
                <div class="panel pr5 pl5">
                    <div class="panel-heading bg-system">
                        {{translate('words.internal_opportunity')}} (S)
                    </div>
                    <div class="panel-body panel-body-ext turtleBox p5">
                        <ul class="h-a">
                            @if(!empty($internal_opportunity))
                                @foreach($internal_opportunity as $eachData)
                                    <li class="process-cls text-default bw" style="border-color:{{$color->color($eachData['impact'])}} !important;" data-html="true" data-toggle="tooltip" data-placement="top" title="<b>{{translate('form.possible_influence')}} : </b>{{$eachData['possible_influence']}}<br/><b>{{translate('form.impact')}} : </b>{{$eachData['impact']}}<br/><b>{{translate('form.description')}} : </b>{{$eachData['description']}}" >
                                    @if(empty($eachData['logs']) || empty($eachData['openlogs']))
                                    	<i class="fa fa-smile-o"></i>
                                    @elseif(!empty($eachData['overduelogs']))
                                    	<i class="fa fa-frown-o"></i>
                                    @elseif(!empty($eachData['openlogs']) && empty($eachData['overduelogs']))
                                        <i class="fa fa-meh-o"></i>
                                    @endif
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#action-log-details" class="action-log" data-contextid="{{$eachData['id']}}" data-type="context">{{$eachData['issue']}}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel pr5 pl5">
                    <div class="panel-heading bg-system">
                        {{translate('words.internal_risk')}} (W)
                    </div>
                    <div class="panel-body panel-body-ext turtleBox p5">
                        <ul class="h-a">
                            @if(!empty($internal_risk))
                                @foreach($internal_risk as $eachData)
                                    <li class="process-cls text-default bw" style="border-color:{{$color->color($eachData['impact'])}} !important;" data-html="true" data-toggle="tooltip" data-placement="top" title="<b>{{translate('form.possible_influence')}} : </b>{{$eachData['possible_influence']}}<br/><b>{{translate('form.impact')}} : </b>{{$eachData['impact']}}<br/><b>{{translate('form.description')}} : </b>{{$eachData['description']}}">
                                    @if(empty($eachData['logs']) || empty($eachData['openlogs']))
                                        <i class="fa fa-smile-o"></i>
                                    @elseif(!empty($eachData['overduelogs']))
                                        <i class="fa fa-frown-o"></i>
                                    @elseif(!empty($eachData['openlogs']) && empty($eachData['overduelogs']))
                                        <i class="fa fa-meh-o"></i>
                                    @endif
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#action-log-details" class="action-log" data-contextid="{{$eachData['id']}}" data-type="context">{{$eachData['issue']}}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>   
        </div>
        <div class="row blueBoxline noborderbottom">
            <div class="col-md-6">
                <div class="panel pr5 pl5">
                    <div class="panel-heading bg-system">
                        {{translate('words.external_opportunity')}} (O)
                    </div>
                    <div class="panel-body panel-body-ext turtleBox p5">
                        <ul class="h-a">
                            @if(!empty($external_opportunity))
                                @foreach($external_opportunity as $eachData)
                                    <li class="process-cls text-default bw" style="border-color:{{$color->color($eachData['impact'])}} !important;" data-html="true" data-toggle="tooltip" data-placement="top" title="<b>{{translate('form.possible_influence')}} : </b>{{$eachData['possible_influence']}}<br/><b>{{translate('form.impact')}} : </b>{{$eachData['impact']}}<br/><b>{{translate('form.description')}} : </b>{{$eachData['description']}}">
                                    @if(empty($eachData['logs']) || empty($eachData['openlogs']))
                                        <i class="fa fa-smile-o"></i>
                                    @elseif(!empty($eachData['overduelogs']))
                                        <i class="fa fa-frown-o"></i>
                                    @elseif(!empty($eachData['openlogs']) && empty($eachData['overduelogs']))
                                        <i class="fa fa-meh-o"></i>
                                    @endif
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#action-log-details" class="action-log" data-contextid="{{$eachData['id']}}" data-type="context">{{$eachData['issue']}}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif 
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel pr5 pl5">
                    <div class="panel-heading bg-system">
                        {{translate('words.external_risk')}} (T)
                    </div>
                    <div class="panel-body panel-body-ext turtleBox p5">
                        <ul class="h-a">
                            @if(!empty($external_risk))
                                @foreach($external_risk as $eachData)
                                    <li class="process-cls text-default bw" style="border-color:{{$color->color($eachData['impact'])}} !important;" data-html="true" data-toggle="tooltip" data-placement="top" title="<b>{{translate('form.possible_influence')}} : </b>{{$eachData['possible_influence']}}<br/><b>{{translate('form.impact')}} : </b>{{$eachData['impact']}}<br/><b>{{translate('form.description')}} : </b>{{$eachData['description']}}">
                                    @if(empty($eachData['logs']) || empty($eachData['openlogs']))
                                    	<i class="fa fa-smile-o"></i>
                                    @elseif(!empty($eachData['overduelogs']))
                                    	<i class="fa fa-frown-o"></i>
                                    @elseif(!empty($eachData['openlogs']) && empty($eachData['overduelogs']))
                                        <i class="fa fa-meh-o"></i>
                                    @endif
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#action-log-details" class="action-log" data-contextid="{{$eachData['id']}}" data-type="context">{{$eachData['issue']}}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>	
        </div>
        <div id="action-log-details" class="modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-extended">
                <div class="modal-content">
                    <div class="modal-header text-default back4d">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{{translate('words.event_logging')}}</h4>
                    </div>
                    <div class="modal-body action-log-body row justified w-wrap-extended">
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$(document).on('click', '.action-log', function() {
        var contextid = $(this).data('contextid');
        var type = $(this).data('type');
        $(".action-log-body").html('<center><i class="fa fa-cog fa-spin fa-3x fa-fw text-system"></i></center>');
        $.ajax({
            url: "{{url('/context/context-action-log')}}/" + contextid + '/' + type,
            type: "GET",
            success: function(data) {
                $(".action-log-body").html(data);
            }
        });
    });

    $(document).on('change', '#branch-opt', function() {
        $('#loaderDv').show();
        var branchId = $(this).val();
        if (branchId != "") {
            $.ajax({
                url: "{{url('/context/filter-map')}}/" + branchId,
                type: 'GET',
                success: function (data) {
                    $('#contextMap').html(data);
                    $('[data-toggle="tooltip"]').tooltip();
                    setTimeout(function(){ $('#loaderDv').hide(); }, 500);
                }
            });
        }
    });
});
</script>
@stop