@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/context/context-subject-form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/context/context-subject-list'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.context_subject')}}</span>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('subject', translate('form.subject'), array('class' => 'col-lg-5 col-md-5 col-sm-5 control-label required')) !!}
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="section">
                            {!! Form::text('subject',(isset($context_subject->subject)) ? $context_subject->subject : old('subject'), array('class' => 'gui-input', 'id' => 'subject', 'placeholder' => translate('form.text'),'required'=>'required','tabindex' => '3')) !!}
                        </div>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('subject', translate('error.required_field'))}}</p>
                        @endif
                    </div>
                </div>
            </div>		
            <div class="section-divider mv40" id="spy2">
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    <input type="hidden" id="hid" name="hid" value="{{ $context_subject->id or '' }}">
                    {!! Html::link(url('/context/context-subject-list'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn')) !!}
                    <br clear="all">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
