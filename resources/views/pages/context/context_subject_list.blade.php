@extends('layouts.default')
@section('content')
<div class="row" id="context">
    <div class="col-md-7">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs">
                    {{translate('words.list')}}
                </div>
            </div>
            <div class="panel-body">
                <list-table :list="list" />
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel">
            <div class="panel-heading exhight">{{translate('words.add')}} / {{translate('words.edit')}}</div>
            <div class="panel-body">
                <div class="admin-form">
                    <div class="row mb10">
                        <div class="col-md-12">
                            {!! Form::label('name', translate('form.context_subject'), ['class' => 'field-label fs15 mb5 required']) !!}
                            <label class="field append-icon">
                                <input type="text" v-model="name" :class="nameClass" tabindex="1" />
                            </label>
                            <p class="text-danger mt5">@{{nameError}}</p>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            {!! Form::button(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', '@click' => 'saveData', ':disabled' => 'enabled', 'tabindex' => '4' ]) !!}
                            <br clear="all">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.context.list_script')
@stop