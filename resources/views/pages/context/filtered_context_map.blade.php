@inject('color', 'App\Services\ContextMap')
        <div class="row blueBoxline">
            <div class="col-md-6">
                <div class="panel pr5 pl5">
                    <div class="panel-heading bg-system">
                        {{translate('words.internal_opportunity')}} (S)
                    </div>
                    <div class="panel-body panel-body-ext turtleBox p5">
                        <ul class="h-a">
                            @if(!empty($internalOpportunity))
                                @foreach($internalOpportunity as $eachData)
                                    <li class="process-cls text-default bw" style="border-color:{{$color->color($eachData['impact'])}} !important;" data-html="true" data-toggle="tooltip" data-placement="top" title="<b>{{translate('form.possible_influence')}} : </b>{{$eachData['possible_influence']}}<br/><b>{{translate('form.impact')}} : </b>{{$eachData['impact']}}<br/><b>{{translate('form.description')}} : </b>{{$eachData['description']}}" >
                                    @if(empty($eachData['logs']) || empty($eachData['openlogs']))
                                    	<i class="fa fa-smile-o"></i>
                                    @elseif(!empty($eachData['overduelogs']))
                                    	<i class="fa fa-frown-o"></i>
                                    @elseif(!empty($eachData['openlogs']) && empty($eachData['overduelogs']))
                                        <i class="fa fa-meh-o"></i>
                                    @endif
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#action-log-details" class="action-log" data-contextid="{{$eachData['id']}}" data-type="context">{{$eachData['issue']}}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel pr5 pl5">
                    <div class="panel-heading bg-system">
                        {{translate('words.internal_risk')}} (W)
                    </div>
                    <div class="panel-body panel-body-ext turtleBox p5">
                        <ul class="h-a">
                            @if(!empty($internalRisk))
                                @foreach($internalRisk as $eachData)
                                    <li class="process-cls text-default bw" style="border-color:{{$color->color($eachData['impact'])}} !important;" data-html="true" data-toggle="tooltip" data-placement="top" title="<b>{{translate('form.possible_influence')}} : </b>{{$eachData['possible_influence']}}<br/><b>{{translate('form.impact')}} : </b>{{$eachData['impact']}}<br/><b>{{translate('form.description')}} : </b>{{$eachData['description']}}">
                                    @if(empty($eachData['logs']) || empty($eachData['openlogs']))
                                        <i class="fa fa-smile-o"></i>
                                    @elseif(!empty($eachData['overduelogs']))
                                        <i class="fa fa-frown-o"></i>
                                    @elseif(!empty($eachData['openlogs']) && empty($eachData['overduelogs']))
                                        <i class="fa fa-meh-o"></i>
                                    @endif
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#action-log-details" class="action-log" data-contextid="{{$eachData['id']}}" data-type="context">{{$eachData['issue']}}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row blueBoxline noborderbottom">
            
            <div class="col-md-6">
                <div class="panel pr5 pl5">
                    <div class="panel-heading bg-system">
                        {{translate('words.external_opportunity')}} (O)
                    </div>
                    <div class="panel-body panel-body-ext turtleBox p5">
                        <ul class="h-a">
                            @if(!empty($externalOpportunity))
                                @foreach($externalOpportunity as $eachData)
                                    <li class="process-cls text-default bw" style="border-color:{{$color->color($eachData['impact'])}} !important;" data-html="true" data-toggle="tooltip" data-placement="top" title="<b>{{translate('form.possible_influence')}} : </b>{{$eachData['possible_influence']}}<br/><b>{{translate('form.impact')}} : </b>{{$eachData['impact']}}<br/><b>{{translate('form.description')}} : </b>{{$eachData['description']}}">
                                    @if(empty($eachData['logs']) || empty($eachData['openlogs']))
                                        <i class="fa fa-smile-o"></i>
                                    @elseif(!empty($eachData['overduelogs']))
                                        <i class="fa fa-frown-o"></i>
                                    @elseif(!empty($eachData['openlogs']) && empty($eachData['overduelogs']))
                                        <i class="fa fa-meh-o"></i>
                                    @endif
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#action-log-details" class="action-log" data-contextid="{{$eachData['id']}}" data-type="context">{{$eachData['issue']}}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif 
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel pr5 pl5">
                    <div class="panel-heading bg-system">
                        {{translate('words.external_risk')}} (T)
                    </div>
                    <div class="panel-body panel-body-ext turtleBox p5">
                        <ul class="h-a">
                            @if(!empty($externalRisk))
                                @foreach($externalRisk as $eachData)
                                    <li class="process-cls text-default bw" style="border-color:{{$color->color($eachData['impact'])}} !important;" data-html="true" data-toggle="tooltip" data-placement="top" title="<b>{{translate('form.possible_influence')}} : </b>{{$eachData['possible_influence']}}<br/><b>{{translate('form.impact')}} : </b>{{$eachData['impact']}}<br/><b>{{translate('form.description')}} : </b>{{$eachData['description']}}">
                                    @if(empty($eachData['logs']) || empty($eachData['openlogs']))
                                    	<i class="fa fa-smile-o"></i>
                                    @elseif(!empty($eachData['overduelogs']))
                                    	<i class="fa fa-frown-o"></i>
                                    @elseif(!empty($eachData['openlogs']) && empty($eachData['overduelogs']))
                                        <i class="fa fa-meh-o"></i>
                                    @endif
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#action-log-details" class="action-log" data-contextid="{{$eachData['id']}}" data-type="context">{{$eachData['issue']}}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>	
        </div>
        <div id="action-log-details" class="modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-extended">
                <div class="modal-content">
                    <div class="modal-header text-default back4d">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{{translate('words.event_logging')}}</h4>
                    </div>
                    <div class="modal-body action-log-body row justified w-wrap-extended">
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>