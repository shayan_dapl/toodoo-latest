<div class="row">
    <div class="col-md-12 text-left">
        {{translate('started.branch_heading')}}
    </div>
</div>

<div id="getstarted-branch" class="mt50 branchPops" v-cloak>
	<div class="row mb5" v-for="(each, i) in branches">
		<div class="col-md-4 col-md-offset-2 fs16 text-left">
			@{{i+1}} : <span v-show="!each.edit">@{{each.name}}</span>
			<input type="text" v-model="each.name" v-show="each.edit" v-bind:id="each.id" :ref="each.id" class="form-control-extended">
		</div>
		<div class="col-md-4 text-left">
			<a href="#" v-show="!each.edit" v-on:click.prevent="edit(each, $event)" class="btn btn-success dark">
				{{translate('started.edit')}}
			</a>
			<a href="#" v-show="!each.edit && each.is_corporate == 0" v-on:click="remove(each.id)" class="ml5 btn btn-danger dark">
				{{translate('started.delete')}}
			</a>
			<button class="btn btn-sm btn-system" v-show="each.edit" v-on:click="save(each)">
				{{translate('started.save')}}
			</button>
		</div>
	</div>

	<div class="row mt20">
		<div class="col-md-4 col-md-offset-2 text-center">
			<input type="text" ref="name" v-model="name" v-bind:class="nameClass" placeholder="{{translate('words.branch')}}" tabindex="1" />
			<input type="hidden" v-model="hid" value="" />
		</div>
		<div class="col-md-2">
			<button class="round-button" v-on:click="add" tabindex="2"><i class="fa fa-plus"></i></button>
		</div>
        <div class="col-md-4 pt10">
            <span class="text-success" v-cloak>@{{success}}</span>
            <span class="text-danger" v-cloak>@{{faliure}}</span>
        </div>
	</div>
	<div class="row">
        <div class="col-md-6 mt20">
            <button class="btn btn-sm btn-system" @click="home">
                {{translate('tutorial.back_button')}}
            </button>
        </div>
        <div class="col-md-6 text-right mt20">
			<next v-show="!next" :name="name" :hid="hid" />
		</div>
	</div>
</div>
@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    Vue.component('next', {
        template : '<button class="btn btn-system text-default w100 mb5">{{translate('started.next')}}</button>',
        mounted : function () {
            $(this.$el).click(function () {
                @if($department > 0)
                    var url = "{{url('/getstarted/department/copy')}}";
                @else
                    var url = "{{url('/getstarted/department')}}";
                @endif

                if (this.name != "" && this.name.match(/^\s+/) == null) {
                    this.nameClass = 'form-control';
                    axios.post("{{url('/branch/')}}", {
                        'name' : this.name,
                        'hid' : this.hid,
                        'is_corporate' : 0,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(() => {
                        $.ajax({
                            url: url,
                            type: "GET",
                            success: function (data) {
                                $("#get-started-popup-body").html(data);
                            }
                        });
                    })
                    .catch(console.log);
                } else {
                    $.ajax({
                        url: url,
                        type: "GET",
                        success: function (data) {
                            $("#get-started-popup-body").html(data);
                        }
                    });
                }
            });
        }
    });

    var vuem = new Vue({
    	el : '#getstarted-branch',
    	data : {
    		branches : [],
            name : '',
            nameClass : 'form-control',
            hid : '',
            next : false,
            success : '',
            faliure : ''
    	},
        created () {
            this.fetch();
        },
        watch : {
            name : function (val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.nameClass = 'form-control';
                }
            },
            success : function (val) {
                if (val != '') {
                    setTimeout(() => {
                        this.success = '';
                    }, 1000);
                }
            },
            faliure : function (val) {
                if (val != '') {
                    setTimeout(() => {
                        this.faliure = '';
                    }, 1000);
                }
            }
        },
    	methods : {
            fetch : function () {
                axios.get("{{url('/getstarted/branches')}}").then(response => {
                    this.branches = response.data;
                });
            },
            add : function () {
                if (this.name != "" && this.name.match(/^\s+/) == null) {
                    axios.post("{{url('/branch/')}}", {
                        'name' : this.name,
                        'hid' : this.hid,
                        'is_corporate' : 0,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        this.$refs.name.focus();
                        vuem.success = (response.data === true) ? "{{translate('words.success_message')}}" : '';
                        vuem.faliure = (response.data === false) ? "{{translate('alert.already_exist')}}" : '';
                    })
                    .then(() => {
                        this.name = this.hid = '';
                        this.fetch();
                    })
                    .catch(console.log);
                } else {
                    this.nameClass = 'form-control error';
                }
            },
    		edit : function (each, e) {
		      	each.edit = true;
                this.next = true;
		      	this.$nextTick(() => {
		        	this.$refs[each.id][0].focus();
		      	})
		    },
    		save : function (each) {
                if (each.name != "" && each.name.match(/^\s+/) == null) {
        			each.edit = false;
                    this.next = false;
                    axios.post("{{ url('/branch/') }}", { 
                        'name' : each.name,
                        'hid' : each.id
                    })
                    .then(response => {
                        vuem.success = (response.data === true) ? "{{translate('words.success_message')}}" : '';
                        vuem.faliure = (response.data === false) ? "{{translate('alert.already_exist')}}" : '';
                    })
                    .then(() => {
                        this.fetch();
                    })
                    .catch(console.log);
                }
    		},
    		remove: function (val) {
                var confirmed = confirm("{{translate('alert.are_you_sure')}}");
                if (confirmed == true) {
                    axios.post("{{ url('/branch/remove') }}", {
                        'id' : val,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        vuem.success = (response.data === true) ? "{{translate('words.success_message')}}" : '';
                        vuem.faliure = (response.data === false) ? "{{translate('words.remove')}} {{translate('alert.not_allowed')}}" : ((response.data === 'child') ? "{{translate('alert.child_posiiton_exists')}}" : "");
                    })
                    .then(() => {
                        this.fetch();
                    })
                    .catch(console.log); 
                }
            },
            home : function () {
                axios.get("{{url('/getstarted/started')}}")
                .then(response => {
                    $("#get-started-popup-body").html(response.data);
                })
                .catch(console.log);
            }
    	}
    });
</script>