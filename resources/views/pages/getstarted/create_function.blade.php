<span id="getstarted-create-function">
	<div class="row">
		<div class="col-md-3 text-right mt20 pt10">
			{{translate('started.create_function_in')}}
		</div>
		<div class="col-md-2 mt20">
			@if ($topPersonExists == 0)
                <p class="text-system fs16 mt10">{{$branches[$selected]}}</p>
            @else
				{!! Form::select('', $branches, $selected, ['class' => 'form-control', 'v-model' => 'branch', 'v-on:change' => 'changeBranch']) !!}
			@endif
		</div>
	</div>
	<div class="row mt50">
		<div class="col-md-2 col-md-offset-3 pt10 text-right">
			{{translate('started.function_name')}}
		</div>
		<div class="col-md-4">
			{!! Form::text('', '', ['v-bind:class' => 'nameClass', 'v-model' => 'name']) !!}
		</div>
		<div class="col-md-3 pt10 text-left">
			<p class="text-danger" v-cloak>@{{nameError}}</p>
		</div>
	</div>
	<div class="row mt10">
		<div class="col-md-2 col-md-offset-3 pt10 text-right">
			{{translate('words.department')}}
		</div>
		<div class="col-md-4">
			<select class="form-control" v-model="department" v-on:change="getParent">
				@foreach ($departments as $id => $name)
					<option value="{{$id}}" :disabled="{{array_keys($departments)[0]}} != {{$id}} && {{$topPersonExists}} == 0">{{$name}}</option>
				@endforeach
			</select>
		</div>
		<div class="col-md-3 text-system" v-show="{{array_keys($departments)[0]}} != {{$id}} && {{$topPersonExists}} == 0" v-cloak>
			{{translate('started.head_of_department')}}
		</div>
	</div>
	<div class="row mt10" v-if="parents != ''" v-cloak>
		<div class="col-md-2 col-md-offset-3 pt10 text-right">
			{{translate('started.parent_function')}}
		</div>
		<div class="col-md-4">
			<select v-model="parent" class="form-control">
				<option v-for="each in parents" v-bind:value="each.id">
					@{{each.name}}
				</option>
			</select>
		</div>
	</div>
	<div class="row mt10" v-if="parents != ''" v-cloak>
		<div class="col-md-2 col-md-offset-3 pt5 text-right">
			{{translate('form.assistant')}}
		</div>
		<div class="col-md-1">
			<div class="switch switch-system switch-started round switch-inline pull-left mt10">
				<input id="assistant" type="checkbox" v-model="assistant">
				<label for="assistant"></label>
			</div>
		</div>
		<div class="col-md-6 pt5">
			<p class="text-system" v-show="assistantError == ''">{{translate('started.what_assistant_is')}}</p>
			<p class="text-danger" v-cloak>@{{assistantError}}</p>
		</div>
	</div>
	<div class="row mt10" v-if="parents != ''" v-cloak>
		<div class="col-md-2 col-md-offset-3 pt5 text-right">
			{{translate('form.special_position')}}
		</div>
		<div class="col-md-1">
			<div class="switch switch-system switch-started round switch-inline pull-left mt10">
				<input id="specialpos" type="checkbox" v-model="special">
				<label for="specialpos"></label>
			</div>
		</div>
		<div class="col-md-6 text-system pt5">
			{{translate('started.what_groupfunction_is')}}
		</div>
	</div>
	<div class="row mt15">
		<div class="col-md-2 col-md-offset-3 pt10 text-right">
			{{translate('form.info')}}
		</div>
		<div class="col-md-4">
			{{ Form::textarea('info', '', ['class' => 'form-control', 'v-model' => 'info']) }}
		</div>
	</div>
	<div class="row mt15 @if(Auth::user()->company->positions()->count() == 0) hide @endif" v-cloak>
		<div class="col-md-2 col-md-offset-3 pt10 text-right">
			{{translate('form.assign_user')}}
		</div>
		<div class="col-md-4">
			<div class="switch switch-system switch-started round switch-inline pull-left mt10">
				<input id="assign-user" type="checkbox" v-model="assignUser">
				<label for="assign-user"></label>
			</div>
		</div>
	</div>
	<div class="row mt15" v-show="assignUser" v-cloak>
		<div class="col-md-2 col-md-offset-3 pt10 text-right">
			{{translate('form.user_with_position')}}
		</div>
		<div class="col-md-4">
			{!! Form::select('', $users, '', ['class' => 'form-control', 'v-model' => 'user']) !!}
		</div>
		<div class="col-md-3 pt10">
			<a href="javascript:void(0)" class="text-system" v-on:click="saveData('user')">
				{{translate('form.not_in_list')}}
			</a>
		</div>
	</div>
	<div class="row mt20">
		<div class="col-md-6">
			<button class="btn btn-sm btn-system" @click="home">{{translate('tutorial.back_button')}}</button>
		</div>
		<div class="col-md-6 text-right">
			<button class="btn btn-sm btn-system" :disabled="!enabled" v-on:click="saveData('save')">{{translate('started.add')}}</button>
		</div>
	</div>
</span>
@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
	new Vue({
		el : '#getstarted-create-function',
		data : {
			branch : "{{$selected}}",
			isCorporate : '{{$isCorporate}}',
			name : '',
			department : '{{array_keys($departments)[0]}}',
			topDept : '{{$topDept}}',
			parents : {!! $parent !!},
			parent : "{{$parentSelected}}",
			assistant : false,
			assistantError : '',
			special : false,
			info : '',
			nameClass : 'form-control',
			nameError : '',
			assignUser : false,
			user : "{{array_keys($users)[0]}}",
			enabled : true
		},
		created () {
			this.assignUser = (this.isCorporate == 1 && this.topDept == 1 && this.parents == '') ? true : false;
		},
		mounted : function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		watch : {
			name : function (val) {
				this.nameError = "";
				if (val != "" && val.match(/^\s+/) == null) {
					this.nameClass = "form-control";
				}
			},
			assistant : function (val) {
				if (val === true) {
					if (this.special === true) {
						this.special = false;
					}
				}
				if (val === false) {
					this.assistantError = '';
				}
			},
			special : function (val) {
				if (val === true) {
					if (this.assistant === true) {
						this.assistant = false;
					}
				}
			},
			assignUser : function (val) {
				if (this.isCorporate == 1 && this.topDept == 1 && this.parents == '' && val === false) {
					this.assignUser = true;
				}
			}
		},
		methods : {
			changeBranch : function () {
                axios.post("{{url('/getstarted/function/create')}}", {
                    'branch' : this.branch,
                    '_token' : "{{csrf_token()}}"
                })
                .then(response => {
                    $("#get-started-popup-body").html(response.data);
                })
                .catch(console.log);
			},
			getParent : function () {
				axios.get("{{url('/getstarted/function/parent')}}/" + this.department)
				.then(response => {
					this.parents = response.data.parent;
					this.topDept = response.data.top;
				})
				.catch(console.log);
			},
			saveData : function (type) {
				this.enabled = false;
				if (this.assistant === true && this.name == "") {
					this.name = "Assistant";
				}
				if (this.name != "" && this.name.match(/^\s+/) == null) {
					this.nameClass = "form-control";
					this.nameError = "";
					var sendUrl = (type == 'save') ? "{{url('/position/form')}}" : "{{url('/getstarted/function/save')}}";
					axios.post(sendUrl, {
						'hid' : '',
						'department_id' : this.department,
						'name' : this.name,
						'parent_id' : this.parent,
						'info' : this.info,
						'assistant' : (this.assistant === true) ? 1 : null,
						'is_special_position' : (this.special === true) ? 1 : null,
						'branch' : this.branch,
						'started' : '1',
						'user_normal' : (this.assignUser === true) ? this.user : '',
						'_token' : "{{csrf_token()}}"
					})
					.then(response => {
						if (response.data !== false) {
							if (response.status == 201) {
								this.assistantError = response.data;
								this.enabled = true;
							} else {
								var url = (type == 'save') ? "{{url('/getstarted/function')}}" : "{{url('/getstarted/user')}}";
								axios.get(url).then(response => {
				                    $("#get-started-popup-body").html(response.data);
				                })
				                .catch(console.log);
							}
						}
					})
					.catch(error => {
						this.nameError = error.response.data.name[0];
						this.nameClass = "form-control error";
						this.enabled = true;
					});
				} else {
					this.nameClass = "form-control error";
					this.enabled = true;
				}
			},
            home : function () {
                axios.get("{{url('/getstarted/function')}}")
                .then(response => {
                    $("#get-started-popup-body").html(response.data);
                })
                .catch(console.log);
            }
		}
	});
</script>