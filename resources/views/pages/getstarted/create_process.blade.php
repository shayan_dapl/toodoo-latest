<div class="row">
	<div class="col-md-12 text-left mt20">
		{{translate('started.create_process_in')}} <strong>{{implode(", ", array_values($branch))}}</strong>: {{translate('started.what_process_is')}}
	</div>
</div>
<span id="getstarted-create-process" v-cloak>
	<div class="row mt50">
		<div class="col-md-3 col-md-offset-1 pt10 text-right">
			{{translate('form.process_name')}}
		</div>
		<div class="col-md-4">
			{!! Form::text('', '', ['v-bind:class' => 'nameClass', 'v-model' => 'name', 'tabindex' => '1']) !!}
		</div>
		<div class="col-md-4 pt10 text-left">
			<p class="text-danger" v-cloak>@{{nameError}}</p>
		</div>
	</div>
	<div class="row mt5">
		<div class="col-md-3 col-md-offset-1 pt10 text-right">
			{{translate('form.category')}}
		</div>
		<div class="col-md-8 text-left">
			<p class="custom_radio" v-for="(cat, i) in categories">
				<input type="radio" v-model="category" :id="'cat-' + i" :value="i">
				<label :for="'cat-' + i">@{{cat}}</label>
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-1 pt10 text-right">
			{{translate('form.process_owner')}}
		</div>
		<div class="col-md-4 text-left">
			{!! Form::select('', $owners, '', [':class' => 'ownerClass', 'v-model' => 'owner', 'tabindex' => '3']) !!}
		</div>
	</div>
	<div class="row mt20">
		<div class="col-md-6">
			<button class="btn btn-sm btn-system" @click="home">{{translate('tutorial.back_button')}}</button>
		</div>
		<div class="col-md-6 text-right">
			<span v-cloak class="text-success">@{{processSuccess}}</span>
			<button class="btn btn-sm btn-system" :disabled="disable" v-on:click="saveData">{{translate('started.add')}}</button>
		</div>
	</div>
</span>
@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
	new Vue({
		el : '#getstarted-create-process',
		data : {
			name : '',
			nameClass : 'form-control',
			nameError : '',
			category : Object.keys({!! $categories !!})[0],
			categories : {!! $categories !!},
			owner : "{{array_keys($owners)[0]}}",
			ownerClass : 'form-control',
			noturtle : true,
			isSub : false,
			processSuccess : "",
			disable : false
		},
		mounted : function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		watch : {
			name : function (val) {
				if (val != "" && val.match(/^\s+/) == null) {
					this.nameClass = "form-control";
					this.nameError = "";
				}
			},
			owner : function (val) {
				if (val != "") {
					this.ownerClass = "form-control";
				}
			}
		},
		methods : {
			saveData : function () {
				let errors = 0;
				if (this.name == "" || this.name.match(/^\s+/) != null) {
					this.nameClass = "form-control error";
					errors = 1;
				}
				if (this.owner == "") {
					this.ownerClass = "form-control error";
					errors = 1;
				}
				if (errors == 0) {
					this.disable = true;
					let branchIds = Object.keys({!! $ids !!});
					axios.post("{{url('/process/form')}}", {
						'is_sub_process' : (this.isSub === true) ? 1 : null,
						'category_id' : this.category,
						'owner_id' : this.owner,
						'no_turtle_process' : (this.noturtle === false) ? 1 : null,
						'name' : this.name,
						'branches' : branchIds
					})
					.then(response => {
						this.disable = false;
						this.processSuccess = "{{translate('words.success_message')}}";
						axios.get("{{url('/getstarted/process')}}")
		                .then(response => {
		                    $("#get-started-popup-body").html(response.data);
		                })
		                .catch(console.log);
					})
					.catch(error => {
						this.disable = false;
						this.nameError = error.response.data.name[0];
						this.nameClass = "form-control error";
					});
				}
			},
            home : function () {
                axios.get("{{url('/getstarted/process')}}")
                .then(response => {
                    $("#get-started-popup-body").html(response.data);
                })
                .catch(console.log);
            }
		}
	});
</script>