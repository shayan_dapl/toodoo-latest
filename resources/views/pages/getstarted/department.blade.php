<span id="getstarted-department">
    <div class="row mt20">
        <div class="col-md-3 text-right pt10">
           {{$serialNo}}. {{translate('started.department_heading')}} 
        </div>
        <div class="col-md-3">
            @if (!empty($topBranchName))
                <p class="text-system fs16 mt10">{{$topBranchName}}</p>
            @else
                {!! Form::select('', $branches, '', ['class'=>'form-control', 'v-model' => 'branch']) !!}
            @endif
        </div>
        <div class="col-md-6 pt10">
            : {{translate('started.what_department_is')}}
            @if (!empty($topBranchName))
                <p class="mt5">{{translate('started.default_created_branch')}}</p>
            @endif
        </div>
    </div>
    <div class="mt30 branchPops" v-cloak>
    	<div class="row mb5" v-for="(each, i) in departments">
            <div class="col-md-4 col-md-offset-2 fs16 text-left">
                @{{i+1}} : <span v-show="!each.edit">@{{each.name}}</span>
                <input type="text" v-model="each.name" v-show="each.edit" v-bind:id="each.id" :ref="each.id" class="form-control-extended">
            </div>
            <div class="col-md-4 text-left">
                <a href="#" v-show="!each.edit" v-on:click.prevent="edit(each, $event)" class="btn btn-success dark">
                    {{translate('started.edit')}}
                </a>
                <a href="#" v-show="!each.edit && each.is_top == 0" v-on:click="remove(each.id)" class="ml5 btn btn-danger dark">
                    {{translate('started.delete')}}
                </a>
                <button class="btn btn-sm btn-system" v-show="each.edit" v-on:click="save(each)">
                    {{translate('form.save_button')}}
                </button>
            </div>
        </div>

    	<div class="row mt20">
    		<div class="col-md-4 col-md-offset-2 text-center">
    			<input type="text" ref="name" v-model="name" :class="nameClass" placeholder="{{translate('words.department')}}" tabindex="1">
    			<input type="hidden" v-model="hid" value="">
    		</div>
            <div class="col-md-2">
                <button class="round-button" v-on:click="add" tabindex="2"><i class="fa fa-plus"></i></button>
            </div>
    		<div class="col-md-4 pt10">
                <span class="text-danger">@{{faliure}}</span>
                <span class="text-success">@{{success}}</span>
            </div>
    	</div>

    	<div class="row mt70">
            <div class="col-md-6">
                <button class="btn btn-sm btn-system" @click="home">{{translate('tutorial.back_button')}}</button>
            </div>
    		<div class="col-md-6 text-right">
    			<next v-show="!next" :data-name="name" :data-hid="hid" :data-branch="branch" />
    		</div>
    	</div>
    </div>
</span>
@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    Vue.component('next', {
        template : '<button class="btn btn-system text-default w100 mb5">{{translate('started.next')}}</button>',
        mounted: function () {
            $(this.$el).click(function () {
                if (this.dataset.name != "" && this.dataset.name.match(/^\s+/) == null) {
                    axios.post("{{ url('/department/') }}", { 
                        'name' : this.dataset.name,
                        'hid' : this.dataset.hid,
                        'branch' : this.dataset.branch,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        $.ajax({
                            url: "{{url('/getstarted/department/copy')}}",
                            type: "GET",
                            success: function (data) {
                                $("#get-started-popup-body").html(data);
                            }
                        });
                    })
                    .catch(console.log);
                } else {
                    $.ajax({
                        url: "{{url('/getstarted/department/copy')}}",
                        type: "GET",
                        success: function (data) {
                            $("#get-started-popup-body").html(data);
                        }
                    });
                }
            });
        }
    });

    new Vue({
        el : '#getstarted-department',
        data : {
            branch : "{{$branch->id}}",
            departments : [],
            name : '',
            nameClass : 'form-control',
            faliure : '',
            success : '',
            hid : '',
            next : false
        },
        created () {
            this.fetch(this.branch);
        },
        watch : {
            branch : function (val) {
                this.fetch(val);
            },
            name : function (val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.nameClass = 'form-control';
                }
            },
            success : function (val) {
                if (val != '') {
                    setTimeout(() => {
                        this.success = '';
                    }, 1000);
                }
            },
            faliure : function (val) {
                if (val != '') {
                    setTimeout(() => {
                        this.faliure = '';
                    }, 1000);
                }
            }
        },
        methods : {
            fetch : function (id) {
                axios.get("{{url('/getstarted/departments')}}/" + id).then(response => {
                    this.departments = response.data;
                });
            },
            add : function () {
                let errors = 0;
                if (this.name == "" || this.name.match(/^\s+/) != null) {
                    this.nameClass = "form-control error";
                    errors = 1;
                }
                if (errors == 0) {
                    axios.post("{{ url('/department/') }}", { 
                        'name' : this.name,
                        'hid' : this.hid,
                        'branch' : this.branch,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        this.$refs.name.focus();
                        if (response.data === false) {
                            this.faliure = "{{translate('words.department')}} " + "{{translate('alert.already_exist')}}";
                        }
                        if (response.data === true) {
                            this.faliure = "";
                            this.success = "{{translate('words.success_message')}}";
                        }
                    })
                    .then(() => {
                        this.fetch(this.branch);
                        this.name = this.hid = '';
                    })
                    .catch(console.log);
                }
            },
            edit : function (each, e) {
                each.edit = true;
                this.next = true;
                this.$nextTick(() => {
                    this.$refs[each.id][0].focus();
                });
            },
            save : function (each) {
                each.edit = false;
                this.next = false;
                let errors = 0;
                if (each.name == "" || each.name.match(/^\s+/) != null) {
                    this.nameClass = "form-control error";
                    errors = 1;
                }
                if (errors == 0) {
                    axios.post("{{ url('/department/') }}", { 
                        'name' : each.name,
                        'branch' : this.branch,
                        'hid' : each.id
                    })
                    .then(response => {
                        if (response.data === false) {
                            this.fetch(this.branch);
                            this.faliure = "{{translate('words.department')}} " + "{{translate('alert.already_exist')}}";
                        } 
                        if (response.data === true) {
                            this.faliure = "";
                            this.success = "{{translate('words.success_message')}}";
                        }
                        this.name = this.hid = '';
                    })
                    .catch(console.log);
                }
            },
            remove: function (val) {
                var confirmed = confirm("{{translate('alert.are_you_sure')}}");
                if (confirmed == true) {
                    axios.post("{{ url('/department/remove') }}", {
                        'id' : val,
                        '_token' : "{{csrf_token()}}"
                    }).then(response => {
                        if (response.data === true) {
                            this.success = "{{translate('words.success_message')}}";
                        }
                        if (response.data === 'child') {
                            this.faliure = "{{translate('alert.child_posiiton_exists')}}";
                        }
                        if (response.data === false) {
                            this.faliure = "{{translate('words.remove')}} " + "{{translate('alert.not_allowed')}}";
                        }
                        this.fetch(this.branch);
                    })
                    .catch(console.log); 
                }
            },
            home : function () {
                axios.get("{{url('/getstarted/started')}}")
                .then(response => {
                    $("#get-started-popup-body").html(response.data);
                })
                .catch(console.log);
            }
        }
    });
</script>