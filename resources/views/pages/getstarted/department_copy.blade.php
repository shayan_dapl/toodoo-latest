<span id="getstarted-department-copy">
	<div class="row">
	    <div class="col-md-12 text-left mt20">
	        {{translate('started.department_heading')}} <strong>{{$branch->name}}</strong>: {{translate('started.what_department_is')}}
	    </div>
	</div>
	<div class="row mt50">
		<div class="col-md-offset-2 col-md-3 pt10">
			{{translate('started.copy_department_from')}}
		</div>
		<div class="col-md-3"> 
			{!! Form::select('', $branches, '', ['class'=>'form-control', 'v-model' => 'branch']) !!}
		</div>
		<div class="col-md-1 pt5">
			<button class="btn btn-sm btn-system" v-on:click="copy" :disabled="enabled">
	            {{translate('started.copy')}}
	        </button>
		</div>
	</div>
	<div class="row mt20">
		<div class="col-md-12 text-center">
			{{translate('started.or')}}
		</div>
	</div>
	<div class="row mt20 mb30">
		<div class="col-md-12 text-center">
	        <next />
		</div>
	</div>
</span>
@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
	Vue.component('next', {
        template : '<button class="btn btn-sm btn-system">{{translate('started.start_from_scratch')}}</button>',
        mounted : function () {
            $(this.$el).click(function () {
                $.ajax({
                    url: "{{url('/getstarted/department')}}",
                    type: "GET",
                    success : function (data) {
                        $("#get-started-popup-body").html(data);
                    }
                });
            });
        }
    });

    var vuem = new Vue({
        el : '#getstarted-department-copy',
        data : {
        	branch : "{{array_keys($branches)[0]}}",
            enabled : false
        },
        methods : {
        	copy : function () {
                this.enabled = true;
                axios.post("{{url('/getstarted/department/copy')}}", {
                    'fromBranch' : this.branch,
                    'toBranch' : "{{$branch->id}}",
                    '_token' : "{{csrf_token()}}"
                })
                .then(response => {
                    if(response.data === true) {
                        this.enabled = false;
                        new PNotify({
                            title: "{{translate('words.success_message')}}",
                            text: "",
                            addclass: 'stack_top_right',
                            type: "success",
                            width: '290px',
                            delay: 2000
                        });

                       	$.ajax({
		                    url: "{{url('/getstarted/department/')}}",
		                    type: "GET",
		                    success : function (data) {
		                        $("#get-started-popup-body").html(data);
		                    }
		                });
                    }
        		})
        		.catch(console.log);
        	}
        }
    });
</script>