<span id="getstarted-function">
    <span v-if="!existing">	
        <div class="row">
    	    <div class="col-md-12 text-left mt20">
    	        {{translate('started.function_heading')}}
    	    </div>
    	</div>
    	<div class="row mt50">
    		<div class="col-md-offset-3 col-md-3 pt10">
    			{{translate('started.want_to_create_function')}}
    		</div>
    		<div class="col-md-3">
                @if ($topPersonExists == 0)
                    <p class="text-system fs16 mt10">{{array_values($branches)[0]}}</p>
                @else
                    <select class="form-control" v-model="branch" v-on:change="takeName">
                        <option v-for="(name, id) in branches" :value="id">
                            @{{name}}
                        </option>
                        <option v-for="(name, id) in unusedBranch" disabled>
                            <strike>@{{name}} [{{translate('alert.please_add_deprtments')}}]</strike>
                        </option>
                    </select>
                @endif
    		</div>
            <div class="col-md-3 pt10">
                
            </div>
    	</div>
    	<div class="row mt20 @if($existingFunctions == 0) mb50 @endif">
    		<div class="col-md-12 text-center">
    	        <button class="btn btn-sm btn-system" v-on:click="create">{{translate('started.create_function')}}</button>
    		</div>
    	</div>
        @if($existingFunctions > 0)
            <div class="row mt20 mb50">
                <div class="col-md-12 text-center">
                    <button class="btn btn-sm btn-default text-system" v-on:click="showExisting">{{translate('started.see_existing_function')}} <strong v-cloak>@{{filterBranch}}</strong>
                    </button>
                </div>
            </div>
        @endif
        <div class="row mt20">
            <div class="col-md-12 text-right">
                <button class="btn btn-sm btn-system" @click="home">{{translate('tutorial.back_button')}}</button>
            </div>
        </div>
    </span>
    <span v-if="existing" v-cloak>
        <div class="row">
            <div class="col-md-12 text-left mt20">
                {{translate('started.overview_functions')}}
            </div>
        </div>
        <div class="row">
           <div class="col-md-offset-2 col-md-10">
                <ul class="getstarted-tree text-left">
                    {!! $positions !!}
                </ul>
           </div> 
        </div>
        <div class="row mt20">
            <div class="col-md-12 text-center">
                <button class="btn btn-sm btn-system" v-on:click="existing = !existing">{{translate('tutorial.back_button')}}</button>
            </div>
        </div>
    </span>
</span>
@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    var vuem = new Vue({
        el : '#getstarted-function',
        data : {
            branches : {!! json_encode($branches) !!},
        	branch : "{{array_keys($branches)[0]}}",
            unusedBranch : {!! json_encode($unusedBranches) !!},
            existing : "{{$showExisting}}",
            filterBranch : "{{array_values($branches)[0]}}"
        },
        created () {
            let encodedStr = this.filterBranch;;
            let parser = new DOMParser;
            let dom = parser.parseFromString('<!doctype html><body>' + encodedStr, 'text/html');
            this.filterBranch = dom.body.textContent;
        },
        mounted : function () {
        	$(".select-branch").select2({
        		allowHtml: true,
        	});
        	$(".select-branch").on('change', function () {
                vuem.branch = [];
                for (var i = 0; i < this.selectedOptions.length; i++) {
                    vuem.branch.push(this.selectedOptions[i].value);
                }
            });
        },
        methods : {
        	create : function () {
                axios.post("{{url('/getstarted/function/create')}}", {
                    'branch' : this.branch,
                    '_token' : "{{csrf_token()}}"
                })
                .then(response => {
                    $("#get-started-popup-body").html(response.data);
                })
                .catch(console.log);
            },
            takeName : function (e) {
                this.filterBranch = e.target.options[e.target.options.selectedIndex].text;
            },
            showExisting : function () {
                axios.get("{{url('/getstarted/function/existing')}}/" + this.branch)
                .then(response => {
                    $("#get-started-popup-body").html(response.data);
                    this.existing = !this.existing;
                })
                .catch(console.log);
            },
            home : function () {
                axios.get("{{url('/getstarted/started')}}")
                .then(response => {
                    $("#get-started-popup-body").html(response.data);
                })
                .catch(console.log);
            }
        }
    });
</script>