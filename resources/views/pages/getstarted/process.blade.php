<span id="getstarted-process">
    <span v-if="!existing">	
        <div class="row">
    	    <div class="col-md-12 text-left mt20">
    	        {{translate('started.process_heading')}}
    	    </div>
    	</div>
    	<div class="row mt50">
    		<div class="col-md-offset-3 col-md-3 pt10">
    			{{translate('started.want_to_create_process')}}
    		</div>
    		<div class="col-md-3">
    			{!! Form::select('', $branches, '', ['class' => 'select-branch form-control', 'id' => 'branch', 'required' => 'required', 'multiple' => 'multiple', 'v-model' => 'branch']) !!}
                <span class="text-danger" v-cloak>@{{branchError}}</span>
    		</div>
    	</div>
    	<div class="row mt20">
    		<div class="col-md-12 text-center">
    	        <button class="btn btn-sm btn-system" v-on:click="create">{{translate('started.create_process')}}</button>
    		</div>
    	</div>
        @if($existingProcesses > 0)
            <div class="row mt20">
                <div class="col-md-12 text-center">
                    <button class="btn btn-sm btn-default text-system" v-on:click="existing = !existing">{{translate('started.see_existing_process')}}</button>
                </div>
            </div>
        @endif
        <div class="row mt20">
            <div class="col-md-12">
                <button class="btn btn-sm btn-system" @click="home">{{translate('tutorial.back_button')}}</button>
            </div>
        </div>
    </span>
    <span v-if="existing" v-cloak>
        <div class="row">
            <div class="col-md-12 text-left mt20">
                {{translate('started.overview_processes')}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.process_name')}}</th>
                            <th>{{translate('table.category')}}</th>
                            <th>{{translate('table.process_owner')}}</th>
                            <th>{{translate('words.branch')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('status', 'App\Services\ProcessList')
                        @inject('name', 'App\Services\ProcessList')
                        @foreach($processes as $eachDetail)
                        <tr>
                            <td>{{$eachDetail->name}} <h6 class="text-danger">{{$eachDetail->parent}}</h6></td>
                            <td>{{$name->name(session('lang'), $eachDetail)}}</td>
                            <td>{{$eachDetail->owner->name}}</td>
                            <td>
                                @if($eachDetail->branches->count() > 0)
                                    <ol>
                                        @foreach($eachDetail->branches as $eachBranch)
                                            <li>{{$eachBranch->branch->name}}</li>
                                        @endforeach
                                    </ol>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt20">
            <div class="col-md-12 text-center">
                <button class="btn btn-sm btn-system" v-on:click="existing = !existing">{{translate('tutorial.back_button')}}</button>
            </div>
        </div>
    </span>
</span>
@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    var vuem = new Vue({
        el : '#getstarted-process',
        data : {
        	branch : ["{{array_keys($branches)[0]}}"],
            branchError : "",
            existing : false
        },
        watch : {
            existing : function (val) {
                if (val === false) {
                    setTimeout(function () {
                        $(".select-branch").select2({
                            allowHtml: true,
                        });
                    }, 100);
                }
            }
        },
        mounted : function () {
        	$(".select-branch").select2({
        		allowHtml: true,
        	});
        	$(".select-branch").on('change', function () {
                vuem.branchError = "";
                vuem.branch = [];
                for (var i = 0; i < this.selectedOptions.length; i++) {
                    vuem.branch.push(this.selectedOptions[i].value);
                }
            });
        },
        methods : {
        	create : function () {
                if (this.branch == "") {
                    this.branchError = "{{translate('alert.please_select_branch')}}";
                } else {
                    this.branchError = "";
                    axios.post("{{url('/getstarted/process/create')}}", {
                        'branch' : this.branch,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        $("#get-started-popup-body").html(response.data);
                    })
                    .catch(console.log);                    
                }
            },
            home : function () {
                axios.get("{{url('/getstarted/started')}}")
                .then(response => {
                    $("#get-started-popup-body").html(response.data);
                })
                .catch(console.log);
            }
        }
    });
</script>