<div class="row">
	<div class="col-md-12 text-left mt20">
		{{translate('started.started_heading')}}
	</div>
</div>
<div class="row mt10 text-system">
	<div class="col-md-4">
		<div class="panel panel-tile panel-title-shadow @if(Auth::user()->company->departments()->count() == 1) active @endif br-a br-light">
			@if(Auth::user()->company->departments()->count() > 1)
			<i class="fa fa-check fa-2x pull-right checkbox-custom-step1 text-system"></i>
			@endif
			<div class="panel-body bg-light started-step1">
				<h4 class="fs16 mbn text-left">{{translate('started.branch_and_department')}}</h4>
				<h6 class="text-system ">{{translate('started.branch_and_department_text')}}</h6>
			</div>
			<div class="panel-footer bg-light br-light p5 text-right">
		      	<button class="btn btn-sm btn-system create dark" data-url="{{url('/getstarted/branch')}}">{{translate('words.create')}}</button>
		  	</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-tile panel-title-shadow @if(Auth::user()->company->departments()->count() > 1 and Auth::user()->company->positions()->count() == 0) active @endif br-a br-light">
			@if(Auth::user()->company->positions()->count() > 0)
				<i class="fa fa-check fa-2x pull-right checkbox-custom-step1 text-system"></i>
			@endif
			<div class="panel-body bg-light started-step1">
				<h4 class="fs16 mbn text-left">{{translate('started.function')}}</h4>
				<h6 class="text-system">{{translate('started.function_text')}}</h6>
			</div>
			<div class="panel-footer bg-light br-light p5 text-right">
				<button class="btn btn-sm create dark @if(Auth::user()->company->departments()->count() == 1) btn-default @else btn-system @endif" data-url="{{url('/getstarted/function')}}" @if(Auth::user()->company->departments()->count() == 1) disabled="" @endif>{{translate('words.create')}}</button>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-tile panel-title-shadow @if(Auth::user()->company->positions()->count() > 0 and Auth::user()->company->processes()->count() == 0) active @endif br-a br-light">
			@if(Auth::user()->company->processes()->count() > 0)
				<i class="fa fa-check fa-2x pull-right checkbox-custom-step1 text-system"></i>
			@endif
			<div class="panel-body bg-light started-step1">
				<h4 class="fs16 mbn text-left">{{translate('started.process')}}</h4>
				<h6 class="text-system">{{translate('started.process_text')}}</h6>
			</div>
			<div class="panel-footer bg-light br-light p5 text-right">
				<button class="btn btn-sm create dark @if(Auth::user()->company->positions()->count() > 0) btn-system @else btn-default @endif" data-url="{{url('/getstarted/process')}}" @if(Auth::user()->company->positions()->count() < 1) disabled="" @endif>{{translate('words.create')}}</button>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 text-right">
		<button type="button" class="btn btn-danger" id="dismiss" data-id="temporary" data-dismiss="modal">{{translate('started.close')}}</button>
		<span id="dismiss-here"></span>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		//Making all boxes of same size
		if ($('.started-step1').length > 0) {
			var boxHeight = 0;
			var boxWidth = 0;
			setTimeout(function() {
				$('.started-step1').each(function () {
					if (parseInt($(this).css('height')) > boxHeight) {
						boxHeight = parseInt($(this).css('height'));
					}
				});
				$('.started-step1').css('height', boxHeight);
			}, 500);
		}
		//===============================//

		$(".create").click(function () {
			$.ajax({
				url: $(this).data('url'),
				type: "GET",
				success: function (data) {
					$("#get-started-popup-body").html(data);
				}
			});
		});

		$('#dismiss').click(function () {
            $.ajax({
                url: "{{url('/getstarted/dismiss')}}/" + $(this).data('id'),
                type: "GET",
                success: function (data) {
                    var s = document.createElement("script");
				    s.type = "text/javascript";
				    s.async = true;
				    s.language = "javascript";
				    s.src = "{{Config::get('settings.whatfix_url')}}";
				    // Use any selector
				    $("head").append(s);
                }
            });
        });
	});
</script>