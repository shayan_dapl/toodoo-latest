<span id="getstarted-user" v-cloak>
    <span v-show="!existing">
        <div class="row">
            <div class="col-md-12 text-left">{{translate('started.user_heading')}}</div>
        </div>
        <div class="admin-form row mt20">
            <div class="col-md-8">
                <div class="row mb5">
                    <div class="col-md-7 pt10 text-right">{{translate('form.first_name')}}</div>
                    <div class="col-md-5">
                        <label class="field prepend-icon">
                            {!! Form::text('', '', ['v-bind:class' => 'fnameClass', 'v-model' => 'fname', 'required' => 'required', 'tabindex' => '1']) !!}
                            <label for="fname" class="field-icon"><i class="fa fa-user"></i></label>
                        </label>
                    </div>
                </div>
                <div class="row mb5">
                    <div class="col-md-7 pt10 text-right">{{translate('form.last_name')}}</div>
                    <div class="col-md-5">
                        <label class="field prepend-icon">
                            {!! Form::text('', '', ['v-bind:class' => 'lnameClass', 'v-model' => 'lname', 'required' => 'required', 'tabindex' => '2']) !!}
                            <label for="lname" class="field-icon"><i class="fa fa-user"></i></label>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 pt10 text-right">{{translate('form.email')}}</div>
                    <div class="col-md-5">
                        <label class="field prepend-icon">
                            {!! Form::email('', '', ['v-bind:class' => 'emailClass', 'v-model' => 'email', 'required' => 'required', 'tabindex' => '3', 'v-on:blur' => 'getImage']) !!}
                            <label for="email" class="field-icon"><i class="fa fa-envelope"></i></label>
                        </label>
                        <span class="text-danger">@{{emailError}}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 img-border">
                <div class="row">
                    <div class="col-md-12">
                        <label for="user_photo" class="pull-left userPhoto">
                            <i class="fa fa-cloud-upload pos-relative user-img-icon fs20 pointer"></i> 
                            {{ Html::image('', '', ['v-bind:src' => 'imgsrc', 'class' => 'thumbnail pointer', 'id' => 'user_preview', 'height' => '100', 'width' => '100']) }}
                        </label>
                        <span id="uploader1"></span>
                        <input type="file" id="user_photo" class="hide" accept=".jpeg,.jpg,.png" v-on:change="onImageChange" />
                    </div>
                    <div class="col-md-12">
                        <div v-if="isityou">
                            <p class="text-danger">{{translate('alert.is_it_you')}}</p>
                            <button class="btn btn-success" @click="confirmIt('yes')">{{translate('form.yes')}}</button>
                            <button class="btn btn-danger" @click="confirmIt('no')">{{translate('form.no')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="admin-form row mt30">
            <div class="col-md-8">
                <div class="row mb5">
                    <div class="col-md-7 pt10 text-right">{{translate('form.language')}}</div>
                    <div class="col-md-5">
                        <label class="field select">
                            {!! Form::select('language', $language, '', ['v-model' => 'language', 'tabindex' => '4', 'v-bind:class' => 'languageClass']) !!}
                            <i class="arrow"></i>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row mt30 mb5">
            <div class="col-md-5 pt10 text-right admin-form pr35">{{translate('form.process_owner')}}</div>
            <div class="col-md-1">
                <div class="switch switch-system switch-started round switch-inline pull-left mt10">
                    <input id="powner" type="checkbox" tabindex="5" v-model="powner" />
                    <label for="powner"></label>
                </div>
            </div>
            <div class="col-md-6 text-system pt5">{{translate('started.who_is_processowner')}}</div>
        </div>
        <div class="row">
            <div class="col-md-5 pt10 text-right admin-form pr35">{{translate('form.is_auditor')}}</div>
            <div class="col-md-1">
                <div class="switch switch-system switch-started round switch-inline pull-left mt10">
                    <input id="auditor" type="checkbox" tabindex="6" v-model="auditor" />
                    <label for="auditor"></label>
                </div>
            </div>
            <div class="col-md-6 text-system pt5">{{translate('started.who_is_auditor')}}</div>
        </div>
        <div class="admin-form row mt30">
            <div class="col-md-8">
                <div class="row mb5">
                    <div class="col-md-7 pt10 text-right">{{translate('started.function_name')}}</div>
                    <div class="col-md-5 text-system fs16 pt10 pl35">{{!empty($position) ? $position['name'] : ''}}</div>
                </div>
            </div>
            <div class="col-md-4 text-success pt10">@{{success}}</div>
        </div>
        <div class="row mt30 ">
            <div class="col-md-5 pt10 text-right admin-form pr35">{{translate('form.resent_invitation_mail')}}</div>
            <div class="col-md-1">
                <div class="switch switch-system switch-started round switch-inline pull-left mt10">
                    <input id="invitation" type="checkbox" tabindex="8" v-model="invitation" />
                    <label for="invitation"></label>
                </div>
            </div>
            <div class="col-md-6 text-system pt5">{{translate('started.what_is_invitation')}}</div>
        </div>
    </span>
    <span v-show="existing">
        <table class="table table-striped table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>{{translate('table.photo')}}</th>
                    <th>{{translate('table.name')}}</th>
                    <th>{{translate('table.email')}}</th>
                    <th>{{translate('table.joined_on')}}</th>
                    <th>{{translate('table.status')}}</th>
                </tr>
            </thead>
            <tbody>
                @inject('status', 'App\Services\UserList')
                @foreach($users as $eachUser)               
                <tr>
                    <td align="right">
                        <span class="sitemapicon">
                            @if($eachUser->is_top_person == 1) 
                            <i class="fa fa-sitemap text-danger pos-absolute"></i>
                            &nbsp;&nbsp;
                            @endif
                            @if($eachUser->is_top_person == 1 and $eachUser->type == 2)
                            &nbsp;&nbsp;&nbsp;
                            @endif
                            @if($eachUser->type == 2) 
                            <i class="fa fa-cog text-danger pos-absolute"></i>
                            &nbsp;&nbsp;
                            @endif
                            @if($eachUser->is_special_user == 1) 
                            <i class="fa fa-strikethrough text-danger pos-absolute"></i>
                            &nbsp;&nbsp;
                            @endif
                            @if($eachUser->is_auditor == 1) 
                            <i class="fa fa-bullhorn text-danger pos-absolute"></i>
                            &nbsp;&nbsp;
                            @endif
                            @if($eachUser->can_process_owner == 1) 
                            <i class="fa fa-cogs text-danger pos-absolute"></i>
                            &nbsp;&nbsp;
                            @endif
                        </span>
                        @if(\File::isFile('storage/company/'.$eachUser->company->id.'/image/users/'.$eachUser->photo))
                            <img src="{{asset('storage/company/'.$eachUser->company->id.'/image/users/'.$eachUser->photo)}}" height="50">
                        @else
                            <img src="{{asset('image/placeholder.png')}}" height="50">
                        @endif
                    </td>
                    <td @if($eachUser->status == 3) class="text-danger" @endif>
                         {{$eachUser->name}}
                    </td>
                    <td>{{$eachUser->email}}</td>
                    <td>{{ $eachUser->created_at }}</td>
                    <td>{{$status->status($eachUser->status)}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </span>
    <div class="row mt30">
        <div class="col-md-8">
            <button class="btn btn-sm btn-system" v-show="!existing" @click="home">
                {{translate('tutorial.back_button')}}
            </button>
            <button class="btn btn-sm btn-system" v-show="!existing" @click="existing = !existing">
                {{translate('started.see_existing_users')}}
            </button>
            <button class="btn btn-sm btn-system" v-show="existing" @click="existing = !existing">
                {{translate('tutorial.back_button')}}
            </button>
        </div>
        <div class="col-md-4 text-right">
            <button class="btn btn-sm btn-system" v-show="!existing" v-on:click="saveData" :disabled="!enabled">
                {{translate('started.add')}}
            </button>
        </div>
    </div>
</span>
@include('includes.vue_assets')
@stack('scripts')
<script type="text/javascript">
    var vuem = new Vue({
        el : '#getstarted-user',
        data : {
            fname : '',
            lname : '',
            email : '',
            image : '',
            imageData : '',
            imageUrl : '/image/placeholder.png',
            imgsrc : '/image/placeholder.png',
            language : "{{session('lang')}}",
            powner : false,
            auditor : false,
            invitation : true,
            fnameClass : 'gui-input',
            lnameClass : 'gui-input',
            emailClass : 'gui-input',
            languageClass : '',
            emailError : '',
            enabled : true,
            success : '',
            existing : false,
            isityou : false
        },
        watch : {
            fname : function (val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.fnameClass = "gui-input";
                }
            },
            lname : function (val) {
                if (val != "" && val.match(/^\s+/) == null) {
                    this.lnameClass = "gui-input";
                }
            },
            email : function (val) {
                this.emailError = '';
                if (val != "" && val.match(/^\s+/) == null) {
                    this.emailClass = "gui-input";
                }
            },
            language : function (val) {
                if (val != "") {
                    this.languageClass = "";
                }
            }
        },
        methods : {
            getImage(e) {
                if (this.email != "" && this.email.match(/^\s+/) == null && vuem.image == '') {
                    axios.post("{{url('/getstarted/getimage')}}", {
                        'email' : this.email,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        if (response.data === false) {
                            vuem.image = '';
                            vuem.imgsrc = '/image/placeholder.png';
                        } else {
                            var img = new Image();
                            img.crossOrigin = 'anonymous';
                            img.src = response.data;
                            var canvas = document.createElement("canvas");
                            var ctx = canvas.getContext("2d");
                            canvas.width = 100;
                            canvas.height = 100;
                            img.onload = function() {
                                ctx.drawImage(img, 0, 0);
                                var url = canvas.toDataURL("image/jpeg", 1.0);
                                vuem.imageData = url;
                            };
                            vuem.imageUrl = response.data;
                            if (vuem.image == '') {
                                setTimeout(() => {
                                    vuem.image = vuem.imageData;
                                }, 1000);
                                vuem.imgsrc = vuem.imageUrl;
                                vuem.isityou = true;
                            }
                        }
                        return Promise.resolve();
                    })
                    .catch(console.log);
                }
            },
            onImageChange(e) {
                let files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                    return;
                let reader = new FileReader();
                let vuem = this;
                reader.onload = (e) => {
                    vuem.image = e.target.result;
                };
                reader.readAsDataURL(files[0]);
                this.imgsrc = '/image/placeholder.png';
            },
            saveData : function () {
                let error = 0;
                if (this.fname == "" || this.fname.match(/^\s+/) != null) {
                    this.fnameClass = "gui-input error";
                    error = 1;
                } if (this.lname == "" || this.lname.match(/^\s+/) != null) {
                    this.lnameClass = "gui-input error";                   
                    error = 1;
                } if (this.email == "" || this.email.match(/^\s+/) != null) {
                    this.emailClass = "gui-input error";              
                    error = 1;
                } if (this.language == "") {
                    this.languageClass = "error"; 
                    error = 1;
                } if (error == 0) {
                    this.enabled = false;
                    axios.post("{{url('/getstarted/user/save')}}", {
                        'fname' : this.fname,
                        'lname' : this.lname,
                        'email' : this.email,
                        'photo' : this.image,
                        'language' : this.language,
                        'usertype' : 3,
                        'is_auditor' : (this.auditor === true) ? 1 : null,
                        'can_process_owner' : (this.powner === true) ? 1 : null,
                        'invite_user' : (this.invitation === true) ? 1 : null,
                        '_token' : "{{csrf_token()}}"
                    })
                    .then(response => {
                        if (response.data !== false) {
                            axios.post("{{url('/position/form')}}", {
                                'hid' : '',
                                'department_id' : "{{$position['department_id']}}",
                                'name' : "{{$position['name']}}",
                                'parent_id' : "{{$position['parent_id']}}",
                                'info' : "{{$position['info']}}",
                                'assistant' : "{{$position['assistant']}}",
                                'is_special_position' : "{{$position['is_special_position']}}",
                                'branch' : "{{$position['branch']}}",
                                'started' : '1',
                                'user_normal' : response.data,
                                '_token' : "{{csrf_token()}}"
                            }).then(() => {
                                this.enabled = true;
                                axios.get("{{url('/getstarted/function')}}")
                                .then(response => {
                                    this.success = "{{translate('words.success_message')}}";
                                    setTimeout(function () {
                                        $("#get-started-popup-body").html(response.data);
                                    }, 1000);
                                })
                                .catch(console.log);
                            });

                        }
                    })
                    .catch(error => {
                        this.enabled = true;
                        this.emailError = error.response.data.email[0];
                        this.emailClass = "gui-input error";
                    });
                }
            },
            home : function () {
                axios.get("{{url('/getstarted/started')}}")
                .then(response => {
                    $("#get-started-popup-body").html(response.data);
                })
                .catch(console.log);
            },
            confirmIt : function (val) {
                if (val == 'no') {
                    this.image = '';
                    this.imgsrc = '/image/placeholder.png';
                }
                this.isityou = false;
            }
        }
    });

    $("#user_photo").change(function () {
        preview(this, 'user_preview', 'uploader1');
    });
</script>