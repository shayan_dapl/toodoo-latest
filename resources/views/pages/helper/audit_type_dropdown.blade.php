<select name="{{$type}}" id="parent_type">
	@foreach($options as $k => $v)
        <option value="{{$k}}" class="@if(in_array($k, $topTypes))fw700 @endif">
            {{$v}}
        </option>
    @endforeach
</select>
<i class="arrow"></i>