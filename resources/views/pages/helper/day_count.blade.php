@if($dateDiff < 0)
    <p class="overdue">{{abs($dateDiff)}} {{translate('words.day_overdue')}}</p>
@elseif($dateDiff == 0)
	<p class="dayleft">{{abs($dateDiff)}} {{translate('words.day_left')}}</p>
@else
	<p class="dayleft">{{$dateDiff}} {{translate('words.day_left')}}</p>
@endif