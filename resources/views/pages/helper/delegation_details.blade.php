@foreach($members as $member) 
	<p>
   		<b>
   			{{getUserDetail($member->member)->name}}
   		</b> 
   		- 
   		{{date(Config::get('settings.dashed_date'),strtotime($member->deadline))}}
   		- 
   		<a href="">{{$typeArr[$member->issue_type]}}</a>
   		@if($member->status == 1)
   			<i class="fs10">( {{date(Config::get('settings.dashed_date'),strtotime($member->complete_date))}} )</i>
   		@endif
   	</p>
@endforeach