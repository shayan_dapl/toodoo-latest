@if (!empty($data))
    @inject('technician', 'App\Services\HelperPositionTree')
    @foreach ($data as $each)
        @if (isset($each->children))
            <li id="{{$each->id}}" data-special="{{$each->is_special_position}}" data-assistant="{{$each->is_assistant}}" data-topdept="{{$each->department->is_top}}" data-childassistant="{{$each->child_assistant}}">
                <span @if($each->is_assistant == 1) class="bg-info light" @endif>
                    <i class="fa fa-user {{$technician->technician($each->is_special_position)}}"></i> 
                    {{$each->name}} <label class="department">(@if(isset($each->branches()->get()[0]->branch->name)){{$each->branches()->get()[0]->branch->name}}@endif - {{$each->department->name}})</label> 
                    @if (Auth::guard('customer')->user()->type == 2 and $mode == "edit")
                        <a href="{{url('/position/edit/' . $each->id)}}">
                            <i class="fa fa-pencil text-success"></i>
                        </a> 
                        <a href="{{url('/position/remove/' . $each->id)}}" onclick="return confirm('{{translate('alert.are_you_sure')}}')">
                            <i class="fa fa-trash text-danger"></i>
                        </a>
                    @endif
                </span>
                <ul>
                    @include('pages.helper.position_tree', ['data' => $each->children])
                </ul>
            </li>
        @else
            <li id="{{$each->id}}" data-special="{{$each->is_special_position}}" data-assistant="{{$each->is_assistant}}" data-topdept="{{$each->department->is_top}}" data-childassistant="{{$each->child_assistant}}">
                <span>
                    <i class="fa fa-user {{$technician->technician($each->is_special_position)}}"></i> 
                    {{$each->name}} <label class="department">({{$each->branches()->get()[0]->branch->name}} - {{$each->department->name}})</label> 
                    @if (Auth::guard('customer')->user()->type == 2 and $mode == "edit")
                        <a href="{{url('/position/edit/' . $each->id)}}">
                            <i class="fa fa-pencil text-success"></i>
                        </a> 
                        <a href="{{url('/position/remove/' . $each->id)}}" onclick="return confirm('{{translate('alert.are_you_sure')}}')">
                            <i class="fa fa-trash text-danger"></i>
                        </a>
                    @endif
                </span>
            </li>
        @endif
    @endforeach
@endif