@extends('layouts.default')
@section('content')
@auth('customer')
@inject('divwdth', 'App\Services\HomeDashboard')
<div class="row">
    <div class="col-md-12">
        <label>&nbsp;</label>
        <div class="tab-block mb25">
            <ul class="nav nav-tabs" id="myTab">
                <li class="lang-links">
                    <a href="#overview" data-toggle="tab" id="cockpitTab">{{translate('words.cockpit')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->is_auditor == 1)
                <li class="lang-links">
                    <a href="#audit_planning" data-toggle="tab" id="planningTab">{{translate('words.audit_planning')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#issues" data-toggle="tab" id="auditIssues">
                        <img src="{{asset('image/too-doo-logo-tab.png')}}" height="12">
                    </a>
                </li>
                <li class="lang-links">
                    <a href="#audit_meetings" data-toggle="tab" id="auditMeeting">{{translate('words.audit_meetings')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                <li class="lang-links">
                    <a href="#procesverbeteringen" data-toggle="tab" id="procesIssues">{{translate('words.process_improvements')}}
                    </a>
                </li>
                @endif
                <li class="lang-links active">
                    <a href="#result" data-toggle="tab" id="auditResult">
                        {{translate('words.audit_results')}}
                    </a>
                </li>
                @if(Auth::guard('customer')->user()->company_view == 1)
                <li class="lang-links">
                    <a href="#company-view" data-toggle="tab" id="companyView">
                        {{translate('words.company_view')}}
                    </a>
                </li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane">
                </div>

                <div id="audit_planning" class="tab-pane">   
                </div>

                <div id="issues" class="tab-pane">
                </div>

                <div id="audit_meetings" class="tab-pane">
                </div>

                <div id="procesverbeteringen" class="tab-pane">
                </div>

                <div id="result" class="tab-pane active">

                    <div class="panel">
                        <div class="row mb10 admin-form">
                            <div class="col-md-3">
                                <span class="date-label">{{translate('words.from')}}: </span>
                                <label class="field append-icon">
                                    <input class="form-control date" type="text" id="minDate" />
                                    <label for="minDate" class="field-icon">
                                        <i class="fa fa-calendar"></i>
                                    </label>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <span class="date-label">{{translate('words.to')}}: </span>
                                <label class="field append-icon">
                                    <input class="form-control date" type="text" id="maxDate" />
                                    <label for="maxDate" class="field-icon">
                                        <i class="fa fa-calendar"></i>
                                    </label>
                                </label>
                            </div>
                            <div class="col-md-3">
                                {{translate('words.status')}}: 
                                <select class="form-control" id="actions">
                                    <option value="">{{translate('words.all')}}</option>
                                    <option value="{{translate('table.not_started')}}">{{translate('table.not_started')}}</option>
                                    <option value="{{translate('table.on_going')}}">{{translate('table.on_going')}}</option>
                                    <option value="{{translate('words.closed')}}">{{translate('words.closed')}}</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                {{translate('form.process_owner')}}: 
                                <select class="form-control" id="processOwner">
                                    <option value="">{{translate('form.select')}}</option>
                                    @foreach($processOwners as $eachProcess)
                                        <option value="{{$eachProcess}}">{{$eachProcess}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="datatable-result" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="bg-light">
                                            <th width="80px" class="">{{translate('words.audit_date')}}</th>
                                            <th class="text-center">{{translate('words.category')}}</th>
                                            <th class="">{{translate('words.process')}}</th>
                                            <th width="180px" class="">{{translate('words.finding_type')}}</th>
                                            <th class="">{{translate('form.description')}}</th>
                                            <th width="140px" class="">{{translate('form.process_owner')}}</th>
                                            <th class="">{{translate('words.status')}}</th>
                                            <th class="">{{translate('words.action')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($auditResults))
                                        @foreach($auditResults as $each)
                                        <tr>
                                        <td class="">{{date(Config::get('settings.dashed_date'), strtotime($each->plan_date))}}</td>
                                            <td class="">{{translate('words.'.$each->category)}}</td>
                                            <td class="">{{$each->process}}</td>
                                            <td class="">{{$each->finding_type}}</td>
                                            <td class="">{{$each->observation}}</td>
                                            <td>{{$each->processowner}}</td>
                                            <td>
                                                @if(delegationStatus($each) == 0)
                                                    {{translate('table.not_started')}}
                                                @elseif(delegationStatus($each) == 1)
                                                    {{translate('table.on_going')}}
                                                @else
                                                    {{translate('words.closed')}}
                                                @endif
                                            </td>
                                            <td class="">
                                            @if($each->owner_id == Auth::guard('customer')->user()->id)
                                                <a href="{{url('/audit/report-issue-action/'.$each->id.'/exec-issue')}}" class="btn btn-system text-default" title="{{translate('words.view_detail')}}">
                                                    {{translate('words.view_detail')}}
                                                </a>
                                            @else
                                                <a href="{{url('/audit/report-issue-view/'.$each->id.'/exec-issue')}}" class="btn btn-system text-default" title="{{translate('words.view_detail')}}">
                                                    {{translate('words.view_detail')}}
                                                </a>
                                            @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="company-view" class="tab-pane">
                </div>
            </div>
        </div>
    </div>
</div>
@endauth
@include('includes.scripts.dashboard_issues_script')
@stop
