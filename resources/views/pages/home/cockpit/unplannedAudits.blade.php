@extends('layouts.default')
@section('content')
@inject('divwdth', 'App\Services\HomeDashboard')
<link rel="stylesheet" type="text/css" href="{{asset('css/highcharts.css')}}"></style>

@guest('customer')
<div class="row">
    <div class="col-sm-6">
        <div class="panel">
            <div class="panel-heading bg-system dark">
                {{translate('words.total_active_companies')}} <b>[ {{$activeCompany}} ]</b>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel">
            <div class="panel-heading bg-system light">
                {{translate('words.total_inactive_companies')}} <b>[ {{$inactiveCompany}} ]</b>
            </div>
        </div>
    </div>
</div>
@endauth
@auth('customer')
<div class="row">
    <div class="col-md-12">
        <label>&nbsp;</label>
        <div class="tab-block mb25">
            <ul class="nav nav-tabs" id="myTab">
                <li class="lang-links active">
                    <a href="#overview" data-toggle="tab">{{translate('words.cockpit')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->is_auditor == 1)
                <li class="lang-links">
                    <a href="#audit_planning" data-toggle="tab" id="planningTab">{{translate('words.audit_planning')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#issues" data-toggle="tab" id="auditIssues">
                        <img src="{{asset('image/too-doo-logo-tab.png')}}" height="12">
                    </a>
                </li>
                <li class="lang-links">
                    <a href="#audit_meetings" data-toggle="tab" id="auditMeeting">{{translate('words.audit_meetings')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                <li class="lang-links">
                    <a href="#procesverbeteringen" data-toggle="tab" id="procesIssues">{{translate('words.process_improvements')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#result" data-toggle="tab" id="auditResult">
                        {{translate('words.audit_results')}}
                    </a>
                </li>
                @if(Auth::guard('customer')->user()->company_view == 1)
                <li class="lang-links">
                    <a href="#company-view" data-toggle="tab" id="companyView">
                        {{translate('words.company_view')}}
                    </a>
                </li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane active">
                    <div id="loaderDv" style="display:none;"><i class="fa fa-cog fa-spin text-default fa-5x"></i></div>
                    <section id="content" class="table-layout animated fadeIn">
                        <div class="tray tray-center">
                            <div class="row">
                                <div class="{{$divwdth->divwdth()}} text-center">
                                    <div class="panel panel-tile br-a br-grey overview-segment-head">
                                        <div class="panel-body overview-segment-check" id="cockpit-issues">
                                            <div class="fw700 fs20 mt5">{{$veryUrgent}} {{translate('words.issues')}}</div>
                                            {{translate('words.last_seven_days')}}
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="text-system fs12 openIssues">{{translate('words.view_details')}}</a>
                                </div>
                                @if(Auth::guard('customer')->user()->is_auditor == 1)
                                <div class="{{$divwdth->divwdth()}} text-center">
                                    <div class="panel panel-tile br-a br-grey overview-segment-head active">
                                        <div class="panel-body overview-segment-check" id="cockpit-unplanned-audit">
                                            <div class="fw700 fs20 mt5">{{$unplannedAuditsNo}} {{translate('words.unplanned_audits')}}</div>
                                            {{translate('words.last_month')}}
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="text-system fs12 openPlaning">{{translate('words.view_details')}}</a>
                                </div>
                                @endif
                                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                                <div class="{{$divwdth->divwdth()}} text-center">
                                    <div class="panel panel-tile br-a br-grey overview-segment-head">
                                        <div class="panel-body overview-segment-check" id="cockpit-insight-process">
                                            <div class="fw700 fs20 mt5">{{$internalPendingIssues->count()}} {{translate('words.new_insights_to_process')}}</div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="text-system fs12 procesIssuess">{{translate('words.view_details')}}</a>
                                </div>
                                @endif
                                <div class="{{$divwdth->divwdth()}} text-center">
                                    <div class="panel panel-tile br-a br-grey overview-segment-head">
                                        <div class="panel-body overview-segment-check" id="cockpit-upcoming-meeting">
                                            <div class="fw700 fs20 mt5">{{$meetingPending}} {{translate('words.upcoming_meetings')}}</div>
                                            {{translate('words.next_seven_days')}}
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="text-system fs12 openMetting">{{translate('words.view_details')}}</a>
                                </div>
                            </div>
                            <div class="panel mt20">

                                <span id="segment-1" class="overview-segments hide"> 
                                </span>
                                <span id="segment-2" class="overview-segments">
                                    @if($unplannedAudits->count() > 0)
                                    <div class="panel-heading">
                                        <span class="fs18 ">{{translate('words.unplanned_audits')}}</span>
                                        <a href="javascript:void(0);" class="text-system fs12 openPlaning">
                                            {{translate('words.view_details')}}
                                        </a>
                                    </div>
                                </br>
                                <div class="row">
                                    @foreach($unplannedAudits as $each)
                                    @if($loop->iteration <= 3)
                                    <div class="col-md-4" id="p17">
                                        <div class="panel-heading ui-sortable-handle fs16" onclick="location.href ='{{url('/audit/audit-prepare/'.$each->id)}}'">
                                            <span class="panel-title">{{$each->audit_name}}</span>
                                        </div>
                                        <div class="panel-body pn default">
                                            <div class="list-group list-group-links list-group-spacing-xs mbn">
                                                <p class="list-group-item pointer">
                                                    {{translate('words.category')}} : 
                                                    <span class="text-success pr5 pl5 pull-right">
                                                        {{translate('words.'.$each->category)}}
                                                    </span> 
                                                </p>
                                                <p class="list-group-item Detailbadge">
                                                    {{translate('words.process')}} : 
                                                    @foreach($each->process as $eachProcess)
                                                    <span class="bg-primary">
                                                        {{$eachProcess->processDetail->name}}</span> 
                                                        @endforeach
                                                    </p>
                                                    <p class="list-group-item">
                                                        {{translate('words.planned_for')}} : 
                                                        <b class="text-success pull-right"> {{$each->plan_month}}/{{$each->plan_year}} </b>
                                                    </p>
                                                    <p class="list-group-item"> 
                                                        {{translate('words.lead_auditor')}} :
                                                        <b class="text-danger pull-right fs13">
                                                            {{$each->lead_auditor->name}}</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @endforeach
                                        </div>
                                        @endif
                                    </br>
                                    <div class="panel-heading">
                                        <span class="fs18 ">{{translate('words.audit_progress')}}</span>
                                    </div>
                                    <div class="form-group mt25">
                                        @if(!empty($unplannedAuditsMonthWise) or !empty($plannedAuditsMonthWise) or !empty($ongoingAuditsMonthWise) or !empty($finishedAuditsMonthWise))
                                        <select class="form-control w-a" id="filter" data-filter="unplanned-audit">
                                            <option value="{{date('Y-m-d', strtotime('-6 months'))}}">{{translate('words.last_six_months')}}</option>
                                            <option value="{{date('Y-m-d', strtotime('-3 months'))}}">{{translate('words.last_three_months')}}</option>
                                        </select>
                                        @endif
                                    </div>
                                    <div>
                                        <canvas id="unplannedAudit"></canvas>
                                    </div>
                                </span>
                                <span id="segment-3" class="overview-segments hide">
                                </span>
                                <span id="segment-4" class="overview-segments hide">  
                                </span>
                            </div>
                        </div>
                    </section>
                </div>

                <div id="audit_planning" class="tab-pane">

                </div>

                <div id="issues" class="tab-pane">

                </div>

                <div id="audit_meetings" class="tab-pane">

                </div>

                <div id="procesverbeteringen" class="tab-pane">

                </div>

                <div id="result" class="tab-pane"></div>

                <div id="company-view" class="tab-pane"></div>
            </div>
        </div>
    </div>
</div>
@endauth

<div id="issue-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header back4d text-default">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body justified"></div>
        </div>
    </div>
</div>

<div id="get-started-popup" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-default">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body justified text-center" id="get-started-popup-body">
            </div>
        </div>
    </div>
</div>

@include('includes.scripts.home.cockpit.unplanned_audit_script')
@stop
