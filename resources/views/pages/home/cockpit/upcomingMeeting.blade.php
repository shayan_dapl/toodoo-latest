@extends('layouts.default')
@section('content')
@inject('divwdth', 'App\Services\HomeDashboard')
<link rel="stylesheet" type="text/css" href="{{asset('css/highcharts.css')}}"></style>

@guest('customer')
<div class="row">
    <div class="col-sm-6">
        <div class="panel">
            <div class="panel-heading bg-system dark">
                {{translate('words.total_active_companies')}} <b>[ {{$activeCompany}} ]</b>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel">
            <div class="panel-heading bg-system light">
                {{translate('words.total_inactive_companies')}} <b>[ {{$inactiveCompany}} ]</b>
            </div>
        </div>
    </div>
</div>
@endauth
@auth('customer')
<div class="row">
    <div class="col-md-12">
        <label>&nbsp;</label>
        <div class="tab-block mb25">
            <ul class="nav nav-tabs" id="myTab">
                <li class="lang-links active">
                    <a href="#overview" data-toggle="tab">{{translate('words.cockpit')}}</a>
                </li>
               @if(Auth::guard('customer')->user()->is_auditor == 1)
                <li class="lang-links">
                    <a href="#audit_planning" data-toggle="tab" id="planningTab">{{translate('words.audit_planning')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#issues" data-toggle="tab" id="auditIssues">
                        <img src="{{asset('image/too-doo-logo-tab.png')}}" height="12">
                    </a>
                </li>
                <li class="lang-links">
                    <a href="#audit_meetings" data-toggle="tab" id="auditMeeting">{{translate('words.audit_meetings')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                <li class="lang-links">
                    <a href="#procesverbeteringen" data-toggle="tab" id="procesIssues">{{translate('words.process_improvements')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#result" data-toggle="tab" id="auditResult">
                        {{translate('words.audit_results')}}
                    </a>
                </li>
                @if(Auth::guard('customer')->user()->company_view == 1)
                <li class="lang-links">
                    <a href="#company-view" data-toggle="tab" id="companyView">
                        {{translate('words.company_view')}}
                    </a>
                </li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane active">
                    <div id="loaderDv" style="display:none;"><i class="fa fa-cog fa-spin text-default fa-5x"></i></div>
                    <section id="content" class="table-layout animated fadeIn">
                        <div class="tray tray-center">
                            <div class="row">
                                <div class="{{$divwdth->divwdth()}} text-center">
                                    <div class="panel panel-tile br-a br-grey overview-segment-head">
                                        <div class="panel-body overview-segment-check" id="cockpit-issues">
                                            <div class="fw700 fs20 mt5">{{$veryUrgent}} {{translate('words.issues')}}</div>
                                            {{translate('words.last_seven_days')}}
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="text-system fs12 openIssues">{{translate('words.view_details')}}</a>
                                </div>
                                @if(Auth::guard('customer')->user()->is_auditor == 1)
                                <div class="{{$divwdth->divwdth()}} text-center">
                                    <div class="panel panel-tile br-a br-grey overview-segment-head">
                                        <div class="panel-body overview-segment-check" id="cockpit-unplanned-audit">
                                            <div class="fw700 fs20 mt5">{{$unplannedAuditsNo}} {{translate('words.unplanned_audits')}}</div>
                                            {{translate('words.last_month')}}
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="text-system fs12 openPlaning">{{translate('words.view_details')}}</a>
                                </div>
                                @endif
                                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                                <div class="{{$divwdth->divwdth()}} text-center">
                                    <div class="panel panel-tile br-a br-grey overview-segment-head">
                                        <div class="panel-body overview-segment-check" id="cockpit-insight-process">
                                            <div class="fw700 fs20 mt5">{{$internalPendingIssues->count()}} {{translate('words.new_insights_to_process')}}</div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="text-system fs12 procesIssuess">{{translate('words.view_details')}}</a>
                                </div>
                                @endif
                                <div class="{{$divwdth->divwdth()}} text-center">
                                    <div class="panel panel-tile br-a br-grey overview-segment-head active">
                                        <div class="panel-body overview-segment-check" id="cockpit-upcoming-meeting">
                                            <div class="fw700 fs20 mt5">{{$meetingPending}} {{translate('words.upcoming_meetings')}}</div>
                                            {{translate('words.next_seven_days')}}
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="text-system fs12 openMetting">{{translate('words.view_details')}}</a>
                                </div>
                            </div>
                            <div class="panel mt20">
                                <span id="segment-1" class="overview-segments hide"> 
                                </span>
                                <span id="segment-2" class="overview-segments hide"> 
                                </span>
                                <span id="segment-3" class="overview-segments hide"> 
                                </span>
                                <span id="segment-4" class="overview-segments">
                                    @if(count($top3Meetings) > 0)
                                    <div class="panel-heading">
                                        <span class="fs18 ">{{translate('words.upcoming_meetings')}}</span>
                                        <a href="javascript:void(0);" class="text-system fs12 openMetting">
                                            {{translate('words.view_details')}}
                                        </a>
                                    </div>
                                </br>
                                <div class="row">
                                    @foreach($top3Meetings as $each)
                                    @if($loop->iteration < 4)
                                    <div class="col-md-4">
                                        <span class="fw600">{{date('jS F Y',strtotime($each['plan_date']))}}</span>
                                        <div class="panel" id="p17">
                                            <div class="panel-heading ui-sortable-handle">
                                                <span class="panel-title">{{$each['audit_name']}}</span>
                                                <span class="panel-controls">
                                                    <span class="bg-warning dark pr10 pl10 text-default">{{$each['type']}}</span>
                                                </span>
                                            </div>
                                            <div class="panel-body pn">
                                                <div class="list-group list-group-links list-group-spacing-xs mbn">
                                                    <a class="list-group-item pointer" href="javascript:void(0);"> {{translate('words.category')}} : <span class="text-success pr5 pl5 pull-right">{{translate('words.'.$each['category'])}}</span> </a>
                                                    <span class="list-group-item"> {{translate('words.process')}} :
                                                        @foreach($each['process'] as $eachprocess)
                                                        <span class="bg-primary pr5 mr5 pl5 pull-right">{{$eachprocess}}</span>
                                                        @endforeach  </span>
                                                        <span class="list-group-item"> {{translate('words.lead_auditor')}}: <b class="text-danger pull-right fs13">{{$each['lead_auditor']}}</b> </span>
                                                        <span class="list-group-item"> {{translate('words.status')}}: @if($each['auditStatus'] ==2)<b class="text-success pull-right fs13">Finished</b>@else<b class="text-danger pull-right fs13">Pending</b>@endif</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>
                                    @endif

                                    @if(count($plannedAuditMeeting) > 0)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="panel-heading exhight">
                                                <div class="panel-title hidden-xs pull-left">
                                                    <h3>{{translate('words.meeting_overview')}} -{{translate('words.internal')}}</h3>
                                                </div>
                                            </div>
                                            <div class="panel-body pn">
                                                <div class="head clearfix">
                                                    <div class="form-group mt25">
                                                        <select class="form-control w-a" id="filter" data-filter="internal">
                                                            <option value="6">{{translate('words.last_six_months')}}</option>
                                                            <option value="3">{{translate('words.last_three_months')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="calendar table-responsive" id="internal">
                                                    <table class="table table-bordered seperated">
                                                        <thead>
                                                            <tr class="bg-system">
                                                                <td>{{ucfirst(translate('words.process'))}}</td>
                                                                @foreach($monthsForMeetingCalender as $month)
                                                                <td>{{$month}}</td>
                                                                @endforeach
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($plannedAuditMeeting as $eachAudit)
                                                            <tr>
                                                                <td>{{$eachAudit['process']}}</td>
                                                                @foreach($eachAudit['months'] as $eachMonth)
                                                                <td class="{{$eachMonth['tdCls']}}">
                                                                    <a href="{{$eachMonth['link']}}" title="{{$eachMonth['title']}}" class="{{$eachMonth['aCls']}}">{{$eachMonth['auditDate']}}</a>
                                                                    @if($eachMonth['userType']!='')
                                                                    <br /><br /><span class="bg-success fs10 p2 pos-extended">{{$eachMonth['userType']}}</span>
                                                                    @endif
                                                                </td>
                                                                @endforeach
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    @if(count($planneExternaldAuditMeeting) > 0)
                                    <div class="row mt20">
                                        <div class="col-md-12">
                                            <div class="panel-heading exhight">
                                                <div class="panel-title hidden-xs pull-left">
                                                    <h3>{{translate('words.meeting_overview')}} - {{translate('words.external')}}</h3>
                                                </div>
                                            </div>
                                            <div class="panel-body pn">
                                                <div class="head clearfix">
                                                    <div class="form-group mt25">
                                                        <select class="form-control w-a" id="filter" data-filter="external">
                                                            <option value="6">{{translate('words.last_six_months')}}</option>
                                                            <option value="3">{{translate('words.last_three_months')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="calendar table-responsive" id="external">
                                                    <table class="table table-bordered seperated">
                                                        <thead>
                                                            <tr class="bg-system">
                                                                <td>{{ucfirst(translate('words.process'))}}</td>
                                                                @foreach($monthsForMeetingCalender as $month)
                                                                <td>{{$month}}</td>
                                                                @endforeach
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($planneExternaldAuditMeeting as $eachAudit)
                                                            <tr>
                                                                <td>{{$eachAudit['process']}}</td>
                                                                @foreach($eachAudit['months'] as $eachMonth)
                                                                <td class="{{$eachMonth['tdCls']}}">
                                                                    <a href="{{$eachMonth['link']}}" title="{{$eachMonth['title']}}" class="{{$eachMonth['aCls']}}">{{$eachMonth['auditDate']}}</a>
                                                                    @if($eachMonth['userType']!='')
                                                                    <br /><br /><span class="bg-success fs10 p2 pos-extended">{{$eachMonth['userType']}}</span>
                                                                    @endif
                                                                </td>
                                                                @endforeach
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </span>
                            </div>
                        </div>
                    </section>
                </div>

                <div id="audit_planning" class="tab-pane">

                </div>

                <div id="issues" class="tab-pane">

                </div>

                <div id="audit_meetings" class="tab-pane">

                </div>

                <div id="procesverbeteringen" class="tab-pane">

                </div>

                <div id="result" class="tab-pane"></div>
                
                <div id="company-view" class="tab-pane"></div>
            </div>
        </div>
    </div>
</div>
@endauth

<div id="issue-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header back4d text-default">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body justified"></div>
        </div>
    </div>
</div>

<div id="get-started-popup" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-default">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body justified text-center" id="get-started-popup-body">
            </div>
        </div>
    </div>
</div>

@include('includes.scripts.home.cockpit.upcoming_meeting_script')
@stop
