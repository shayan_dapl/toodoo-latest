<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs text-center"> 
                {{$modalheader}}         
                </div>
            </div>
            <div class="p15 table-responsive">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.company')}}</th>
                            <th>{{translate('table.created_since')}}</th>
                            @if(($type == 'trial-inactive') || ($type == 'subscribed-inactive'))
                            <th>{{translate('table.inactive_since')}}</th>
                            @elseif(($type == 'trial-active') || ($type == 'subscribed-active'))
                            <th>{{translate('table.last_login_since')}}</th>
                            @endif
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($companies as $eachCompany)
                            <tr>
                                <td>{{$eachCompany->name}}</td>
                                <td>{{(!empty($eachCompany->customeradmins[0])) ? timeLeft($eachCompany->customeradmins[0]['created_at']) : ''}}</td> 
                                @if(($type == 'trial-inactive') || ($type == 'subscribed-inactive'))
                                <td>{{isset($eachCompany->trial_end) ? timeLeft($eachCompany->trial_end) : ''}}</td>
                                @elseif(($type == 'trial-active') || ($type == 'subscribed-active'))
                                <td>{{isset($eachCompany->log->in_time) ? timeLeft($eachCompany->log->in_time) : ''}}</td>
                                @endif
                            </tr>	
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>