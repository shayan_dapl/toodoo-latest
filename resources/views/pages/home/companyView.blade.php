@extends('layouts.default')
@section('content')
@inject('divwdth', 'App\Services\HomeDashboard')
<link rel="stylesheet" type="text/css" href="{{asset('css/highcharts.css')}}"></style>
@auth('customer')
<div class="row">
    <div class="col-md-12">
        <label>&nbsp;</label>
        <div class="tab-block mb25">
            <ul class="nav nav-tabs" id="myTab">
                <li class="lang-links">
                    <a href="#overview" data-toggle="tab" id="cockpitTab">{{translate('words.cockpit')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->is_auditor == 1)
                <li class="lang-links">
                    <a href="#audit_planning" data-toggle="tab" id="planningTab">{{translate('words.audit_planning')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#issues" data-toggle="tab" id="auditIssues">
                        <img src="{{asset('image/too-doo-logo-tab.png')}}" height="12">
                    </a>
                </li>
                <li class="lang-links">
                    <a href="#audit_meetings" data-toggle="tab" id="auditMeeting">{{translate('words.audit_meetings')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                <li class="lang-links">
                    <a href="#procesverbeteringen" data-toggle="tab" id="procesIssues">{{translate('words.process_improvements')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#result" data-toggle="tab" id="auditResult">
                        {{translate('words.audit_results')}}
                    </a>
                </li>
                @if(Auth::guard('customer')->user()->company_view == 1)
                <li class="lang-links active">
                    <a href="#company-view" data-toggle="tab" id="companyView">
                        {{translate('words.company_view')}}
                    </a>
                </li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane">   
                </div>
                <div id="audit_planning" class="tab-pane"></div>
                <div id="issues" class="tab-pane"></div>
                <div id="audit_meetings" class="tab-pane"></div>
                <div id="procesverbeteringen" class="tab-pane"></div>
                <div id="result" class="tab-pane"></div>
                <div id="company-view" class="tab-pane active">
                    <div id="loaderDv" style="display:none;"><i class="fa fa-cog fa-spin text-default fa-5x"></i></div>
                    <section id="content" class="table-layout animated fadeIn">
                        <div class="tray tray-center">
                            <div class="panel mt20">
                                <span id="segment-1" class="overview-segments">
                                    <div class="panel-heading">
                                        <span class="fs18 ">{{translate('words.company_open_and_closed_issue')}}</span>
                                    </div>
                                    <div class="form-group mt25 disp-flex">
                                        <select class="form-control w200 upfilter" data-filter="open-closed-company-view">
                                            <option value="{{date('Y-m-d', strtotime('-6 months'))}}">{{translate('words.last_six_months')}}</option>
                                            <option value="{{date('Y-m-d', strtotime('-3 months'))}}">{{translate('words.last_three_months')}}</option>
                                        </select>
                                        {!! Form::select('language', $issueCategories, 'all', ['class' => 'form-control w200 ml5 issuecategory upper', 'data-filter' => 'open-closed-company-view']) !!}

                                        {!! Form::select('user', $users, '', ['class' => 'form-control w200 ml5 user-filter upper', 'data-filter' => 'open-closed-company-view']) !!}
                                    </div>
                                    <div>
                                        <canvas id="open-closed-issues-chart"></canvas>
                                    </div>

                                    <div class="form-group mt25 disp-flex">
                                        <select class="form-control w-a downfilter" data-filter="open-closed-company-view-pie">
                                            <option value="{{date('Y-m-d', strtotime('-6 months'))}}">{{translate('words.last_six_months')}}</option>
                                            <option value="{{date('Y-m-d', strtotime('-3 months'))}}">{{translate('words.last_three_months')}}</option>
                                        </select>
                                        {!! Form::select('user', $users, '', ['class' => 'form-control w200 ml5 user-filter-pie', 'data-filter' => 'open-closed-company-view-pie']) !!}
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel">
                                                <div class="panel-heading exhight">{{translate('words.company_open_issue')}}</div>
                                                <div class="panel-body">
                                                    <canvas id="open-issues-pie-chart"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="panel">
                                                <div class="panel-heading exhight">{{translate('words.company_closed_issue')}}</div>
                                                <div class="panel-body">
                                                    <canvas id="closed-issues-pie-chart"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </span>
                                <span id="segment-2" class="overview-segments hide"> 
                                </span>
                                <span id="segment-3" class="overview-segments hide">  
                                </span>
                                <span id="segment-4" class="overview-segments hide">   
                                </span>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endauth
<div id="companyListModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-system">
                &nbsp;
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="list-company">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{translate('started.close')}}</button>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.dashboard_company_view_script')
@stop
