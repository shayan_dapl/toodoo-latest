@extends('layouts.default')
@section('content')
@inject('divwdth', 'App\Services\HomeDashboard')
<link rel="stylesheet" type="text/css" href="{{asset('css/highcharts.css')}}"></style>

@guest('customer')
<div class="row">
    <div class="col-sm-6">
        <div class="panel">
            <div class="panel-heading bg-system dark clearfix">
                <strong>{{translate('words.trial')}}</strong>
                <span class="pull-right">
                    <a href="{{url('/getExport/trial-active')}}" class="ml20 btn btn-success">
                        <i class="fa fa-print"></i>
                    </a>

                    <a href="{{url('/getExport/trial-inactive')}}" class="ml5 btn btn-danger">
                        <i class="fa fa-print"></i>
                    </a>
                </span>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="badge bg-system fs16 dark show-cmps" data-type = "trial-active">{{$trialActiveCompany}} / {{$trialActiveUser}}</span>
                        {{translate('words.active_companies_users')}}
                    </li>
                    <li class="list-group-item">
                        <span class="badge bg-system fs16 light show-cmps" data-type = "trial-inactive">{{$trialInactiveCompany}} / {{$trialInactiveUser}}</span>
                        {{translate('words.disabled_companies_users')}}
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel">
            <div class="panel-heading bg-system dark clearfix">
                <strong>{{translate('words.subscribed')}}</strong>
                <span class="pull-right">
                    <a href="{{url('/getExport/subscribed-active')}}" class="ml20 btn btn-success">
                        <i class="fa fa-print"></i>
                    </a>

                    <a href="{{url('/getExport/subscribed-inactive')}}" class="ml5 btn btn-danger">
                        <i class="fa fa-print"></i>
                    </a>
                </span>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="badge bg-system fs16 dark show-cmps" data-type = "subscribed-active">{{$subscribedActiveCompany}} / {{$subscribedActiveUser}}</span>
                        {{translate('words.active_companies_users')}}
                    </li>
                    <li class="list-group-item">
                        <span class="badge bg-system fs16 light show-cmps" data-type = "subscribed-inactive">{{$subscribedInactiveCompany}} / {{$subscribedInactiveUser}}</span>
                        {{translate('words.disabled_companies_users')}}
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center fs20 bg-grey">
        {{translate('words.monthly_recurrent_revenue')}} : {{$companyRevenue}} €
    </div>
</div>
@endauth
@auth('customer')
<div class="row">
    <div class="col-md-12">
        <label>&nbsp;</label>
        <div class="tab-block mb25">
            <ul class="nav nav-tabs" id="myTab">
                <li class="lang-links active">
                    <a href="#overview" data-toggle="tab">{{translate('words.cockpit')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->is_auditor == 1)
                <li class="lang-links">
                    <a href="#audit_planning" data-toggle="tab" id="planningTab">{{translate('words.audit_planning')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#issues" data-toggle="tab" id="auditIssues">
                        <img src="{{asset('image/too-doo-logo-tab.png')}}" height="12">
                    </a>
                </li>
                <li class="lang-links">
                    <a href="#audit_meetings" data-toggle="tab" id="auditMeeting">{{translate('words.audit_meetings')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                <li class="lang-links">
                    <a href="#procesverbeteringen" data-toggle="tab" id="procesIssues">{{translate('words.process_improvements')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#result" data-toggle="tab" id="auditResult">
                        {{translate('words.audit_results')}}
                    </a>
                </li>
                @if(Auth::guard('customer')->user()->company_view == 1)
                <li class="lang-links">
                    <a href="#company-view" data-toggle="tab" id="companyView">
                        {{translate('words.company_view')}}
                    </a>
                </li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane active">
                    <div id="loaderDv" style="display:none;"><i class="fa fa-cog fa-spin text-default fa-5x"></i></div>
                    <section id="content" class="table-layout animated fadeIn">
                        <div class="tray tray-center">
                            <div class="row">
                                <div class="{{$divwdth->divwdth()}} text-center">
                                    <div class="panel panel-tile br-a br-grey overview-segment-head active">
                                        <div class="panel-body overview-segment-check" id="cockpit-issues">
                                            <div class="fw700 fs20 mt5">{{$veryUrgent}} {{translate('words.issues')}}</div>
                                            {{translate('words.last_seven_days')}}
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="text-system fs12 openIssues">{{translate('words.view_details')}}</a>
                                </div>
                                @if(Auth::guard('customer')->user()->is_auditor == 1)
                                <div class="{{$divwdth->divwdth()}} text-center">
                                    <div class="panel panel-tile br-a br-grey overview-segment-head">
                                        <div class="panel-body overview-segment-check" id="cockpit-unplanned-audit">
                                            <div class="fw700 fs20 mt5">{{$unplannedAuditsNo}} {{translate('words.unplanned_audits')}}</div>
                                            {{translate('words.last_month')}}
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="text-system fs12 openPlaning">{{translate('words.view_details')}}</a>
                                </div>
                                @endif
                                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                                    <div class="{{$divwdth->divwdth()}} text-center">
                                        <div class="panel panel-tile br-a br-grey overview-segment-head">
                                            <div class="panel-body overview-segment-check" id="cockpit-insight-process">
                                                <div class="fw700 fs20 mt5">{{$internalPendingIssues->count()}} {{translate('words.new_insights_to_process')}}</div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0);" class="text-system fs12 procesIssuess">{{translate('words.view_details')}}</a>
                                    </div>
                                @endif
                                <div class="{{$divwdth->divwdth()}} text-center">
                                    <div class="panel panel-tile br-a br-grey overview-segment-head">
                                        <div class="panel-body overview-segment-check" id="cockpit-upcoming-meeting">
                                            <div class="fw700 fs20 mt5">{{$meetingPending}} {{translate('words.upcoming_meetings')}}</div>
                                            {{translate('words.next_seven_days')}}
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="text-system fs12 openMetting">{{translate('words.view_details')}}</a>
                                </div>
                            </div>
                            <div class="panel mt20">
                                <span id="segment-1" class="overview-segments">
                                    @if(!empty($urgentIssues))
                                    <div class="panel-heading">
                                        <span class="fs18 ">{{translate('words.top')}} 3 {{translate('words.issues')}}</span>
                                        <a href="javascript:void(0);"  class="text-system fs12 openIssues">{{translate('words.view_details')}}</a>
                                    </div>
                                    <div class="panel-body pn">
                                        <div class="table-responsive">
                                            <table class="table admin-form theme-warning tc-checkbox-1 fs13">
                                                <thead>
                                                    <tr class="bg-light">
                                                        <th class="text-center">{{translate('words.issue_title')}}</th>
                                                        <th class="">{{translate('words.process')}}</th>
                                                        <th class="">{{translate('words.type')}}</th>
                                                        <th class="">{{translate('words.source')}}</th>
                                                        <th class="urgencyHead">{{translate('words.urgency')}}</th>
                                                        <th class="statusHead">{{translate('words.status')}}</th>
                                                        <th class="statusHead">{{translate('words.next_action_to')}}</th>
                                                        <th class="">{{translate('words.action')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @inject('findPending', 'App\Services\HomeDashboard')
                                                    @inject('actionPending', 'App\Services\HomeDashboard')
                                                    
                                                    @foreach($urgentIssues as $each)
                                                    @if($loop->iteration <= 3)
                                                    @if(dayDifference($each->urgency) <= $urgencyCount)
                                                    
                                                    @if($findPending->findPending($each) !== false)
                                                    <tr>
                                                        <td class="">
                                                            @if($each->type == 'risk' || $each->type == 'opportunity')
                                                            {{getRiskOrOptTitle($each->issue_title, $each->type)}}
                                                            @elseif($each->type == 'context' || $each->type == 'stakeholder')
                                                            {{getContextStakeholderTitle($each->issue_title, $each->type)}}
                                                            @else
                                                            {{$each->issue_title}}
                                                            @endif
                                                        </td>
                                                        <td class="">{{$each->process}}</td>
                                                        <td class="">{{($each->type == 'internal' || $each->type == 'external' || $each->type == 'supplier-rating' || $each->type == 'corrective-action') ? Config::get('settings.audit_category')[$each->type] : $each->type }}</td>
                                                        <td class="">{{$each->sources}}</td>
                                                        <td class="">{!! dayCount($each->urgency) !!}</td>
                                                        <td>
                                                            @if($each->type != 'internal' || $each->type != 'external')
                                                            {{$langArr[$each->stats]}}
                                                            @else
                                                            {{$langArr[$actionPending->actionPending($each)]}}
                                                            @endif
                                                        </td>
                                                        <td>{{$each->next_action}}</td>
                                                        <td class="">
                                                            @if($each->type == 'internal' || $each->type == 'external')
                                                            <a href="{{url('/audit/action-by-member/' . $each->exec_id)}}" class="text-system fs12" title="{{translate('words.watch')}} {{$each->sources}}">
                                                                <i class="fa fa-arrow-circle-o-right fa-2x"></i>
                                                            </a>
                                                            @elseif($each->type=='risk' || $each->type=='opportunity')
                                                            <a href="javascript:void(0);" class="text-system fs12 riskOpportunityCls" title="{{translate('words.watch')}} {{$each->sources}}" data-riskoptid="{{$each->exec_id}}" data-processid="{{$each->process_id}}" data-type="{{$each->type}}">
                                                                <i class="fa fa-arrow-circle-o-right fa-2x"></i>
                                                            </a>
                                                            @elseif($each->type == 'context' || $each->type == 'stakeholder')
                                                            <a href="{{url('/context-stakeholder-fixing/'.$each->exec_id)}}" class="text-system fs12"><i class="fa fa-arrow-circle-o-right fa-2x"></i> </a>
                                                            @elseif($each->type == 'competent-rating')
                                                            <a href="{{url('/competent-rating-issue-fixing/'.$each->exec_id)}}" class="text-system fs12"><i class="fa fa-arrow-circle-o-right fa-2x"></i> </a>
                                                            @elseif($each->type == 'supplier-rating')
                                                            <a href="{{url('/supplier-rating-issue-fixing/'.$each->exec_id)}}" class="text-system fs12"><i class="fa fa-arrow-circle-o-right fa-2x"></i> </a>
                                                            @elseif($each->type == 'corrective-action')
                                                            <a href="{{url('/corrective-action-issue-fixing/'.$each->exec_id)}}" class="text-system fs12"><i class="fa fa-arrow-circle-o-right fa-2x"></i> </a>
                                                            @else
                                                            <a href="{{url('/audit/report-issue-fixing/'.$each->exec_id)}}" class="text-system fs12" title="{{translate('words.watch')}} {{$each->sources}}">
                                                                <i class="fa fa-arrow-circle-o-right fa-2x"></i>
                                                            </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endif
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @endif
                                    <br clear="all">
                                    <div class="panel-heading">
                                        <span class="fs18 ">{{translate('words.open_and_closed_issues')}}</span>
                                    </div>
                                    <div class="form-group mt25 disp-flex">
                                        <select class="form-control w200 filter upfilter" data-filter="open-closed-issues">
                                            <option value="{{date('Y-m-d', strtotime('-6 months'))}}">{{translate('words.last_six_months')}}</option>
                                            <option value="{{date('Y-m-d', strtotime('-3 months'))}}">{{translate('words.last_three_months')}}</option>
                                        </select>
                                        {!! Form::select('language', $issueCategories, 'all', ['class' => 'form-control w200 ml5 issuecategory', 'data-filter' => 'open-closed-issues']) !!}
                                    </div>
                                    <div>
                                        <canvas id="open-closed-issues-chart"></canvas>
                                    </div>

                                    <div class="form-group mt25">
                                        <select class="form-control w-a filter downfilter" data-filter="open-closed-issues-pie">
                                            <option value="{{date('Y-m-d', strtotime('-6 months'))}}">{{translate('words.last_six_months')}}</option>
                                            <option value="{{date('Y-m-d', strtotime('-3 months'))}}">{{translate('words.last_three_months')}}</option>
                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel">
                                                <div class="panel-heading exhight">{{translate('words.open_issues')}}</div>
                                                <div class="panel-body">
                                                    <canvas id="open-issues-pie-chart"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="panel">
                                                <div class="panel-heading exhight">{{translate('words.closed_issues')}}</div>
                                                <div class="panel-body">
                                                    <canvas id="closed-issues-pie-chart"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </span>
                                <span id="segment-2" class="overview-segments hide"> 
                                </span>
                                <span id="segment-3" class="overview-segments hide">  
                                </span>
                                <span id="segment-4" class="overview-segments hide">   
                                </span>
                            </div>
                        </div>
                    </section>
                </div>
                <div id="audit_planning" class="tab-pane"></div>
                <div id="issues" class="tab-pane"></div>
                <div id="audit_meetings" class="tab-pane"></div>
                <div id="procesverbeteringen" class="tab-pane"></div>
                <div id="result" class="tab-pane"></div>
                <div id="company-view" class="tab-pane"></div>
            </div>
        </div>
    </div>
</div>
@endauth
<div id="companyListModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header bg-system">
            &nbsp;
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="list-company">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{translate('started.close')}}</button>
        </div>
    </div>
  </div>
</div>
@include('includes.scripts.dashboard_script')
@stop
