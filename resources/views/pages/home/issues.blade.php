@extends('layouts.default')
@section('content')
@auth('customer')
<div class="row">
    <div class="col-md-12">
        <label>&nbsp;</label>
        <div class="tab-block mb25">
            <ul class="nav nav-tabs" id="myTab">
                <li class="lang-links">
                    <a href="#overview" data-toggle="tab" id="cockpitTab">{{translate('words.cockpit')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->is_auditor == 1)
                <li class="lang-links">
                    <a href="#audit_planning" data-toggle="tab" id="planningTab">{{translate('words.audit_planning')}}</a>
                </li>
                @endif
                <li class="lang-links active">
                    <a href="#issues" data-toggle="tab" id="auditIssues">
                    <img src="{{asset('image/too-doo-logo-tab.png')}}" height="12">
                    </a>
                </li>
                <li class="lang-links">
                    <a href="#audit_meetings" data-toggle="tab" id="auditMeeting">{{translate('words.audit_meetings')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                <li class="lang-links">
                    <a href="#procesverbeteringen" data-toggle="tab" id="procesIssues">{{translate('words.process_improvements')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#result" data-toggle="tab" id="auditResult">
                    {{translate('words.audit_results')}}
                    </a>
                </li>
                @if(Auth::guard('customer')->user()->company_view == 1)
                <li class="lang-links">
                    <a href="#company-view" data-toggle="tab" id="companyView">
                        {{translate('words.company_view')}}
                    </a>
                </li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane">
                </div>

                <div id="audit_planning" class="tab-pane">   
                </div>

                <div id="issues" class="tab-pane active">
                    <div class="mb5">
                        <span class="text-system fw700">
                            {{$veryUrgent + $lessUrgent}} {{translate('words.issues')}}
                        </span>
                        {{translate('words.assigned_to_me')}}
                    </div>
                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="datatable-dashboard" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="bg-light">
                                            <th class="text-center">{{translate('words.issue_title')}}</th>
                                            <th class="">{{translate('words.process')}}</th>
                                            <th class="">{{translate('words.type')}}</th>
                                            <th class="">{{translate('words.source')}}</th>
                                            <th width="160px" class="">{{translate('words.urgency')}}</th>
                                            <th width="140px" class="">{{translate('words.status')}}</th>
                                            <th width="140px" class="">{{translate('words.next_action_to')}}</th>
                                            <th class="">{{translate('words.action')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($urgentIssues))
                                        @inject('findPending', 'App\Services\HomeDashboard')
                                        @inject('actionPending', 'App\Services\HomeDashboard')
                                        @foreach($urgentIssues as $each)
                                        @if(dayDifference($each->urgency) <= $urgencyCount)
                                        @if($findPending->findPending($each) !== false)
                                        <tr>
                                            <td class="">
                                                @if($each->type == 'risk' || $each->type == 'opportunity')
                                                {{getRiskOrOptTitle($each->issue_title, $each->type)}}
                                                @elseif($each->type == 'context' || $each->type == 'stakeholder')
                                                {{getContextStakeholderTitle($each->issue_title, $each->type)}}
                                                @else
                                                {{$each->issue_title}}
                                                @endif</td>
                                                <td class="">{{$each->process}}</td>
                                                <td class="">{{($each->type == 'internal' || $each->type == 'external' || $each->type == 'supplier-rating' || $each->type == 'corrective-action') ? Config::get('settings.audit_category')[$each->type] : $each->type }}</td>
                                                <td class="">{{$each->sources}}</td>
                                                <td class="">{!! dayCount($each->urgency) !!}</td>
                                                <td>
                                                    @if($each->type != 'internal' || $each->type != 'external')
                                                    {{$langArr[$each->stats]}}
                                                    @else
                                                    {{$langArr[$actionPending->actionPending($each)]}}
                                                    @endif
                                                </td>
                                                <td>{{$each->next_action}}</td>
                                                <td class="">
                                                    @if($each->type == 'internal' || $each->type == 'external')
                                                    <a href="{{url('/audit/action-by-member/' . $each->exec_id)}}" class="text-system fs12" title="{{translate('words.watch')}} {{$each->sources}}">
                                                        <i class="fa fa-arrow-circle-o-right fa-2x"></i>
                                                    </a>
                                                    @elseif($each->type =='risk' || $each->type =='opportunity')
                                                    <a href="javascript:void(0);" class="text-system fs12 riskOpportunityCls" title="{{translate('words.watch')}} {{$each->sources}}" data-riskoptid="{{$each->exec_id}}" data-processid="{{$each->process_id}}" data-type="{{$each->type}}">
                                                        <i class="fa fa-arrow-circle-o-right fa-2x"></i>
                                                    </a>
                                                    @elseif($each->type == 'context' || $each->type == 'stakeholder')
                                                    <a href="{{url('/context-stakeholder-fixing/'.$each->exec_id)}}" class="text-system fs12"><i class="fa fa-arrow-circle-o-right fa-2x"></i> </a>
                                                    @elseif($each->type == 'competent-rating')
                                                    <a href="{{url('/competent-rating-issue-fixing/'.$each->exec_id)}}" class="text-system fs12"><i class="fa fa-arrow-circle-o-right fa-2x"></i> </a>
                                                    @elseif($each->type == 'supplier-rating')
                                                    <a href="{{url('/supplier-rating-issue-fixing/'.$each->exec_id)}}" class="text-system fs12"><i class="fa fa-arrow-circle-o-right fa-2x"></i> </a>
                                                    @elseif($each->type == 'corrective-action')
                                                    <a href="{{url('/corrective-action-issue-fixing/'.$each->exec_id)}}" class="text-system fs12"><i class="fa fa-arrow-circle-o-right fa-2x"></i> </a>
                                                    @else
                                                    <a href="{{url('/audit/report-issue-fixing/'.$each->exec_id)}}" class="text-system fs12" title="{{translate('words.watch')}} {{$each->sources}}">
                                                        <i class="fa fa-arrow-circle-o-right fa-2x"></i>
                                                    </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endif
                                            @endif
                                            @endforeach
                                            @endif

                                            @if(!empty($urgentIssues))
                                            @inject('findPending', 'App\Services\HomeDashboard')
                                            @foreach($urgentIssues as $each)
                                            @if(dayDifference($each->urgency) > $urgencyCount)
                                            @if($findPending->findPending($each) !== false)
                                            <tr>
                                                <td class="">
                                                    @if($each->type=='risk' || $each->type=='opportunity')
                                                    {{getRiskOrOptTitle($each->issue_title,$each->type)}}
                                                    @elseif($each->type == 'context' || $each->type == 'stakeholder')
                                                    {{getContextStakeholderTitle($each->issue_title, $each->type)}}
                                                    @else
                                                    {{$each->issue_title}}
                                                    @endif
                                                </td>
                                                <td class="">{{$each->process}}</td>
                                                <td class="">{{($each->type == 'internal' || $each->type == 'external' || $each->type == 'supplier-rating' || $each->type == 'corrective-action') ? Config::get('settings.audit_category')[$each->type] : $each->type }}</td>
                                                <td class="">{{$each->sources}}</td>
                                                <td class="">{!! dayCount($each->urgency) !!}</td>
                                                <td>
                                                    @if($each->type != 'internal' || $each->type != 'external')
                                                    {{$langArr[$each->stats]}}
                                                    @else
                                                    {{$langArr[$each->stats]}}
                                                    @endif
                                                </td>
                                                <td>{{$each->next_action}}</td>
                                                <td class="">
                                                    @if($each->type == 'internal' || $each->type == 'external')
                                                    <a href="{{url('/audit/action-by-member/'.$each->exec_id)}}" class="text-system fs12" title="{{translate('words.watch')}} {{$each->sources}}">
                                                        <i class="fa fa-arrow-circle-o-right fa-2x"></i>
                                                    </a>
                                                    @elseif($each->type == 'risk' || $each->type == 'opportunity')
                                                    <a href="javascript:void(0);" class="text-system fs12 riskOpportunityCls" title="{{translate('words.watch')}} {{$each->sources}}" data-riskoptid="{{$each->exec_id}}" data-processid="{{$each->process_id}}" data-type="{{$each->type}}">
                                                        <i class="fa fa-arrow-circle-o-right fa-2x"></i>
                                                    </a>
                                                    @elseif($each->type=='context' || $each->type=='stakeholder')
                                                    <a href="{{url('/context-stakeholder-fixing/'.$each->exec_id)}}" class="text-system fs12">
                                                        <i class="fa fa-arrow-circle-o-right fa-2x"></i>
                                                    </a>
                                                    @elseif($each->type == 'competent-rating')
                                                    <a href="{{url('/competent-rating-issue-fixing/'.$each->exec_id)}}" class="text-system fs12"><i class="fa fa-arrow-circle-o-right fa-2x"></i> </a>
                                                    @elseif($each->type == 'supplier-rating')
                                                    <a href="{{url('/supplier-rating-issue-fixing/'.$each->exec_id)}}" class="text-system fs12"><i class="fa fa-arrow-circle-o-right fa-2x"></i> </a>
                                                    @elseif($each->type == 'corrective-action')
                                                    <a href="{{url('/corrective-action-issue-fixing/'.$each->exec_id)}}" class="text-system fs12"><i class="fa fa-arrow-circle-o-right fa-2x"></i> </a>
                                                    @else
                                                    <a href="{{url('/audit/report-issue-fixing/'.$each->exec_id)}}" class="text-system fs12" title="{{translate('words.watch')}} {{$each->sources}}">
                                                        <i class="fa fa-arrow-circle-o-right fa-2x"></i>
                                                    </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endif
                                            @endif
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                </div>

                <div id="audit_meetings" class="tab-pane">
                </div>

                <div id="procesverbeteringen" class="tab-pane">
                </div>

                <div id="result" class="tab-pane">
                </div>

                <div id="company-view" class="tab-pane">
                </div>
            </div>
        </div>
    </div>
</div>
@endauth
@include('includes.scripts.dashboard_issues_script')
@stop
