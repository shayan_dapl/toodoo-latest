@extends('layouts.default')
@section('content')
@inject('divwdth', 'App\Services\HomeDashboard')
<link rel="stylesheet" type="text/css" href="{{asset('css/highcharts.css')}}"></style>
@auth('customer')
<div class="row">
    <div class="col-md-12">
        <label>&nbsp;</label>
        <div class="tab-block mb25">
            <ul class="nav nav-tabs" id="myTab">
                <li class="lang-links">
                    <a href="#overview" data-toggle="tab" id="cockpitTab">{{translate('words.cockpit')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->is_auditor == 1)
                <li class="lang-links">
                    <a href="#audit_planning" data-toggle="tab" id="planningTab">{{translate('words.audit_planning')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#issues" data-toggle="tab" id="auditIssues">
                    <img src="{{asset('image/too-doo-logo-tab.png')}}" height="12">
                    </a>
                </li>
                <li class="lang-links active">
                    <a href="#audit_meetings" data-toggle="tab" id="auditMeeting">{{translate('words.audit_meetings')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                <li class="lang-links">
                    <a href="#procesverbeteringen" data-toggle="tab" id="procesIssues">{{translate('words.process_improvements')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#result" data-toggle="tab" id="auditResult">
                    {{translate('words.audit_results')}}
                    </a>
                </li>
                @if(Auth::guard('customer')->user()->company_view == 1)
                <li class="lang-links">
                    <a href="#company-view" data-toggle="tab" id="companyView">
                        {{translate('words.company_view')}}
                    </a>
                </li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane">
                </div>

                <div id="audit_planning" class="tab-pane">   
                </div>

                <div id="issues" class="tab-pane"> 
                </div>

                <div id="audit_meetings" class="tab-pane active">
                    
                    @if($getauditorAndAuditees->count() > 0)
                    <span class="disp-flex">
                        <div class="checkbox-custom checkbox-system mb5">
                            <input id="pending" type="checkbox">
                            <label for="pending">{{translate('table.pending')}}</label>
                        </div>
                        <div class="checkbox-custom checkbox-system mb5">
                            <input id="closed" type="checkbox">
                            <label for="closed">{{translate('words.closed')}}</label>
                        </div>
                    </span>
                    @foreach($getauditorAndAuditees as $each)
                    @if($each->plan->plan_date != '')
                    <div class="row {{($each->auditStatus != 2) ? 'pending' : 'closed'}}">
                        <div class="col-md-5">
                            <span class="fw600">{{date('jS F Y',strtotime($each->audit_date))}}</span>
                                @if($each->auditStatus < 2)
                                <div class="panel pointer" id="p17" onclick="location.href ='{{url('/audit/audit-prepare/'.$each->audit_id)}}'">
                                @else
                                <div class="panel pointer" id="p17" onclick="location.href ='{{url('/audit/audit-execute/'.$each->audit_id)}}'">
                                @endif
                            <div class="panel-heading">
                                    <span class="panel-title">{{$each->plan->audit_name}}</span>
                                    <span class="panel-controls">
                                        @if(Auth::guard('customer')->user()->id==$each->lead_auditor_id)
                                        <span class="bg-warning dark pr10 pl10 text-default">{{translate('words.lead_auditor')}}</span>
                                        @else
                                        <span class="bg-warning dark pr10 pl10 text-default">{{$each->type}}</span>
                                        @endif
                                    </span>
                                </div>
                                <div class="panel-body pn">
                                    <div class="list-group list-group-links list-group-spacing-xs mbn">
                                        <span class="list-group-item"> {{translate('words.process')}} :
                                        @foreach($each->process as $eachprocess)
                                        <span class="bg-primary pr5 mr5 pl5 pull-right">{{$eachprocess}}</span>
                                         @endforeach </span>
                                        <span class="list-group-item"> {{translate('words.lead_auditor')}}: <b class="text-danger pull-right fs13">{{$each->lead_auditor}}</b> </span>
                                        <span class="list-group-item"> {{translate('words.status')}}: @if($each->auditStatus ==2)<b class="text-success pull-right fs13">{{translate('words.closed')}}</b>@else<b class="text-danger pull-right fs13">{{translate('table.pending')}}</b>@endif</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    @endif
                    @if($getauditorAndAuditees->count() == 0)
                    <div class="row">
                        <div class="col-md-3 col-md-offset-4 panel-heading text-center">
                            {{translate('table.no_data')}}
                        </div>
                    </div>
                    @endif
                </div>

                <div id="procesverbeteringen" class="tab-pane">
                </div>

                <div id="result" class="tab-pane">
                </div>

                <div id="company-view" class="tab-pane">
                </div>
            </div>
        </div>
    </div>
</div>
@endauth
@include('includes.scripts.dashboard_meeting_script')
@stop
