<table class="table table-bordered seperated">
    <thead>
        <tr class="bg-system">
            <td>{{ucfirst(translate('words.process'))}}</td>
            @foreach($monthsForMeetingCalender as $month)
            <td>{{$month}}</td>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($plannedAuditMeeting as $eachAudit)
        <tr>
            <td>{{$eachAudit['process']}}</td>
            @foreach($eachAudit['months'] as $eachMonth)
            <td class="{{$eachMonth['tdCls']}}">
                <a href="{{$eachMonth['link']}}" title="{{$eachMonth['title']}}" class="{{$eachMonth['aCls']}}">{{$eachMonth['auditDate']}}</a>
                @if($eachMonth['userType']!='')
                <br /><br /><span class="bg-success fs10 p2 pos-extended">{{$eachMonth['userType']}}</span>
                @endif
            </td>
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>