@extends('layouts.default')
@section('content')
@inject('divwdth', 'App\Services\HomeDashboard')
<link rel="stylesheet" type="text/css" href="{{asset('css/highcharts.css')}}"></style>

@guest('customer')
<div class="row">
    <div class="col-sm-6">
        <div class="panel">
            <div class="panel-heading bg-system dark">
                {{translate('words.total_active_companies')}} <b>[ {{$activeCompany}} ]</b>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel">
            <div class="panel-heading bg-system light">
                {{translate('words.total_inactive_companies')}} <b>[ {{$inactiveCompany}} ]</b>
            </div>
        </div>
    </div>
</div>
@endauth
@auth('customer')
<div class="row">
    <div class="col-md-12">
        <label>&nbsp;</label>
        <div class="tab-block mb25">
            <ul class="nav nav-tabs" id="myTab">
                <li class="lang-links">
                    <a href="#overview" data-toggle="tab" id="cockpitTab">{{translate('words.cockpit')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->is_auditor == 1)
                <li class="lang-links active">
                    <a href="#audit_planning" data-toggle="tab" id="planningTab">{{translate('words.audit_planning')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#issues" data-toggle="tab" id="auditIssues">
                    <img src="{{asset('image/too-doo-logo-tab.png')}}" height="12">
                    </a>
                </li>
                <li class="lang-links">
                    <a href="#audit_meetings" data-toggle="tab" id="auditMeeting">{{translate('words.audit_meetings')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                <li class="lang-links">
                    <a href="#procesverbeteringen" data-toggle="tab" id="procesIssues">{{translate('words.process_improvements')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#result" data-toggle="tab" id="auditResult">
                    {{translate('words.audit_results')}}
                    </a>
                </li>
                @if(Auth::guard('customer')->user()->company_view == 1)
                <li class="lang-links">
                    <a href="#company-view" data-toggle="tab" id="companyView">
                        {{translate('words.company_view')}}
                    </a>
                </li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane">
                </div>

                <div id="audit_planning" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-3">
                            <h3 class="text-center fw600 text-system">
                                {{translate('words.audits_to_schedule')}}
                            </h3>
                        </div>
                        <div class="col-md-3">
                            <h3 class="text-center fw600 text-system">
                                {{translate('words.audits_planned')}}
                            </h3>
                        </div>
                        <div class="col-md-3">
                            <h3 class="text-center fw600 text-system">
                                {{translate('words.audits_in_progress')}}
                            </h3>
                        </div>
                        <div class="col-md-3">
                            <h3 class="text-center fw600 text-system">
                                {{translate('words.audits_finalize')}}
                            </h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            @if($auditPlans->count() > 0)
                            @foreach($auditPlans as $each)
                            @if($each->plan_time == null)
                            <div class="panel" id="p17" >
                                <div class="panel-heading fs16">
                                    <span class="panel-title pointer" onclick="location.href ='{{url('/audit/audit-prepare/'.$each->id)}}'">{{$each->audit_name}}</span>
                                </div>
                                <div class="panel-body pn default pointer" onclick="location.href ='{{url('/audit/audit-prepare/'.$each->id)}}'">
                                    <div class="list-group list-group-links list-group-spacing-xs mbn">
                                        <p class="list-group-item pointer">
                                           {{translate('words.category')}} : 
                                          <span class="text-success pr5 pl5 pull-right">
                                             {{translate('words.'.$each->category)}}
                                          </span> 
                                        </p>
                                        <p class="list-group-item pointer">
                                           {{translate('words.branch')}} : 
                                          <span class="text-success pr5 pl5 pull-right">
                                             {{$each->branch->name}}
                                          </span> 
                                        </p>
                                        <p class="list-group-item pointer Detailbadge"> 
                                          {{translate('words.process')}} :  
                                          @foreach($each->process as $eachDetail)
                                            <span class="bg-primary multiprocs">
                                            {{$eachDetail->processDetail->name}} </span>
                                          @endforeach    
                                        </p>
                                        <p class="list-group-item pointer" > 
                                           {{translate('words.planned_for')}} : 
                                           <b class="text-success pull-right"> 
                                             {{$each->plan_month}}/{{$each->plan_year}} 
                                            </b> 
                                        </p>
                                        <p class="list-group-item pointer" > 
                                           {{translate('words.lead_auditor')}} :
                                             <b class="text-danger pull-right fs13">
                                              {{$each->lead_auditor->name}}
                                            </b> 
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                            @endif
                            @if($plannedAuditCount == 0 or $auditPlans->count() == 0)
                            <div class="panel" id="p17">
                                <div class="panel-heading text-center">
                                    {{translate('table.no_data')}}
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-3">
                            @if($auditPlans->count() > 0)
                            @foreach($auditPlans as $each)
                            @if($each->plan_date != null and $each->plan_time != null and $each->executions->count() == 0)
                            <div class="panel" id="p17" >
                                <div class="panel-heading fs16">
                                    <span class="panel-title pointer" onclick="location.href ='{{url('/audit/audit-execute/'.$each->id)}}'">{{$each->audit_name}}</span>
                                </div>
                                <div class="panel-body pn default pointer" onclick="location.href ='{{url('/audit/audit-execute/'.$each->id)}}'">
                                    <div class="list-group list-group-links list-group-spacing-xs mbn">
                                        <p class="list-group-item pointer">
                                         {{translate('words.category')}} : 
                                         <span class="text-success pr5 pl5 pull-right">
                                         {{translate('words.'.$each->category)}}
                                         </span> 
                                        </p>
                                        <p class="list-group-item pointer">
                                           {{translate('words.branch')}} : 
                                          <span class="text-success pr5 pl5 pull-right">
                                             {{$each->branch->name}}
                                          </span> 
                                        </p>
                                        <p class="list-group-item pointer Detailbadge">
                                         {{translate('words.process')}} : 
                                            @foreach($each->process as $eachDetail)
                                            <span class="bg-primary multiprocs">
                                            {{$eachDetail->processDetail->name}} 
                                            </span>
                                            @endforeach
                                        </p>
                                        <p class="list-group-item pointer"> 
                                        {{translate('words.planned_date')}}: 
                                        <b class="text-success pull-right "> 
                                        {{date('d-m-Y',strtotime($each->plan_date))}}</b>
                                        </p>
                                        <p class="list-group-item pointer">
                                         {{translate('words.lead_auditor')}}:
                                          <b class="text-danger pull-right fs13">
                                          {{$each->lead_auditor->name}}
                                          </b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                            @endif
                            @if($preparedAuditCount == 0 or $auditPlans->count() == 0)
                            <div class="panel" id="p17">
                                <div class="panel-heading text-center">
                                    {{translate('table.no_data')}}
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-3">
                            @if(!empty($notReleasedExecutedAudits))
                            @foreach($notReleasedExecutedAudits as $each)
                            <div class="panel" id="p17">
                                <div class="panel-heading fs16">
                                    <span class="panel-title pointer" onclick="location.href ='{{url('/audit/audit-execute/'.$each->id)}}'">{{$each->audit_name}}</span>
                                </div>
                                <div class="panel-body pn default pointer" onclick="location.href ='{{url('/audit/audit-execute/'.$each->id)}}'">
                                    <div class="list-group list-group-links list-group-spacing-xs mbn">
                                        <p class="list-group-item pointer">
                                         {{translate('words.category')}} :
                                          <span class="text-success pr5 pl5 pull-right">
                                          {{translate('words.'.$each->category)}}
                                          </span>
                                        </p>
                                        <p class="list-group-item pointer">
                                           {{translate('words.branch')}} : 
                                          <span class="text-success pr5 pl5 pull-right">
                                             {{$each->branch->name}}
                                          </span> 
                                        </p>
                                        <p class="list-group-item pointer Detailbadge">
                                         {{translate('words.process')}} :
                                            @foreach($each->process as $eachDetail)
                                            <span class="bg-primary multiprocs">
                                            {{$eachDetail->processDetail->name}}
                                            </span>
                                            @endforeach
                                        </p>
                                        <p class="list-group-item pointer">
                                         {{translate('words.planned_date')}}:
                                          <b class="text-success pull-right">
                                           {{date('d-m-Y',strtotime($each->plan_date))}}
                                          </b>
                                        </p>
                                        <p class="list-group-item pointer">
                                         {{translate('words.lead_auditor')}}:
                                          <b class="text-danger pull-right fs13">
                                          {{$each->lead_auditor->name}}</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="panel" id="p17">
                                <div class="panel-heading text-center">
                                    {{translate('table.no_data')}}
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-3">
                            @if($releasedExecutedAudits->count() > 0)
                            @foreach($releasedExecutedAudits as $each)
                            <div class="panel" id="p17">
                                <div class="panel-heading fs16">
                                    <span class="panel-title pointer" onclick="location.href ='{{url('/audit/audit-execute/'.$each->id)}}'">{{$each->audit_name}}</span>
                                    <span class ="pull-right prntcls">
                                    <a href="{{url('/audit/report-print/'.$each->id)}}" class="btn btn-system ml5">
                                        <i class="fa fa-print"></i>
                                    </a>
                                    </span>
                                </div>
                                <div class="panel-body pn default pointer" onclick="location.href ='{{url('/audit/audit-execute/'.$each->id)}}'">
                                    <div class="list-group list-group-links list-group-spacing-xs mbn">
                                        <p class="list-group-item pointer">
                                         {{translate('words.category')}} :
                                          <span class="text-success pr5 pl5 pull-right">
                                          {{translate('words.'.$each->category)}}
                                          </span>
                                        </p>
                                        <p class="list-group-item pointer">
                                           {{translate('words.branch')}} : 
                                          <span class="text-success pr5 pl5 pull-right">
                                             {{$each->branch->name}}
                                          </span> 
                                        </p>
                                        <p class="list-group-item pointer">
                                         {{translate('words.process')}} :
                                            @foreach($each->process as $eachDetail)
                                            <span class="bg-primary multiprocs">
                                            {{$eachDetail->processDetail->name}}
                                            </span>
                                            @endforeach
                                        </p>
                                        <p class="list-group-item pointer">
                                         {{translate('words.planned_date')}}:
                                          <b class="text-success pull-right">
                                           {{date('d-m-Y',strtotime($each->plan_date))}}
                                          </b>
                                        </p>
                                        <p class="list-group-item pointer">
                                         {{translate('words.release_date')}}:
                                          <b class="text-success pull-right">
                                           {{date('d-m-Y',strtotime($each->release_date))}}
                                          </b>
                                        </p>
                                        <p class="list-group-item pointer">
                                         {{translate('words.lead_auditor')}}: 
                                         <b class="text-danger pull-right fs13">
                                         {{$each->lead_auditor->name}}
                                         </b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="panel" id="p17">
                                <div class="panel-heading text-center">
                                    {{translate('table.no_data')}}
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-heading exhight">
                                <div class="panel-title hidden-xs text-center">
                                    <h3>{{translate('words.your_audit_planning')}} - {{translate('words.internal')}}</h3>
                                </div>
                            </div>
                            @if(!empty($plannedAudit))
                            <div class="panel-body pn">
                                <div class="head clearfix">
                                    <div class="period">
                                        <a href="{{url('/home/planning/'.$prvYear)}}" class="prev w-a"><span>&lt; {{translate('words.prev')}}</span></a>
                                        <span class="month">{{ $curYearPlanningTab }}</span>
                                        <a href="{{url('/home/planning/'.$nxtYear)}}" class="next w-a"><span>{{translate('words.next')}} &gt;</span></a>
                                    </div>
                                </div>
                                <div class="calendar table-responsive">
                                    <table class="table table-bordered seperated">
                                        <thead>
                                            <tr class="bg-system">
                                                <td>{{ucfirst(translate('words.process'))}}</td>
                                                @foreach($monthsForCalender as $month)
                                                <td>{{$month}}</td>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($plannedAudit as $eachAudit)
                                            <tr>
                                                <td>{{$eachAudit['process']}}</td>
                                                @foreach($eachAudit['months'] as $eachMonth)
                                                <td class="{{$eachMonth['tdCls']}}">
                                                    <a href="{{$eachMonth['link']}}" title="{{$eachMonth['title']}}" class="{{$eachMonth['aCls']}}">{{$eachMonth['auditDate']}}</a>
                                                    @if($eachMonth['userType']!='')
                                                    <br /><br /><span class="bg-system text-deafult dark fs10 p2 pos-extended">{{$eachMonth['userType']}}</span>
                                                    @endif
                                                </td>
                                                @endforeach
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @elseif(Auth::guard('customer')->user()->type == 2)
                            <div class="col-md-4 col-md-offset-4 mt20">
                            <div class="panel" id="p17">
                                <div class="panel-heading text-center ml10">
                                    {{translate('words.define_audit_plan')}}
                                </div>
                            </div>
                            </div>
                            @elseif(Auth::guard('customer')->user()->type == 3)
                            <div class="col-md-4 col-md-offset-4 mt20">
                            <div class="panel" id="p17">
                                <div class="panel-heading text-center ml10">
                                   {{translate('words.no_calender_available')}}
                                </div>
                            </div>
                            </div>   
                            @endif
                        </div>
                    </div>

                     <div class="row mt20">
                        <div class="col-md-12">
                            <div class="panel-heading exhight">
                                <div class="panel-title hidden-xs text-center">
                                    <h3>{{translate('words.your_audit_planning')}} - {{translate('words.external')}}</h3>
                                </div>
                            </div>
                            @if(!empty($plannedExternalAudit))
                            <div class="panel-body pn">
                                <div class="head clearfix">
                                    <div class="period">
                                        <a href="{{url('/home/planning/'.$prvYear)}}" class="prev w-a"><span>&lt; {{translate('words.prev')}}</span></a>
                                        <span class="month">{{ $curYearPlanningTab }}</span>
                                        <a href="{{url('/home/planning/'.$nxtYear)}}" class="next w-a"><span>{{translate('words.next')}} &gt;</span></a>
                                    </div>
                                </div>
                                <div class="calendar table-responsive">
                                    <table class="table table-bordered seperated">
                                        <thead>
                                            <tr class="bg-system">
                                                <td>{{ucfirst(translate('words.process'))}}</td>
                                                @foreach($monthsForCalender as $month)
                                                <td>{{$month}}</td>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($plannedExternalAudit as $eachAudit)
                                            <tr>
                                                <td>{{$eachAudit['process']}}</td>
                                                @foreach($eachAudit['months'] as $eachMonth)
                                                <td class="{{$eachMonth['tdCls']}}">
                                                    <a href="{{$eachMonth['link']}}" title="{{$eachMonth['title']}}" class="{{$eachMonth['aCls']}}">{{$eachMonth['auditDate']}}</a>
                                                    @if($eachMonth['userType']!='')
                                                    <br /><br /><span class="bg-system text-deafult dark fs10 p2 pos-extended">{{$eachMonth['userType']}}</span>
                                                    @endif
                                                </td>
                                                @endforeach
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @elseif(Auth::guard('customer')->user()->type == 2)
                            <div class="col-md-4 col-md-offset-4 mt20">
                            <div class="panel" id="p17">
                                <div class="panel-heading text-center ml10">
                                    {{translate('words.define_audit_plan')}}
                                </div>
                            </div>
                            </div>
                            @elseif(Auth::guard('customer')->user()->type == 3)
                            <div class="col-md-4 col-md-offset-4 mt20">
                            <div class="panel" id="p17">
                                <div class="panel-heading text-center ml10">
                                   {{translate('words.no_calender_available')}}
                                </div>
                            </div>
                            </div>   
                            @endif
                        </div>
                    </div> 
                    
                </div>

                <div id="issues" class="tab-pane">
                </div>

                <div id="audit_meetings" class="tab-pane">
                </div>

                <div id="procesverbeteringen" class="tab-pane">
                </div>

                <div id="result" class="tab-pane"></div>

                <div id="company-view" class="tab-pane"></div>
            </div>
        </div>
    </div>
</td>
@endauth

<div id="issue-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header back4d text-default">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body justified"></div>
        </div>
    </div>
</div>

<div id="get-started-popup" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-default">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body justified text-center" id="get-started-popup-body">
            </div>
        </div>
    </div>
</div>

@include('includes.scripts.dashboard_planning_script')
@stop
