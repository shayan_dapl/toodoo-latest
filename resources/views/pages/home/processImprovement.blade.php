@extends('layouts.default')
@section('content')
@auth('customer')
@inject('divwdth', 'App\Services\HomeDashboard')
<div class="row">
    <div class="col-md-12">
        <label>&nbsp;</label>
        <div class="tab-block mb25">
            <ul class="nav nav-tabs" id="myTab">
                <li class="lang-links">
                    <a href="#overview" data-toggle="tab" id="cockpitTab">{{translate('words.cockpit')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->is_auditor == 1)
                <li class="lang-links">
                    <a href="#audit_planning" data-toggle="tab" id="planningTab">{{translate('words.audit_planning')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#issues" data-toggle="tab" id="auditIssues">
                    <img src="{{asset('image/too-doo-logo-tab.png')}}" height="12">
                    </a>
                </li>
                <li class="lang-links">
                    <a href="#audit_meetings" data-toggle="tab" id="auditMeeting">{{translate('words.audit_meetings')}}</a>
                </li>
                @if(Auth::guard('customer')->user()->can_process_owner == 1)
                <li class="lang-links active">
                    <a href="#procesverbeteringen" data-toggle="tab" id="procesIssues">{{translate('words.process_improvements')}}</a>
                </li>
                @endif
                <li class="lang-links">
                    <a href="#result" data-toggle="tab" id="auditResult">
                    {{translate('words.audit_results')}}
                    </a>
                </li>
                @if(Auth::guard('customer')->user()->company_view == 1)
                <li class="lang-links">
                    <a href="#company-view" data-toggle="tab" id="companyView">
                        {{translate('words.company_view')}}
                    </a>
                </li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane">
                </div>

                <div id="audit_planning" class="tab-pane">   
                </div>

                <div id="issues" class="tab-pane"> 
                </div>

                <div id="audit_meetings" class="tab-pane"> 
                </div>

                <div id="procesverbeteringen" class="tab-pane active">
                    @if(!empty($pendingIssues))
                    <span class="disp-flex">
                        <div class="checkbox-custom checkbox-system mb5">
                            <input id="not_started" type="checkbox">
                            <label for="not_started">{{translate('table.not_started')}}</label>
                        </div>
                        <div class="checkbox-custom checkbox-system mb5">
                            <input id="ongoing" type="checkbox">
                            <label for="ongoing">{{translate('table.on_going')}}</label>
                        </div>
                        <div class="checkbox-custom checkbox-system mb5">
                            <input id="closed" type="checkbox">
                            <label for="closed">{{translate('words.closed')}}</label>
                        </div>
                    </span>
                    @foreach($pendingIssues as $k => $each)
                    </br>
                    <div class="panel-menu p12 admin-form theme-primary">
                        <div class="row">
                            <div class="col-md-12 fw600">
                                {{$k}}
                            </div>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        @inject('clauses', 'App\Services\HomeDashboard')
                        @foreach($each as $eachLine)
                        <div class="col-md-4 {{(delegationStatus($eachLine) == 0) ? 'not_started' : ((delegationStatus($eachLine) == 1) ? 'ongoing' : 'closed')}}">
                            <div class="panel p5" id="p17">
                                <div class="panel-heading ui-sortable-handle fs13" onclick="location.href ='{{url('/audit/report-issue-action/'.$eachLine->id.'/exec-issue')}}'">
                                    <span class="panel-title">{{$eachLine->finding_type}}</span>
                                    <span class="blockStatus fw600 fs12 pull-right">
                                        @if(delegationStatus($eachLine) == 0)
                                        <span class="text-danger">{{translate('table.not_started')}} </span>
                                        @elseif(delegationStatus($eachLine) == 1)
                                        <span class="text-danger">{{translate('table.on_going')}} </span>
                                        @else
                                        <span class="text-success">{{translate('words.closed')}}</span>
                                        @endif
                                    <span>
                                </div>
                                <div class="panel-body pn pointer" onclick="location.href ='{{url('/audit/report-issue-action/'.$eachLine->id.'/exec-issue')}}'">
                                    <div class="list-group list-group-links list-group-spacing-xs mbn defultCusrsor">
                                        <p class="list-group-item">
                                            {{translate('form.remark')}} : {{$eachLine->observation}}
                                        </p>
                                        <p class="list-group-item">
                                            {{translate('words.process_improvements')}} : <span class="bg-primary pr5 pl5 pull-right">{{$eachLine->process}}</span>
                                        </p>
                                        {!! delegationDetails($eachLine->id, $clauses->clauses($eachLine)) !!}
                                        <p class="list-group-item"> {{translate('words.responsible')}} : <b class="text-system light pull-right fs13">{{$eachLine->responsible}}</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                    @else
                    <div class="row">
                        <div class="col-md-3 col-md-offset-4 panel-heading text-center">
                            {{translate('table.no_data')}}
                        </div>
                    </div>
                    @endif
                </div>

                <div id="result" class="tab-pane">
                </div>

                <div id="company-view" class="tab-pane">
                </div>
            </div>
        </div>
    </div>
</div>
@endauth
@include('includes.scripts.dashboard_issues_script')
@stop
