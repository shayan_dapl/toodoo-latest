@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(['method' => 'post', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) !!}
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.personal_details')}}</span>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2 mt40">
                    <div class="section">
                    </div>
                </div>
                <div class="col-md-4">
                    @if(\File::isFile('storage/company/'.$details->company_id.'/image/users/'.$details->photo))
                        {{ Form::image(asset('storage/company/'.$details->company_id.'/image/users/'.$details->photo.'?'.rand(0,9)), 'User Image', ['class' => 'thumbnail pull-right', 'id' => 'user_preview', 'height' => '100', 'width' => '100', 'disabled' => 'disabled']) }}
                    @else
                        {{ Form::image(asset('image/placeholder.png'), translate('form.photo'), ['class' => 'thumbnail pull-right', 'id' => 'user_preview', 'height' => '100', 'width' => '100', 'disabled' => 'disabled']) }}
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('email', translate('form.email'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('email', (isset($details->email)) ? $details->email : old('email'), ['class' => 'gui-input', 'id' => 'email', 'placeholder' => translate('form.email'), 'required' => 'required', 'readonly' => 'readonly']) !!}
                            <label for="email" class="field-icon">
                                <i class="fa fa-envelope"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('email')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('user_photo', translate('form.picture'), ['class' => 'field-label fs15 mb5']) !!}
                        <label class="field prepend-icon append-button file">
                            <span class="button">{{translate('form.file')}}</span>
                            {!! Form::file('photo', ['class' => 'gui-file', 'id' => 'user_photo', 'accept' => '.jpeg,.jpg,.png']) !!}
                            {!! Form::text('', '', ['class' => 'gui-input', 'id' => 'uploader1', 'placeholder' => translate('form.image')]) !!}
                            <label class="field-icon">
                                <i class="fa fa-upload"></i>
                            </label>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('fname', translate('form.first_name'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('fname', isset($details->fname) ? $details->fname : old('fname'), ['class' => 'gui-input', 'id' => 'fname', 'placeholder' => translate('form.first_name'), 'required' => 'required', 'tabindex' => '1']) !!}
                            <label for="fname " class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('fname')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('lname', translate('form.last_name'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field append-icon">
                            {!! Form::text('lname', isset($details->lname) ? $details->lname : old('lname'), ['class' => 'gui-input', 'id' => 'lname', 'placeholder' => translate('form.first_name'), 'required' => 'required', 'tabindex' => '2']) !!}
                            <label for="lname" class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('lname')}}</p>
                    </div>
                </div>
            </div>

            @if(Auth::guard('customer')->check() and !empty($company->subscription_id))
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('street', translate('form.street'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('street', isset($company->street) ? $company->street : old('street'), ['class' => 'gui-input', 'id' => 'address_line1', 'placeholder' => translate('form.street'), 'required' => 'required', 'tabindex' => '3']) !!}
                            <label for="street" class="field-icon">
                                <i class="fa fa-map-marker"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('street')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('nr', translate('form.nr'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field append-icon">
                            {!! Form::text('nr', isset($company->nr) ? $company->nr : old('nr'), ['class' => 'gui-input', 'id' => 'address_line2', 'placeholder' => translate('form.nr'), 'required' => 'required', 'tabindex' => '4']) !!}
                            <label for="nr" class="field-icon">
                                <i class="fa fa-map-marker"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('nr')}}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('bus', translate('form.bus'), ['class' => 'field-label fs15 mb5']) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('bus', isset($company->bus) ? $company->bus : old('bus'), ['class' => 'gui-input', 'id' => 'bus', 'placeholder' => translate('form.bus'), 'tabindex' => '5']) !!}
                            <label for="bus" class="field-icon">
                                <i class="fa fa-map-marker"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('bus')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('zip', translate('form.zip'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field append-icon">
                            {!! Form::text('zip', isset($company->zip) ? $company->zip : old('zip'), ['class' => 'gui-input', 'id' => 'zip', 'placeholder' => translate('form.zip'), 'required' => 'required', 'tabindex' => '6']) !!}
                            <label for="zip" class="field-icon">
                                <i class="fa fa-map-marker"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('zip')}}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('city', translate('form.city'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field append-icon">
                            {!! Form::text('city', isset($company->city) ? $company->city : old('city'), ['class' => 'gui-input', 'id' => 'city', 'placeholder' => translate('form.city'), 'tabindex' => '7']) !!}
                            <label for="city" class="field-icon">
                                <i class="fa fa-map-marker"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('city')}}</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('country', translate('form.country'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field select">
                            {!! Form::select('country', $countries, $company->country, ['id' => 'country', 'tabindex' => '8']) !!}
                            <i class="arrow"></i>
                        </label>
                        <p class="text-danger mn">{{$errors->first('country')}}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('language', translate('form.language'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field select">
                            {!! Form::select('language', $language, $details->language, ['id' => 'language', 'tabindex' => '9']) !!}
                            <i class="arrow"></i>
                        </label>
                        <p class="text-danger mn">{{$errors->first('language')}}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <label class="switch-extended switch-success">
                        <input type="checkbox" name="can_process_owner" value="1" @if($details->can_process_owner == 1) checked @endif>
                        <div class="slider-extended round"></div>
                    </label>
                    <span class="lbl fs16 pos-relative top11">{{translate('form.process_owner')}}</span>
                </div>
                <div class="col-md-4">
                    @if($details->no_email == 0)
                        <span id="is-auditor">
                            <label class="switch-extended switch-success">
                                {!! Form::checkbox('is_auditor', 1, $details->is_auditor == 1, ['id' => 'is_auditor']) !!}
                                <div class="slider-extended round"></div>
                            </label>
                            <span class="lbl switch-custom-pos">{{translate('form.is_auditor')}}</span> 
                        </span>
                    @endif
                </div>
                <div class="col-md-4 col-md-offset-2"></div>
            </div>
            @endif

            @if(Auth::guard('customer')->check() and $details->type == 2)
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.company_details')}}</span>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('name', translate('form.company_name'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('name', isset($company->name) ? $company->name : old('name'), ['class' => 'gui-input', 'id' => 'name', 'placeholder' => translate('form.company_name'), 'required' => 'required', 'tabindex' => '10']) !!}
                            <label for="name" class="field-icon">
                                <i class="fa fa-bank"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('name')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    @if(!empty($company->subscription_id))
                    <div class="section">
                        {!! Form::label('tva_no', translate('form.company_tva'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field append-icon">
                            {!! Form::text('tva_no', isset($company->tva_no) ? $company->tva_no : old('tva_no'), ['class' => 'gui-input', 'id' => 'tva_no', 'placeholder' => translate('form.company_tva'), 'required' => 'required', 'readonly' => 'readonly', 'tabindex' => '11']) !!}
                            <label for="tva_no" class="field-icon">
                                <i class="fa fa-bank"></i>
                            </label>
                        </label>
                        <p class="text-danger mn">{{$errors->first('tva_no')}}</p>
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    {!! Form::label('company_photo', translate('form.company_picture'), ['class' => 'field-label fs15 mb5']) !!}
                    <label class="field prepend-icon append-button file">
                        <span class="button">{{translate('form.file')}}</span>
                        {!! Form::file('company_photo', ['class' => 'gui-file', 'id' => 'company_photo', 'accept' => '.jpeg,.jpg,.png']) !!}
                        {!! Form::text('', '', ['class' => 'gui-input', 'id' => 'uploader2', 'placeholder' => translate('form.image')]) !!}
                        <label class="field-icon">
                            <i class="fa fa-upload"></i>
                        </label>
                    </label>
                </div>
                <div class="col-md-4">
                    @if(\File::isFile('storage/company/'.$company->id.'/image/'.$company->photo))
                        {{ Html::image(asset('storage/company/'.$company->id.'/image/'.$company->photo), translate('form.company_picture'), ['class' => 'thumbnail pull-right', 'id' => 'company_preview', 'height' => '100', 'width' => '100']) }}
                    @else
                        {{ Html::image(asset('/image/placeholder.png'), translate('form.company_picture'), array('class' => 'thumbnail pull-right', 'id' => 'company_preview', 'height' => '100', 'width' => '100')) }}
                    @endif
                </div>
            </div>
            @endif
            @if(Auth::guard('customer')->check() and Auth::guard('customer')->user()->type == 2)
                @if($orgStructExistance == 0)
                    <h1 class="text-center">{{translate('form.org_struct_exists')}}</h1>
                @else
                    <span id="position-list" class="for-hidefunction">
                        @if(isset($details) and $details->positions()->count() > 0)
                            @foreach($details->positions() as $eachFn)
                                @if (!empty($eachFn->position))
                                <div class="row repeter">
                                    <div class="col-sm-5">
                                        <h3>{{$eachFn->position->name_with_details}}</h3>
                                    </div>
                                    <div class="col-sm-6 col-xs-10">
                                        <h3>
                                            @if (!empty($eachFn->parentName($eachFn->position->parent_id)))
                                                {{$eachFn->parentName($eachFn->position->parent_id)->user->name}} 
                                                ({{$eachFn->parentName($eachFn->position->parent_id)->position->name}})
                                            @else
                                                {{translate('form.report_none')}}
                                            @endif
                                        </h3>
                                    </div>
                                    <div class="col-sm-1 col-xs-2">
                                        <button type="button" class="btn btn-danger btn-sm functions-del" data-id="{{$eachFn->id}}">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                        @endif

                        <div class="row repeter {{(isset($details) and $details->is_special_user == 1) ? 'hide' : ''}}" id="output-1">
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <label class="field select">
                                    {!! Form::select('functions[position][]', $positions, '', ['id' => 'position-1', 'data-id' => '1', 'class' => 'position']) !!}
                                    <i class="arrow"></i>
                                </label>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-10">
                                <h3 id="holdername-1">{{translate('form.parent_position')}}</h3>
                                {!! Form::hidden('functions[parent][]', '0', ['id' => 'parentpos-1']) !!}
                            </div>
                            <div class="col-md-1 col-sm-1 col-xs-2">
                                <button type="button" class="btn btn-system functions" data-row="1" data-id="">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </span>
                @endif
            @endif
            <div class="section-divider mv40" id="spy2">
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    {!! Form::hidden('', '0', array('id' => 'org-pos-activate')) !!}
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', ($details->id) ? $details->id : '', array('id' => 'id')) !!}
                    {!! Html::link(url('/home'), translate('form.cancel'), ['class' => 'btn btn-system']) !!}
                    {!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-system', 'id' => 'profile-btn']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('includes.scripts.profile_script')
@stop