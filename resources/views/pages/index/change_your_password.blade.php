@extends('layouts.main')
@section('content')
{!! Html::style('css/extra-styles/index_change_password.css') !!}
<div class="panel-body bg-light p30" id="passwordresetpassword">
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(array('method' => 'post', 'autocomplete' => 'off')) !!}
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('form.like_to_change_password')}}</span>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        <label for="password" class="field-label fs15 mb5">
                            {{translate('form.password')}}
                            <i class="fa fa-info-circle text-danger pointer" data-container="body" data-toggle="popover" data-placement="top" data-content="{{translate('words.password_strength')}}" data-original-title="" title=""></i>
                        </label>
                        <label class="field">
                            <input v-bind:type="passtype" name="password" class="gui-input" id="new-password" placeholder="{{translate('form.create_password')}}" v-model="password" v-on:keyup="passStrength()" value="">
                             <label class="field-icon" v-on:click="showHide('password')">
                                <i v-bind:class="passeye"></i>
                            </label>
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('password')}}</p>
                        @endif
                        <div class="progress mt10" v-if="password != ''" v-cloak>
                            <div v-bind:class="passclass" role="progressbar" v-bind:aria-valuenow="passval" aria-valuemin="0" aria-valuemax="100">@{{passval}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        <label for="password_confirmation" class="field-label fs15 mb5">{{translate('form.confirm_password')}}</label>
                        <label for="conf-password" class="field">
                            <input v-bind:type="confpasstype" name="password_confirmation" class="gui-input" id="conf-password" placeholder="{{translate('form.conf_password')}}" v-model="confpassword" v-on:keyup="passMatch()">
                            <label for="email" class="field-icon" v-on:click="showHide('confpassword')">
                                <i v-bind:class="confpasseye"></i>
                            </label>
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('password_confirmation')}}</p>
                        @endif
                        <div class="progress mt10" v-if="confpassword != ''" v-cloak>
                            <div v-bind:class="confpassclass" role="progressbar" v-bind:aria-valuenow="confpassval" aria-valuemin="0" aria-valuemax="100">@{{confpasstext}}</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-divider mv40" id="spy2">
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', Auth::guard('customer')->user()->email) !!}
                    {!! Form::submit(translate('form.update'), array('class' => 'btn btn-hover btn-system', 'tabindex' => '3',':disabled' => '!enable')) !!}
                    {!! Html::link(url('/home'), translate('form.skip'), array('class' => 'btn btn-hover btn-system', 'tabindex' => '4')) !!}
                </div>
            </div> 
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('includes.scripts.password_script')
@stop