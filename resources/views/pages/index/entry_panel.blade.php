@extends('layouts.main')
@section('content')
{!! Html::style('css/extra-styles/index_entry_panel.css') !!}
<div class="panel-body bg-light p30">
    <div class="row">
        @foreach($companies as $company)
        <div class="col-sm-4 text-center">
            <div class="panel panel-tile br-a br-grey bg-system company-names" onclick="window.location ='{{url('entry-panel/'.$company->id)}}'">
                <div class="panel-body">
                    <a href="javascript:void(0);" class="fw300 fs28 mt5 text-default">
                        {{ $company->name }}
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@stop