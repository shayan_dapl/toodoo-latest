@extends('layouts.main')
@section('content')
{!! Form::open(['method' => 'post']) !!}
<div class="panel-body p15 pt25">
    <div class="alert alert-micro alert-border-left alert-info pastel alert-dismissable mn">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="fa fa-info pr10"></i> {{translate('words.fgt_pwd_msg')}}
    </div>
</div>
<div class="panel-footer p25 pv15">
    <div class="section mn" style="display: inline-flex; width: 100%;">
        <div class="smart-widget sm-right smr-80" style="float: left; width: 90%;">
            <label for="email" class="field prepend-icon">
                {!! Form::input('email', 'email', '', ['class' => 'gui-input', 'id'=>'email', 'required' => 'required', 'placeholder' => translate('words.email'), 'tabindex' => '1']) !!}
                <label for="email" class="field-icon">
                    <i class="fa fa-envelope-o"></i>
                </label>
            </label>
            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
            {!! Form::button(translate('words.send'), array('class' => 'button', 'id' => 'chgPassword')) !!}
        </div>
        &nbsp;{!! Html::link(url('/'), translate('form.sign_in'), array('class' => 'btn p10 text-system')) !!}
    </div>
</div>
{!! Form::close() !!}
@stop