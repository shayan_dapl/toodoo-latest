@extends('layouts.main')
@section('content')
{!! Form::open(['method' => 'post', 'url' => '#']) !!}
<div class="panel-body bg-light p30">
    <div class="row">
        <div class="col-sm-7 pr30">
            <div class="section">
                {!! Form::label('username', translate('form.login_username'), ['class' => 'field-label text-muted fs18 mb10']) !!}
                <label for="username" class="field prepend-icon">
                    {!! Form::text('username', '', ['class' => 'gui-input', 'id'=>'username', 'required' => 'required', 'placeholder' => translate('form.enter_username'), 'tabindex' => '1']) !!}
                    <label for="username" class="field-icon">
                        <i class="fa fa-user"></i>
                    </label>
                </label>
                <p class="text-danger mn">{{$errors->first('username')}}</p>
            </div>
            <div class="section">
                {!! Form::label('password', translate('form.login_password'), ['class' => 'field-label text-muted fs18 mb10']) !!}
                <label for="password" class="field prepend-icon">
                    {!! Form::input('password', 'password', '', array('class' => 'gui-input', 'id' => 'password', 'placeholder' => translate('form.enter_password'), 'required' => 'required', 'tabindex' => '2', 'minlength' => 6)) !!}
                    <label for="password" class="field-icon">
                        <i class="fa fa-lock"></i>
                    </label>
                </label>
                <p class="text-danger mn">{{$errors->first('password')}}</p>
            </div>
        </div>
        <div class="col-sm-5 br-l br-grey pl30">
            {!! Form::image(asset('image/too-doo-logo.png'), 'User Image', array('class' => 'img-responsive', 'alt' => 'Site Logo')) !!}
            <br clear="all">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
            @endif
            @if(Session::has('faliure'))
            <div class="alert alert-danger">
                {{Session::get('faliure')}}
            </div>
            @endif
        </div>
    </div>
</div>
<div class="panel-footer clearfix p10 ph15">
    {!! csrf_field() !!}
    {!! Form::submit(translate('form.sign_in'), array('class' => 'button btn-system mr10 pull-right', 'tabindex' => '3')) !!}
    <br/>
    {!! Html::link(url('/password/reset'), translate('form.forgot_password'), array('tabindex' => '3')) !!}
</div>
{!! Form::close() !!}
@stop