@extends('layouts.main')
	@section('content')
	<div class="row p50">
		<div class="col-md-12 text-center">
			<h3>{{translate('words.want_to_prevent')}}</h3>
			{!! Form::open(['method' => 'post', 'url' => '/prevent', 'class' => 'mt50']) !!}
				{!! csrf_field() !!}
		    	{!! Html::link(url('/'), translate('tutorial.back_button'), ['class' => 'btn text-system pull-left']) !!}
		    	{!! Form::hidden('email', Session::get('loginEmail')) !!}
		    	{!! Form::submit(translate('form.discard'), ['class' => 'btn btn-system mr10 pull-right']) !!}
			{!! Form::close() !!}
		</div>
	</div>
@stop