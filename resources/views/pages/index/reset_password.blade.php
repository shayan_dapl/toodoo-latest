@extends('layouts.main')
@section('content')
{!! Html::style('css/extra-styles/index_change_password.css') !!}
{!! Form::open(['method' => 'post', 'url' => '#']) !!}
<div class="panel-footer p25 pv15" id="passwordresetpassword">
    <div class="section">
    <i class="fa fa-info-circle text-danger" data-container="body" data-toggle="popover" data-placement="top" data-content="{{translate('words.password_strength')}}" data-original-title="" title=""></i>
    <div class="section mn">
        <div class="smr-80-extended">
            <label for="new-password" class="field prepend-icon">
                {!! Form::text('email', old('email'), array('class' => 'gui-input', 'id' => 'email', 'placeholder' => translate('form.email'), 'required' => 'required')) !!}
                <label for="email" class="field-icon">
                    <i class="fa fa-envelope"></i>
                </label>
            </label>
            @if (count($errors) > 0)
                <p class="text-danger mn">{{$errors->first('password')}}</p>
            @endif

            <label for="new-password" class="field prepend-icon">
                {!! Form::input('password', 'password', '', ['class' => 'gui-input', 'id' => 'new-password', 'placeholder' => translate('form.new_password'), 'v-model' => 'password', 'v-on:keyup' => 'passStrength()']) !!}
                <label for="email" class="field-icon">
                    <i class="fa fa-eye"></i>
                </label>
            </label>
            @if (count($errors) > 0)
                <p class="text-danger mn">{{$errors->first('password')}}</p>
            @endif
            <div class="progress mt10" v-if="password != ''" v-cloak>
                <div v-bind:class="passclass" role="progressbar" v-bind:aria-valuenow="passval" aria-valuemin="0" aria-valuemax="100">@{{passval}}%</div>
            </div>
            <br clear="all">

            <label for="conf-password" class="field prepend-icon">
                {!! Form::input('password', 'password_confirmation', '', ['class' => 'gui-input', 'id' => 'conf-password', 'placeholder' => translate('form.conf_password'), 'v-model' => 'confpassword', 'v-on:keyup' => 'passMatch()']) !!}
                <label for="email" class="field-icon">
                    <i class="fa fa-eye"></i>
                </label>
            </label>
            @if (count($errors) > 0)
                <p class="text-danger mn">{{$errors->first('password_confirmation')}}</p>
            @endif
            <div class="progress mt10" v-if="confpassword != ''" v-cloak>
                <div v-bind:class="confpassclass" role="progressbar" v-bind:aria-valuenow="confpassval" aria-valuemin="0" aria-valuemax="100">@{{confpasstext}}</div>
            </div>

            <br clear="all">

            {!! csrf_field() !!}
            {!! Form::hidden('hid', isset($hid) ? $hid : '') !!}
            {!! Form::submit(translate('form.save_button'), array('class' => 'btn text-default btn-system', 'id' => 'profile-btn', ':disabled' => '!enable')) !!}
        </div>
    </div>
</div>
{!! Form::close() !!}
@include('includes.scripts.password_script')
@stop