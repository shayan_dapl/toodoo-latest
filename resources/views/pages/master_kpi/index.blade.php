@extends('layouts.default')
@section('content')
@inject('status', 'App\Services\ProcessList')
<div id="masterkpi" v-cloak>
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-visible" id="spy2">
                <div class="panel-heading exhight">
                    {{translate('words.list')}}
                </div>
                <div class="panel-body">
                    <list-table :list="list" :booloptions="boolOptions" :trendoptions="trendOptions"  />
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel">
                <div class="panel-heading exhight">{{translate('words.add')}} / {{translate('words.edit')}}</div>
                <div class="panel-body">
                    <div class="admin-form">
                        <div class="row mb10">
                            <div class="col-md-12">
                                {!! Form::label('name', translate('form.kpi_name'), ['class' => 'field-label fs15 mb5 required']) !!}
                                <label class="field append-icon">
                                    <input type="text" v-model="kpiName" :class="kpiNameClass" tabindex="1" />
                                </label>
                            </div> 
                        </div>
                        <div class="row mb10">
                            <div class="col-md-6">
                                {!! Form::label('valid_from', translate('form.valid_from'), ['class' => 'field-label fs15 mb5 required']) !!}
                                <label class="field prepend-icon">
                                    <label class="field-icon">
                                        <i class="fa fa-calendar-o"></i>
                                    </label>
                                    <valid-date v-model="validFrom" data-type="from" :class="validFromClass" id="valid-from" tabindex="2" readonly></valid-date>
                                </label>
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('valid_to', translate('form.valid_to'), ['class' => 'field-label fs15 mb5 required']) !!}
                                <label class="field prepend-icon">
                                    <label class="field-icon">
                                        <i class="fa fa-calendar-o"></i>
                                    </label>
                                    <valid-date v-model="validTo" data-type="to" :class="validToClass" id="valid-to" tabindex="3" readonly></valid-date>
                                </label>
                            </div>
                        </div>
                        <div class="row mb10">
                            <div class="col-md-6">
                                {!! Form::label('frequency', translate('form.frequency'), ['class' => 'field-label fs15 mb5 required']) !!}
                                {!! Form::select('', $frequency, '', [':class' => 'frequencyClass', 'v-model' => 'frequency', 'tabindex' => '4']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('kpi_unit', translate('form.kpi_unit'), ['class' => 'field-label fs15 mb5 required']) !!}
                                {!! Form::select('', $kpiUnits, '', [':class' => 'kpiUnitClass', 'tabindex' => '5', 'v-model' => 'kpiUnit']) !!}
                            </div>
                        </div>
                        <div class="row mb10">
                            <div class="col-md-12">
                                {!! Form::label('target', translate('form.target'), ['class' => 'field-label fs15 mb5 required', 'v-if' => 'kpiUnit == 1 || kpiUnit == 2']) !!}
                                {!! Form::text('', '', [':class' => 'targetClass', 'tabindex' => '6', 'v-model' => 'target', 'v-if' => 'kpiUnit == 1 || kpiUnit == 2']) !!}
                                <p class="text-danger mt5" v-if="kpiUnit == 1 || kpiUnit == 2">@{{targetErr}}</p>

                                {!! Form::label('target', translate('form.target'), ['class' => 'field-label fs15 mb5 required', 'v-if' => 'kpiUnit == 3']) !!}
                                <select :class="targetBoolClass" v-model="targetBool" tabindex="6" v-if="kpiUnit == 3">
                                    <option v-for="(each, id) in boolOptions" :value="id">@{{each}}</option>
                                </select>
                                
                                {!! Form::label('target', translate('form.kpi_unit'), ['class' => 'field-label fs15 mb5 required', 'v-if' => 'kpiUnit == 4']) !!}
                                <select :class="trendClass" v-model="trend" tabindex="6" v-if="kpiUnit == 4">
                                    <option v-for="(each, id) in trendOptions" :value="id">@{{each}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <input type="hidden" v-model="hid" />
                                {!! Form::button(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', '@click' => 'saveData', ':disabled' => 'enabled', 'tabindex' => '7' ]) !!}
                                <br clear="all">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.master_kpi_script')
@stop