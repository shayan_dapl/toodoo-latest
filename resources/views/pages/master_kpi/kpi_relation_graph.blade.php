@extends('layouts.default')
@section('content')
<div class="panel panel-visible" id="spy2">
    <div class="panel-heading exhight">
        <div class="panel-title hidden-xs">
            {{ucfirst(translate('words.kpi_relation_graph'))}}
        </div>
    </div>
    <div class="panel-body bg-light">
        <div class="row">
            @if(!empty($masterKpis[0]))
                @foreach($masterKpis as $eachMasterKpi)
                    <div class="col-md-3 p10">
                        <ul class="treeview">
                            <li>
                                <a href="javascript:void(0)" class="p5 bg-system light text-default bord-rad-5">{{$eachMasterKpi->name}}</a>
                                @if(!empty($eachMasterKpi->processkpis))
                                    <ul>
                                        @foreach($eachMasterKpi->processkpis as $eachKpi)
                                            <li class="line-cls" data-model="{{$eachKpi->kpi->name}} {{translate('words.kpi')}}" data-id="{{$eachKpi->kpi->id}}" data-target="#turtle-details" data-toggle="modal" data-model="Kpi">
                                                <span>
                                                    {{$eachKpi->kpi->name}}
                                                </span>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        </ul>
                    </div>
                    @if ($loop->iteration % 4 == 0)
                        </div>
                        <div class="row">
                    @endif
                @endforeach
            @else
                <div class="col-md-12">
                    <h2 class="text-center">{{translate('table.no_data')}}</h2>
                </div>
            @endif
        </div>
    </div>
</div>
<div id="turtle-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content turtle-graph-modal-content">
            <div class="modal-header text-default back4d">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title turtle-title"></h4>
            </div>
            <div class="modal-body turtle-body row justified w-wrap-extended">
                <p>.....</p>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.kpi_relation_graph_script')
@stop
