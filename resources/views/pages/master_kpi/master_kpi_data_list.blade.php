@extends('layouts.default')
@section('content')
@inject('status', 'App\Services\ProcessList')
@inject('kpiunit', 'App\Services\KpiUnit')
@inject('weeks', 'App\Services\KpiUnit')
@inject('months', 'App\Services\KpiUnit')
@inject('quarters', 'App\Services\KpiUnit')
<div id="kpidata" v-cloak>
    <div class="row">
        <div class="col-md-12 mb10">
            {!! Html::link(url('/master-kpi/list'), translate('form.back'), ['class' => 'btn btn-system pull-right']) !!}
        </div>
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-heading">
                    {{translate('words.kpi_value')}}
                </div>
                <div class="panel-body fs18">
                    <div class="row">
                        <div class="col-md-4 text-right">
                            {{translate('form.kpi_name')}} : 
                        </div>
                        <div class="col-md-8">
                            {{$details->name}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            {{translate('form.valid_from')}} : 
                        </div>
                        <div class="col-md-8">
                            {{date(Config::get('settings.dashed_date'), strtotime($details->valid_from))}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            {{translate('form.valid_to')}} : 
                        </div>
                        <div class="col-md-8">
                            {{date(Config::get('settings.dashed_date'), strtotime($details->valid_to))}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            {{translate('form.frequency')}} : 
                        </div>
                        <div class="col-md-8">
                            {{$frequency[$details->frequency]}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            {{translate('form.kpi_unit')}} : 
                        </div>
                        <div class="col-md-8">
                            {{$kpiUnits[$details->kpi_unit_id]}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            {{translate('form.target')}} : 
                        </div>
                        <div class="col-md-8">
                            @if($details->kpi_unit_id == 3)
                                {{$boolOptions[$details->target]}}
                            @elseif($details->kpi_unit_id == 4)
                                {{$trendOptions[$details->target]}}
                            @else
                                {{$details->target}}
                            @endif
                        </div>
                    </div>
                    <div class="row mt30">
                        <div class="col-md-4 text-right">
                            <span v-if="frequency == 'daily'">{{translate('form.day')}} :</span> 
                            <span v-if="frequency == 'weekly'">{{translate('form.weeknumber')}} :</span> 
                            <span v-if="frequency == 'monthly'">{{translate('form.monthnumber')}} :</span> 
                            <span v-if="frequency == 'quarterly'">{{translate('form.quarternumber')}} :</span> 
                        </div>
                        <div class="col-md-8 admin-form">
                            <label class="field prepend-icon" v-if="frequency == 'daily'">
                                <label class="field-icon">
                                    <i class="fa fa-calendar-o"></i>
                                </label>
                                <period class="form-control pointer" :value="period" readonly></period>
                            </label>
                            <select :class="periodClass" v-model="period" v-if="frequency == 'weekly'" ref="weekly" :disabled="edited">
                                <option v-for="i in weeks" :disabled="restricted.includes(i)">@{{i}}</option>
                            </select>
                            <select :class="periodClass" v-model="period" v-if="frequency == 'monthly'" ref="monthly" :disabled="edited">
                                <option v-for="i in months" :disabled="restricted.includes(i)">@{{i}}</option>
                            </select>
                            <select :class="periodClass" v-model="period" v-if="frequency == 'quarterly'" ref="quarterly" :disabled="edited">
                                <option v-for="i in quarters" :disabled="restricted.includes(i)">@{{i}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt10">
                        <div class="col-md-4 text-right pt5">
                            {{translate('words.kpi_value')}} :
                        </div>
                        <div class="col-md-8">
                            <input type="text" v-model="target" :class="targetClass" v-if="kpiUnit == 1 || kpiUnit == 2" />
                            <p class="text-danger mt5 fs12" v-if="kpiUnit == 1 || kpiUnit == 2">@{{targetErr}}</p>

                            <select :class="targetBoolClass" v-model="targetBool" tabindex="6" v-if="kpiUnit == 3">
                                <option v-for="(each, id) in boolOptions" :value="id">@{{each}}</option>
                            </select>
                            
                            <select :class="trendClass" v-model="trend" tabindex="6" v-if="kpiUnit == 4">
                                <option v-for="(each, id) in trendOptions" :value="id">@{{each}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt10">
                        <div class="col-md-12 text-right">
                            <input type="hidden" v-model="hid" />
                            <button class="btn btn-system" @click="saveData()" :disabled="enabled">{{translate('form.save_button')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-visible" id="spy2">
                <div class="panel-heading">
                    {{$details->name}} - {{translate('words.graph')}}
                </div>
                <div class="panel-body">
                    <canvas id="master-kpi-graph"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-visible" id="spy2">
                <div class="panel-heading exhight">
                    {{$details->name}} - {{translate('words.list')}}
                </div>
                <div class="panel-body">
                    <kpi-list :list="list" :frequency="frequency"  />
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.master_kpi_data_script')
@stop