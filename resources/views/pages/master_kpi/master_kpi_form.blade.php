@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/master-kpi/form', 'enctype' => 'multipart/form-data','id'=>'master_kpi_form', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/master-kpi/list'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('words.master_kpi')}}</span>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('name', translate('form.kpi_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('kpi_name',(isset($details->name)) ? $details->name : '', array('class' => 'gui-input', 'id' => 'kpi_name', 'placeholder' => translate('form.kpi_name'), 'required' => 'required', 'tabindex' => '3')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('kpi_name')}}</p>
                        @endif
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('valid_from', translate('form.valid_from'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            <input type="text" name="valid_from" class="form-control valid-from-to-date" id="valid-from" value="{{$details->valid_from or ''}}" required="">
                            <label class="field-icon">
                                <i class="fa fa-calendar-o"></i>
                            </label>
                        </label>
                    </div> 
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('valid_to', translate('form.valid_to'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            <input type="text" name="valid_to" id="valid-to" class="form-control valid-from-to-date" value="{{$details->valid_to or ''}}" required="">
                            <label class="field-icon">
                                <i class="fa fa-calendar-o"></i>
                            </label>
                        </label>
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('frequency', translate('form.frequency'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field select">
                            {!! Form::select('frequency',$frequency, (isset($details->frequency)) ? $details->frequency : old('frequency'), array('class'=>'select2-single select-frequency','id' => 'frequency','required' => 'true')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('frequency')}}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('kpi_unit', translate('form.kpi_unit'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field select">
                            {!! Form::select('kpi_unit_id',$kpiUnits, (isset($details->kpi_unit_id)) ? $details->kpi_unit_id : old('kpi_unit_id'), array('class'=>'select2-single select-kpi-unit','id' => 'kpi_unit','required' => 'true')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('kpi_unit_id')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section hide" id="txtOpt">
                        {!! Form::label('target', translate('form.target'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('targettxt',(isset($details->target) && ($details->kpi_unit_id == 1 || $details->kpi_unit_id == 2 )) ? $details->target : '', array('class' => 'gui-input', 'id' => 'txtOpt-field', 'placeholder' => translate('form.target'), 'required' => 'required','oninput'=>"validateNumber(this);", 'tabindex' => '3')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('target')}}</p>
                        @endif
                    </div>
                    <div class="section hide" id="boolOpt">
                        {!! Form::label('target', translate('form.target'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field select">
                            {!! Form::select('targetboolopt',$boolOptions, (isset($details->target) && ($details->kpi_unit_id == 3)) ? $details->target : old('target'), array('class'=>'select2-single select-target','id' => 'boolOpt-field','required' => 'required')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('target')}}</p>
                        @endif
                    </div>
                    <div class="section hide" id="trendOpt">
                        {!! Form::label('target', translate('form.kpi_unit'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field select">
                            {!! Form::select('targettrndopt',$trendOptions, (isset($details->target) && ($details->kpi_unit_id == 4)) ? $details->target : old('target'), array('class'=>'select2-single select-target','id' => 'trendOpt-field','required' => 'required')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('target')}}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        
                    </div>
                </div>
            </div>

            <div class="section-divider mv40" id="spy2">
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    <input type="hidden" id="hid" name="hid" value="{{ $details->id or '' }}">
                    {!! Html::link(url('/master-kpi/list'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'save-btn')) !!}
                    <br clear="all">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var kpi_unit = '{{ isset($details->kpi_unit_id) ? $details->kpi_unit_id : "" }}';
        if(kpi_unit == 1 || kpi_unit == 2){
            $('#txtOpt').removeClass('hide');
            $('#boolOpt-field').removeAttr('required');
            $('#trendOpt-field').removeAttr('required');
        }else if(kpi_unit == 3){
            $('#boolOpt').removeClass('hide');
            $('#trendOpt-field').removeAttr('required');
            $('#txtOpt-field').removeAttr('required');
        }else if(kpi_unit == 4){
            $('#trendOpt').removeClass('hide');
            $('#boolOpt-field').removeAttr('required');
            $('#txtOpt-field').removeAttr('required');
        }
        
        $('#kpi_unit').on("change", function (e) { 
            var selectedKpiUnit = $(this).val();
            if(selectedKpiUnit == 3){
                $('#boolOpt').removeClass('hide');
                $('#trendOpt-field').removeAttr('required');
                $('#txtOpt-field').removeAttr('required');
                $('#txtOpt-field').val('');
                $('#trendOpt').addClass('hide');
                $('#txtOpt').addClass('hide');
            }else if(selectedKpiUnit == 4){
                $('#trendOpt').removeClass('hide');
                $('#boolOpt-field').removeAttr('required');
                $('#txtOpt-field').removeAttr('required');
                $('#txtOpt-field').val('');
                $('#boolOpt').addClass('hide');
                $('#txtOpt').addClass('hide');
            }else{
                $('#txtOpt').removeClass('hide');
                $('#txtOpt-field').val('');
                $('#boolOpt-field').removeAttr('required');
                $('#trendOpt-field').removeAttr('required');
                $('#boolOpt').addClass('hide');
                $('#trendOpt').addClass('hide');
            } 
            $(".select-target").select2({
                placeholder: "{{translate('form.target')}}",
                width: '100%'
            });
        }); 

        $( "#save-btn" ).click(function( ) {
          var frequency = $('#frequency').val();
          var kpi_unit = $('#kpi_unit').val();
          if(frequency == ''){
            $('#select2-frequency-container').parent('span').addClass('error');
          }else{
            $('#select2-frequency-container').parent('span').removeClass('error');
          }
          if(kpi_unit == ''){
            $('#select2-kpi_unit-container').parent('span').addClass('error');
          }else{
            $('#select2-kpi_unit-container').parent('span').removeClass('error');
          }
        });
        var validFromOpt = $.extend(      
        {},  // empty object      
        $.datepicker.regional["{{session('lang')}}"],  // Dynamically      
        {
            dateFormat: "dd-mm-yy",
            onSelect: function () {
                var dt2 = $('#valid-to');
                var startDate = $(this).datepicker('getDate');
                //add 30 days to selected date
                startDate.setDate(startDate.getDate() + 30);
                var minDate = $(this).datepicker('getDate');
                var dt2Date = dt2.datepicker('getDate');
                //difference in days. 86400 seconds in day, 1000 ms in second
                var dateDiff = (dt2Date - minDate)/(86400 * 1000);

                //dt2 not set or dt1 date is greater than dt2 date
                if (dt2Date == null || dateDiff < 0) {
                        dt2.datepicker('setDate', minDate);
                }
                //dt1 date is 30 days under dt2 date
                else if (dateDiff > 30){
                        dt2.datepicker('setDate', startDate);
                }
                //sets dt2 maxDate to the last day of 30 days window
                //first day which can be selected in dt2 is selected date in dt1
                dt2.datepicker('option', 'minDate', minDate);
            }
        });
        var validToOpt = $.extend(      
        {},  // empty object      
        $.datepicker.regional["{{session('lang')}}"],  // Dynamically      
        { 
            dateFormat: "dd-mm-yy"
        });
        $("#valid-from").datepicker(validFromOpt);
        $('#valid-to').datepicker(validToOpt); 
        $("#valid-from").keypress(function(event) {event.preventDefault();});
        $("#valid-to").keypress(function(event) {event.preventDefault();});
    }) 	
    var validNumber = new RegExp(/^\d*\.?\d*$/);
    var lastValid = document.getElementById("txtOpt-field").value;
    function validateNumber(elem) {
          if (validNumber.test(elem.value)) {
            lastValid = elem.value;
          } else {
            elem.value = lastValid;
            new PNotify({
                        title: "{{translate('alert.only_numeric_data_allowed')}}",
                        text: "",
                        addclass: 'stack_top_right',
                        type: 'error',
                        width: '290px',
                        delay: 2000
                    });
          }
    }
</script>
@stop
