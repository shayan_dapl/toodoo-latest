@extends('layouts.default')
@section('content')
@inject('status', 'App\Services\ProcessList')
@inject('kpiunit', 'App\Services\KpiUnit')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    <a class="btn btn-hover btn-system" href="{{url('master-kpi/kpi-relation-graph')}}">{{ucfirst(translate('words.kpi_relation_graph'))}}</a>
                    <a class="btn btn-hover btn-system" href="{{url('master-kpi/form')}}">{{ucfirst(translate('words.add'))}}</a>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('form.kpi_name')}}</th>
                            <th>{{translate('form.valid_from')}}</th>
                            <th>{{translate('form.valid_to')}}</th>
                            <th>{{translate('words.kpi_unit')}}</th>
                            <th>{{translate('words.target')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($masterKpis as $eachKpi)
                        <tr>
                            <td>{{$eachKpi->name}}</td>
                            <td>{{$eachKpi->valid_from}}</td>
                            <td>{{$eachKpi->valid_to}}</td>
                            <td>{{$kpiunit->kpiunit(session('lang'),$eachKpi->kpiunit)}}</td>
                            @if($eachKpi->kpi_unit_id == 3)
                            <td>{{$boolOptions[$eachKpi->target]}}</td>
                            @elseif($eachKpi->kpi_unit_id == 4)
                            <td>{{$trendOptions[$eachKpi->target]}}</td>
                            @else
                            <td>{{$eachKpi->target}}</td>
                            @endif
                            <td>
                                <a href="{{ url('/master-kpi/datalist/'.$eachKpi->id) }}" class="btn btn-default">
                                    <span class="text-system fa fa-cog"></span> {{translate('words.manage_data')}}
                                </a>
                                <a href="{{ url('/master-kpi/edit/'.$eachKpi->id) }}" class="btn btn-default">
                                    <span class="text-success fa fa-pencil"></span> {{translate('words.edit')}}
                                </a>
                                <a href="{{ url('/master-kpi/remove/'.$eachKpi->id) }}" onclick="return confirm('{{translate('alert.are_you_sure')}}')" class="btn btn-default">
                                    <span class="text-danger fa fa-times"></span> {{translate('words.remove')}}
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop