@extends('layouts.default')
@section('content')
@inject('space', 'App\Services\Membership')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible mt10 mb10" id="spy2">
            <div class="panel-heading exhight">
                {{translate('words.account_information')}}
                @if (!empty(Auth::user()->company->subscription_id) and empty(Auth::user()->company->subscription_cancelled_at))
                <button class="btn btn-danger text-default pull-right" id="end-subscription">{{translate('form.end_subscription')}}</button>
                @endif
                @if (!empty(Auth::user()->company->subscription_cancelled_at))
                    <span class="text-danger fs16 pull-right">
                        {{translate('alert.subscription_ended_at')}} : {{Auth::user()->company->next_renewal_date}}
                    </span>
                @endif
            </div>
            <div class="panel-body">
                <div class="col-md-7 pt5 pb5">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="fw700 fs16"> {{translate('words.actual_plan')}} : </span> 
                            {{$plans['iso']}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <span class="fw700 fs16"> {{translate('words.user')}} : </span> 
                            {{$userAdded}} / {{$plans['user']}}
                            @if(!empty($company->subscription_id) and empty($company->subscription_cancelled_at))
                            {!! Html::link('/membership/user', translate('words.add_more'), ['class' => 'btn btn-xs text-system ml5']) !!}
                            @endif
                            <div class="progress progCustom">
                                <div class="progress-bar progress-bar-{{($space->calcwidth($userAdded, $plans['user']) < 33) ? 'warning' : (($space->calcwidth($userAdded, $plans['user']) > 33 && $space->calcwidth($userAdded, $plans['user'])< 66) ? 'warning dark' : 'danger')}}" role="progressbar" aria-valuenow="{{$userAdded}}" aria-valuemin="0" aria-valuemax="{{$plans['user']}}" style="width: {{$space->calcwidth($userAdded, $plans['user'])}}%;">
                                    <span class="sr-only"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <span class="fw700 fs16"> {{translate('words.storage')}} : </span>
                            {{formatSize($fileSize)}} / {{$space->space($plans['storage'])}} GB
                            <div class="progress progCustom">
                                <div class="progress-bar progress-bar-{{($space->calcwidth($space->spacemb($fileSize), $plans['storage']) < 33) ? 'warning' : (($space->calcwidth($space->spacemb($fileSize), $plans['storage']) > 33 && $space->calcwidth($space->spacemb($fileSize), $plans['storage']) < 66) ? 'warning dark' : 'danger')}}" role="progressbar" aria-valuenow="{{$space->spacemb($fileSize)}}" aria-valuemin="0" aria-valuemax="{{$plans['storage']}}" style="width: {{$space->calcwidth($space->spacemb($fileSize), $plans['storage'])}}%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 pt5 pb5">
                    @if(!empty($company->subscription_id))
                    <span class="fw700 fs16"> {{translate('words.subscription_id')}} :</span> 
                    {{!empty($company->subscription_id) ? $company->subscription_id : translate('words.na')}}
                    <br clear="all">
                    <span class="fw700 fs16"> {{translate('words.subscription_amount')}} :</span> 
                    &euro; {{$company->subscription_amount}} ({{translate('words.vat_excluded')}}) 
                    <br clear="all">
                    <span class="fw700 fs16">{{translate('words.next_renewal_date')}} :</span> 
                    {{date(\Config::get('settings.dashed_date'), strtotime($company->next_renewal_date))}}
                    <br clear="all">
                    <span class="fw700 fs16">{{translate('form.tva_no')}} :</span> 
                    {{$company->tva_no}}
                    <br clear="all">
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@if($approvalPanel)
<div class="panel">
    <div class="panel-heading panel-primary">
        <span class="panel-title">{{translate('words.please_approve_price_increase_request')}}</span>
        <div class="widget-menu pull-right">
        </div>
    </div>
    <div class="panel-body fill">
        <div class="row">
            <div class="col-md-4">
                <p class="p3">{{translate('words.current_package_price')}} : &euro; {{$currentPackagePrice}}</p>
                <p class="p3">{{translate('words.current_user_price')}} : &euro; {{$currentUserPrice}}</p>
                <p class="p3">{{translate('words.current_storage_price')}} : &euro; {{$currentStoragePrice}}</p> 
            </div>
            <div class="col-md-4">
                <p class="p3">{{translate('words.new_package_price')}} : &euro; {{$newPackagePrice}}</p>
                <p class="p3">{{translate('words.new_user_price')}} : &euro; {{$newUserPrice}}</p>
                <p class="p3">{{translate('words.new_storage_price')}} : &euro; {{$newStoragePrice}}</p>
            </div>
            <div class="col-md-4">
                <p class="mt30 fw700">{{translate('words.new_subscription_amount')}} : &euro; {{$newSubscriptionAmount}}</p>
            </div>
        </div>
        <div class="row">
            <p>
                {!! Form::open(['method' => 'post', 'url' => '/', 'id' => 'approve-req']) !!}
                <input type ="hidden" name="newPackagePrice" id="newPackagePrice" value="{{$newPackagePrice}}"/>
                <input type ="hidden" name="newUserPrice" id="newUserPrice" value="{{$newUserPrice}}"/>
                <input type ="hidden" name="newStoragePrice" id="newStoragePrice" value="{{$newStoragePrice}}"/>
                <input type ="hidden" name="newSubscriptionAmount" id="newSubscriptionAmount" value="{{$newSubscriptionAmount}}"/>
                <input type ="hidden" name="proposalId" id="proposalId" value = "{{$proposalId}}"/>
                <a class="btn btn-primary pull-right" href="javascript:void(0)" id = "approve-request">{{translate('words.approve')}}</a>
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                {!! Form::close() !!}
            </p>
        </div>
    </div>
</div>
@endif
@if($company->transactions->count() > 0)
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                {{translate('words.payment_history')}}
            </div>
            <div class="panel-body pn">
                <table class="table table-striped table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.invoice_no')}}</th>
                            <th>{{translate('table.payment_date')}}</th>
                            <th>{{translate('table.transaction_id')}}</th>
                            <th>{{translate('table.payment_amount')}}</th>
                            <th>{{translate('table.status')}}</th>
                            <th>{{translate('table.view')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($company->transactions->count() > 0)
                        @foreach($company->transactions as $invoice)
                        @if($invoice->transaction_id != null)
                        <tr>
                            <td>{{$invoice->invoice_no}}</td>
                            <td>{{date(\Config::get('settings.dashed_date'), strtotime($invoice->payment_date))}}</td>
                            <td>{{$invoice->transaction_id}}</td>
                            <td>&euro; {{$invoice->total_amount}}</td>
                            <td>{{$invoice->status}}</td>
                            <td>
                                <a class="btn btn-success" href="{{ url('/membership/transaction-details/'.$invoice->id) }}" title="{{translate('words.see_invoice')}}" target="_blank">
                                    <span class="fa fa-credit-card"></span> 
                                </a>
                                <a href="{{url('/membership/invoice-print/'.$invoice->id)}}" class="ml20 btn btn-system" title="{{translate('words.print_invoice')}}">
                                    <span class="fa fa-print"></span>
                                </a>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" align="center">{{translate('table.no_data')}}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif
<script type="text/javascript">
    $(document).ready(function () {
        $('#approve-request').click(function () {
            $('#ajxLoader').removeClass('hide');
            var newPackagePrice = $('#newPackagePrice').val();
            var newUserPrice = $('#newUserPrice').val();
            var newStoragePrice = $('#newStoragePrice').val();
            var newSubscriptionAmount = $('#newSubscriptionAmount').val();
            var proposalId = $('#proposalId').val();
            var token = $('#token').val();
            $.ajax({
                url: "{{url('membership/approve-price-request')}}",
                type: "POST",
                data: {newPackagePrice : newPackagePrice, newUserPrice: newUserPrice, newStoragePrice: newStoragePrice, newSubscriptionAmount : newSubscriptionAmount, proposalId : proposalId, '_token': token},
                success: function (data) {
                    setTimeout(function () { $('#ajxLoader').addClass('hide'); }, 500);
                    new PNotify({
                        text: "{{translate('alert.thanks_for_accepting_the_request')}}",
                        addclass: 'stack_top_right',
                        type: 'success',
                        width: '290px',
                        delay: 2000
                    });
                    $(document).delay(1000).queue(function () {
                        location.reload();
                    });
                }
            });
        });

        $('#end-subscription').click(function () {
            var c = confirm("{{translate('alert.are_you_sure')}}");
            if (c === true) {
                $.ajax({
                    url : "{{url('/membership/end-subscription')}}",
                    type : "GET",
                    success : function (response) {
                        if (response === true) {
                            new PNotify({
                                text: "{{translate('alert.subscription_cancelled')}}",
                                addclass: 'stack_top_right',
                                type: 'success',
                                width: '290px',
                                delay: 2000
                            });
                            $(document).delay(1000).queue(function () {
                                location.reload();
                            });
                        }
                    }
                });
            }
        });
    })
</script>
@stop