@extends('layouts.main')
	@section('content')
	@inject('total', 'App\Services\MembershipPreview')
	<div class="content">		
		<div class=" subscriptionFooter text-center">
			<h2>Your new subscription amount will be</h2>
			<h1>&euro; {{number_format(($previousTotalPayment + $total->total($data, $addons)['total']), 2)}}</h1>
			{!! Form::open(['method' => 'post', 'url' => '/membership/make-payment/']) !!}
				{!! csrf_field() !!}
				{!! Form::hidden('company_id', Auth::guard('customer')->user()->company_id) !!}
				{!! Form::hidden('id', Auth::guard('customer')->user()->id) !!}
				{!! Form::hidden('name', Auth::guard('customer')->user()->name) !!}
				{!! Form::hidden('email', Auth::guard('customer')->user()->email) !!}
				{!! Form::hidden('amount', $total->total($data, $addons)['total']) !!}
				{!! Form::hidden('vat', $total->total($data)['vat']) !!}
				{!! Form::hidden('addons', "0") !!}
				{!! Form::hidden('order_id', $data->id) !!}
				{!! Form::submit(translate('words.pay'), array('class' => 'btn text-default btn-system mt20 mb30', 'id' => 'cmpdetail-btn', 'tabindex' => '5')) !!}
			{!! Form::close() !!}
		</div>
	</div>
@stop