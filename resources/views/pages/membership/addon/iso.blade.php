@extends('layouts.main')
@section('content')
{!! Form::open(array('method' => 'post', 'url' => '/membership/iso')) !!}
<section id="content" class="table-layout animated fadeIn">
	<div class="tray-center">
		<div class="row">
			<div class="col-md-12 fs22 fw700">
				{{translate('words.convinced')}}
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 fs22 fw300 mb20">
				{{translate('words.what_you_need')}}
			</div>
		</div>
		<div class="row">
			@if($iso->count() > 0)
			@foreach($iso as $each)
			<div class="col-sm-4">
				<label for="chk{{$loop->iteration}}" class="br-a mb20 br-grey overview-segment-head @if($each->is_base == 1 || (!empty($details) and in_array($each->id, $selected))) active @endif" data-id="{{$each->id}}" data-base="{{$each->is_base}}" data-price="{{$each->price}}">
					<div class="panel-title">
						<span class="fs16 fw700 w100 p5">
							{{$each->iso->iso_no_en}}
						</span>
						@if($each->is_base == 1)
						<span class="pull-right w130 fs14 bg-system text-default p5">
							{{translate('words.base_package')}}
						</span>
						@endif
					</div>
					<div class="panel-body">
						<div class="count mt30">
							<div class="fw300 time fs14">
								{!!$each->description_en!!}
							</div>
						</div>
					</div>
					<div class="panel-footer">										
						<span class="fs20 fw700 text-system pl20">
							+{{floatval($each->price)}} &euro; /{{translate('form.month')}}
						</span>
					</div>
				</label>
				{!! Form::checkbox('packages[]', $each->id, ($each->is_base == 1 || (!empty($details) and in_array($each->id, $selected))), ['id' => ('chk'.$loop->iteration), 'class' => 'hide']) !!}
			</div>
			@endforeach
			@endif			
		</div>
		<div class="row">
			<div class="col-md-12">
				{!! csrf_field() !!}
				{!! Form::hidden('hid', !empty($details) ? $details->id : '') !!}
				{!! Form::submit(translate('tutorial.next_button'), array('class' => 'btn text-default btn-system')) !!}
			</div>
		</div>
	</div>
</section>
{!! Form::close() !!}

<script type="text/javascript">
	$(document).ready(function() {
		//Making all iso boxes size of same
		if ($('.overview-segment-head').length > 0) {
			var boxHeight = 0;
			var boxWidth = 0;
			$('.overview-segment-head').each(function () {
				if(parseInt($(this).find('.panel-body').css('height')) > boxHeight) {
					boxHeight = $(this).find('.panel-body').css('height');
				}
				if(parseInt($(this).find('.panel-body').css('width')) > boxWidth) {
					boxWidth = $(this).find('.panel-body').css('width');
				}
			});
			$('.overview-segment-head').find('.panel-body').css('height', boxHeight);
			$('.overview-segment-head').find('.panel-body').css('width', boxWidth);
		}
		//===============================//

		$('.overview-segment-head').click(function() {
			var price = $('#price').val();
			if (!$(this).hasClass('active')) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
	});
</script>
@stop