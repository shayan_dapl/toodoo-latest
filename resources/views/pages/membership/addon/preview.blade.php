@extends('layouts.main')
	@section('content')
	@inject('space', 'App\Services\Membership')
	@inject('isoPrice', 'App\Services\Membership')
	@inject('total', 'App\Services\Membership')
	@inject('tenureprice', 'App\Services\Membership')
	<section id="content">
		<div class="row">
			<div class="col-md-12 fs20 fw700 mb20">
				{{translate('words.convinced')}}
			</div>
		</div>
		@if (!empty($data))
			@foreach($data['iso'] as $each)
				<div class="row mb5">
					<div class="col-md-5">
						{{translate('words.package')}} : {{$each->iso->iso_no_en}} (&euro; {{$each->price}})
					</div>
					<div class="col-md-3 text-right">
						&euro; {{number_format($isoPrice->isoPrice($each), 2)}} ({{$total->total($data, $type)['remaining']}} {{translate('words.days')}})
					</div>
					<div class="col-md-3">
						{!! Html::link( url('/membership/iso'), translate('words.no_modify'), array('class' => 'text-system')) !!}
					</div>
				</div>
			@endforeach
			@if (!empty($data['user']))
				<div class="row mb5">
					<div class="col-md-5">
						{{translate('words.extra_user')}} : {{$data['user']->user_count}} {{translate('words.users')}} (&euro; {{$tenureprice->tenureprice($data['user']->price_per_month,$subscriptionTenure)}} {{($subscriptionTenure == 1) ? translate('words.per_month') : translate('words.per_year')}})
					</div>
					<div class="col-md-3 text-right">
						&euro; {{number_format($total->total($data, $type, $subscriptionTenure)['total'],2)}} ({{$total->total($data, $type)['remaining']}} {{translate('words.days')}})
					</div>
					<div class="col-md-3">
						{!! Html::link( url('/membership/user'), translate('words.no_modify'), array('class' => 'text-system')) !!}
					</div>
				</div>
			@endif
			@if (!empty($data['storage']))
				<div class="row">
					<div class="col-md-5">
						{{translate('words.extra_storage')}} : {{$space->space($data['storage']->space)}} GB (&euro; {{$data['storage']->price_per_month}})
					</div>
					<div class="col-md-3 text-right">
						&euro; {{$total->total($data, $type)['total']}} ({{$total->total($data, $type)['remaining']}} {{translate('words.days')}})
					</div>
					<div class="col-md-3">
						{!! Html::link(url('/membership/storage'), translate('words.no_modify'), array('class' => 'text-system')) !!}
					</div>
				</div>
			@endif
		@endif
		@if ($data['company']->company->is_vat == 0 and $data['company']->company->countries->vat_percent != '')
			<div class="row">
				<div class="col-md-5">
					{{ translate('words.vat')}} ({{$vat}} %)
				</div>
				<div class="col-md-3 text-right">
					&euro; {{$total->total($data, $type, $subscriptionTenure)['vat']}}
				</div>
			</div>
		@endif
		<div class="row mt20">
			<div class="col-md-3 fw700 fs16">
				{{translate('words.total')}}
			</div>
			<div class="col-md-5 fw600 fs14 text-right">
				&euro; {{number_format(($total->total($data, $type, $subscriptionTenure)['total'] + $total->total($data, $type, $subscriptionTenure)['vat']), 2)}} {{translate('words.per_month')}}
			</div>
		</div>
		@if ($total->total($data, $type)['total'] > 0)
			@if($data['company']->company->is_vat == 1)
			<div class="row mt20">
				<div class="col-md-12">
					{{translate('words.vat_excluded_price')}}
				</div>
			</div>
			@endif
			<div class="row mt20">
				<div class="col-md-12">
					{{translate('words.cancel_anytime')}}
				</div>
			</div>
		@endif
		<div class="row mt20">
		   	<div class="col-md-12">
		   		@if ($total->total($data, $type)['total'] == 0)
		   			<span class="fw700 text-danger">{{translate('alert.cart_empty')}}</span>
					{!! Html::link(URL::previous(), translate('tutorial.back_button'), array('class' => 'btn btn-default')) !!}
		   		@else
			   		{!! Form::button(translate('words.pay'), ['class' => 'btn text-default btn-system', 'data-toggle' => 'modal', 'data-target' => '#confirm-payment']) !!}
		  		@endif
		  	</div>
		</div>
	</section>

	<div id="confirm-payment" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-system">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">&nbsp;</h4>
				</div>
				<div class="modal-body">
					<h3>{{translate('alert.your_next_renewal')}} &euro; {{number_format($total->total($data, $type, $subscriptionTenure)['nextPayment'], 2)}}</h3>
					{!! Form::open(['method' => 'post', 'url' => '/membership/addon-payment/']) !!}
						{!! csrf_field() !!}
						{!! Form::hidden('company_id', $data['company']->company_id) !!}
						{!! Form::hidden('id', $data['company']->id) !!}
						{!! Form::hidden('name', Auth::guard('customer')->user()->name) !!}
						{!! Form::hidden('email', Auth::guard('customer')->user()->email) !!}
						{!! Form::hidden('amount', ($total->total($data, $type, $subscriptionTenure)['total'] + $total->total($data, $type,$subscriptionTenure)['vat'])) !!}
						{!! Form::hidden('base_amount', $total->total($data, $type, $subscriptionTenure)['total']) !!}
						{!! Form::hidden('vat', $total->total($data, $type, $subscriptionTenure)['vat']) !!}
						{!! Form::hidden('nextPayment', $total->total($data, $type, $subscriptionTenure)['nextPayment']) !!}
						{!! Form::hidden('nextPaymentWithoutVat', $total->total($data, $type, $subscriptionTenure)['nextPaymentWithoutVat']) !!}
						{!! Form::hidden('package_price', (!empty($data['iso'])) ? $packageprice->packageprice($data['iso']) : '0.00') !!}
						{!! Form::hidden('user_package_price', (!empty($data['user'])) ? $total->total($data, $type, $subscriptionTenure)['total'] : '0.00') !!}
						{!! Form::hidden('storage_package_price', (!empty($data['storage'])) ? $data['storage']->price_per_month : '0.00') !!}
						{!! Form::submit(translate('words.pay'), ['class' => 'btn text-default btn-system', 'tabindex' => '5']) !!}
					{!! Form::close() !!}
				</div>
				<div class="modal-footer">
		        	<button type="button" class="btn btn-danger" data-dismiss="modal">{{translate('form.cancel')}}</button>
		      	</div>
			</div>
		</div>
	</div>
@stop