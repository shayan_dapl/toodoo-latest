@extends('layouts.main')
@section('content')
{!! Html::style('css/extra-styles/membership_company_details.css') !!}
<div class="panel-body pl50 pr50" id="modify">
    <h2><b>{{translate('form.convinced')}} ?</b></h2>
    {!! Form::open(['method' => 'post', 'url' => '/membership/storage', 'autocomplete' => 'off', 'class' => 'form-horizontal','id'=>'cmpDetailFrm']) !!}
    <h4>{{translate('words.how_much_storage_do_you_need')}}?</h4>

    <div class="row mt20 mb20" id="extra-storage-area">
        <div class="col-md-3 mt20">
            <span class="lbl switch-custom-pos">
                {{translate('words.extra_storage')}}
            </span>
        </div>
        <div class="col-md-4">
            <label class="field select">
                {!! Form::select('extra_storage', $storagePlans, $selected, array('id' => 'extra_storage', 'tabindex' => '6')) !!}
                <i class="arrow"></i>
            </label>  
        </div>   
    </div>

    <div class="row">
        <div class="col-md-3">
            {!! Form::hidden('hid', !empty($details) ? $details->id : '') !!}
            {!! Form::submit(translate('tutorial.next_button'), array('class' => 'btn text-default btn-system', 'id' => 'cmpdetail-btn', 'tabindex' => '5')) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
@include('includes.scripts.choose_addon_script')
@stop