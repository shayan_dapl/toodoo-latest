@extends('layouts.main')
@section('content')
	<div class="row">
		<div class="col-md-12 text-center">
			<br clear="all">
			<span class="fs20 fw700">{{translate('words.thank_you')}}</span>
			<br clear="all"><br clear="all">
			<span class="fs16 fw600">{{translate('words.can_start_right_away')}}</span>
			<br clear="all"><br clear="all">
			{!! Html::link(url('/home'), translate('words.start_with_toodoo'), ['class' => 'btn btn-system']) !!}
			<br clear="all"><br clear="all">
		</div>
	</div>
@stop