@extends('layouts.main')
@section('content')
{!! Html::style('css/extra-styles/membership_company_details.css') !!}

<div class="panel-body pl50 pr50" id="modify">
    <h2><b>{{translate('form.convinced')}} ?</b></h2>
    <h4>{{translate('words.how_meny_users_you_want_to_use_on_too_doo')}}?</h4>
    {!! Form::open(['method' => 'post', 'url' => '/membership/user/', 'autocomplete' => 'off', 'class' => 'form-horizontal','id'=>'cmpDetailFrm']) !!}
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="useraddonsection useraddonsection-addon" id="useraddonsection">
                @foreach($userPlans as $k=>$each)
                <div class="radio-custom radio-system mb5">  
                    <input id="radioExample{{$each['id']}}" data-max="{{$each['is_max']}}" data-price="{{$each['price_per_month']}}" name="userCount" value="{{$each['id']}}" type="radio" class="usr-addon-radio" @if ($each['id'] == '2' || (!empty($details) and $details->user_count == $each['id'])) checked @endif>
                    @if($each['is_max'] == 1)
                    <label for="radioExample{{$each['id']}}">{{translate('words.more_than')}} {{$each['user_count']}} {{translate('words.users')}}</label>
                    @else
                    <label for="radioExample{{$each['id']}}">{{$each['user_count']}} {{translate('words.users')}}</label>
                    @endif
                </div>
                @endforeach 
            </div>
            <div class="addondetails">
                <h4 id="userCountPrice"></h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            {!! Form::hidden('hid', !empty($details) ? $details->id : '') !!}
            {!! Form::hidden('tenure', $subscriptionTenure, array('id' => 'subscriptionTenure')) !!}
            {!! Form::submit(translate('tutorial.next_button'), array('class' => 'btn text-default btn-system', 'id' => 'cmpdetail-btn', 'tabindex' => '5')) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
@include('includes.scripts.choose_addon_script')
@stop