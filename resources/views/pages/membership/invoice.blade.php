@extends('layouts.main')
@section('content')
@inject('space', 'App\Services\Membership')
@inject('countryName', 'App\Services\Membership')
@inject('packageName', 'App\Services\Membership')
@inject('tenureprice', 'App\Services\Membership')
<style type="text/css">

</style>
<div class="row p10">
	<div class="col-md-3 text-right">
		{!! Form::image(asset('image/too-doo-logo.png'), 'User Image', array('class' => 'img-responsive', 'alt' => 'Site Logo')) !!}
	</div>
	<div class="col-md-9 fw700 fs16 p20 text-right">
		{{translate('words.invoice')}}
		<a href="{{url('/membership/invoice-print/'.$data->id)}}" class="ml20 btn btn-system">
                    <i class="fa fa-print"></i>
        </a>
         {!! Html::link(URL::previous(), translate('form.back'), ['class' => 'ml10 btn btn-hover btn-system pull-right']) !!}
	</div>
</div>
<div class="row">
	<div class="col-md-6 p20">
		{{translate('table.invoice_no')}}: {{$data->invoice_no}}<br clear="all">
		{{translate('words.customer_ref')}}: {{$customerId}}
	</div>
	<div class="col-md-6">
		{{translate('words.name')}}: {{isset($data->invoicedata->username) ? $data->invoicedata->username : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->name)}}<br clear="all">
		{{translate('form.company_name')}}: {{ isset($data->invoicedata->company_name) ? $data->invoicedata->company_name : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->name)}}<br clear="all">
		{{translate('words.address')}}: 
		{{isset($data->invoicedata->street) ? $data->invoicedata->street : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->street)}}, 
		{{isset($data->invoicedata->nr) ? $data->invoicedata->nr : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->nr)}} ,
		{{isset($data->invoicedata->bus) ? $data->invoicedata->bus : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->bus)}}
		<br clear="all">
		{{translate('form.zip')}}: 
		{{ isset($data->invoicedata->zip) ? $data->invoicedata->zip : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->zip)}}
		<br clear="all">
		{{translate('form.city')}}: {{ isset($data->invoicedata->city) ? $data->invoicedata->city : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->city)}}
		<br clear="all">
		{{translate('form.country')}}: {{ ($countryData != '') ? $countryData : (Auth::guard('web')->check() ? '' : $countryName->countryName(Auth::guard('customer')->user()->company->countries))}}
		<br clear="all">
		{{translate('words.vat_no')}}: {{ isset($data->invoicedata->tva_no) ? $data->invoicedata->tva_no : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->tva_no)}}
		<br clear="all">
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		
	</div>
</div>
<div class="row">
	<div class="col-md-12 p25">
		<table border="1" cellspacing="0" cellpadding="0" style="width: 100%">
			<thead>
				<tr>
					<th align="center">
						{{translate('words.package')}}
					</th>
					<th align="center">
						{{translate('words.payment_date')}}
					</th>
					<th class="text-right">
						{{translate('words.unit_price')}}
					</th>
					<th class="text-right">
						{{translate('words.price')}}
					</th>
				</tr>
			</thead>
			@if(!empty($data->purchased) && $paymentType != 'renewal')
				@foreach($data->purchased as $each)
					@if ($each->type == 'iso' && $each->status == 1)
					<tr>
						<td>
							{{$packageName->packageName($each->iso())}}
						</td>
						<td>
							{{\Carbon\Carbon::parse($data->payment_date)->format('d-m-Y')}}
						</td>
						<td align="right">
							&euro; {{$tenureprice->tenureprice($each->iso()->price, $data->company->subscription_tenure)}}
						</td>
						<td align="right">
							&euro; {{$each->transaction->package_price}}
						</td>
					</tr>
					@elseif($each->type == 'user' && $each->status == 1)
					<tr>
						<td>
							{{$each->user()->user_count}} {{translate('words.extra_user')}} ({{$each->day_count}} {{translate('words.days') }})
						</td>
						<td>
							{{\Carbon\Carbon::parse($data->payment_date)->format('d-m-Y')}}
						</td>
						<td align="right">
							&euro; {{$tenureprice->tenureprice($each->user()->price_per_month, $data->company->subscription_tenure)}}
						</td>
						<td align="right">
							&euro; {{$each->transaction->user_package_price}}
						</td>
					</tr>
					@elseif($each->type == 'storage' && $each->status == 1)
					<tr>
						<td>
							{{$space->space($each->storage()->space)}} GB {{translate('words.extra_storage')}} ({{$each->day_count}} {{translate('words.days') }})
						</td>
						<td>
							{{\Carbon\Carbon::parse($data->payment_date)->format('d-m-Y')}}
						</td>
						<td align="right">
							&euro; {{$each->storage()->price_per_month}}
						</td>
						<td align="right">
							&euro; {{$each->amount}}
						</td>
					</tr>
					@endif
				@endforeach
			@else
				<tr>
					<td>
						{{translate('words.package_renewal')}}
					</td>
					<td>
						{{\Carbon\Carbon::parse($data->payment_date)->format('d-m-Y')}}
					</td>
					<td align="right">
						{{translate('words.na')}}
					</td>
					<td align="right">
						&euro; {{$data->amount}}
					</td>
				</tr>
			@endif
		</table>
	</div>
</div>
@if($data->vat_amount != null)
<div class="row mt10">
	<div class="col-md-9 fw700 fs16 text-right">
		{{translate('words.vat')}}
	</div>
	<div class="col-md-3 fw600 fs14 text-right pr30">
		&euro; {{$data->vat_amount}}
	</div>
</div>
@endif
<div class="row mt10">
	<div class="col-md-9 fw700 fs16 text-right">
		{{translate('words.total')}}
	</div>
	<div class="col-md-3 fw600 fs14 text-right pr30">
		&euro; {{$data->total_amount}}
	</div>
</div>
<div class="row mt10">
	<div class="col-md-12 pl30">
		{{translate('words.payment_date')}} : {{\Carbon\Carbon::parse($data->payment_date)->format('d-m-Y')}}
		<br clear="all"><br clear="all"><br clear="all"><br clear="all">
	</div>
</div>
<div class="row">
	<div class="col-md-12 pl20 pr20 text-center pb20">
		<hr>
		{{translate('words.organization_details')}}
	</div>
</div>
@stop