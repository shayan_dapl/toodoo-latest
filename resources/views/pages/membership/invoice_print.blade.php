@inject('space', 'App\Services\Membership')
@inject('countryName', 'App\Services\Membership')
@inject('packageName', 'App\Services\Membership')
@inject('tenureprice', 'App\Services\Membership')
<style type="text/css">
	table tr td {
		border-right: 1px solid #000;
		border-bottom: 0;
		border-top: 0;
		border-left: 0;
	}
	
	table thead tr {
		background: #eee;
		border: 1px solid #333 !important;
	}	
	table thead tr th, table tbody tr td {
		padding: 5px;
	}
</style>
<div class="row p10">
	<div style="width:25%; display:inline-block;">
		<img src="{{public_path('image/too-doo-logo.png')}}" height="100" class='img-responsive'>
	</div>
	<div style="width: 75%; display:inline-block; text-align: right; padding: 20px;">
		<strong style="font-size: 25px;">{{translate('words.invoice')}}</strong>
	</div>
</div>
<div class="row">
	<div style="width: 50%; padding: 20px; display:inline-block;">
		<p style="font-size: 15px; margin: 0; line-height: 18px;">
			{{translate('table.invoice_no')}}: {{$data->invoice_no}}
		</p>
		<p style="font-size: 15px; margin: 0; line-height: 18px;">
			{{translate('words.customer_ref')}}: {{$customerId}}
		</p>
	</div>

	<div style="width: 50%; display:inline-block;">
		<p style="font-size: 15px; margin: 0; line-height: 18px;">
		   {{translate('words.name')}}: {{isset($data->invoicedata->username) ? $data->invoicedata->username : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->name)}}
		</p>

		<p style="font-size: 15px; margin: 0; line-height: 18px;">
		   {{translate('form.company_name')}}: {{ isset($data->invoicedata->company_name) ? $data->invoicedata->company_name : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->name)}}
		</p>

		<p style="font-size: 15px; margin: 0; line-height: 18px;">
		   {{translate('words.address')}}: 
		{{isset($data->invoicedata->street) ? $data->invoicedata->street : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->street)}}, 
		{{isset($data->invoicedata->nr) ? $data->invoicedata->nr : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->nr)}} ,
		{{isset($data->invoicedata->bus) ? $data->invoicedata->bus : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->bus)}}
		</p>

		<p style="font-size: 15px; margin: 0; line-height: 18px;">
		   {{translate('form.zip')}}: {{ isset($data->invoicedata->zip) ? $data->invoicedata->zip : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->zip)}}
		</p>

		<p style="font-size: 15px; margin: 0; line-height: 18px;">
		   {{translate('form.city')}}: {{ isset($data->invoicedata->city) ? $data->invoicedata->city : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->city)}}
		</p>

		<p style="font-size: 15px; margin: 0; line-height: 18px;">
		   {{translate('form.country')}}: {{ ($countryData != '') ? $countryData : (Auth::guard('web')->check() ? '' : $countryName->countryName(Auth::guard('customer')->user()->company->countries))}}
		</p>

		<p style="font-size: 15px; margin: 0; line-height: 18px;">
		   {{translate('words.vat_no')}}: {{ isset($data->invoicedata->tva_no) ? $data->invoicedata->tva_no : (Auth::guard('web')->check() ? '' : Auth::guard('customer')->user()->company->tva_no)}}
		</p>

	</div>
</div>

<div class="row">
	<div style="padding: 10px 0;">
		<table border="1" cellspacing="0" cellpadding="0" style="width: 100%">
			<thead>
				<tr>
					<th align="center">
						{{translate('words.package')}}
					</th>
					<th align="center">
						{{translate('words.payment_date')}}
					</th>
					<th align="center">
						{{translate('words.unit_price')}}
					</th>
					<th align="center">
						{{translate('words.price')}}
					</th>
				</tr>
			</thead>
			@if(!empty($data->purchased) && $paymentType != 'renewal')
				@foreach($data->purchased as $each)
					@if ($each->type == 'iso' && $each->status == 1)
					<tr>
						<td>
							{{$packageName->packageName($each->iso())}}
						</td>
						<td>
							{{\Carbon\Carbon::parse($data->payment_date)->format('d-m-Y')}}
						</td>
						<td align="right">
							&euro; {{$tenureprice->tenureprice($each->iso()->price, $data->company->subscription_tenure)}}
						</td>
						<td align="right">
							&euro; {{$each->transaction->package_price}}
						</td>
					</tr>
					@elseif($each->type == 'user' && $each->status == 1)
					<tr>
						<td>
							{{$each->user()->user_count}} {{translate('words.extra_user')}} ({{$each->day_count}} {{translate('words.days') }})
						</td>
						<td>
							{{\Carbon\Carbon::parse($data->payment_date)->format('d-m-Y')}}
						</td>
						<td align="right">
							&euro; {{$tenureprice->tenureprice($each->user()->price_per_month, $data->company->subscription_tenure)}}
						</td>
						<td align="right">
							&euro; {{$each->transaction->user_package_price}}
						</td>
					</tr>
					@elseif($each->type == 'storage' && $each->status == 1)
					<tr>
						<td>
							{{$space->space($each->storage()->space)}} GB {{translate('words.extra_storage')}} ({{$each->day_count}} {{translate('words.days') }})
						</td>
						<td>
							{{\Carbon\Carbon::parse($data->payment_date)->format('d-m-Y')}}
						</td>
						<td align="right">
							&euro; {{$each->storage()->price_per_month}}
						</td>
						<td align="right">
							&euro; {{$each->amount}}
						</td>
					</tr>
					@endif
				@endforeach
			@else
				<tr>
					<td>
						{{translate('words.package_renewal')}}
					</td>
					<td>
						{{\Carbon\Carbon::parse($data->payment_date)->format('d-m-Y')}}
					</td>
					<td align="right">
						{{translate('words.na')}}
					</td>
					<td align="right">
						&euro; {{$data->amount}}
					</td>
				</tr>
			@endif
		</table>
	</div>
</div>
@if($data->vat_amount != null)
<div style="margin-top: 10px; display: block;">
	<div style="width: 75%; display: inline-block; text-align: right;">
		<strong>{{translate('words.vat')}}</strong> 
	</div>
	<div style="width: 25%; display: inline-block; text-align: right;">
		<span style="display: block; text-align: right; padding-right: 5px;">&euro; {{$data->vat_amount}}</span>
	</div>
</div>
@endif
<div style="margin-top: 10px; display: block;">
	<div style="width: 75%; display: inline-block; text-align: right;">
		<strong>{{translate('words.total')}}</strong>
	</div>
	<div style="width: 25%; display: inline-block; text-align: right;">
		<span style="display: block; text-align: right; padding-right: 5px;">&euro; {{$data->total_amount}}</span>
	</div>
</div>
<div style="margin-top: 0px; display: block;">
	<div style="display: block; padding-bottom: 0px;">
		{{translate('words.payment_date')}} : {{\Carbon\Carbon::parse($data->payment_date)->format('d-m-Y')}}		
	</div>
</div>
<div class="row">
   <hr>
	<div style="padding: 20px 0 10px; text-align: center;">
		
		{{translate('words.organization_details')}}
	</div>
</div>