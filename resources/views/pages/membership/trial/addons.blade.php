@extends('layouts.main')
@section('content')
@inject('user', 'App\Services\Membership')
@inject('storage', 'App\Services\Membership')
<div class="panel-body pl50 pr50" id="modify">
    <h2><b>{{translate('form.convinced')}} ?</b></h2>
    <h4>{{translate('words.how_meny_users_you_want_to_use_on_too_doo')}}?</h4>
    {!! Form::open(['method' => 'post', 'url' => '/membership/choose-addons/', 'autocomplete' => 'off', 'class' => 'form-horizontal','id'=>'cmpDetailFrm']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="useraddonsection text-center" id="useraddonsection">
                @foreach($userPlans as $k => $each)
                <div class="radio-custom radio-system mb5">
                    <input type="radio" value="{{($each['is_base'] == 1) ? 0 : $each['id']}}" class="usr-addon-radio" id="radio{{$k}}" data-max="{{$each['is_max']}}" data-price="{{$each['price_per_month']}}" name="addons[user]" @if($each['id'] == '1' || (!empty($user->user($details)) and $user->user($details) == $each['id'])) checked @endif />
                    
                    @if($each['is_max'] == 1)
                    <label for="radio{{$k}}">
                        {{translate('words.more_than')}} {{$each['user_count']}} {{translate('words.users')}}
                    </label>
                    @else
                    <label for="radio{{$k}}">{{$each['user_count']}} {{translate('words.users')}}</label>
                    @endif
                </div>
                @endforeach 
            </div>
            <div class="addondetails">
                <h4 id="userCountPrice"></h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            {!! Form::hidden('hid', !empty($details) ? $details->id : '') !!}
            {!! Form::hidden('tenure', $subscriptionTenure, array('id' => 'subscriptionTenure')) !!}
            {!! Html::link(url('/membership/choose-iso'), translate('tutorial.back_button'), array('class' => 'btn btn-default')) !!}
            {!! Form::submit(translate('tutorial.next_button'), array('class' => 'btn text-default btn-system', 'id' => 'cmpdetail-btn', 'tabindex' => '5')) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
@include('includes.scripts.choose_addon_script')
@stop