@extends('layouts.main')
@section('content')
<div class="panel-body pl50 pr50" id="modify">
    <h2><b>{{translate('words.convinced')}}</b></h2>
    <h3>{{translate('form.just_some_formalities')}}</h3>
    <b>{{translate('form.checking_address')}}</b>
    {!! Form::open(['method' => 'post', 'url' => '/membership/company-details/', 'autocomplete' => 'off', 'class' => 'form-horizontal','id'=>'cmpDetailFrm', 'v-on:change' => 'validate()']) !!}
    <div class="row">
        <div class="col-md-6">
            <div class="section">
                {!! Form::label('street', translate('form.street'), array('class' => 'field-label fs15 mb5 required')) !!}
                <label class="field prepend-icon">
                    {!! Form::text('street', (isset($companyDetail->street)) ? $companyDetail->street : old('street'), array('class' => 'gui-input', 'id' => 'street', 'placeholder' => translate('form.street'), 'required' => 'required', 'tabindex' => '1')) !!}
                    <label for="street-name" class="field-icon">
                        <i class="fa fa-road"></i>
                    </label>
                </label>
                <p class="text-danger mn">{{$errors->first('street')}}</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="section">
                {!! Form::label('nr', translate('form.nr'), array('class' => 'field-label fs15 mb5 required')) !!}
                <label class="field prepend-icon">
                    {!! Form::text('nr', (isset($companyDetail->nr)) ? $companyDetail->nr : old('nr'), array('class' => 'gui-input', 'id' => 'street', 'placeholder' => 'Nr', 'required' => 'required', 'tabindex' => '2')) !!}
                    <label for="nr" class="field-icon">
                        <i class="fa fa-list-ol"></i>
                    </label>
                </label>
                <p class="text-danger mn">{{$errors->first('nr')}}</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="section">
                {!! Form::label('bus', translate('form.bus'), array('class' => 'field-label fs15 mb5')) !!}
                <label class="field prepend-icon">
                    {!! Form::text('bus',(isset($companyDetail->bus)) ? $companyDetail->bus : old('bus'), array('class' => 'gui-input', 'id' => 'bus', 'placeholder' => translate('form.bus'),'tabindex' => '3')) !!}
                    <label for="bus-name" class="field-icon">
                        <i class="fa fa-road"></i>
                    </label>
                </label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="section">
                {!! Form::label('zip', translate('form.zip_code'), array('class' => 'field-label fs15 mb5 required')) !!}
                <label class="field prepend-icon">
                    {!! Form::text('zip', (isset($companyDetail->zip)) ? $companyDetail->zip : old('zip'), array('class' => 'gui-input', 'id' => 'zip_code', 'placeholder' => translate('form.zip_code'), 'required' => 'required', 'tabindex' => '4')) !!}
                    <label for="zip" class="field-icon">
                        <i class="fa fa-list-ol"></i>
                    </label>
                </label>
                <p class="text-danger mn" id="zip-error">{{$errors->first('zip')}}</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="section">
                {!! Form::label('city', translate('form.city'), array('class' => 'field-label fs15 mb5 required')) !!}
                <label class="field prepend-icon">
                    {!! Form::text('city', (isset($companyDetail->city)) ? $companyDetail->city : old('city'), array('class' => 'gui-input', 'id' => 'city', 'placeholder' => translate('form.city'), 'required' => 'required', 'tabindex' => '5')) !!}
                    <label for="city-name" class="field-icon">
                        <i class="fa fa-road"></i>
                    </label>
                </label>
                <p class="text-danger mn">{{$errors->first('city')}}</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="section">
                {!! Form::label('country', translate('form.country'), array('class' => 'field-label fs15 mb5 required')) !!}
                <label class="field select">
                    {!! Form::select('comp_country', $countries, Auth::guard('customer')->user()->company->country, array('id' => 'country', 'required' => 'required', 'tabindex' => '6')) !!}
                    <i class="arrow"></i>
                </label>
                <p class="text-danger mn">{{$errors->first('comp_country')}}</p>
            </div>
        </div>
    </div>
    <h3>{{translate('form.vat_no_please')}}</h3>
    <div class="row">
        <div class="col-md-6">
            <div class="section">
                {!! Form::label('tva_no', translate('form.tva_no'), array('class' => 'field-label fs15 mb5 required', 'id' => 'tva_label')) !!}
                <label class="field prepend-icon">
                    {!! Form::text('tva_no', (isset($companyDetail->tva_no)) ? $companyDetail->tva_no : old('tva_no'), array('class' => 'gui-input', 'id' => 'tva', 'placeholder' => translate('form.tva_no'), 'required' => 'required', 'tabindex' => '7')) !!}
                    <label for="tva-name" class="field-icon">
                        <i class="fa fa-list-ol"></i>
                    </label>
                </label>
                <p class="text-danger mn" id="tva-error">{{$errors->first('tva_no')}}</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="section">
                {!! Form::label('is_apply_vat', translate('form.does_not_applied_vat'), array('class' => 'field-label fs15 mb5 required')) !!}
                <label class="switch-extended switch-success">
                    {!! Form::checkbox('is_apply_vat', 1, $companyDetail->is_vat, ['id' => 'is_apply_vat']) !!}
                    <div class="slider-extended round"></div>
                </label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            {!! Form::hidden('compid', Auth::guard('customer')->user()->company_id) !!}
            {!! Form::hidden('hid', Auth::guard('customer')->user()->id) !!}
            {!! Html::link(url('/membership/modify'), translate('tutorial.back_button'), array('class' => 'btn btn-default', 'tabindex' => '9')) !!}
            {!! Form::submit(translate('tutorial.next_button'), array('class' => 'btn text-default btn-system', 'id' => 'cmpdetail-btn', 'tabindex' => '8')) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
@include('includes.scripts.company_detail_script')
@stop