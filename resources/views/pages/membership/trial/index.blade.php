@extends('layouts.main')
@section('content')
	<div class="row">
		<div class="col-md-12 text-center">
			<br clear="all">
			{{translate('words.we_are_sorry')}}<br clear="all">
			{{translate('words.trial_period_over')}}<br clear="all"><br clear="all">
			{{translate('words.optimize_your_company')}}<br clear="all">
			{{translate('words.become_a_customer')}}<br clear="all"><br clear="all">
			{{translate('words.you_lost_nothing')}}<br clear="all"><br clear="all">
		</div>
	</div>
	<div class="row">
        <div class="col-md-12 text-center">
            {!! Html::link(url('/out'), translate('tutorial.back_button'), ['class' => 'ml10 btn btn-hover btn-default']) !!}
            {!! Html::link(url('/membership/detail'), translate('words.become_a_customer'), ['class' => 'btn btn-system']) !!}
			<br clear="all"><br clear="all">
			{!! Html::link('https://too-doo.be', translate('words.need_more_info'), ['class' => 'text-system']) !!}
			<br clear="all"><br clear="all">
        </div>
    </div>
@stop