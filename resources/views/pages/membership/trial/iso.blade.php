@extends('layouts.main')
@section('content')
@inject('monthlyPkgCalc', 'App\Services\Membership')
@inject('packageName', 'App\Services\Membership')
@inject('packageDescription', 'App\Services\Membership')
{!! Form::open(array('method' => 'post', 'url' => '/membership/choose-iso')) !!}
<section id="content" class="table-layout animated fadeIn">
	<div class="tray-center">
		<div class="row">
			<div class="col-md-12 fs22 fw700">
				{{translate('words.convinced')}}
			</div>
		</div>
		<div class="row">
			@if($iso->count() > 0)
			@foreach($iso as $each)
			<div class="col-sm-4">
				<label for="chk{{$loop->iteration}}" class="br-a mb20 br-grey overview-segment-head @if($each->is_base == 1 || (!empty($details) and in_array($each->id, $selected))) active @endif" data-id="{{$each->id}}" data-base="{{$each->is_base}}" data-price="{{$each->price}}">
					<div class="panel-title">
						<span class="fs16 fw700 w100 p5">
							{{$packageName->packageName($each)}}
						</span>
						@if($each->is_base == 1 and $iso->count() > 1)
						<span class="pull-right w130 fs14 bg-system text-default p5">
							{{translate('words.base_package')}}
						</span>
						@endif
					</div>
					<div class="panel-body">
						@if($each->is_base == 1)
							<div class="mt10">
								{{translate('words.select_tenure')}}
								<select class="form-control" name="tenure" data-currency="&euro;">
									<option value="1" {{(Auth::guard('customer')->user()->company->subscription_tenure == "1") ? 'selected=selected' : ''}}>
										&euro; {{$monthlyPkgCalc->monthlyPkgCalc(floatval($each->price))}}  / {{translate('form.month')}}
									</option>
									
								</select>
							</div>
						@endif
						<div class="count mt30">
							<div class="fw300 time fs14">
								{!!$packageDescription->packageDescription($each)!!}
							</div>
						</div>
					</div>
					<div class="panel-footer">										
						<span class="fs20 fw700 text-system pl20 package-price" data-isbase="{{$each->is_base}}" data-price="{{floatval($each->price)}}" data-monthlyprice="{{$monthlyPkgCalc->monthlyPkgCalc(floatval($each->price))}}">
							@if($each->is_base != 1) + @endif &euro; {{$monthlyPkgCalc->monthlyPkgCalc(floatval($each->price))}} /{{translate('form.month')}}
						</span>
					</div>
				</label>
				{!! Form::checkbox('packages[]', $each->id, ($each->is_base == 1 || (!empty($details) and in_array($each->id, $selected))), ['id' => ('chk'.$loop->iteration), 'class' => 'hide']) !!}
			</div>
			@endforeach
			@endif			
		</div>
		<div class="row">
			<div class="col-md-12">
				{!! csrf_field() !!}
				{!! Form::hidden('hid', !empty($details) ? $details->id : '') !!}
				{!! Html::link(url('/membership/company-details'), translate('tutorial.back_button'), array('class' => 'btn btn-default')) !!}
				{!! Form::submit(translate('tutorial.next_button'), array('class' => 'btn text-default btn-system')) !!}
			</div>
		</div>
	</div>
</section>
{!! Form::close() !!}

<script type="text/javascript">
	$(document).ready(function() {
		var oneTime = 0;
		if (oneTime == 0) {
			oneTime++;
			var segment = $('select[name="tenure"] :selected').val();
			var currency = $('select[name="tenure"]').data('currency');
			$('.package-price').each(function (each) {
				var span = (segment == 2) ? " " + " / {{translate('form.year')}}" : " " + " / {{translate('form.month')}}";
				var isBase = ($(this).data('isbase') == 1) ? " " : "+";
				var newPrice = (segment == 2) ? $(this).data('price') * 12 : $(this).data('monthlyprice');
				var result = currency + isBase + newPrice + span;
				$(this).text(result);
			});
		}



		//Making all iso boxes size of same
		if ($('.overview-segment-head').length > 0) {
			var boxHeight = 0;
			var boxWidth = 0;
			$('.overview-segment-head').each(function () {
				if(parseInt($(this).find('.panel-body').css('height')) > boxHeight) {
					boxHeight = $(this).find('.panel-body').css('height');
				}
				if(parseInt($(this).find('.panel-body').css('width')) > boxWidth) {
					boxWidth = $(this).find('.panel-body').css('width');
				}
			});
			$('.overview-segment-head').find('.panel-body').css('height', boxHeight);
			$('.overview-segment-head').find('.panel-body').css('width', boxWidth);
		}
		//===============================//

		$('.overview-segment-head').click(function () {
			if ($(this).data('base') == 0) {
				var price = $('#price').val();
				if (!$(this).hasClass('active')) {
					$(this).addClass('active');
					price = parseInt(price) + parseInt($(this).data('price'));
					$('#price').val(price);
				} else {
					$(this).removeClass('active');
					price = parseInt(price) - parseInt($(this).data('price'));
					$('#price').val(price);
				}
			} else {
				return false;
			}
		});

		$('select[name="tenure"]').change(function () {
			var segment = $(this).val();
			var currency = $(this).data('currency');
			$('.package-price').each(function (each) {
				var span = (segment == 2) ? " " + " / {{translate('form.year')}}" : " " + " / {{translate('form.month')}}";
				var isBase = ($(this).data('isbase') == 1) ? " " : "+";
				var newPrice = (segment == 2) ? $(this).data('price') * 12 : $(this).data('monthlyprice');
				var result = currency + isBase + newPrice + span;
				$(this).text(result);
			});
		});
	});
</script>
@stop