@extends('layouts.main')
@section('content')
	<div class="p15 text-center">
		<div class="row">
			<div class="col-md-12">
				<h2>{{translate('words.convinced')}}</h2>
			</div>
		</div>
		<div class="row mt20">
			<div class="col-md-12">
				{{translate('form.just_some_formalities')}}
			</div>
		</div>
		<div class="row mt20">
			<div class="col-md-12">
				<b>{{translate('form.data_still_correct')}}</b>
			</div>
		</div>
		<div class="row mt20">
			<div class="col-md-12">
				{{Auth::guard('customer')->user()->company->name}}<br clear="all">
				{{Auth::guard('customer')->user()->fname}} {{Auth::guard('customer')->user()->lname}}
				<br clear="all">
				{{Auth::guard('customer')->user()->phone}}<br clear="all">
			</div>
		</div>
		<div class="row mt20">
			<div class="col-md-6 text-right pt5">
				{!! Html::link(url('/membership/modify'), translate('words.no_modify'), ['class' => 'text-system']) !!}
			</div>
			<div class="col-md-6 text-left">
				{!! Html::link(url('/membership/company-details'), translate('words.yes_correct'), ['class' => 'btn btn-system']) !!}
			</div>
		</div>
	</div>
@stop