@extends('layouts.main')
@section('content')
    {!! Html::style('css/extra-styles/membership_modify.css') !!}
	<div class="panel-body pl50 pr50" id="modify">
    	{!! Form::open(['method' => 'post', 'url' => '/membership/modify', 'autocomplete' => 'off', 'class' => 'form-horizontal', 'v-on:change' => 'validate()']) !!}
    		<div class="row">
                <div class="col-md-12">
                    <div class="section">
                        {!! Form::label('company-name', translate('form.company_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('company_name', '', array('class' => 'gui-input', 'id' => 'company-name', 'placeholder' => translate('form.company_name'), 'required' => 'required', 'tabindex' => '1', 'v-model' => 'companyname', 'v-bind:class' => 'companyname_class')) !!}
                            <label for="company-name" class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="section">
                        {!! Form::label('first-name', translate('form.first_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('first_name', '', array('class' => 'gui-input', 'id' => 'first-name', 'placeholder' => translate('form.first_name'), 'required' => 'required', 'tabindex' => '2', 'v-model' => 'firstname', 'v-bind:class' => 'firstname_class')) !!}
                            <label for="first-name" class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="section">
                        {!! Form::label('last-name', translate('form.last_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('last_name', '', array('class' => 'gui-input', 'id' => 'last-name', 'placeholder' => translate('form.last_name'), 'required' => 'required', 'tabindex' => '3', 'v-model' => 'lastname', 'v-bind:class' => 'lastname_class')) !!}
                            <label for="last-name" class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="section">
                        {!! Form::label('phone', translate('form.phone_optional'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('phone', '', array('class' => 'gui-input', 'id' => 'phone', 'placeholder' => translate('form.phone_optional'), 'tabindex' => '4', 'v-model' => 'phone')) !!}
                            <label for="phone" class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="section">
                        {!! Form::label('email', translate('form.email'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field prepend-icon">
                            {!! Form::text('email', '', array('class' => 'gui-input', 'id' => 'email', 'placeholder' => translate('form.email'), 'required' => 'required','readonly'=>'readonly','tabindex' => '5', 'v-model' => 'email', 'v-bind:class' => 'email_class')) !!}
                            <label for="email" class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('email')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-3">
            		{!! Form::hidden('compid', Auth::guard('customer')->user()->company_id) !!}
            		{!! Form::hidden('hid', Auth::guard('customer')->user()->id) !!}
            		{!! Form::submit(translate('form.save_button'), array('class' => 'btn text-default btn-system', 'id' => 'profile-btn', 'tabindex' => '5', ':disabled' => '!enabled')) !!}
            	</div>
            </div>
    	{!! Form::close() !!}
    </div>
    @include('includes.scripts.modify_script')
@stop