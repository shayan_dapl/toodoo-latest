@extends('layouts.main')
	@section('content')
	@inject('space', 'App\Services\Membership')
	@inject('total', 'App\Services\Membership')
	@inject('tenureprice', 'App\Services\Membership')
	@inject('packageName', 'App\Services\Membership')
	<section id="content">
		<div class="row">
			<div class="col-md-12 fs20 fw700 mb20">
				{{translate('words.convinced')}}
			</div>
		</div>
		@if (!empty($data))
			@inject('packageprice', 'App\Services\Membership')
			@foreach($data['iso'] as $each)
				<div class="row mb5">
					<div class="col-md-5">
						{{translate('words.package')}} : {{$packageName->packageName($each)}}
					</div>
					<div class="col-md-3 text-right">
						&euro; {{$tenureprice->tenureprice($each->price, $subscriptionTenure)}} {{($subscriptionTenure == 1) ? translate('words.per_month') : translate('words.per_year')}}
					</div>
					<div class="col-md-3">
						{!! Html::link( url('/membership/choose-iso'), translate('tutorial.back_button'), array('class' => 'text-system')) !!}
					</div>
				</div>
			@endforeach
			@if (!empty($data['user']) and $data['user']->is_base == 0)
				<div class="row mb5">
					<div class="col-md-5">
						{{translate('words.extra_user')}} : {{$data['user']->user_count}} {{translate('words.users')}}
					</div>
					<div class="col-md-3 text-right">
						&euro; {{$tenureprice->tenureprice($data['user']->price_per_month, $subscriptionTenure)}} {{($subscriptionTenure == 1) ? translate('words.per_month') : translate('words.per_year')}}
					</div>
					<div class="col-md-3">
						{!! Html::link( url('/membership/choose-addons'), translate('tutorial.back_button'), array('class' => 'text-system')) !!}
					</div>
				</div>
			@endif
			@if (!empty($data['storage']) and $data['storage']->is_base == 0)
				<div class="row">
					<div class="col-md-5">
						{{translate('words.extra_storage')}} : {{$space->space($data['storage']->space)}} GB
					</div>
					<div class="col-md-3 text-right">
						&euro; {{$data['storage']->price_per_month}} {{translate('words.per_month')}}
					</div>
					<div class="col-md-3">
						{!! Html::link(url('/membership/choose-addons'), translate('tutorial.back_button'), array('class' => 'text-system')) !!}
					</div>
				</div>
			@endif
		@endif
		@if ($data['company']->company->is_vat == 0 and $data['company']->company->countries->vat_percent != '')
			<div class="row">
				<div class="col-md-5">
					{{ translate('words.vat')}} ({{$vat}} %)
				</div>
				<div class="col-md-3 text-right">
					&euro; {{$total->total($data, '', $subscriptionTenure)['vat']}}
				</div>
			</div>
		@endif
		<div class="row mt20">
			<div class="col-md-3 fw700 fs16">
				{{translate('words.total')}}
			</div>
			<div class="col-md-5 fw600 fs14 text-right">
				&euro; {{number_format(($total->total($data, null, $subscriptionTenure)['total'] + $total->total($data , null, $subscriptionTenure)['vat']), 2)}} {{($subscriptionTenure == 1) ? translate('words.per_month') : translate('words.per_year')}}
			</div>
		</div>
		@if ($total->total($data , null, $subscriptionTenure)['total'] > 0)
			@if($data['company']->company->is_vat == 1)
			<div class="row mt20">
				<div class="col-md-12">
					{{translate('words.vat_excluded_price')}}
				</div>
			</div>
			@endif
		@endif
		<div class="row mt20">
		   	<div class="col-md-12">
		   		{!! Form::open(['method' => 'post', 'url' => '/membership/make-payment/']) !!}
					{!! csrf_field() !!}
					{!! Form::hidden('company_id', $data['company']->company_id) !!}
					{!! Form::hidden('id', $data['company']->id) !!}
					{!! Form::hidden('name', Auth::guard('customer')->user()->name) !!}
					{!! Form::hidden('email', Auth::guard('customer')->user()->email) !!}
					{!! Form::hidden('amount', ($total->total($data, '', $subscriptionTenure)['total'] + $total->total($data, '', $subscriptionTenure)['vat'])) !!}
					{!! Form::hidden('base_amount', $total->total($data, '', $subscriptionTenure)['total']) !!}
					{!! Form::hidden('package_price', $packageprice->packageprice($data['iso'], $subscriptionTenure)) !!}
					{!! Form::hidden('user_package_price', !empty($data['user']) ? $tenureprice->tenureprice($data['user']->price_per_month, $subscriptionTenure) : '0.00') !!}
					{!! Form::hidden('storage_package_price', !empty($data['storage']) ? $data['storage']->price_per_month : '0.00') !!}
					{!! Form::hidden('vat', $total->total($data, '', $subscriptionTenure)['vat']) !!}
					{!! Html::link(url('/membership/choose-addons'), translate('tutorial.back_button'), ['class' => 'ml10 btn btn-hover btn-default']) !!}
					{!! Form::submit(translate('words.pay'), array('class' => 'btn text-default btn-system', 'id' => 'cmpdetail-btn', 'tabindex' => '5')) !!}
				{!! Form::close() !!}
		  	</div>
		</div>
	</section>
@stop