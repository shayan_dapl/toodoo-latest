@extends('layouts.default')
@section('content')
<div class="form-group mt15 disp-flex">
    <div class="form-group posfilter disp-flex pull-right">
    <span>{{ucfirst(translate('words.filter'))}} :</span>
    {!! Form::select('branch', $branches, isset($selectedBranch) ? $selectedBranch : '', ['class' => 'form-control w200 ml5 issuecategory', 'id' => 'branch-opt']) !!}
    </div>
</div>
<h6 class="text-danger">[{{translate('words.click_on_process')}}]</h6>
<div class="panel-heading exhight">
    <div class="panel-title hidden-xs">
    {{translate('words.active_process')}}
    </div>
</div>
<div class="row">
    @if(!empty($details))
        @inject('name', 'App\Services\ProcessCategorized')
        @inject('processes', 'App\Services\ProcessCategorized')
        @foreach($details as $eachDetail)
            <div class="col-md-4">
                <div class="panel">
                    <div class="panel-heading bg-system">
                        @if($loop->iteration == 1)
                            <i class="fa fa-cubes"></i>
                        @elseif($loop->iteration == 2)
                            <i class="fa fa-cog"></i>
                        @else
                            <i class="fa fa-handshake-o"></i>
                        @endif
                        <span class="panel-title"><b>{{$name->name(session('lang'), $eachDetail)}}</b></span>
                    </div>    
                    <div class="panel-body panel-body-ext turtleBox p5">

                        <ul class="process-sortable-icon h-a">
                            @if(!empty($eachDetail->processes))
                                @foreach($processes->processes($eachDetail) as $eachProcess)
                                    @if(in_array($eachProcess->id, $filteredProcess))
                                        @if($eachProcess->no_turtle_process == 0)
                                            <li class="process-cls cropper-crop cross-hair" data-id="{{$eachProcess->id}}" data-toggle="tooltip" data-placement="top" title="{{translate('form.process_owner')}} : {{$eachProcess->owner->name}}">
                                                <span onmousedown="startT()" onmouseup="endT(this, {{$eachProcess->id}})" class="pointer">
                                                    {{ucfirst($eachProcess->name)}} 
                                                </span>
                                                @if(!empty($eachProcess->children))
                                                <span class="pull-right text-danger">
                                                    <i class="fa fa-plus showchildren z2 pointer p3" data-id="{{$eachProcess->id}}"></i>
                                                </span>
                                                <div class="turtleBox" id="{{$eachProcess->id}}" style="display: none">
                                                    <ul class="text-danger h-a">
                                                        @foreach($eachProcess->children as $child)
                                                        <li>
                                                            @if($child->no_turtle_process == 0)
                                                            <span onmousedown="startT()" onmouseup="endT(this, {{$child->id}})" class="pointer">
                                                                {{$child->name}}
                                                            </span>
                                                            @else
                                                            <i class="fa fa-eye-slash cropper-crop"></i> 
                                                            {{$child->name}}
                                                            @endif
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                @endif
                                            </li>
                                        @else
                                            <li class="process-cls cross-hair" data-toggle="tooltip" data-placement="top" title="{{translate('form.process_owner')}} : {{$eachProcess->owner->name}}">
                                                <span onmousedown="startT()" onmouseup="endT(this, {{$eachProcess->id}})" data-id="{{$eachProcess->id}}">
                                                    <i class="fa fa-eye-slash cropper-crop"></i> {{$eachProcess->name}}
                                                </span>
                                                @if(!empty($eachProcess->children))
                                                <span class="pull-right text-danger">
                                                    <i class="fa fa-plus showchildren z2 pointer p3" data-id="{{$eachProcess->id}}"></i>
                                                </span>
                                                <div class="turtleBox" id="{{$eachProcess->id}}" style="display: none">
                                                    <ul class="text-danger h-a">
                                                        @foreach($eachProcess->children as $child)
                                                        <li>
                                                            @if($child->no_turtle_process == 0)
                                                            <span onmousedown="startT()" onmouseup="endT(this, {{$child->id}})" class="pointer">
                                                                {{$child->name}}
                                                            </span>
                                                            @else
                                                            <i class="fa fa-eye-slash cropper-crop"></i> 
                                                            {{$child->name}}
                                                            @endif
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                @endif
                                            </li>
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                            @if(count($eachDetail->processes) == 0)
                                <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="text-center">
            <h1>{{translate('table.no_assigned_process')}}</h1>
        </div>
    @endif
</div>

<div class="panel-heading exhight">
    <div class="panel-title hidden-xs">
    {{translate('words.inactive_process')}}
    </div>
</div>
<div class="row">
    @if(!empty($details))
        @inject('name', 'App\Services\ProcessCategorized')
        @inject('inactiveprocesses', 'App\Services\ProcessCategorized')
        @foreach($details as $eachDetail)
            <div class="col-md-4">
                <div class="panel">
                    <div class="panel-heading bg-system">
                        @if($loop->iteration == 1)
                            <i class="fa fa-cubes"></i>
                        @elseif($loop->iteration == 2)
                            <i class="fa fa-cog"></i>
                        @else
                            <i class="fa fa-handshake-o"></i>
                        @endif
                        <span class="panel-title"><b>{{$name->name(session('lang'), $eachDetail)}}</b></span>
                    </div>
                    <div class="panel-body panel-body-ext turtleBox p5">
                        <ul class="process-sortable-icon h-a">
                            @if(!empty($eachDetail->processes))
                                @foreach($processes->inactiveprocesses($eachDetail) as $eachProcess)
                                @if(in_array($eachProcess->id, $filteredProcess))
                                    @if($eachProcess->no_turtle_process == 0)
                                        <li class="process-cls cropper-crop cross-hair" data-id="{{$eachProcess->id}}" data-toggle="tooltip" data-placement="top" title="{{translate('form.process_owner')}} : {{$eachProcess->owner->name}}">
                                            <span onmousedown="startT()" onmouseup="endT(this, {{$eachProcess->id}})" class="pointer">
                                                {{ucfirst($eachProcess->name)}} 
                                            </span>
                                            @if(!empty($eachProcess->children))
                                            <span class="pull-right text-danger">
                                                <i class="fa fa-plus showchildren z2 pointer p3" data-id="{{$eachProcess->id}}"></i>
                                            </span>
                                            <div class="turtleBox" id="{{$eachProcess->id}}" style="display: none">
                                                <ul class="text-danger h-a">
                                                    @foreach($eachProcess->children as $child)
                                                    <li>
                                                        @if($child->no_turtle_process == 0)
                                                        <span onmousedown="startT()" onmouseup="endT(this, {{$child->id}})" class="pointer">
                                                            {{$child->name}}
                                                        </span>
                                                        @else
                                                        <i class="fa fa-eye-slash cropper-crop"></i> 
                                                        {{$child->name}}
                                                        @endif
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                        </li>
                                    @else
                                        <li class="process-cls">
                                            <span onmousedown="startT()" onmouseup="endT(this, {{$eachProcess->id}})" data-id="{{$eachProcess->id}}">
                                                <i class="fa fa-eye-slash cropper-crop"></i> {{$eachProcess->name}}
                                            </span>
                                            @if(!empty($eachProcess->children))
                                            <span class="pull-right text-danger">
                                                <i class="fa fa-plus showchildren z2 pointer p3" data-id="{{$eachProcess->id}}"></i>
                                            </span>
                                            <div class="turtleBox" id="{{$eachProcess->id}}" style="display: none">
                                                <ul class="text-danger h-a">
                                                    @foreach($eachProcess->children as $child)
                                                    <li>
                                                        @if($child->no_turtle_process == 0)
                                                        <span onmousedown="startT()" onmouseup="endT(this, {{$child->id}})" class="pointer">
                                                            {{$child->name}}
                                                        </span>
                                                        @else
                                                        <i class="fa fa-eye-slash cropper-crop"></i> 
                                                        {{$child->name}}
                                                        @endif
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                        </li>
                                    @endif
                                 @endif    
                                @endforeach
                            @endif
                            @if(count($eachDetail->processes) == 0)
                                <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="text-center">
            <h1>{{translate('table.no_assigned_process')}}</h1>
        </div>
    @endif
</div>
@include('includes.scripts.categorized_process_list_script')
@stop