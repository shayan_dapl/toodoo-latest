@extends('layouts.default')
@section('content')
@inject('rating', 'App\Services\ProcessCompetent')
@inject('ratingid', 'App\Services\ProcessCompetent')
@inject('fieldwidth', 'App\Services\ProcessCompetent')
<span id="app">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-visible">
                <div class="panel-heading exhight">
                    {{translate('words.competent_metrix')}}
                    <span class="pull-right">
                    @if (!empty($ratingLevel))
                        @foreach ($ratingLevel as $each)
                            <span class="mr5">
                                <strong>{{$each['name']}}</strong>: {{$each['description']}}
                            </span>
                        @endforeach
                    @endif
                    <a class="btn bg-system ml10 text-default" href="{{url('/process/job-description-metrix'.$competentParams)}}">
                        {{ ucfirst(translate('tutorial.back_button')) }}
                    </a>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="row mb10">
                        <div class="col-md-6 text-right pt10">
                            {{ucfirst(translate('words.filter'))}} :
                        </div>
                        <div class="col-md-2">
                            {!! Form::select('branch', $branches, isset($selectedBranch) ? $selectedBranch : '', ['class' => 'form-control', 'id' => 'branch-filter']) !!}
                        </div>
                        <div class="col-md-2">
                            <select class="form-control filter" id="position-filter">
                                <option value="">{{translate('words.select_functions')}}</option>
                                @foreach($allPositions as $eachPosition)
                                    <option value="{{$eachPosition['position_id']}}" {{($positionId == $eachPosition['position_id']) ? 'selected' : ''}}>{{$eachPosition['positionname']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control filter" id="rating-filter">
                                <option value="">{{translate('words.select_rating')}}</option>
                                @if (!empty($ratingLevel))
                                    @foreach($ratingLevel as $eachRating)
                                        <option value="{{$eachRating['id']}}" {{($selectedRating == $eachRating['id']) ? 'selected' : ''}}>{{$eachRating['name']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        @if(!empty($positions))
                            <div class="competent-table-wrapper">
                                <div class="competent-table-scroller">
                                    <table id="compitentTable" class="competent-table-table table table-bordered jobDescTable table-striped">
                                        <thead class="bg-system">
                                            <tr>
                                                <th class="competent-table-sticky-col bg-system pt25">{{translate('form.process_name')}}</th>
                                                <th class="competent-table-sticky-col1 bg-system pt25">{{translate('words.process_steps')}}</th>
                                                @if(!empty($positions))
                                                @foreach($positions as $posId => $posName)
                                                <th class="text-center">{{$posName}}</th>
                                                @endforeach
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="competent-table-sticky-col">&nbsp;</td>
                                                <td class="competent-table-sticky-col1">&nbsp;</td>
                                                @foreach($positions as $key=>$val)
                                                <td class="noPadding">
                                                    <table class="table tableNoBorder subTableHeading">
                                                        <thead>
                                                            <tr>
                                                                @foreach($structures[$key]->competentstructure($orgIds)->get() as $eachStructure)
                                                                    @if(!empty($eachStructure->user))
                                                                    <th width="{{$fieldwidth->fieldwidth($structures[$key]->competentstructure($orgIds)->count())}}%">{{$eachStructure->user->name}}</th>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </td>
                                                @endforeach
                                            </tr>
                                            @foreach($processData as $keyProces => $processdata)
                                            @foreach($processData[$keyProces] as $eachstep => $positionId)
                                            <tr>
                                                @if($loop->iteration == 1)
                                                <td class="competent-table-sticky-col bg-default">
                                                    {{$keyProces}}
                                                </td>
                                                @endif
                                                <td class="competent-table-sticky-col1">{{$eachstep}}</td>
                                                @foreach($positions as $key=>$val)
                                                <td class="noPadding">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                @foreach($structures[$key]->competentstructure($orgIds)->get() as $eachStructure)
                                                                    @if(in_array($key, $processData[$keyProces][$eachstep]))
                                                                        <td 
                                                                        width="{{$fieldwidth->fieldwidth($structures[$key]->competentstructure($orgIds)->count())}}%" 
                                                                        v-on:click="pushRating({{$positionId[0].''.$processInfo[$keyProces].''.$stepInfo[$keyProces][$eachstep].''.$eachStructure->id}})" 
                                                                        class="text-center pointer" 
                                                                        :ref="{{$positionId[0].''.$processInfo[$keyProces].''.$stepInfo[$keyProces][$eachstep].''.$eachStructure->id}}" 
                                                                        data-position="{{$positionId[0]}}" 
                                                                        data-process="{{$processInfo[$keyProces]}}" 
                                                                        data-step="{{$stepInfo[$keyProces][$eachstep]}}" 
                                                                        data-org="{{$eachStructure->id}}" 
                                                                        data-ratingid="{{$ratingid->ratingid($ratings, $positionId[0], $processInfo[$keyProces], $stepInfo[$keyProces][$eachstep], $eachStructure->id)}}">
                                                                        {{$rating->rating($ratings, $positionId[0], $processInfo[$keyProces], $stepInfo[$keyProces][$eachstep], $eachStructure->id)}}
                                                                        </td>
                                                                    @else
                                                                        <td class="disableTable"></td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                @endforeach
                                            </tr>
                                            @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else
                            <div class="col-md-12 text-center bg-grey p20 mt5">
                                {{translate('table.no_data')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="competentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-default  back4d">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{translate('words.change_rating')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>
                                {{translate('words.choose_rating')}}
                            </label>
                            <select :ref="level" v-model="level" :class="levelClass" tabindex="1">
                                <option value="" data-rating="">{{translate('form.select')}}</option>
                                @if (!empty($ratingLevel))
                                @foreach ($ratingLevel as $each)
                                <option value="{{$each['id']}}" data-rating="{{$each['name']}}">
                                    {{$each['name']}}
                                </option>
                                @endforeach
                                @endif
                            </select>
                            <div class="mt10">
                                <label>
                                   {{translate('words.action_required')}}
                                </label>
                                <label class="switch-extended switch-success rankSwitchAction">
                                    {!! Form::checkbox('', 0, '', ['v-model' => 'is_action']) !!}
                                    <div class="slider-extended round"></div>
                                </label>
                            </div>
                            <div class="mt10" v-show="is_action">
                                <label class="required">
                                    {{translate('words.action_type')}}
                                </label>
                                {!! Form::select('', $actionTypes, '', [':class' => 'actionTypeClass', 'v-model' =>'actionType', 'tabindex' => '1']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>
                                {{translate('form.comment')}}
                            </label>
                            {!! Form::textarea('', '', ['class' => 'gui-input rating-comment', 'v-model' => 'comment', 'placeholder' => translate('form.comment'), 'rows' => '1', 'tabindex' => '2']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" v-show="is_action">
                            <span id="action-segment">
                                <div class="row  mt5">
                                    <div class="col-md-6">
                                        <label class="required">
                                            {{translate('form.action')}}
                                        </label>
                                        {!! Form::text('', '', [':class' => 'actionClass', 'v-model' => 'action', 'placeholder' => translate('form.define_action'), 'tabindex' => '1']) !!}
                                    </div>
                                    <div class="col-md-6">
                                        <label class="required">
                                            {{translate('table.delegate_to')}}
                                        </label>
                                        <br clear="all">
                                        {!! Form::select('', $users, '', [':class' => 'delegateClass', 'v-model' =>'delegate','tabindex' => '2']) !!}
                                    </div>
                                </div>
                               
                                <div class="row  mt5">
                                    <div class="col-md-6">
                                        <label class="required">
                                            {{translate('form.deadline')}}
                                        </label>
                                        {!! Form::text('', '', [':class' => 'deadlineClass', 'v-model' => 'deadline', 'placeholder' => translate('form.define_deadline'), 'readonly' => 'readonly', 'tabindex' => '3']) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::button(translate('form.save_button'), ['class' => 'btn btn-hover btn-system mt25 btn-block py11', 'v-on:click' => 'saveRating']) !!}
                                    </div> 
                                </div>
                            </span>
                        </div> 
                        <div class="col-md-6 border-rad-5 col-md-offset-6" v-show="!is_action">
                            {!! Form::button(translate('form.save_button'), ['class' => 'btn btn-hover btn-system mt25 btn-block py11', 'v-on:click' => 'saveRating']) !!}
                        </div>   
                        <div class="col-md-12 pull-right">
                            <h4 class="text-system"><strong>{{translate('words.rating_log')}}</strong></h4>
                            <table class="table table-bordered table-striped table-hover tablScroll">
                                <thead class="bg-secondary">
                                    <tr>
                                        <th>{{translate('words.rating')}}</th>
                                        <th>{{translate('table.plan_date')}}</th>
                                        <th>{{translate('words.comment')}}</th>
                                        <th>{{translate('words.is_action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-if="list.length > 0" v-for="(each, i) in list">
                                        <td>@{{each.rating_name}}</td>
                                        <td>@{{each.created_at}}</td>
                                        <td>@{{each.comment}}</td>
                                        <td v-if="each.is_action == 1">{{translate('form.yes')}}
                                            <a href="javascript:void(0);" class="text-system" data-html="true" data-toggle="tooltip" data-placement="top" :title="actionStatement(each.actions[0], each.user)">
                                                <i class="fa fa-info-circle"></i>
                                            </a>
                                            <span v-if="each.actions.length > 0 && each.actions[0].closed == 1 && each.actions[0].files.length > 0">
                                                <a class="mr5" :href="file.url" v-for="file in each.actions[0].files" target="_blank">
                                                    <i :class="'fa ' + file.icon"></i>
                                                </a>
                                            </span>
                                        </td>
                                        <td v-if="each.is_action != 1"></td>
                                    </tr>
                                    <tr v-if="list.length == 0">
                                        <td colspan="3" align="center">
                                            {{translate('table.no_data')}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</span>
@include('includes.scripts.competent_metrix_script')
@stop