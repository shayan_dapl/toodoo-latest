@inject('lang', 'App\Services\ProcessDetailView')
@inject('level', 'App\Services\ProcessDetailView')

@if($for == 'Scope')
    <div class="col-md-11">
        <b>{{translate("words.scope")}}:</b> {{ $details->scope }}
    </div>
    <div class="col-sm-1">
        @if($modify == "yes")
        <a href="{{url('/process-step/scope/edit/'.$id)}}" class="btn btn-system btn-sm mb5" >
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        <a href="{{url('/process-step/scope/remove/'.$id)}}" onclick="return confirm('{{translate('alert.are_you_sure')}}')" class="btn btn-danger btn-sm">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
        @endif
    </div>
@endif

@if($for == 'Procedures')
    <div class="col-md-8">
        <b>{{translate("words.doc_reference")}}</b>: {{ $details->doc_ref }}
        <br clear="all">
        @if($details->version != '')
        <b>{{translate("words.version")}}</b>: {{ $details->version }}
        <br clear='all'>
        @endif
        <b>{{translate("words.language")}}</b>: {{ $lang->lang($details->language) }}
        <br clear="all">
        @if($details->document_type != "")
        <b>{{translate("words.document_type")}}</b>: {{ $details->document_type }}
        @endif
    </div>
    <div class="col-md-3 text-center">
    @if($details->version != '')
        @if($modify == "yes")
        <a href="{{url('/process-step/procedure/edit/'.$id.'/version')}}" class="btn btn-default mb10">
            {{translate('words.new_version')}}
        </a>
        <a href="{{url('/process-step/procedure/archive/'.$source_id.'/'.$details->process_id)}}" class="btn btn-default">
            {{translate('words.archive')}}
        </a>
        @endif
    @endif
    </div>
    <div class="col-md-1">
        @if($modify == "yes")
        <a href="{{url('/process-step/procedure/edit/'.$id)}}" class="btn btn-system btn-sm mb5 mb5">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        <a href="{{url('/process-step/procedure/remove/'.$id)}}" onclick="return confirm('{{translate('alert.are_you_sure')}}')" class="btn btn-danger btn-sm">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
        @endif
    </div>
    <br clear="all">
    <br clear="all">
    <div class="row">
        <div class="col-md-12">
        @if(!empty($details->docs))
        <ul class="docContainercls">
            @inject('file', 'App\Services\ProcessDetailView')
            @foreach($details->docs as $eachDoc)
            @if($eachDoc->doc_name != NULL)
            <li>
                <i class="fa {{$file->file($eachDoc->doc_name,'process_docs')['icon']}} fa-3x text-system-faded"></i>
                <a href="{{$file->file($eachDoc->doc_name, 'process_docs')['url']}}" target="_blank">
                    <i class="fa fa-eye fa-1x" aria-hidden="true"></i>
                </a>
                <a  data-doc="{{$eachDoc->doc_name}}" href="{{url('/doc_download/process_docs/'.$eachDoc->doc_name)}}">
                    <i class="fa fa-download text-success"></i> 
                </a>
                <p>{{$eachDoc->doc_name}}</p>
            </li>
            @else
            <li>
            <b>{{translate('form.document_link')}}:</b>
            <p>{{$eachDoc->location_doc}}</p>
            </li>
            @endif
            @endforeach
        </ul>
        @endif
        </div>
    </div>
@endif

@if($for == 'Regulations')
    <div class="col-md-8">
        <b>{{translate("words.doc_reference")}}</b>: {{ $details->doc_ref }}
        <br clear="all">
        @if($details->version != '')
        <b>{{translate("words.version")}}</b>: {{ $details->version }}
        <br clear='all'>
        @endif
        <b>{{translate("words.language")}}</b>: {{ $lang->lang($details->language) }}
        <br clear="all">
        @if($details->regulation_type != "")
        <b>{{translate("words.document_type")}}</b>: {{ $details->regulation_type }}
        @endif
    </div>
    <div class="col-md-3 text-center">
    @if($details->version != '')
        @if($modify == "yes")
        <a href="{{url('/process-step/regulation/edit/'.$id.'/version')}}" class="btn btn-default mb10">
            {{translate('words.new_version')}}
        </a>
        <a href="{{url('/process-step/regulation/archive/'.$source_id.'/'.$details->process_id)}}" class="btn btn-default">
            {{translate('words.archive')}}
        </a>
        @endif
    @endif
    </div>
    <div class="col-md-1">
        @if($modify == "yes")
        <a href="{{url('/process-step/regulation/edit/'.$id)}}" class="btn btn-system btn-sm mb5 mb5">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        <a href="{{url('/process-step/regulation/remove/'.$id)}}" onclick="return confirm('{{translate('alert.are_you_sure')}}')" class="btn btn-danger btn-sm">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
        @endif
    </div>
    <br clear="all">
    <br clear="all">
    <div class="row">
        <div class="col-md-12 max-hght-extended">
        @if(!empty($details->regdocs))
        <ul class="docContainercls">
            @inject('file', 'App\Services\ProcessDetailView')
            @foreach($details->regdocs as $eachDoc)
            @if($eachDoc->doc_name != NULL)
            <li>
                <i class="fa {{$file->file($eachDoc->doc_name,'regulation_docs')['icon']}} fa-3x text-system-faded"></i>
                <a href="{{$file->file($eachDoc->doc_name, 'regulation_docs')['url']}}" target="_blank">
                    <i class="fa fa-eye fa-1x" aria-hidden="true"></i>
                </a>
                <a  data-doc="{{$eachDoc->doc_name}}" href="{{url('/doc_download/regulation_docs/'.$eachDoc->doc_name)}}">
                    <i class="fa fa-download text-success"></i> 
                </a>
                <p>{{$eachDoc->doc_name}}</p>
            </li>
            @else
            <li>
            <b>{{translate('form.document_link')}}:</b>
            <p>{{$eachDoc->location_doc}}</p>
            </li>
            @endif
            @endforeach
        </ul>
        @endif
        </div>
    </div>
@endif

@if($for == 'Kpi')
    @inject('kpiunit', 'App\Services\KpiUnit')
    <div class="col-md-11">
        <b>{{translate('form.kpi_name')}}:</b> {{ $details->name }}
        <br clear="all">
        @if(!empty($details->input))
        <b>{{translate('form.input')}}:</b> {{ $details->input }}
        <br clear="all">
        @endif
        @if($details->masterkpi()->count() > 0)
        <b>{{translate('words.master_kpi')}}:</b> {{ $details->masterkpi()->with('master')->get()->pluck('master.name')->implode(', ') }}
        <br clear="all">
        @endif
        <b>{{translate('form.valid_from')}}:</b> {{ date(Config::get('settings.dashed_date'), strtotime($details->valid_from)) }}
        <br clear="all">
        <b>{{translate('form.valid_to')}}:</b> {{ date(Config::get('settings.dashed_date'), strtotime($details->valid_to)) }}
        <br clear="all">
        <b>{{translate('words.frequency')}}:</b> {{ $frequency[$details->frequency] }}
        <br clear="all">
        <b>{{translate('words.kpi_unit')}}:</b> {{$kpiunit->kpiunit(session('lang'),$details->kpiunit)}}
        <br clear="all">
        <b>{{translate("words.target")}}</b>: {{ $details->target }}
    </div>
    <div class="col-md-1 pr5 text-right">
        @if($modify == "yes")
        <a href="{{url('/process-step/kpi/edit/'.$id)}}" class="btn btn-system btn-sm mb5" >
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        <a href="{{url('/process-step/kpi/remove/'.$id)}}" onclick="return confirm('{{translate('alert.are_you_sure')}}')" class="btn btn-danger btn-sm mb5">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
        <a href="{{url('/process-step/kpi/datalist/'.$id)}}" class="btn btn-system btn-sm" title="{{translate('words.manage_data')}}">
            <i class="fa fa-align-justify" aria-hidden="true"></i>
        </a>
        @endif
    </div>
    <div class="col-md-12">
        <canvas id="process-kpi-graph"></canvas>
    </div>
@endif

@if($for == 'Input')
    <div class="col-md-11">
        <b>{{translate("words.input")}}:</b> {{ $details->name }}
        <br clear="all">
        <b>{{translate("words.supporting_process")}}:</b> {{ $details->supporting_process }}
        <br clear="all">
        <b>{{translate("words.comment")}}:</b> {{ $details->comment }}
    </div>
    <div class="col-md-1">
        @if($modify == "yes")
        <a href="{{url('/process-step/input/edit/'.$id)}}" class="btn btn-system btn-sm mb5" >
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        <a href="{{url('/process-step/input/remove/'.$id)}}" onclick="return confirm('{{translate('alert.are_you_sure')}}')" class="btn btn-danger btn-sm">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
        @endif
    </div>
@endif

@if($for == 'ProcessSteps')
    <div class="col-md-11">
        <b>{{translate("words.steps")}}:</b> {{ $details->step_name }}
        <br clear="all">
        <br clear="all">
        @if(isset($roleDetails))
            @foreach($roleDetails as $role)
            <table border="0" width="100%">
                <tr>
                    <td width="30%">
                        <b>{{$role->position->name}}</b>
                    </td>
                    <td width="10%" class="text-left">
                        @if(in_array('1', explode(",", $role->roles_id)))
                        <b>R</b>
                        @else
                        <b>*</b>
                        @endif
                    </td>
                    <td width="10%" class="text-left">
                        @if(in_array('3', explode(",", $role->roles_id)))
                        <b>C</b>
                        @else
                        <b>*</b>
                        @endif
                    </td>   
                    <td width="10%" class="text-left">
                        @if(in_array('4', explode(",", $role->roles_id)))
                        <b>I</b>
                        @else
                        <b>*</b>
                        @endif
                    </td>
                </tr>
            </table>
            <br clear="all">
            @endforeach
        @endif
    </div>
    <div class="col-md-1">
        @if($modify == "yes")
        <a href="{{url('/process-step/steps/edit/'.$id)}}" class="btn btn-system btn-sm mb5" >
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        <a href="{{url('/process-step/steps/remove/'.$id)}}" onclick="return confirm('{{translate('alert.are_you_sure')}}')" class="btn btn-danger btn-sm">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
        @endif
    </div>
@endif

@if($for == 'Output')
    <div class="col-md-11">
        <b>{{translate("words.output")}}:</b> {{ $details->output }}
        <br clear="all">
        <b>{{translate("words.supporting_process")}}:</b> {{ $details->supporting_process }}
        <br clear="all">
        <b>{{translate("words.comment")}}:</b> {{ $details->comment }}
    </div>
    <div class="col-md-1">
        @if($modify == "yes")
        <a href="{{url('/process-step/output/edit/'.$id)}}" class="btn btn-system btn-sm mb5" >
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        <a href="{{url('/process-step/output/remove/'.$id)}}" onclick="return confirm('{{translate('alert.are_you_sure')}}')" class="btn btn-danger btn-sm">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
        @endif
    </div>
@endif

@if($for == 'Risk')
    <div class="col-md-11">
        <b>{{translate("words.process")}}:</b> {{ $details->process->name }}
        <br clear="all">
        <b>{{translate("words.risk")}}:</b> {{ $details->risk }}
        <br clear="all">
        <b>{{translate("words.level")}}</b>: {{ $level->level($details->risk_level) }}
        <br clear="all">
        <b>{{translate("words.comment")}}</b>: {{ $details->comment }}
        <br clear="all">
        <b>{{translate("words.status")}}</b>: {{isset($action->closed) ? (($action->closed==1)? translate("words.finished") : translate("words.open")) : translate("words.no_action")}}
        @if(isset($action->closed))
            @if($action->closed == 0)
            <br clear="all">
            <b>{{translate("words.open_action")}}</b>: {{$action->action}}
            @endif
        @endif
    </div>
    <div class="col-md-1">
        @if($modify == "yes")
        <a href="{{url('/process-step/risk/remove/'.$id)}}" onclick="return confirm('{{translate('alert.are_you_sure_all_related_actions_will_also_be_deleted')}}')" class="btn btn-danger btn-sm mb5" title="{{translate('words.remove')}}">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
        @endif
        @if(!empty($action))
            <a href="javascript:void(0);" data-toggle="modal" data-target="#event-log-details" data-riskid="{{$details->id}}" data-type="risk" class="btn btn-success dark text-default mb5" id="event-log" title="{{translate('words.event_logging')}}">
                <i class="fa fa-list"></i>
            </a>
        @endif
    </div>
    @if($modify == "yes" or $commentAllow == "yes")
    <div class="col-md-1">
    @if(!empty($action))
        @if($action->closed == 0)
            <a href="javascript:void(0);" id="post-comment" title="{{translate('form.comment')}}" class="btn btn-success mb5">
                <i class="fa fa-comment"></i>
            </a>
        @else
            <a href="javascript:void(0);" id="add-action" title="{{translate('form.add_action')}}" class="btn btn-system btn-sm">
                <i class="fa fa-plus-circle"></i>
            </a>
        @endif
    @else
        <a href="javascript:void(0);" id="add-action" title="{{translate('form.add_action')}}" class="btn btn-system btn-sm">
            <i class="fa fa-plus-circle"></i>
        </a>
    @endif
    </div>
    @endif
    @if(!empty($action))
        @if(!empty($action->comments('risk')))
            <div class="col-md-11 risk-segment">
                <hr class="mt10 mb10">
                <div class="panel-heading">
                {{translate('form.comment')}} ({{translate('form.comment_on')}})
                </div>
                <ul class="h-a segmentul pt5">
                    @foreach($action->comments('risk') as $eachAction)
                    <li>
                        {{$eachAction->reply}} ({{$eachAction->created_at}})
                    </li>
                    @endforeach
                </ul>
            </div>
        @endif
    @endif
    <div class="row">
        
    </div>
    @if(!empty($action))
    {!! Form::open(array('method' => 'post', 'url' => 'process-step/risk/action/comment', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data', 'class' => 'comment-class hide')) !!}
        <div class="col-md-12">
            <hr class="mt10 mb10">
        </div>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                <label>
                    {{translate('form.comment')}}
                </label>
                <br clear="all">
                {!! Form::text('reply', '', ['class' => 'form-control', 'placeholder' => translate('form.comment'), 'required' => 'required']) !!}
            </div>
        </div>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                <label>
                    {{translate('form.upload_doc')}}
                </label>
                <br clear="all">
                {!! Form::file('upload_doc[]', ['class' => 'form-control h-a', 'multiple' => 'multiple']) !!}
            </div>
            <div class="col-md-2">
                <label>
                    {{translate('form.closed')}}
                </label>
                <div class="checkbox-custom checkbox-system mt5 mb5 text-center">
                    <input id="checkboxExample2" name="closed" type="checkbox" value="1">
                    <label class="pl5" for="checkboxExample2">&nbsp;</label>
                </div>
            </div>
        </div>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                {!! csrf_field() !!}
                {!! Form::hidden('process_id', $details->process_id) !!}
                {!! Form::hidden('risk_id', $action->id) !!}
                {!! Form::hidden('type', 'risk') !!}
                {!! Form::hidden('action_log_id', $action->id) !!}
                {!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'name' => 'save']) !!}
            </div>
        </div>
    {!! Form::close() !!}
    @endif
    {!! Form::open(array('method' => 'post', 'url' => 'process-step/risk/action/add', 'autocomplete' => 'off', 'class' => 'addaction-class hide')) !!}
        <div class="col-md-12">
            <hr class="mt10 mb10">
        </div>
        <div class="row pl10 pr10">
            <div class="col-md-6">
                <label>
                    {{translate('form.risk_level')}}
                </label>
                {!! Form::select('risk_level', ['' => translate('form.risk_level'), 'L' => translate('form.low'), 'M' => translate('form.medium'), 'H' => translate('form.high')], '', array('id' => 'level','class' => 'form-control', 'required' => 'required')) !!}
            </div>
            <div class="col-md-6">
                <label>
                    {{translate('form.comment')}}
                </label>
                <br clear="all">
                {!! Form::text('comment', '', ['class' => 'form-control', 'id' => 'comment', 'placeholder' => translate('form.comment'), 'tabindex' => '1']) !!}
            </div>
        </div>
        <span id="action-segment">
        <div class="row pl10 mt5">
            <div class="col-md-6">
                <label>
                    {{translate('form.action')}}
                </label>
                <br clear="all">
                {!! Form::text('action', '', ['class' => 'form-control', 'id' => 'action', 'placeholder' => translate('form.define_action'), 'required' => 'required', 'tabindex' => '1']) !!}
            </div>
        </div>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                <label>
                    {{translate('table.delegate_to')}}
                </label>
                <br clear="all">
                {!! Form::select('who', $users, '', ['class' => 'form-control', 'id' =>'who','tabindex' => '5','required' => 'required']) !!}
            </div>
        </div>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                <label>
                    {{translate('form.deadline')}}
                </label>
                {!! Form::text('deadline', '', ['class' => 'form-control DatePicker4', 'id' => 'deadline', 'placeholder' => translate('form.define_deadline'), 'required' => 'required', 'tabindex' => '2']) !!}
            </div>
        </div>
        </span>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                {!! csrf_field() !!}
                {!! Form::hidden('actions', $definedActions, ['id' => 'defined-actions']) !!}
                {!! Form::hidden('process_id', $details->process_id) !!}
                {!! Form::hidden('risk_id', $details->id) !!}
                {!! Form::hidden('type', 'risk') !!}
                {!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'name' => 'save']) !!}
            </div>
        </div>
    {!! Form::close() !!} 
@endif

@if($for == 'Opportunity')
    <div class="col-md-11">
        <b>{{translate("words.process")}}:</b> {{ $details->process->name }}
        <br clear="all">
        <b>{{translate("words.opportunity")}}:</b> {{ $details->details }}
        <br clear="ll">
        <b>{{translate("words.level")}}</b>: {{ $level->level($details->level) }}
        <br clear="all">
        <b>{{translate("words.comment")}}</b>: {{ $details->comment }}
        <br clear="all">
        <b>{{translate("words.status")}}</b>: {{isset($action->closed) ? (($action->closed==1)? translate("words.finished") : translate("words.open")) : translate("words.no_action")}}
        @if(isset($action->closed))
            @if($action->closed==0)
            <br clear="all">
            <b>{{translate("words.open_action")}}</b>: {{$action->action}}
            @endif
        @endif
    </div>
    <div class="col-md-1">
        @if($modify == "yes")
        <a href="{{url('/process-step/opportunity/remove/'.$id)}}" onclick="return confirm('{{translate('alert.are_you_sure_all_related_actions_will_also_be_deleted')}}')" class="btn btn-danger btn-sm mb5">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
        @endif
        @if(!empty($action))
            <a href="javascript:void(0);" data-toggle="modal" data-target="#event-log-details" data-riskid="{{$details->id}}" data-type="opportunity" class="btn btn-success dark text-default mb5" id="event-log" title="{{translate('words.event_logging')}}">
                <i class="fa fa-list"></i>
            </a>
        @endif
    </div>
    @if($modify == "yes" or $commentAllow == "yes")
    <div class="col-md-1">
        @if(!empty($action))
            @if($action->closed == 0)
                <a href="javascript:void(0);" id="post-comment" title="{{translate('form.comment')}}" class="btn btn-success mb5">
                    <i class="fa fa-comment"></i>
                </a>
            @else
                <a href="javascript:void(0);" id="add-action" title="{{translate('form.add_action')}}" class="btn btn-system btn-sm">
                    <i class="fa fa-plus-circle"></i>
                </a>
            @endif
        @else
            <a href="javascript:void(0);" id="add-action" title="{{translate('form.add_action')}}" class="btn btn-system btn-sm">
                <i class="fa fa-plus-circle"></i>
            </a>
        @endif
    </div>
    @endif
    @if(!empty($action))
        @if(!empty($action->comments('opportunity')))
            <div class="col-md-11 risk-segment">
                <hr class="mt10 mb10">
                <div class="panel-heading">
                {{translate('form.comment')}} ({{translate('form.comment_on')}})
                </div>
                <ul class="h-a segmentul list-unstyled pt5">
                    @foreach($action->comments('opportunity') as $eachAction)
                    <li>
                        {{$eachAction->reply}} ({{$eachAction->created_at}})
                    </li>
                    @endforeach
                </ul>
            </div>
        @endif
    @endif
    <div class="row">
        
    </div>
    @if(!empty($action))
    {!! Form::open(array('method' => 'post', 'url' => 'process-step/opportunity/action/comment', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data', 'class' => 'comment-class hide')) !!}
        <div class="col-md-12">
            <hr class="mt10 mb10">
        </div>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                <label>
                    {{translate('form.comment')}}
                </label>
                <br clear="all">
                {!! Form::text('reply', '', ['class' => 'form-control', 'placeholder' => translate('form.comment'), 'required' => 'required']) !!}
            </div>
        </div>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                <label>
                    {{translate('form.upload_doc')}}
                </label>
                <br clear="all">
                {!! Form::file('upload_doc[]', ['class' => 'form-control h-a', 'multiple' => 'multiple']) !!}
            </div>
            <div class="col-md-2">
                <label>
                    {{translate('form.closed')}}
                </label>
                <div class="checkbox-custom checkbox-system mt5 mb5 text-center">
                    <input id="checkboxExample2" name="closed" type="checkbox" value="1">
                    <label class="pl5" for="checkboxExample2">&nbsp;</label>
                </div>
            </div>
        </div>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                {!! csrf_field() !!}
                {!! Form::hidden('process_id', $details->process_id) !!}
                {!! Form::hidden('risk_id', $action->id) !!}
                {!! Form::hidden('type', 'opportunity') !!}
                {!! Form::hidden('action_log_id', $action->id) !!}
                {!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'name' => 'save']) !!}
            </div>
        </div>
    {!! Form::close() !!}
    @endif
    {!! Form::open(array('method' => 'post', 'url' => 'process-step/opportunity/action/add', 'autocomplete' => 'off', 'class' => 'addaction-class hide')) !!}
        <div class="col-md-12">
            <hr class="mt10 mb10">
        </div>
        <div class="row pl10 pr10">
            <div class="col-md-6">
                <label>
                    {{translate('form.opportunity_level')}}
                </label>
                {!! Form::select('level', ['' => translate('form.opportunity_level'), 'L' => translate('form.low'), 'M' => translate('form.medium'), 'H' => translate('form.high')], '', array('id' => 'level', 'class' => 'form-control', 'required' => 'required')) !!}
            </div>
            <div class="col-md-6">
                <label>
                    {{translate('form.comment')}}
                </label>
                <br clear="all">
                {!! Form::text('comment', '', ['class' => 'form-control', 'id' => 'comment', 'placeholder' => translate('form.comment'), 'tabindex' => '1']) !!}
            </div>
        </div>
        <span id="action-segment">
        <div class="row pl10 mt5">
            <div class="col-md-6">
                <label>
                    {{translate('form.action')}}
                </label>
                <br clear="all">
                {!! Form::text('action', '', ['class' => 'form-control', 'id' => 'action', 'placeholder' => translate('form.define_action'), 'required' => 'required', 'tabindex' => '1']) !!}
            </div>
        </div>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                <label>
                    {{translate('table.delegate_to')}}
                </label>
                <br clear="all">
                {!! Form::select('who', $users, '', ['class' => 'form-control','id' =>'who', 'tabindex' => '5','required' => 'required']) !!}
            </div>
        </div>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                <label>
                    {{translate('form.deadline')}}
                </label>
                {!! Form::text('deadline', '', ['class' => 'form-control DatePicker4', 'id' => 'deadline', 'placeholder' => translate('form.define_deadline'), 'required' => 'required', 'tabindex' => '2']) !!}
            </div>
        </div>
        </span>
        <div class="row pl10 mt5">
            <div class="col-md-6">
                {!! csrf_field() !!}
                {!! Form::hidden('actions', $definedActions, ['id' => 'defined-actions']) !!}
                {!! Form::hidden('process_id', $details->process_id) !!}
                {!! Form::hidden('risk_id', $details->id) !!}
                {!! Form::hidden('type', 'opportunity') !!}
                {!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'name' => 'save']) !!}
            </div>
        </div>
    {!! Form::close() !!}
@endif

@if($for == 'Resources')
    <div class="col-md-5">
        <b>{{translate("words.resources")}}:</b> {{ $details->resource }}
    </div>
    <div class="col-md-6 risk-segment">
        @if($details->activities->count() > 0)
            <div class="panel-heading">
            {{translate('form.activity')}}
            </div>
            <ul class="h-a segmentul">
                @foreach($details->activities as $eachActivity)
                <li>{{$eachActivity->activity->activity}}</li>
                @endforeach
            </ul>
        @endif
    </div>
    <div class="col-md-1">
        @if($modify == "yes")
        <a href="{{url('/process-step/resources/edit/'.$id)}}" class="btn btn-system btn-sm mb5" >
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        <a href="{{url('/process-step/resources/remove/'.$id)}}" onclick="return confirm('{{translate('alert.are_you_sure')}}')" class="btn btn-danger btn-sm">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
        @endif
    </div>
@endif

@if($for == 'Roles')
    <div class="col-md-6">
        <b>{{translate("words.process")}}:</b> {{ $process->name }}
        <br clear="all">
        <b>{{translate("words.process_step")}}:</b> {{ $process_steps->step_name }}
        <br clear="all">
        <b>{{translate("words.position")}}:</b> {{ $position->name }}
        <br clear="all">
    </div>
    <div class="col-md-6">
        <b>{{translate('words.role_responsibility')}}:</b> 
        <br clear="all">
        @if(in_array('1',$roles))
        <h5>
            <i class="fa fa-check fa-1x text-system"></i> 
            {{ translate('table.responsible') }}
        </h5>
        @endif

        @if(in_array('3',$roles))
        <h5>
            <i class="fa fa-check fa-1x text-system"></i> 
            {{ translate('table.consulted') }}
        </h5>
        @endif

        @if(in_array('4',$roles))
        <h5>
            <i class="fa fa-check fa-1x text-system"></i> 
            {{ translate('table.informed') }}
        </h5>
        @endif
    </div>
@endif

@if($for == 'multiRoles')
    @for($i = 0; $i< count($process); $i++)
    <div class="col-md-6">
        @if($i == 0)
        <b>{{translate("words.process")}}:</b> {{ $process[$i]->name }}
        <br clear="all">
        <b>{{translate("words.position")}}:</b> {{ $position[$i]->name }}
        <br clear="all">
        @endif
        <b>{{translate("words.process_step")}}:</b> {{ $process_steps[$i]->step_name }}
        <br clear="all">
    </div>
    <div class="col-md-6">
        <b>{{translate('words.role_responsibility')}}:</b> 
        <br clear="all">
        @if(in_array('1',$roles[$i]))
        <h5>
            <i class="fa fa-check fa-1x text-system"></i> 
            {{ translate('table.responsible') }}
        </h5>
        @endif

        @if(in_array('3',$roles[$i]))
        <h5>
            <i class="fa fa-check fa-1x text-system"></i> 
            {{ translate('table.consulted') }}
        </h5>
        @endif

        @if(in_array('4',$roles[$i]))
        <h5>
            <i class="fa fa-check fa-1x text-system"></i> 
            {{ translate('table.informed') }}
        </h5>
        @endif
    </div>
    @endfor
@endif
<script type="text/javascript">
    $("#level").on('change', function(){
        var actions = $('#defined-actions').val();
        if(actions != "") {
            var actionFound = actions.search($(this).val());
            if(actionFound >= 0) {
                $("#action-segment").removeClass("hide");
                $("#action").prop("required", true);
                $("#who").prop("required", true);
                $("#deadline").prop("required", true);
            } else {
                $("#action-segment").addClass("hide");
                $("#action").val("");
                $("#deadline").val("");
                $("#action").prop("required", false);
                $("#who").prop("required", false);
                $("#deadline").prop("required", false);
            }
        }
    });
</script>
@if(isset($labelArr) and isset($dataArr))
<script type="text/javascript">
$('#turtle-details').on('shown.bs.modal', function (event) {
    if(document.getElementById('process-kpi-graph') != null) {
        var ctx = document.getElementById('process-kpi-graph').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: {!! $labelArr !!},
                datasets: [{
                    label: '{{$details->name." ".translate("words.graph")}}',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: {!! $dataArr !!},
                }]
            },

            // Configuration options go here
            options: {}
        });
    }
})
</script>
@endif