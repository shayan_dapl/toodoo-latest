@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            @if($processOwners == 0)
            <div class="label bg-danger pull-left mr30 text-default dark full-width mb10">
                    {{translate('alert.please_add_process_owner_before_creating_process')}}
            </div>
            @endif
            {!! Form::open(array('method' => 'post', 'url' => '/process/form', 'class' => 'form-horizontal', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/process/list'), translate('form.back'), ['class' => 'btn btn-hover btn-system pull-right']) !!}
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('company_id', translate('form.company_name'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3>{{$company->name}}</h3>
                        <input type="hidden" name="company_id" id="company_id" value="{{$company->id}}"><br/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', translate('form.process_name'), ['class' => 'col-lg-3 col-md-offset-2 control-label required']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {!! Form::text('name', isset($details->name) ? $details->name : old('name'), ['class' => 'gui-input', 'id' => 'name', 'placeholder' => translate('form.process_name'), 'required' => 'required', 'tabindex' => '1']) !!}
                                <label for="name" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            @if (count($errors) > 0)
                                <p class="text-danger mn">{{$errors->first('name')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    {!! Form::label('branch', translate('words.branch'), ['class' => 'col-lg-3 col-md-offset-2 control-label required']) !!}
                    <div class="col-md-4">
                        <div class="section branch">
                        <label class="field prepend-icon">
                            {!! Form::select('branches[]',$branches, !empty($selectedBranches) ? $selectedBranches : '' , array('class'=>'select2-single select-branch gui-input','id' => 'branch','required' => 'required','multiple' => 'multiple')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('branches[]')}}</p>
                        @endif
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="form-group">
                    {!! Form::label('parent_id', translate('form.subprocess'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-1">
                        <label class="switch-extended switch-success">
                            {!! Form::checkbox('is_sub_process', 1, (isset($details->parent_id) and $details->parent_id!=0), ['id' => 'make-sub-process'] ) !!}
                            <div class="slider-extended round"></div>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row {{$subprocess}}" id="parent-process">
                <div class="form-group">
                    {!! Form::label('name', translate('form.process_name'), ['class' => 'col-lg-3 col-md-offset-2 control-label required']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field select">
                                {!! Form::select('parentProcess', $parentProcess, ((isset($details->parent_id) and $details->parent_id!=0) ? $details->parent_id : ''), ['id' => 'parentProcess', 'tabindex' => '2']) !!}
                                <i class="arrow"></i>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row {{$nocategory}}" id="process-category">
                <div class="form-group">
                    {!! Form::label('category_id', translate('form.category'), ['class' => 'col-lg-3 col-md-offset-2 control-label required']) !!}
                    <div class="col-md-9">
                        @foreach($category as $id => $name)
                        <p class="custom_radio">
                            <input type="radio" name="category_id" id="test{{$loop->iteration}}" value="{{$id}}" @if((Session::has('category') and Session::get('category') == $id) or (!empty($details->category_id) and $details->category_id == $id) or ($loop->iteration == 1))) checked @endif>
                            <label for="test{{$loop->iteration}}">{{$name}}</label>
                        </p>
                        @endforeach
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('category_id', 'Please Mention A Category')}}</p>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('owner_id', translate('form.process_owner'), ['class' => 'col-lg-3 col-md-offset-2 control-label required']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field select">
                                {!! Form::select('owner_id', $owners, (Session::has('ownername') ? Session::get('ownername') : (!empty($details->owner_id) ? $details->owner_id : old('owner_id'))), ['id' => 'owner_id', 'required' => 'required', 'tabindex' => '4']) !!}
                                <i class="arrow"></i>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('owner_id','Please Select An Owner')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('owner_id', translate('form.no_turtle_process'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <label class="switch-extended switch-success">
                            {!! Form::checkbox('no_turtle_process', 1, (isset($details->no_turtle_process) and $details->no_turtle_process==1)) !!}
                            <div class="slider-extended round"></div>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', isset($details->id) ? $details->id : '') !!}
                    {!! Html::link(url('/process/list'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system', 'tabindex' => '6')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'save-btn', 'tabindex' => '5')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('includes.scripts.process_form_script')
@stop