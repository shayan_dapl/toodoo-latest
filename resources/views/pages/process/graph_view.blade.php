<div class="col-md-12">
    <canvas id="process-kpi-graph"></canvas>
</div>

@if(isset($labelArr) and isset($dataArr))
<script type="text/javascript">
$('#turtle-details').on('shown.bs.modal', function (event) {
    if(document.getElementById('process-kpi-graph') != null) {
        var ctx = document.getElementById('process-kpi-graph').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',
            // The data for our dataset
            data: {
                labels: {!! $labelArr !!},
                datasets: [{
                    label: '{{$kpiTitle." ".translate("words.graph")}}',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: {!! $dataArr !!},
                }]
            },
            // Configuration options go here
            options: {}
        });
    }
})
</script>
@endif