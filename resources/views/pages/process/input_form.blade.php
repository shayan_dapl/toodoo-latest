@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => 'process-step/input/form', 'class' => 'form-horizontal', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', translate('form.process_name'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3>{{\App\Models\Process::find(session('turtle_process_id'))->name}}</h3>
                        {!! Form::hidden('process_id', session('turtle_process_id')) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', translate('form.name'), ['class' => 'col-lg-3 col-md-offset-2 control-label required']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {{ Form::textarea('name', (isset($details->name) ? $details->name : old('name')), ['class' => 'gui-input','id'=>'name','required' => 'required', 'placeholder' => translate('form.name'), 'tabindex' => '1']) }}
                                <label for="name" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('name')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('supp_process', translate('form.related_process'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field select">
                                {!! Form::select('supporting_process_id', $process, (isset($details->supporting_process_id) ? $details->supporting_process_id : old('supporting_process_id')), ['id' => 'supp_process', 'class' => 'gui-input', 'tabindex' => '2']) !!}
                                <i class="arrow"></i>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('scope')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('comment', translate('form.comment'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {{ Form::textarea('comment', (isset($details->comment) ? $details->comment : old('comment')), ['class' => 'gui-input','id'=>'comment', 'placeholder' => translate('form.comment'), 'tabindex' => '3']) }}
                                <label for="comment" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('comment')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', isset($details->id) ? $details->id : '') !!}
                    {!! Html::link(url('/process/turtle/'.session('turtle_process_id')), translate('form.cancel'), array('class' => 'btn btn-hover btn-system', 'tabindex' => '5')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn', 'tabindex' => '4')) !!}    
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop