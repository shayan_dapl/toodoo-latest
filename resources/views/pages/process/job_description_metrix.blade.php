@extends('layouts.default')
@section('content')
<!--###########################Jobdescription matrix###############################-!-->
 <div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible">
            <div class="panel-heading text-center">        
                {{translate('words.job_description_metrix')}} 
            </div>
            <div class="panel-body">
             <div class="form-group posfilter disp-flex pull-right">
                <span>{{ucfirst(translate('words.filter'))}} :</span>
                <select class="form-control w200 filter" id="position-filter">
                    <option value="">{{translate('words.select_functions')}}</option>
                    @foreach($allPositions as $eachPosition)
                        <option value="{{$eachPosition['position_id']}}" {{($positionId == $eachPosition['position_id']) ? 'selected' : ''}}>
                            {{$eachPosition['positionname']}}
                        </option>
                    @endforeach
                </select>
                {!! Form::select('branch', $branches, isset($selectedBranch) ? $selectedBranch : '', ['class' => 'form-control w200 ml5', 'id' => 'branch-filter']) !!}
                <a class="btn bg-system ml10 text-default" href ="{{url('/process/competent-metrix').$competentParams}}">
                    {{ucfirst(translate('words.competent_metrix'))}}
                </a>
             </div>
            <div class="row">
                    <table class="table table-bordered jobDescTable table-striped">
                        @if(!empty($positions))
                        <thead class="bg-system">
                            <tr>
                                <th width="180px" class="text-left">{{translate('form.process_name')}}</th>
                                <th width="180px" class="text-left">{{translate('words.process_steps')}}</th>
                                @if(!empty($positions))
                                @foreach($positions as $posId => $posName)
                                <th class="text-center">{{$posName}}</th>
                                @endforeach
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                                @foreach($processData as $keyProces=>$processdata)
                                    @foreach($processData[$keyProces] as $eachstep => $positionId)
                                            <tr>
                                                @if($loop->iteration == 1)
                                                <td rowspan="{{count($processData[$keyProces])}}" class="">
                                                    {{$keyProces}}
                                                </td>
                                                @endif
                                                <td>{{$eachstep}}</td>
                                                @foreach($positions as $posId => $posName)
                                                    @if(in_array($posId, $processData[$keyProces][$eachstep]))
                                                    <td class ="text-center fw700 fs18">X</td>
                                                    @else
                                                    <td></td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                     @endforeach
                                @endforeach 
                        </tbody>
                        @else
                        <tr>
                        <td class="text-center">{{translate('table.no_data')}}</td>
                        </tr>
                        @endif 
                    </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div> 

<script type="text/javascript">
    jQuery(document).ready(function() {
        $("#position-filter").change(function () {
            var branchId = $('#branch-filter').val();
            var positionId = $(this).val();
            if(positionId != ''){
                var positionId = '/'+ positionId;
            } else {
                var positionId = '/all';
            }
            if(branchId != ''){
                var branchId = '/'+ branchId;
            } else {
                var branchId = '';
            }
            var url = "{{url('/process/job-description-metrix/')}}"+positionId + branchId;
            $(location).attr('href',url);
        })

        $('#branch-filter').change(function(){
            var branchId = $(this).val();
            var positionId = $('#position-filter').val();
            if (positionId != '') {
                var positionId = '/'+ positionId;
            } else {
                var positionId = '/all';
            }
            if (branchId != '') {
                var branchId = '/'+ branchId;
            } else {
                var branchId = '';
            }
            var url = "{{url('/process/job-description-metrix/')}}"+positionId + branchId;
            $(location).attr('href',url);
        })
    })
</script>
@stop