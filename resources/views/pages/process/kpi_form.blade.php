@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(['method' => 'post', 'url' => '/process-step/kpi/form', 'class' => 'form-horizontal', 'id'=>'kpi_entry_form', 'autocomplete' => 'off']) !!}
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('name', translate('form.process_name'), ['class' => 'field-label fs15 mb5']) !!}
                        <label class="field append-icon">
                            <h3>{{\App\Models\Process::find(session('turtle_process_id'))->name}}</h3>
                            {!! Form::hidden('process_id', session('turtle_process_id')) !!}
                        </label>
                    </div>
                </div> 
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('name', translate('form.kpi_name'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field append-icon">
                            {!! Form::text('kpi_name',isset($details) ? $details->name : '', ['class' => 'gui-input', 'id' => 'kpi_name', 'placeholder' => translate('form.kpi_name'), 'required' => 'required', 'tabindex' => '1']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('kpi_name')}}</p>
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('input', translate('form.description'), ['class' => 'field-label fs15 mb5']) !!}
                        <label class="field append-icon">
                           {!! Form::text('input',isset($details) ? $details->input : '', ['class' => 'gui-input', 'id' => 'input', 'placeholder' => translate('form.description'), 'tabindex' => '2']) !!}
                        </label>
                    </div>
                </div> 
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('master_kpi', translate('form.master_kpi'), ['class' => 'field-label fs15 mb5']) !!}
                        <label class="field select">
                            {!! Form::select('master_kpi[]',$masterKpi, isset($details) ? $details->masterkpi()->pluck('master_kpi_id')->toArray() : old('master_kpi'), ['class'=>'select2-single', 'multiple' => 'multiple', 'id' => 'master_kpi', 'tabindex' => '3']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('master_kpi')}}</p>
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('valid_from', translate('form.valid_from'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field prepend-icon">
                            <input type="text" id="valid-from" name="valid_from" class="form-control pointer" value="{{$details->valid_from or date('01-01-Y')}}" required="" tabindex="4" readonly="" />
                            <label class="field-icon">
                                <i class="fa fa-calendar-o"></i>
                            </label>
                        </label>
                    </div> 
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('valid_to', translate('form.valid_to'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field prepend-icon">
                           <input type="text" name="valid_to" id="valid-to" class="form-control pointer" value="{{$details->valid_to or date('31-12-Y')}}" required="" tabindex="5" readonly="" />
                            <label class="field-icon">
                                <i class="fa fa-calendar-o"></i>
                            </label>
                        </label>
                    </div> 
                </div>
            </div>
             <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('frequency', translate('form.frequency'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field select">
                            {!! Form::select('frequency', $frequency, isset($details) ? $details->frequency : old('frequency'), ['class'=>'select2-single select-frequency', 'id' => 'frequency', 'required' => 'true', 'tabindex' => '6']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('frequency')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                        {!! Form::label('kpi_unit', translate('form.kpi_unit'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field select">
                            {!! Form::select('kpi_unit_id', $kpiUnits, isset($details) ? $details->kpi_unit_id : old('kpi_unit_id'), ['class'=>'select2-single select-kpi-unit', 'id' => 'kpi_unit', 'required' => 'true', 'tabindex' => '7']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('kpi_unit_id')}}</p>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section hide" id="txtOpt">
                        {!! Form::label('target', translate('form.target'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field append-icon">
                            {!! Form::text('targettxt',(isset($details) and ($details->kpi_unit_id == 1 or $details->kpi_unit_id == 2 )) ? $details->target : '', ['class' => 'gui-input', 'id' => 'txtOpt-field', 'placeholder' => translate('form.target'), 'required' => 'required', 'oninput' => "validateNumber(this);", 'tabindex' => '8']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('target')}}</p>
                    </div>
                    <div class="section hide" id="boolOpt">
                        {!! Form::label('target', translate('form.target'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field select">
                            {!! Form::select('targetboolopt', $boolOptions, (isset($details) and $details->kpi_unit_id == 3) ? $details->target : old('target'), ['class' => 'select2-single select-target', 'id' => 'boolOpt-field', 'required' => 'required', 'tabindex' => '9']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('target')}}</p>
                    </div>
                    <div class="section hide" id="trendOpt">
                        {!! Form::label('target', translate('form.kpi_unit'), ['class' => 'field-label fs15 mb5 required']) !!}
                        <label class="field select">
                            {!! Form::select('targettrndopt', $trendOptions, (isset($details) and $details->kpi_unit_id == 4) ? $details->target : old('target'), ['class' => 'select2-single select-target', 'id' => 'trendOpt-field', 'required' => 'true', 'tabindex' => '10']) !!}
                        </label>
                        <p class="text-danger mn">{{$errors->first('target')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', isset($details) ? $details->id : '') !!}
                    {!! Html::link(url('/process/turtle/' . session('turtle_process_id')), translate('form.cancel'), ['class' => 'btn btn-hover btn-system', 'tabindex' => '12']) !!}
                    {!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'id' => 'profile-btn', 'tabindex' => '11']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('includes.scripts.kpi_form_script')
@stop