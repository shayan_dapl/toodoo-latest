@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    <a class="btn btn-hover btn-system" href="{{url('process/form')}}">{{ucfirst(translate('words.add'))}}</a>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.process_name')}}</th>
                            <th>{{strtoupper(translate('words.branch'))}}</th>
                            <th>{{translate('table.category')}}</th>
                            <th>{{translate('table.process_owner')}}</th>
                            <th>{{translate('table.status')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('status', 'App\Services\ProcessList')
                        @inject('name', 'App\Services\ProcessList')
                        @foreach($details as $eachDetail)
                        <tr>
                            <td>
                                {{$eachDetail->name}} <h6 class="text-danger">{{$eachDetail->parent}}</h6>
                            </td>
                            <td>
                                @if($eachDetail->branches->count() > 0)
                                    @foreach($eachDetail->branches as $eachBranch)
                                        <div class="mb5">{{$eachBranch->branch->name}}</div> 
                                    @endforeach
                                @endif
                            </td>
                            <td>{{$name->name(session('lang'), $eachDetail)}}</td>
                            <td>{{$eachDetail->owner->name}}</td>
                            <td>{{$status->status($eachDetail->status)}}</td>
                            <td>
                                <a href="{{ url('/process/edit/'.$eachDetail->id) }}" class="btn btn-default">
                                    <span class="text-success fa fa-pencil"></span> 
                                </a>
                                @if($eachDetail->status == 0)
                                    <a href="javascript:void(0);" data-model="Process" data-id="{{$eachDetail->id}}" data-status="1" class="statusChange btn btn-default">
                                        <span class="text-success fa fa-check"></span> 
                                    </a>
                                @endif
                                @if($eachDetail->status == 1)
                                    <a href="javascript:void(0);" data-model="Process" data-id="{{$eachDetail->id}}" data-status="0" class="statusChange btn btn-default">
                                        <span class="text-danger fa fa-times"></span> 
                                    </a>
                                @endif
                                @if($eachDetail->no_turtle_process == 0)
                                    <a href="{{url('/process/turtle')}}/{{$eachDetail->id}}" class="btn btn-sm text-system custom-tooltip" data-tooltip="{{translate('words.turtle_setup')}}">
                                        <i class="fa fa-chain"></i>
                                    </a>
                                @endif
                                <a href="{{url('/process/remove')}}/{{$eachDetail->id}}" class="btn btn-sm text-system custom-tooltip" data-tooltip="{{translate('words.remove')}}" onclick="return confirm('{{translate('alert.are_you_sure')}}')">
                                        <i class="text-danger fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop