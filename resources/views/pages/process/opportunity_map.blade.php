@extends('layouts.default')
@section('content')
<div class="row">
    @if(!empty($levels))
        @inject('color', 'App\Services\ProcessOpportunityMap')
        @inject('stat', 'App\Services\ProcessOpportunityMap')
        @foreach($levels as $level => $data)
            <div class="col-md-4">
                <div class="panel">
                    <div class="panel-heading bg-system" style="background-color:{{$color->color($level)}} !important;">
                        {{$stat->oppstat($level)}}
                    </div>
                    <div class="panel-body panel-body-ext turtleBox" style="padding:5px;">
                        <ul style="height: auto">
                            @if(!empty($data))
                            @foreach($data as $eachData)
                            <li class="process-cls text-center riskOpportunityCls" data-riskoptid="{{$eachData->id}}" data-type="opportunity" data-processid="{{$eachData->process_id}}">
                                <span class="pull-left">{{$eachData->details}}</span>
                                @if(!empty($eachData->opportunityaction))
                                  <a href="javascript:void(0)" class="fs10 pt3 nocursorCls riskoptmap" data-tooltip="{{$eachData->process->owner->name}}">{{($eachData->opportunityaction->closed == 0) ? translate('words.ongoing') : translate('words.closed')}}</a>
                                @else
                                  <span class="text-center fs10 pt3">{{translate('words.no_action')}}</span>
                                @endif
                                <span style="float: right; font-size: 10px; padding-top: 3px">{{$eachData->process->name}}</span>
                            </li>
                            @endforeach
                            @else
                            <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="text-center">
            <h1>{{translate('table.no_assigned_process')}}</h1>
        </div>
    @endif
</div>
@stop