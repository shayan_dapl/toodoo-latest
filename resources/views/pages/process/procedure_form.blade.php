@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/process-step/procedure/form', 'class' => 'form-horizontal','id'=>'procedureForm', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', translate('form.process_name'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3 class="mt10">{{\App\Models\Process::find(session('turtle_process_id'))->name}}</h3>
                        {!! Form::hidden('process_id', session('turtle_process_id')) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    @if(!empty($version) || !empty($details->id))
                    {!! Form::label('doc_ref', translate('form.doc_ref'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label']) !!}
                    @else
                    {!! Form::label('doc_ref', translate('form.doc_ref'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label required']) !!}
                    @endif
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                @if(!empty($version) || !empty($details->id))
                                <h3 class="mt10">{{$details->doc_ref}}</h3>
                                {!! Form::hidden('doc_ref', (isset($details->doc_ref) ? $details->doc_ref : old('doc_ref'))) !!}
                                @else
                                {!! Form::text('doc_ref', (isset($details->doc_ref) ? $details->doc_ref : old('doc_ref')), ['class' => 'gui-input', 'id' => 'doc_ref', 'placeholder' => translate('form.doc_ref'), 'required' => 'required', 'tabindex' => '1']) !!}
                                <label for="doc_ref" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                                @endif
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('doc_ref')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="row">
                <div class="form-group">
                    {!! Form::label('doctype', translate('form.doctype'), array('class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label required')) !!}
                    <div class="col-md-4">
                        @if(!empty($version))
                        @if($details->doc_type_id)
                        <h3 class="mt10">{{$doctypes[$details->doc_type_id]}}</h3>
                        @endif
                        {!! Form::select('doc_type_id', $doctypes, isset($details->doc_type_id) ? $details->doc_type_id : old('doc_type_id'), array('id' => 'doctype', 'class' => 'hide required')) !!}
                        @else
                        <div class="section">	
                            <label class="field select">
                                {!! Form::select('doc_type_id', $doctypes, isset($details->doc_type_id) ? $details->doc_type_id : old('doc_type_id'), array('id' => 'doctype', 'required' => 'required','tabindex' => '3')) !!}
                                <i class="arrow"></i>
                            </label>

                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('doc_type_id')}}</p>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    @if(!empty($version))
                    {!! Form::label('language', translate('form.language'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label']) !!}
                    @else
                    {!! Form::label('language', translate('form.language'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label required']) !!}
                    @endif
                    <div class="col-md-4">
                        @if(!empty($version))
                        <h3 class="mt10">{{$language[$details->language]}}</h3>
                        {!! Form::select('language', $language, isset($details->language) ? $details->language : old('language'), ['id' => 'language', 'required' => 'required', 'class' => 'hide']) !!}
                        @else
                        <div class="section">
                            <label class="field select">
                                {!! Form::select('language', $language, isset($details->language) ? $details->language : old('language'), ['id' => 'language', 'required' => 'required', 'tabindex' => '3']) !!}
                                <i class="arrow"></i>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('language')}}</p>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @if(!isset($details->id))
            <div class="row">
                <div class="form-group" id="procedure-form">
                    {!! Form::label('interexter', translate('form.upload_doc'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-6">
                        <div class="section">
                            <div class="radio-custom radio-system pt10">
                                <input type="radio" id="upload_doc_yes" checked="" name="upload_doc" value="Yes">
                                <label for="upload_doc_yes">{{translate('form.yes')}}</label>

                                <input type="radio" id="upload_doc_no" name="upload_doc" value="No">
                                <label for="upload_doc_no">{{translate('form.no')}}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            
        @if(!empty($details->docs) and empty($version))
            @inject('file', 'App\Services\ProcessProcedureForm')
            <div class="row procedure-form">
                <div class="form-group">
                 @foreach($details->docs as $eachDoc)
                 <div class="col-md-offset-2 col-md-1">
                    @if($eachDoc->doc_name == '')
                    {{translate('form.document_link')}} :
                    @endif
                </div>
                <div class="col-md-4">

                    @if($eachDoc->doc_name != '')
                    <span class="pos-relative">
                        <a href="{{url('storage/company/'.Auth::guard('customer')->user()->company_id.'/documents/process_docs/'.$eachDoc->doc_name)}}" target="_blank">
                            <i class="fa fa-download text-success doc-del-btn ml25-extended"></i> 
                        </a>
                        <i class="fa {{$file->file($eachDoc->doc_name, 'process_docs')['icon']}} fa-3x text-system-faded"></i>
                    </span>
                    {{$eachDoc->doc_name}}
                    <br clear="all">
                    <br clear="all">
                    @else
                    {{$eachDoc->location_doc}}
                    @endif
                </div>
                @endforeach
            </div>
            </div>
        @endif

    @if(!isset($details->id) or !empty($version))
    <div id="doc_upload_dv">
            <div class="row">
                <div class="form-group">
                    @if(!empty($version))
                    {!! Form::label('version', translate('form.new_version'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label fw700 required']) !!}
                    @else
                    {!! Form::label('version', translate('form.version'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label required']) !!}
                    @endif
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {!! Form::text('version', (isset($details->version) and empty($version)) ? $details->version : old('version'), ['class' => 'gui-input', 'id' => 'version', 'placeholder' => translate('form.version'), 'required' => 'required']) !!}
                                <label for="version" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('version')}}</p>
                            @endif
                        </div>
                    </div>
                    @if(!empty($version))
                    <div class="col-md-4">
                       {{translate('form.old_version')}} : {{$details->version}} <br>
                       {{translate('form.upload_date')}} : {{$details->created_at}}
                   </div>
                   @endif
               </div>
           </div>
           @if(!empty($version))
            <div class="row">
                <div class="form-group">
                    {!! Form::label('remark', translate('form.remark'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {!! Form::text('remark', '', ['class' => 'gui-input', 'id' => 'remark', 'placeholder' => translate('form.remark')]) !!}
                                <label for="remark" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('version')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endif
        <div class="row">
            <div class="form-group">
                {!! Form::label('user_photo', translate('form.upload_doc'), array('class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label required')) !!}
                <div class="col-md-4">
                    <div class="section">
                        <label class="field prepend-icon append-button file" id="uploaderr">
                            <span class="button">{{translate('form.file')}}</span>
                            {!! Form::file('upload_doc', array('class' => 'gui-file required', 'id' =>'uploadfile', 'onChange' => 'document.getElementById("uploader1").value = this.files[0].name;')) !!}
                            {!! Form::text('', '', array('class' => 'gui-input', 'placeholder' => translate('form.image'), 'id' => 'uploader1')) !!}
                            <label class="field-icon">
                                <i class="fa fa-upload"></i>
                            </label>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="row" id="location_doc" style="display:none">
        <div class="form-group">
            {!! Form::label('location_doc', translate('form.location_doc'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label required']) !!}
            <div class="col-md-4">
                <div class="section">
                    <label class="field prepend-icon">
                        {!! Form::text('location_doc', '', ['class' => 'gui-input', 'id' => 'doc_location', 'placeholder' => translate('form.location_doc'), 'required' => 'required', 'tabindex' => '2']) !!}
                        <label for="location_doc" class="field-icon">
                            <i class="fa fa-pencil"></i>
                        </label>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            {!! Form::label('inform_user', translate('form.inform_user'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
            <div class="col-md-4">
                <label class="switch-extended switch-success">
                    {!! Form::checkbox('inform_user', 1, (isset($details->inform_user) and $details->inform_user==1)) !!}
                    <div class="slider-extended round"></div>
                </label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-2 text-center">
            {!! csrf_field() !!}
            {!! Form::hidden('hid', isset($details->id) ? $details->id : '') !!}
            {!! Form::hidden('version_number', isset($version) ? $version : '', array('id'=>'version_number')) !!}
            {!! Form::hidden('source_id', isset($source_id) ? $source_id : '') !!}
            {!! Html::link(url('/process/turtle/'.session('turtle_process_id')), translate('form.cancel'), array('class' => 'btn btn-hover btn-system', 'tabindex' => '5')) !!}
            {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'procedure-btn', 'tabindex' => '4')) !!} 
        </div>
    </div>
    {!! Form::close() !!}
</div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#doc_location').prop('required',false);
        $('input:radio[name=upload_doc]').change(function() {
            if (this.value == 'Yes') {
                $('#doc_upload_dv').show();
                $('#location_doc').hide();
                $('#version').prop('required',true);
                $('#doc_location').prop('required',false);
            } else if (this.value == 'No') {
                $('#doc_upload_dv').hide();
                $('#location_doc').show();
                $('#doc_location').prop('required',true);
                $('#version').prop('required',false);
                $('#version').val('');
                $('#doc_location').prop('required',true);
            }
        });

        $('#procedureForm').bind("submit",function() 
        { 
            var imgVal = $('#uploadfile').val();
            var version_number = $('#version_number').val(); 
            if (version_number == '') {
                if (imgVal=='' && $('input:radio[name="upload_doc"]:checked').val() == 'Yes') 
                { 
                    $('#doc_location').prop('required',false);
                    $('#uploaderr').css('box-shadow', 'red 0px 0px 1.5px 1px');
                    return false;   
                }
            }
            else {
                if (imgVal=='') {
                    $('#doc_location').prop('required',false);
                    $('#uploaderr').css('box-shadow', 'red 0px 0px 1.5px 1px');
                    return false;  
                }
            }
        });
        
    });
</script>
@stop