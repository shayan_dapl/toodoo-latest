<div class="row p15">
    <div class="table-responsive">
        <table class="table table-bordered table-stripped">
            <thead>
            <tr>
                <th>{{translate('words.process')}}</th>
                <th>{{translate('words.steps')}}</th>
                <th>{{translate('words.roles_responsibilities')}}</th>
            </tr>
            </thead>
            <tbody>
            @if ($roles->count() > 0)
                @foreach($roles as $role)
                    <tr>
                        <td align="left">{{$role->process->name}}</td>
                        <td align="left">{{$role->steps->step_name}}</td>
                        <td align="left">{{Config::get('settings.raci_arr')[$role->role_id]}}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="3" align="center">{{translate('table.no_data')}}</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>