@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4" style="display: inline-flex; line-height: 2.8">
        Process: 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <select id="process" class="form-control" onchange="showracimap(this.value);">
            <option value="0">
                {{translate('form.select')}}
            </option>
            @foreach($processes as $process)
            <option value="{{$process->id}}" @if( !empty(Request::segment(3)) and Request::segment(3) == $process->id ) selected @endif>
                    {{$process->name}}
            </option>
            @endforeach
        </select>	
    </div>
</div>
<br clear="all">
<div class="row text-center" id="map-div">
    @if(!empty($roles))
        <div class="outer-panel">
            <div class="inner-panel">
                @foreach($steps as $step)
                    @if($loop->iteration == 1)
                    <div class="panel display-block pos-absolute panel-extended">
                        <div class="panel-heading bg-system">
                            {{translate('table.position')}}
                        </div>
                        <div class="panel-body turtleBox1 p5">
                            <ul class="h-a">
                                @foreach($roles as $posname => $roleData)
                                <li class="process-cls text-center raci pointer" data-process="{{$process->id}}" data-name="{{$posname}}" data-target="#raci-details" data-toggle="modal">
                                    {{$posname}}
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                    <div class="panel w-a display-block" style="@if($loop->iteration == 1) margin-left: 23% @endif">
                        <div class="panel-heading bg-system">
                            {{$step->step_name}}
                        </div>
                        <div class="panel-body turtleBox1 p5">
                            <ul class="h-a">
                                @foreach($roles as $posname => $roleData)
                                    <li class="process-cls text-center">
                                        <span class="text-danger">
                                            @foreach($roleData as $val)
                                                @if($val['process_step_id'] == $step->id)
                                                    {{Config::get('settings.raci_arr')[$val['role_id']]}}
                                                @endif
                                            @endforeach
                                        </span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @elseif(empty(Request::segment(3)))
        <h2>{{translate('words.choose_process')}}</h2>
    @else
        <h2>{{translate('table.no_data')}}</h2>
    @endif
</div>
<div id="raci-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header back4d text-default">
                <span id="position-name"></span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body row raci-result text-center justified w-wrap-extended">

            </div>
        </div>
    </div>
</div>
@include('includes.scripts.raci_map_script')
@stop