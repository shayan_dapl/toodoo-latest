@extends('layouts.default')
@section('content')
<div class="row">
    @if(!empty($details))
        @foreach($details as $doctypename=>$data)
            <div class="col-md-4">
                <div class="panel">
                    <div class="panel-heading bg-system">
                        {{$doctypename}}
                    </div>
                    <div class="panel-body panel-body-ext turtleBox1 p5 doc-map-height">
                        <ul class="h-a">
                            @if(!empty($data))
                                @foreach($data as $eachData)
                                    <li class="process-cls">
                                        {{$eachData->doc_ref}}
                                        <br clear="all">
                                        <span class="fs10 pt3">
                                            {{$eachData->process->name}}
                                        </span>
                                        <span class="pull-right"> 
                                        @if(!empty($eachData->regdocuments($eachData->id)) and $eachData->regdocuments($eachData->id)->count() > 0)
                                            @if($eachData->regdocuments($eachData->id)->doc_name != null)
                                                <span class="pos-relative">
                                                    <a href="{{getfileType(extension($eachData->regdocuments($eachData->id)->doc_name), $eachData->regdocuments($eachData->id)->doc_name, 'regulation_docs')['url']}}" target="_blank">
                                                        <i class="fa {{getfileType(extension($eachData->regdocuments($eachData->id)->doc_name), $eachData->regdocuments($eachData->id)->doc_name, 'regulation_docs')['icon']}} text-danger dark"></i>
                                                    </a>
                                                </span>
                                            @endif
                                        @endif
                                        </span>
                                    </li>
                                @endforeach
                            @else
                                <li class="text-center pt15 pb15">{{translate('table.no_data')}}<br clear="all"></h3>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            @if($loop->iteration%3 == 0)
                </div>
                <div class="row">
            @endif
        @endforeach
    @else
        <div class="text-center">
            <h1>{{translate('table.no_assigned_process')}}</h1>
        </div>
    @endif
</div>
@stop