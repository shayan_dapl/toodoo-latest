@extends('layouts.default')
@section('content')
<div class="row" id="procedures-archive">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading pb10">
                <div class="panel-title hidden-xs">
                    {{translate('words.list')}}
                    {!! Html::link(url('/process/turtle/'.session('turtle_process_id')), translate('form.cancel'), array('class' => 'btn btn-hover btn-system pull-right', 'tabindex' => '3')) !!}
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>{{translate('form.version')}}</th>
                                <th>{{translate('table.created_at')}}</th>
                                <th>{{translate('form.remark')}}</th>
                                <th>{{translate('table.attach')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @inject('file', 'App\Services\ProcessProcedureForm')
                            @foreach($data as $eachData)
                            <tr>
                                <td>{{$eachData->regulation->version}}</td>
                                <td>{{date('d/m/Y H:i',strtotime($eachData->created_at))}}</td>
                                <td>{{$eachData->regulation->remark}}</td>
                                <td>   
                                    <span class=" doctypecls">
                                     @if($eachData->doc_name != '')
                                        <i class="fa {{$file->file($eachData->doc_name,'regulation_docs')['icon']}} fa-4x text-system-faded"></i>
                                        <a href="{{$file->file($eachData->doc_name, 'regulation_docs')['url']}}" target="_blank">
                                            <i class="fa fa-eye fa-1x" aria-hidden="true"></i>
                                        </a>
                                        <a  data-doc="{{$eachData->doc_name}}" href="{{url('/doc_download/regulation_docs/'.$eachData->doc_name)}}">
                                            <i class="fa fa-download text-success"></i> 
                                        </a>
                                     @else
                                     {{$eachData->location_doc}}
                                     @endif   
                                    </span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop