@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/process-step/resources/form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' , 'novalidate' => '')) !!}
            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', translate('form.process_name'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3>{{\App\Models\Process::find(session('turtle_process_id'))->name}}</h3>
                        {!! Form::hidden('process_id', session('turtle_process_id')) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    {!! Form::label('resource', translate('form.resource'), ['class' => 'col-lg-3 col-md-offset-2 control-label required']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {{ Form::text('resource', (isset($details->resource) ? $details->resource : old('resource')), ['class' => 'gui-input','id'=>'resource','required' => 'required', 'placeholder' => translate('form.resource'), 'tabindex' => '1']) }}
                                <label for="resource" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('resource')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    {!! Form::label('brand', translate('form.brand'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {{ Form::text('brand', (isset($details->brand) ? $details->brand : old('brand')), ['class' => 'gui-input','id'=>'brand','placeholder' => translate('form.brand'), 'tabindex' => '2']) }}
                                <label for="brand" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('brand')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    {!! Form::label('type', translate('form.type'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {{ Form::text('type', (isset($details->type) ? $details->type : old('type')), ['class' => 'gui-input','id'=>'type','placeholder' => translate('form.type'), 'tabindex' => '3']) }}
                                <label for="type" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('type')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    {!! Form::label('serial_no', translate('form.serial_no'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {{ Form::text('serial_no', (isset($details->serial_no) ? $details->serial_no : old('serial_no')), ['class' => 'gui-input','id'=>'resource','placeholder' => translate('form.serial_no'), 'tabindex' => '4']) }}
                                <label for="serial_no" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('serial_no')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="resorce-activity-{{count($activityDetails)+1}}">
                <label for="inputStandard" class="col-md-3 col-md-offset-2 col-sm-2 control-label">
                    {{translate('form.activity')}}
                </label>

                <div class="col-md-4 col-xs-10">
                    <div class="section">
                        <label class="field select">
                                {!! Form::select('activity['.(count($activityDetails)+1).']', $activity, isset($details->activity_id) ? $details->activity_id : old('activity_id'), array('id' => ('activity-id-'.(count($activityDetails)+1)), 'class' => 'activity', 'tabindex' => '1')) !!}
                            <i class="arrow"></i>
                        </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="button" class="btn btn-system activity-add" data-id="{{count($activityDetails)+1}}"><i class="fa fa-plus"></i></button>
                </div>
                  {!! Form::hidden('activities['.(count($activityDetails)+1).'][]', '') !!}
            </div>
            @if(isset($activityDetails))
                @foreach($activityDetails as $activ)
                <br clear="all">
                <div class="row" id="resorce-activity-{{$loop->iteration}}">
                     <label for="inputStandard" class="col-md-3 col-md-offset-2 col-sm-2 control-label">
                    {{translate('form.activity')}}
                </label>

                <div class="col-md-4 col-xs-10">
                    <div class="section">
                        <label class="field select">
                                {!! Form::select('activity['.$loop->iteration.']', $activity, isset($activ->activity_id) ? $activ->activity_id : old('activity_id'), array('id' => ('activity-id-'.$loop->iteration), 'class' => 'activity', 'tabindex' => '1')) !!}
                            <i class="arrow"></i>
                        </label>
                    </div>
                </div>

                <div class="col-md-3">
                    <button type="button" class="btn btn-danger activity-remove" data-id="{{$loop->iteration}}" data-rowid="{{$activ->id}}" onclick="return confirm('{{translate('alert.are_you_sure')}}');"><i class="fa fa-minus"></i></button>
                </div>
                {!! Form::hidden('activities['.$loop->iteration.'][]', $activ->id) !!}
                </div>
                 @endforeach
            @endif
            
            <div class="row">
                <div class="col-md-4 col-md-offset-2 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', isset($details->id) ? $details->id : '') !!}
                    {!! Html::link(url('/process/turtle/'.session('turtle_process_id')), translate('form.cancel'), array('class' => 'btn btn-hover btn-system', 'tabindex' => '5')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn', 'tabindex' => '4')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function() {
        //Adding dynamic roles segment for process step in process/steps_form.blade.php
        $(document).on('click', '.activity-add', function() {
            var id = $(this).data('id');
            var nextId = id + 1;
            var nextEl = '<br clear="all">\
            <div class="row" id="resorce-activity-' + nextId + '">\
                <label for="inputStandard" class="col-md-3 col-md-offset-2 col-sm-2 control-label">\
                    {{translate("form.activity")}}\
                </label>\
                \
                <div class="col-md-4 col-xs-10">\
                    <div class="section">\
                        <label class="field select">\
                            <select id="activity-id-' + nextId + '" class="activity" name="activity[' + nextId + ']">\
                                @if(isset($activity))\
                                @foreach($activity as $key=>$val)\
                                <option value="{{$key}}">{{$val}}</option>\
                                @endforeach\
                                @endif\
                            </select>\
                            <i class="arrow"></i>\
                        </label>\
                    </div>\
                </div>\
                \
                <div class="col-md-3">\
                    <button type="button" class="btn btn-system activity-add" data-id="' + nextId + '"><i class="fa fa-plus"></i></button>\
                </div>\
                \
                <input name="activities[' + nextId + '][]" value="" type="hidden">\
            </div><br clear="all">';
            $(this).removeClass("btn-system").removeClass("activity-add").addClass("btn-danger").addClass("activity-remove").html('<i class="fa fa-minus"></i>');
            $("#resorce-activity-" + id).before(nextEl);
        });

         $(document).on('click', '.activity-remove', function() {
            var id = $(this).data('id');
            var row_id = $(this).data('rowid');;
            if (row_id != "")
            {
                $.ajax({
                    url: "{{url('/process-step/resources/remove-activity')}}/" + row_id,
                    async: true,
                    type: "GET",
                    cache: false,
                    success: function(data) {
                        new PNotify({
                            title: "{{translate('alert.activity')}}",
                            text: "{{translate('alert.removed_successfully')}}",
                            addclass: 'stack_top_right',
                            type: 'success',
                            width: '290px',
                            delay: 2000
                        });
                        $("#resorce-activity-" + id).prev('br').remove();
                        $("#resorce-activity-" + id).remove();
                    }
                });
            }
            else
            {
                $("#resorce-activity-" + id).prev('br').remove();
                $("#resorce-activity-" + id).remove();
            }
        });
    });
</script>
@stop