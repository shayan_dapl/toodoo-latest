@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/process-step/risk/form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', translate('form.process_name'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3>{{\App\Models\Process::find(session('turtle_process_id'))->name}}</h3>
                        {!! Form::hidden('process_id', session('turtle_process_id')) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('risk', translate('form.risk'), ['class' => 'col-lg-3 col-md-offset-2 control-label required']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {!! Form::text('risk', (isset($details->risk) ? $details->risk : ''), ['class' => 'gui-input', 'id' => 'risk', 'placeholder' => translate('form.risk'), 'required' => 'required', 'tabindex' => '1']) !!}
                                <label for="risk" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            <p class="text-danger mn">{{$errors->first('risk')}}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('risk_level', translate('form.risk_level'), array('class' => 'col-lg-3 col-md-offset-2 control-label required')) !!}
                    <div class="col-md-4">
                        @if (!isset($details->id))
                        <div class="section">
                            <div class="inputcls">
                                <label class="field select">
                                    {!! Form::select('level', ['' => translate('form.risk_level'), 'L' => translate('form.low'), 'M' => translate('form.medium'), 'H' => translate('form.high')], isset($details->risk_level) ? $details->risk_level : old('risk_level'), array('id' => 'level', 'required' => 'required', 'tabindex' => '2')) !!}
                                    <i class="arrow"></i>
                                </label>
                            </div>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('risk_level')}}</p>
                            @endif
                        </div>
                        @else
                        <div class="section">
                            <label class="field select">
                                {!! Form::select('level', ['' => translate('form.risk_level'), 'L' => translate('form.low'), 'M' => translate('form.medium'), 'H' => translate('form.high')], isset($details->risk_level) ? $details->risk_level : old('risk_level'), array('id' => 'level', 'required' => 'required', 'tabindex' => '2')) !!}
                                <i class="arrow"></i>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('risk_level')}}</p>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('comment', translate('form.comment'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {{ Form::textarea('comment', isset($details->comment) ? $details->comment : old('comment'), ['class' => 'gui-input', 'placeholder' => translate('form.comment'), 'tabindex' => '3']) }}
                                <label for="risk" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            <p class="text-danger mn">{{$errors->first('comment')}}</p>
                        </div>
                    </div>
                </div>
            </div>

            <span class="{{!empty(old('has_action')) ? '' : $riskOptActCls}}" id="action-segment">
                <div class="row">
                    <div class="form-group">
                        {!! Form::label('action', translate('form.action'), array('class' => 'col-lg-3 col-md-offset-2 control-label required')) !!}
                        <div class="col-md-4">
                            <label class="field prepend-icon">
                                @if (!empty(old('has_action')))
                                    {!! Form::text('action', '', ['class' => 'gui-input', 'id' => 'action', 'placeholder' => translate('form.define_action'), 'required' => 'required', 'tabindex' => '4']) !!}
                                @else    
                                    {!! Form::text('action', '', ['class' => 'gui-input', 'id' => 'action', 'placeholder' => translate('form.define_action'), 'tabindex' => '4']) !!}
                                @endif
                                <label for="action" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        {!! Form::label('who', translate('table.delegate_to'), array('class' => 'col-lg-3 col-md-offset-2 control-label required')) !!}
                        <div class="col-md-4">
                            <label class="field prepend-icon">
                                @if (!empty(old('has_action')))
                                    {!! Form::select('who', $users, '', ['class' => 'form-control', 'tabindex' => '5','required' => 'required']) !!}
                                @else
                                    {!! Form::select('who', $users, '', ['class' => 'form-control', 'tabindex' => '5']) !!}
                                @endif
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        {!! Form::label('deadline', translate('form.deadline'), array('class' => 'col-lg-3 col-md-offset-2 control-label required')) !!}
                        <div class="col-md-4">
                            <label class="field prepend-icon">
                                @if (!empty(old('has_action')))
                                    {!! Form::text('deadline', '', ['class' => 'gui-input DatePicker4', 'id' => 'deadline', 'placeholder' => translate('form.define_deadline'), 'tabindex' => '6', 'required' => 'required']) !!}
                                @else
                                    {!! Form::text('deadline', '', ['class' => 'gui-input DatePicker4', 'id' => 'deadline', 'placeholder' => translate('form.define_deadline'), 'tabindex' => '6']) !!}
                                @endif
                                <label for="deadline" class="field-icon">
                                    <i class="fa fa-calendar"></i>
                                </label>
                            </label>
                        </div>
                    </div>
                </div>
            </span>

            <div class="row">
                <div class="col-md-4 col-md-offset-2 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('has_action', old('has_action'), ['id' => 'has-action']) !!}
                    {!! Form::hidden('actions', $definedActions, ['id' => 'defined-actions']) !!}
                    {!! Form::hidden('type', 'risk') !!}
                    {!! Form::hidden('hid', isset($details->id) ? $details->id : '') !!}
                    {!! Html::link(url('/process/turtle/'.session('turtle_process_id')), translate('form.cancel'), array('class' => 'btn btn-hover btn-system', 'tabindex' => '8')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn', 'tabindex' => '7', 'name' => 'save')) !!}
                </div>
            </div>
            {!! Form::close() !!}
            <div class="section-divider" id="spy2">
                <span>{{translate('form.risk_list')}}</span>
            </div>
            <div class="panel-body pn">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.risk')}}</th>
                            <th>{{translate('table.level')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($risk_list as $k=>$v)
                        <tr>
                            <td>
                                {{$v->risk}}
                            </td>
                            <td>
                                {{$v->risk_level}}
                            </td>
                            <td>
                                <div class="navbar-btn btn-group">
                                    <a href="{{ url('/process-step/risk/remove/'.$v->id) }}" onclick="return confirm('{{translate('alert.are_you_sure')}}')" title="{{translate('words.remove')}}"">
                                        <span class="text-danger fa fa-trash"></span> 
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
@include('includes.scripts.risk_form_script')
@stop