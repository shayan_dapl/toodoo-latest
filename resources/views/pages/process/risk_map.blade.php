@extends('layouts.default')
@section('content')
@inject('color', 'App\Services\ProcessOpportunityMap')
@inject('stat', 'App\Services\ProcessOpportunityMap')
<div class="row">
    @if(!empty($levels))
        @foreach($levels as $level=>$data)
            <div class="col-md-4">
                <div class="panel">
                    <div class="panel-heading bg-system" style="background-color:{{$color->color($level)}} !important;">
                        {{$stat->riskstat($level)}}
                    </div>
                    <div class="panel-body panel-body-ext turtleBox p5">
                        <ul class="h-a">
                            @if(!empty($data))
                            @foreach($data as $eachData)
                            <li class="process-cls text-center riskOpportunityCls" data-riskoptid="{{$eachData->id}}" data-type="risk" data-processid="{{$eachData->process_id}}">
                                <span class="pull-left">{{$eachData->risk}}</span>
                                @if(!empty($eachData->riskaction))
                                  <a href="javascript:void(0)" class="fs10 pt3 nocursorCls riskoptmap" data-tooltip="{{$eachData->process->owner->name}}">{{($eachData->riskaction->closed == 0) ? translate('words.ongoing') : translate('words.closed')}}</a>
                                @else
                                  <span class="text-center fs10 pt3">{{translate('words.no_action')}}</span>
                                @endif
                                <span class="pull-right fs10 pt3">{{$eachData->process->name}}</span>
                            </li>
                            @endforeach
                            @else
                            <h4 class="text-center">{{translate('table.no_data')}}</h4>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="text-center">
            <h1>{{translate('table.no_assigned_process')}}</h1>
        </div>
    @endif
</div>
@stop