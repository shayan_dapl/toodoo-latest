@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post','url' => '/process-step/roles/form', 'class' => 'form-horizontal', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', translate('form.process_name'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3>{{\App\Models\Process::find(session('turtle_process_id'))->name}}</h3>
                        {!! Form::hidden('process_id', session('turtle_process_id')) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('process_step_id', translate('form.step_name'), ['class' => 'col-lg-3 col-md-offset-2 control-label required']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field select">
                                {!! Form::select('process_step_id', $process_steps, isset($details->process_step_id) ? $details->process_step_id : old('process_step_id'), array('id' => 'process_step_id', 'required' => 'required', 'tabindex' => '1')) !!}
                                <i class="arrow"></i>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('process_step_id')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label for="inputStandard" class="col-md-3 col-md-offset-2 control-label">
                        {{translate('form.position')}}
                        <i class="text-danger">*</i>
                    </label>
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field select">
                                {!! Form::select('position_id', $position, isset($details->position_id) ? $details->position_id : old('position_id'), array('id' => 'position_id', 'required' => 'required', 'tabindex' => '1')) !!}
                                <i class="arrow"></i>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('position_id')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-3">
                    <div class="checkbox-custom checkbox-system mb5">
                        {!! Form::checkbox('roles[]', 1, (isset($roles) and in_array("1",$roles)), ['class' => 'roles', 'id' => 'checkboxExample1']) !!}
                        <label for="checkboxExample1">{{translate('form.roles_responsible')}}</label>
                    </div>
                    <br clear="all">
                    <div class="checkbox-custom checkbox-system mb5">
                        {!! Form::checkbox('roles[]', 2, (isset($roles) and in_array("2",$roles)), ['class' => 'roles', 'id' => 'checkboxExample2']) !!}
                        <label for="checkboxExample2">{{translate('form.roles_accountable')}}</label>
                    </div>
                    <br clear="all">
                    <div class="checkbox-custom checkbox-system mb5">
                        {!! Form::checkbox('roles[]', 3, (isset($roles) and in_array("3",$roles)), ['class' => 'roles', 'id' => 'checkboxExample3']) !!}
                        <label for="checkboxExample3">{{translate('form.roles_consulted')}}</label>
                    </div>
                    <br clear="all">
                    <div class="checkbox-custom checkbox-system mb5">
                        {!! Form::checkbox('roles[]', 4, (isset($roles) and in_array("4",$roles)), ['class' => 'roles', 'id' => 'checkboxExample4']) !!}
                        <label for="checkboxExample4">{{translate('form.roles_informed')}}</label>
                    </div>
                    <br clear="all">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-2 text-center">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    {!! Form::hidden('hid', isset($details->id) ? $details->id : '') !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn', 'tabindex' => '4')) !!}
                    {!! Html::link(url('/process/turtle/'.session('turtle_process_id')), translate('form.cancel'), array('class' => 'btn btn-hover btn-system', 'tabindex' => '5')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('includes.scripts.roles_form_script')
@stop