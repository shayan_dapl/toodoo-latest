@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <label id="processName">
                    @if($process->name != "")
                    {{translate('form.process_name')}}: {{$process->name}}
                    @endif
                </label>
                <span class="pull-right">
                    <a class="btn btn-system pull-right" href="{{url('/process/turtle').'/'.$process->id}}">
                        {{translate('tutorial.back_button')}}
                    </a>
                </span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="panel">
                            <div class="panel-heading bg-system text-center">
                                {{translate('words.supplier')}}
                            </div>
                            <div class="panel-body panel-body-ext turtleBox1 p5">
                                <ul class="h-a">
                                    @if(count($data['supplier']) > 0)
                                        @foreach($data['supplier'] as $eachData)
                                            <li class="process-cls">
                                                {{$eachData}}
                                            </li>
                                        @endforeach
                                    @else
                                        <li class="text-center pt15 pb15">{{translate('table.no_data')}}</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel">
                            <div class="panel-heading bg-system text-center">
                                {{translate('words.input')}}
                            </div>
                            <div class="panel-body panel-body-ext turtleBox1 p5">
                                <ul class="h-a">
                                    @if(count($data['input'][0]) > 0)
                                        @foreach($data['input'][0] as $eachData)
                                            <li class="process-cls">
                                                {{$eachData->name}}
                                            </li>
                                        @endforeach
                                    @else
                                        <li class="text-center pt15 pb15">{{translate('table.no_data')}}</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="panel">
                            <div class="panel-heading bg-system text-center">
                                 {{translate('words.process')}}
                            </div>
                            <div class="panel-body panel-body-ext turtleBox1 p5">
                                <ul class="h-a">
                                    @if(count($data['steps'][0]) > 0)
                                        @foreach($data['steps'][0] as $eachData)
                                            <li class="process-cls">
                                                {{$eachData->step_name}}
                                            </li>
                                        @endforeach
                                    @else
                                        <li class="text-center pt15 pb15">{{translate('table.no_data')}}</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel">
                            <div class="panel-heading bg-system text-center">
                                 {{translate('words.output')}}
                            </div>
                            <div class="panel-body panel-body-ext turtleBox1 p5">
                                <ul class="h-a">
                                    @if(count($data['output'][0]) > 0)
                                        @foreach($data['output'][0] as $eachData)
                                            <li class="process-cls">
                                                {{$eachData->output}}
                                            </li>
                                        @endforeach
                                    @else
                                        <li class="text-center pt15 pb15">{{translate('table.no_data')}}</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="panel">
                            <div class="panel-heading bg-system text-center">
                                 {{translate('words.customers')}}
                            </div>
                            <div class="panel-body panel-body-ext turtleBox1 p5">
                                <ul class="h-a">
                                    @if(count($data['customer']) > 0)
                                        @foreach($data['customer'] as $eachData)
                                            <li class="process-cls">
                                                {{$eachData}}
                                            </li>
                                        @endforeach
                                    @else
                                        <li class="text-center pt15 pb15">{{translate('table.no_data')}}</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop