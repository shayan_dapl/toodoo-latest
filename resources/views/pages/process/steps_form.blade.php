@extends('layouts.default')
@section('content')
<div class="admin-form" id="steps-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/process-step/steps/form', 'class' => 'form-horizontal', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', translate('form.process_name'), ['class' => 'col-lg-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3>{{\App\Models\Process::find(session('turtle_process_id'))->name}}</h3>
                        {!! Form::hidden('process_id', session('turtle_process_id')) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('step_no', translate('form.step_no'), ['class' => 'col-lg-3 col-md-offset-2 control-label required']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {{ Form::text('step_no', (!empty($details) ? $details->step_no : old('step_no')), ['class' => 'gui-input','id'=>'step_no','required' => 'required', 'placeholder' => translate('form.step_no'), 'tabindex' => '1']) }}
                                <label for="step_no" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('step_no')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('step_name', translate('form.step_name'), ['class' => 'col-lg-3 col-md-offset-2 control-label required']) !!}
                    <div class="col-md-4">
                        <div class="section">
                            <label class="field prepend-icon">
                                {{ Form::text('step_name', (!empty($details) ? $details->step_name : old('step_name')), ['class' => 'gui-input','id'=>'step_name','required' => 'required', 'placeholder' => translate('form.step_name'), 'tabindex' => '1']) }}
                                <label for="step_name" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </label>
                            @if (count($errors) > 0)
                            <p class="text-danger mn">{{$errors->first('step_name')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('words.roles')}}</span>
            </div>
            <div class="row" id="roles-pos-{{count($roleDetails)+1}}">
                <label for="inputStandard" class="col-md-3 col-md-offset-2 col-sm-2 control-label @if(count($roleDetails) == 0) required @endif">
                    {{translate('form.position')}}
                </label>

                <div class="col-md-4 col-xs-10">
                    <div class="section">
                        <label class="field select">
                            @if(count($roleDetails) > 0)
                                {!! Form::select('roles['.(count($roleDetails)+1).'][position]', $position, isset($roleDetails->position_id) ? $roleDetails->position_id : old('position_id'), ['id' => ('position-id-'.(count($roleDetails)+1)), 'class' => 'positions', 'tabindex' => '1', 'data-sl' => (count($roleDetails)+1)]) !!}
                            @else
                                {!! Form::select('roles['.(count($roleDetails)+1).'][position]', $position, isset($roleDetails->position_id) ? $roleDetails->position_id : old('position_id'), ['id' => ('position-id-'.(count($roleDetails)+1)), 'class' => 'positions', 'required' => 'required', 'tabindex' => '1', 'data-sl' => (count($roleDetails)+1)]) !!}
                            @endif
                            <i class="arrow"></i>
                        </label>
                    </div>
                </div>

                <div class="col-md-5">
                    <button type="button" class="btn btn-system roles-add" data-id="{{count($roleDetails)+1}}"><i class="fa fa-plus"></i></button>
                    <span class="text-danger fs12" id="error-{{count($roleDetails)+1}}"></span>
                </div>

                <div class="col-md-3 col-md-offset-3 process-roles-extended">
                    <div class="checkbox-custom checkbox-system mb5 side-extended">
                        {!! Form::checkbox('roles['.(count($roleDetails)+1).'][responsibility][]', 1, (isset($roles) and in_array("1",$roles)), ['id' => ('checkboxExample'.(count($roleDetails)+1).'1'), 'data-sl' => (count($roleDetails)+1)]) !!}
                        <label for="checkboxExample{{count($roleDetails)+1}}1">{{translate('form.roles_responsible')}}</label>
                    </div>
                    <div class="checkbox-custom checkbox-system mb5 side-extended">
                        {!! Form::checkbox('roles['.(count($roleDetails)+1).'][responsibility][]', 3, (isset($roles) and in_array("3",$roles)), ['id' => ('checkboxExample'.(count($roleDetails)+1).'2'), 'data-sl' => (count($roleDetails)+1)]) !!}
                        <label for="checkboxExample{{count($roleDetails)+1}}2">{{translate('form.roles_consulted')}}</label>
                    </div>
                    <div class="checkbox-custom checkbox-system mb5 side-extended">
                        {!! Form::checkbox('roles['.(count($roleDetails)+1).'][responsibility][]', 4, (isset($roles) and in_array("4",$roles)), ['id' => ('checkboxExample'.(count($roleDetails)+1).'3'), 'data-sl' => (count($roleDetails)+1)]) !!}
                        <label for="checkboxExample{{count($roleDetails)+1}}3">{{translate('form.roles_informed')}}</label>
                    </div>
                </div>

                {!! Form::hidden('roles['.(count($roleDetails)+1).'][id]', '') !!}
            </div>
            @if(isset($roleDetails))
                @foreach($roleDetails as $role)
                <br clear="all">
                    <div class="row" id="roles-pos-{{$loop->iteration}}">
                        <label for="inputStandard" class="col-md-3 col-md-offset-2 col-sm-2 control-label required">
                            {{translate('form.position')}}
                        </label>

                        <div class="col-md-4 col-xs-10">
                            <div class="section">
                                <label class="field select">
                                    {!! Form::select('roles['.$loop->iteration.'][position]', $position, isset($role->position_id) ? $role->position_id : old('position_id'), ['id' => ('position-id-'.$loop->iteration), 'class' => 'positions', 'required' => 'required', 'tabindex' => '1', 'data-sl' => $loop->iteration]) !!}
                                    <i class="arrow"></i>
                                </label>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <button type="button" class="btn btn-danger roles-remove" data-id="{{$loop->iteration}}" data-processid = "{{$role->process_id}}" data-stepid = "{{$role->process_step_id}}" data-positionid = "{{$role->position_id}}" onclick="return confirm('{{translate('alert.are_you_sure')}}');"><i class="fa fa-minus"></i></button>
                            <span class="text-danger fs12" id="error-{{$loop->iteration}}"></span>
                        </div>

                        <div class="col-md-3 col-md-offset-3 process-roles-extended">
                            <div class="checkbox-custom checkbox-system mb5 side-extended">
                                {!! Form::checkbox('roles['.$loop->iteration.'][responsibility][]', 1, (in_array("1",explode(",", $role->roles_id))), ['id' => ('checkboxExample'.$loop->iteration.'1'), 'data-sl' => $loop->iteration]) !!}
                                <label for="checkboxExample{{$loop->iteration}}1">{{translate('form.roles_responsible')}}</label>
                            </div>
                            <div class="checkbox-custom checkbox-system mb5 side-extended">
                                {!! Form::checkbox('roles['.$loop->iteration.'][responsibility][]', 3, (in_array("3",explode(",", $role->roles_id))), ['id' => ('checkboxExample'.$loop->iteration.'2'), 'data-sl' => $loop->iteration]) !!}
                                <label for="checkboxExample{{$loop->iteration}}2">{{translate('form.roles_consulted')}}</label>
                            </div>
                            <div class="checkbox-custom checkbox-system mb5 side-extended">
                                {!! Form::checkbox('roles['.$loop->iteration.'][responsibility][]', 4, (in_array("4",explode(",", $role->roles_id))), ['id' => ('checkboxExample'.$loop->iteration.'3'), 'data-sl' => $loop->iteration]) !!}
                                <label for="checkboxExample{{$loop->iteration}}3">{{translate('form.roles_informed')}}</label>
                            </div>
                        </div>

                        {!! Form::hidden('roles['.$loop->iteration.'][id]', $role->id) !!}
                    </div>
                @endforeach
            @endif
            <br clear="all">
            <br clear="all">
            <div class="row">
                <div class="col-md-4 col-md-offset-2 text-center">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    {!! Form::hidden('role_id', isset($roleDetails->id) ? $roleDetails->id : '') !!}
                    {!! Form::hidden('', $everyOne->id, ['id' => 'everyone-pos']) !!}
                    {!! Form::hidden('hid', !empty($details) ? $details->id : '') !!}
                    {!! Html::link(url('/process/turtle/'.session('turtle_process_id')), translate('form.cancel'), ['class' => 'btn btn-hover btn-system', 'tabindex' => '3']) !!}
                    {!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'id' => 'profile-btn', 'tabindex' => '2']) !!}
                    {!! Form::button(translate('form.add_step'), ['name' => 'add_step', 'type' => 'submit', 'value' => 'add_step', 'class' => 'btn btn-hover btn-system', 'id' => 'add-step-btn', 'tabindex' => '3']) !!}
                </div>
            </div>
            {!! Form::close() !!}
            <div class="section-divider" id="spy2">
                <span>{{translate('form.process_step_list')}}</span>
            </div>
            <div class="panel-body pn">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('form.step_no')}}</th>
                            <th>{{translate('form.step_name')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($stepList as $k=>$v)
                        <tr>
                            <td>
                                {{$v['step_no']}}
                            </td>
                            <td>
                                {{$v['step_name']}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.process.steps_form_script')
@stop