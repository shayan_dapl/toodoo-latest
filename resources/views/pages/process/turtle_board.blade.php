@extends('layouts.default')
@section('content')
@include('includes.chart_assets')
@inject('color', 'App\Services\ProcessOpportunityMap')
<link rel="stylesheet" type="text/css" href="{{asset('css/hopscotch-0.2.6.min.css')}}">
@if($process->status == 0)
<div class="inactive_bgs"><h2>{{translate('words.inactive')}}</h2></div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <label id="processName">
                    @if($process->name != "")
                    {{translate('form.process_name')}}: {{$process->name}}
                    @endif
                </label>
                <a href="{{url('/process/turtle/output/'.$process->id)}}" class="ml20 btn btn-system">
                    <i class="fa fa-print"></i>
                </a>
                <a href="{{url('/process/sipoc/'.$process->id)}}" class="ml20 btn btn-system">
                    {{translate('words.sipoc')}}
                </a>
                <span class="pull-right">
                    {{translate('form.process_owner_accountable')}}: {{$process->owner->name}}
                    @if($process->owner->photo == "placeholder.png")
                        <img src="{{asset('image/placeholder.png')}}" alt="avatar" class="mw30 br64">
                    @else
                        <img src="{{asset('storage/company/'.$process->company_id.'/image/users/'.$process->owner->photo)}}" alt="avatar" class="mw30 br64">
                    @endif
                </span>
            </div>
            <div class="panel-body">
                <div class="admin-form">
                    <form name="turtle_Frm" action="" method="post" class="form-horizontal">
                        <input type="hidden" name="process_owner" id="process_owner" value="{{$process->owner->id}}"/>
                        <div class="row mb10">
                            <div class="col-md-12">
                            </div>
                        </div>

                        @if($process->name != "")
                        <div class="row turtle-board">
                            <div class="col-sm-3">
                                <div class="turtleBox">
                                    <div class="header" id="turtleScope">
                                        <i class="fa fa-bullseye"></i>
                                        {{translate('words.scope')}} 
                                        @if($modify == "yes")
                                        <a class="addBtn" href="{{URL('process-step/scope/form')}}">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        @endif
                                    </div>
                                    @if( isset($turtle_arr['scope'][0]) )
                                    <ul>
                                        @foreach($turtle_arr['scope'][0] as $ks=>$vs)
                                        <li class="line-cls" data-id="{{$vs->id}}" data-target="#turtle-details" data-toggle="modal" data-model="Scope">{{strippedText($vs->scope)}}</li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="turtleBox">
                                    <div class="header" id="turtleDocument">
                                        <i class="fa fa-play"></i>
                                        {{translate('words.procedures')}} 
                                        @if($modify == "yes")
                                        <a class="addBtn" href="{{url('process-step/procedure/form')}}">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        @endif
                                    </div>
                                    @if( isset($turtle_arr['procedures'][0]) )
                                    <ul>
                                        @foreach($turtle_arr['procedures'][0] as $kp=>$vp)
                                        <li class="line-cls" data-id="{{$vp->id}}" data-target="#turtle-details" data-toggle="modal" data-model="Procedures">
                                            {{strippedText($vp->doc_ref)}}
                                            @if(!empty($vp->docs[0]) && $vp->version!='')
                                            <span class="pull-right">
                                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                            </span>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="turtleBox">
                                    <div class="header" id="turtleRegulation">
                                        <i class="fa fa-legal"></i>
                                        {{translate('words.regulations')}}
                                        @if($modify == "yes")
                                        <a class="addBtn" href="{{URL('process-step/regulation/form')}}">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        @endif
                                    </div>
                                    @if( isset($turtle_arr['regulations'][0]) )
                                    <ul>
                                        @foreach($turtle_arr['regulations'][0] as $kr=>$vr)
                                        <li class="line-cls" data-id="{{$vr->id}}" data-target="#turtle-details" data-toggle="modal" data-model="Regulations">{{strippedText($vr->doc_ref)}}
                                        @if(!empty($vr->regdocs[0]) && $vr->version!='')
                                            <span class="pull-right">
                                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                            </span>
                                        @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="turtleBox">
                                    <div class="header" id="turtleKpi">
                                        <i class="fa fa-line-chart"></i>
                                        {{translate('words.kpi')}} 
                                        @if($modify == "yes")
                                        <a class="addBtn" href="{{URL('process-step/kpi/form')}}">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        @endif
                                    </div>
                                    @if(isset($turtle_arr['kpis'][0])) 
                                    <ul>
                                        @foreach($turtle_arr['kpis'][0] as $kk=>$vk)
                                        <li class="line-cls" data-id="{{$vk->id}}" data-target="#turtle-details" data-toggle="modal" data-model="Kpi">{{strippedText($vk->name)}}
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="turtleBox middleBox">
                                    <div class="header" id="turtleInput">
                                        <i class="fa fa-sign-in"></i>
                                        {{translate('words.input')}}
                                        @if($modify == "yes")
                                        <a class="addBtn" href="{{URL('process-step/input/form')}}">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        @endif
                                    </div>
                                    @if(isset($turtle_arr['input'][0]))
                                    <ul>
                                        @foreach($turtle_arr['input'][0] as $ki=>$vi)
                                        <li class="line-cls" data-id="{{$vi->id}}" data-target="#turtle-details" data-toggle="modal" data-model="Input">{{strippedText($vi->name)}}</li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="turtleBox middleBox">
                                    <div class="header" id="turtleProsessStep">
                                        <i class="fa fa-list-ol"></i>
                                        {{translate('words.process_steps')}}
                                        @if($modify == "yes")
                                        <a class="addBtn" href="{{URL('process-step/steps/form')}}">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        @endif
                                    </div>
                                    @if(isset($turtle_arr['steps'][0]))
                                    <ul id="sortable-icons">
                                        @foreach($turtle_arr['steps'][0] as $kst=>$vst)
                                        <li class="line-cls" data-id="{{$vst->id}}" data-target="#turtle-details" data-toggle="modal" data-model="ProcessSteps">{{$vst->step_no}}.{{strippedText($vst->step_name)}}</li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="turtleBox middleBox">
                                    <div class="header" id="turtleOutput">
                                        <i class="fa fa-sign-out"></i>
                                        {{translate('words.output')}}
                                        @if($modify == "yes")
                                        <a class="addBtn" href="{{URL('process-step/output/form')}}">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        @endif
                                    </div>
                                    @if(isset($turtle_arr['output'][0]))
                                    <ul>
                                        @foreach($turtle_arr['output'][0] as $kst=>$vst)
                                        <li class="line-cls" data-id="{{$vst->id}}" data-target="#turtle-details" data-toggle="modal" data-model="Output">{{strippedText($vst->output)}}</li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="turtleBox" id="turtleRisk">
                                    <div class="header">
                                        <i class="fa fa-bug"></i>
                                        {{translate('words.risk')}}
                                        @if($modify == "yes")
                                        <a class="addBtn" href="{{URL('process-step/risk/form')}}">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        @endif
                                    </div>
                                    @if(isset($turtle_arr['risks'][0]))
                                    <ul id="riskArea">
                                        @foreach($turtle_arr['risks'][0] as $kr => $vr)   
                                            <li class="line-cls levelcls text-default bw" data-id="{{$vr->id}}" data-target="#turtle-details" data-toggle="modal" data-model="Risk" style="background-color:{{$color->color($vr->risk_level)}};">{{strippedText($vr->risk)}}</li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="turtleBox">
                                    <div class="header" id="turtleOpportunity">
                                        <i class="fa fa-eye"></i>
                                        {{translate('words.opportunity')}}
                                        @if($modify == "yes")
                                            <a class="addBtn" href="{{URL('process-step/opportunity/form')}}">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        @endif
                                    </div>
                                    @if(isset($turtle_arr['opportunity'][0]))
                                    <ul id="opportunityArea">
                                        @foreach($turtle_arr['opportunity'][0] as $ko => $vo)
                                            <li class="line-cls levelcls text-default bbw" data-id="{{$vo->id}}" data-target="#turtle-details" data-toggle="modal" data-model="Opportunity" style="background-color:{{$color->color($vo->level)}};">{{strippedText($vo->details)}}</li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="turtleBox">
                                    <div class="header" id="turtleResources">
                                        <i class="fa fa-users"></i>
                                        {{translate('words.resources')}}
                                        @if($modify == "yes")
                                            <a class="addBtn" href="{{URL('process-step/resources/form')}}">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        @endif
                                    </div>
                                    @if(isset($turtle_arr['resources'][0]))
                                        <ul>
                                            @foreach($turtle_arr['resources'][0] as $kr => $vr)
                                                <li class="line-cls" data-id="{{$vr->id}}" data-target="#turtle-details" data-toggle="modal" data-model="Resources">
                                                    {{strippedText($vr->resource)}}
                                                    @if(!empty($vr->docs[0]))
                                                    <span class="pull-right">
                                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                                    </span>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="turtleBox">
                                    <div class="header" id="turtleRoles">
                                        <i class="fa fa-mortar-board"></i>
                                        {{translate('words.roles')}}
                                    </div>
                                    @if(isset($turtle_arr['roles'][0]) and !empty($turtle_arr['roles'][0]))
                                    @inject('role', 'App\Services\ProcessRaciMap')
                                        <ul>
                                            @foreach($turtle_arr['roles'][0] as $kr => $vr)
                                                <li class="line-cls" data-id="{{$vr->id}}" data-target="#turtle-details" data-toggle="modal" data-model="Roles">
                                                    {{strippedText($vr->position_name)}}
                                                    <span class="pull-right text-danger">
                                                        {{$role->role($vr->role_id)}}
                                                    </span>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @else
                                        <ul>
                                            
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @else
                        <h1>{{translate('form.choose_process_to_view_turtle')}}</h1>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="turtle-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content turtle-details-modal-content">
            <div class="modal-header text-default back4d">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title turtle-title"></h4>
            </div>
            <div class="modal-body turtle-body row justified w-wrap-extended">
                <p>.....</p>
            </div>
        </div>
    </div>
</div>
<div id="event-log-details" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-extended">
        <div class="modal-content">
            <div class="modal-header text-default back4d">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{translate('words.event_logging')}}</h4>
            </div>
            <div class="modal-body event-log-body row justified w-wrap-extended">
                <p>.....</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{translate('started.close')}}</button>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.turtle_board_script')
@stop