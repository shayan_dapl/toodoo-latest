<style type="text/css">
	table tr td {
	    font-family: Arial;
	    border: 1px solid #000;
	    font-size: 12px;
	    height: 120px;
	}

	td:first-child {
		height: 10px !important;
	}

	ul {
	    list-style: none;
	    text-align: left;
	    text-align: justify;
	    padding: 2%;
	    margin: 0
	}

	ul li {
	    margin-bottom: 3px
	}

	.first-td {
	    background-color: #d8d8d8;
	    font-weight: 600;
	    font-size: 20px
	}

	.second-tr {
	    background-color: #e2efda
	}
	
	.third-tr {
	    background-color: #ddebf6
	}
	
	.fourth-tr {
	    background-color: #ffc
	}

	.td-title {
	    vertical-align: top;
	    font-weight: 600
	}
	
	.role-text {
	    position: absolute;
	    right: 20px !important;
	}
	
	.responsibilities li {
	    position: relative
	}
</style>
<table width="100%" cellpadding="0" cellspacing="0" id="turtle-output">
	<tr>
		<td colspan="2" style="border: none;">
			<img src="{{public_path('storage/company/'.Auth::guard('customer')->user()->company_id.'/image/'.Auth::guard('customer')->user()->logo)}}" height="100">
		</td>
		<td style="border: none; text-align: right">{{date("d-m-Y")}}</td>
	</tr>
	<tr>
		<td colspan="3" align="center" class="first-td">
			{{strtoupper(translate('words.process'))}} : {{$process->name}}
		</td>
	</tr>
	<tr>
		<td align="center" class="second-tr" width="33.3%" valign="top">
			<div class="td-title">{{translate('words.scope')}}</div>
			@if( isset($turtle['scope'][0]) )
	            <ul>
	                @foreach($turtle['scope'][0] as $ks=>$vs)
	                <li>{{$vs->scope}}</li>
	                @endforeach
	            </ul>
            @endif
		</td>
		<td align="center" class="second-tr" width="33.3%" valign="top">
			<div class="td-title">{{translate('words.procedures')}}</div>
			@if( isset($turtle['procedures'][0]) )
	            <ul>
	                @foreach($turtle['procedures'][0] as $kp=>$vp)
	                <li>
	                    {{$vp->doc_ref}}
	                    @if(!empty($vp->documents($vp->id)))
	                    	<b style="float:right">{{$vp->documents($vp->id)->doc_name}}</b>
	                    @endif
	                </li>
	                @endforeach
	            </ul>
            @endif
            @if( isset($turtle['regulations'][0]) )
	            <ul>
	                @foreach($turtle['regulations'][0] as $kr=>$vr)
	                <li>{{$vr->doc_ref}}</li>
	                @endforeach
	            </ul>
            @endif
		</td>
		<td align="center" class="second-tr" width="33.3%" valign="top">
			<div class="td-title">{{translate('words.kpi')}}</div>
			@if(isset($turtle['kpis'][0])) 
	            <ul>
	                @foreach($turtle['kpis'][0] as $kk=>$vk)
		                <li>
		                	{{$vk->name}}
		                    @if(!empty($vk->target))
		                        {{$vk->target}}
		                    @endif
		                </li>
	                @endforeach
	            </ul>
            @endif
		</td>
	</tr>
	<tr>
		<td align="center" class="third-tr" width="33.3%" valign="top">
			<div class="td-title">{{translate('words.input')}}</div>
			@if(isset($turtle['input'][0]))
	            <ul class="responsibilities">
	                @foreach($turtle['input'][0] as $ki=>$vi)
	                	<li>{{$vi->name}}</li>
	                @endforeach
	            </ul>
            @endif
		</td>
		<td align="center" class="third-tr" width="33.3%" valign="top" rowspan="2" style="border-left: 1px solid #000">
			<div class="td-title">{{translate('words.process_steps')}}</div>
			@if(isset($turtle['steps'][0]))
	            <ul id="sortable-icons">
	                @foreach($turtle['steps'][0] as $kst=>$vst)
	                	<li>{{$vst->step_no}}.{{$vst->step_name}}</li>
	                @endforeach
	            </ul>
            @endif
		</td>
		<td align="center" class="third-tr" width="33.3%" valign="top">
			<div class="td-title">{{translate('words.output')}}</div>
			@if(isset($turtle['output'][0]))
	            <ul>
	                @foreach($turtle['output'][0] as $kst=>$vst)
	                	<li>{{$vst->output}}</li>
	                @endforeach
	            </ul>
            @endif
		</td>
	</tr>
	<tr>
		<td align="center" class="third-tr" valign="top" style="border-right: 1px solid #000">
			<div class="td-title">{{translate('words.linked_process')}}</div>
			@if(isset($turtle['input'][0]))
	            <ul class="responsibilities">
	                @foreach($turtle['input'][0] as $ki=>$vi)
	                	<li>{{$vi->supporting_process}}</li>
	                @endforeach
	            </ul>
            @endif
		</td>
		<td align="center" class="third-tr" valign="top">
			<div class="td-title">{{translate('words.linked_process')}}</div>
			@if(isset($turtle['output'][0]))
	            <ul>
	                @foreach($turtle['output'][0] as $kst=>$vst)
	                	<li>{{$vst->supporting_process}}</li>
	                @endforeach
	            </ul>
            @endif
		</td>
	</tr>
	<tr>
		<td align="center" class="fourth-tr" valign="top">
			<div class="td-title">
				{{translate('words.risk')}} &amp; {{translate('words.opportunity')}}
			</div>
			@if(isset($turtle['risks'][0]))
	            <ul>
	                @foreach($turtle['risks'][0] as $kr => $vr)
	                    <li>R: {{strippedText($vr->risk)}} - {{$vr->risk_level}}</li>
	                @endforeach
	            </ul>
            @endif
            @if(isset($turtle['opportunity'][0]))
	            <ul id="opportunityArea">
	                @foreach($turtle['opportunity'][0] as $ko => $vo)
	                    <li>O: {{strippedText($vo->details)}} - {{$vo->level}}</li>
	                @endforeach
	            </ul>
            @endif
		</td>
		<td align="center" class="fourth-tr" valign="top">
			<div class="td-title">{{translate('words.resources')}}</div>
			@if(isset($turtle['resources'][0]))
                <ul>
                    @foreach($turtle['resources'][0] as $kr => $vr)
                        <li>{{$vr->resource}}</li>
                    @endforeach
                </ul>
            @endif
		</td>
		<td align="center" class="fourth-tr" valign="top">
			<div class="td-title">{{translate('words.roles')}}</div>
			@if(isset($turtle['roles'][0]) and !empty($turtle['roles'][0]))
			@inject('role', 'App\Services\ProcessRaciMap')
                <ul>
                    @foreach($turtle['roles'][0] as $kr => $vr)
                        <li>
                            {{$vr->position_name}}
                            <span class="role-text">
                                {{$role->role($vr->role_id)}}
                            </span>
                        </li>
                    @endforeach
                </ul>
            @endif
		</td>
	</tr>
</table>