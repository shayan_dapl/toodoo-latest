@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/process-step/kpi/dataform', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/process-step/kpi/datalist/'.$details->id), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('words.kpi_value')}}</span>
            </div>
             <div class="row">
                <div class="form-group">
                    {!! Form::label('name', translate('form.kpi_name'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3 class="mt10">{{$details->name}}</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    {!! Form::label('valid_from',translate('form.valid_from'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3 class="mt10">{{date(Config::get('settings.dashed_date'), strtotime($details->valid_from))}}</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    {!! Form::label('valid_to',translate('form.valid_to'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3 class="mt10">{{date(Config::get('settings.dashed_date'), strtotime($details->valid_to))}}</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    {!! Form::label('frequency',translate('form.frequency'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3 class="mt10">{{$frequency[$details->frequency]}}</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    {!! Form::label('kpi_unit',translate('form.kpi_unit'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                        <h3 class="mt10">{{$kpiUnits[$details->kpi_unit_id]}}</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    {!! Form::label('target',translate('form.target'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4">
                    @if($details->kpi_unit_id == 3)
                        <h3 class="mt10">{{$boolOptions[$details->target]}}</h3>
                    @elseif($details->kpi_unit_id == 4)
                        <h3 class="mt10">{{$trendOptions[$details->target]}}</h3>
                    @else
                        <h3 class="mt10">{{$details->target}}</h3>
                    @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    {!! Form::label('kpi_value',translate('words.kpi_value'), ['class' => 'col-lg-3 col-md-3 col-md-offset-2 control-label']) !!}
                    <div class="col-md-4 @if($details->kpi_unit_id == 3 || $details->kpi_unit_id == 4) hide @endif">
                         {!! Form::text('kpi_value_txt',(isset($processKpiData->kpi_value) && ($details->kpi_unit_id == 1 || $details->kpi_unit_id == 2 )) ? $processKpiData->kpi_value : '', array('class' => 'gui-input', 'id' => 'txtOpt-field', 'placeholder' => translate('words.kpi_value'), 'required' => 'required', 'tabindex' => '3', 'oninput'=>"validateNumber(this);")) !!}
                    </div>
                    <div class="col-md-4 @if($details->kpi_unit_id != 3) hide @endif">
                         {!! Form::select('kpi_value_bool',$boolOptions,(isset($processKpiData->kpi_value) && ($details->kpi_unit_id == 3)) ? $processKpiData->kpi_value : '', array('class'=>'select2-single select-target','id' => 'boolOpt-field','required' => 'required')) !!}
                    </div>
                    <div class="col-md-4 @if($details->kpi_unit_id != 4) hide @endif">
                          {!! Form::select('kpi_value_trnd',$trendOptions, (isset($processKpiData->kpi_value) && ($details->kpi_unit_id == 4)) ? $processKpiData->kpi_value : '', array('class'=>'select2-single select-target','id' => 'trendOpt-field','required' => 'required')) !!}
                    </div>
                </div>
            </div>
            <div class="section-divider mv40" id="spy2">
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    <input type="hidden" id="hid" name="hid" value="{{ $processKpiData->id or '' }}">
                    <input type="hidden" id="kpi_id" name="kpi_id" value="{{ $details->id }}">
                    {!! Html::link(url('/process-step/kpi/datalist/'.$details->id), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'save-btn')) !!}
                    <br clear="all">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var kpi_unit = '{{ isset($details->kpi_unit_id) ? $details->kpi_unit_id : "" }}';
        if(kpi_unit == 1 || kpi_unit == 2){
            $('#boolOpt-field').removeAttr('required');
            $('#trendOpt-field').removeAttr('required');
        }else if(kpi_unit == 3){
            $('#trendOpt-field').removeAttr('required');
            $('#txtOpt-field').removeAttr('required');
        }else if(kpi_unit == 4){
            $('#boolOpt-field').removeAttr('required');
            $('#txtOpt-field').removeAttr('required');
        }
        setTimeout(function(){
            $(".select-target").select2({
                width: '100%'
            });
        }, 10);
       
    })
    var validNumber = new RegExp(/^\d*\.?\d*$/);
    var lastValid = document.getElementById("txtOpt-field").value;
    function validateNumber(elem) {
          if (validNumber.test(elem.value)) {
            lastValid = elem.value;
          } else {
            elem.value = lastValid;
            new PNotify({
                        title: "{{translate('alert.only_numeric_data_allowed')}}",
                        text: "",
                        addclass: 'stack_top_right',
                        type: 'error',
                        width: '290px',
                        delay: 2000
                    });
          }
        }
</script>
@stop
