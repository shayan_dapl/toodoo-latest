@extends('layouts.default')
	@section('content')
    <div class="panel panel-visible" id="spy2">
        <div class="panel-heading">
            <div class="panel-title hidden-xs">
                {{translate('words.edit')}}
            </div>
        </div>
        <div class="panel-body bg-light">
            <div class="admin-form">
            	{!! Form::open(array('method' => 'post', 'url' => '/settings/company-save', 'autocomplete' => 'off')) !!}
                <div class="section-divider mb40" id="spy1">
                    <span class="bg-default">{{translate('words.company_settings')}}</span>
                </div>
                <div class="row">
                    {!! Form::label('name', translate('words.urgency_count'), ['class' => 'col-lg-2 text-right control-label pt10 required']) !!}
                    <div class="col-md-1">
                        <div class="section">
                            {!! Form::text('urgency_count', isset($data) ? $data->urgency_count : old('urgency_count'), ['class' => 'gui-input', 'required' => 'required', 'tabindex' => '1']) !!}
                        </div>
                    </div>
                    {!! Form::label('name', translate('words.deadline_editable'), ['class' => 'col-lg-2 text-right control-label pt10']) !!}
                    <div class="col-md-1">
                        <div class="section">
                            <label class="switch-extended switch-success">
								{!! Form::checkbox('deadline_editable', 1, (isset($data) and $data->deadline_editable == 1), ['id' => 'deadline_editable']) !!}
								<div class="slider-extended round"></div>
							</label>
                        </div>
                    </div>
                    {!! Form::label('name', translate('words.supplier_threshold_limit'), ['class' => 'col-lg-3 text-right control-label pt10 required']) !!}
                    <div class="col-md-1">
                        <div class="section w50">
                            <span class="threshold">
                            {!! Form::text('supplier_threshold', isset($data) ? $data->supplier_threshold : old('supplier_threshold'), ['class' => 'gui-input', 'required' => 'required', 'tabindex' => '3']) !!}
                            </span>
                        </div>
                    </div>
                </div>
                <div class="section-divider mb40" id="spy1">
                    <span class="bg-default">{{translate('words.users_with_company_view_access')}}</span>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-body p1 memberbox">
                            <select class="user_view_access" multiple="multiple" name="users[]">
                                @foreach($users as $k=>$user)
                                <option value="{{$k}}" @if(in_array($k,$selectedUsers)) selected @endif>
                                        {{$user}}
                                </option> 
                                @endforeach
                            </select> 
                        </div>  
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-6">
                		@if ($errors->any())
                			<ul>
                				@foreach ($errors->all() as $error)
                				<li class="text-danger mn">{{ $error }}</li>
                				@endforeach
                			</ul>
                		@endif
                	</div>
                    <div class="col-md-6 text-right">
                        {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'tabindex' => '3')) !!}
                    </div>
                </div>
                {!! csrf_field() !!}
            	{!! Form::hidden('hid', $companyId) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function() {
        var demo3 = $('.user_view_access').bootstrapDualListbox({
            nonSelectedListLabel: "{{translate('words.select_user')}}",
            selectedListLabel: "{{translate('words.users_with_company_view_access')}}",
            preserveSelectionOnMove: 'moved',
            moveOnSelect: true,
        });
    });
    </script>
@stop