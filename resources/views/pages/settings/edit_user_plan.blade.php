@extends('layouts.default')
@section('content')
<div class="admin-form" id="edit-user-plan">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/package/user-plan', 'autocomplete' => 'off', 'class' => 'form-horizontal')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('package/user-plan'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
           <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="section">
                        {!! Form::label('user_count', (translate('form.no_of_user')), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field append-icon">
                           {{$details->user_count}}
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">   
                <div class="col-md-4 col-md-offset-4">
                    <div class="section">
                       {!! Form::label('price', translate('form.price'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field select" id="parent_type_import">
                            {!! Form::number('price', (isset($details->price_per_month)) ? $details->price_per_month : old('price_per_month'), array('class' => 'gui-input', 'id' => 'price', 'placeholder' => translate('form.price'), 'step' => 'any', 'min' => '1', 'tabindex' => '3')) !!}              
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('price')}}</p>
                        @endif
                        </label>
                    </div>
                </div>		
            </div>
            @if($details->is_max==1)
			<div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="section">
                    {!! Form::label('package_type', translate('form.package_type'), array('class' => 'field-label fs15 mb5')) !!}
                    <label class="field select">
                        <span class="lbl switch-custom-pos text-success">{{translate('form.max_package')}}</span>
                    </label>
                    </div>
                </div>
			</div>
            @endif
            	   
            <div class="row">
                <div class="col-md-offset-5">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', isset($details->id) ? $details->id : '') !!}
                    {!! Html::link(url('package/user-plan'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn', 'tabindex' => '6')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop