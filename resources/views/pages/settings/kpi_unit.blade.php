@extends('layouts.default')
	@section('content')
	<span id="kpi-unit" v-cloak>
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<div class="panel-heading">
						{{translate('words.list')}}
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-stripped">
							<tr>
								<th>
									{{translate('table.name')}}
								</th>
								<th>
									{{strtoupper(translate('table.action'))}}
								</th>
							</tr>
							<tr v-for="(each, i) in units">
								<td>
									<span ref="@{{i}}" v-show="!each.edit">@{{each | format}}</span>
									<table v-show="each.edit">
										<tr>
											<th class="pr5" align="center">{{translate('words.nl')}}</th>
											<th align="center">{{translate('words.en')}}</th>
											<th class="pl5" align="center">{{translate('words.fr')}}</th>
										</tr>
										<tr>
											<td class="pr5">
												<input type="text" v-model="each.name_nl" class="form-control" />
											</td>
											<td>
												<input type="text" v-model="each.name_en" class="form-control" />
											</td>
											<td class="pl5">
												<input type="text" v-model="each.name_fr" class="form-control" />
											</td>
										</tr>
									</table>
								</td>
								<td>
									<button class="btn btn-sm btn-success" v-show="!each.edit" @click="editData(each, i)">
										<i class="fa fa-pencil"></i>
									</button>
									<input type="hidden" v-model="id" />
									<button class="btn btn-sm btn-system" v-show="each.edit" @click="save(each, i)">
										{{translate('form.save_button')}}
									</button>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</span>
	@include('includes.scripts.kpi_unit_script')
@stop