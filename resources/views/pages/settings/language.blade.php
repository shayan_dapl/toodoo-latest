@extends('layouts.default')
@section('content')
<div class="panel">
	<div class="panel-heading text-right" id="app" v-cloak>
		<span class="text-danger fs14">@{{waitmessage}}</span>
		<span class="text-success fs14">@{{success}}</span>
		<button class="btn btn-system" :disabled="disable" @click="restore">
			{{translate('form.update')}}
			<i class="fa fa-cog fa-spin" v-if="wait"></i>
		</button>
	</div>
	<div class="panel-body">
		<div class="row">
			@foreach($languages as $segment => $data)
				{!! Form::open(['method' => 'post', 'url' => url('/language')]) !!}
					<div class="col-md-12 panel-heading exhight text-center fw700 mb5">
						<a href="javascript:void(0);" class="minimize pull-left btn btn-danger" data-segment="{{$segment}}">
							<i class="fa fa-minus"></i>
						</a> 
						{{strtoupper($segment)}}
						{!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-system pull-right']) !!}
					</div>
					<span id="{{$segment}}">
						<div class="row mb5">
							<div class="col-md-3 pt5 pl20">
								&nbsp;
							</div>
							<div class="col-md-3 text-center">
								{{translate('words.nl')}}
							</div>
							<div class="col-md-3 text-center">
								{{translate('words.en')}}						
							</div>
							<div class="col-md-3 text-center">
								{{translate('words.fr')}} 
							</div>
						</div>
						@foreach($data as $each)
							<div class="row mb5">
								<div class="col-md-3 pt5 pl20">
									{{ucwords(str_replace('_', ' ', $each['field']))}}
								</div>
								<div class="col-md-3">
									{!! Form::text('lang['.$each['id'].'][nl]', $each['nl'], ['class' => 'form-control']) !!}
								</div>
							    <div class="col-md-3">
								    {!! Form::text('lang['.$each['id'].'][en]', $each['en'], ['class' => 'form-control']) !!}
							    </div>
							    <div class="col-md-3">
								    {!! Form::text('lang['.$each['id'].'][fr]', $each['fr'], ['class' => 'form-control']) !!}
							    </div>
						    </div>
						@endforeach
						{!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-system pull-right mb10 mr10']) !!}
					</span>
					{!! csrf_field() !!}
				{!! Form::close() !!}
			@endforeach
		</div>
	</div>
</div>
@include('includes.scripts.language_script')
@stop