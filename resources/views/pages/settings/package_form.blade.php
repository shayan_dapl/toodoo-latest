@extends('layouts.default')
@section('content')
@inject('packageName', 'App\Services\Membership')
@inject('packageDescription', 'App\Services\Membership')
<div class="admin-form" id="package-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/package/form', 'autocomplete' => 'off', 'class' => 'form-horizontal')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/package/list'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
           <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('iso_reference', translate('form.iso_reference'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field select">
                            {!! Form::select('iso_reference',$isoReference,(isset($auditType->iso_ref_id)) ? $auditType->iso_ref_id : (isset($selectedIso) ? $selectedIso : ""), ['id' => 'iso_reference', 'required' => 'required','tabindex' => '1']) !!}
                            <i class="arrow"></i>
                        </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section">
                       {!! Form::label('price', translate('form.price'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field select" id="parent_type_import">
                            {!! Form::number('price', (isset($details->price)) ? $details->price : old('price'), array('class' => 'gui-input', 'id' => 'price', 'placeholder' => translate('form.price'), 'required' => 'required', 'step' => 'any', 'min' => '1', 'tabindex' => '3')) !!}  
                            <input type="hidden" name="old_price" value = "{{(isset($details->price)) ? $details->price : old('price')}}">            
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('price')}}</p>
                        @endif
                        </label>
                    </div>
                </div>		
            </div>
            @if(isset($details) and $details->is_base == 1)
			<div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <span class="lbl switch-custom-pos text-success">{{translate('form.base_package')}}</span>
                </div>
			</div>
            @endif
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <label>&nbsp;</label>
                    <div class="tab-block mb25">
                        <ul class="nav nav-tabs">
                            @foreach (\Config::get('settings.lang') as $lang => $code)
                            <li class="lang-links {{$lang}} @if(session('lang') == $lang) active @endif">
                                <a href="#{{$lang}}" data-toggle="tab">{{translate('words.'.$lang)}}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach (\Config::get('settings.lang') as $lang => $code)
                            <div id="{{$lang}}" class="tab-pane @if(session('lang') == $lang) active @endif">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('name_' . $lang, translate('form.package_name'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            <label class="field prepend-icon">
                                                {{ Form::text('name_' . $lang, isset($details) ? $packageName->packageName($details) : old('name_' . $lang), ['class' => 'gui-input','id'=>'name_' . $lang,'required' => 'required', 'placeholder' => translate('form.package_name')]) }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('description_' . $lang, translate('form.package_desc'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            <label class="field prepend-icon">
                                                {{ Form::textarea('description_' . $lang, isset($details) ? $packageDescription->packageDescription($details) : old('description_' . $lang), ['class' => 'gui-textarea','id'=>'description_' . $lang,'required' => 'required', 'placeholder' => translate('form.package_desc'), 'tabindex' => '4']) }}
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>	
            <div class="row">
                <div class="col-md-offset-5">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', isset($details->id) ? $details->id : '') !!}
                    {!! Form::hidden('base_package', isset($details->is_base) ? $details->is_base : '0') !!}
                    {!! Html::link(url('package/list'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn', 'tabindex' => '6')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('pages.settings.summernote')
@stop