@extends('layouts.default')
@section('content')
@inject('status', 'App\Services\CompanyList')
@inject('packageName', 'App\Services\Membership')
@inject('packageDescription', 'App\Services\Membership')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-body">
                <table class="table table-striped table-hover" id="datatable2">
                    <thead>
                        <tr>
                            <th>{{translate('table.iso_reference')}}</th>
                            <th>{{translate('form.package_name')}}</th>
                            <th>{{translate('table.package_details')}}</th>
                            <th>{{translate('table.package_price')}}</th>
                            <th>{{translate('table.status')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $each)
                            <tr>
                                <td>{{$each->iso->iso_no_en}}</td>
                                <td>{{$packageName->packageName($each)}}</td>
                                <td>{!! $packageDescription->packageDescription($each) !!}</td>
                                <td>{{floatval($each->price)}} &euro;</td>
                                <td>{{$status->status($each->status)}}</td>
                                <td>
                                    <a href="{{ url('/package/edit/'.$each->id) }}" class="btn btn-default">
                                        <span class="text-success fa fa-pencil"></span>
                                    </a>
                                    @if($each->status == 0)
                                        <a href="javascript:void(0);" data-model="PackageMaster" data-id="{{$each->id}}" data-status="1" class="btn btn-default statusChange custom-tooltip" data-tooltip="{{translate('table.make_active')}}">
                                            <span class="text-danger fa fa-times"></span>
                                        </a>
                                    @else
                                    <a href="javascript:void(0);" data-model="PackageMaster" data-id="{{$each->id}}" data-status="0" class="btn btn-default statusChange custom-tooltip" data-tooltip="{{translate('table.make_inactive')}}">
                                        <span class="text-success fa fa-check"></span>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop