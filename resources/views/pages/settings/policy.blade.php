@extends('layouts.default')
	@section('content')
	<div class="row mt20">
		<div class="col-md-4 text-center">
			<img src="{{asset('/image/privacy.png')}}" />
			<p class="fs20 fw600">{{translate('words.privacy_policy')}}</p>
			<a href="https://devsite.too-doo.be/privacy/" class="btn btn-system bord-rad-5 fs20 mt20" target="_blank">{{translate('words.read')}}</a>
		</div>
		<div class="col-md-4 text-center">
			<img src="{{asset('/image/cookie.png')}}" />
			<p class="fs20 fw600">{{translate('words.cookie_policy')}}</p>
			<a href="https://devsite.too-doo.be/cookieverklaring/" class="btn btn-system bord-rad-5 fs20 mt20" target="_blank">{{translate('words.read')}}</a>
		</div>
		<div class="col-md-4 text-center">
			<img src="{{asset('/image/verkoop.png')}}" />
			<p class="fs20 fw600">{{translate('words.verkoop_policy')}}</p>
			<a href="https://devsite.too-doo.be/algemene-voorwaarden/" class="btn btn-system bord-rad-5 fs20 mt20" target="_blank">{{translate('words.read')}}</a>
		</div>
	</div>
@stop