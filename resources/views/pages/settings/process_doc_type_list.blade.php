@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-7">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    {{translate('words.list')}}
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.doctype_name')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list as $eachDetail)
                            <tr>
                                <td>{{$eachDetail->type_name}}</td>
                                <td>
                                    <a href="{{ url('/settings/doctype/'.$eachDetail->id) }}" class="btn btn-default">
                                        <span class="text-success fa fa-pencil"></span> {{translate('words.edit')}}
                                    </a>
                                    @if($eachDetail->active() == 0)
                                    <a href="{{ url('/settings/doctype/remove/'.$eachDetail->id) }}" class="btn btn-default" onclick="return confirm('{{translate('alert.are_you_sure')}}')">
                                        <span class="text-danger fa fa-trash"></span> {{translate('table.remove')}}
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    {{translate('words.add')}} / {{translate('words.edit')}}
                </div>
            </div>
            <div class="panel-body bg-light">
                <div class="admin-form">
                    {!! Form::open(array('method' => 'post', 'url' => '/settings/doctype', 'class' => 'form-horizontal', 'autocomplete' => 'off')) !!}

                    <div class="row">
                        <div class="form-group">
                            {!! Form::label('type_name', translate('form.doctype'), ['class' => 'col-lg-4 control-label required']) !!}
                            <div class="col-md-8">
                                <div class="section">
                                    {!! Form::text('type_name', (isset($details->type_name) ? $details->type_name : old('type_name')), ['class' => 'gui-input', 'required' => 'required', 'tabindex' => '1'] ) !!}
                                    @if (count($errors) > 0)
                                    <p class="text-danger mn">{{$errors->first('type_name')}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            {!! csrf_field() !!}
                            {!! Form::hidden('hid', isset($details->id) ? $details->id : '') !!}
                            {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'profile-btn', 'tabindex' => '2')) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop