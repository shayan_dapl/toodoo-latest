@extends('layouts.main')
@section('content')
<div class="row p5">
    @foreach($model as $each)
    <div class="col-md-4">
        <div class="panel panel-system">
            <div class="panel-heading bg-system text-center">
                <span class="panel-title fw700">{{$each->name}}</span>
            </div>
            <div class="panel-body border text-center">
                <p class="justified">{{$each->details}}</p>
                <hr>
                <h2>{{$each->price .' '. $each->currency}}</h2>
                <hr>
                <button type="button" data-membership="{{$each->id}}" class="btn btn-system btn-lg activate-membership">{{translate('words.activate_membership')}}</button>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div class="row">
    <div class="col-md-12 p10">
        <a class="btn btn-danger text-default pull-right mr10" href="{{url('/out')}}" tabindex="1">
            {{ucfirst(translate('words.logout'))}}
        </a>
    </div>
</div>
@stop