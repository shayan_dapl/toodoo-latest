@extends('layouts.default')
	@section('content')
	{!! Form::open(['method' => 'post', 'url' => '/settings/risk-opp-action']) !!}
	<div class="row" id="risk-opp-action">
		<div class="col-md-4">
			<div class="panel">
				<div class="panel-heading text-default text-center" style="background-color:#dc2132">
					{{translate('form.high')}}
				</div>
				<div class="panel-body panel-body-ext turtleBox p5">
					<div class="checkbox-custom checkbox-system mt5 mb5 text-center">
						<input id="checkboxExample1" name="action[]" type="checkbox" value="H" @if(in_array('H', $definedAction)) checked="" @endif>
						<label class="pl5" for="checkboxExample1">&nbsp;</label>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel">
				<div class="panel-heading text-default text-center" style="background-color:#e67e22">
					{{translate('form.medium')}}
				</div>
				<div class="panel-body panel-body-ext turtleBox p5">
					<div class="checkbox-custom checkbox-system mt5 mb5 text-center">
						<input id="checkboxExample2" name="action[]" type="checkbox" value="M" @if(in_array('M', $definedAction)) checked="" @endif>
						<label class="pl5" for="checkboxExample2">&nbsp;</label>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel">
				<div class="panel-heading text-default text-center" style="background-color:#239b56">
					{{translate('form.low')}}
				</div>
				<div class="panel-body panel-body-ext turtleBox p5">
					<div class="checkbox-custom checkbox-system mt5 mb5 text-center">
						<input id="checkboxExample3" name="action[]" type="checkbox" value="L" @if(in_array('L', $definedAction)) checked="" @endif>
						<label class="pl5" for="checkboxExample3">&nbsp;</label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 text-center">
		    {!! Html::link(url('/home'), translate('form.cancel'), array('class' => 'btn btn-system text-default')) !!}
			{!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-system text-default')) !!} 
		</div>
	</div>
	{!! Form::close() !!}
@stop