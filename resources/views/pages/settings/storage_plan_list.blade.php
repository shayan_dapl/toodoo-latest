@extends('layouts.default')
@section('content')
@inject('status', 'App\Services\CompanyList')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-body">
                <table class="table table-striped table-hover" id="datatable2">
                    <thead>
                        <tr>
                            <th>{{translate('table.space')}}</th>
                            <th>{{translate('table.per_month')}} </th>
                            <th>{{translate('table.status')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $each)
                            <tr>
                                <td>{{$each->space/1024}} GB</td>
                                <td>{{floatval($each->price_per_month)}} &euro;</td>
                                <td>{{$status->status($each->status)}}</td>
                                <td>
                                    <div class="navbar-btn btn-group">
                                        <a href="{{ url('/package/storage-plan/edit/'.$each->id) }}" class="btn btn-default">
                                            <span class="text-success fa fa-pencil"></span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop