<link href="{{asset('js/summernote/summernote.css')}}" rel="stylesheet">
<script src="{{asset('js/summernote/summernote.js')}}"></script>
<script type="text/javascript">
	$('#summer-demo-dutch').summernote({
        height: 300,
    });

    $('#summer-demo-english').summernote({
        height: 300,
    });

    $('#summer-demo-french').summernote({
        height: 300,
    });
    ////Package area in supar admin
    $('#description_nl').summernote({
        height: 300,
    });

    $('#description_en').summernote({
        height: 300,
    });

    $('#description_fr').summernote({
        height: 300,
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('#description_nl').summernote('destroy');
        $('#description_nl').summernote({
            height: 300,
        });
        $('#description_en').summernote('destroy');
        $('#description_en').summernote({
            height: 300,
        });
        $('#description_fr').summernote('destroy');
        $('#description_fr').summernote({
            height: 300,
        });
    });
</script>