@extends('layouts.default')
	@section('content')
	<div class="row">
	    <div class="col-md-6">
	        <div class="panel panel-visible" id="spy2">
	            <div class="panel-heading">
	                <div class="panel-title hidden-xs">
	                    &nbsp;
	                </div>
	            </div>
	            <div class="panel-body bg-light">
	                <div class="admin-form">
	                	{!! Form::open(array('method' => 'post', 'url' => '/trial-period', 'autocomplete' => 'off')) !!}
	                    <div class="row">
	                        <div class="form-group">
	                            {!! Form::label('name', translate('form.trial_period_days'), ['class' => 'col-lg-4 control-label required']) !!}
	                            <div class="col-md-8">
	                                <div class="section">
	                                    {!! Form::number('package_trial_count', (!empty($data) ? $data->package_trial_count : 14), ['class' => 'gui-input', 'required' => 'required', 'min' => '1'] ) !!}
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="col-md-12 text-right">
	                            <input type="hidden" v-model="hid">
	                            {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system')) !!}
	                        </div>
	                    </div>
	                    {!! csrf_field() !!}
                    	{!! Form::hidden('hid', !(empty($data)) ? $data->id : 0) !!}
	                    {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@stop