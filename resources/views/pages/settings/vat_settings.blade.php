@extends('layouts.default')
@section('content')
@inject('name', 'App\Services\ProcessCategorized')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-body">
                <table class="table table-striped table-hover" id="datatable2">
                    <thead>
                        <tr>
                            <th>{{translate('table.country')}}</th>
                            <th>{{translate('table.vat_percent')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $each)
                            <tr>
                                <td>{{$name->name(session('lang'), $each)}}</td>
                                <td>{{!empty($each->vat_percent) ? $each->vat_percent : 'NA'}}</td>
                                <td>
                                    <div class="navbar-btn btn-group">
                                        <a href="{{ url('/vat-settings/edit/'.$each->id) }}" class="btn btn-default">
                                            <span class="text-success fa fa-pencil"></span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop