@extends('layouts.default')
@section('content')
@inject('category', 'App\Services\VideoTutorial')
@inject('title', 'App\Services\VideoTutorial')
@inject('description', 'App\Services\VideoTutorial')
@inject('path', 'App\Services\VideoTutorial')
@inject('description', 'App\Services\VideoTutorial')
@inject('limitText', 'App\Services\VideoTutorial')
<div class="panel">
    <div class="panel-heading text-right">
        <div class="row">
            <div class="col-md-11 text-right">
                <span class="mr5">{{translate('words.reopen_getstarted')}}</span>
            </div>
            <div class="col-md-1 text-left">
                <div class="switch switch-system switch-started round switch-inline mt10">
                    <input id="started-reopen" @if(Auth::user()->company->getstarted == 0 and empty(session('dismiss_started')) and empty(session('close_started'))) checked="" @endif type="checkbox">
                    <label for="started-reopen"></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label>&nbsp;</label>
            <div class="tab-block mb25">
                <ul class="nav nav-tabs" id="myTab">
                    @if ($categories->count() > 0)
                    @foreach ($categories as $each)
                    <li class="lang-links dropdown {{($loop->iteration == 1)? 'active' : ''}}">
                        @if ($each->parent_id == 0)
                        <a href="{{($each->children->count() > 0) ? '' : ('#tabno-'.$each->id)}}" data-toggle="{{($each->children->count() > 0) ? 'dropdown' : 'tab'}}" class="dropdown-toggle pointer" role="menu" aria-expanded="false">
                            {{$category->category(session('lang'), $each)}}
                            @if ($each->children->count() > 0)
                            <i class="fa fa-caret-down pl5"></i>
                            @endif
                        </a>
                        @if ($each->children->count() > 0)
                        <ul class="dropdown-menu">
                            @foreach($categories as $eachChildCat)
                            @if ($eachChildCat->parent_id == $each->id)
                            <li class="lang-links">
                                <a href="#tabno-{{$eachChildCat->id}}" data-toggle="tab">
                                    {{$category->category(session('lang'), $eachChildCat)}}
                                </a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                        @endif
                        @endif
                    </li>
                    @endforeach
                    @endif
                </ul>
                <div class="tab-content">
                    @if ($categories->count() > 0)
                    @foreach($categories as $each)
                    <div id="tabno-{{$each->id}}" class="tab-pane {{($loop->iteration == 1)? 'active' : ''}}">
                        @if ($each->contents->count() > 0)
                        @foreach ($each->contents as $content)
                        <div class="panel panel-tile text-center br-a br-light">
                            <div class="panel-header bg-light dark br-t br-light p12">
                                <span class="fs20 fw700">
                                    {{$title->title(session('lang'), $content)}}
                                </span>
                            </div>
                            <div class="panel-body bg-light">
                                <div class="row">
                                    <div class="col-md-3">
                                        @if ($path->path(session('lang'), $content) == null || $path->path(session('lang'), $content) == 'image/no_image.png')
                                            <img src="{{asset('image/no_image.png')}}" width="200" />
                                        @else
                                        <div class="vidImg videocls pointer" data-id="{{$content->id}}}" data-media="{{$content->media_type}}" data-imgsrc="{{asset($path->path(session('lang'), $content))}}" data-vidsrc="{{$path->path(session('lang'), $content)}}?autoplay=1" data-toggle="modal" data-target="#video-tutorial">
                                            @if ($content->media_type == 'image')
                                                <img src="{{asset($path->path(session('lang'), $content))}}" width="" />
                                            @elseif ($content->media_type == 'video')
                                                <a href="javascript:void(0)" class="popVidImg">
                                                   <i class="fa fa-play-circle fa-5x pos-absolute text-default video-icon-pos"></i>
                                                </a>
                                                <iframe id="player1" src="{{$path->path(session('lang'), $content)}}" width="" height="" frameborder="0"></iframe>
                                            @else
                                                <img src="{{asset($path->path(session('lang'), $content))}}" width="" />
                                            @endif
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-md-9 text-left">
                                        <span class="less">
                                            {!! $limitText->limitText($description->description(session('lang'), $content)) !!}
                                        </span>
                                        <span class="complete hide">
                                            {!! $description->description(session('lang'), $content) !!}
                                        </span>
                                        @if (str_word_count($description->description(session('lang'), $content), 0) > 50)
                                            <span class="text-system fs16 pointer more">{{translate('words.more')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <div class="panel panel-tile text-center br-a br-light">
                            <div class="panel-header bg-light dark br-t br-light p12">
                                <span class="fs20 fw700">
                                    {{translate('table.no_data')}}
                                </span>
                            </div>
                            <div class="panel-body bg-light">
                                
                            </div>
                        </div>
                        @endif
                    </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
</div>
<div id="video-tutorial" class="modal fade tutorial-videos" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-default bg-system">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body text-center">
                <img src="{{asset($path->path(session('lang'), $content))}}" class="imagemediacontent p20 max-height-400" />
                <iframe class="videomodalpositionlist" id="video-tutorial-video" src="" width="100%" height="460" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
 </div>
<script type="text/javascript">
    //Basically stops and starts the video on modal open/close
    $(document).ready(function () {
        $('.videocls').click(function () {
            var vidUrl = $(this).data('vidsrc');
            var mediaType = $(this).data('media');
            if (mediaType == 'image') {
                $('.imagemediacontent').removeClass('hide');
                $('.videomodalpositionlist').addClass('hide');
                $('.imagemediacontent').attr('src', $(this).data('imgsrc'));
            } else {
                $('.imagemediacontent').addClass('hide');
                $('.videomodalpositionlist').removeClass('hide');
                $('#video-tutorial-video').attr('src', vidUrl);
            }
        });

        $('#video-tutorial').on('hide.bs.modal', function (e) {
            $('#video-tutorial-video').attr('src','');
        });

        $(document).on("click", ".more", function () {
            if ($(this).siblings(".complete").hasClass("hide")) {
                $(this).text("{{translate('words.less')}}").siblings(".complete").removeClass("hide");    
                $(this).siblings(".less").addClass("hide");    
            } else {
                $(this).siblings(".less").removeClass("hide");    
                $(this).text("{{translate('words.more')}}").siblings(".complete").addClass("hide");    
            }
        });

        $('#started-reopen').click(function () {
            $.ajax({
                url: "{{url('/getstarted/reopen')}}",
                data : {
                    '_token' : "{{csrf_token()}}"
                },
                type: "POST",
                success: function () {
                    new PNotify({
                        title: "",
                        text: "{{translate('alert.get_started_not_dismissed')}}",
                        addclass: 'stack_top_right',
                        type: 'warning',
                        delay: 2000
                    });
                    setTimeout(() => {
                        location.reload();
                    }, 2000);
                }
            });
        });
    });
</script>
@stop