@extends('layouts.default')
@section('content')
<div class="admin-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/video/category/form', 'autocomplete' => 'off')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/video/category/list'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
            <div class="section-divider mb40" id="spy1">
                <span>{{translate('words.video_category')}}</span>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="section">
                        {!! Form::label('parent_cat', (translate('words.parent_category')), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field prepend-icon">
                            <label class="field select">
                                {!! Form::select('parent_cat', $category, isset($details->parent_id) ? $details->parent_id : '', ['id' => 'parent_cat', 'tabindex' => '6']) !!}
                                <i class="arrow"></i>
                            </label>
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('vid_cat_nl')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            @if ( !isset($details) or (isset($details) and ($details->parent_id == 0)) )
            <div class="row" id="seqtab">
                <div class="col-md-4 col-md-offset-4">
                    <div class="section">
                        {!! Form::label('sequence', translate('words.sequence'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field append-icon">
                            {!! Form::text('sequence',(isset($details->sequence)) ? $details->sequence : old('sequence'), array('class' => 'gui-input', 'id' => 'sequence', 'placeholder' => translate('words.sequence'), 'oninput'=>"validateNumber(this);", 'tabindex' => '1')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('sequence')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="section">
                        {!! Form::label('vid_cat_nl', (translate('words.video_category') . ' ('.translate('words.nl').')'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('vid_cat_nl',(isset($details->vid_cat_nl)) ? $details->vid_cat_nl : old('vid_cat_nl'), array('class' => 'gui-input', 'id' => 'vid_cat_nl', 'placeholder' => translate('words.video_category'), 'required' => 'required', 'tabindex' => '1')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('vid_cat_nl')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="section">
                        {!! Form::label('vid_cat_en', (translate('words.video_category') . ' ('.translate('words.en').')'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('vid_cat_en',(isset($details->vid_cat_en)) ? $details->vid_cat_en : old('vid_cat_en'), array('class' => 'gui-input', 'id' => 'vid_cat_en', 'placeholder' => translate('words.video_category'), 'required' => 'required', 'tabindex' => '2')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('vid_cat_en')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="section">
                        {!! Form::label('vid_cat_fr', (translate('words.video_category') . ' ('.translate('words.fr').')'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field append-icon">
                            {!! Form::text('vid_cat_fr',(isset($details->vid_cat_fr)) ? $details->vid_cat_fr : old('vid_cat_fr'), array('class' => 'gui-input', 'id' => 'vid_cat_fr', 'placeholder' => translate('words.video_category'), 'required' => 'required', 'tabindex' => '3')) !!}
                        </label>
                        @if (count($errors) > 0)
                        <p class="text-danger mn">{{$errors->first('vid_cat_fr')}}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="section-divider mv40" id="spy2">
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', isset($details) ? $details->id : '', ['id' => 'hid']) !!}
                    {!! Html::link(url('/video/category/list'), translate('form.cancel'), ['class' => 'btn btn-hover btn-system', 'tabindex' => '5']) !!}
                    {!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'id' => 'profile-btn', 'tabindex' => '4']) !!}
                    <br clear="all">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var categoryId = $('#parent_cat').val();
    if (categoryId == 0) {
        $('#sequence').prop('required',true);
    } 
    $('#parent_cat').change(function(){
            var catId = $(this).val();
            if(catId != 0){
                $('#seqtab').addClass('hide');
                $('#sequence').prop('required',false);
            } else {
                 $('#seqtab').removeClass('hide');
                 $('#sequence').prop('required',true);
            }
        })
})
 var validNumber = new RegExp(/^\d*\.?\d*$/);
 var lastValid = document.getElementById("sequence").value;
 function validateNumber(elem) {
          if (validNumber.test(elem.value)) {
            lastValid = elem.value;
          } else {
            elem.value = lastValid;
            new PNotify({
                        title: "{{translate('alert.only_numeric_data_allowed')}}",
                        text: "",
                        addclass: 'stack_top_right',
                        type: 'error',
                        width: '290px',
                        delay: 2000
                    });
          }
    }
</script>
@stop
