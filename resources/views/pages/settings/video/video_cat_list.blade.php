@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    {!! Html::link(url('/video/category/form'), translate('words.add'), ['class' => 'btn btn-system text-default pointer']) !!}
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" id="video-category" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('words.video_category')}}</th>
                            <th>{{translate('words.parent')}}</th>
                            <th>{{translate('words.sequence')}}</th>
                            <th>{{translate('table.action')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('vidCat' , App\Services\VideoCategoryList)
                        @foreach($details as $each)
                        <tr>
                            <td>{{$vidCat->vidCat(session('lang'),$each)}}</td>
                            <td>{{($each->parent_id != 0) ? $vidCat->vidCat(session('lang'), $each->parent) : ''}}</td>
                            <td>{{($each->parent_id == 0) ? $each->sequence : ''}}</td>
                            <td>
                                <a href="{{ url('/video/category/edit/'.$each->id) }}" class="btn btn-default">
                                    <span class="text-success fa fa-pencil"></span>
                                </a>
                                <a href="{{ url('/video/category/delete/'.$each->id) }}" class="btn btn-default" onclick="return confirm('{{translate('alert.all_tutorials_under_this_item_will_be_deleted_are_you_sure')}}')">
                                    <span class="text-danger fa fa-trash"></span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop