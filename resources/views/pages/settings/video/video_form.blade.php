@extends('layouts.default')
@section('content')
<div class="admin-form" id="package-form">
    <div class="panel">
        <div class="panel-body bg-light">
            {!! Form::open(array('method' => 'post', 'url' => '/video/form','enctype' => 'multipart/form-data', 'autocomplete' => 'off', 'class' => 'form-horizontal')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Html::link(url('/video/list'), translate('form.back'), array('class' => 'btn btn-hover btn-system pull-right')) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('video_cat', translate('words.video_category'), array('class' => 'field-label fs15 mb5 required')) !!}
                        <label class="field prepend-icon">
                            <label class="field select">
                                {!! Form::select('vid_category', $vidCategory, isset($selectedParent) ? $selectedParent : '', ['id' => 'category','required' => 'required']) !!}
                                <i class="arrow"></i>
                            </label>
                        </label>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section">
                        {!! Form::label('video_sub_cat', translate('words.sub_category'), array('class' => 'field-label fs15 mb5')) !!}
                        <label class="field prepend-icon">
                            <label class="field select">
                                <select id="video_sub_cat" name="video_sub_cat">
                                </select>
                                <i class="arrow"></i>
                            </label>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <label>&nbsp;</label>
                    <div class="tab-block mb25">
                        <ul class="nav nav-tabs">
                            <li class="lang-links Nederlands @if(session('lang') == 'nl') active @endif">
                                <a href="#Nederlands" data-toggle="tab">{{translate('words.nl')}}</a>
                            </li>
                            <li class="lang-links English @if(session('lang') == 'en') active @endif">
                                <a href="#English" data-toggle="tab">{{translate('words.en')}}</a>
                            </li>
                            <li class="lang-links Francais @if(session('lang') == 'fr') active @endif">
                                <a href="#Francais" data-toggle="tab">{{translate('words.fr')}}</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="Nederlands" class="tab-pane @if(session('lang') == 'nl') active @endif">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('title_nl', translate('form.video_title'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            <label class="field prepend-icon">
                                                {!! Form::text('title_nl', (isset($details->title_nl)) ? $details->title_nl : old('title_nl'), array('class' => 'gui-input', 'id' => 'title_nl', 'placeholder' => translate('form.video_title'), 'required' => 'required', 'step' => 'any', 'min' => '1', 'tabindex' => '3')) !!} 
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('description_nl', translate('form.video_desc'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            <label class="field prepend-icon">
                                                {{ Form::textarea('description_nl', (isset($details->description_nl) ? $details->description_nl : old('description_nl')), ['class' => 'gui-textarea','id'=>'description_nl','required' => 'required', 'placeholder' => translate('form.video_desc'), 'tabindex' => '4']) }}
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="English" class="tab-pane @if(session('lang') == 'en') active @endif">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('title_en', translate('form.video_title'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            <label class="field prepend-icon">
                                                {!! Form::text('title_en', (isset($details->title_en)) ? $details->title_en : old('title_en'), array('class' => 'gui-input', 'id' => 'title_en', 'placeholder' => translate('form.video_title'), 'required' => 'required', 'step' => 'any', 'min' => '1', 'tabindex' => '3')) !!} 
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('description_en', translate('form.video_desc'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            <label class="field prepend-icon">
                                                {{ Form::textarea('description_en', (isset($details->description_en) ? $details->description_en : old('description_en')), ['class' => 'gui-textarea','id'=>'description_en','required' => 'required', 'placeholder' => translate('form.video_desc'), 'tabindex' => '4']) }}
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Francais" class="tab-pane @if(session('lang') == 'fr') active @endif">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('title_fr', translate('form.video_title'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            <label class="field prepend-icon">
                                                {!! Form::text('title_fr', (isset($details->title_fr)) ? $details->title_fr : old('title_fr'), array('class' => 'gui-input', 'id' => 'title_fr', 'placeholder' => translate('form.video_title'), 'required' => 'required', 'step' => 'any', 'min' => '1', 'tabindex' => '3')) !!} 
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('description_fr', translate('form.video_desc'), array('class' => 'field-label fs15 mb5 required')) !!}
                                            <label class="field prepend-icon">
                                                {{ Form::textarea('description_fr', (isset($details->description_fr) ? $details->description_fr : old('description_fr')), ['class' => 'gui-textarea','id'=>'description_fr','required' => 'required', 'placeholder' => translate('form.video_desc'), 'tabindex' => '4']) }}
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    {!! Form::label('mediatype', translate('words.media_type'), ['class' => 'field-label fs15 mb5']) !!}
                    <div class="section">
                        <div class="radio-custom radio-system pt10">
                            <input type="radio" id="media-video" name="media_type" value="video" @if (isset($details->media_type) && $details->media_type == 'video') checked @endif >
                            <label for="media-video" class="mr10">{{translate('words.video')}}</label>

                            <input type="radio" id="media-image" name="media_type" value="image" @if (isset($details->media_type) && $details->media_type == 'image') checked @endif>
                            <label for="media-image">{{translate('words.image')}}</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide" id = "video-tab">
                <div class="col-md-8 col-md-offset-2">
                    <label>&nbsp;</label>
                    <div class="tab-block mb25">
                        <ul class="nav nav-tabs">
                            <li class="lang-links Nederlands @if(session('lang') == 'nl') active @endif">
                                <a href="#NederlandsVidLink" data-toggle="tab">{{translate('words.nl')}}</a>
                            </li>
                            <li class="lang-links English @if(session('lang') == 'en') active @endif">
                                <a href="#EnglishVidLink" data-toggle="tab">{{translate('words.en')}}</a>
                            </li>
                            <li class="lang-links Francais @if(session('lang') == 'fr') active @endif">
                                <a href="#FrancaisVidLink" data-toggle="tab">{{translate('words.fr')}}</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="NederlandsVidLink" class="tab-pane @if(session('lang') == 'nl') active @endif">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('video_link_nl', translate('form.video_link'), array('class' => 'field-label fs15 mb5')) !!}
                                            <label class="field prepend-icon">
                                                {!! Form::text('video_link_nl', (isset($details->path_nl)) ? $details->path_nl : old('video_link_nl'), array('class' => 'gui-input', 'id' => 'path_nl', 'placeholder' => translate('form.video_link'),'step' => 'any', 'min' => '1', 'tabindex' => '3')) !!} 
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="EnglishVidLink" class="tab-pane @if(session('lang') == 'en') active @endif">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('video_link_en', translate('form.video_link'), array('class' => 'field-label fs15 mb5')) !!}
                                            <label class="field prepend-icon">
                                                {!! Form::text('video_link_en', (isset($details->path_en)) ? $details->path_en : old('video_link_en'), array('class' => 'gui-input', 'id' => 'path_en', 'placeholder' => translate('form.video_link'), 'step' => 'any', 'min' => '1', 'tabindex' => '3')) !!} 
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="FrancaisVidLink" class="tab-pane @if(session('lang') == 'fr') active @endif">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section">
                                            {!! Form::label('video_link_fr', translate('form.video_link'), array('class' => 'field-label fs15 mb5')) !!}
                                            <label class="field prepend-icon">
                                                {!! Form::text('video_link_fr', (isset($details->path_fr)) ? $details->path_fr : old('video_link_fr'), array('class' => 'gui-input', 'id' => 'path_fr', 'placeholder' => translate('form.video_link'), 'step' => 'any', 'min' => '1', 'tabindex' => '3')) !!} 
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-comments"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide" id = "image-tab">
                <div class="col-md-8 col-md-offset-2">
                    <label>&nbsp;</label>
                    <div class="tab-block mb25">
                        <ul class="nav nav-tabs">
                            <li class="lang-links Nederlands @if(session('lang') == 'nl') active @endif">
                                <a href="#NederlandsImg" data-toggle="tab">{{translate('words.nl')}}</a>
                            </li>
                            <li class="lang-links English @if(session('lang') == 'en') active @endif">
                                <a href="#EnglishImg" data-toggle="tab">{{translate('words.en')}}</a>
                            </li>
                            <li class="lang-links Francais @if(session('lang') == 'fr') active @endif">
                                <a href="#FrancaisImg" data-toggle="tab">{{translate('words.fr')}}</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="NederlandsImg" class="tab-pane @if(session('lang') == 'nl') active @endif">
                                <div class="row">
                                  <div class="col-md-6">
                                      {!! Form::label('image_nl', translate('form.upload_doc'), array('class' => 'dblock mb10')) !!}
                                    <div class="">
                                        <div class="section">
                                            <label class="field prepend-icon append-button file" id="uploaderr">
                                                <span class="button">{{translate('form.file')}}</span>
                                                {!! Form::file('image_nl', ['class' => 'gui-file', 'id' => 'image_nl', 'accept' => '.jpeg,.jpg,.png']) !!}
                                                {!! Form::text('', '', ['class' => 'gui-input', 'id' => 'uploader1', 'placeholder' => translate('form.image')]) !!}
                                                <label class="field-icon">
                                                    <i class="fa fa-upload"></i>
                                                </label>
                                            </label>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                       <div class="pull-right">
                                        @if(isset($details->path_nl) and \File::isFile('storage/tutorial/'.$details->path_nl))
                                            {{ Html::image(asset('storage/tutorial/'.$details->path_nl), translate('form.company_picture'), ['class' => 'thumbnail pull-right', 'id' => 'image_nl_preview', 'height' => '100', 'width' => '100']) }}
                                        @else
                                            {{ Html::image(asset('/image/no_image.png'), translate('form.company_picture'), array('class' => 'thumbnail pull-right', 'id' => 'image_nl_preview', 'height' => '100', 'width' => '100')) }}
                                        @endif
                                    </div>
                                  </div>
                                    
                                   
                                </div>
                            </div>
                            <div id="EnglishImg" class="tab-pane @if(session('lang') == 'en') active @endif">
                                <div class="row">
                                     <div class="col-md-6">
                                          {!! Form::label('image_en', translate('form.upload_doc'), array('class' => 'dblock mb10')) !!}
                                        <div class="">
                                            <div class="section">
                                                <label class="field prepend-icon append-button file" id="uploaderr">
                                                    <span class="button">{{translate('form.file')}}</span>
                                                    {!! Form::file('image_en', ['class' => 'gui-file', 'id' => 'image_en', 'accept' => '.jpeg,.jpg,.png']) !!}
                                                    {!! Form::text('', '', ['class' => 'gui-input', 'id' => 'uploader2', 'placeholder' => translate('form.image')]) !!}
                                                    <label class="field-icon">
                                                        <i class="fa fa-upload"></i>
                                                    </label>
                                                </label>
                                            </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                           <div class="pull-right">
                                               @if(isset($details->path_en) and \File::isFile('storage/tutorial/'.$details->path_en))
                                                    {{ Html::image(asset('storage/tutorial/'.$details->path_en), translate('form.company_picture'), ['class' => 'thumbnail pull-right', 'id' => 'image_en_preview', 'height' => '100', 'width' => '100']) }}
                                                @else
                                                    {{ Html::image(asset('/image/no_image.png'), translate('form.company_picture'), array('class' => 'thumbnail pull-right', 'id' => 'image_en_preview', 'height' => '100', 'width' => '100')) }}
                                                @endif
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div id="FrancaisImg" class="tab-pane @if(session('lang') == 'fr') active @endif">
                                <div class="row">
                                    <div class="col-md-6">
                                        {!! Form::label('image_fr', translate('form.upload_doc'), array('class' => 'dblock mb10')) !!}
                                        <div class="">
                                            <div class="section">
                                                <label class="field prepend-icon append-button file" id="uploaderr">
                                                    <span class="button">{{translate('form.file')}}</span>
                                                    {!! Form::file('image_fr', ['class' => 'gui-file', 'id' => 'image_fr', 'accept' => '.jpeg,.jpg,.png']) !!}
                                                    {!! Form::text('', '', ['class' => 'gui-input', 'id' => 'uploader3', 'placeholder' => translate('form.image')]) !!}
                                                    <label class="field-icon">
                                                        <i class="fa fa-upload"></i>
                                                    </label>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="pull-right">
                                            @if(isset($details->path_fr) and \File::isFile('storage/tutorial/'.$details->path_fr))
                                                {{ Html::image(asset('storage/tutorial/'.$details->path_fr), translate('form.company_picture'), ['class' => 'thumbnail pull-right', 'id' => 'image_fr_preview', 'height' => '100', 'width' => '100']) }}
                                            @else
                                                {{ Html::image(asset('/image/no_image.png'), translate('form.company_picture'), array('class' => 'thumbnail pull-right', 'id' => 'image_fr_preview', 'height' => '100', 'width' => '100')) }}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-5">
                    {!! csrf_field() !!}
                    {!! Form::hidden('hid', isset($details->id) ? $details->id : '') !!}
                    {!! Html::link(url('video/list'), translate('form.cancel'), array('class' => 'btn btn-hover btn-system')) !!}
                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'vid-save-btn', 'tabindex' => '6')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('includes.scripts.video_form_script')
@include('pages.settings.summernote')
@stop