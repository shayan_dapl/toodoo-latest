@extends('layouts.default')
@section('content')
@inject('status', 'App\Services\CompanyList')
@inject('path', 'App\Services\VideoTutorial')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    <button type="button" class="btn btn-hover btn-system" onclick="location.href ='{{url('/video/form')}}'">{{ucfirst(translate('words.add'))}}</button>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" id="datatable2">
                    <thead>
                        <tr>
                            <th>{{translate('table.video')}} / {{translate('words.image')}}</th>
                            <th>{{translate('table.title')}}</th>
                            <th>{{translate('table.category')}}</th>
                            <th>{{translate('table.description')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('vidCat' , App\Services\VideoCategoryList)
                        @foreach($data as $each)
                            <tr>
                                <td>
                                    @if ($each->media_type == 'image')
                                        <img src="{{asset($path->path(session('lang'), $each))}}" height="100" width="100" />
                                    @elseif($each->media_type == 'video')
                                        <iframe id="player1" class="pointerNone" src="{{$path->path(session('lang'), $each)}}" width="100" height="100" frameborder="0"></iframe>
                                    @else 
                                        <img src="{{asset($path->path(session('lang'), $each))}}" height="100" width="100" />
                                    @endif
                                </td>
                                <td>{{$each->title_en}}</td>
                                <td>{{$vidCat->vidCat(session('lang'), $each->category)}}</td>
                                <td>{!!$each->description_en!!}</td>
                                <td>
                                    <a href="{{ url('/video/edit/'.$each->id) }}" class="btn btn-default">
                                        <span class="text-success fa fa-pencil"></span>
                                    </a>
                                     <a href="{{ url('/video/delete/'.$each->id) }}" class="btn btn-default" onclick="return confirm('{{translate('alert.are_you_sure')}}')">
                                    <span class="text-danger fa fa-trash"></span>
                                </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop