@extends('layouts.default')
@section('content')
@inject('status', 'App\Services\CompanyList')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    <button type="button" class="btn btn-hover btn-system" onclick="location.href ='{{url('/video/form')}}'">{{ucfirst(translate('words.add'))}}</button>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" id="datatable2">
                    <thead>
                        <tr>
                            <th>{{translate('table.video')}}</th>
                            <th>{{translate('table.title')}}</th>
                            <th>{{translate('table.description')}}</th>
                            <th>{{translate('table.operations')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $each)
                            <tr>
                                <td><img src="{{($each->cover_img !='') ? asset('/image/video-covers/'.$each->cover_img) : asset('/image/no_image.png')}}" height="100"></td>
                                <td>{{$each->title_en}}</td>
                                <td>{!! $each->description_en !!}</td>
                                <td>
                                    <a href="{{ url('/video/edit/'.$each->id) }}" class="btn btn-default">
                                        <span class="text-success fa fa-pencil"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop