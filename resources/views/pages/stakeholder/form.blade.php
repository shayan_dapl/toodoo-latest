@extends('layouts.default')
	@section('content')
	<div class="admin-form" id="stakeholder-form">
	    <div class="panel">
	        <div class="panel-body bg-light">
	            {!! Form::open(array('method' => 'post', 'url' => '/stakeholder-analysis/form', 'class' => 'form-horizontal', 'autocomplete' => 'off')) !!}
	            <div class="row">
	                <div class="form-group">
	                    {!! Form::label('interexter', translate('form.internal_external'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
	                    <div class="col-md-6">
	                        <div class="section">
								<div class="radio-custom radio-system pt10">
								  	<input type="radio" id="stakeholder-internal" name="internal_external" value="Internal" @unless(!empty($details->type) and $details->type == 'External') checked="" @endunless >
								  	<label for="stakeholder-internal">{{translate('form.stakeholder_internal')}}</label>

								  	<input type="radio" id="stakeholder-external" name="internal_external" value="External" @if(!empty($details->type) and $details->type == 'External') checked="" @endif >
								  	<label for="stakeholder-external">{{translate('form.stakeholder_external')}}</label>
								</div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	             <div class="row">
	                <div class="form-group">
	                    {!! Form::label('category', translate('form.category'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
	                    <div class="col-md-5">
	                        <div class="section">
	                        	<label class="field select">
									{!! Form::select('category', $category, !empty($details->category_id) ? $details->category_id : '', array('id' => 'category', 'required' => 'required', 'tabindex' => '1')) !!}
									<i class="arrow"></i>
                                </label>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="form-group">
	                    {!! Form::label('branch', translate('form.branch'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
	                    <div class="col-md-5">
	                        <div class="section branch">
	                        	<label class="field select">
									 {!! Form::select('branches[]',$branches, !empty($selectedBranches) ? $selectedBranches : '' , array('class'=>'select2-single select-branch','id' => 'branch','required' => 'required','multiple' => 'multiple')) !!}
                                </label>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
                    <div class="form-group">
                        {!! Form::label('stakeholder', translate('form.stakeholder_name'), ['class' => 'col-md-3 col-md-offset-2 control-label required']) !!}
                        <div class="col-md-5">
                            <label class="field prepend-icon">
                                {!! Form::text('stakeholder', !empty($details->stakeholder_name) ? $details->stakeholder_name : old('stakeholder'), ['class' => 'gui-input', 'id' => 'stakeholder', 'placeholder' => translate('form.stakeholder_name'), 'required' => 'required', 'tabindex' => '2']) !!}
                                <label for="stakeholder" class="field-icon">
                                    <i class="fa fa-cube"></i>
                                </label>
                            </label>
                            <p class="text-danger mn">
                                {{$errors->first('stakeholder')}}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        {!! Form::label('needs-expectations', translate('form.needs_expectations'), ['class' => 'col-md-3 col-md-offset-2 control-label required']) !!}
                        <div class="col-md-5">
                            <label class="field prepend-icon">
                                {!! Form::text('needs_expectations', !empty($details->needs) ? $details->needs : old('needs_expectations'), ['class' => 'gui-input', 'id' => 'needs-expectations', 'placeholder' => translate('form.needs_expectations'), 'required' => 'required', 'tabindex' => '3']) !!}
                                <label for="name" class="field-icon">
                                    <i class="fa fa-cube"></i>
                                </label>
                            </label>
                            <p class="text-danger mn">
                                {{$errors->first('needs_expectations')}}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row"><!-- Information -->
                    <div class="form-group">
                        {!! Form::label('detail', translate('form.detail'), ['class' => 'col-md-3 col-md-offest-2 control-label']) !!}
                        <div class="col-md-5">
                            <div class="section">
                                <label class="field prepend-icon">
                                    {{ Form::textarea('detail', !empty($details->detail) ? $details->detail : old('detail'), ['class' => 'gui-input', 'id' => 'detail', 'placeholder' => translate('form.detail'), 'tabindex' => '4']) }}
                                    <label for="detail" class="field-icon">
                                        <i class="fa fa-newspaper-o"></i>
                                    </label>
                                </label>
                                <p class="text-danger mn">{{$errors->first('detail')}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
            		<div class="form-group">
	                	{!! Form::label('impacts', translate('form.impacts'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
	                	<div class="col-md-5 pt10" id="impacts">
	                		<span class="rating block mn pull-left">
			                    <input class="rating-input" id="r1" type="radio" name="impacts" value="4" v-on:click="setRating('4')" @if(!empty($details->impact) and $details->impact == 4) checked="" @endif>
			                    <label class="rating-star text-system" for="r1">
		                      		<i class="fa fa-star va-m"></i>
			                    </label>
			                    <input class="rating-input" id="r2" type="radio" name="impacts" value="3" v-on:click="setRating('3')" @if(!empty($details->impact) and $details->impact == 3) checked="" @endif>
			                    <label class="rating-star text-system" for="r2">
		                      		<i class="fa fa-star va-m"></i>
			                    </label>
			                    <input class="rating-input" id="r3" type="radio" name="impacts" value="2" v-on:click="setRating('2')" @if(!empty($details->impact) and $details->impact == 2) checked="" @endif>
			                    <label class="rating-star text-system" for="r3">
		                      		<i class="fa fa-star va-m"></i>
			                    </label>
			                    <input class="rating-input text-system" id="r4" type="radio" name="impacts" value="1" v-on:click="setRating('1')" @unless(!empty($details->impact) and $details->impact != 1) checked="" @endunless>
			                    <label class="rating-star" for="r4">
		                      		<i class="fa fa-star va-m"></i>
			                    </label>
			                </span>
		                    <span class="ml15 lh20 h-20 fs14 pt5 label label-success label-sm" v-html="rating">
		                    	
		                    </span>
	                	</div>
	                </div>
                </div>

                <div class="row">
            		<div class="form-group">
	                	{!! Form::label('interest', translate('form.influence'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
	                	<div class="col-md-5 pt10" id="interest">
	                		<span class="rating block mn pull-left">
			                    <input class="rating-input" id="r5" type="radio" name="interests" value="4" v-on:click="setRating('4')" @if(!empty($details->interest) and $details->interest == 4) checked="" @endif>
			                    <label class="rating-star text-system" for="r5">
		                      		<i class="fa fa-star va-m"></i>
			                    </label>
			                    <input class="rating-input" id="r6" type="radio" name="interests" value="3" v-on:click="setRating('3')" @if(!empty($details->interest) and $details->interest == 3) checked="" @endif>
			                    <label class="rating-star text-system" for="r6">
		                      		<i class="fa fa-star va-m"></i>
			                    </label>
			                    <input class="rating-input" id="r7" type="radio" name="interests" value="2" v-on:click="setRating('2')" @if(!empty($details->interest) and $details->interest == 2) checked="" @endif>
			                    <label class="rating-star text-system" for="r7">
		                      		<i class="fa fa-star va-m"></i>
			                    </label>
			                    <input class="rating-input text-system" id="r8" type="radio" name="interests" value="1" v-on:click="setRating('1')" @unless(!empty($details->interest) and $details->interest != 1) checked="" @endunless>
			                    <label class="rating-star" for="r8">
		                      		<i class="fa fa-star va-m"></i>
			                    </label>
			                </span>
			                <span class="ml15 lh20 h-20 fs14 pt5 label label-success label-sm" v-html="rating">
			                	
			                </span>
	                	</div>
	                </div>
                </div>
                
                <div class="row">
	                <div class="form-group">
	                    {!! Form::label('actionyesno', translate('form.action'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
	                    <div class="col-md-6">
	                        <div class="section">
								<div class="radio-custom radio-system pt10">
								  	<input type="radio" id="is-action-yes" name="action_yes_no" value="Yes">
								  	<label for="is-action-yes">{{translate('form.yes')}}</label>

								  	<input type="radio" id="is-action-no" name="action_yes_no" value="No" checked="">
								  	<label for="is-action-no">{{translate('form.no')}}</label>
								</div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row actionDiv hide">
	            	<div class="form-group">
	            		{!! Form::label('action', translate('table.action'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
	            		<div class="col-md-5">
	                        <div class="section">
								{!! Form::text('action', '', ['class' => 'form-control action-field', 'id' => 'action', 'placeholder' => translate('form.define_action'), 'tabindex' => '5']) !!}
	                        </div>
	                    </div>
	            	</div>
	            </div>

	            <div class="row actionDiv hide">
	            	<div class="form-group">
	            		{!! Form::label('actiondelegateto', translate('table.delegate_to'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
	            		<div class="col-md-5">
	                        <div class="section">
								<label class="field select">
	                                {!! Form::select('who', $users, '', ['class' => 'action-field select2-single', 'tabindex' => '5']) !!}
	                            </label>
	                        </div>
	                    </div>
	            	</div>
	            </div>

	            <div class="row actionDiv hide">
	            	<div class="form-group">
	            		{!! Form::label('actiondelegateto', translate('form.deadline'), ['class' => 'col-md-3 col-md-offset-2 control-label']) !!}
	            		<div class="col-md-5">
	                        <div class="section">
								{!! Form::text('deadline', '', ['class' => 'form-control action-field pointer', 'id' => 'datetimepicker2', 'placeholder' => translate('form.define_deadline'), 'tabindex' => '6', 'readonly' => 'readonly']) !!}
	                        </div>
	                    </div>
	            	</div>
	            </div>
	            @if(!empty($stakeholderActionLog) && ($stakeholderActionLog->count() > 0 ))      
	            <div class="row">
	                <div class="col-md-8 col-md-offset-2">
	                <div class="section-divider" id="spy2">
	                    <span>{{translate('words.old_actions')}}</span>
	                </div>
	                <table class="table table-striped table-hover table-bordered mb20" cellspacing="0" width="100%">
	                    <thead>
	                        <tr>
	                            <th>{{translate('form.define_action')}}</th>
	                            <th>{{translate('table.delegate_to')}}</th>
	                            <th>{{translate('form.deadline')}}</th>
	                            <th>{{translate('table.status')}}</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        @foreach($stakeholderActionLog as $eachLog)
	                        <tr>
	                            <td>{{$eachLog->action}}</td>
	                            <td>{{$eachLog->deligate_to}}</td>
	                            <td>{{$eachLog->deadline}}</td>
	                            <td>{{($eachLog->closed == 0) ? translate('table.pending') : translate('table.closed')}}</td>
	                        </tr>
	                        @endforeach
	                    </tbody>
	                </table>
	                </div>
	            </div>
	            @endif
	            <div class="row">
	                <div class="col-md-4 col-md-offset-2 text-center">
	                    <br clear="all">
	                    {!! csrf_field() !!}
	                    {!! Form::hidden('hid', isset($details->id) ? $details->id : '') !!}
	                    {!! Html::link(url('/stakeholder-analysis/list'), translate('form.cancel'), array('class' => 'btn btn-system text-default', 'tabindex' => '8')) !!}
	                    {!! Form::submit(translate('form.save_button'), array('class' => 'btn btn-hover btn-system', 'id' => 'save-btn', 'tabindex' => '7')) !!} 
	                </div>
	            </div>
	            {!! Form::close() !!} 
	        </div>
	    </div>
	</div>
	@include('includes.scripts.stakeholder_script')
@stop