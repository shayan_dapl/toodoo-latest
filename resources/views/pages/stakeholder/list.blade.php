@extends('layouts.default')
	@section('content')
	<div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-visible" id="spy2">
	            <div class="panel-heading exhight">
	                <div class="panel-title hidden-xs pull-right">
	                	@if(Auth::guard('customer')->user()->type == 2)
	                    	{!! Html::link(url('/stakeholder-analysis/form'), translate('words.add'), array('class' => 'btn btn-system text-default')) !!}
	                    @endif
	                </div>
	            </div>
	            <div class="panel-body">
	                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
	                    <thead>
	                        <tr>
	                            <th>{{translate('form.internal_external')}}</th>
	                            <th>{{translate('form.category')}}</th>
	                            <th>{{translate('form.stakeholder_name')}}</th>
	                            <th>{{translate('form.needs_expectations')}}</th>
	                            <th>{{translate('form.detail')}}</th>
	                            <th>{{translate('form.impacts')}}</th>
	                            <th>{{translate('form.influence')}}</th>
	                            <th>{{translate('table.operations')}}</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        @foreach($data as $each)
	                        <tr>
	                            <td>{{ $each->type }}</td>
	                            <td>{{ !empty($each->category) ? $each->category->name : '' }}</td>
	                            <td>{{ $each->stakeholder_name }}</td>
	                            <td>{{ $each->needs }}</td>
	                            <td>{{ $each->detail }}</td>
	                            <td>
		                            <span class="disp-flex">
		                            @for($i=1; $i<=$each->impact; $i++) 
		                            	<i class="fa fa-star text-system"></i> 
		                            @endfor
		                            </span>
	                            </td>
	                            <td>
		                            <span class="disp-flex">
			                            @for($i=1; $i<=$each->interest; $i++) 
			                            	<i class="fa fa-star text-system"></i> 
			                            @endfor
			                        </span>
	                            </td>
	                            <td>
	                            	<div class="disp-flex">
		                                <a class="btn btn-default mr5" href="{{ url('/stakeholder-analysis/edit/'.$each->id) }}">
		                                    <span class="text-success fa fa-pencil"></span> 
		                                </a>
	                                    <a class="btn  btn-default" href="{{ url('/stakeholder-analysis/remove/'.$each->id) }}" onclick="return confirm('{{translate('alert.are_you_sure_all_related_actions_will_also_be_deleted')}}')">
	                                        <span class="text-danger fa fa-trash"></span> 
	                                    </a>
	                            	</div>
	                            </td>
	                        </tr>
	                        @endforeach
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
@stop