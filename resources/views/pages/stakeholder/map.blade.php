@extends('layouts.default')
	@section('content')
	<div class="panel">
		<div class="panel-heading exhight">
		</div>
		<div class="form-group mt25 disp-flex">
        {!! Form::select('branch', $branches, isset($selectedBranch) ? $selectedBranch : 'all', ['class' => 'form-control w200 ml5 issuecategory', 'id' => 'branch-opt']) !!}
	    </div>
	    <div id="loaderDv" style="display:none;"><i class="fa fa-cog fa-spin text-default fa-5x"></i></div>
		
		<div class="panel-body bg-light dark">
			<div id="scatter-load"></div>
		</div>
	</div>
	<div class="fixed tooltip tooltip-extended"></div>
	<div id="action-log-details" class="modal fade" role="dialog">
		<div class="modal-dialog modal-dialog-extended">
			<div class="modal-content">
				<div class="modal-header text-default back4d">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">{{translate('words.event_logging')}}</h4>
				</div>
				<div class="modal-body action-log-body row justified w-wrap-extended">
					<p>.....</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{translate('started.close')}}</button>
				</div>
			</div>
		</div>
	</div>
	@include('includes.scripts.stakeholder_script')
	<script type="text/javascript">
		$(document).ready(function() {
		    $(document).on('change', '#branch-opt', function() {
		        $('#loaderDv').show();
		        var branchId = $(this).val();
		        if (branchId != "") {
		            $.ajax({
		                url: "{{url('/stakeholder-analysis/filter-map')}}/" + branchId,
		                type: 'GET',
		                success: function (data) {
		                    $('#scatter-load').html(data);
		                    $('[data-toggle="tooltip"]').tooltip();
		                    setTimeout(function(){ $('#loaderDv').hide(); }, 500);
		                }
		            });
		        }
		    });
		})
	</script>
@stop