@extends('layouts.default')
@section('content')
@inject('file', 'App\Services\ProcessRiskActionLog')
<div class="row">
	<div class="col-md-6">
		{{translate('words.report_finding')}} : <span class="fw700 fs16">{{translate('words.supplier_corrective_action')}}</span><br clear="all">
		{{translate('form.deadline')}} : <span class="fw700 fs16">{{date(Config::get('settings.dashed_date'), strtotime($log->deadline))}}</span>
	</div>
	<div class="col-md-3 col-md-offset-3">
		<a href="{{url('/supplier/download-scar/'.$log->log_id)}}"><i class="fa fa-file-pdf-o text-system fa-3x"></i></a>
	</div>
</div>
<div class="row mt20">
	{!! Form::open(array('method' => 'post', 'url' => 'corrective-action-issue-fixing/'.$log->id, 'autocomplete' => 'off', 'enctype' => 'multipart/form-data', 'class' => 'comment-class')) !!}
		<div class="col-md-4">
			<label>{{translate('form.comment')}}</label>
			{!! Form::text('reply', '', ['class' => 'form-control', 'placeholder' => translate('form.comment'), 'required' => 'required']) !!}
		</div>
		<div class="col-md-3">
			<label>{{translate('form.upload_doc')}}</label>
			{!! Form::file('upload_doc[]', ['class' => 'pt5', 'multiple' => 'multiple']) !!}
		</div>
		<div class="col-md-2 text-center">
			<label>{{translate('form.closed')}}</label>
			<div class="checkbox-custom checkbox-system pt5">
				<input id="checkboxExample2" name="closed" type="checkbox" value="1">
				<label class="pl5" for="checkboxExample2">&nbsp;</label>
			</div>
		</div>
		<div class="col-md-3 pt25">
			{!! csrf_field() !!}
			{!! Form::hidden('action_log_id', $log->id) !!}
			{!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'name' => 'save']) !!}
		</div>
	{!! Form::close() !!}
</div>
<div class="panel mt20">
	<div class=" panel-heading">
		<span>{{translate('words.event_logging')}}</span>
	</div>
	<div class="panel-body">
		<table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>{{translate('words.comment')}}</th>
					<th>{{translate('form.documents')}}</th>
					<th>{{translate('form.created_at')}}</th>
				</tr>
			</thead>
			<tbody>
				@if(!empty($log->comments))
					@foreach($log->comments as $comment)
					<tr>
						<td>
							{{$comment->reply}}
						</td>
						<td>
							@if($comment->files->count() > 0)
								@foreach($comment->files as $eachFile)
								<a href="{{$file->file($eachFile->doc_name, 'supplier_corrective_action_docs')['url']}}" target="_blank">
									<i class="fa {{$file->file($eachFile->doc_name, 'supplier_corrective_action_docs')['icon']}} text-system dark"></i>
								</a>
								@endforeach
							@else
							--
							@endif
						</td>
						<td>{{date('d-m-Y',strtotime($comment->created_at))}}</td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>
@stop