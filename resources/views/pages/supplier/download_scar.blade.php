<style type="text/css">
	div {
		font-family: sans-serif
	}
</style>
<div class="row p10">
	<div style="width:25%; display:inline-block;">
		
	</div>
	<div style="width: 75%; display:inline-block; text-align: right; padding: 20px;">
		<img src="{{public_path('storage/company/'.$corrective->user->company_id.'/image/'.$corrective->user->logo)}}" height="100" class='img-responsive'>
	</div>
</div>
<div class="row">
	<div style="display:inline-block; text-align: center;">
		<h2>{{translate('words.supplier_corrective_doc_header')}}</h2>
	</div>
</div>
<div class="row">
	<div style="padding: 20px; display:inline-block;">
		<p style="font-size: 15px; margin: 0; line-height: 18px;">
			{{translate('words.date')}} : {{$action->created_at}}
		</p>
		<p style="font-size: 15px; margin: 0; line-height: 18px;">
			{{translate('words.supplier_name')}} : {{$action->supplier->name}}
		</p>
	</div>
</div>

<div class="row">
	<div style="padding: 20px; display:inline-block;">
		<p>{{translate('words.dear_supplier')}},</p>
		<p>{{translate('words.supplier_body')}}</p>
	</div>
</div>

<div class="row">
	<div style="padding-left: 20px;">
	@foreach($aboveThreshold as $param => $rate)
		<p><strong>{{$param}} : </strong> {{$rate}} </p>
	@endforeach
	</div>
</div>

<div class="row">
	<p style="padding-left: 20px;">{{translate('words.supplier_below_threshold_msg')}}</p>
	<div style="padding-left: 20px;">
	@foreach($belowThreshold as $param => $rate)
		<p><strong>{{$param}} : </strong> {{$rate}} </p>
	@endforeach
	</div>
</div>

<div class="row">
	<p style="padding-left: 20px;">{{translate('words.supplier_threshold_footer')}} <strong>{{$action->deadline}}</strong></p>
</div>

<div style="margin-top: 0px; display: block;">
	<div style="display: block; padding-bottom: 0px;">
		<p style="padding-left: 20px; font-size: 15px; margin: 0; line-height: 18px;">
			{{translate('words.best_regards')}}
		</p>
		<p style="padding-left: 20px; font-size: 15px; margin: 0; line-height: 18px;">
			{{$corrective->user->name}}
		</p>
		<p style="padding-left: 20px; font-size: 15px; margin: 0; line-height: 18px;">
			{{$corrective->user->positions()->pluck('position.name')->implode(", ")}}
		</p>
	</div>
</div>