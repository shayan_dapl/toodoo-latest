@extends('layouts.default')
	@section('content')
	<div id="supplier" v-cloak>
	    <div class="row">
	        <div class="col-md-9">
	            <div class="panel panel-visible" id="spy2">
	                <div class="panel-heading exhight">
	                    {{translate('words.list')}}
	                </div>
	                <div class="panel-body">
	                    <list-table :list="list" />
	                </div>
	            </div>
	        </div>
	        <div class="col-md-3">
	            <div class="panel">
	                <div class="panel-heading exhight">{{translate('words.add')}} / {{translate('words.edit')}}</div>
	                <div class="panel-body">
	                    <div class="admin-form">
	                        <div class="row mb10">
	                            <div class="col-md-12">
	                                {!! Form::label('name', translate('form.supplier_name'), ['class' => 'field-label fs15 mb5 required']) !!}
	                                <label class="field append-icon">
	                                    <input type="text" v-model="name" :class="nameClass" tabindex="1" />
	                                </label>
	                                <p class="text-danger mt5">@{{nameError}}</p>
	                            </div> 
	                        </div>
	                        <div class="row mb10">
	                            <div class="col-md-12">
	                                {!! Form::label('name', translate('form.product_services'), ['class' => 'field-label fs15 mb5 required']) !!}
	                                <label class="field append-icon">
	                                    <input type="text" v-model="service" :class="serviceClass" tabindex="2" />
	                                </label>
	                            </div> 
	                        </div>
	                        <div class="row mb10">
	                            <div class="col-md-12">
	                                {!! Form::label('name', translate('form.comment'), ['class' => 'field-label fs15 mb5']) !!}
	                                <label class="field append-icon">
	                                    <textarea v-model="comment" class="form-control" tabindex="3"></textarea>
	                                </label>
	                            </div> 
	                        </div>
	                        <div class="row">
	                            <div class="col-md-12 text-right">
	                                <input type="hidden" v-model="hid" />
	                                {!! Form::button(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', '@click' => 'saveData', ':disabled' => 'enabled', 'tabindex' => '4' ]) !!}
	                                <br clear="all">
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	@include('includes.scripts.supplier.supplier_script')
@stop