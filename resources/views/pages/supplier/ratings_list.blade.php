@extends('layouts.default')
	@section('content')
	<div id="supplier" v-cloak>
	    <div class="row">
	        <div class="col-md-7">
	            <div class="panel panel-visible" id="spy2">
	                <div class="panel-heading exhight">
	                    {{translate('words.list')}}
	                </div>
	                <div class="panel-body">
	                    <list-table :list="list" />
	                </div>
	            </div>
	        </div>
	        <div class="col-md-5">
	            <div class="panel">
	                <div class="panel-heading exhight">{{translate('words.add')}} / {{translate('words.edit')}}</div>
	                <div class="panel-body">
	                    <div class="admin-form">
	                        <div class="row mb10">
	                            <div class="col-md-12">
	                                {!! Form::label('name', translate('words.ratings'), ['class' => 'field-label fs15 mb5 required']) !!}
	                                <label class="field append-icon">
	                                    <input type="number" v-model="name" :class="nameClass" min="1" max="9" @keyUp="limitText" tabindex="1" />
	                                </label>
	                                <p class="text-danger mt5">@{{nameError}}</p>
	                            </div> 
	                        </div>
	                        <div class="row mb10">
	                            <div class="col-md-12">
	                                {!! Form::label('name', translate('form.description'), ['class' => 'field-label fs15 mb5 required']) !!}
	                                <label class="field append-icon">
	                                    <textarea v-model="description" :class="descriptionClass" tabindex="2"></textarea>
	                                </label>
	                            </div> 
	                        </div>
	                        <div class="row">
	                            <div class="col-md-12 text-right">
	                                <input type="hidden" v-model="hid" />
	                                {!! Form::button(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', '@click' => 'saveData', ':disabled' => 'enabled', 'tabindex' => '3' ]) !!}
	                                <br clear="all">
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	@include('includes.scripts.supplier.supplier_ratings_script')
@stop