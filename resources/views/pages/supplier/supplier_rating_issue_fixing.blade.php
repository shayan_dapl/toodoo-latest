@extends('layouts.default')
@section('content')
@inject('file', 'App\Services\ProcessRiskActionLog')
<div class="row">
	<div class="col-md-6">
		{{translate('words.report_finding')}} : <span class="fw700 fs16">{{translate('words.supplier_rating')}}</span><br clear="all">
		{{translate('form.action')}} : <span class="fw700 fs16">{{ucfirst($log->action)}}</span><br clear="all">
		{{translate('form.deadline')}} : <span class="fw700 fs16">{{date(Config::get('settings.dashed_date'), strtotime($log->deadline))}}</span><br clear="all">
	</div>
	<div class="col-md-6">
		<div class="row">
			{!! Form::open(array('method' => 'post', 'url' => 'supplier-rating-issue-fixing/'.$log->id, 'autocomplete' => 'off', 'enctype' => 'multipart/form-data', 'class' => 'comment-class')) !!}
			<div class="row pl10 mt5">
				<div class="col-md-6">
					<label>
						{{translate('form.comment')}}
					</label>
					<br clear="all">
					{!! Form::text('reply', '', ['class' => 'form-control', 'placeholder' => translate('form.comment'), 'required' => 'required']) !!}
				</div>
			</div>
			<div class="row pl10 mt5">
				<div class="col-md-6">
					<label>
						{{translate('form.upload_doc')}}
					</label>
					<br clear="all">
					{!! Form::file('upload_doc[]', ['multiple' => 'multiple']) !!}
				</div>
				<div class="col-md-2">
					<label>
						{{translate('form.closed')}}
					</label>
					<div class="checkbox-custom checkbox-system mt5 mb5 text-center">
						<input id="checkboxExample2" name="closed" type="checkbox" value="1">
						<label class="pl5" for="checkboxExample2">&nbsp;</label>
					</div>
				</div>
			</div>
			<div class="row pl10 mt5">
				<div class="col-md-6">
					{!! csrf_field() !!}
					{!! Form::hidden('action_log_id', $log->id) !!}
					{!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-hover btn-system', 'name' => 'save']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div class="admin-form mt20">
	<div class="panel">
		<div class="section-divider" id="spy2">
			<span>{{translate('words.event_logging')}}</span>
		</div>
		<div class="row mt20">
			<div class="col-md-12">
				<div class="panel">
					<div class="panel-body">
						<table class="table table-striped table-hover" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>{{translate('words.comment')}}</th>
									<th>{{translate('form.documents')}}</th>
									<th>{{translate('form.created_at')}}</th>
								</tr>
							</thead>
							<tbody>
								@if(!empty($log->comments))
									@foreach($log->comments as $comment)
									<tr>
										<td>
											{{$comment->reply}}
										</td>
										<td>
											@if($comment->files->count() > 0)
												@foreach($comment->files as $eachFile)
												<a href="{{$file->file($eachFile->doc_name, 'supplier_rating_action_docs')['url']}}" target="_blank">
													<i class="fa {{$file->file($eachFile->doc_name, 'supplier_rating_action_docs')['icon']}} text-system dark"></i>
												</a>
												@endforeach
											@else
											--
											@endif
										</td>
										<td>{{date('d-m-Y',strtotime($comment->created_at))}}</td>
									</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop