@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        @if($positionLog->count() > 0)
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs"> 
                    {{translate('words.position_log')}} : {{$positionLog->first()->user->name}}
                </div>
            </div>
            <div class="panel-body">
                <section id="content" class="animated fadeIn">
                    <div id="timeline" class="timeline-single mt30">
                        <div class="timeline-divider mtn">
                            <div class="divider-label bg-primary text-default">{{date('Y', strtotime($positionLog->first()->created_at))}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 left-column">
                                <div class="timeline-item">
                                    <div class="timeline-icon">
                                        <span class="fa fa-user-plus text-success"></span>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading pl10">
                                            <span class="panel-title">
                                                {{ucfirst(strtolower(translate('table.joined_on')))}}
                                            </span>
                                            <div class="panel-header-menu pull-right mr10 fs14"> {{$positionLog->first()->user->created_at}}</div>
                                        </div>
                                    </div>
                                </div>
                                @foreach($positionLog as $eachpos)
                                <div class="timeline-item">
                                    <div class="timeline-icon">
                                        <span class="glyphicon glyphicon-user text-system"></span>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading pl10">
                                            <span class="panel-title">
                                                {{$eachpos->position->name}}
                                            </span>
                                            <div class="panel-header-menu pull-right mr10 fs14">{{$eachpos->created_at}}</div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @if (!empty($positionLog->first()->user->out_of_service))
                                    <div class="timeline-item">
                                        <div class="timeline-icon">
                                            <span class="fa fa-user-times text-danger"></span>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading pl10">
                                                <span class="panel-title">
                                                    {{ucfirst(strtolower(translate('form.out_of_service')))}}
                                                </span>
                                                <div class="panel-header-menu pull-right mr10 fs14"> {{$positionLog->first()->user->out()}}</div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="timeline-divider">
                            <div class="divider-label {{($positionLog->first()->user->out_of_service != null) ? 'bg-danger text-default' : ''}}">{{date('Y', strtotime($positionLog->last()->created_at))}}</div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        @else
        <div class="panel">
            <div class="panel-heading">
                &nbsp;
            </div>
            <div class="panel-body text-center fs30">
                {{translate('table.no_data')}}
            </div>
        </div>
        @endif
    </div>
</div>
@stop