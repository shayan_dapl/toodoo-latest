@extends('layouts.default')
@section('content')
<div class="admin-form" id="user-form" v-cloak>
	<div class="panel">
		<div class="panel-body bg-light">
			{!! Form::open(['method' => 'post', 'url' => '/user/form', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) !!}
				<div class="row">
				    <!-- Out of service -->
					<div class="col-md-6 text-danger fs16 pt10">
                    	@if(!empty($details->out_of_service))
							{{translate('form.out_of_service')}} ({{$details->out_of_service}})
						@endif
                	</div>
					<div class="col-md-6 text-right">
						@if(isset($details))
							@if(empty($details->out_of_service))
								{!! Form::button(translate('form.out_of_service'), ['class' => 'btn btn-danger dark', 'id' => 'outofservice-date']) !!}
								{!! Form::hidden('out_of_service', (!empty($details->out_of_service) ? $details->out_of_service : ''), ['class' => 'date'] ) !!}
							@endif
						@endif
						@if(!empty($details->invite_user_token) and (isset($details) and empty($details->out_of_service)))
	                    <a href="javascript:void(0)" class="btn btn-hover btn-system {{isset($noEmail) ? $noEmail : ''}}" id='resent_mail' data-id='{{$details->id}}'>{{translate('form.resent_invitation_mail')}}</a>
	                    @endif
	                    <a class="btn btn-system" href="{{url('/user/list')}}">
							{{translate('form.back')}}
						</a>
					</div>
				</div>

				<div class="section-divider mb20 mt30" id="spy1">
					<span>{{translate('form.personal_details')}}</span>
				</div>

				<div class="row">
					<div class="col-md-4 col-md-offset-2 mt40">
						<div class="section">
							<span id="no-email-span" class="">
								{!! Form::label('fname', translate('form.no_email'), array('class' => 'field-label fs15 mb5')) !!}
								<label class="switch-extended switch-success">
									{!! Form::checkbox('no_email', 1, (isset($details->no_email) and $details->no_email==1), ['id' => 'no_email']) !!}
									<div class="slider-extended round"></div>
								</label>
							</span>
						</div>
					</div>
					
					<div class="col-md-4">
					</div>
				</div>

				<div class="row">
					<div id="email-div" class="col-md-4 col-md-offset-2 pt20">
						<div class="section {{isset($noEmail) ? $noEmail : ''}}" id="user-email">
							{!! Form::label('email', translate('form.email'), array('class' => 'field-label fs15 mb5 '.(isset($email) ? $email : 'required'), 'id' => 'email-label')) !!}

							<label class="field prepend-icon">
								{!! Form::email('email', (isset($details->email)) ? $details->email : old('email'), ['class' => 'gui-input', 'id' => 'email', 'placeholder' => translate('form.email'), (isset($email) ? $email : 'required'), 'tabindex' => '1', 'v-model' => 'email', 'v-on:blur' => 'getImage']) !!}

								<label for="email" class="field-icon">
									<i class="fa fa-envelope"></i>
								</label>
							</label>
							<p class="text-danger mn">{{$errors->first('email')}}</p>
						</div>
					</div>
					<div id="image-div" class="{{(isset($noEmail) and $noEmail == 'hide') ? 'col-md-8 col-md-offset-2' : 'col-md-2'}}">
						<label for="user_photo" class="pull-left userPhoto">
                            <i class="fa fa-cloud-upload pos-relative user-img-icon fs20 pointer"></i> 
                            {{ Html::image('', '', [':src' => 'imgsrc', 'class' => 'thumbnail pointer', 'id' => 'user_preview', 'height' => '100', 'width' => '100']) }}
                        </label>
                        <input type="file" id="user_photo" class="hide" accept=".jpeg,.jpg,.png" v-on:change="onImageChange" />
                        <input type="hidden" name="photo" :value="image" />
					</div>
					<div class="col-md-2 pt20">
                        <div v-if="isityou">
                            <p class="text-danger">{{translate('alert.is_it_you')}}</p>
                            <button type="button" class="btn btn-success" @click="confirmIt('yes')">{{translate('form.yes')}}</button>
                            <button type="button" class="btn btn-danger" @click="confirmIt('no')">{{translate('form.no')}}</button>
                        </div>
                    </div>
				</div>

				<div class="row">
					<div class="col-md-4 col-md-offset-2">
						<div class="section">
							{!! Form::label('fname', translate('form.first_name'), array('class' => 'field-label fs15 mb5 required')) !!}
							<label class="field prepend-icon">
								{!! Form::text('fname', (isset($details->fname)) ? $details->fname : old('fname'), array('class' => 'gui-input', 'id' => 'fname', 'placeholder' => translate('form.first_name'), 'required' => 'required', 'tabindex' => '2')) !!}
								<label for="fname " class="field-icon">
									<i class="fa fa-user"></i>
								</label>
							</label>
							@if (count($errors) > 0)
								<p class="text-danger mn">{{$errors->first('fname')}}</p>
							@endif
						</div>
					</div>
					<div class="col-md-4">
						<div class="section">
							{!! Form::label('lname', translate('form.last_name'), array('class' => 'field-label fs15 mb5 required')) !!}
							<label class="field append-icon">
								{!! Form::text('lname', (isset($details->lname)) ? $details->lname : old('lname'), array('class' => 'gui-input', 'id' => 'lname', 'placeholder' => translate('form.last_name'), 'required' => 'required', 'tabindex' => '3')) !!}
								<label for="lname" class="field-icon">
									<i class="fa fa-user"></i>
								</label>
							</label>
							@if (count($errors) > 0)
								<p class="text-danger mn">{{$errors->first('lname')}}</p>
							@endif
						</div>
					</div>
				</div>

				<div class="row">
					<div id="language-div" class="{{(isset($noEmail) and $noEmail == 'hide') ? 'col-md-8' : 'col-md-4'}} col-md-offset-2">
						<div class="section">
							<label for="language" id="language-label" class="field-label fs15 mb5 {{isset($email) ? $email : 'required'}}">
								{{translate('form.language')}}
							</label>
							<label class="field select">
								{!! Form::select('language', $language, isset($details->language) ? $details->language : Auth::user()->language, ['id' => 'language', 'tabindex' => '6']) !!}
								<i class="arrow"></i>
							</label>
							@if (count($errors) > 0)
								<p class="text-danger mn">{{$errors->first('language')}}</p>
							@endif
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4 col-md-offset-2 min-hght">
						<span id="can-process-owner" class="{{isset($noEmail) ? $noEmail : ''}}">
							<label class="switch-extended switch-success">
								{!! Form::checkbox('can_process_owner', 1, (isset($details->can_process_owner) and $details->can_process_owner==1),['id' => 'can_process_owner']) !!}
								<div class="slider-extended round"></div>
							</label>
							<span class="lbl lbl-extended">
								{{translate('form.process_owner')}}
								<button class="btn btn-xs btn-system information" data-toggle="tooltip" title="{{translate('started.who_is_processowner')}}" data-placement="right"><i class="fa fa-question-circle-o"></i></button>
							</span>
						</span>
					</div>
					<div class="col-md-4">
						<span id="is-auditor" class="{{isset($noEmail) ? $noEmail : ''}}">
							<label class="switch-extended switch-success">
								{!! Form::checkbox('is_auditor', 1, (isset($details->is_auditor) and $details->is_auditor==1),['id' => 'is_auditor']) !!}
								<div class="slider-extended round"></div>
							</label>
							<span class="lbl switch-custom-pos">
								{{translate('form.is_auditor')}}
								<button class="btn btn-xs btn-system information" data-toggle="tooltip" title="{{translate('started.who_is_auditor')}}" data-placement="right"><i class="fa fa-question-circle-o"></i></button>
							</span> 
						</span>
					</div>
				</div>
				
				<div class="section-divider" id="spy2">
					<span>{{translate('form.position_function_details')}}</span>
				</div>

				<div class="row for-function" class="" id="pos-and-func-dtl">
					<div class="col-md-4 col-md-offset-2 mb20">
						<label class="switch-extended switch-success">
							{!! Form::checkbox('is_special_user', 1, (isset($details) and $details->is_special_user==1), ['id' => 'make_special_user']) !!}
							<div class="slider-extended round"></div>
						</label>
						<span class="lbl switch-custom-pos">{{translate('form.special_user')}}</span>
					</div>
					
					@if(!isset($details))
						<div class="col-md-6" id="invite-div">
							<label class="switch-extended switch-success">
								{!! Form::checkbox('invite_user', 1, '',['checked', 'id' => 'invite_user']) !!}
								<div class="slider-extended round"></div>
							</label>
							<span class="lbl switch-custom-pos">{{translate('form.invite_user')}}</span>
						</div>
					@endif
				</div>

				@if($orgStructExistance == 0)
					<h1 class="text-center">{{translate('form.org_struct_exists')}}</h1>
				@else
					<span id="position-list" class="{{(isset($details) and $details->is_special_user == 1) ? 'hide' : ''}}">
						@if(isset($details) and $details->positions()->count() > 0)
							@foreach($details->positions() as $eachFn)
								@if (!empty($eachFn->position))
								<div class="row repeter">
									<div class="col-sm-5">
										<h3>{{$eachFn->position->name_with_details}}</h3>
									</div>
									<div class="col-sm-6 col-xs-10">
										<h3>
											@if (!empty($eachFn->parentName($eachFn->position->parent_id)))
												{{$eachFn->parentName($eachFn->position->parent_id)->user->name}} 
												({{$eachFn->parentName($eachFn->position->parent_id)->position->name}})
											@else
												{{translate('form.report_none')}}
											@endif
										</h3>
									</div>
									<div class="col-sm-1 col-xs-2">
										<button type="button" class="btn btn-danger btn-sm functions-del" data-id="{{$eachFn->id}}">
											<i class="fa fa-minus"></i>
										</button>
									</div>
								</div>
								@endif
							@endforeach
						@endif

						<div class="row repeter" id="output-1">
							<div class="col-md-5 col-sm-5 col-xs-12">
								<label class="field select">
									{!! Form::select('functions[position][]', $positions, '', ['id' => 'position-1', 'data-id' => '1', 'class' => 'position']) !!}
									<i class="arrow"></i>
								</label>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-10">
								<h3 id="holdername-1">{{translate('form.parent_position')}}</h3>
								{!! Form::hidden('functions[parent][]', '0', ['id' => 'parentpos-1']) !!}
							</div>
							<div class="col-md-1 col-sm-1 col-xs-2">
								<button type="button" class="btn btn-system functions" data-row="1" data-id="">
									<i class="fa fa-plus"></i>
								</button>
							</div>
						</div>
					</span>

					<div class="row {{(isset($details) and $details->is_special_user == 1 and $details->positions()->count() == 0) ? '' : 'hide'}}" id="special-position-list">
						<div class="col-md-5 col-sm-5 col-xs-12">
							<label class="field select position-span-0">
								<input type="hidden" name="functions[special][existing]" value="{{!empty($special) ? $special->id : '0'}}">
								{!! Form::select('functions[special][id]', $specialPositions, !empty($special) ? $special->position->id : '', ['class' => 'position', 'id' => 'position-0', 'data-id' => '0']) !!}
								<i class="arrow"></i>
							</label>
						</div>
						<span class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" name="functions[special][parent]" id="parentpos-0" value="0">
							<h3 id="holdername-0">@if(!empty($special)) {{($special->position->parent->structure->count() > 0) ? $special->position->parent->structure[0]->user->name : ''}} ({{$special->position->parent->name}}) @else {{translate('form.parent_position')}} @endif</h3>
						</span>
					</div>
				@endif
				<div class="section-divider mb20 mt30" id="spy2"></div>
				<div class="row">
					<div class="col-md-offset-4 col-md-4 text-center">
						{!! csrf_field() !!}
						{!! Html::link(url('/user/list'), translate('form.cancel'), ['class' => 'btn btn-system']) !!}
						{!! Form::submit(translate('form.save_button'), ['class' => 'btn btn-system']) !!}
						{!! Form::hidden('hid', isset($details) ? $details->id : '', ['id' => 'hid']) !!}
						{!! Form::hidden('usertype', isset($details) ? $details->type : 3) !!}
						{!! Form::hidden('compid', isset($details) ? $details->company_id : '') !!}
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div id="service-position-handover" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-extended">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body justified">
                {!! Form::open(['method' => 'post', 'url' => '/user/position/exchange', 'id' => 'service-handover-form']) !!}
                <div class="row mb20">
                    <div class="col-md-6 pt30">
                        <span class="lbl pos-relative top17">{{translate('form.audit_handover')}}</span>
                        <label class="switch-extended switch-success">
                            <input type="checkbox" name="audit_handover" value="1">
                            <div class="slider-extended round"></div>
                        </label>
                    </div>
                    <div class="col-md-6 admin-form" id="out-of-service-date-panel">
                        <p>{{translate('form.out_of_service')}}</p>
	                    <div class="section">    
	                        <label class="field append-icon">
		                        {!! Form::text('out_service_date', (!empty($getUser->out_of_service) ? $getUser->out_of_service : ''), ['class' => 'form-control pointer', 'id' => 'service-handover-date', 'autocomplete' => 'off', 'required' => 'required'] ) !!}
		                        <label for="service-handover-date" class="field-icon">
									<i class="fa fa-calendar"></i>
								</label>
							</label>
						</div>
                    </div>
                </div>
                @if(isset($details))
                    {!! Form::hidden('current_user', $details->id) !!}
                    @if($details->positions()->count() > 0)
                        <div class="text-grey fs16 fw600">
                            {{translate('words.position')}}
                        </div>
                        @foreach($details->positions() as $eachPosition)
                        @if(!empty($eachPosition->position))
                        <div class="row mt10">
                            <div class="col-md-6 text-grey pt10">
                                {{$loop->iteration}}. {{$eachPosition->position->name}}
                            </div>
                            <div class="col-md-6">
                                <select name="to[position][{{$eachPosition->id}}]" class="form-control handover-positions user-position" required="">
                                    <option value="">{{translate('form.select')}}</option>
                                    @if(!empty($nonAssociatedUsers))
                                        @foreach($nonAssociatedUsers as $eachNonAssociate)
                                        <option value="{{$eachNonAssociate->id}}">{{$eachNonAssociate->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    @endif
                    @if(!empty($details->specialPositions()))
                        <div class="text-grey fs16 fw600">
                            {{translate('words.position')}}
                        </div>
                        @if(!empty($details->specialPositions()->position))
                        <div class="row mt10">
                            <div class="col-md-6 text-grey pt10">
                                1. {{$details->specialPositions()->position->name}}
                            </div>
                            <div class="col-md-6">
                                <select name="to[position][{{$details->specialPositions()->id}}]" class="form-control handover-positions user-position" required="">
                                    <option value="">{{translate('form.select')}}</option>
                                    @if(!empty($nonAssociatedSpecialUsers))
                                        @foreach($nonAssociatedSpecialUsers as $eachNonAssociate)
                                        <option value="{{$eachNonAssociate->id}}">{{$eachNonAssociate->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        @endif
                    @endif
                    @if($details->processes()->count() > 0)
                        <div class="text-grey fs16 fw600 mt20">
                            {{translate('words.process')}}
                        </div>
                        @foreach($details->processes() as $eachProcess)
                        <div class="row mt10">
                            <div class="col-md-6 text-grey pt10">
                                {{$loop->iteration}}. {{$eachProcess->name}}
                            </div>
                            <div class="col-md-6">
                                <select name="to[process][{{$eachProcess->id}}]" class="form-control handover-positions user-process" required="">
                                    <option value="">{{translate('form.select')}}</option>
                                    @if(!empty($onlyProcessOwner))
                                        @foreach($onlyProcessOwner as $eachNonAssociate)
                                            <option value="{{$eachNonAssociate->id}}">
                                                {{$eachNonAssociate->name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        @endforeach
                    @endif
                    @if($details->plannedAudits()->count() > 0)
                        <div class="text-grey fs16 fw600 mt20">
                            {{translate('form.lead_auditor')}}
                        </div>
                        @foreach($details->plannedAudits() as $eachPlannedAudit)
                        <div class="row mt10">
                            <div class="col-md-6 text-grey pt10">
                                {{$loop->iteration}}. {{$eachPlannedAudit->audit_name}}
                            </div>
                            <div class="col-md-6">
                                <select name="to[plannedaudit][{{$eachPlannedAudit->id}}]" class="form-control handover-positions" required="">
                                    <option value="">{{translate('form.select')}}</option>
                                    @if(!empty($details->otherPOwners($eachPlannedAudit->id, $details->id)))
                                        @foreach($details->otherPOwners($eachPlannedAudit->id, $details->id) as $eachNonAssociate)
                                            <option value="{{$eachNonAssociate->id}}">
                                                {{$eachNonAssociate->name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        @endforeach
                    @endif
                    @if($details->auditMember()->count() > 0)
                        <div class="text-grey fs16 fw600 mt20">
                            {{translate('words.audit_team_member')}}
                        </div>
                        @foreach($details->auditMember as $eachMember)
                        <div class="row mt10">
                            <div class="col-md-6 text-grey pt10">
                                {{$loop->iteration}}. {{$eachMember->plan->audit_name}}
                            </div>
                            <div class="col-md-6">
                                <select name="to[auditmember][{{$eachMember->id}}]" class="form-control handover-positions" required="">
                                    <option value="">{{translate('form.select')}}</option>
                                   	@if(!empty($details->otherPOwners($eachMember->plan->id, $details->id)))
                                        @foreach($details->otherPOwners($eachMember->plan->id, $details->id) as $eachNonAssociate)
                                            <option value="{{$eachNonAssociate->id}}">
                                                {{$eachNonAssociate->name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        @endforeach
                    @endif
                    @if($details->auditees()->count() > 0)
                        <div class="text-grey fs16 fw600 mt20">
                            {{translate('words.auditee')}}
                        </div>
                        @foreach($details->auditees() as $eachAuditee)
                        <div class="row mt10">
                            <div class="col-md-6 text-grey pt10">
                                {{$loop->iteration}}. {{$eachAuditee->plan->audit_name}}
                            </div>
                            <div class="col-md-6">
                                <select name="to[auditee][{{$eachAuditee->id}}]" class="form-control handover-positions" required="">
                                    <option value="">{{translate('form.select')}}</option>
                                    @if(!empty($users))
                                        @foreach($users as $eachNonAssociate)
                                            @if($eachNonAssociate->is_auditor == 1)
                                                <option value="{{$eachNonAssociate->id}}">
                                                    {{$eachNonAssociate->name}}
                                                </option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        @endforeach
                    @endif
                    @if($details->externalIssues()->count() > 0)
                        <div class="text-grey fs16 fw600 mt20">
                            {{translate('words.report_finding')}}
                        </div>
                        @foreach($details->externalIssues() as $eachExternal)
                        <div class="row mt10">
                            <div class="col-md-6 text-grey pt10">
                                {{$loop->iteration}}. {{$eachExternal->issue}}
                            </div>
                            <div class="col-md-6">
                                <select name="to[external][{{$eachExternal->id}}]" class="form-control handover-positions" required="">
                                    <option value="">{{translate('form.select')}}</option>
                                    @if(!empty($users))
                                        @foreach($users as $eachNonAssociate)
                                            <option value="{{$eachNonAssociate->id}}">
                                                {{$eachNonAssociate->name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        @endforeach
                    @endif
                    @if($details->delegateMember()->count() > 0)
                        <div class="text-grey fs16 fw600 mt20">
                            {{translate('form.report_delegation')}}
                        </div>
                        @inject('issueType', 'App\Services\UserForm')
                        @foreach($details->delegateMember() as $eachDelegateMember)
                        <div class="row mt10">
                            <div class="col-md-6 text-grey pt10">
                                {{$loop->iteration}}. {{$eachDelegateMember->action->execution->audit->audit_name}} - {{$issueType->issueType($eachDelegateMember->issue_type)}}
                            </div>
                            <div class="col-md-6">
                                <select name="to[delegate][{{$eachDelegateMember->id}}]" class="form-control handover-positions" required="">
                                    <option value="">{{translate('form.select')}}</option>
                                    @if(!empty($users))
                                        @foreach($users as $eachNonAssociate)
                                            <option value="{{$eachNonAssociate->id}}">
                                                {{$eachNonAssociate->name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        @endforeach
                    @endif
                    @if($details->competentAction()->count() > 0)
                        <div class="text-grey fs16 fw600 mt20">
                            {{translate('words.competent_metrix')}}
                        </div>
                        @foreach($details->competentAction() as $eachCompetent)
                        <div class="row mt10">
                            <div class="col-md-6 text-grey pt10">
                                {{$loop->iteration}}. {{$eachCompetent->action}}
                            </div>
                            <div class="col-md-6">
                                <select name="to[competent][{{$eachCompetent->id}}]" class="form-control handover-positions" required="">
                                    <option value="">{{translate('form.select')}}</option>
                                    @if(!empty($users))
                                        @foreach($users as $eachNonAssociate)
                                            <option value="{{$eachNonAssociate->id}}">
                                                {{$eachNonAssociate->name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        @endforeach
                    @endif
                @endif
                <br clear="all">
                <button type="button" class="btn btn-system btn-block" id="service-position-handover-btn">{{translate('form.handover')}}</button>
                <br clear="all"><br clear="all">
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.user_form_script')
@stop