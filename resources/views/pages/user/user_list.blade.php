@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading exhight">
                <div class="panel-title hidden-xs pull-right">
                    <small>
                        [ <i class="fa fa-sitemap text-danger"></i> = {{translate('table.head_person')}} ]
                        [ <i class="fa fa-cog text-danger"></i> = {{translate('table.admin')}} ]
                        [ <i class="fa fa-strikethrough text-danger"></i> = {{translate('table.special_user')}} ]
                        [ <i class="fa fa-bullhorn text-danger"></i> = {{translate('words.auditor')}} ]
                        [ <i class="fa fa-cogs text-danger"></i> = {{translate('form.process_owner')}} ]
                    </small>
                    <a class="btn text-default btn-system" href="{{url('/user/form')}}">{{ucfirst(translate('words.add'))}}</a>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" id="datatable2" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{translate('table.photo')}}</th>
                            <th>{{translate('table.name')}}</th>
                            <th>{{translate('table.email')}}</th>
                            <th>{{translate('table.joined_on')}}</th>
                            <th>{{translate('table.status')}}</th>
                            <th>{{translate('table.operations')}}</th>
                            <th>{{strtoupper(translate('form.make_customer_admin'))}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('status', 'App\Services\UserList')
                        @foreach($users as $eachUser)               
                        <tr>
                            <td align="right">
                                <span class="sitemapicon">
                                    @if($eachUser->is_top_person == 1) 
                                    <div class="fa fa-sitemap text-danger"></div>
                                    @endif
                                    @if($eachUser->is_top_person == 1 and $eachUser->type == 2)
                                    @endif
                                    @if($eachUser->type == 2) 
                                    <div class="fa fa-cog text-danger"></div>
                                    @endif
                                    @if($eachUser->is_special_user == 1) 
                                    <div class="fa fa-strikethrough text-danger"></div>
                                    @endif
                                    @if($eachUser->is_auditor == 1) 
                                    <div class="fa fa-bullhorn text-danger"></div>
                                    @endif
                                    @if($eachUser->can_process_owner == 1) 
                                    <div class="fa fa-cogs text-danger"></div>
                                    @endif
                                </span>
                                @if(\File::isFile('storage/company/'.$eachUser->company->id.'/image/users/'.$eachUser->photo))
                                    <img src="{{asset('storage/company/'.$eachUser->company->id.'/image/users/'.$eachUser->photo)}}" height="50" />
                                @else
                                    <img src="{{asset('image/placeholder.png')}}" height="50" />
                                @endif
                            </td>
                            <td @if($eachUser->status == 3) class="text-danger" @endif>
                                 {{$eachUser->name}}
                            </td>
                            <td>{{$eachUser->email}}</td>
                            <td>{{ $eachUser->created_at }}</td>
                            <td>{{$status->status($eachUser->status)}}</td>
                            <td>
                                <a class="btn btn-default" href="{{ url('/user/edit/'.$eachUser->id) }}" title="{{translate('words.edit')}}">
                                    <i class="text-success fa fa-pencil"></i> 
                                </a>
                                <a class="btn btn-default" target="_blank" href="{{ url('/user/position-log/'.$eachUser->id) }}" title="{{translate('words.position_log')}}">
                                    <i class="text-success fa fa-tasks"></i> 
                                </a>
                            </td>
                            <td>
                                @if(!empty($eachUser->email) and $eachUser->is_special_user == 0)
                                <span id="user-list-switch">
                                    <label class="switch-extended switch-success">
                                        {!! Form::checkbox('is_customer_admin', 1, (isset($eachUser->type) and $eachUser->type==2), ['class' => 'make-cadmin', 'data-id' => $eachUser->id]) !!}
                                        <div class="slider-extended round"></div>
                                    </label>
                                    <span class="lbl switch-custom-pos fs10 text-danger"></span>
                                </span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.user_list_script')
@stop