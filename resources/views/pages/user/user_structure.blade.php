@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6 pt5 text-right">
                        [<img src="/image/bullhorn.png" height="15"> = {{translate('words.auditor')}}]
                        [<img src="/image/cog.png" height="15"> = {{translate('form.process_owner')}}]
                    </div>
                    <div class="col-md-2">
                        {!! Form::select('branch', $branches, $branch->id, ['class' => 'form-control', 'id' => 'branch']) !!}
                    </div>
                    <div class="col-md-2">
                        {!! Form::select('department', $departments, $department, ['class' => 'form-control', 'id' => 'department']) !!}
                    </div>
                    <div class="col-md-2 text-left pt5">
                        <a href="javascript:void(0);" class="btn btn-system" id="btnSave"><i class="fa fa-print"></i></a>
                        <a href="javascript:void(0);" class="btn btn-system" id="btnShowPic"><i class="fa fa-file-image-o"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="chart-container"></div>
                <div id="img-out"></div>
            </div>
        </div>
    </div>
</div>
@include('includes.scripts.user_structure_script')
@stop