<?php
/*
|--------------------------------------------------------------------------
| Customer Routes
|--------------------------------------------------------------------------
|
| Here is where you can register customer routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "customer" middleware group. Now create something great!
|
 */
Route::group(['prefix' => 'api'], function () {
    Route::any('/add-customer-admin', 'ApiController@addCustomerAdmin')->name('add-customer-admin');
    Route::any('/package-user-plans', 'ApiController@PackageUserPlans')->name('add-customer-admin');
    Route::any('/package-storage-plans', 'ApiController@PackageStoragePlans')->name('add-customer-admin');
    Route::post('/update-payment-status', 'Membership\TrialController@updateTrialPaymentStatus');
    Route::post('/update-addon-payment-status', 'Membership\AddonController@updateAddonPaymentStatus');
    Route::post('/subscription-webhook', 'Membership\TrialController@subscriptionWebhook');
});
Route::get('/doc_download/{doctype}/{docTitle}', 'Process\ProcedureController@downloadDoc')->name('change-status');
Route::get('/remove-trial-banner', 'SettingsController@removeTrialBanner')->name('remove-trial-banner');
Route::group(['middleware' => ['auth:web,customer', 'language', 'doubleCheck']], function () {
    Route::get('/entry-panel', 'Site\AccountController@entryPanel')->name('entry-panel');
    Route::get('/entry-panel/{company_id}', 'Site\AccountController@entryPanel')->name('entered');
    Route::get('/change-your-password', 'Site\IndexController@changeYourPassword')->name('change-your-password');
    Route::post('/change-your-password', 'Site\IndexController@changeYourPasswordSave')->name('change-your-password-save');
    Route::get('/profile', 'Site\ProfileController@profile')->name('profile-form')->middleware('permission');
    Route::post('/profile', 'Site\ProfileController@profileSave')->name('profile-save')->middleware('permission');
    Route::post('/customeradmin-swap', 'Company\AdminController@customerAdminExchange');
    Route::match(['get', 'post'], '/statuschange/{model}/{id}/{status}', 'Process\ProcessController@makeStatusChange')->name('change-status');
    Route::get('/resend_invitation_mail/{userId}', 'Site\AccountController@resendInvitationMail')->name('resend-mail');
    Route::get('/password', 'Company\AdminController@changePassword')->name('change-password-form')->middleware('permission');
    Route::post('/password', 'Company\AdminController@changePasswordSave')->name('change-password-save')->middleware('permission');
    Route::get('/make-customer-admin/{id}/{status}', 'Company\HomeController@makeCustomerAdmin')->name('make-customer-admin');
    Route::get('/context-stakeholder-fixing/{id}', 'Context\AnalysisController@issueFixing')->name('context-stakeholder-fixing');
    Route::post('/context-stakeholder-fixing/{id}', 'Context\AnalysisController@issueFixingSave')->name('context-stakeholder-fixing');
    Route::get('/policy', 'SettingsController@policy')->name('policy')->middleware('permission');
    
    Route::group(['prefix' => '/home', 'middleware' => 'permission'], function () {
        Route::get('/', 'Company\HomeController@home')->name('dashboard');
        Route::get('/planning', 'Home\PlanningController@index')->name('planning');
        Route::get('/planning/{year}', 'Home\PlanningController@index')->name('planning-with-year');
        Route::get('/issues', 'Home\IssuesController@index')->name('issues');
        Route::get('/meeting', 'Home\MeetingController@index')->name('meeting');
        Route::get('/process_improvement', 'Home\ProcessImprovementController@index')->name('process-improvement');
        Route::get('/audit_results', 'Home\AuditResultsController@index')->name('audit-results');
        Route::get('/company-view', 'Home\CompanyViewController@index')->name('company-view');
        Route::get('/chart/filter/{for}/{value}', 'Home\Cockpit\AjaxController@homeChartFilter')->name('home-chart-filter');
        Route::group(['prefix' => '/cockpit'], function () {
            Route::get('/unplanned_audit', 'Home\Cockpit\UnplannedAuditController@index')->name('cockpit-unplanned-audit');
            Route::get('/insight_process', 'Home\Cockpit\InsightProcessController@index')->name('cockpit-insight-process');
            Route::get('/upcoming_meeting', 'Home\Cockpit\UpcomingMeetingController@index')->name('cockpit-upcoming-meeting');
        });
    });

    Route::group(['prefix' => '/getstarted'], function () {
        Route::get('/started', 'GetStartedController@stepStarted')->name('get-started-step-one');
        Route::get('/branch', 'GetStartedController@stepBranch')->name('get-started-step-two');
        Route::get('/branches', 'GetStartedController@branches')->name('get-started-step-two');
        Route::get('/department', 'GetStartedController@stepDepartment')->name('get-started-step-three');
        Route::get('/department/copy', 'GetStartedController@stepDepartmentCopy')->name('get-started-step-three');
        Route::post('/department/copy', 'GetStartedController@stepDepartmentCopySave')->name('get-started-step-three');
        Route::get('/departments/{id}', 'GetStartedController@departments')->name('get-started-step-three');
        Route::get('/function', 'GetStartedController@stepFunction')->name('get-started-step-four');
        Route::get('/function/existing/{branch}', 'GetStartedController@stepFunction')->name('get-started-step-four');
        Route::post('/function/create', 'GetStartedController@stepFunctionCreate')->name('get-started-step-four');
        Route::post('/function/save', 'GetStartedController@stepFunctionSave')->name('get-started-step-four')->middleware('assistantPos');
        Route::get('/function/parent/{id}', 'GetStartedController@stepFunctionParent')->name('get-started-step-four');
        Route::get('/user', 'GetStartedController@stepUser')->name('get-started-step-five');
        Route::post('/getimage', 'GetStartedController@stepUserGetImage')->name('get-started-step-five');
        Route::post('/user/save', 'GetStartedController@stepUserSave')->name('get-started-step-five');
        Route::get('/process', 'GetStartedController@stepProcess')->name('get-started-step-six');
        Route::post('/process/create', 'GetStartedController@stepProcessCreate')->name('get-started-step-six');
        Route::get('/dismiss/{type}', 'GetStartedController@dismiss')->name('get-started-dismiss');
        Route::post('/reopen', 'GetStartedController@reopen')->name('get-started-dismiss');
    });
    
    Route::group(['prefix' => '/department', 'middleware' => 'permission'], function () {
        Route::get('/list', 'Company\DepartmentController@department')->name('department');
        Route::get('/data', 'Company\DepartmentController@departmentData')->name('department');
        Route::post('/', 'Company\DepartmentController@departmentSave')->name('department');
        Route::post('/remove', 'Company\DepartmentController@departmentRemove')->name('department');
        Route::get('/positions/{id}/{hidpos}', 'Company\PositionController@departmentPositions')->name('department');
    });

    Route::group(['prefix' => '/branch', 'middleware' => 'permission'], function () {
        Route::get('/list', 'Company\BranchController@branch')->name('branch');
        Route::get('/data', 'Company\BranchController@branchData')->name('branch');
        Route::get('/check-corporate', 'Company\BranchController@checkCorporate')->name('check-corporate');
        Route::post('/', 'Company\BranchController@branchSave')->name('branch');
        Route::post('/remove', 'Company\BranchController@branchRemove')->name('branch');
    });

    Route::group(['prefix' => '/rating', 'middleware' => 'permission'], function () {
        Route::get('/list', 'Company\RatingController@rating')->name('rating');
        Route::get('/data', 'Company\RatingController@ratingData')->name('rating');
        Route::post('/', 'Company\RatingController@ratingSave')->name('rating');
        Route::post('/remove', 'Company\RatingController@ratingRemove')->name('rating');
        Route::get('/action-type', 'Process\CompetentController@actionType')->name('rating');
        Route::get('/action-type/data', 'Process\CompetentController@actionTypeData')->name('rating');
        Route::post('/action-type/save', 'Process\CompetentController@actionTypeSave')->name('rating');
        Route::post('/action-type/remove', 'Process\CompetentController@actionTypeRemove')->name('rating');
    });

    Route::group(['prefix' => '/position', 'middleware' => 'permission'], function () {
        Route::get('/list', 'Company\PositionController@positionList')->name('position-list');
        Route::get('/form', 'Company\PositionController@positionForm')->name('position-form');
        Route::post('/form', 'Company\PositionController@positionFormSave')->name('position-save')->middleware('assistantPos');
        Route::post('/special/save', 'Company\PositionController@positionSpecialSave')->name('position-save');
        Route::get('/edit/{id}', 'Company\PositionController@positionEdit')->name('position-edit');
        Route::get('/remove/{id}', 'Company\PositionController@positionRemove')->name('position-remove');
        Route::get('/rearrange/{child}/{parent}', 'Company\PositionController@positionRearrange')->name('position-rearrange');
    });

    Route::get('/video/tutorial', 'VideoController@tutorial')->name('tutorial');
    
    Route::group(['prefix' => '/user', 'middleware' => 'permission'], function () {
        Route::get('/list', 'User\IndexController@userList')->name('user-list');
        Route::get('/form', 'User\IndexController@userForm')->name('user-form');
        Route::post('/form', 'User\IndexController@userFormSave')->name('user-save')->middleware('trialUserLimit');
        Route::get('/edit/{id}', 'User\IndexController@userEdit')->name('user-edit');
        Route::get('/find_immidiate_parent/{position_id}', 'User\StructureController@parentPosition')->name('find-parent');
        Route::any('/structure', 'User\StructureController@index')->name('structure');
        Route::get('/structure/remove/{id}', 'User\StructureController@structureRemove')->name('structure-remove');
        Route::any('/structure/{branch}/{department}', 'User\StructureController@index')->name('structure');
        Route::get('/structure/allremove/{company_id}/{id}', 'User\StructureController@structureRemoveAll')->name('structure-remove');
        Route::get('/structure/specialposremove/{company_id}/{id}', 'User\StructureController@rmSpecialPos')->name('structure-special-pos-remove');
        Route::post('/position/exchange/', 'User\StructureController@exchangePosition')->name('position-exchange');
        Route::get('/position-log/{id}', 'User\IndexController@positionLog')->name('position-log');
    });

    Route::group(['prefix' => '/process', 'middleware' => 'permission'], function () {
        Route::get('/categorized', 'Process\ProcessController@categorizedProcess')->name('process-categorized');
        Route::get('/categorized/{branchId}', 'Process\ProcessController@categorizedProcess')->name('process-categorized');
        Route::get('/categorized-filter/{branchId}', 'Process\ProcessController@categorizedFilterProcess')->name('process-categorized-filter');
        Route::get('/riskmap', 'Process\MapController@processRiskMap')->name('process-riskmap');
        Route::get('/opportunitymap', 'Process\MapController@processOpportunityMap')->name('process-opportunitymap');
        Route::get('/racimap', 'Process\MapController@processRACIMap')->name('process-racimap');
        Route::get('/racimap/{process_id}', 'Process\MapController@processRACIMap')->name('process-racimap');
        Route::get('/racimap/details/{process_id}/{posname}', 'Process\MapController@processRACIMapDetails')->name('process-racimap');
        Route::get('/documentmap', 'Process\MapController@processDocumentMap')->name('process-documentmap');
        Route::get('/regulationmap', 'Process\MapController@processRegulationMap')->name('process-regulationmap');
        Route::get('/list', 'Process\ProcessController@processList')->name('process-list');
        Route::match(['GET', 'POST'], '/edit/{id}', 'Process\ProcessController@processForm')->name('process-edit');
        Route::get('/form', 'Process\ProcessController@processForm')->name('process-form');
        Route::post('/form', 'Process\ProcessController@processFormSave')->name('process-save');
        Route::get('/remove/{id}', 'Process\ProcessController@processRemove')->name('proces-remove');
        Route::match(['get', 'post'], '/turtle', 'Process\TurtleController@turtle')->name('process-turtle');
        Route::match(['get', 'post'], '/turtle/{process_id}', 'Process\TurtleController@turtle')->name('turtle-with-data');

        Route::get('/sipoc/{process_id}', 'Process\SipocController@sipoc')->name('sipoc-with-data');

        Route::get('/turtle/details/{model}/{id}/{processOwnerId}', 'Process\TurtleController@turtleDetails')->name('turtle-details');
        Route::post('/steparrange', 'Process\ProcessController@stepsRearrange')->name('process-step-rearrange');
        Route::post('/processarrange', 'Process\ProcessController@processRearrange')->name('process-step-rearrange');
        Route::get('/changeprocess/{process_id}', 'Process\ProcessController@changeProcess')->name('change-process');
        Route::match(['GET', 'POST'], '/doctype', 'Process\ProcessController@processDocType');
        Route::get('/turtle/output/{process_id}', 'Process\TurtleController@turtleOutput')->name('turtle-output');

        Route::get('/job-description-metrix/{position_id?}/{branch_id?}', 'Process\JobDescriptionController@jobDescriptionMetrix')->name('job-description-metrix');

        Route::get('/competent-metrix/{position_id?}/{branch_id?}/{rating_id?}', 'Process\CompetentController@competentMetrix')->name('competent-metrix');
        Route::get('/competent-rating/{process}/{step}/{org}', 'Process\CompetentController@rating')->name('competent-metrix');
        Route::post('/competent-rating-save', 'Process\CompetentController@ratingSave')->name('competent-metrix');
        Route::get('/turtle/graph/{id}', 'Process\TurtleController@turtleGraph')->name('turtle-graph');

        Route::group(['prefix' => '/doctype'], function () {
            Route::get('/', 'Process\DocTypeController@list')->name('settings-doc-type');
            Route::get('/list/data', 'Process\DocTypeController@listData')->name('settings-doc-type');
            Route::post('/save', 'Process\DocTypeController@save')->name('settings-doc-type');
            Route::get('/remove/{id}', 'Process\DocTypeController@remove')->name('settings-doc-type');
        });

        Route::group(['prefix' => '/regulationtype'], function () {
            Route::get('/', 'Process\RegulationTypeController@list')->name('settings-regulation-type');
            Route::get('/list/data', 'Process\RegulationTypeController@listData')->name('settings-regulation-type');
            Route::post('/save', 'Process\RegulationTypeController@save')->name('settings-regulation-type');
            Route::get('/remove/{id}', 'Process\RegulationTypeController@remove')->name('settings-regulation-type');
        });
    });

    Route::group(['prefix' => '/master-kpi', 'middleware' => 'permission'], function () {
        Route::get('/list', 'Company\MasterKpiController@index')->name('master-kpi-list');
        Route::get('/data', 'Company\MasterKpiController@data')->name('master-kpi-list');
        Route::post('/form', 'Company\MasterKpiController@masterKpiSave')->name('master-kpi-save');
        Route::get('/remove/{id}', 'Company\MasterKpiController@masterKpiRemove')->name('master-kpi-remove');

        Route::get('/datalist/{id}', 'Company\MasterKpiController@masterKpiDataList')->name('master-kpi-data-list');
        Route::get('/datalist/{id}/data', 'Company\MasterKpiController@masterKpiData')->name('master-kpi-data-list');
        Route::get('/datalist/{id}/graph', 'Company\MasterKpiController@masterKpiGraph')->name('master-kpi-data-list');
        Route::post('/datalist/restricted', 'Company\MasterKpiController@restrict')->name('master-kpi-data-list');
        Route::post('/dataform', 'Company\MasterKpiController@masterKpiDataSave')->name('master-kpi-data-save');
        Route::get('/dataremove/{id}', 'Company\MasterKpiController@masterKpiDataRemove')->name('master-kpi-data-remove');

        Route::get('/kpi-relation-graph', 'Company\MasterKpiController@kpiRelationGraph')->name('kpi-relation-graph');
    });

    Route::group(['prefix' => '/process-step', 'middleware' => ['permission']], function () {
        Route::group(['prefix' => '/scope'], function () {
            Route::match(['GET', 'POST'], '/edit/{id}', 'Process\ScopeController@processScopeForm')->name('process-scope-edit');
            Route::get('/form', 'Process\ScopeController@processScopeForm')->name('process-scope-form');
            Route::post('/form', 'Process\ScopeController@processScopeFormSave')->name('process-scope-save');
            Route::get('/remove/{id}', 'Process\ScopeController@processScopeRemove')->name('proceess-scope-remove');
        });
        
        Route::group(['prefix' => '/procedure'], function () {
            Route::match(['GET', 'POST'], '/edit/{id}', 'Process\ProcedureController@processProcedureForm')->name('process-procedure-list');
            Route::match(['GET', 'POST'], '/edit/{id}/{version}', 'Process\ProcedureController@processProcedureForm')->name('process-procedure-list');
            Route::match(['GET', 'POST'], '/archive/{id}/{turtle_id}', 'Process\ProcedureController@processProcedureArchive')->name('process-procedure-archive');
            Route::get('/form', 'Process\ProcedureController@processProcedureForm')->name('process-procedure-form');
            Route::post('/form', 'Process\ProcedureController@processProcedureFormSave')->name('process-procedure-save');
            Route::get('/remove/{id}', 'Process\ProcedureController@processProcedureRemove')->name('process-procedure-remove');
            Route::get('/del-doc/{id}', 'Process\ProcedureController@processProcedureDocRemove')->name('process-proceduredoc-remove');
        });
        
        Route::group(['prefix' => '/regulation'], function () {
            Route::match(['GET', 'POST'], '/edit/{id}', 'Process\RegulationController@processRegulationForm')->name('process-regulation-edit');
            Route::match(['GET', 'POST'], '/edit/{id}/{version}', 'Process\RegulationController@processRegulationForm')->name('process-regulation-edit');
            Route::match(['GET', 'POST'], '/archive/{id}/{turtle_id}', 'Process\RegulationController@processregulationArchive')->name('process-regulation-archive');
            Route::get('/form', 'Process\RegulationController@processRegulationForm')->name('process-regulation-form');
            Route::post('/form', 'Process\RegulationController@processRegulationFormSave')->name('process-regulation-save');
            Route::get('/remove/{id}', 'Process\RegulationController@processRegulationRemove')->name('process-regulation-remove');
            Route::get('/del-doc/{id}', 'Process\RegulationController@processRegulationDocRemove')->name('process-regulationdoc-remove');
        });

        Route::group(['prefix' => '/kpi'], function () {
            Route::match(['GET', 'POST'], '/edit/{id}', 'Process\KpiController@processKpiForm')->name('process-kpi-edit');
            Route::get('/form', 'Process\KpiController@processKpiForm')->name('process-kpi-form');
            Route::post('/form', 'Process\KpiController@processKpiFormSave')->name('process-kpi-save');
            Route::get('/remove/{id}', 'Process\KpiController@processKpiRemove')->name('process-kpi-remove');

            Route::get('/datalist/{id}', 'Process\KpiController@processKpiDataList')->name('process-kpi-data-list');
            Route::get('/datalist/{id}/data', 'Process\KpiController@processKpiData')->name('process-kpi-data-list');
            Route::get('/datalist/{id}/graph', 'Process\KpiController@processKpiGraph')->name('process-kpi-data-list');
            Route::post('/dataform', 'Process\KpiController@processKpiDataSave')->name('process-kpi-data-save');
            Route::get('/dataremove/{id}', 'Process\KpiController@processKpiDataRemove')->name('process-kpi-data-remove');
        });

        Route::group(['prefix' => '/input'], function () {
            Route::match(['GET', 'POST'], '/edit/{id}', 'Process\InputController@processInputForm')->name('process-input-edit');
            Route::get('/form', 'Process\InputController@processInputForm')->name('process-input-form');
            Route::post('/form', 'Process\InputController@processInputFormSave')->name('process-input-save');
            Route::get('/remove/{id}', 'Process\InputController@processInputRemove')->name('process-input-remove');
        });

        Route::group(['prefix' => '/steps'], function () {
            Route::match(['GET', 'POST'], '/edit/{id}', 'Process\StepsController@processStepsForm')->name('process-step-edit');
            Route::get('/form', 'Process\StepsController@processStepsForm')->name('process-step-form');
            Route::post('/form', 'Process\StepsController@processStepsFormSave')->name('process-step-save');
            Route::get('/remove/{id}', 'Process\StepsController@processStepsRemove')->name('process-step-remove');
        });

        Route::group(['prefix' => '/output'], function () {
            Route::match(['GET', 'POST'], '/edit/{id}', 'Process\OutputController@processOutputForm')->name('process-output-edit');
            Route::get('/form', 'Process\OutputController@processOutputForm')->name('process-output-form');
            Route::post('/form', 'Process\OutputController@processOutputFormSave')->name('process-output-save');
            Route::get('/remove/{id}', 'Process\OutputController@processOutputRemove')->name('process-output-remove');
        });

        Route::group(['prefix' => '/risk'], function () {
            Route::match(['GET', 'POST'], '/edit/{id}', 'Process\RiskController@processRiskForm')->name('process-risk-edit');
            Route::get('/form', 'Process\RiskController@processRiskForm')->name('process-risk-form');
            Route::post('/form', 'Process\RiskController@processRiskFormSave')->name('process-risk-save');
            Route::get('/remove/{id}', 'Process\RiskController@processRiskRemove')->name('process-risk-remove');
            Route::post('/action/comment/', 'Process\RiskController@processRiskActionComment')->name('process-risk-action-comment');
            Route::post('/action/add/', 'Process\RiskController@processRiskActionAdd')->name('process-risk-action-add');
            Route::get('/action/eventlog/{riskId}/{type}', 'Process\RiskController@processRiskActionEventLog')->name('process-risk-action-event-log');
        });

        Route::group(['prefix' => '/opportunity'], function () {
            Route::match(['GET', 'POST'], '/edit/{id}', 'Process\OpportunityController@processOpportunityForm')->name('process-opportunity-edit');
            Route::get('/form', 'Process\OpportunityController@processOpportunityForm')->name('process-opportunity-form');
            Route::post('/form', 'Process\OpportunityController@processOpportunityFormSave')->name('process-opportunity-save');
            Route::get('/remove/{id}', 'Process\OpportunityController@processOpportunityRemove')->name('process-opportunity-remove');
            Route::post('/action/comment/', 'Process\OpportunityController@processOpportunityActionComment')->name('process-risk-action-comment');
            Route::post('/action/add/', 'Process\OpportunityController@processOpportunityActionAdd')->name('process-risk-action-add');
        });

        Route::group(['prefix' => '/resources'], function () {
            Route::group(['prefix' => '/activity'], function () {
                Route::get('/list', 'Process\ResourceActivityController@list')->name('resource-activity-list');
                Route::get('/data', 'Process\ResourceActivityController@data')->name('resource-activity-form');
                Route::post('/save', 'Process\ResourceActivityController@save')->name('resource-activity-save');
                Route::get('/remove/{id}', 'Process\ResourceActivityController@remove')->name('resource-activity-delete');
            });
            Route::match(['GET', 'POST'], '/edit/{id}', 'Process\ResourcesController@processResourcesForm')->name('process-resources-edit');
            Route::get('/form', 'Process\ResourcesController@processResourcesForm')->name('process-resources-form');
            Route::post('/form', 'Process\ResourcesController@processResourcesFormSave')->name('process-resources-save');
            Route::get('/remove/{id}', 'Process\ResourcesController@processResourcesRemove')->name('process-resources-remove');
            Route::get('/del-doc/{id}', 'Process\ResourcesController@processResourceDocRemove')->name('process-resourcedoc-remove');
            Route::get('/remove-activity/{activity_id}', 'Process\ResourcesController@processRemoveActivity')->name('process-activity-remove');
        });

        Route::group(['prefix' => '/roles'], function () {
            Route::match(['GET', 'POST'], '/edit/{id}', 'Process\RolesController@processRolesForm')->name('process-roles-edit');
            Route::get('/form', 'Process\RolesController@processRolesForm')->name('process-roles-form');
            Route::post('/form', 'Process\RolesController@processRolesFormSave')->name('process-roles-save');
            Route::get('/remove/{id}', 'Process\RolesController@processRolesRemove')->name('process-roles-remove');
            Route::get('/remove-roles/{process_id}/{step_id}/{position_id}', 'Process\RolesController@processRemoveRole')->name('process-role-remove');
        });
    });

    Route::group(['prefix' => '/audit', 'middleware' => 'permission'], function () {
        Route::get('/planning-add', 'Audit\PlanningController@auditPlanningForm')->name('planning-add');
        Route::get('/planning-add/{id}/{month}/{year}/{type}', 'Audit\PlanningController@auditPlanningForm')->name('planning-add-date');
        Route::post('/audit-duplicate-check', 'Audit\PlanningController@auditDuplicateCheck')->name('audit-duplicate-check');
        Route::post('/planning-save', 'Audit\PlanningController@auditPlanningSave')->name('planning-save');
        Route::get('/planning-list/{branchid}', 'Audit\CalenderController@planningList')->name('planning-list');
        Route::get('/planning-list/{branchid}/{year}', 'Audit\CalenderController@planningList')->name('planning-list-year');

        Route::get('/audit-prepare/{id}', 'Audit\PrepareController@auditPrepareForm')->name('preperation-edit');
        Route::post('/preparation-save', 'Audit\PrepareController@auditPrepareSave')->name('preperation-save');

        Route::get('/audit-execute/{audit_id}', 'Audit\ExecutionController@auditExecuteForm')->name('audit-execute');
        Route::get('/audit-execute/preview/{audit_id}', 'Audit\ExecutionController@executePreview')->name('audit-execute-preview');
        Route::post('/audit-execute-general-save', 'Audit\ExecutionController@auditExecuteGeneralFormSave')->name('audit-execute-general-save');
        Route::post('/audit-execute/{audit_id}', 'Audit\ExecutionController@auditExecuteDetailFormSave')->name('audit-execute-save');
        Route::post('/execute/rowsave', 'Audit\ExecutionController@detailsRowSave')->name('audit-execute-row-save');
        Route::get('/audit-execute/remove/{id}', 'Audit\ExecutionController@auditExecuteDetailsRemove')->name('audit-execute-details-remove');
        Route::get('/exec-audit-type/{ref_id}/{type}', 'Audit\ExecutionController@execAuditType')->name('exec-audit-type');
        Route::get('/execute/document/{execution_id}/{newrow}', 'Audit\ExecutionController@detailsDocumentForm')->name('execution-document-details');
        Route::get('/execute/document/{execution_id}/{newrow}/{evidence}', 'Audit\ExecutionController@detailsDocumentForm')->name('execution-document-details');
        Route::post('/execute/documentsave', 'Audit\ExecutionController@detailsDocumentSave')->name('execution-document-save');
        Route::get('/execute/documents/{execution_id}', 'Audit\ExecutionController@detailsDocuments')->name('execution-document-details-view');

        Route::post('/audit-release', 'Audit\ExecutionController@auditRelease')->name('audit-release');
        Route::get('/report-print/{audit_id}', 'Audit\ExecutionController@auditRepotPrint')->name('audit-report-print');

        Route::group(['prefix' => '/finding-type-list'], function () {
            Route::get('/', 'Audit\FindingTypeController@list')->name('find-type-list');
            Route::get('/list/data', 'Audit\FindingTypeController@listData')->name('find-type-list');
            Route::post('/save', 'Audit\FindingTypeController@save')->name('find-type-save');
            Route::get('/remove/{id}', 'Audit\FindingTypeController@remove')->name('finding-type-delete');
        });

        Route::get('/action-matrix', 'Audit\ActionMatrixController@actionMatrix')->name('action-matrix');
        Route::post('/action-matrix-save', 'Audit\ActionMatrixController@actionMatrixSave')->name('action-matrix-save');

        Route::get('/report-issue', 'Audit\ReportFindingController@reportIssueForm')->name('report-issue-form');
        Route::get('/report-issue/detail/{source?}/{reference?}', 'Audit\ReportFindingController@reportIssueDetail')->name('report-issue-form');
        Route::post('/report-issue-save', 'Audit\ReportFindingController@reportIssueSave')->name('report-issue-save');
        Route::get('/report-issue-edit/{id}', 'Audit\ReportFindingController@reportIssueFormEdit')->name('report-issue-edit');
        Route::get('/report-issue-fixing/{id}', 'Audit\ReportFindingController@reportedIssueFixForm')->name('report-issue-fix-form');
        Route::post('/report-issue-fixing/{id}', 'Audit\ReportFindingController@reportedIssueFixSave')->name('report-issue-fix-save');
        Route::get('/report-issue/data/{id}', 'Audit\ReportFindingController@getFixingData')->name('report-issue-form');
        Route::post('/report-issue-deadline-save', 'Audit\ReportFindingController@updateDeadline')->name('report-issue-deadline');

        Route::get('/source-type-list', 'Audit\SourceTypeController@sourceTypelist')->name('source-type-list');
        Route::get('/source-type-form', 'Audit\SourceTypeController@sourceTypeForm')->name('source-type-form');
        Route::post('/source-type-form', 'Audit\SourceTypeController@sourceTypeSave')->name('source-type-save');
        Route::get('/source-type-edit/{id}', 'Audit\SourceTypeController@sourceTypeEdit')->name('source-type-edit');
        Route::get('/source-type-delete/{id}', 'Audit\SourceTypeController@sourceTypeDelete')->name('source-type-delete');
        Route::get('/get-sourcetype-access/{id?}', 'Audit\SourceTypeController@getSourceTypeAccess')->name('get-sourcetype-access');

        Route::get('/report-issue-action/{issue_id}/{issue_type?}', 'Audit\ActionController@reportIssueActionForm')->name('report-issue-action');
        Route::get('/report-issue-view/{issue_id}/{issue_type?}', 'Audit\ActionController@reportIssueActionForm')->name('report-issue-view');
        Route::get('/action-by-member/{issue_id}', 'Audit\ActionController@reportIssueActionForm')->name('action-by-member-form');

        Route::post('/issue-action-save', 'Audit\ActionController@issueActionSave')->name('issue-action-save');
        Route::post('/issue-action-save/immidiate-action', 'Audit\ActionController@issueImmidiateActionSave')->name('issue-action-save');
        Route::post('/issue-action-save/root-cause', 'Audit\ActionController@issueRootCauseSave')->name('issue-action-save');
        Route::post('/issue-action-save/corrective-measure', 'Audit\ActionController@issueCorrectiveMeasureSave')->name('issue-action-save');
        Route::post('/issue-action-save/effectiveness', 'Audit\ActionController@issueEffectivenessSave')->name('issue-action-save');
       
        Route::post('/action-by-member-save', 'Audit\ActionController@actionByMemberSave')->name('action-by-member-save');
        Route::get('/delegation/document-form/{delegationId}', 'Audit\ActionController@actionByMemberdocForm')->name('action-by-member-doc-form');
        Route::post('/delegation/document-form-save', 'Audit\ActionController@actionByMemberdocSave')->name('action-by-member-doc-save'); 
        Route::get('/check-process-branch/{branchId}', 'Audit\PlanningController@checkProcessBranch')->name('check-process-branch');
        Route::post('/check-process-owner', 'Audit\PlanningController@checkProcessOwner')->name('check-process-owner');
        Route::get('/supplier-evaluation', 'Audit\SupplierController@index')->name('audit-supplier-evaluation');
        Route::post('/supplier-rating-save', 'Audit\SupplierController@save')->name('audit-supplier-evaluation');
        Route::get('/supplier-evaluation/list/{supplier}', 'Audit\SupplierController@list')->name('audit-supplier-evaluation');
    });

    Route::group(['prefix' => '/context', 'middleware' => 'permission'], function () {
        Route::get('/list', 'Context\SubjectController@list')->name('context-subject-list');
        Route::get('/data', 'Context\SubjectController@data')->name('context-subject-form');
        Route::post('/save', 'Context\SubjectController@save')->name('context-subject-save');
        Route::get('/remove/{id}', 'Context\SubjectController@remove')->name('context-subject-delete');

        Route::get('/context-analysis-form', 'Context\AnalysisController@contextAnalysisForm')->name('context-analysis-form');
        Route::post('/context-analysis-form', 'Context\AnalysisController@contextAnalysisSave')->name('context-analysis-save');
        Route::get('/context-analysis-edit/{id}', 'Context\AnalysisController@contextAnalysisEdit')->name('context-analysis-edit');
        Route::get('/context-analysis-delete/{id}', 'Context\AnalysisController@contextAnalysisDelete')->name('context-analysis-delete');
        Route::get('/context-map', 'Context\ContextController@contextMap')->name('context-map');
        Route::get('/filter-map/{branchId}', 'Context\ContextController@filterContextMap')->name('filter-context-map');
        Route::get('/context-action-log/{contextId}/{type}', 'Context\ContextController@actionLog')->name('context-action-log');
        Route::post('/check-process-branch', 'Context\AnalysisController@checkProcessBranch')->name('check-process-branch');
    });

    Route::group(['prefix' => '/settings', 'middleware' => 'permission'], function () {
        Route::get('/risk-opp-action', 'SettingsController@riskOppAction')->name('risk-opp-action')->middleware('permission');
        Route::post('/risk-opp-action', 'SettingsController@riskOppActionSave')->name('risk-opp-action')->middleware('permission');
        Route::get('/company', 'SettingsController@companySettings')->name('company-settings');
        Route::post('/company-save', 'SettingsController@companySettingsSave')->name('company-settings-save');
    });

    Route::group(['prefix' => '/stakeholder-analysis', 'middleware' => 'permission'], function () {
        Route::get('/map', 'StakeholderController@map')->name('stakeholder-analysis-map');
        Route::get('/filter-map/{branchId}', 'StakeholderController@filterStackMap')->name('filter-stake-map');
        Route::get('/category', 'StakeholderController@category')->name('stakeholder-analysis-category');
        Route::post('/category', 'StakeholderController@categorySave')->name('stakeholder-analysis-category');
        Route::get('/category/data', 'StakeholderController@categoryData')->name('stakeholder-analysis-category');
        Route::post('/category/remove', 'StakeholderController@categoryRemove')->name('stakeholder-analysis-category-remove');
        Route::get('/list', 'StakeholderController@list')->name('stakeholder-analysis-list');
        Route::get('/form', 'StakeholderController@form')->name('stakeholder-analysis-form');
        Route::post('/form', 'StakeholderController@save')->name('stakeholder-analysis-form');
        Route::get('/edit/{id}', 'StakeholderController@edit')->name('stakeholder-analysis-edit');
        Route::get('/remove/{id}', 'StakeholderController@remove')->name('stakeholder-analysis-remove');
    });

    Route::group(['prefix' => '/membership', 'middleware' => 'permission'], function () {
        Route::get('/', 'Membership\TrialController@index')->name('membership');
        Route::get('/detail', 'Membership\TrialController@memberDetail')->name('member-detail');
        Route::get('/modify', 'Membership\TrialController@memberModify')->name('member-modify');
        Route::post('/modify', 'Membership\TrialController@memberModifySave')->name('member-modify');
        Route::get('/company-details', 'Membership\TrialController@companyDetails')->name('company-detail');
        Route::post('/company-details', 'Membership\TrialController@companyDetailsSave')->name('company-detail-modify');
        Route::get('/choose-iso', 'Membership\TrialController@chooseIso')->name('choose-iso');
        Route::post('/choose-iso', 'Membership\TrialController@chooseIsoSave')->name('choose-iso');
        Route::get('/choose-addons', 'Membership\TrialController@chooseAddons')->name('choose-addons');
        Route::post('/choose-addons', 'Membership\TrialController@chooseAddonSave')->name('choose-addon-save');
        Route::get('/preview', 'Membership\TrialController@preview')->name('preview');
        Route::any('/make-payment', 'Membership\TrialController@makePayment')->name('make-payment');
        Route::get('/success', 'Membership\TrialController@success')->name('success');
        Route::get('/account', 'Membership\AddonController@account')->name('invoice-list');
        Route::get('/iso', 'Membership\AddonController@iso')->name('more-iso');
        Route::post('/iso', 'Membership\AddonController@isoSave')->name('more-iso');
        Route::get('/user', 'Membership\AddonController@user')->name('more-user');
        Route::post('/user', 'Membership\AddonController@userSave')->name('more-user');
        Route::get('/storage', 'Membership\AddonController@storage')->name('more-storage');
        Route::post('/storage', 'Membership\AddonController@storageSave')->name('more-storage');
        Route::get('/preview-addon', 'Membership\AddonController@preview')->name('preview');
        Route::any('/addon-payment', 'Membership\AddonController@makePayment')->name('addon-payment');
        Route::get('/addon-success', 'Membership\AddonController@success')->name('success');
        Route::get('/transaction-details/{id}', 'Membership\AddonController@transactionDetail')->name('transaction-detail');
        Route::get('/invoice-print/{id}', 'Membership\AddonController@printInvoice')->name('invoice-print'); 
        Route::post('/approve-price-request', 'Membership\AddonController@approvePriceRequest')->name('approve-price-request'); 
        Route::get('/end-subscription', 'Membership\AddonController@endSubscription')->name('end-subscription');    
    });

    Route::group(['prefix' => '/supplier', 'middleware' => 'permission'], function () {
        Route::get('/list', 'Supplier\IndexController@list')->name('suppliers');
        Route::get('/list/data', 'Supplier\IndexController@listData')->name('suppliers');
        Route::post('/save', 'Supplier\IndexController@save')->name('suppliers');
        Route::post('/status', 'Supplier\IndexController@status')->name('suppliers');
        Route::get('/remove/{id}', 'Supplier\IndexController@remove')->name('suppliers');

        Route::group(['prefix' => '/parameters'], function () {
            Route::get('/', 'Supplier\ParameterController@list')->name('suppliers');
            Route::get('/list/data', 'Supplier\ParameterController@listData')->name('suppliers');
            Route::post('/save', 'Supplier\ParameterController@save')->name('suppliers');
            Route::get('/remove/{id}', 'Supplier\ParameterController@remove')->name('suppliers');
        });

        Route::group(['prefix' => '/ratings'], function () {
            Route::get('/', 'Supplier\RatingsController@list')->name('suppliers');
            Route::get('/list/data', 'Supplier\RatingsController@listData')->name('suppliers');
            Route::post('/save', 'Supplier\RatingsController@save')->name('suppliers');
            Route::get('/remove/{id}', 'Supplier\RatingsController@remove')->name('suppliers');
        });

        Route::get('/download-scar/{id}', 'Audit\SupplierController@downloadScar')->name('download-scar-report');
    });

    Route::get('/competent-rating-issue-fixing/{id}', 'Process\CompetentController@issueFixing')->name('competent-rating-issue-fixing');
    Route::post('/competent-rating-issue-fixing/{id}', 'Process\CompetentController@issueFixingSave')->name('competent-rating-issue-fixing');
    Route::get('/supplier-rating-issue-fixing/{id}', 'Supplier\RatingsController@issueFixing')->name('supplier-rating-issue-fixing');
    Route::post('/supplier-rating-issue-fixing/{id}', 'Supplier\RatingsController@issueFixingSave')->name('supplier-rating-issue-fixing');
    Route::get('/corrective-action-issue-fixing/{id}', 'Audit\SupplierController@correctiveIssueFixing')->name('corrective-action-issue-fixing');
    Route::post('/corrective-action-issue-fixing/{id}', 'Audit\SupplierController@correctiveIssueFixingSave')->name('corrective-action-issue-fixing');

    Route::get('/out', 'Site\IndexController@logout');

    Route::get('/oauth', function () {
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => '<YOUR_APPLICATION_ID>',
            'clientSecret' => '<YOUR_PASSWORD>',
            'redirectUri' => 'http://localhost/toodoo/oauth',
            'urlAuthorize' => 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize',
            'urlAccessToken' => 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
            'urlResourceOwnerDetails' => '',
            'scopes' => 'openid mail.send',
        ]);

        if (!$request->has('code')) {
            return redirect($provider->getAuthorizationUrl());
        }
    });
});
