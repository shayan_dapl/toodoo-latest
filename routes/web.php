<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Site\IndexController Routes
Route::get('/', 'Site\IndexController@index')->name('index')->middleware('language');
Route::post('/', 'Site\IndexController@login')->name('index')->middleware('language');

Route::any('/prevent', 'Site\IndexController@prevent');

Route::get('/firstlogin/{inviteToken}', 'Site\AccountController@createAccount')->name('create-account')->middleware('language');
Route::post('/firstlogin', 'Site\AccountController@SaveCreateAccount')->name('save-create-account')->middleware('language');

Route::get('/lang/{name}', 'Site\IndexController@setLang')->middleware('language');

Route::group(['middleware' => ['auth:web', 'permission', 'language']], function () {
    //Site\IndexController Routes
    Route::get('/profile', 'Site\ProfileController@profile')->name('profile');
    Route::post('/profile', 'Site\ProfileController@profileSave')->name('profile-save');

    Route::get('/language', 'Site\IndexController@languageManager')->name('manage-language-form');
    Route::post('/language', 'Site\IndexController@languageManager')->name('manage-language-save');
    Route::post('/language/restore', 'Site\IndexController@languageRestore')->name('manage-language-restore');

    Route::get('/vat-settings', 'SettingsController@vatSettings')->name('vat-settings-form');
    Route::get('/vat-settings/edit/{id}', 'SettingsController@editVatSettings')->name('edit-vat-settings');
    Route::post('/vat-settings', 'SettingsController@saveVatSettings')->name('save-vat-settings');

    Route::get('/out', 'Site\IndexController@logout')->middleware('language');

    //Company\HomeController Routes
    Route::get('/home', 'Company\HomeController@home')->name('dashboard');
    Route::get('/home/show-companies/{type}', 'Company\HomeController@showCompanies')->name('dashboard');
    Route::get('/home/show-invoice/{id?}', 'Company\HomeController@showInvoice')->name('dashboard');
    Route::get('/home/download-invoice-list/{id?}', 'Company\HomeController@downloadInvoiceList')->name('dashboard');
    
    Route::get('/list', 'Company\AdminController@listing')->name('customeradmin-list');
    Route::get('/subscription-list', 'Company\AdminController@subscriptionListing')->name('subscription-list');
    Route::post('/send-price-increase-request', 'Company\AdminController@sendPriceIncreaseRequest')->name('send-price-increase-request');
    Route::get('/view-detail/{id}', 'Company\AdminController@viewDetail')->name('view-detail');
    Route::get('/form', 'Company\AdminController@form')->name('customeradmin-form');
    Route::post('/form', 'Company\AdminController@formSave')->name('customeradmin-save');
    Route::get('/edit/{id}', 'Company\AdminController@edit')->name('customeradmin-edit');
    Route::get('/account-info/{id}', 'Company\AdminController@accountInfo')->name('customeradmin-edit');
    Route::get('/remove/{id}', 'Company\AdminController@remove')->name('customeradmin-remove');

    Route::get('/password', 'Company\AdminController@changePassword')->name('change-password-form');
    Route::post('/password', 'Company\AdminController@changePasswordSave')->name('change-password-save');

    Route::get('/permission/{id}', 'Company\HomeController@accessPermission')->name('access-permission-form');
    Route::post('/permission/{id}', 'Company\HomeController@accessPermissionSave')->name('access-permission-save');

    Route::group(['prefix' => 'audit'], function () {
        Route::get('/get-audit-type/{ref_id}/{type}', 'Audit\TypeController@getAuditType')->name('get-audit-type');
        Route::get('/exec-audit-type/{ref_id}', 'Audit\PlanningController@execAuditType')->name('exec-audit-type');

        //ISO clause
        Route::group(['prefix' => 'iso'], function () {
            Route::get('/list', 'Audit\IsoController@auditIsoList')->name('iso-list');
            Route::get('/form', 'Audit\IsoController@auditIsoForm')->name('iso-form');
            Route::post('/form', 'Audit\IsoController@auditIsoFormSave')->name('iso-save');
            Route::get('/edit/{id}', 'Audit\IsoController@auditIsoFormEdit')->name('iso-edit');
            Route::get('/delete/{id}', 'Audit\IsoController@auditIsoDelete')->name('iso-remove');
        });

        //Audit type
        Route::group(['prefix' => 'type'], function () {
            Route::get('/{id}', 'Audit\TypeController@auditTypeList')->name('audit-type-list');
            Route::get('/form/{id}', 'Audit\TypeController@auditTypeForm')->name('audit-type-form');
            Route::post('/save', 'Audit\TypeController@auditTypeSave')->name('audit-type-save');
            Route::get('/edit/{id}', 'Audit\TypeController@auditTypeEdit')->name('audit-type-edit');
            Route::get('/delete/{id}', 'Audit\TypeController@auditTypeDelete')->name('audit-type-delete');
        });
    });

    Route::group(['prefix' => 'package'], function () {
        Route::get('/list', 'Membership\PackageController@list')->name('package-list');
        Route::get('/form', 'Membership\PackageController@form')->name('package-form');
        Route::post('/form', 'Membership\PackageController@save')->name('package-save');
        Route::get('/edit/{id}', 'Membership\PackageController@edit')->name('package-edit');
        Route::get('/remove/{id}', 'Membership\PackageController@remove')->name('package-remove');
        Route::get('/user-plan', 'Membership\PackageController@userPlanlist')->name('user-plan-list');
        Route::get('/user-plan/edit/{id}', 'Membership\PackageController@edituserPlan')->name('user_plan-edit');
        Route::post('/user-plan', 'Membership\PackageController@userPlanSave')->name('user_plan-save');
    });

    Route::group(['prefix' => 'video'], function () {
        Route::get('/list', 'VideoController@list')->name('video-list');
        Route::get('/form', 'VideoController@form')->name('video-form');
        Route::post('/form', 'VideoController@save')->name('video-save');
        Route::get('/edit/{id}', 'VideoController@edit')->name('video-edit');
        Route::get('/delete/{id}', 'VideoController@delete')->name('video-delete');

        Route::get('/category/list', 'VideoController@catList')->name('video-cat-list');
        Route::get('/category/form', 'VideoController@catForm')->name('video-cat-form');
        Route::post('/category/form', 'VideoController@catFormSave')->name('video-cat-save');
        Route::get('/category/edit/{id}', 'VideoController@catFormEdit')->name('video-cat-edit');
        Route::get('/category/delete/{id}', 'VideoController@catDelete')->name('video-cat-remove');

        Route::get('/get-video-sub-category/{pid}','VideoController@getSubCategory')->name('video-sub-category');
        Route::get('/get-video-sub-category/{pid}/{selectedid}','VideoController@getSubCategory')->name('video-sub-category');
    });

    Route::get('/trial-period', 'SettingsController@generalSettings')->name('general-settings');
    Route::post('/trial-period', 'SettingsController@generalSettings')->name('general-settings');

    Route::get('/kpi-unit', 'SettingsController@kpiUnit')->name('kpi-unit');
    Route::post('/kpi-unit', 'SettingsController@kpiUnitSave')->name('kpi-unit');
    Route::get('/kpi-unit/data', 'SettingsController@kpiUnitData')->name('kpi-unit');

    Route::get('/getExport/{type}', 'Company\HomeController@getExport')->name('getExport');
});

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request')->middleware('language');;
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset')->middleware('language');;
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
